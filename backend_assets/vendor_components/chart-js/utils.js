'use strict';

window.chartColors = {
	
yellow	: 'rgb(225,178,43)',
black	: 'rgb(17, 17, 17)',
purple	: 'rgb(116, 96, 238)',
red		: 'rgb(252, 75, 108)',
green	: 'rgb(125, 171, 46)',
blue	: 'rgb(56, 154, 240)',
C1		: 'rgb(68, 145, 134)',
C2		: 'rgb(145, 228, 166)',
C3		: 'rgb(95, 100, 192)',
C4		: 'rgb(69, 48, 100)',
C5		: 'rgb(221, 221, 199)',
C6		: 'rgb(181, 26, 98)',
C7		: 'rgb(222, 112, 60)',
C8		: 'rgb(250, 176, 64)',
C9		: 'rgb(79, 111, 134)',
C10		: 'rgb(47, 60, 79)',
C11		: 'rgb(232, 126, 36)',
C12		: 'rgb(232, 76, 61)',
C13		: 'rgb(103,155,214)',
C14		: 'rgb(190, 195, 199)',
C15		: 'rgb(149, 165, 165)',
C16		: 'rgb(217, 198, 47)',
C17		: 'rgb(247, 75, 104)',
C18		: 'rgb(251, 254, 162)',
C19		: 'rgb(42, 141, 240)',
C20		: 'rgb(14, 65, 130)',
C21		: 'rgb(154, 151, 135)',
C22		: 'rgb(204, 191, 116)',
C23		: 'rgb(125, 149, 217)',
C24		: 'rgb(71, 103, 179)',
C25		: 'rgb(66, 72, 93)',
C26		: 'rgb(38, 198, 218)',
C27		: 'rgb(29, 142, 192)',
C28		: 'rgb(231, 183, 56)',
C29		: 'rgb(0, 121, 133)',
C30		: 'rgb(220, 0, 111)',
C31		: 'rgb(0, 158, 216)',
C32		: 'rgb(246, 202, 54)',
C33		: 'rgb(134, 83, 219)',
C34		: 'rgb(219, 83, 169)',
C35		: 'rgb(83, 101, 219)',

};

(function(global) {
	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	];

	var Samples = global.Samples || (global.Samples = {});
	var Color = global.Color;

	Samples.utils = {
		// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
		srand: function(seed) {
			this._seed = seed;
		},

		rand: function(min, max) {
			var seed = this._seed;
			min = min === undefined ? 0 : min;
			max = max === undefined ? 1 : max;
			this._seed = (seed * 9301 + 49297) % 233280;
			return min + (this._seed / 233280) * (max - min);
		},

		numbers: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 1;
			var from = cfg.from || [];
			var count = cfg.count || 8;
			var decimals = cfg.decimals || 8;
			var continuity = cfg.continuity || 1;
			var dfactor = Math.pow(10, decimals) || 0;
			var data = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = (from[i] || 0) + this.rand(min, max);
				if (this.rand() <= continuity) {
					data.push(Math.round(dfactor * value) / dfactor);
				} else {
					data.push(null);
				}
			}

			return data;
		},

		labels: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 100;
			var count = cfg.count || 8;
			var step = (max - min) / count;
			var decimals = cfg.decimals || 8;
			var dfactor = Math.pow(10, decimals) || 0;
			var prefix = cfg.prefix || '';
			var values = [];
			var i;

			for (i = min; i < max; i += step) {
				values.push(prefix + Math.round(dfactor * i) / dfactor);
			}

			return values;
		},

		months: function(config) {
			var cfg = config || {};
			var count = cfg.count || 12;
			var section = cfg.section;
			var values = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = MONTHS[Math.ceil(i) % 12];
				values.push(value.substring(0, section));
			}

			return values;
		},

		color: function(index) {
			return COLORS[index % COLORS.length];
		},

		transparentize: function(color, opacity) {
			var alpha = opacity === undefined ? 0.5 : 1 - opacity;
			return Color(color).alpha(alpha).rgbString();
		}
	};

	// DEPRECATED
	window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(-100, 100));
	};

	// INITIALIZATION

	Samples.utils.srand(Date.now());

	// Google Analytics
	/* eslint-disable */
	if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-28909194-3', 'auto');
		ga('send', 'pageview');
	}
	/* eslint-enable */

}(this));

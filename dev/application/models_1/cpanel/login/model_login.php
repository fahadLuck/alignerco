<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

class Model_login extends CI_Model {
	
	function chk_member_login($email,$password) {
	
					$this->db->select('organizationTeam.id as teamID,organizationTeam.HR_id as teamMemberHRID,organizationTeam.organization_id as teamMemberOrganizationID,organizationTeam.email as teamMemberOfficialEmail,organizationTeam.created as TeamMemberCreated,
												HR.name as teamMemberName,HR.mobile as teamMemberMobile,HR.mobile_verified_status as temMemberMobileVerifiedStatus,HR.photo as teamMemberPhoto,
											    account.id as accountID,account.account_status as accountStatus');
										  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as account');
					$this->db->where('account.password',$password);
					$this->db->where("(account.email = '".$email."' OR account.mobile = '".$email."' OR account.username = '".$email."' OR account.code = '".$email."')", NULL, FALSE);
					$this->db->where('account.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('organizationTeam.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationTeam.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationTeam', 'account.team_id = organizationTeam.id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationTeam.HR_id = HR.id', 'left');
				
					$result = $this->db->get();  
		
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}



	function chk_member_login_for_patient($email,$password) {
	
					$this->db->select('organizationTeam.id as teamID,organizationTeam.HR_id as teamMemberHRID,organizationTeam.organization_id as teamMemberOrganizationID,organizationTeam.email as teamMemberOfficialEmail,organizationTeam.created as TeamMemberCreated,
												HR.name as teamMemberName,HR.mobile as teamMemberMobile,HR.mobile_verified_status as temMemberMobileVerifiedStatus,HR.photo as teamMemberPhoto,
											    account.id as accountID,account.account_status as accountStatus');
										  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as account');
					$this->db->where('account.password',$password);
					$this->db->where("(account.email = '".$email."' OR account.mobile = '".$email."' OR account.username = '".$email."' OR account.code = '".$email."')", NULL, FALSE);
					$this->db->where('account.is_deleted',HARD_CODE_ID_NO);
					/*$this->db->where('organizationTeam.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationTeam.employment_status !=',HARD_CODE_ID_LAYOFF);*/
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationTeam', 'account.team_id = organizationTeam.id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationTeam.HR_id = HR.id', 'left');
				
					$result = $this->db->get();  
		
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}

	
	
	function chk_member_email($email) {
	
					$this->db->select('organizationTeam.id as teamID,organizationTeam.HR_id as  teamMemberHRID,organizationTeam.organization_id as teamMemberInstituteID,organizationTeam.email as teamMemberOfficialEmail,organizationTeam.created as TeamMemberCreated,
												HR.name as teamMemberName,HR.mobile as teamMemberMobile,HR.mobile_verified_status as temMemberMobileVerifiedStatus,HR.photo as teamMemberPhoto,
											    account.id as accountID,account.account_status as accountStatus');
										  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as account');
					$this->db->where("(account.email = '".$email."' OR account.mobile = '".$email."' OR account.code = '".$email."')", NULL, FALSE);
					$this->db->where('account.is_deleted',HARD_CODE_ID_NO);
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationTeam', 'account.team_id = organizationTeam.id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationTeam.HR_id = HR.id', 'left');
				
					$result = $this->db->get();
		
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}
	
	function chk_password_change($accountID,$oldPassword) {
		
		$this->db->select('account.password as password');
		$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as account');
		$this->db->where('account.id', $accountID);
		$this->db->where('account.password',$oldPassword);
		$this->db->where('account.is_deleted', HARD_CODE_ID_NO);
		
		$result = $this->db->get();
		 
		 if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}
	
	function update_password($accountID,$data) {
		
		$this->db->where('id', $accountID);
		$this->db->update(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,$data);
	}
	
	function getAccountInfo($accountID) {
	
		$this->db->select('account.id as accountID,account.account_status as accountStatus,
									organizationTeam.id as teamID,organizationTeam.HR_id as teamMemberHRID,organizationTeam.organization_id as teamMemberOrganizationID,
									HR.name as temMemberName,
									HR.photo as temMemberPhoto');
		
		$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as account');
		$this->db->where('account.id',$accountID);
		$this->db->where('account.is_deleted',HARD_CODE_ID_NO);
		$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationTeam', 'organizationTeam.id = account.team_id', 'left');
		$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationTeam.HR_id = HR.id', 'left');
		
		$result =  $this->db->get();
		
		if ($result->num_rows() > 0)  {
			
			return $result->row_array();	
		
		} else {
		
			return false;	
		}
	}
	
 }
 
?>
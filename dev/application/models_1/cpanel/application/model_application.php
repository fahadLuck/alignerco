<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

class Model_application extends CI_Model {
	
	function userInfo($accountID) {
						
					$this->db->select('organizationTeam.id as teamID,
									   organizationTeam.HR_id as teamMemberHRID,
									   organizationTeam.organization_id as teamMemberOrganizationID,
									   organizationTeam.email as teamMemberOfficialEmail,
									   HR.name as teamMemberName,
									   HR.personal_email as teamMemberPersonalEmail,
									   HR.identity_card as teamMemberCNIC,
									   HR.identity_card_formatted as teamMemberCNICFormatted,
									   HR.about as teamMemberAbout,
									   HR.mobile as teamMemberMobile,
									   HR.mobile_formatted as teamMemberMobileFormatted,
									   HR.mobile_verified_status as teamMemberMobileVerifiedStatus,
									   HR.photo as teamMemberPhoto,
									   prefix.name as prefixName,
									   account.email as accountEmail,
									   account.mobile as accountMobile,
									   account.account_status as accountStatus,
									   account.created as accountCreated,
									   company.id as companyID,
                                       company.name as company');
					
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as account');
					$this->db->where('account.id',$accountID);
					$this->db->where('account.account_status',HARD_CODE_ID_ACTIVATED);
					$this->db->where('account.is_deleted',HARD_CODE_ID_NO);
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationTeam', 'account.team_id = organizationTeam.id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationTeam.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'HR.company = company.id', 'left');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}
			
	function userRoles($teamID) {
						
					$this->db->select('medicalTeamRole.id as tableID,
									   role.id as roleID,
									   role.name roleName');
					
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE.' as medicalTeamRole');
					$this->db->where('medicalTeamRole.team_id',$teamID);
					$this->db->join(JOB_ROLES_TABLE.' as role', 'medicalTeamRole.role_id = role.id', 'left');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
	}


	function userRolesForPatient($userOfficialAccountID) {
						
					$this->db->select('medicalTeamRole.id as tableID,
									   role.id as roleID,
									   role.name roleName');
					
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE.' as medicalTeamRole');
					$this->db->where('medicalTeamRole.team_official_id',$userOfficialAccountID);
					$this->db->join(JOB_ROLES_TABLE.' as role', 'medicalTeamRole.role_id = role.id', 'left');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
	}
	
	
	function getOfficialAccountPrivilegesModules($userOfficialAccountID) {
						
					$this->db->select('officialPrivilege.id as tableID,officialPrivilege.module_id as privilegeModuleID');
					
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_MODULE_ACCESS_TABLE.' as officialPrivilege');
					$this->db->where('officialPrivilege.team_official_id',$userOfficialAccountID);
					$this->db->where('officialPrivilege.is_deleted',HARD_CODE_ID_NO);
					
					$result = $this->db->get();  
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 		return false;
		   		  }
			
			}
	
	function getRolePrivilegesModules($roleID) {
			
					$this->db->select('rolePrivilege.id as tableID,rolePrivilege.role_id as privilegeRoleID,rolePrivilege.module_id as privilegeModuleID');
					
					$this->db->from(MY_ORGANIZATION_JOB_ROLES_MODULE_ACCESS_TABLE.' as rolePrivilege');
					$this->db->where('rolePrivilege.role_id',$roleID);
					$this->db->where('rolePrivilege.is_deleted',HARD_CODE_ID_NO);
					$this->db->join(MY_ORGANIZATION_JOB_ROLES_TABLE.' as role', 'rolePrivilege.role_id = role.id', 'left');
					
					$result = $this->db->get();
		  			
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}
	
	function getLoggingEmployee($employeeID) {
				
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.id as employeeHRID,prefix.id as prefixID,
									   prefix.name as prefixName,
									   HR.name as employeeName,
									   organizationEmployee.email as employeeEmail,
									   HR.identity_card_formatted as employeeCNICFormatted,
									   HR.identity_card as employeeCNIC,
									   HR.personal_email as employeePersonalEmail,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile,
									   HR.gender as employeeGenderID');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					$this->db->where('organizationEmployee.id',$employeeID);
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					
					$result = $this->db->get(); 
		
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}
	
	function getPrefix() {
						
					$this->db->select('prefix.id as tableID,
									   prefix.name as prefixName,
									   prefix.description as prefixDescription');
					
					$this->db->from(PREFIX_TABLE.' as prefix');
					$this->db->where('prefix.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('prefix.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('prefix.name','ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}
			
	function getGenderMaleFemale() {
						
					$this->db->select('gender.id as tableID,
									   gender.id as genderID,
									   gender.name as genderName,
									   gender.sef_url as genderSefURL,
									   gender.code as genderCode,
									   gender.description as genderDescription');
					
					$this->db->from(GENDER_TABLE.' as gender');
					$this->db->where_in('id', array(HARD_CODE_ID_MALE,HARD_CODE_ID_FEMALE));
					$this->db->where('gender.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('gender.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('gender.name','ASC');
					
					$result = $this->db->get(); 

					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}	
			
	function getGenderName($ID=NULL) {
						
					$this->db->select('gender.id as tableID,
									   gender.name as genderName,
									   gender.sef_url as genderSefURL,
									   gender.code as genderCode,
									   gender.description as genderDescription');
					
					$this->db->from(GENDER_TABLE.' as gender');
					$this->db->where('gender.id',$ID);
					$this->db->where('gender.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('gender.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('gender.name','ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}	
	
	function getMaritalStatus() {
						
					$this->db->select('marital.id as tableID,
									   marital.name as maritalStatusName,
									   marital.sef_url as maritalStatusSefURL,
									   marital.code as maritalStatusCode,
									   marital.description as maritalStatusDescription');
					
					$this->db->from(MARITAL_STATUS_TABLE.' as marital');
					$this->db->where('marital.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('marital.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('marital.name','ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}	
			
	function getNationalities() {
						
					$this->db->select('nationality.id as tableID,
									   nationality.name as nationalityName,
									   nationality.sef_url as nationalitySefURL,
									   nationality.description as nationalityDescription');
					
					$this->db->from(NATIONALITIES_TABLE.' as nationality');
					$this->db->where('nationality.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('nationality.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('nationality.name','ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}
			
	function getLanguages() {
						
					$this->db->select('language.id as tableID,
									   language.name as languageName,
									   language.sef_url as languageSefURL,
									   language.description as languageDescription');
					
					$this->db->from(LANGUAGES_TABLE.' as language');
					$this->db->where('language.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('language.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('language.name','ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}	
		
	function getBloodGroups() {
						
					$this->db->select('blood.id as tableID,
									   blood.name as bloodGroupName,
									   blood.sef_url as bloodGruopSefURL,
									   blood.description as bloodGruopDescription');
					
					$this->db->from(BLOOD_GROUPS_TABLE.' as blood');
					$this->db->where('blood.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('blood.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('blood.id','ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}	
			
	function getReligions() {
						
					$this->db->select('religion.id as tableID,
									   religion.id as religionID,
									   religion.name as religionName,
									   religion.sef_url as religionSefURL,
									   religion.description as religionDescription');
					
					$this->db->from(RELIGIONS_TABLE.' as religion');
					$this->db->where('religion.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('religion.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('religion.order','ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}
	
	function getOccupations() {
						
					$this->db->select('occupation.id as tableID,
									   occupation.id as occupationID,
									   occupation.name as occupationName,
									   occupation.sef_url as occupationSefURL,
									   occupation.description as occupationDescription');
					
					$this->db->from(OCCUPATIONS_TABLE.' as occupation');
					$this->db->where('occupation.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('occupation.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('occupation.order','ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}
			
	function getModeOfEmployment() {
						
		  $this->db->select('mode.id as tableID,mode.name as modeName,mode.description as modeDescription');
		  
		  $this->db->from(MODE_OF_EMPLOYMENT_TABLE.' as mode');
		  $this->db->where('mode.publish_status',HARD_CODE_ID_PUBLISHED);
		  $this->db->where('mode.is_deleted',HARD_CODE_ID_NO);
		  $this->db->order_by('mode.name','ASC');
		  
		  $result = $this->db->get(); 
		  
		  if ($result->num_rows() > 0) {

			  return $result;

		 } else {

			  return false;
		}
  
	}
	
	function getEmploymentStatus() {
						
			$this->db->select('employment.id as tableID,employment.name as employmentStatusName,employment.sef_url as employmentStatusSefURL,employment.code as employmentStatusCode,employment.description as employmentStatusDescription');
			
			$this->db->from(EMPLOYMENT_STATUSES_TABLE.' as employment');
			$this->db->where('employment.publish_status',HARD_CODE_ID_PUBLISHED);
			$this->db->where('employment.is_deleted',HARD_CODE_ID_NO);
			$this->db->order_by('employment.order','ASC');
			
			$result = $this->db->get(); 
			
			if ($result->num_rows() > 0) {

				return $result;

		   } else {

				return false;
		  }
			
	}
	
	function getPayFrequency() {
						
			$this->db->select('pay.id as tableID,pay.name as payFrequencyName,pay.sef_url as payFrequencySefURL,pay.code as payFrequencyCode,pay.description as payFrequencyDescription');
			
			$this->db->from(PAY_FREQUENCY_TABLE.' as pay');
			$this->db->where('pay.publish_status',HARD_CODE_ID_PUBLISHED);
			$this->db->where('pay.is_deleted',HARD_CODE_ID_NO);
			$this->db->order_by('pay.name','ASC');
			
			$result = $this->db->get(); 
			
			if ($result->num_rows() > 0) {

				return $result;

		   } else {

				return false;
		  }
			
	}
	
	function getDiseases() {
						
			$this->db->select('disease.id as tableID,
							   disease.id as diseaseID,
							   disease.name as diseaseName');
			
			$this->db->from(MEDICAL_DISEASES_TABLE.' as disease');
			$this->db->where('disease.publish_status',HARD_CODE_ID_PUBLISHED);
			$this->db->where('disease.is_deleted',HARD_CODE_ID_NO);
			
			$this->db->order_by('disease.order','ASC');
			
			$result = $this->db->get(); 
			
			if ($result->num_rows() > 0) {

				return $result;

		   } else {

				return false;
		  }
			
	}
	
	function getApplicationModuleInfoByID($tableID) {
						
					$this->db->select('module.id as tableID,
									   module.name_singular as moduleNameSingular,
									   module.name_plural as moduleNamePlural');
					
					$this->db->from(APPLICATION_MODULES_TABLE.' as module');
					$this->db->where('module.id',$tableID);
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		    	return false;
		   		  }
			
	}	
	
	function getOrganizationConfiguration($organizationID) {
						
					$this->db->select('config.id as tableID,
									   currency.id as currencyID,
									   currency.name as currencyName,
									   currency.code as currencyCode,
									   currency.symbol as currencySymbol,
									   currency.description as currencyDescription,
									   date.id as dateFormatID,
									   date.name as dateFormatName,
									   date.description as dateFormatDescription,
									   time.id as timeFormatID,
									   time.name as timeFormatName,
									   time.description as timeFormatDescription');
					
					$this->db->from(MY_APPLICATION_CONFIGURATION_TABLE.' as config');
					$this->db->where('config.organization_id',$organizationID);
					$this->db->join(CURRENCY_TABLE.' as currency', 'config.currency = currency.id', 'inner');
					$this->db->join(DATE_FORMATS_TABLE.' as date', 'config.date_format = date.id', 'inner');
					$this->db->join(TIME_FORMATS_TABLE.' as time', 'config.time_format = time.id', 'inner');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}



			function getStates() 
			{	
					$this->db->select('state.id as tableID,
									   state.name as stateName,
									   state.country_id as country_id,
									   country.name as countryname');
					
					$this->db->from(STATES_TABLE.' as state');
					$this->db->join(COUNTRIES_TABLE.' as country', 'country.id = state.country_id', 'left');

					$this->db->where('state.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('state.is_deleted',HARD_CODE_ID_NO);
					$this->db->order_by('state.id','DESC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}



			function getSpecificState($tableID) 
			{	
					$this->db->select('state.id as tableID,
									   state.name as stateName,
									   state.country_id as country_id,
									   country.name as countryname');
					
					$this->db->from(STATES_TABLE.' as state');
					$this->db->join(COUNTRIES_TABLE.' as country', 'country.id = state.country_id', 'left');

					$this->db->where('state.id',$tableID);
					$this->db->where('state.publish_status',HARD_CODE_ID_PUBLISHED);
					$this->db->where('state.is_deleted',HARD_CODE_ID_NO);
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result->row();
		
				   } else {
		
		    		 	return false;
		   		  }
			
			}
	
 }
?>
<?php 
error_reporting(0);
 if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

class Model_Report extends CI_Model {
		
		public function __construct() {
			
			$this->caseStatusReadyArray     	= array(
														CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY,
														CASE_STATUS_KIT_TO_CUSTOMER_IN_TRANSIT,
														CASE_STATUS_IMPRESSIONS_TO_NY_READY,
														CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_READY,
														CASE_STATUS_READY_FOR_UPLOADING_READY,
														CASE_STATUS_WAITING_FOR_APPROVAL_WAITING,
														CASE_STATUS_PRODUCTION_TO_NY_READY,
														CASE_STATUS_ALIGNERS_TO_CUSTOMER_READY
														);
											
			$this->caseStatusTransitArray    	= array(
														CASE_STATUS_IMPRESSIONS_TO_NY_IN_TRANSIT,
														CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT,
														CASE_STATUS_PRODUCTION_TO_NY_IN_TRANSIT,
														CASE_STATUS_ALIGNERS_TO_CUSTOMER_IN_TRANSIT
														);
			
			$this->caseStatusProcessArray    	= array();
														
			$this->caseStatusHoldArray    		= array();
														
			$this->caseStatusOFFHoldArray    	= array();
			
			$this->caseStatusModificationArray  = array(
														CASE_STATUS_WAITING_FOR_APPROVAL_MODIFICATION
														);
			
			$this->caseStatusDoneArray  	 	= array(
														CASE_STATUS_KIT_SHIP_TO_CUSTOMER_SHIPPED,
														CASE_STATUS_KIT_TO_CUSTOMER_RECEIVED,
														CASE_STATUS_IMPRESSIONS_TO_NY_RECEIVED,
														CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_RECEIVED,
														CASE_STATUS_READY_FOR_UPLOADING_DONE,
														CASE_STATUS_WAITING_FOR_APPROVAL_APPROVED,
														CASE_STATUS_PRODUCTION_TO_NY_RECEIVED,
														CASE_STATUS_ALIGNERS_TO_CUSTOMER_RECEIVED
														);
			
			$this->caseStatusRejectArray  		= array();
		
		}
		





	function getPendingCases($doctorID,$searchParameters) 
	{							
		$this->db->select('
						   doctorCase.status_id  AS statusID
						   ');
									  
		$this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
		$this->db->where('doctorCase.is_deleted',HARD_CODE_ID_NO);
		$this->db->where('doctorCase.doctor_id',$doctorID);
		$this->db->where('doctorCase.status_id',PENDING);

		//case received date from to date to search start 
		if ($searchParameters['searchReceivedDate'] && $searchParameters['searchReceivedDateTo'] == '') {
					
					$searchReceivedDate = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
					
					$this->db->where('doctorCase.receive_date',$searchReceivedDate);
			
		} else if ($searchParameters['searchReceivedDate'] == '' && $searchParameters['searchReceivedDateTo']) {
					
					$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
					
					$this->db->where('doctorCase.receive_date <=',$searchReceivedDateTo);
		
		 } else if ($searchParameters['searchReceivedDate'] != '' && $searchParameters['searchReceivedDateTo']!= '') {
					
					$searchReceivedDate   = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
					$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
					
					$this->db->where('doctorCase.receive_date >=',$searchReceivedDate);
					$this->db->where('doctorCase.receive_date <=',$searchReceivedDateTo);
		 }
		 //case received date from to date to search end
					 
		$result = $this->db->get();  
   		return $result;
		
	}


	function getApprovedCases($doctorID,$searchParameters) 
	{							
		$this->db->select('
						   doctorCase.status_id  AS statusID
						   ');
									  
		$this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
		$this->db->where('doctorCase.is_deleted',HARD_CODE_ID_NO);
		$this->db->where('doctorCase.doctor_id',$doctorID);
		$this->db->where('doctorCase.status_id',APPROVED);

		//case received date from to date to search start 
		if ($searchParameters['searchReceivedDate'] && $searchParameters['searchReceivedDateTo'] == '') {
					
					$searchReceivedDate = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
					
					$this->db->where('doctorCase.receive_date',$searchReceivedDate);
			
		} else if ($searchParameters['searchReceivedDate'] == '' && $searchParameters['searchReceivedDateTo']) {
					
					$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
					
					$this->db->where('doctorCase.receive_date <=',$searchReceivedDateTo);
		
		 } else if ($searchParameters['searchReceivedDate'] != '' && $searchParameters['searchReceivedDateTo']!= '') {
					
					$searchReceivedDate   = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
					$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
					
					$this->db->where('doctorCase.receive_date >=',$searchReceivedDate);
					$this->db->where('doctorCase.receive_date <=',$searchReceivedDateTo);
		 }
		 //case received date from to date to search end
		 
		$result = $this->db->get();  
   		return $result;
		
	}


	function getModifiedCases($doctorID,$searchParameters) 
	{							
		$this->db->select('
						   doctorCase.status_id  AS statusID
						   ');
									  
		$this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
		$this->db->where('doctorCase.is_deleted',HARD_CODE_ID_NO);
		$this->db->where('doctorCase.doctor_id',$doctorID);
		$this->db->where('doctorCase.status_id',MODIFIED);

		//case received date from to date to search start 
		if ($searchParameters['searchReceivedDate'] && $searchParameters['searchReceivedDateTo'] == '') {
					
					$searchReceivedDate = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
					
					$this->db->where('doctorCase.receive_date',$searchReceivedDate);
			
		} else if ($searchParameters['searchReceivedDate'] == '' && $searchParameters['searchReceivedDateTo']) {
					
					$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
					
					$this->db->where('doctorCase.receive_date <=',$searchReceivedDateTo);
		
		 } else if ($searchParameters['searchReceivedDate'] != '' && $searchParameters['searchReceivedDateTo']!= '') {
					
					$searchReceivedDate   = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
					$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
					
					$this->db->where('doctorCase.receive_date >=',$searchReceivedDate);
					$this->db->where('doctorCase.receive_date <=',$searchReceivedDateTo);
		 }
		 //case received date from to date to search end
		 
		$result = $this->db->get();  
   		return $result;
		
	}




	function getActiveDoctorsList($organizationID,$searchParameters) {
						
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.email as employeeEmail,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.date_of_joining as employeeJoiningDate,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.employType_ID as employType,
									   HR.id as employeeHRID,
									   HR.name as employeeName,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile,
									   HR.photo as employeePhoto,
									   HR.company as company,
									   employmentStatus.name as employmentStatusName,
									   employeeOfficialRoles.role_id as officialRole_ID,
									   states.name as stateName,
                                       states.id as stateID,
                                       company.id as companyID,
                                       company.name as companyName');
					
												  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->where('organizationEmployee.employType_ID =',HARD_CODE_ID_EMPLOYTYPE_DOCTOR);
					
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					
					if ($searchParameters['doctor_id']) {
								
						$this->db->where('organizationEmployee.id',$searchParameters['doctor_id']);
					}
					
					/*if ($searchParameters['searchDepartment']) {
								
						$this->db->where('employeeJob.department_id',$searchParameters['searchDepartment']);
					}
					
					if ($searchParameters['searchJobTitle']) {
								
						$this->db->where('employeeJob.job_title_id',$searchParameters['searchJobTitle']);
					}
					
					if ($searchParameters['searchPosition']) {
								
						$this->db->where('employeeJob.job_position_id',$searchParameters['searchPosition']);
					}
					
					if ($searchParameters['searchShiftTimings']) {
								
						$this->db->where('employeeShiftTimings.shift_id',$searchParameters['searchShiftTimings']);
						$this->db->where('employeeShiftTimings.active',HARD_CODE_ID_YES);
						$this->db->where('employeeShiftTimings.is_deleted',HARD_CODE_ID_NO);
					}
					
					if ($searchParameters['searchCustom'] && $searchParameters['searchCustomColumn']) {
								
								if ($searchParameters['searchCustomColumn'] == 'employee_number') {
								
										$this->db->like('organizationEmployee.employee_number',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'name') {
								
										$this->db->like('HR.name',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'email') {
								
										$this->db->like('organizationEmployee.email',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'join_date') {
								
										$dateOfJoin = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);  // Calling From Shared Helper
										
										$this->db->where('organizationEmployee.date_of_joining',$dateOfJoin);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'date_of_birth') {
								
										$dateOfBirth = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);
										
										$this->db->where('HR.date_of_birth',$dateOfBirth);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'CNIC_number') {
								
										$CNICNumber  = cleanSringFromDashes($searchParameters['searchCustom']); // Calling From General Helper
										
										$this->db->where('HR.identity_card',$CNICNumber);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'blood_group') {
								
										
										$this->db->where('HR.blood_group',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'personal_email') {
								
										$this->db->where('HR.personal_email',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'mobile') {
								
										$mobile	  =	cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($searchParameters['searchCustom']))); // Calling From General Helper
										
										$this->db->where('HR.mobile',$mobile);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'street_address') {
								
										$this->db->like('HR.street_address',$searchParameters['searchCustom']);
								}
					}
					
					if ($searchParameters['searchBusinessUnit'] || $searchParameters['searchDepartment'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_JOBS_TABLE.' as employeeJob', 'organizationEmployee.id = employeeJob.team_id', 'left');
					}
					
					if ($searchParameters['searchShiftTimings']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_SHIFT_TIME_TABLE.' as employeeShiftTimings', 'organizationEmployee.id = employeeShiftTimings.team_id', 'left');
					}*/
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(EMPLOYMENT_STATUSES_TABLE.' as employmentStatus', 'organizationEmployee.employment_status = employmentStatus.id', 'left');


					$this->db->join(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE.' as employeeOfficialRoles', 'organizationEmployee.id = employeeOfficialRoles.team_id', 'left');

					$this->db->join(STATES_TABLE.' as states', 'HR.state = states.id', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'HR.company = company.id', 'left');

					
					$this->db->order_by('HR.name','ASC');
					
					$this->db->group_by('organizationEmployee.id');
					
					
					return $result = $this->db->get();  
		
					/*if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }*/
		}






		function getActiveDoctors($organizationID) {
						
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.email as employeeEmail,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.date_of_joining as employeeJoiningDate,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.employType_ID as employType,
									   HR.id as employeeHRID,
									   HR.name as employeeName,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile,
									   HR.photo as employeePhoto,
									   HR.company as company
									   ');
					
												  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->where('organizationEmployee.employType_ID =',HARD_CODE_ID_EMPLOYTYPE_DOCTOR);
					
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					
				
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');

					
					$this->db->order_by('HR.name','ASC');
					
					$this->db->group_by('organizationEmployee.id');
					
					
					return $result = $this->db->get();  
		
					/*if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }*/
		}


	




 }
?>
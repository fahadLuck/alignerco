<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

class Model_shared extends CI_Model {


	function insertRecord_SecondDB_ReturnID($tbl_name,$data) {
		$db2 = $this->load->database('aligner_CO_DB', TRUE);
		$db2->insert($tbl_name,$data);
		return $db2->insert_id();
	}

	function updateRecord_SecondDB_ReturnID($tbl_name,$data,$CaseID) {
		$db2 = $this->load->database('aligner_CO_DB', TRUE);
		$db2->where('case_reference_number',$CaseID);
		$db2->update($tbl_name,$data);
		return $db2->insert_id();
	}

	function updateRecordSecondDB_ReturnID($whereArray,$tbl_name,$data) {
		$db2 = $this->load->database('aligner_CO_DB', TRUE);
		$db2->where($whereArray);
		$db2->update($tbl_name,$data);
		return $db2->insert_id();
	}

	function getRecord_SecondDB_MultipleWhere($select,$tbl_name,$whereArray) {
		$db2 = $this->load->database('aligner_CO_DB', TRUE);
		$db2->select($select);
		$db2->from($tbl_name);
		$db2->where($whereArray);
		$result = $db2->get();
		return $result;
	}

	function page_listing_SecondDB_where($page,$recordperpage,$select,$tbl_name,$whereArray,$orderBy_columName,$ASC_DESC) {
		$db2 = $this->load->database('aligner_CO_DB', TRUE);
		$db2->select($select);
		$db2->from($tbl_name);
		$db2->where($whereArray);
		$db2->order_by($orderBy_columName,$ASC_DESC);
		$db2->limit($recordperpage , $page);
		$result=$db2->get();
		return $result;	
	}

	function editRecord_SecondDB_Where($whereArray,$tbl_name,$data) {
		$db2 = $this->load->database('aligner_CO_DB', TRUE);
		$db2->where($whereArray);
		$db2->update($tbl_name,$data);
		
		return $db2->affected_rows();
	}

	function page_listing($page,$recordperpage,$select,$tbl_name,$orderBy_columName,$ASC_DESC) {
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->order_by($orderBy_columName,$ASC_DESC);
		$this->db->limit($recordperpage , $page);
		$result=$this->db->get();
		return $result;	
	}
	
	function page_listing_num($select,$tbl_name) {
	
		$this->db->select($select);
		$this->db->from($tbl_name);
		$query=$this->db->get();
		return $query->num_rows();
	}
	
	function page_listing_where($page,$recordperpage,$select,$tbl_name,$whereArray,$orderBy_columName,$ASC_DESC) {
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$this->db->order_by($orderBy_columName,$ASC_DESC);
		$this->db->limit($recordperpage , $page);
		$result=$this->db->get();
		return $result;	
	}
	
	function page_listing_num_where($select,$tbl_name,$whereArray) {
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$query=$this->db->get();
		return $query->num_rows();
	}
	
	function getRecord($select,$tbl_name,$columName,$where) {
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($columName,$where);
		return $this->db->get();
	}
	
	function getRecordLimit($select,$tbl_name,$columName,$where,$recordperpage,$page) {
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($columName,$where);
		$this->db->limit($recordperpage , $page);
		return $this->db->get();
	}
	
	function getAllRecords($select,$tbl_name) {
		$this->db->select($select);
		$this->db->from($tbl_name);
		return $this->db->get();
	}
	
	function getAllRecordsOrderBY($select,$tbl_name,$orderBy_columName,$ASC_DESC) {
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->order_by($orderBy_columName,$ASC_DESC);
		return $this->db->get();
	}
	
	function getRecordMultipleWhere($select,$tbl_name,$whereArray) {
		
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$result = $this->db->get();
		return $result;
	}
	
	function getRecordMultipleWhereOrderBy($select,$tbl_name,$whereArray,$orderByColumName,$ASC_DESC) {
		
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$this->db->order_by($orderByColumName,$ASC_DESC);
		$result = $this->db->get();
		return $result;
	}
	
	function getRecordMultipleWhereOrderByLimit($select,$tbl_name,$whereArray,$orderBy_columName,$ASC_DESC,$recordPerPage,$page) {
		
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$this->db->order_by($orderBy_columName,$ASC_DESC);
		$this->db->limit($recordPerPage,$page);
		$result = $this->db->get();
		return $result;
	}
	
	function getRecordMultipleWhereLimit($select,$tbl_name,$whereArray,$recordPerPage,$page) {
		
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$this->db->limit($recordPerPage,$page);
		$result = $this->db->get();
		return $result;
	}
	
	function getRecordMultipleWhereIN($select,$tbl_name,$whereArray,$whereINColumn,$whereINArray) {
		
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$this->db->where_in($whereINColumn,$whereINArray);
		$result = $this->db->get();
		return $result;
	}
	
	function getRecordMultipleWhereNotIN($select,$tbl_name,$whereArray,$whereNotINColumn,$whereNotINArray) {
		
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$this->db->where_not_in($whereNotINColumn,$whereNotINArray);
		$result = $this->db->get();
		return $result;
	}
	
	function insertRecord_ReturnID($tbl_name,$data) {
		$this->db->insert($tbl_name,$data);
		return $this->db->insert_id();
	}

	function updateRecordReturnID($whereArray,$tbl_name,$data) {
		$this->db->where($whereArray);
		$this->db->update($tbl_name,$data);
		return $this->db->insert_id();
	}

	function updateRecord_ReturnID($tbl_name,$data,$HRID) {
		$this->db->where('id',$HRID);
		$this->db->update($tbl_name,$data);
		return $this->db->insert_id();
	}
	
	function insertRecord($tbl_name,$data) {
		$this->db->insert($tbl_name,$data);
	}
	
	function editRecord($columName,$where,$tbl_name,$data) {
		$this->db->where($columName,$where);
		$this->db->update($tbl_name,$data);
		return $this->db->affected_rows();
	}
	
	function editRecordWhere($whereArray,$tbl_name,$data) {
		
		$this->db->where($whereArray);
		$this->db->update($tbl_name,$data);
		
		return $this->db->affected_rows();
	}
	
	function deleteRecord($columName,$where,$tbl_name) {
		$this->db->where($columName,$where);
		$this->db->delete($tbl_name);
	}
	
	function deleteRecordWhere($whereArray,$tbl_name) {
		       
			   $this->db->where($whereArray);
	       	   $this->db->delete($tbl_name);
     	
			  return $this->db->affected_rows();
	}
	
	function checkRecordExist_SoftDelete($select,$tbl_name,$columName,$where) {
	
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($columName,$where);
		$this->db->where('is_deleted',HARD_CODE_ID_NO);
		$result = $this->db->get();
		return $result;
	}

	function checkRecordExistMultipleWhere_SoftDelete($select,$tbl_name,$whereArray) {
	
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($whereArray);
		$this->db->where('is_deleted',HARD_CODE_ID_NO);
		$result = $this->db->get();
		return $result;
	}
		
	function checkRecordEdit_SoftDelete($columName,$tbl_name,$whereColumn,$name_txt,$old_name) {
	
			return $this->db->query('select '.$columName.' from '.$tbl_name.' where is_deleted = '.HARD_CODE_ID_NO.' and '.$whereColumn.' != "'.$old_name.'" and '.$whereColumn.'= "'.$name_txt.'"');
	}
	
	function check_record_exist($select,$tbl_name,$columName,$where) {
	
		$this->db->select($select);
		$this->db->from($tbl_name);
		$this->db->where($columName,$where);
		$result = $this->db->get();
		return $result;
	}
	
	function check_record_edit($columName,$tbl_name,$whereColumn,$name_txt,$old_name) {
	
			return $this->db->query('select '.$columName.' from '.$tbl_name.' where '.$whereColumn.' != "'.$old_name.'" and '.$whereColumn.'= "'.$name_txt.'"');
	}
	
	function generalQuery($sql) {
		
		$result =  $this->db->query($sql);
		  
		  if ($result->num_rows() > 0) {
			   return $result;
		   } else {
		     	return 0;
		   }
		
    }
	
	function getMaximum($selectMAX,$tbl_name,$columName,$where) {
		$this->db->select_max($selectMAX);
		$this->db->where($columName,$where);
		$query = $this->db->get($tbl_name);
		return $query;
	}
	
	function getMinimum($selectMAX,$tbl_name,$columName,$where) {
		$this->db->select_min($selectMAX);
		$this->db->where($columName,$where);
		$query = $this->db->get($tbl_name);
		return $query;
	}

	function check_duplicate($tableName,$where) {
		
		$this->db->select('id');
		$this->db->from($tableName);	
		$this->db->where($where);
		$result = $this->db->get();
		/*print_r($this->db->last_query()); */
		return $result;
	}
	
 }
?>
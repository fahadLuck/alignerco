<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

class Model_employee extends CI_Model {
	
		function getEmployeeProfile($organizationID,$employeeID) {
				
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   organizationEmployee.date_of_joining as dateOfJoining,
									   organizationEmployee.email as officialEmail,
									   HR.id as employeeHRID,prefix.id as prefixID,
									   prefix.name as prefixName,
									   HR.name as employeeName,
									   HR.date_of_birth as dateOfBirth,
									   HR.identity_card_formatted as employeeCNICFormatted,
									   HR.identity_card as employeeCNIC,
									   HR.personal_email as employeePersonalEmail,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile,
									   HR.street_address as streetAddress,
									   gender.name as genderName,
									   blodGroup.name as blodGroupName,
									   nationality.name as nationalityName,
									   religion.name as religionName,
									   maritalStatus.name as maritalStatusName,');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.id',$employeeID);
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					$this->db->join(GENDER_TABLE.' as gender', 'HR.gender = gender.id', 'left');
					$this->db->join(NATIONALITIES_TABLE.' as nationality', 'HR.nationality = nationality.id', 'left');
					$this->db->join(RELIGIONS_TABLE.' as religion', 'HR.religion = religion.id', 'left');
					$this->db->join(BLOOD_GROUPS_TABLE.' as blodGroup', 'HR.blood_group = blodGroup.id', 'left');
					$this->db->join(MARITAL_STATUS_TABLE.' as maritalStatus', 'HR.marital_status = maritalStatus.id', 'left');
					
					$result = $this->db->get(); 
		
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
		}
		
		function getEmployeeAssignJobs($employeeID) {
						
					$this->db->select('organizationEmployeeJob.id as tableID,
									   businessUnit.id as businessUnitID,
									   businessUnit.name as businessUnitName,
									   businessUnit.code as businessUnitCode,
									   department.id as departmentID,
									   department.name as departmentName,
									   jobTitle.id as jobTitleID,
									   jobTitle.name as jobTitleName,
									   jobPosition.id as jobPositionID,
									   jobPosition.name as jobPositionName,
									   organizationEmployeeJob.date as employeeJobDate,
									   organizationEmployeeJob.created as employeeJobCreated');
					
					$this->db->from(MY_ORGANIZATION_TEAM_JOBS_TABLE.' as organizationEmployeeJob');
					$this->db->where('organizationEmployeeJob.team_id',$employeeID);
					$this->db->join(MY_ORGANIZATION_BUSINESS_UNITS_TABLE.' as businessUnit', 'organizationEmployeeJob.business_unit_id = businessUnit.id', 'left');
					$this->db->join(MY_ORGANIZATION_DEPARTMENTS_TABLE.' as department', 'organizationEmployeeJob.department_id = department.id', 'left');
					$this->db->join(JOB_TITLES_TABLE.' as jobTitle', 'organizationEmployeeJob.job_title_id = jobTitle.id', 'left');
					$this->db->join(JOB_POSITIONS_TABLE.' as jobPosition', 'organizationEmployeeJob.job_position_id = jobPosition.id', 'left');
					
					$this->db->order_by('organizationEmployeeJob.date', 'DESC');
					
					$result = $this->db->get(); 
		  	
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
		
	}
	
		function getEmployeeInfoByID($organizationID,$employeeID) {
						
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.HR_id  as employeeHRID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.email as employeeEmail,
									   HR.mobile as employeeMobile');
											  
					
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.id',$employeeID);
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					
					$result = $this->db->get(); 
		
		  			if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   	} else {
		
		    		 	return false;
		   		  	}
		}
	
		function getActiveEmployees($page,$recordPerPage,$organizationID,$searchParameters) {
						
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.email as employeeEmail,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.date_of_joining as employeeJoiningDate,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.employType_ID as employType,
									   HR.id as employeeHRID,
									   HR.name as employeeName,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile,
									   HR.photo as employeePhoto,
									   employmentStatus.name as employmentStatusName,
									   employeeOfficialRoles.role_id as officialRole_ID');
					
												  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->where('organizationEmployee.employType_ID =',HARD_CODE_ID_EMPLOYTYPE_EMPLOY);
					
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					
					if ($searchParameters['searchBusinessUnit']) {
								
						$this->db->where('employeeJob.business_unit_id',$searchParameters['searchBusinessUnit']);
					}
					
					if ($searchParameters['searchDepartment']) {
								
						$this->db->where('employeeJob.department_id',$searchParameters['searchDepartment']);
					}
					
					if ($searchParameters['searchJobTitle']) {
								
						$this->db->where('employeeJob.job_title_id',$searchParameters['searchJobTitle']);
					}
					
					if ($searchParameters['searchPosition']) {
								
						$this->db->where('employeeJob.job_position_id',$searchParameters['searchPosition']);
					}
					
					if ($searchParameters['searchShiftTimings']) {
								
						$this->db->where('employeeShiftTimings.shift_id',$searchParameters['searchShiftTimings']);
						$this->db->where('employeeShiftTimings.active',HARD_CODE_ID_YES);
						$this->db->where('employeeShiftTimings.is_deleted',HARD_CODE_ID_NO);
					}
					
					if ($searchParameters['searchCustom'] && $searchParameters['searchCustomColumn']) {
								
								if ($searchParameters['searchCustomColumn'] == 'employee_number') {
								
										$this->db->like('organizationEmployee.employee_number',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'name') {
								
										$this->db->like('HR.name',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'email') {
								
										$this->db->like('organizationEmployee.email',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'join_date') {
								
										$dateOfJoin = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);  // Calling From Shared Helper
										
										$this->db->where('organizationEmployee.date_of_joining',$dateOfJoin);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'date_of_birth') {
								
										$dateOfBirth = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);
										
										$this->db->where('HR.date_of_birth',$dateOfBirth);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'CNIC_number') {
								
										$CNICNumber  = cleanSringFromDashes($searchParameters['searchCustom']); // Calling From General Helper
										
										$this->db->where('HR.identity_card',$CNICNumber);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'blood_group') {
								
										
										$this->db->where('HR.blood_group',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'personal_email') {
								
										$this->db->where('HR.personal_email',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'mobile') {
								
										$mobile	  =	cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($searchParameters['searchCustom']))); // Calling From General Helper
										
										$this->db->where('HR.mobile',$mobile);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'street_address') {
								
										$this->db->like('HR.street_address',$searchParameters['searchCustom']);
								}
					}
					
					if ($searchParameters['searchBusinessUnit'] || $searchParameters['searchDepartment'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_JOBS_TABLE.' as employeeJob', 'organizationEmployee.id = employeeJob.team_id', 'left');
					}
					
					if ($searchParameters['searchShiftTimings']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_SHIFT_TIME_TABLE.' as employeeShiftTimings', 'organizationEmployee.id = employeeShiftTimings.team_id', 'left');
					}
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(EMPLOYMENT_STATUSES_TABLE.' as employmentStatus', 'organizationEmployee.employment_status = employmentStatus.id', 'left');


					$this->db->join(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE.' as employeeOfficialRoles', 'organizationEmployee.id = employeeOfficialRoles.team_id', 'left');

					
					$this->db->order_by('HR.name','ASC');
					
					$this->db->group_by('organizationEmployee.id');
					
					$this->db->limit($recordPerPage,$page);
					
					$result = $this->db->get();  
		
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
		}




		function getActiveDoctors($page,$recordPerPage,$organizationID,$searchParameters) {
						
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.email as employeeEmail,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.date_of_joining as employeeJoiningDate,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.employType_ID as employType,
									   HR.id as employeeHRID,
									   HR.name as employeeName,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile,
									   HR.photo as employeePhoto,
									   HR.company as company,
									   employmentStatus.name as employmentStatusName,
									   employeeOfficialRoles.role_id as officialRole_ID,
									   states.name as stateName,
                                       states.id as stateID,
                                       company.id as companyID,
                                       company.name as companyName');
					
												  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->where('organizationEmployee.employType_ID =',HARD_CODE_ID_EMPLOYTYPE_DOCTOR);
					
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					
					if ($searchParameters['searchBusinessUnit']) {
								
						$this->db->where('employeeJob.business_unit_id',$searchParameters['searchBusinessUnit']);
					}
					
					if ($searchParameters['searchDepartment']) {
								
						$this->db->where('employeeJob.department_id',$searchParameters['searchDepartment']);
					}
					
					if ($searchParameters['searchJobTitle']) {
								
						$this->db->where('employeeJob.job_title_id',$searchParameters['searchJobTitle']);
					}
					
					if ($searchParameters['searchPosition']) {
								
						$this->db->where('employeeJob.job_position_id',$searchParameters['searchPosition']);
					}
					
					if ($searchParameters['searchShiftTimings']) {
								
						$this->db->where('employeeShiftTimings.shift_id',$searchParameters['searchShiftTimings']);
						$this->db->where('employeeShiftTimings.active',HARD_CODE_ID_YES);
						$this->db->where('employeeShiftTimings.is_deleted',HARD_CODE_ID_NO);
					}
					
					if ($searchParameters['searchCustom'] && $searchParameters['searchCustomColumn']) {
								
								if ($searchParameters['searchCustomColumn'] == 'employee_number') {
								
										$this->db->like('organizationEmployee.employee_number',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'name') {
								
										$this->db->like('HR.name',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'email') {
								
										$this->db->like('organizationEmployee.email',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'join_date') {
								
										$dateOfJoin = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);  // Calling From Shared Helper
										
										$this->db->where('organizationEmployee.date_of_joining',$dateOfJoin);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'date_of_birth') {
								
										$dateOfBirth = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);
										
										$this->db->where('HR.date_of_birth',$dateOfBirth);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'CNIC_number') {
								
										$CNICNumber  = cleanSringFromDashes($searchParameters['searchCustom']); // Calling From General Helper
										
										$this->db->where('HR.identity_card',$CNICNumber);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'blood_group') {
								
										
										$this->db->where('HR.blood_group',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'personal_email') {
								
										$this->db->where('HR.personal_email',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'mobile') {
								
										$mobile	  =	cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($searchParameters['searchCustom']))); // Calling From General Helper
										
										$this->db->where('HR.mobile',$mobile);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'street_address') {
								
										$this->db->like('HR.street_address',$searchParameters['searchCustom']);
								}
					}
					
					if ($searchParameters['searchBusinessUnit'] || $searchParameters['searchDepartment'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_JOBS_TABLE.' as employeeJob', 'organizationEmployee.id = employeeJob.team_id', 'left');
					}
					
					if ($searchParameters['searchShiftTimings']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_SHIFT_TIME_TABLE.' as employeeShiftTimings', 'organizationEmployee.id = employeeShiftTimings.team_id', 'left');
					}
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(EMPLOYMENT_STATUSES_TABLE.' as employmentStatus', 'organizationEmployee.employment_status = employmentStatus.id', 'left');


					$this->db->join(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE.' as employeeOfficialRoles', 'organizationEmployee.id = employeeOfficialRoles.team_id', 'left');

					$this->db->join(STATES_TABLE.' as states', 'HR.state = states.id', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'HR.company = company.id', 'left');

					
					$this->db->order_by('HR.name','ASC');
					
					$this->db->group_by('organizationEmployee.id');
					
					$this->db->limit($recordPerPage,$page);
					
					return $result = $this->db->get();  
		
					/*if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }*/
		}





		function getActiveDoctorsAssignedStateWise($page,$recordPerPage,$organizationID,$searchParameters,$patient_state) {
						
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.email as employeeEmail,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.date_of_joining as employeeJoiningDate,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.employType_ID as employType,
									   HR.id as employeeHRID,
									   HR.name as employeeName,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile,
									   HR.photo as employeePhoto,
									   HR.company as company,
									   employmentStatus.name as employmentStatusName,
									   employeeOfficialRoles.role_id as officialRole_ID,
									   states.name as stateName,
                                       states.id as stateID,
                                       company.id as companyID,
                                       company.name as companyName');
					
												  
					/*$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');*/
					$this->db->from(ASSIGNED_STATES_TABLE.' as assignedStates');
					
					/*$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->where('organizationEmployee.employType_ID =',HARD_CODE_ID_EMPLOYTYPE_DOCTOR);*/

					$this->db->where('assignedStates.state_id',$patient_state);	
					$this->db->where('assignedStates.is_deleted',HARD_CODE_ID_NO);
					
					if ($searchParameters['searchBusinessUnit']) {
								
						$this->db->where('employeeJob.business_unit_id',$searchParameters['searchBusinessUnit']);
					}
					
					if ($searchParameters['searchDepartment']) {
								
						$this->db->where('employeeJob.department_id',$searchParameters['searchDepartment']);
					}
					
					if ($searchParameters['searchJobTitle']) {
								
						$this->db->where('employeeJob.job_title_id',$searchParameters['searchJobTitle']);
					}
					
					if ($searchParameters['searchPosition']) {
								
						$this->db->where('employeeJob.job_position_id',$searchParameters['searchPosition']);
					}
					
					if ($searchParameters['searchShiftTimings']) {
								
						$this->db->where('employeeShiftTimings.shift_id',$searchParameters['searchShiftTimings']);
						$this->db->where('employeeShiftTimings.active',HARD_CODE_ID_YES);
						$this->db->where('employeeShiftTimings.is_deleted',HARD_CODE_ID_NO);
					}
					
					if ($searchParameters['searchCustom'] && $searchParameters['searchCustomColumn']) {
								
								if ($searchParameters['searchCustomColumn'] == 'employee_number') {
								
										$this->db->like('organizationEmployee.employee_number',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'name') {
								
										$this->db->like('HR.name',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'email') {
								
										$this->db->like('organizationEmployee.email',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'join_date') {
								
										$dateOfJoin = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);  // Calling From Shared Helper
										
										$this->db->where('organizationEmployee.date_of_joining',$dateOfJoin);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'date_of_birth') {
								
										$dateOfBirth = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);
										
										$this->db->where('HR.date_of_birth',$dateOfBirth);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'CNIC_number') {
								
										$CNICNumber  = cleanSringFromDashes($searchParameters['searchCustom']); // Calling From General Helper
										
										$this->db->where('HR.identity_card',$CNICNumber);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'blood_group') {
								
										
										$this->db->where('HR.blood_group',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'personal_email') {
								
										$this->db->where('HR.personal_email',$searchParameters['searchCustom']);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'mobile') {
								
										$mobile	  =	cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($searchParameters['searchCustom']))); // Calling From General Helper
										
										$this->db->where('HR.mobile',$mobile);
								}
								
								if ($searchParameters['searchCustomColumn'] == 'street_address') {
								
										$this->db->like('HR.street_address',$searchParameters['searchCustom']);
								}
					}
					
					if ($searchParameters['searchBusinessUnit'] || $searchParameters['searchDepartment'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_JOBS_TABLE.' as employeeJob', 'organizationEmployee.id = employeeJob.team_id', 'left');
					}
					
					if ($searchParameters['searchShiftTimings']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_SHIFT_TIME_TABLE.' as employeeShiftTimings', 'organizationEmployee.id = employeeShiftTimings.team_id', 'left');
					}

					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'assignedStates.team_id = organizationEmployee.id', 'left');
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(EMPLOYMENT_STATUSES_TABLE.' as employmentStatus', 'organizationEmployee.employment_status = employmentStatus.id', 'left');


					$this->db->join(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE.' as employeeOfficialRoles', 'organizationEmployee.id = employeeOfficialRoles.team_id', 'left');

					$this->db->join(STATES_TABLE.' as states', 'HR.state = states.id', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'HR.company = company.id', 'left');

					
					$this->db->order_by('HR.name','ASC');
					
					$this->db->group_by('organizationEmployee.id');
					
					$this->db->limit($recordPerPage,$page);
					
					return $result = $this->db->get();  
		
					/*if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }*/
		}




	
		function getActiveEmployeesNum($organizationID,$searchParameters) {
						
					$this->db->select('organizationEmployee.id as employeeID');

					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					
					if ($searchParameters['searchBusinessUnit']) {
								
						$this->db->where('employeeJob.business_unit_id',$searchParameters['searchBusinessUnit']);
					}
					
					if ($searchParameters['searchDepartment']) {
								
								$this->db->where('employeeJob.department_id',$searchParameters['searchDepartment']);
					}
					
					if ($searchParameters['searchJobTitle']) {
								
						$this->db->where('employeeJob.job_title_id',$searchParameters['searchJobTitle']);
					}
					
					if ($searchParameters['searchPosition']) {
								
						$this->db->where('employeeJob.job_position_id',$searchParameters['searchPosition']);
					}
					
					if ($searchParameters['searchShiftTimings']) {
								
						$this->db->where('employeeShiftTimings.shift_id',$searchParameters['searchShiftTimings']);
						$this->db->where('employeeShiftTimings.active',HARD_CODE_ID_YES);
						$this->db->where('employeeShiftTimings.is_deleted',HARD_CODE_ID_NO);
					}
					
					if ($searchParameters['searchCustom'] && $searchParameters['searchCustomColumn']) {
								
						  if ($searchParameters['searchCustomColumn'] == 'employee_number') {
						  
								  $this->db->like('organizationEmployee.employee_number',$searchParameters['searchCustom']);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'name') {
						  
								  $this->db->like('HR.name',$searchParameters['searchCustom']);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'email') {
						  
								  $this->db->like('organizationEmployee.email',$searchParameters['searchCustom']);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'join_date') {
						  
								  $dateOfJoin = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);  // Calling From Shared Helper
								  
								  $this->db->where('organizationEmployee.date_of_joining',$dateOfJoin);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'date_of_birth') {
						  
								  $dateOfBirth = DATABASE_DATE_FORMAT($searchParameters['searchCustom']);
								  
								  $this->db->where('HR.date_of_birth',$dateOfBirth);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'CNIC_number') {
						  
								  $CNICNumber  = cleanSringFromDashes($searchParameters['searchCustom']); // Calling From General Helper
								  
								  $this->db->where('HR.identity_card',$CNICNumber);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'blood_group') {
						  
								  
								  $this->db->where('HR.blood_group',$searchParameters['searchCustom']);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'personal_email') {
						  
								  $this->db->where('HR.personal_email',$searchParameters['searchCustom']);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'mobile') {
						  
								  $mobile	  =	cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($searchParameters['searchCustom']))); // Calling From General Helper
								  
								  $this->db->where('HR.mobile',$mobile);
						  }
						  
						  if ($searchParameters['searchCustomColumn'] == 'street_address') {
						  
								  $this->db->like('HR.street_address',$searchParameters['searchCustom']);
						  }
					}
					
					if ($searchParameters['searchBusinessUnit'] || $searchParameters['searchDepartment'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition'] || $searchParameters['searchJobTitle'] || $searchParameters['searchPosition']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_JOBS_TABLE.' as employeeJob', 'organizationEmployee.id = employeeJob.team_id', 'left');
					}
					
					if ($searchParameters['searchShiftTimings']) {
						
						$this->db->join(MY_ORGANIZATION_TEAM_SHIFT_TIME_TABLE.' as employeeShiftTimings', 'organizationEmployee.id = employeeShiftTimings.team_id', 'left');
					}
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(EMPLOYMENT_STATUSES_TABLE.' as employmentStatus', 'organizationEmployee.employment_status = employmentStatus.id', 'left');
					
					$this->db->group_by('organizationEmployee.id');
					
					$result = $this->db->get(); 
		
					return $result->num_rows();
		
	}




	function getSingalActiveEmployees($organizationID,$tableID) {
						
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.HR_id as HRID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.email as employeeEmail,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   organizationEmployee.date_of_joining as employeeJoiningDate,
									   organizationEmployee.date_of_rejoining as employeeReJoiningDate,
									   HR.id as employeeHRID,
									   HR.name as employeeName,
									   HR.father_name as fatherName,
									   HR.personal_email as personalEmail,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile,
									   HR.photo as employeePhoto,
									   HR.country as country,
									   gender.name as genderName,
									   employmentStatus.name as employmentStatusName,
									   states.name as stateName,
									   states.id as stateID,
									   company.id as companyID,
                                       company.name as companyNamem,
									   assignedStates.state_id as assignedStateID');
												  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->where('organizationEmployee.id =',$tableID);
					
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(EMPLOYMENT_STATUSES_TABLE.' as employmentStatus', 'organizationEmployee.employment_status = employmentStatus.id', 'left');

					$this->db->join(GENDER_TABLE.' as gender', 'HR.gender = gender.id', 'left');

					$this->db->join(ASSIGNED_STATES_TABLE.' as assignedStates', 'HR.id = assignedStates.hr_id', 'left');

					$this->db->join(STATES_TABLE.' as states', 'assignedStates.state_id = states.id', 'left');

					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'HR.company = company.id', 'left');
					
					$this->db->order_by('HR.name','ASC');
					
					$this->db->group_by('organizationEmployee.id');
					
					/*$this->db->limit($recordPerPage,$page);*/
					
					$result = $this->db->get();  
		
					if ($result->num_rows() > 0) {
		
		       			return $result->row();
		
				   } else {
		
		    		 	return false;
		   		  }
		}



		function getDoctorAssignedStates($doctorID) {
						
					$this->db->select('assignedState.id as tableID,
									   assignedState.licience as licience,
									   assignedState.state_id as assignedStateId,
									   state.name as stateName,
									   state.id as stateID');
											  
					
					$this->db->from(ASSIGNED_STATES_TABLE.' as assignedState');
					$this->db->where('assignedState.team_id',$doctorID);
					$this->db->where('assignedState.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(STATES_TABLE.' as state', 'assignedState.state_id = state.id', 'left');
					
					$result = $this->db->get(); 
		
		  			if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   	} else {
		
		    		 	return false;
		   		  	}
		}




	
	
 }
 
?>
<div class="page-wrapper">
      <div class="content container-fluid">
      
          <!-- Page Header -->
          <div class="page-header">
              <div class="row">
                  <div class="col">
                      <h3 class="page-title"><?php echo $pageHeading; ?></h3>
                      <ul class="breadcrumb">
                          <li class="breadcrumb-item">Settings</li>
                          <li class="breadcrumb-item"><a href="<?php echo base_url();?>roles/">Roles</a></li>
                          <li class="breadcrumb-item active">New Role</li>
                      </ul>
                  </div>
              </div>
          </div>
          <!-- /Page Header -->
          
          <?php if ($this->session->userdata('admin_msg') !='')  { ?>
     
                  <div class="alert alert-secondary alert-dismissible fade show" role="alert">
                      <strong>Alert!</strong> <?php echo $this->session->userdata('admin_msg');?>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                  </div>
  
       <?php $this->session->unset_userdata('admin_msg'); } ?>
          
          <div class="row">
              <div class="col-xl-6">
                  <div class="card flex-fill">
                      <div class="card-header">
                          <h4 class="card-title mb-0">New Role</h4>
                      </div>
                      <div class="card-body">
                           <form id="" action="<?php echo base_url(); ?>role-add/" method="post" autocomplete="off">
                              <div class="form-group row">
                                  <label class="col-lg-3 col-form-label">Name</label>
                                  <div class="col-lg-9">
                                      <input id="name" name="name" type="text" class="form-control <?php if (form_error('name')) { echo 'is-invalid'; } ?>" value="<?php echo $this->input->post('name'); ?>">
                                      <?php if (form_error('name')) { echo form_error('name'); } ?> 
                                  </div>
                              </div>
                              
                              <div class="form-group row">
                                  <label class="col-lg-3 col-form-label">Description</label>
                                  <div class="col-lg-9">
                                      <textarea id="description" name="description" rows="4" cols="5" class="form-control" placeholder=""><?php echo $this->input->post('description'); ?></textarea>
                                      <?php if (form_error('description')) { echo form_error('description'); } ?> 
                                  </div>
                              </div>
                              
                              <div class="text-right">
                                  <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      
      </div>			
</div>                
<style>

.user-panel>.image>img {
    width: 100%;
    max-width: 60%;
    height: auto;
    margin: 0 auto;
    display: block;
    border: 5px solid rgba(56, 154, 240, .27);
}
.img-circle {
    border-radius: 50%;
}
</style>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  
    <?php if($accessModules) { ?>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
      <?php 
	  		
			$userPhoto 		= $this->session->userdata(USER_PHOTO_SESSION);
		
			if ($userPhoto) {
		
	  ?>
      
      <div class="user-panel">
        <div class="image">
          <img src="<?php echo HR_OFFICIALS_USERS_URL_PATH; ?><?php echo $userPhoto; ?>" class="img-circle" alt="">
        </div>
        <div class="info">
          <p><?php echo $userInfo['name']; ?></p>
          <a href="JavaScript:Void(0);">
		  		<?php 
					
					if ($employeeAssignJobPosition) {
							
								echo $employeeAssignJobPosition;
					
					} else {
						
								echo NOT_AVAILABLE;	
					}
				  ?>
                </a>
        </div>
      </div>
      
      <?php } ?>
  
      <?php if (in_array(MODULE_DASHBOARD,$accessModules)) { ?>
       <form action="<?php echo base_url();?>case/" method="post" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="case_id" class="form-control" placeholder="case ID" value="<?php echo $this->input->post('case_id'); ?>" >
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <?php } ?>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
       
       <?php if (in_array(MODULE_DASHBOARD,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '1') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>my-dashboard/">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">
           
            <?php if (in_array(MODULE_DASHBOARD_HOME,$accessModules)) { ?>
            	
                <li class="<?php if($activeSubMenu == '1.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
            	
			<?php } ?>
               
          </ul>
      
        </li>
        
       <?php } ?>
       
       <?php if (in_array(MODULE_SETUPS,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '5') { echo 'active'; } ?> treeview">
          
           <a href="javascript:void(0);">
                <i class="fa fa-th"></i> <span>Setups</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
             
          
          <ul class="treeview-menu">
           
           <?php if (in_array(MODULE_SETUP_PRODUCTION_ROOMS,$accessModules)) { ?>
            	
               <li class="<?php if($activeSubMenu == '5.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>manage-stages/"><i class="fa fa-delicious"></i> Stages</a></li>
            
			<?php } ?>
            
          </ul>
      
        </li>
        
       <?php } ?>
       
	   <?php if (in_array(MODULE_CASES,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '4') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>cases/">
            <i class="fa fa-medkit"></i> <span>Cases</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">

            <?php if (in_array(MODULE_CASE_ADD_NEW,$accessModules)) { ?>	
            <li class="<?php if($activeSubMenu == '4.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>case-add/"><i class="fa fa-plus"></i> New Case</a></li>
             <?php } ?>
            
            <li class="<?php if($activeSubMenu == '4.2') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>cases-list/"><i class="fa fa-medkit"></i> Cases</a></li>

           <?php if (in_array(MODULE_CASE_ADD_NEW,$accessModules)) { ?>
            <li class="<?php if($activeSubMenu == '4.3') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>cases-missing/"><i class="fa fa-medkit"></i>Missing Cases</a></li>
            <?php } ?>
			    
          </ul>
      
        </li>
        
       <?php } ?>
       
	   <?php if (in_array(MODULE_USERS,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '3') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>employees/">
            <i class="fa fa-users"></i> <span>Employees</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">
            	
            <li class="<?php if($activeSubMenu == '3.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>employees/"><i class="fa fa-users"></i> All Employees</a></li>
            
			    
          </ul>
      
        </li>
        
       <?php } ?>






        <?php if (in_array(MODULE_USERS,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '6') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>doctors/">
            <i class="fa fa-user-md"></i> <span>Dentist</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">
              
            <li class="<?php if($activeSubMenu == '6.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>doctors/"><i class="fa fa-user-md"></i> All Dentist</a></li>
            
          
          </ul>
      
        </li>
        
       <?php } ?>


       
       <?php if (in_array(MODULE_USERS,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '2') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>users/">
            <i class="fa fa-user-circle"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">
            	
            <li class="<?php if($activeSubMenu == '2.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>users/"><i class="fa fa-user-circle"></i> All Users</a></li>
            
			    
          </ul>
      
        </li>
        
       <?php } ?>




        <?php if (in_array(MANAGE_STATES,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '7') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>states/">
            <i class="fa fa-flag"></i> <span>States</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">
              
            <li class="<?php if($activeSubMenu == '7.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>states/"><i class="fa fa-flag"></i> All States</a></li>
            
          
          </ul>
      
        </li>
        
       <?php } ?>


       <?php if (in_array(PATIENT_LIST,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '8') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>patient-list/">
            <i class="fa fa-wheelchair"></i> <span>Patients</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">
              
            <li class="<?php if($activeSubMenu == '8.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>patient-list/"><i class="fa fa-wheelchair"></i> All Patients</a></li>
            
          
          </ul>
      
        </li>
        
       <?php } ?>


       <?php if (in_array(MODULE_REPORTS,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '9') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>patient-list/">
            <i class="fa fa-file"></i> <span>Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">
              
            <?php if (in_array(DOCTOR_REPORTS,$accessModules)) { ?>
              <li class="<?php if($activeSubMenu == '9.1') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>doctor-report/"><i class="fa fa-file"></i>Doctor Report</a></li>
            <?php } ?>
          
          </ul>
      
        </li>
        
       <?php } ?>


        <?php if (in_array(PATIENT_PICTURES,$accessModules)) { ?>  
      
        <li class="<?php if($activeMenu == '8') { echo 'active'; } ?> treeview">
          
          <a href="<?php echo base_url(); ?>patient-list/">
            <i class="fa fa-image"></i> <span>Pictures</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
          <ul class="treeview-menu">
              
            <li class="<?php if($activeSubMenu == '8.3') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>patient-images/"><i class="fa fa-image"></i> Picture History</a></li>
            
          
          </ul>
      
        </li>
        
       <?php } ?>


     
     </ul>
       
   </li> 
       
        
       <br />
       <br />
       <br />
       <br />
       <br />
       
      
      </ul>
   
    </section>
    <!-- /.sidebar -->
    <?php } ?>
    
    <div class="sidebar-footer">
		
		<a href="<?php echo base_url(); ?>logout/" class="link" data-toggle="tooltip" title="Logout" data-original-title="Logout"><i class="fa fa-power-off"></i></a>
	</div>
  </aside>
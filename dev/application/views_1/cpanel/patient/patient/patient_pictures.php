<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>backend_css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>backend_js/dropzone.js"></script>

<script>

function removePhotos() {
	
	if (confirm('Are sure want to remove case pictures?')) {
    	
		var URL = '<?php echo base_url(); ?>remove-case-pictures/<?php echo encodeString($ID); ?>';
		
		window.location.href = URL;
		
	} else {
		
		return false;
	}
	
}

</script>


<script>
  $(document).ready(function() {

     $('#picture_of').change(function(){

         picture_ofID=$(this).val();
         if (picture_ofID == <?= PICTURE_TYPE_ID_STEPS ?>)
         {
          $('#steps').show(1000);
         }

         if (picture_ofID != <?= PICTURE_TYPE_ID_STEPS ?>)
         {
          $('#steps').hide(1000);
         }

      });

  });
</script>

<style type="text/css">
  .files input {
    outline: 2px dashed #92b0b3;
    outline-offset: -10px;
    -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
    transition: outline-offset .15s ease-in-out, background-color .15s linear;
    padding: 120px 0px 85px 15%;
    text-align: center !important;
    margin: 0;
    width: 100% !important;
}
.files input:focus{     outline: 2px dashed #92b0b3;  outline-offset: -10px;
    -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
    transition: outline-offset .15s ease-in-out, background-color .15s linear; border:1px solid #92b0b3;
 }
.files{ position:relative}
.files:after {  pointer-events: none;
    position: absolute;
    top: 60px;
    left: 0;
    width: 50px;
    right: 0;
    height: 56px;
    content: "";
    background-image: url(https://image.flaticon.com/icons/png/128/109/109612.png);
    display: block;
    margin: 0 auto;
    background-size: 100%;
    background-repeat: no-repeat;
}
.color input{ background-color:#f1f1f1;}
.files:before {
    position: absolute;
    bottom: 10px;
    left: 0;  pointer-events: none;
    width: 100%;
    right: 0;
    height: 57px;
    content: " or drag it here. ";
    display: block;
    margin: 0 auto;
    color: #2ea591;
    font-weight: 600;
    text-transform: capitalize;
    text-align: center;
}
</style>

<!-- fancybox -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend_assets/vendor_components/lightbox-master/dist/ekko-lightbox.css" />

	<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Manage Pictures</a></li>
    </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
     	
        				<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
    <?php 
			 	if ($ID) {
						
						$ID = encodeString($ID);	
				}
	 ?>
     
     <!-- <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-cloud-upload"></i> Upload Intra &amp; Extra Oral Pictures</h3>
          	<div class="pull-right box-tools">
              <a href="<?php echo base_url(); ?>patient-images"><i class="fa fa-refresh"></i></a>
            </div>
         </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">     
             <form action="<?php echo base_url(); ?>upload-patient-pictures/<?php echo encodeString($ID); ?>/" class="dropzone">
               
             </form>

             <input type="file" name="file[]" id="file" multiple />

	        </div>
          </div>
        </div>
 	  </div> -->



    <div class="box box-default" id="waiting-for-approval-frm-area">
        <div class="box-header with-border">
          <h3 class="box-title">Upload Images</h3>

         <!--  <a class="pull-right box-tools" href="<?php echo base_url(); ?>doctor-report"><i style="font-weight: bold;" class="fa fa-refresh"></i></a> -->
        </div>

       

        <!-- /.box-header -->
      
      <form id="" name="frm" action="<?php echo base_url(); ?>upload-patient-pictures/<?php echo encodeString($ID); ?>/" method="post" enctype="multipart/form-data" autocomplete="off">
        <div id='waiting_checkboxes'></div>
        <div class="box-body">

          <div class="row">


            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>Pictures Are Of</label>
                    <select name="picture_of" id="picture_of" class="form-control" required="">
                      <option value="">Select</option>
                      <?php
                      foreach($pictureType->result() as $pt)
                      { 
                      ?>
                      <option value="<?= $pt->id; ?>"><?= $pt->name; ?></option>
                      <?php
                      }
                      ?>
                    </select>
              </div>
            </div>


            <div class="col-md-2 col-12" id="steps" style="display: none;">
              <div class="form-group">
                <label>Steps</label>
                    <select name="steps" class="form-control" >
                      <option value="">Select</option>
                      <?php
                      foreach($Steps->result() as $step)
                      { 
                      ?>
                      <option value="<?= $step->name; ?>"><?= $step->name; ?></option>
                      <?php
                      }
                      ?>
                    </select>
              </div>
            </div>


 
             <div class="col-md-6 col-12">
              <div class="form-group">

                <div class="form-group files">
                  <label>Upload Your File </label>
                  <input type="file" name="file[]" class="form-control" multiple="" required="">
                </div>

              </div>
            </div>


             <div class="col-md-2 col-12" style="margin-top: 30px;">
               <div class="form-group">
                 <div >
                    <button type="submit"  type="button" onClick="retun abc();" class="btn btn-blue"><i class="fa fa-upload"></i> Upload</button>
                 </div>
               </div> 
            </div>


          </div>
           
        </div>
      </form>   
  </div>




 	  <?php if ($photos->num_rows() > 0) {  ?>
      
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-camera"></i> Intra &amp; Extra Oral Pictures</h3>
          <!-- <div class="pull-right box-tools">
            	<a href="javascript:void(0);" onClick="return removePhotos();"><i class="fa fa-trash"></i></a>
             </div> -->
      </div>

      <div class="timeline-body">
		                  
        <!--  <?php foreach($photos->result() as $photo) {
          $pic_type=$photo->type; 
          $public_url=$photo->public_url;
         	?>
         	<?php
	         	if($pic_type=='pdf')
	         	{
	         		?><a href="<?= $public_url ?>" download="file">
					  <img src="https://cdn3.iconfinder.com/data/icons/brands-applications/512/File-512.png" alt="file" style="height: 100px;" width="104" height="142">
					</a><?php
	         	} 
	         	else
	         	{
	         		?><a href="<?= $public_url ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="image_name"><img  style="padding-top:10px; padding-left:5px height: 100px; width: 100px;" src="<?= $public_url ?>" alt="gallery" ></a><?php
	         	}
         	?>
                 
         <?php } ?> -->

         <div class="box-body no-padding">
              <table id="employee" class="table table-responsive" style="width:100%">
              	<thead>
                <tr>
                  <th width="">Sr.</th>
                  <th width="">Picture Of</th>
                  <th width="">Picture</th>
                  <th width="">Date</th>
                  <th width="">Time</th>
                </tr>
            </thead>
               <tbody>
               	<?php 
               	$count=1;
               	foreach($photos->result() as $photo) 
               	{
		            $pic_type                            = $photo->type; 
                $picture_of                          = $photo->picture_of;
                $steps                               = $photo->steps;
		            $created                             = $photo->created; 
		            $public_url                          = $photo->public_url;
		            $counter                             = $count++;
		            $CreatedDate				                 = date('F d, Y',$created);
	              $CreatedTime		 		                 = date('h:i A',$created);
	              $CreatedTimeAgo			                 = timeAgo($created).' ago'; // Calling From Shared Helper

                $picType                           = $this->model_shared->getRecordMultipleWhereOrderBy('name',PICTURE_TYPE_TABLE,array('is_deleted' => HARD_CODE_ID_NO, 'id' => $picture_of),'id','DESC')->row_array();

                $picTypeName                       = $picType['name'];
			    ?>
                               
                    <tr>
                    <td><?= $counter ?></td>

                    <?php
                      if($steps!='')
                      {
                        ?><td>Step <?= $steps ?></td><?php
                      } 
                      else
                      {
                        ?><td><?= $picTypeName ?></td><?php
                      }

                    ?>
                    

                    <td>
	                    <?php
				         	if($pic_type=='pdf')
				         	{
				         		?><a href="<?= $public_url ?>" download="file">
								  <img src="https://cdn3.iconfinder.com/data/icons/brands-applications/512/File-512.png" alt="file" style="height: 100px;" width="104" height="142">
								</a><?php
				         	} 
				         	else
				         	{
				         		?><a href="<?= $public_url ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?= $CreatedDate; ?>"><img  style="padding-top:10px; padding-left:5px height: 100px; width: 100px;" src="<?= $public_url ?>" alt="gallery" ></a><?php
				         	}
				         ?>
                    </td>
                    <td><?= $CreatedDate ?></td>
                    <td><?= $CreatedTimeAgo ?></td>
                    </td>
                </tr>
                <?php } ?>
                                   
               
               </tbody>
               <!-- <tfoot>
                      <tr>
                        <th width="32">Sr.</th>
		                  <th width="118">Code</th>
		                  <th width="122">Name</th>
		                  <th width="144">Official Email</th>
		                  <th width="123">Password</th>
		                  <th width="131">Role</th>
		                  <th width="159">Moblie</th>
                      </tr>
                  </tfoot> -->
              </table>
            </div>
		
        </div>
          <br />
       <br />
 	  </div>
      
      <?php } ?>
      

   
    </section>

      <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function() {
    $('#employee').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
</script>



        <!-- <script>
$(document).ready(function(){
 $(document).on('change', '#file', function(){
  var name = document.getElementById("file").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
   alert("Invalid Image File");
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("file").files[0]);
  var f = document.getElementById("file").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 2000000)
  {
   alert("Image File Size is very big");
  }
  else
  {
   form_data.append("file", document.getElementById('file').files[0]);
   $.ajax({
    url:"<?php echo base_url(); ?>upload-patient-pictures",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
     $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
    },   
    success:function(data)
    {
      
     $('#uploaded_image').html(data);
     document.getElementById("img_div").style.display = "none";
    }
   });
  }
 });
});
</script> -->
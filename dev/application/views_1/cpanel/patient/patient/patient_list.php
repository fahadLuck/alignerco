<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Users</li>
      
      </ol>
    </section>

    <style type="text/css">
    	.blink_me 
    	{
		  animation: blinker 1s linear infinite;
		}

		@keyframes blinker 
		{
		  50% 
		  {
		    opacity: 0;
		  }
		}
    </style>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           
           <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
           
           
          <div class="box">
            <div class="box-header">
              
              <?php 
			  		
					$totalpatients = 0;
					
					if ($patients) {
						
						$totalpatients = $patients->num_rows();	
					}
			  ?>
            
              <h3 class="box-title">Patients <small>(<?php echo $totalpatients; ?>)</small></h3>
			    <!-- <button onclick="window.location.href='<?php echo base_url(); ?>user-add/'"type="button" class="btn btn-blue pull-right"><i class="fa fa-plus"></i> Create User</button> -->
                <div class="box-tools">
                
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="employee" class="table table-responsive" style="width:100%">
              	<thead>
                <tr>
                  <th width="">Sr.</th>
                  <!-- <th width="118">Code</th -->
                  <th width="">CaseID</th>
                  <th width="">Official Email</th>
                  <th width="">Name</th>
                  <th width="">Password</th>
                  <th width="">Picture Status</th>
                  <!-- <th width="131">Role</th>
                  <th width="159">Moblie</th> -->
                  <th width="">Status</th>
                  <th width="">&nbsp;</th>
                </tr>
            </thead>
               <tbody>
               
               <?php if ($patients) {
                        
						$counter = 1;
						foreach ($patients->result() as $user) {
							
							$tableID 								=   encodeString($user->tableID);
							$accountID 								=   encodeString($user->accountID);
							$patientPictureID   					=   $user->patientPictureID;
							$username   							=   $user->username;
							$CaseID   							    =   $user->CaseID;
							$employeeID   							=   $user->employeeID;
							/*$employeeCode   						=   $user->employeeCode;*/
							$employeePrefix   						=   $user->prefixName;
							/*$employeeName   						=   $user->employeeName;*/
							/*$employeeMobileFormatted   				=   $user->employeeMobileFormatted;
							$employeePhoto   						=   $user->employeePhoto;*/
							$officialAccountEmail   				=   $user->officialAccountEmail;
							/*$officialAccountMobile   				=   $user->officialAccountMobile;*/
							$officialAccountPassword   				=   decodeString($user->officialAccountPasswordVisible);  // Calling From General Helper
							$officialAccountStatus   				=   getStatusName($user->officialAccountStatus); // Calling From Shared Helper
							
							$assignRoleID   						=   $user->roleID; 
									
						/*	$employeeAssignJobs					 	=   getEmployeeAssignJobs($employeeID);*/ // Calling From Employee Helper
							
							/*$assignRole					 			=   getEmployeeAssignedRoles($organizationID,$employeeID);*/ // Calling From User Helper


							// check last ids view status
							 $account_ID 							=   $user->accountID;
						     $recordperpage                         = 1;
						     $page                                  = 0;
						     $status_result                         = $this->model_shared->page_listing_where($page,$recordperpage,'id,account_id,view_status',MEDICAL_CASE_PATIENT_PICTURES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'account_id' => $account_ID),'id','DESC');
						     $status_row                            = $status_result->row();
						     $viewStatus                            = $status_row->view_status;
						     $lastID                                = $status_row->id;

						     //count new arrived images if last id images has not been seen
						     if($viewStatus == '') 
                               {
                               		$view_status_result                   = $this->model_shared->page_listing_where($page,$recordperpage,'id,account_id,view_status',MEDICAL_CASE_PATIENT_PICTURES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'account_id' => $account_ID,'view_status' => HARD_CODE_ID_PICTURE_SEEN),'id','DESC');
                               		$view_status_row                       = $view_status_result->row();

						             $lastViewID                             = $view_status_row->id;
						             if($lastViewID == '')
						             {
						             	 $where="id >='$lastViewID' AND id <='$lastID' AND account_id='$account_ID' ";
						             }
						             else
						             {
						             	 $where="id >='$lastViewID' AND id <'$lastID' AND account_id='$account_ID' ";
						             }

						           
						            $newImagesCount                         = $this->model_shared->page_listing_num_where('id',MEDICAL_CASE_PATIENT_PICTURES_TABLE,$where);	
                               }
						     
						     
							
							/*if ($employeeAssignJobs) {
					  
								 $employeeAssignJob 				=   $employeeAssignJobs->row_array();
								 $employeeAssignDepartment			=   $employeeAssignJob['departmentName'];
								 $employeeAssignJobPosition			=   $employeeAssignJob['jobPositionName'];
								
							 } else {
				  
								 $employeeAssignDepartment 			= 	'No department assign';
								 $employeeAssignJobPosition			= 	'No position assign';
							 }
							 
							 
							 if ($assignRole) {
								
								 $employeeAssignRoleName	= NULL;
					  
								 foreach($assignRole->result() as $roleAssign) {
										
										$employeeAssignRoleName	  .=   $roleAssign->roleName;
										$employeeAssignRoleName	 .=  ', ';
								 }
								 
								 $employeeAssignRoleName = rtrim($employeeAssignRoleName,', ');
								
							 } else {
				  
								  $employeeAssignRoleName  = 'No role assign';
							 }*/
                            ?>
                                    
                                    <tr>
                                    <td><?php echo $counter; ?></td>
                                    <!-- <td><?php echo $employeeCode; ?></td> -->
                                    <td><?php echo $CaseID; ?></td>
                                    <td><?php echo $officialAccountEmail; ?></td>
                                     <td><?php echo $username; ?></td>
                                    <?php
	                                    if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles)) 
	                                    {
											?><td><?php echo $officialAccountPassword; ?></td><?php
										} 
										else
										{
											?><?php ?><td><?php echo '••••••'; ?></td><?php ?><?php
										}
                                    ?>



                                    <?php
                                		if($patientPictureID !='' AND $viewStatus == HARD_CODE_ID_PICTURE_SEEN) 
                                		{
                                			?><td style="color: green;">Viewed</td><?php
                                		}

                                		if($patientPictureID !='' AND $viewStatus == '')
                                		{
                                			?><td style="color: red;">Not Seen</td><?php     
                                		}
                                		if($patientPictureID =='')
                                		{
                                			?><td style="color: orange;">Not Uploaded</td><?php 
                                		}

                                    ?>

                                    
                                    <!-- <td><?php echo $employeeAssignRoleName; ?></td>
                                    <td><?php echo $employeeMobileFormatted; ?></td> -->
                                    <td><?php echo $officialAccountStatus; ?></td>
                                    <td><div class="pull-right">
                                    	<?php
                                    		if($patientPictureID !='' AND $viewStatus == HARD_CODE_ID_PICTURE_SEEN) 
                                    		{
                                    			?><a href="<?php echo base_url(); ?>view-images/<?php echo $accountID; ?>/" class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i>
	                                    	     </a><?php
                                    		}

                                    		if($patientPictureID !='' AND $viewStatus == '')
                                    		{
                                    			?>
                                    			<a href="<?php echo base_url(); ?>view-images/<?php echo $accountID; ?>/" >
                                    				<sup style="font-size: 15px;"><span class="blink_me badge badge-danger"><?= $newImagesCount ?></span></sup>

                                    				<!-- <span class="blink_me badge position-absolute top-0 left-0 translate-middle bg-danger"><?= $newImagesCount ?></span> -->

                                    				<i class="blink_me glyphicon glyphicon-eye-open" style="font-weight: bold; font-size: 23px;"></i>

                                    				

	                                    	     </a>&nbsp;
	                                    	     <?php
                                    		}

                                    	?>


                                    		<a href="<?php echo base_url(); ?>patient-edit/<?php echo $accountID; ?>/" class="btn btn-success">
                                    			<i class="glyphicon glyphicon-pencil" style=""></i>
                                    		</a>

                                   			<!-- <a href="<?php echo base_url(); ?>user-remove/<?php echo $tableID;?>/" onclick="return confirm('Are you sure you want to remove the <?php echo $employeePrefix.' '.$employeeName; ?> (<?php echo $employeeCode; ?>) user?');" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a> -->
                                   			
                                    	</div>
                                    </td>
                                </tr>
                                    
                    
                    <?php $counter++; } } ?>
               
               
               </tbody>
               <!-- <tfoot>
                      <tr>
                        <th width="32">Sr.</th>
		                  <th width="118">Code</th>
		                  <th width="122">Name</th>
		                  <th width="144">Official Email</th>
		                  <th width="123">Password</th>
		                  <th width="131">Role</th>
		                  <th width="159">Moblie</th>
                      </tr>
                  </tfoot> -->
              </table>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function() {
    $('#employee').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
</script>
   
   
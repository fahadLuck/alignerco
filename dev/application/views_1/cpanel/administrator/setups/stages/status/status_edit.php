
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item">Setups</li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>manage-stages/">Production Rooms</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>stage-status/<?php echo $stageSefURL; ?>">Status</a></li>
        <li class="breadcrumb-item active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="glyphicon glyphicon-pencil"></i> Status</h3>
      </div>
   	  
      <form id="frm" name="frm" action="<?php echo base_url(); ?>edit-status/<?php echo $stageSefURL; ?>/<?php echo $row['sef_url']; ?>" method="post">
      <input type="hidden" name="old_name" value="<?php echo $row['name']; ?>" />
									
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	<div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Name</label>
				  <div class="col-sm-10">
					<input name="name" class="form-control" type="text" value="<?php echo $row['name']; ?>" id="example-text-input">
                    <?php if (form_error('name')) { echo form_error('name'); } ?>
				  </div>
				</div>
                
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Description</label>
				  <div class="col-sm-10">
					<textarea name="description" class="form-control" rows="3" placeholder=""><?php echo $row['description']; ?></textarea>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">&nbsp;</label>
				  <div class="col-sm-10 offset-md-2">
					 <button type="submit" name="submit" value="Save" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-saved"></i> Update</button>
                     <button type="button" class="btn btn-warning "  onclick="window.location.href='<?php echo base_url(); ?>stage-status/<?php echo $stageSefURL; ?>'">Cancel</button>
				  </div>
				</div>
                
                
				
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
     
      </form>   
        
        
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
  
    </section>
    <!-- /.content -->
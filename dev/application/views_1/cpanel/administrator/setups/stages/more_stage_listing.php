	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item">Setups</li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>manage-stages/">Production Stages</a></li>
        <?php
                  
		             	$checkParent 		=	$this->model_shared->getRecord('*',MEDICAL_PRODUCTION_STAGES_TABLE,'id',$parentID);
						$checkParent 		=  $checkParent->row_array();
						
						if ($checkParent['parent'] != HARD_CODE_ID_PARENT_OR_INDEPENDENT) {
							  
				  			$genrationFamilyArray 	=	 	getFamilyCircle(MEDICAL_PRODUCTION_STAGES_TABLE,$parentID); // Calling From Admin Helper
							
							if (sizeof($genrationFamilyArray) > 0) {
								
									foreach ($genrationFamilyArray as $value) {
										
										 if ($value == HARD_CODE_ID_PARENT_OR_INDEPENDENT) { 
										 		
												continue; 
										 }
										 
										$valueSefURL  = $this->model_shared->getRecordMultipleWhere('sef_url',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $value));
										$valueSefURL  = $valueSefURL->row_array();
										
										$valueSefURL  =	$valueSefURL['sef_url'];
										
										
										echo '<li class="breadcrumb-item"><a href="'.base_url().'more-stages/'.$valueSefURL.'">'.getName(MEDICAL_PRODUCTION_STAGES_TABLE,$value).'</a></li>'; // Calling From Admin Helper
									} 		
							}
						}
				  
		?> 
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>more-stages/<?php echo $parentSefURL; ?>/"><?php  echo getName(MEDICAL_PRODUCTION_STAGES_TABLE,$parentID); // Calling From Application Helper ?></a></li>
        <li class="breadcrumb-item active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           
           <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
           
           
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Production Stages <small>(<?php echo $totalRows; ?>)</small></h3>
			   
               <?php  if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles)) { ?>
               
                <button onclick="window.location.href='<?php echo base_url(); ?>add-more-stage/<?php echo $parentSefURL; ?>/'"type="button" class="btn btn-blue pull-right"><i class="fa fa-plus"></i> Add item</button>
              
			  <?php } ?>
              <div class="box-tools">
               	<?php echo $paginglink;?>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-responsive" style="width:100%">
                <tr>
                  <th width="50">#</th>
                  <th width="135">Name</th>
                  <th width="216">&nbsp;</th>
                  <th width="95">&nbsp;</th>
                </tr>
                
                <?php
					
					if ($result->num_rows() > 0) {
							
							$counter = 1;
							foreach($result->result() as $row) {
				?>
                
                 <tr>
                  <td><?php echo $counter; ?></td>
                  <td><?php echo $row->name; ?></td>
                  <td>
                  		
                       <?php  $checkChilds =   $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'parent' => $row->id),'order','ASC');
						 
								if ($checkChilds->num_rows() > 0) {

											$i=1;
											$colorCounter = 1;
											foreach($checkChilds->result() as $child) {
												
												if ($colorCounter == 1) {
						
														  $colorClass = 'bg-blue';	
														
												  } elseif($colorCounter == 2) {
														  
														  $colorClass = 'bg-red';
													
												  } elseif($colorCounter == 3) {
														  
														  $colorClass = 'bg-light-blue';	
													
												  } elseif($colorCounter == 4) {
														  
														  $colorClass = 'bg-green';
												  
												  } elseif($colorCounter == 5) {
														  
														  $colorClass = 'bg-yellow';
												  
												  } elseif($colorCounter == 6) {
														  
														  $colorClass = 'bg-aqua';
												  
												  } elseif($colorCounter == 7) {
														  
														  $colorClass = 'bg-purple';
												  }
												  
												  if($colorCounter == 7) {
														  
														  $colorCounter = 1;
												  }
												
												
												echo '<span class="label '.$colorClass.'">'.$child->name.'</span>';
												echo '&nbsp;';
												
												if ($i == 5) {
														echo '<p></p>';
														$i = 1;	
												}
	
												$i++;
												$colorCounter++;
											  }
								
								} else {
											echo '----';  
								}
						 
			   			?>
                  	
                  </td>
                  <td>
                    <div class="pull-right">
                 	 
                      <a href="<?php echo base_url(); ?>more-stages/<?php echo $row->sef_url;?>/"  class="btn bg-olive" data-toggle="tooltip" title="More Production Rooms"><i class="glyphicon glyphicon-plus"></i></a>
                   	  
                      <a href="<?php echo base_url(); ?>stage-status/<?php echo $row->sef_url;?>/" class="btn bg-orange" data-toggle="tooltip" title="Status"><i class="glyphicon glyphicon-tags"></i></a> 
                       
                      <?php if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles)) { ?>
                    	<a href="<?php echo base_url();?>edit-more-stage/<?php echo $parentSefURL;?>/<?php echo $row->sef_url;?>" class="btn btn-primary" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                  	 <?php } ?>
                     
                     <?php if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles)) { ?>
                  		<a onclick="return confirm('Are you sure you want to delete <?php echo $row->name; ?> stage?');" href="<?php echo base_url();?>remove-more-stage/<?php echo $parentSefURL;?>/<?php echo $row->sef_url;?>"  class="btn btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                 	 <?php } ?>
                    
                    </div>
                  </td>
                </tr>
                
                <?php $counter++;
							}	
					}
				 ?>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
		</div>
        <!-- /.col -->
      </div>
     
  
    </section>
    <!-- /.content -->
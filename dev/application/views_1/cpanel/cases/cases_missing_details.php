<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Cases</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           

           
           
          <div class="box">
            <div class="box-header">
              
            
              <h3 class="box-title">Missing Cases <small>(<?php echo $counter; ?>)</small></h3>

		     <!--  <button onclick="window.location.href='<?php echo base_url(); ?>employee-add/'"type="button" class="btn btn-blue pull-right"><i class="fa fa-plus"></i> Add Employee</button> -->

                <div class="box-tools">
                  
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="example" class="table table-responsive" style="width:100%">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Shipment Number</th>
                  <th>Patient</th>
                  <th>Status</th>
                  <th>Created By</th>
                  <th width="13%">Date</th>
                  <th width="11%">&nbsp;</th>
                </tr>
              </thead>
               <tbody>
                  <?php
                  $count=1;
                  foreach($cases->result() as $mcd)
                    {
                      $counter=$count++;
                      $trackingNumber=$mcd->trackingNumber;
                      $patientName=$mcd->patientName;
                      $statusName=$mcd->statusName;
                      $receiveDate = date('d M Y',strtotime($mcd->receiveDate));
                      $caseStatusCreatedTimeAgo      = timeAgo($mcd->caseStatusCreated).' ago';
                      $companyName             = $mcd->companyName;
                      $employName             = $mcd->employName;
                      /*$table_ID=$mcd->tableID;

                      $tableID = encodeString($mcd->trackingNumber);*/
                    ?>      
                    <tr>
                    <td><?php echo $counter; ?></td>
                    <td><?php echo $trackingNumber; ?></td>
                   <td>
                      <span data-toggle="tooltip" title="" data-original-title="Patient Name" style="font-size:12px;"><a target="_blank" href=""><strong><?php echo $patientName; ?></strong></a></span>
                        <br />
                      <span data-toggle="tooltip" title="" data-original-title="Distributor"><a target="_blank" href=""><?php echo $companyName; ?></a></span>
                   </td>
                    <td><?php echo $statusName; ?></td>
                    <td><?php echo $employName; ?></td>
                    <td><?php echo $receiveDate; ?></td>
                    <td><?php echo $caseStatusCreatedTimeAgo; ?></td>
                    <!-- <td>sdfsdfsd</td>

                    <td>
                      adsadad
                    </td> -->
                </tr>
              <?php } ?>
               </tbody>
              </table>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>
   

   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
</script>
   
<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>manage-cases/">Cases</a></li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     	
        				<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
     
     <?php 
	 		$todayDate 						= date('d-m-Y',time());
			
			$caseID 						= $case['caseID'];
			$patientName 					= $case['patientName'];
			$archUpper 						= $case['archUpper'];
			$archLower 						= $case['archLower'];
			$companyName 					= $case['companyName'];
			$companyLogo 					= $case['companyLogo'];
			$countryName 					= $case['countryName'];
			$caseComments					= $case['caseComments'];
			$caseShipped					= $case['caseShipped'];
			$caseReceiveDate 				= $case['receiveDate'];
			$caseCreated					= $case['caseCreated'];
			$caseCreatedEmployeeName		= $case['employeeName'];
			$caseCreatedEmployeeCode		= $case['employeeCode'];
			$caseCreatedEmployeeID			= $case['employeeID'];
			
			$companyID 						= $case['companyID'];
			
			$employeeAssignJobs				= getEmployeeAssignJobs($caseCreatedEmployeeID); // Calling From HR Employees Helper
										
			if ($employeeAssignJobs) {
						
				$employeeAssignJob 								= $employeeAssignJobs->row_array();
				$caseCreatedEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
				
			
			} else {
					
					$caseCreatedEmployeeAssignJobDescription 	= NULL;
			}	   		
			
			$caseCreatedDate		= date('d F. Y',$caseCreated);
			$caseCreatedTime		= date('h:i A',$caseCreated);
			$caseCreatedTimeAgo		= timeAgo($caseCreated).' ago';
			
			$caseReceived			= date('l, d F Y',strtotime($caseReceiveDate));
			
			if (empty($caseComments)) {
				
					$caseComments  = '-----';
			}
		
	 ?>
     
     <div class="row">
       
        <div class="col-xl-4 col-lg-5">

          <div class="box box-primary">
            <div class="box-body box-profile">
             <?php if ($companyLogo) { ?>
              <img class="profile-user-img img-fluid mx-auto d-block" src="<?php echo base_url(); ?>backend_images/companies/<?php echo $companyLogo; ?>" alt="Company Logo">
             <?php } ?>
              <h3 class="profile-username text-center"><?php echo $patientName; ?></h3>

              <p class="text-muted text-center">Receive Date: <?php echo date('d F. Y',strtotime($caseReceiveDate)); ?></p>
				
              <div class="row">
              	<div class="col-12">
              		<div class="profile-user-info">
						<p>Case ID </p>
						<h6 class="margin-bottom"><?php echo $caseID; ?></h6>
                        <p>Arch (es)</p>
						<h6 class="margin-bottom">
					      <?php if ($archUpper == HARD_CODE_ID_YES) { ?>
                          
                              <input id="md_checkbox_1" name="arch_upper" value="<?php echo HARD_CODE_ID_YES ?>" class="chk-col-green" type="checkbox" <?php if ($archUpper == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?> disabled />
                              <label for="md_checkbox_1">Upper</label>
                           &nbsp;&nbsp;
						  <?php } ?>
                          
                           <?php if ($archLower == HARD_CODE_ID_YES) { ?>
                            
                                <input id="md_checkbox_2" name="arch_lower" value="<?php echo HARD_CODE_ID_YES ?>" class="chk-col-green" type="checkbox" <?php if ($archLower == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?> disabled />
                                <label for="md_checkbox_2">Lower</label>
                          <?php } ?>
                        </h6>
                        <p>Distributor</p>
						<h6 class="margin-bottom"><?php echo $companyName; ?></h6>
                        <p>Country</p>
						<h6 class="margin-bottom"><?php echo $countryName; ?></h6>
                        <p>Comments / Remarks</p>
						<h6 class="margin-bottom"><?php echo $caseComments; ?></h6>
					</div>
             	</div>
              </div>
            
            </div>
          </div>
        </div>
        
        <div class="col-xl-8 col-lg-7">
          
          <div class="nav-tabs-custom">
            
            <ul class="nav nav-tabs">
              
              <li><a class="active" href="#timeline" data-toggle="tab">Timeline</a></li>
              
              <!-- Quality of Cases is the last point where case circle ended. Shippment Section-->
              <?php if ($presentProductionStageID != RECEIVE_ORDER_ENTRY && $presentStatusID != CASE_STATUS_ALIGNERS_TO_CUSTOMER_RECEIVED) { ?>
                
                 <?php 
					  
				  if (in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) {
					  
				 ?>
                
      	        <li id="status-tab-area"><a href="#status" data-toggle="tab" onClick="return resetStatusForm();">Update Status</a></li>
            
             <?php } ?>
             <?php } ?>
             </ul>
                        
            <div class="tab-content">
             
              <div class="active tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline">
				  
                    <?php 
							$totalStages = $caseStageHistory->num_rows();
							
							if ($totalStages) {
										
									$i 				 = $totalStages;
									$stageCounter 	 = 1;
									$operatorCounter = 1;
									foreach($caseStageHistory->result() as $stageHistory) {
											
											$caseStageID 					= $stageHistory->caseStageID;
											$productionStageID 				= $stageHistory->productionStageID;
											$productionStageName 			= $stageHistory->productionStageName;
											$caseStageCreated 				= $stageHistory->caseStageCreated;
											
											$stageCreatedDate				= date('d F. Y',$caseStageCreated);
											$stageCreatedTime				= date('h:i A',$caseStageCreated);
											
											$caseStatusHistory				= caseChangeStatusHistory($caseStageID,$caseID); // Calling From Case Helper
					?>
                    
<li class="time-label"><span class="bg-green"><?php echo $productionStageName; ?></span></li>

<?php 
if ($caseStatusHistory) {

	$statusCounter = 1;
	foreach($caseStatusHistory->result() as $statusHistory) {
		
		$caseStatusID 			 			 = $statusHistory->caseStatusID;  
		$statusID 				 			 = $statusHistory->statusID;
		$statusName 			 			 = $statusHistory->statusName;
		$statusParentID 		 			 = $statusHistory->statusParentID;
		$caseStatusComments 	 			 = $statusHistory->caseStatusComments;
		$caseStatusAutoMove 	 			 = $statusHistory->caseStatusAutoMove;
		$caseStatusCreated 		 			 = $statusHistory->caseStatusCreated;
		$caseStatusCreatedEmployeeID 		 = $statusHistory->employeeID;
		$caseStatusCreatedEmployeeName 		 = $statusHistory->employeeName;
		$caseStatusCreatedEmployeeCode 		 = $statusHistory->employeeCode;
		$caseStatusCreatedEmployeePhoto 	 = $statusHistory->employeePhoto;
			
		$statusCreatedDate		 			 = date('D, M d Y,',$caseStatusCreated);
		$statusCreatedTime					 = date('h:i A',$caseStatusCreated);
		$statusCreatedTimeAgo				 = timeAgo($caseStatusCreated).' ago';
		
		$employeeAssignJobs					= getEmployeeAssignJobs($caseStatusCreatedEmployeeID); // Calling From HR Employees Helper
		
		if ($employeeAssignJobs) {
									
			 $employeeAssignJob 								= $employeeAssignJobs->row_array();
			 $caseStatusCreatedEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
			  
		  
		 } else {
				  
			 $caseStatusCreatedEmployeeAssignJobDescription 	= NULL;
		 }
		 
		  $caseStatusCreatedPhotoPath = NULL; 
		  $photoFlag 				  = false;
											   
		   if ($caseStatusCreatedEmployeePhoto) {
					
					$caseStatusCreatedPhotoPath = HR_OFFICIALS_USERS_URL_PATH.'thumbnail_'.$caseStatusCreatedEmployeePhoto;   
					$photoFlag = true;
		   } else {
			  	  
				    $caseStatusCreatedPhotoPath = USER_DEFAULT_IMAGE_URL_PATH; 
		    }	   		   	
?>
	
<li>
  <i class="fa fa-tags bg-blue"></i>
   <div class="timeline-item">
   
    <span class="time"><?php echo $statusCreatedDate; ?> <?php echo $statusCreatedTime; ?></span>

    <h3 class="timeline-header" style="color:#111; font-weight:600;">
			
		   <?php if ($statusParentID) { 
					   
					   echo '('.statusName($statusParentID).') '; // Calling From Case Helper
				  }
			?>
            
			<?php echo $statusName; ?>
         
    </h3>
    
     <?php if ($caseStatusAutoMove == HARD_CODE_ID_YES) { ?>
     
	 <?php } ?>
    
    <span class="time" style="font-size:11px;">
    	
    	<i class="fa fa-clock-o"></i> <?php echo $statusCreatedTimeAgo; ?>
    
     </span>
    
     <?php if ($productionStageID == RECEIVE_ORDER_ENTRY) { ?>
    
      <div class="timeline-body">
              
              <span style="font-size:0.9em;"> 
              	Case is created by <strong><?php echo $caseCreatedEmployeeName; ?> - <?php echo $caseCreatedEmployeeCode; ?> (<?php echo $caseCreatedEmployeeAssignJobDescription; ?>)</strong>
              </span>
              </div>
    <?php } ?>
   
    <?php if ($productionStageID != RECEIVE_ORDER_ENTRY) { ?>
    
          
          		<?php if (in_array($statusID,$caseStatusReadyArray)) { ?>
                			
                            <?php  if ($caseStatusAutoMove == HARD_CODE_ID_YES) { ?>
                                 
                                        <div class="timeline-body">
                          
                                             <span style="font-size:0.8em;"> 
               									<i class="fa fa-forward"></i> automaticaly forworded
                 							 </span>
                                         
                                         </div>
                                   
                              <?php } else if ($caseStatusAutoMove == HARD_CODE_ID_NO) { ?>
                               		
                                    	<div class="timeline-body">
                    
                                             <span style="font-size:0.9em;"> 
                                                Case status updated by <strong><?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)</strong>
                                             </span>
                                   
                                  		 </div>
                                         
                                       <?php if($caseStatusComments) { ?>
                                         
                                        <div class="activitytimeline">
                              
                                                   <p style="margin-bottom:10px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                   <br />
                                              
                                          </div>
                                          
                                       <?php } ?>
                               
                               <?php } ?>
                
                <?php
							
								
								
						} else if (in_array($statusID,$caseStatusProcessArray)) {
							
								
								$caseStatusOperator = $this->model_case->getCaseStatusOperator($caseID,$caseStageID,$caseStatusID);
								if ($caseStatusOperator->num_rows() == 1) { 
										
											  $caseStatusOperator = $caseStatusOperator->row_array();
					
											  $caseOperatorComments 			 = $caseStatusOperator['caseOperatorComments'];
											  $caseOperatorAssignmentType 		 = $caseStatusOperator['caseOperatorAssignmentType']; 
											  $caseOperatorResumeFlag 			 = $caseStatusOperator['caseResumeFlag'];
											  $caseOperatorCreated 				 = $caseStatusOperator['caseOperatorCreated'];
						
											  $operatorEmployeeID 				 = $caseStatusOperator['employeeID'];
											  $operatorEmployeeCode 			 = $caseStatusOperator['employeeCode'];
											  $operatorEmployeeName 			 = $caseStatusOperator['employeeName'];
											  $operatorEmployeePhoto 			 = $caseStatusOperator['employeePhoto'];
											  
											  $operatorCreatedDate				 = date('F d. Y',$caseOperatorCreated);
											  $operatorCreatedTime		 		 = date('h:i A',$caseOperatorCreated);
											  $operatorCreatedTimeAgo			 = timeAgo($caseOperatorCreated).' ago'; // Calling From Shared Helper
											  
											  $employeeAssignJobs				 = getEmployeeAssignJobs($operatorEmployeeID); // Calling From HR Employees Helper
											  
											  if ($employeeAssignJobs) {
															
													$employeeAssignJob 								= $employeeAssignJobs->row_array();
													$caseOperatorEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
													
												
											   } else {
														
														$caseOperatorEmployeeAssignJobDescription 	= NULL;
											   }
											   
											   $photoPath 		= USER_DEFAULT_IMAGE_URL_PATH; 
											   $photoLargePath  = USER_DEFAULT_IMAGE_URL_PATH; 
											   
											   if ($operatorEmployeePhoto) {
														
														$photoPath 		= HR_OFFICIALS_USERS_URL_PATH.'thumbnail_'.$operatorEmployeePhoto;   
														$photoLargePath = HR_OFFICIALS_USERS_URL_PATH.$operatorEmployeePhoto;   
											   } 	   		
					?>
                    							
                                                
                                                <div class="post" style="margin-left:20px; padding-top:20px;">
                            	
													  <?php if ($caseOperatorAssignmentType == HARD_CODE_PICK) { ?>
                                                           
                                                           <div class="user-block">
                                                              <?php if ($operatorEmployeePhoto) { ?>
                                                           	
                                                                <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $operatorEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                            
                                                              <?php } else { ?>
                                                             	
                                                                <img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                                
                                                              <?php } ?>
                                                                  
                                                                  
                                                                <?php if ($caseOperatorResumeFlag == HARD_CODE_ID_YES) { ?>
																 
                                                                  <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                    
                                                                     <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>) 's
                                                    				    case <span class="label" style="background:#D11283;">resumed processing</span> by
                                                                       
                                                                       <span style="color:#000">
																	   
																	   <?php echo $caseStatusOperator['creatorEmployeeName']; ?> - <?php echo $caseStatusOperator['creatorEmployeeCode']; ?>
                                                                       
                                                                       <?php 
																	   		
																			$creatorEmployeeAssignJobs	 = getEmployeeAssignJobs($caseStatusOperator['creatorEmployeeID']); // Calling From HR Employees Helper
																			
																			if ($creatorEmployeeAssignJobs) {
															
																				$creatorEmployeeAssignJobs 								= $creatorEmployeeAssignJobs->row_array();
																				$caseOperatorCreatorEmployeeAssignJobDescription		= $creatorEmployeeAssignJobs['jobPositionName'];
												
																			   } else {
																						
																						$caseOperatorCreatorEmployeeAssignJobDescription 	= NULL;
																			   }
																	   ?>
                                                                       
                                                                       (<?php echo $caseOperatorCreatorEmployeeAssignJobDescription; ?>)
                                                                       
                                                                       </span>
                                                                       
                                                                        and <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                        
																		<?php echo $IS_WAS; ?> now                                                                                                     
                                                                    
                                                                        working as operator. 
                                                      
                                                                      </span>
                                                                 
																 <?php } else { ?>
                                                                 
                                                                
                                                                  <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                    
                                                                     <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)
                                                    
                                                                          <span class="label label-warning">picked</span> the case and
                                                    
                                                                      <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                      
                                                                      <?php echo $IS_WAS; ?>  working as operator. 
                                                      
                                                                      </span>
                                                   				 
                                                                 <?php } ?>
                                                   
                                                                      <span class="description" style="margin-bottom: <?php if($caseOperatorComments) { echo 0; } else {echo '15';} ?>px; font-size:11px;">
                                                          
                                                                              <?php 	
                                                                                  
                                                                                  if ($todayDate == date('d-m-Y',$caseOperatorCreated)) {
                                                                          
                                                                                          echo $operatorCreatedTimeAgo;
                                                                          
                                                                                  } else {
                                                                  
                                                                                          echo $operatorCreatedDate.' '.$operatorCreatedTime;
                                                                                  }
                                                                                
                                                                                ?>
                                                                          </span>
                                                                    </div>
                                                                    
                                                                    <div class="activitytimeline">
                                                
                                                                       <?php if($caseOperatorComments) { ?>
                                                      
                                                                           <p style="margin-bottom:10px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseOperatorComments; ?>”</p>
                                                                           <br />
                                                                       
                                                                       <?php } ?>
                                                                      
                                                                    </div>
                                                      
                                                      <?php } else if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED || $caseOperatorAssignmentType == HARD_CODE_AUTO_ASSIGNED) { ?>
                                
                                				
                                                                      <div class="user-block">
                                                                       
                                                                       <?php if ($operatorEmployeePhoto) { ?>
                                                           	
                                                              				  <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $operatorEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                                        
																		<?php } else { ?>
                                                                       	     
                                                                             <img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                                        
																		<?php } ?>
                                                                              <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                        
                                                                                     <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)
                                                              
                                                                                            <span class="label label-info"><?php if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED) { echo 'assigned'; } else if ($caseOperatorAssignmentType == HARD_CODE_AUTO_ASSIGNED) { echo 'auto assigned'; } ?></span> the case and
                                                              
                                                                                     <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                
                                                                                     <?php echo $IS_WAS; ?>  working as operator. 
                                                          
                                                                              </span>
                                                       
                                                                               <span class="description" style="margin-bottom: <?php if($caseOperatorComments) { echo 0; } else { echo '15';} ?>px; font-size:11px;">
                                                              
                                                                                  <?php 	
                                                                                      
                                                                                      if ($todayDate == date('d-m-Y',$caseOperatorCreated)) {
                                                                              
                                                                                              echo $operatorCreatedTimeAgo;
                                                                              
                                                                                      } else {
                                                                      
                                                                                              echo $operatorCreatedDate.' '.$operatorCreatedTime;
                                                                                      }
                                                                                    
                                                                                   ?>
                                                                                 </span>
                                                                           </div>	
                                                     
                                                <div class="activitytimeline">
                      
                                                       <?php if($caseOperatorComments) { ?>
                                      
                                                           <p style="margin-bottom:10px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseOperatorComments; ?>”</p>
                                                           <br />
                                                       <?php } ?>
                                                      
                                                 </div>
								
								
								<?php } ?>
                                
                            </div>
                                                
                    			
                    
                    <?php
								} else {
										
										$caseOperatorHistory   = caseChangeOperatorHistory($caseID,$caseStageID,$caseStatusID); // Calling From Case Helper
										
										if ($caseOperatorHistory) {
				 	  
											  $operatorCounter = 1;
											  foreach($caseOperatorHistory->result() as $operatorHistory) {
												  
												  $caseOperatorComments 			 = $operatorHistory->caseOperatorComments;
												  $caseOperatorAssignmentType 		 = $operatorHistory->caseOperatorAssignmentType;
												  $caseOperatorResumeFlag 			 = $operatorHistory->caseResumeFlag;
												  $caseOperatorCreatedBy 			 = $operatorHistory->caseOperatorCreatedBy;
												  $caseOperatorCreated 				 = $operatorHistory->caseOperatorCreated;
							
												  $operatorEmployeeID 				 = $operatorHistory->employeeID;
												  $operatorEmployeeCode 			 = $operatorHistory->employeeCode;
												  $operatorEmployeeName 			 = $operatorHistory->employeeName;
												  $operatorEmployeePhoto 			 = $operatorHistory->employeePhoto;
												  
												  $caseOperatorCreatedBy 			 = getCaseOperatorCreatedBy($caseOperatorCreatedBy); // Calling From Case Helper
												  
												  if ($caseOperatorCreatedBy) {
															
															$caseOperatorCreatedBy  					= $caseOperatorCreatedBy->row_array();
															$caseOperatorCreatedByID  					= $caseOperatorCreatedBy['employeeID'];
															$caseOperatorCreatedByName  				= $caseOperatorCreatedBy['employeeName'];
															$caseOperatorCreatedByCode 					= $caseOperatorCreatedBy['employeeCode'];
															
															$caseOperatorCreatedByAssignJobs			= getEmployeeAssignJobs($caseOperatorCreatedByID); // Calling From HR Employees Helper
															
															if ($caseOperatorCreatedByAssignJobs) {
																
																$caseOperatorCreatedByAssignJobs 				= $caseOperatorCreatedByAssignJobs->row_array();
																$caseOperatorCreatedByAssignJobDescription		= $caseOperatorCreatedByAssignJobs['jobPositionName'];
															}
														
												  }
												  
												  $operatorCreatedDate				 = date('F d. Y',$caseOperatorCreated);
												  $operatorCreatedTime		 		 = date('h:i A',$caseOperatorCreated);
												  $operatorCreatedTimeAgo			 = timeAgo($caseOperatorCreated).' ago'; // Calling From Shared Helper
												  
												  $employeeAssignJobs				 = getEmployeeAssignJobs($operatorEmployeeID); // Calling From HR Employees Helper
												  
												  if ($employeeAssignJobs) {
																
														$employeeAssignJob 								= $employeeAssignJobs->row_array();
														$caseOperatorEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
														
													
												   } else {
															
															$caseOperatorEmployeeAssignJobDescription 	= NULL;
												   }
												   
												   $photoPath = NULL; 
												   
												   if ($operatorEmployeePhoto) {
															
															$photoPath = HR_OFFICIALS_USERS_URL_PATH.'thumbnail_'.$operatorEmployeePhoto;   
												   }  else {
													 		
															$photoPath = USER_DEFAULT_IMAGE_URL_PATH;   
												   }	   		
					  ?>
                                            
                                            				<div class="post" style="margin-left:20px; padding-top:20px;">
                            	
																	  <?php if ($caseOperatorAssignmentType == HARD_CODE_PICK) { ?>
                                                                           
                                                                           <div class="user-block">
                                                                              
                                                                          	<?php if ($operatorEmployeePhoto) { ?>
                                                           	
                                                              					  <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $operatorEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                                        
																			 <?php } else { ?>
                                                                       	    
                                                                             	 <img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                                        
																			<?php } ?>
                                                                              
                                                                                 
                                                                                   <?php if ($caseOperatorResumeFlag == HARD_CODE_ID_YES) { ?>
																 
                                                                                          <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                            
                                                                                            <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>) 's
                                                                                                case <span class="label" style="background:#D11283;">resumed processing</span> by
                                                                                               
                                                                                               <span style="color:#000">
                                                                                               
                                                                                               <?php echo $operatorHistory->creatorEmployeeName; ?> - <?php echo $operatorHistory->creatorEmployeeCode; ?>
                                                                                               
                                                                                               <?php 
                                                                                                    
                                                                                                    $creatorEmployeeAssignJobs	 = getEmployeeAssignJobs($operatorHistory->creatorEmployeeID); // Calling From HR Employees Helper
                                                                                                    
                                                                                                    if ($creatorEmployeeAssignJobs) {
                                                                                    
                                                                                                        $creatorEmployeeAssignJobs 								= $creatorEmployeeAssignJobs->row_array();
                                                                                                        $caseOperatorCreatorEmployeeAssignJobDescription		= $creatorEmployeeAssignJobs['jobPositionName'];
                                                                        
                                                                                                       } else {
                                                                                                                
                                                                                                                $caseOperatorCreatorEmployeeAssignJobDescription 	= NULL;
                                                                                                       }
                                                                                               ?>
                                                                                               
                                                                                               (<?php echo $caseOperatorCreatorEmployeeAssignJobDescription; ?>)
                                                                                               
                                                                                               </span>
                                                                                               
                                                                                                and <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                                                
                                                                                                <?php echo $IS_WAS; ?> now                                                                                                     
                                                                                            
                                                                                                working as operator. 
                                                                              
                                                                                              </span>
                                                                                         
                                                                                   <?php } else { ?>
                                                                                                          
                                                                                           <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                                            
                                                                                                            <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)
                                                                                            
                                                                                                                  <span class="label label-warning">picked</span> the case and
                                                                                            
                                                                                                              <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                                              
                                                                                                              <?php echo $IS_WAS; ?>  working as operator. 
                                                                                              
                                                                                                              </span>
                                                                                      
                                                                                   <?php } ?>
                                                                   
                                                                                  <span class="description" style="margin-bottom: <?php if($caseOperatorComments) { echo 0; } else {echo '15';} ?>px; font-size:11px;">
                                                                      
                                                                                          <?php 	
                                                                                              
                                                                                              if ($todayDate == date('d-m-Y',$caseOperatorCreated)) {
                                                                                      
                                                                                                      echo $operatorCreatedTimeAgo;
                                                                                      
                                                                                              } else {
                                                                              
                                                                                                      echo $operatorCreatedDate.' '.$operatorCreatedTime;
                                                                                              }
                                                                                            
                                                                                            ?>
                                                                                      </span>
                                                                                   
                                                                                    </div>
                                                                                    
                                                                                    <div class="activitytimeline">
                                                                
                                                                                       <?php if($caseOperatorComments) { ?>
                                                                      
                                                                                           <p style="margin-bottom:10px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseOperatorComments; ?>”</p>
                                                                                           <br />
                                                                                       
                                                                                       <?php } ?>
                                                                                      
                                                                                    </div>
                                                                      
                                                                      <?php } else if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED || $caseOperatorAssignmentType == HARD_CODE_AUTO_ASSIGNED) { ?>
                                
                                				
                                                                                  <div class="user-block">
                                                                                   
                                                                                       
                                                                                       <?php if ($operatorEmployeePhoto) { ?>
                                                           	
                                                              					  			<a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $operatorEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                                        
																						 <?php } else { ?>
                                                                       	    
                                                                             	 			<img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                                        
																						<?php } ?>
                                                                                     
                                                                                          <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                    
                                                                                                 <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)
                                                                          
                                                                                                        <span class="label label-info"><?php if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED) { echo 'assigned'; } else if ($caseOperatorAssignmentType == HARD_CODE_AUTO_ASSIGNED) { echo 'auto assigned'; } ?></span> the case <?php if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED) { ?>by <span style="color:#000"><?php echo $caseOperatorCreatedByName; ?> - <?php echo $caseOperatorCreatedByCode; ?> (<?php echo $caseOperatorCreatedByAssignJobDescription; ?>)</span> <?php } ?> and
                                                                          
                                                                                                 <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                            
                                                                                                 <?php echo $IS_WAS; ?>  working as operator. 
                                                                      
                                                                                          </span>
                                                                   
                                                                                           <span class="description" style="margin-bottom: <?php if($caseOperatorComments) { echo 0; } else {echo '15';} ?>px; font-size:11px;">
                                                                          
                                                                                              <?php 	
                                                                                                  
                                                                                                  if ($todayDate == date('d-m-Y',$caseOperatorCreated)) {
                                                                                          
                                                                                                          echo $operatorCreatedTimeAgo;
                                                                                          
                                                                                                  } else {
                                                                                  
                                                                                                          echo $operatorCreatedDate.' '.$operatorCreatedTime;
                                                                                                  }
                                                                                                
                                                                                               ?>
                                                                                             </span>
                                                                                       </div>	
                                                     
                                                                                  <div class="activitytimeline">
                                                          
                                                                                           <?php if($caseOperatorComments) { ?>
                                                                          
                                                                                               <p style="margin-bottom:0px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseOperatorComments; ?>”</p>
                                                                                              
                                                                                           <?php } ?>
                                                                                          
                                                                                     </div>
								
								
																	  <?php } ?>
                                
                           									  </div>
                                            
                      <?php
											
												$operatorCounter++;
											}
										
										}
								}
															 
								
						}  

						else if (in_array($statusID,$caseStatusHoldArray)) { 
	                  ?>
    						 <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                               <div class="user-block">
                                                                       
                                     <?php if ($caseStatusCreatedEmployeePhoto && $photoFlag) { ?>
                      
                                       <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseStatusCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)"> <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt=""></a>
                                  
                                       <?php } else { ?>
                                      
                                            <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt="">
                                  
                                      <?php } ?>
                                    
                                
                                        <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                  
                                               <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                        
                                                      <span class="label" style="background-color:#001f3f;">hold</span> the case status 
                        
                                               <?php if ($caseStatusComments) { ?>
                                       
                                              		 with following comments
                                              		 <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                              
                                       		  <?php } ?>
                                     
                                     </span>
                                     
                                     <span class="description" style="margin-bottom:0px; font-size:11px;"> <?php echo $statusCreatedTimeAgo; ?></span>
                 		           
                                   </div>
                                 </div>
    
    <?php
						
						} 



						/*changes by fahad for update case activity start*/	
						else if (in_array($statusID,$caseStatusUpdateArray)) { 
	                  ?>
    						 <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                               <div class="user-block">
                                                                       
                                     <?php if ($caseStatusCreatedEmployeePhoto && $photoFlag) { ?>
                      
                                       <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseStatusCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)"> <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt=""></a>
                                  
                                       <?php } else { ?>
                                      
                                            <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt="">
                                  
                                      <?php } ?>
                                    
                                
                                        <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                  
                                               <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                        
                                                      <span class="label" style="background-color:#001f3f;">update</span> the case information 
                        
                                               <?php if ($caseStatusComments) { ?>
                                       
                                              		 with following comments
                                              		 <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                              
                                       		  <?php } ?>
                                     
                                     </span>
                                     
                                     <span class="description" style="margin-bottom:0px; font-size:11px;"> <?php echo $statusCreatedTimeAgo; ?></span>
                 		           
                                   </div>
                                 </div>
    
    <?php
						
						}
						/*changes by fahad for update case activity ends*/



						else if (in_array($statusID,$caseStatusOFFHoldArray)) { 
	?>
    						 <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                               <div class="user-block">
                                                                       
                                     <?php if ($caseStatusCreatedEmployeePhoto && $photoFlag) { ?>
                      
                                       <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseStatusCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)"> <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt=""></a>
                                  
                                       <?php } else { ?>
                                      
                                            <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt="">
                                  
                                      <?php } ?>
                                   
                                
                                        <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                  
                                               <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                        
                                                      <span class="label" style="background-color:#001f3f;">OFF-hold</span> the case status 
                        
                                               <?php if ($caseStatusComments) { ?>
                                       
                                              		 with following comments
                                              		 <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                              
                                       		  <?php } ?>
                                     
                                     </span>
                                     
                                     <span class="description" style="margin-bottom:0px; font-size:11px;"> <?php echo $statusCreatedTimeAgo; ?></span>
                 		           
                                   </div>
                                 </div>
    
    <?php
						
						} 


						else if (in_array($statusID,$caseStatusDoneArray)) { 
							
	?>
    						
                               
                            <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                               
                               <div class="user-block">
                                                                       
                                    <?php if ($caseStatusCreatedEmployeePhoto && $photoFlag) { ?>
                      
                                       <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseStatusCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)"> <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt=""></a>
                                  
                                       <?php } else { ?>
                                      
                                            <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt="">
                                  
                                      <?php } ?>
                                
                                        <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                  
                                               <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                        
                                                      <span class="label" style="background-color:#7dab2e">done</span> the case status 
                        
                                               <?php if ($caseStatusComments) { ?>
                                       
                                              		 with following comments
                                              		 <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                              
                                       		  <?php } ?>
                                     
                                     </span>
                                     
                                     <span class="description" style="margin-bottom:0px; font-size:11px;"> <?php echo $statusCreatedTimeAgo; ?></span>
                 		           
                                   </div>
                                 </div>


                                 <!-- doctor history start here -->
                                 <?php if ($productionStageID == WAITING_FOR_APPROVAL) { ?>

                                 	<?php if($doctorCaseHistory->num_rows() > 0) {

                                 		foreach($doctorCaseHistory->result() as $DcHistory) {
										/*$tableID		  	 		  = $DcHistory->tableID;*/
										$caseLink		  = $DcHistory->caseLink;
										$caseLink		  = str_replace('https://','',$caseLink);
										$caseLink		  = 'https://'.$caseLink;

										$caseCode	  = $DcHistory->caseCode;
										$caseStatusID	  = $DcHistory->caseStatusID;
										$caseComment	  = $DcHistory->caseComment;
										$doctorCaseStatusNames	  = $DcHistory->doctorCaseStatusName;
										if($doctorCaseStatusNames=='Pending')
										{
											$doctorCaseStatusName='Assign';
										}
										else
										{
											$doctorCaseStatusName=$doctorCaseStatusNames;
										}

										if(isset($caseComment))
										{
											$caseHistoryComment=$caseComment;
										}
										else
										{
											$caseHistoryComment='-----';
										}

										$doctorCaseHistoryCreated		  = $DcHistory->doctorCaseHistoryCreated;
										$doctorCaseHistoryCreatedBy	  = $DcHistory->doctorCaseHistoryCreatedBy;
										$CreatedName		 	  = $DcHistory->employeeName;
										$CreatedCode		  	  = $DcHistory->employeeCode;
										
										
										$caseHistoryCreatorAssignJobs	  = getEmployeeAssignJobs($doctorCaseHistoryCreatedBy); // Calling From HR Employees Helper
													
										if ($caseHistoryCreatorAssignJobs) {
									
												$caseHistoryCreatorAssignJobs 	= $caseHistoryCreatorAssignJobs->row_array();
												$caseHistoryCreatorAssignJobs	= $caseHistoryCreatorAssignJobs['jobPositionName'];
							
										} else {
								
												$caseHistoryCreatorAssignJobs 	= NULL;
										}	   		
						
										$doctorCaseHistoryCreatedDate			= date('d M. Y',$doctorCaseHistoryCreated);
										$doctorCaseHistoryCreatedTime			= date('h:i A',$doctorCaseHistoryCreated);
										$doctorCaseHistoryCreatedTimeAgo		= timeAgo($doctorCaseHistoryCreated).' ago';
										
						           ?>
					
				
    
								        <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                               
			                                  <div class="user-block">                                    
			                                    <?php if ($caseStatusCreatedEmployeePhoto && $photoFlag) { ?>
			                      
			                                       <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseStatusCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)"> <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt=""></a>
			                                  
			                                       <?php } else { ?>
			                                      
			                                            <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt="">
			                                  
			                                      <?php } ?>
			                                
			                                        <span class="username" style="font-size:13px; color:#000; font-weight:500;">
			                                  
			                                               <?php echo $CreatedName; ?> - <?php echo $CreatedCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
			                        								<?php
				                        								if($doctorCaseStatusName=='Assign')
				                        								{
				                        									?><span class="label" style="background-color:#427ef5">Assign</span> the case <?php
				                        								} 

				                        								if($doctorCaseStatusName=='Modified')
				                        								{
				                        									?><span class="label" style="background-color:#f2cc41;">Modified</span> the case with following comment 
				                        					
			                                              		            <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?= $caseHistoryComment ?>”</p>
				                        									<?php
				                        								}

				                        								if($doctorCaseStatusName=='Approved')
				                        								{
				                        									?><span class="label" style="background-color:#7dab2e">Approved</span> the case <?php
				                        								}
			                        								?>
			        

			                                       </span>

			                                     
			                                     <span class="description" style="margin-bottom:0px; font-size:11px;"> <?php echo $doctorCaseHistoryCreatedTimeAgo; ?></span>
			                 		           
			                                   </div>
			                              </div>
			                              
			                          <?php } } ?>

							    <?php } ?>
							    <!-- doctor history ends here  -->

                               
    
    <?php
								
						} else {
							
								if (in_array($statusID,$caseStatusModificationArray)) {
	?>
    									
                                        <div class="timeline-body">
                                             
                                              <span class="description-text text-black"> 
                                                 
                                                 
                                                  <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                          
                                                           <?php 
														   		
																$modificationLiability = NULL;
															
																if ($caseStatusModificationLiabilityClient == HARD_CODE_ID_YES && $caseStatusModificationLiabilityOperator == HARD_CODE_ID_YES) {
																		
																		$modificationLiability = ' (OPERATOR MISTAKE + NEW DEMAND)';
																
																} else if ($caseStatusModificationLiabilityClient == HARD_CODE_ID_YES && $caseStatusModificationLiabilityOperator == HARD_CODE_ID_NO) {
																		
																		$modificationLiability = ' (NEW DEMAND)';
																
																} else if ($caseStatusModificationLiabilityClient == HARD_CODE_ID_NO && $caseStatusModificationLiabilityOperator == HARD_CODE_ID_YES) {
																		
																		$modificationLiability = ' (OPERATOR MISTAKE)';
																}
														   ?>
                                                           
                                                           <span class="label" style="background-color:#7460ee;">modification<?php echo $modificationLiability; ?></span> the case status 
                                  
                                                   <?php if ($caseStatusComments) { ?>
                                                   
                                                           with following comments
                                                           <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                          
                                                    <?php } ?>
                                                    
                                                    <?php if ($caseStatusModificationLiabilityNote) { ?>
                                                   
                                                           <u>Liability Note:</u>
                                                           <p style="margin-left:0px; margin-top:10px;">“<?php echo $caseStatusModificationLiabilityNote; ?>”</p>
                                          
                                                    <?php } ?>
                                                 
                                                 </span>
                                            </div>
    
    <?php
								
								} 
							else if (in_array($statusID,$caseStatusRejectArray)) {
	?>							
    
                                            <div class="timeline-body">
                                             
                                              <span class="description-text text-black"> 
                                                 
                                                  <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                          
                                                           <span class="label label-danger">rejected</span> the case status 
                                  
                                                   <?php if ($caseStatusComments) { ?>
                                                   
                                                           with following comments
                                                           <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                          
                                                    <?php } ?>
                                                 
                                                 </span>
                                            </div>
    <?php
								 } else {
	?>
	
    										<div class="timeline-body">
                                             
                                              <span class="description-text text-black"> 
                                                 
                                                  <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                          
                                                           <span class="label label-danger">hold</span> the case status 
                                  
                                                   <?php if ($caseStatusComments) { ?>
                                                   
                                                           with following comments
                                                           <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                          
                                                    <?php } ?>
                                                 
                                                 </span>
                                            </div>
    
	<?php	 
								 }
						}
	?>
          
         
	 <?php } ?>
    		
    
		
  </div>
  
  
  
</li>

<?php	
  $statusCounter++;
	
	}
}

?>    


<?php	
$stageCounter++;

}
}
?>
                   
                    <li>
					  <i class="fa fa-clock-o bg-gray"></i>
					</li>
                   
				  </ul>
              </div>
              
           
            
                  <div class="tab-pane" id="status">
                   	
                     <form id="form-update-status" class="form-horizontal form-element col-12" method="post" action="<?php echo base_url(); ?>update-case-status/" autocomplete="off" onSubmit="return submitStatusForm();" enctype="multipart/form-data">
                        
                        <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>" />
                        <input type="hidden" name="production_stage_id" id="production_stage_id" value="<?php echo $presentProductionStageID; ?>" />
                        <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>" />
                        <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>" />
                        <input type="hidden" name="status_id" id="status_id" value="0" />
                        
                     <?php if ($presentStatusName) { ?>	
                      
                        <div class="form-group row" style="margin-top:10px;">
                          <label for="" class="col-sm-2 control-label">&nbsp;</label>
                          <div class="col-sm-10">
                           <h4>
                           <?php
                                if ($presentStatusParentID) { 
                                                                           
                                     echo '('.statusName($presentStatusParentID).') '; // Calling From Case Helper
                                }
                                
                                echo $presentStatusName; ?></h4>
                          </div>
                        </div>
                        
                      <?php } ?>
                        
                        
                        <div id="ajax-dropbox-case-status-data-content">
                     
                          <div class="form-group row">
                            <label for="" class="col-sm-2 control-label">Status:</label>
                            <div class="col-sm-10">
                              <select name="status" id="status" class="form-control" disabled>
                                   <option value="">Choose a status...</option>
                              </select>
                              <?php if (form_error('status')) { echo form_error('status'); } ?>
                            </div>
                          </div>
                     
                     	</div>
                     
                       <div id="ajax-dropbox-status-child-data-content"></div>
                       <br />
                    
               
                   <div class="form-group row">
                        <label for="" class="col-sm-2 control-label">Comments / Remarks:</label>
    
                        <div class="col-sm-10">
                          <textarea class="textarea" id="comments" name="comments" rows="3" cols="5" placeholder="Add a comment..." style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                     </div>   
               
                   <div class="form-group row">
                        <div class="ml-auto col-sm-10">
                       
                         <button type="submit" name="submit" value="search" class="btn btn-blue" id="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Save </button>
                         
                         <button type="button" class="btn btn-warning " onclick="resetStatusForm();">Reset</button>
                         &nbsp;<span id="form-status-error-message" class="help-block" style="color:#fc4b6c"></span>
                        </div>
                     </div>
                    
                 </form>
                 
                  </div>
             
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        
      </div>
    </section>
    
    <script language="javascript" type="text/javascript">
			
			function getStageStatus(stageID) {
											
										$('#ajax-dropbox-status-child-data-content').empty();
										
										$.ajax({
 															
											   type				: "POST",
											   url				: '<?php echo base_url(); ?>load-stage-status/',
											   dataType			: 'json',
											   data				: {
																  
																		  stageID		: stageID,
																  },
											   beforeSend: function( xhr ) {
											   },
											  
											  success: function (data) { 
														  
													  if(data.result) {
																  
														  $('#ajax-dropbox-case-status-data-content').html(data.HTML);
														
													  } else {
														  
														  var HTML = '<div class="form-group row"><label for="example-text-input" class="col-sm-2 col-form-label">Status</label><div class="col-sm-10"><select name="status" id="status" class="form-control select2" disabled><option value="">Choose a status...</option></select></div></div>';
														  
														  $('#ajax-dropbox-case-status-data-content').html(HTML);
														  
														 
														  
														  setCaseStatus();
													  }
											   }
										 }); // END AJAX FUNCTION
											
							}
			
			function getStatusChilds(statusID) {
											
										if (statusID) {
										
												$.ajax({
																	
													   type				: "POST",
													   url				: '<?php echo base_url(); ?>load-status-childs/',
													   dataType			: 'json',
													   data				: {
																		  
																				  statusID		: statusID,
																			},
													   beforeSend: function( xhr ) {
													   },
													  
													  success: function (data) { 
																  
															  if(data.result) {
																		  
																  $('#ajax-dropbox-status-child-data-content').html(data.HTML);
																   setCaseStatus();
															  
															  } else {
																  
																  var HTML = '';
																  
																  $('#ajax-dropbox-status-child-data-content').html(HTML);
																  setCaseStatus(statusID);
															  }
													   }
												 }); // END AJAX FUNCTION
										
										} else {
											   
											  var HTML = '';
											  
											  $('#ajax-dropbox-status-child-data-content').html(HTML);
											  setCaseStatus();
											
										}
											
							}
							
			function setCaseStatus(statusID=null) {
							   			
										if (statusID) {
												
												$('#status_id').val(statusID);	
										
										} else {
												
												$('#status_id').val(0);	
										}
						   }
			
			function getStageOperators(stageID) {
											
										$.ajax({
 															
											   type				: "POST",
											   url				: '<?php echo base_url(); ?>load-stage-operators/',
											   dataType			: 'json',
											   data				: {
																  
																		  stageID		: stageID,
																	},
											   beforeSend: function( xhr ) {
											   },
											  
											  success: function (data) { 
														  
													  if(data.result) {
																  
														  $('#ajax-dropbox-operators-data-content').html(data.HTML);
														  
														  $(".cls-operator option[value='<?php echo $presentCaseOperatorEmployeeID; ?>']").remove();
														  
													  
													  } else {
														  
														  var HTML = '<div class="form-group row"><label for="example-text-input" class="col-sm-2 col-form-label">Operator</label><div class="col-sm-10"><select name="operator" id="operator" class="form-control select2" disabled><option value="">Choose a operator...</option></select></div></div>';
														  
														  $('#ajax-dropbox-operators-data-content').html(HTML);
													  }
											   }
										 }); // END AJAX FUNCTION
											
							}
						   
			function resetStatusForm() {
						
					$('#ajax-dropbox-status-child-data-content').html('');
					$('#form-update-status').trigger("reset");
					 
					 $('#form-status-error-message').text('');
			}
			
			function resetOperatorForm() {
					
					$('#form-update-operator').trigger("reset");
					
			}
			
			function submitStatusForm() {
						
								var caseID		 = $('input[name="case_id"]').val();
								var caseStageID  = $('input[name="case_stage_id"]').val();
								var status 		 = $('input[name="status_id"]').val();
								var comments 	 = $('input[name="comments"]').val();
								
								if (caseID != null && caseStageID != null && status !=0) {
										
										return true;
									
								} else {
										
										$('#form-status-error-message').html('<i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;Please select case status.');
										return false;
								}
								
							}
							
			function submitOperatorForm() {
								
								var caseID		 		 = $('input[name="case_id"]').val();
								var caseStageID			 = $('input[name="case_stage_id"]').val();
								var caseStatusID 		 = $('input[name="case_status_id"]').val();
								var operator 			 = $('select[name="operator"]').val();
								
								if (caseID != null && caseStageID != null && caseStatusID != null && operator !='') {
										
										return true;
										
								} else {
										
										$('#form-operator-error-message').html('<i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;Please select case operator.');
										
										return false;
								}
								
							}
						   
			$(document).ready(function () {
					
					var stageID = <?php echo $presentProductionStageID; ?>;
					
					getStageStatus(stageID);
					
					getStageOperators(stageID);
					
					
					$('INPUT[type="file"]').change(function () {
  						
						  var ext = this.value.match(/\.(.+)$/)[1];
							  
							  ext = ext.toLowerCase();
									  
									  switch (ext) {
									  case 'jpg':
									  case 'JPG':
									  case 'jpeg':
									  case 'JPEG':
									  case 'png':
									  case 'PNG':
									  case 'gif':
									  case 'GIF':
										
										  $('#submit').attr('disabled', false);
										  $('#form-status-error-message').html('');
										  break;
									  
									  default:
										 
										  $('#form-status-error-message').html('<i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;Only image file type for upload diagram.');
										  this.value = '';
									  }

									 // restrict file size in 1 MB
 									 if((this.files[0].size)/1024 > 1000) {
        								  $('#form-status-error-message').html('<i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;Please check upload diagram image not over 1MB.');
       									 this.value = '';
  									 }

					});
					
					 //called when key is pressed in textbox
					  $(".allow-numbers").keypress(function (e) {
						 //if the letter is not digit then display error and don't type anything
						 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
							//display error message
							return false;
						}
					   });
			});
			
    
    </script>  
    
    
    <script type="text/javascript">
			
			var fileReader = new FileReader();
			var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

			fileReader.onload = function (event) {
  			var image = new Image();
  
					  image.onload=function(){
					  
					  document.getElementById("treatment_diagram").src=image.src;
					  
					  var canvas=document.createElement("canvas");
					  var context=canvas.getContext("2d");
					 
					 canvas.width=image.width/1.5;
					 canvas.height=image.height/1.5;
					 context.drawImage(image,
					 0,
					 0,
					 image.width,
					 image.height,
					 0,
					 0,
					 canvas.width,
					 canvas.height
				 );
				
				document.getElementById("upload-Preview").src = canvas.toDataURL();
			}
 			
			image.src=event.target.result;
 		};

			  var loadImageFile = function () {
			  var uploadImage = document.getElementById("treatment_diagram");
	
			  $('.diagram-preview').show();
		  
			  //check and retuns the length of uploded file.
			  if (uploadImage.files.length === 0) { 
				return; 
			  }
	
			  //Is Used for validate a valid file.
			  var uploadFile = document.getElementById("treatment_diagram").files[0];
		  
			  if (!filterType.test(uploadFile.type)) {
		   
				  $('.diagram-preview').hide();
				  return;
			   }
	
			  fileReader.readAsDataURL(uploadFile);
	  }
</script>
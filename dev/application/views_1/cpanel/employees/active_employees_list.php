<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Employees</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           
           <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
           
           
          <div class="box">
            <div class="box-header">
              
              <?php $totalEmployees = $employees->num_rows(); ?>
            
              <h3 class="box-title">Employees <small>(<?php echo $totalEmployees; ?>)</small></h3>
		      <button onclick="window.location.href='<?php echo base_url(); ?>employee-add/'"type="button" class="btn btn-blue pull-right"><i class="fa fa-plus"></i> Add Employee</button>
                <div class="box-tools">
                  <?php echo $pagingLink;?>
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="example" class="table table-responsive" style="width:100%">
                <thead>
                <tr>
                  <th width="44">Sr.</th>
                  <th width="54">Code</th>
                  <th width="192">Name</th>
                  <th width="164">D. O. J</th>
                  <th width="305">Official Email</th>
                  <th width="179">Mobile</th>
                  <th width="90">&nbsp;</th>
                </tr>
              </thead>
               <tbody>
               
               <?php if ($totalEmployees) {
                        
						$counter = $serialNumber;
						foreach($employees->result() as $employee) {
			  								
											$tableID 								=   encodeString($employee->tableID); // Calling From General Helper
											$employeeID								=	$employee->employeeID;
											$employeeCode							=	$employee->employeeCode;
											$employeeName							=	$employee->employeeName;
											$employeeEmail							=	$employee->employeeEmail;
											$employeeMobile							=	$employee->employeeMobile;
											$employeeMobileFormatted				=	$employee->employeeMobileFormatted;
											
											$employeeJoiningDate					=	applicationDateFormat(convertStrToTime($employee->employeeJoiningDate)); // Calling From Application + General Helper
											
											$employeeReJoiningDate					= 	$employee->employeeReJoiningDate;
											
											if ($employeeReJoiningDate) {
												
												 $employeeReJoiningDate				=	applicationDateFormat(convertStrToTime($employee->employeeReJoiningDate)); // Calling From Application + General Helper
												 $employeeJoiningDate 				=   $employeeReJoiningDate;
											
											} 
											
											$employeePhoto   						=   $employee->employeePhoto;
											
											$employeeAssignJobs						=   getEmployeeAssignJobs($employeeID); // Calling From Employee Helper
                                            
											if ($employeeAssignJobs) {
                                      
                                                 $employeeAssignJob 				=   $employeeAssignJobs->row_array();
                                                 $employeeAssignDepartment			=   $employeeAssignJob['departmentName'];
                                                 $employeeAssignJobPosition			=   $employeeAssignJob['jobPositionName'];
                                                
                                             } else {
                                  
                                                 $employeeAssignDepartment 			= 	'No department assign';
                                                 $employeeAssignJobPosition			= 	'No position assign';
                                             }
                            ?>
                                    
                                    <tr>
                                    <td><?php echo $counter; ?></td>
                                    <td><?php echo $employeeCode; ?></td>
                                    <td><?php echo $employeeName; ?></td>
                                    <td><?php echo $employeeJoiningDate; ?></td>
                                    <td><?php echo $employeeEmail; ?></td>
                                    <td><?php echo $employeeMobileFormatted; ?></td>
                                    <td><div class="pull-right"><a href="<?php echo base_url(); ?>employee-edit/<?php echo $tableID; ?>/" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
                                    	<a href="<?php echo base_url(); ?>employee-remove/<?php echo $tableID;?>/" onclick="return confirm('Are you sure you want to remove employee <?php echo $employeeName.' ('.$employeeCode.')'; ?> ?');" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                    
                    
                    <?php $counter++;
					
						 }
					  }
				?>
               
               
               </tbody>
                  <tfoot>
                      <tr>
                        <th width="44">Sr.</th>
                        <th width="54">Code</th>
                        <th width="192">Name</th>
                        <th width="164">D. O. J</th>
                        <th width="305">Official Email</th>
                        <th width="179">Mobile</th>
                        <!-- <th width="90">&nbsp;</th> -->
                      </tr>
                  </tfoot>
              </table>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
</script>
   
   
   
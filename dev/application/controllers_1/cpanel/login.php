<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');  
error_reporting(0);

class Login extends CI_Controller {

	 public function __construct() {
        
		parent::__construct();
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
	}
	
	function index() { 	
		
				 /*Check login function used for if user already loggedIn this function / page can not be called after logged in. */
				 checkLogin();  // Calling From Login Helper
				 
				  /*Distroy Session Due To Show Updated Member Profile Image , Name ,  Email From Database ID Member Enter Currect Email Only */
				  distroySessions(); // Calling From Application Helper
		
		          $defaultRedirect     = 'cpanel/';
				  $foreceToRedirect    = $this->input->get_post('redirect');
	
				  /* Set Form Validation Errors */ 
				 $this->form_validation->set_rules('signin_email','email or phone','trim|required|callback_check_member_exist');
				 $this->form_validation->set_rules('signin_password','password','trim|required');
				  
				  
				  $this->form_validation->set_error_delimiters('<div class="error-block">', '</div>');
				  
					  if($this->form_validation->run() === FALSE ) {
						  
						
							  if ($foreceToRedirect) {
						  
								   $data['redirect'] = decodeString($foreceToRedirect);  // Calling From General Helper
						  
							  } else {
						  
								  $data['redirect']  = $defaultRedirect;
							  }
		  
						 	$this->load->view('cpanel/login',$data);
		  
					  } else {
						  
						  $flag = false;
						  
						  $redirectPath = $this->input->get_post('redirect');
						  
						  if ($redirectPath) {
							
							  $redirect = decodeString($redirectPath);  // Calling From General Helper 
						  
						  } else {
							
							   $redirect = $defaultRedirect;
						  }
						  
						    /*
								Count Lengh to Check If Try To Login With Employee Code Number
								  If Login With Code Dont Remove Plus + Sign and Country Code 92
							*/
		 					 $inputLength  = strlen($this->input->post('signin_email'));
		 					 $simpleEmail=$this->input->post('signin_email'); 
		 
		 					 if ($inputLength >= 5) {
						  
						    	$email 		=  cleanPhonePlusSign(removeAllSpacesFromString($this->input->post('signin_email')));  // Calling From General Helper
		 					 	$email 		=  cleanPhoneStartingZero($email); // Calling From General Helper
							 
							 } else {
								
								$email 		= $this->input->post('signin_email');
							 }
						  
						  	 $password		= md5(md5($this->input->post('signin_password')));

						  	 $userEmail     =  $this->input->post('signin_email');

							  // to check if user is normal user or patient
							   $result_row   =  $this->model_shared->getRecordMultipleWhere('type',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('email' => $userEmail, 'is_deleted' => HARD_CODE_ID_NO))->row_array();
							   if($result_row)
							   {
							   	   $type         = $result_row['type'];
							   }
							   if($type==HARD_CODE_ID_USER_TYPE_PATIENT)
							   {
							   		$userInfo = $this->model_login->chk_member_login_for_patient($email,$password);
							   }
							   else
							   {
							   		$userInfo = $this->model_login->chk_member_login($email,$password);
							   }	
							
							 			
										
						 				if ($userInfo)  {
							  
								 				 $userInfo = $userInfo->row_array();
								
								 						if ($this->makeUserLogin($userInfo['accountID'])) {
										 
															// $flag = true;
															//$this->session->set_userdata('promo','mobile');
														}
										} 
						
											if ($flag) {
								 
								  						redirect(base_url().$redirect);
							
												} else {
								 
								  						redirect('my-dashboard/');
											    }
					   				    }
	}
		
	private function check_input_email_or_phone($input) {
		
				$input = removeAllSpacesFromString($input);  // Calling From General Helper
				
				 if (is_numeric ($input)) {
            				 
							 return "number";
   			 	 } else {
             
            			$checkEmailValidation = $this->check_email_validation($input);
						
						if ($checkEmailValidation) {
						
								return 'valid_email';	
						
						} else {
								
								/*return 'characters'; */

								//change by fahad
								return 'valid_email';
						 }
     			 }
	
	}
	
	private function check_email_validation($x) {
				
						 if (strpos($x, "@") !== false) {
							   
							   $split = explode("@", $x);
							   if (strpos($split['1'], ".") !== false) { 
									
									return true;//'Is an email';
									
								} else { 
									return false;
									//'invalid email';
								}
						 } else { 
									return false;
							  //'invalid email'; 
						 }
	}
	
	
	public  function check_member_exist() {
		  
		  distroySessions(); // Calling From Application Helper
		  
		  /*
		  	  Count Lengh to Check If Try To Login With Employee Code Number
		  		If Login With Code Dont Remove Plus + Sign and Country Code 92
		  */
		  $inputLength  = strlen($this->input->post('signin_email'));
		  $simpleEmail=$this->input->post('signin_email'); 
		 
		  if ($inputLength >= 5) {
			
				 $email =  cleanPhonePlusSign(removeAllSpacesFromString($this->input->post('signin_email'))); // Calling From General Helper
		 
		  		 $email  =  cleanPhoneStartingZero($email); // Calling From General Helper
		 
		 } else {
				
				$email = $this->input->post('signin_email');
		 }
		
		 $inputEmail  =  $this->check_input_email_or_phone($email);
		
		  if ($inputEmail == 'characters') {
			
				$this->form_validation->set_message('check_member_exist',WRONG_EMAIL_FORMAT);
				return false;	
			  
		  } elseif ($inputEmail == 'number' || $inputEmail == 'valid_email')  {
			  
			  if ($this->input->post('signin_password') == '')  {
				    
					$this->form_validation->set_message('check_member_exist','');
					return false;	
			   }
			   
			   
				  $password	     =  md5(md5(removeAllSpacesFromString($this->input->post('signin_password'))));   // Calling From General Helper

				  $userEmail     =  $this->input->post('signin_email');

				  // to check if user is normal user or patient
				   $result_row   =  $this->model_shared->getRecordMultipleWhere('type',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('email' => $userEmail, 'is_deleted' => HARD_CODE_ID_NO))->row_array();
				   if($result_row)
				   {
				   	   $type         = $result_row['type'];
				   }
				   if($type==HARD_CODE_ID_USER_TYPE_PATIENT)
				   {
				   		$result 		 =  $this->model_login->chk_member_login_for_patient($email,$password);
				   }
				   else
				   {
				   		$result 		 =  $this->model_login->chk_member_login($email,$password);
				   }
				   	
		  	  	  
				  
		
				    if ($result)  {
					
								$row = $result->row_array();
									
											if ($inputEmail == 'number' && $row['temMemberMobileVerifiedStatus'] == HARD_CODE_ID_NOT_VERIFIED) {
								
													$this->form_validation->set_message('check_member_exist',MOBILE_AUTH_FAILED);
													return false;
							
							 				 } else if ($row['accountStatus'] == HARD_CODE_ID_BLOCKED) { 
											 		
																   $memberData = array (
	  
																		  USER_NAME_SESSION        		 => $row['teamMemberName'],
																		  USER_PHOTO_SESSION        	 => $row['teamMemberPhoto'],
																		  USER_LOGIN_SESSION     	     => FALSE,
																	  );

																	$this->session->set_userdata($memberData);
									
													$this->form_validation->set_message('check_member_exist',BLOCKED_ACCOUNT);
													return false;
											 
											 } else if ($row['accountStatus'] == HARD_CODE_ID_INACTIVE) {
												 
																	 $memberData = array (
	  
																										USER_NAME_SESSION        		 => $row['teamMemberName'],
																										USER_PHOTO_SESSION       	 	 => $row['teamMemberPhoto'],
																										USER_LOGIN_SESSION     	    	 => FALSE,
																	  								  );

																	  $this->session->set_userdata($memberData);
									
													$this->form_validation->set_message('check_member_exist',INACTIVE_ACCOUNT);
													return false;
											 
											 } else if ($row['accountStatus'] == HARD_CODE_ID_ACTIVATED) {
				
													return true;
							 				
								 			 } else {
											
													$this->form_validation->set_message('check_member_exist','Unknown Error.');
													return false;
								 			}
		
						} else {
							
							$cpanelUser	= $this->model_login->chk_member_email($email);
						
									if ($cpanelUser)  {
										
											$cpanelUser	=  $cpanelUser->row_array();
											
																 $memberData = array (
																										USER_NAME_SESSION        	 => $cpanelUser['teamMemberName'],
																										USER_PHOTO_SESSION        	 => $cpanelUser['teamMemberPhoto'],
									 																	USER_LOGIN_SESSION     	     => FALSE,
								  																	);
					
																									$this->session->set_userdata($memberData);
											
												
																									$this->form_validation->set_message('check_member_exist','Incorrect password');
																									return false;	
								} else {
											
											if ($inputEmail == 'number') {
													
													$errorMessage = 'phone';
															
														if ($inputLength <= 5) {
															
																$errorMessage  = 'code';
														}
														
											} elseif ($inputEmail == 'valid_email') {
													$errorMessage  = 'email';
											} 
				
											$this->form_validation->set_message('check_member_exist','The '.$errorMessage.' or password you entered is incorrect.');
											return false;	
					
							}
				  }
		  }
	}
	
	private function makeUserLogin($accountID) {
	
				$result =  $this->model_login->getAccountInfo($accountID);
				
					if ($result) { 
										
					$memberData = array(
									 					USER_ACCOUNT_ID_SESSION          			  => $result['accountID'],
									 					USER_TEAM_ID_SESSION          				  => $result['teamID'],
														USER_HR_ID_SESSION          				  => $result['teamMemberHRID'],
									 					USER_ORGANIZATION_ID_SESSION          		  => $result['teamMemberOrganizationID'],
									 					USER_ACCOUNT_STATUS_SESSION     		  	  => $result['accountStatus'],
									 					USER_NAME_SESSION        	 			  	  => $result['temMemberName'],
														USER_PHOTO_SESSION        	 			  	  => $result['temMemberPhoto'],
									 					USER_LOGIN_SESSION     	     			 	  => TRUE,
								 					 );
					
						$this->session->set_userdata($memberData);
				
						return true;
					
					} else {
					
						return false;	
					}
					
	}
	
	public  function signOut() {
		
				distroySessions(); // Calling From Application Helper
				
				redirect('cpanel/');
	}
	
	function operatorNotFound() { 
		
		$data['activeMenu']  			= '';
		$data['activeSubMenu']  		= '';
		
		$data['pageHeading']    		= "Operator Not Found";
		$data['subHeading']    			= "Control panel";
		
		$data['metaType']     			= 'internal';
		$data['pageName']    			= 'Operator Not Found';
		$data['pageTitle']      		= 'Operator Not Found | '.DEFAULT_APPLICATION_NAME;
		
		$this->load->view('cpanel/operator_not_found',$data,false);
	}
	
	function page404() { 
		
		$this->load->view('cpanel/404');
		
	}
	
}
?>
<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');

class apiController extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		$this->load->model('cpanel/cases/model_shared');



	}

	

	function missingCases() 
	{
		
		/*$record  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_CASES_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED));*/
		$record  = $this->model_shared->getRecord_SecondDB_MultipleWhere('*',MISSING_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO));

		/*$count  = $record->num_rows();*/

		/*if ($count > 0) 
		{*/
			foreach($record->result() as $row) 
			{
				$caseID                                           = $row->case_id;
				$missing_cases_data['missing_case_table_id']      = $row->id;
				$missing_cases_data['case_id']                    = $row->case_id;
				$missing_cases_data['tracking_number_sending']    = $row->tracking_number_sending;
				$missing_cases_data['delivery_service_sending']   = $row->delivery_service_sending;
				$missing_cases_data['tracking_number_receiving']  = $row->tracking_number_receiving;
				$missing_cases_data['delivery_service_receiving'] = $row->delivery_service_receiving;
				$missing_cases_data['status']                     = $row->status;
				$missing_cases_data['description']                = $row->description;
				$missing_cases_data['is_deletable']               = $row->is_deletable;
				$missing_cases_data['created']                    = DATABASE_NOW_DATE_TIME_FORMAT();
				$missing_cases_data['created_at']                 = $row->created;
				$missing_cases_data['created_by']                 = $row->created_by;
				$missing_cases_data['created_by_reference_table'] = $row->created_by_reference_table;
				$missing_cases_data['is_deleted']                 = $row->is_deleted;
				$missing_cases_data['deleted_by']                 = $row->deleted_by;
				$missing_cases_data['deleted_by_reference_table'] = $row->deleted_by_reference_table;

				$checkPresentMissingCases 	                      = $this->model_shared->getRecordMultipleWhere('id',MISSING_CASES_TABLE,array('case_id' => $caseID));

				if ($checkPresentMissingCases->num_rows() > 0) 
				{
				    $missingCasesCasesID 	= $this->model_shared->editRecordWhere(array('case_id' => $caseID),MISSING_CASES_TABLE,$missing_cases_data);
				}

				else
				{
				    $missingCasesCasesID						  = 	$this->model_shared->insertRecord_ReturnID(MISSING_CASES_TABLE,$missing_cases_data);
				}
			}

			if($missingCasesCasesID)
			{
				echo "missing cases api successfully run";
			}		

    }



    function missingCasesHistory() 
	{
		$page                                        = 0;
		$recordperpage						         = 1;
		$result	   				                     = $this->model_shared->page_listing($page,$recordperpage,'missing_cases_history_table_id',MISSING_CASES_HISTORY_TABLE,'id','DESC');
		$result_row	                                 = $result->row_array();

		$missing_cases_history_table_id_count		 = $result->num_rows();
		if($missing_cases_history_table_id_count > 0)	
		{
			$latest_missing_cases_history_table_id   = $result_row['missing_cases_history_table_id'];
		}
		else
		{
			$latest_missing_cases_history_table_id           = 0;
		}
		

		$record  = $this->model_shared->getRecord_SecondDB_MultipleWhere('*',MISSING_CASES_HISTORY_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id >' => $latest_missing_cases_history_table_id));
		$count   = $record->num_rows();

		if ($count > 0) 
		{
			foreach($record->result() as $row) 
			{

				$cases_history_data['missing_cases_history_table_id']  = $row->id;
				$cases_history_data['case_id']                    = $row->case_id;
				$cases_history_data['tracking_number_sending']    = $row->tracking_number_sending;
				$cases_history_data['delivery_service_sending']   = $row->delivery_service_sending;
				$cases_history_data['tracking_number_receiving']  = $row->tracking_number_receiving;
				$cases_history_data['delivery_service_receiving'] = $row->delivery_service_receiving;
				$cases_history_data['status']                     = $row->status;
				$cases_history_data['description']                = $row->description;
				$cases_history_data['is_deletable']               = $row->is_deletable;
				$cases_history_data['created']                    = DATABASE_NOW_DATE_TIME_FORMAT();
				$cases_history_data['created_at']                 = $row->created;
				$cases_history_data['created_by']                 = $row->created_by;
				$cases_history_data['created_by_reference_table'] = $row->created_by_reference_table;
				$cases_history_data['is_deleted']                 = $row->is_deleted;
				$cases_history_data['deleted_by']                 = $row->deleted_by;
				$cases_history_data['deleted_by_reference_table'] = $row->deleted_by_reference_table;

				// Save All data Into tbl_application_inventory_cases
				$scases_historyTableID					          = 	$this->model_shared->insertRecord_ReturnID(MISSING_CASES_HISTORY_TABLE,$cases_history_data);
				
			}
			
		}


		echo "missing cases history api successfully run";

	}


	function casesPictures()
	{
		$notPushed      = HARD_CODE_ID_NOT_PUSHED;
		$notDeleted     = HARD_CODE_ID_NO;
		$where="push_status ='$notPushed' AND public_url!='' AND is_deleted='$notDeleted' ";
		$picture_query  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASE_PICTURES_TABLE,$where);

		foreach($picture_query->result() as $record_row)
		{
			$patient_pictures_data['case_id']                        = $record_row->case_id;
			$patient_pictures_data['name']                           = $record_row->name;
			$patient_pictures_data['type']                           = $record_row->type;
			$patient_pictures_data['public_url']                     = $record_row->public_url;
			$patient_pictures_data['created']                        = DATABASE_NOW_DATE_TIME_FORMAT();
			$patient_pictures_data['created_by']                     = $record_row->created_by;
			$patient_pictures_data['created_by_reference_table']     = $record_row->created_by_reference_table;
			// Save All data Into tbl_application_medical_cases_temporary_inventory_record
			$patientPicturesID									     = 	$this->model_shared->insertRecord_SecondDB_ReturnID(ALIGNERCO_CASES_PICTURES_TABLE,$patient_pictures_data);

			if($patientPicturesID OR $patientPicturesID!='')
			{
				$update_data['push_status']                          = HARD_CODE_ID_PUSHED;
				$editWhere="push_status ='$notPushed' AND public_url!='' AND is_deleted='$notDeleted' ";
				$this->model_shared->editRecordWhere($editWhere,MEDICAL_CASE_PICTURES_TABLE,$update_data);
			}
			 
		}

		echo "cases pictures api successfully run";
	}



	function patientCasesPictures()
	{
		$notPushed      = HARD_CODE_ID_NOT_PUSHED;
		$notDeleted     = HARD_CODE_ID_NO;
		$where="push_status ='$notPushed' AND case_id!='' AND is_deleted='$notDeleted' ";
		$patient_picture_query  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASE_PATIENT_PICTURES_TABLE,$where);

		foreach($patient_picture_query->result() as $record_row)
		{
			$patient_pictures_data['case_id']                        = $record_row->case_id;
			$patient_pictures_data['name']                           = $record_row->name;
			$patient_pictures_data['type']                           = $record_row->type;
			$patient_pictures_data['public_url']                     = $record_row->public_url;
			$patient_pictures_data['created']                        = DATABASE_NOW_DATE_TIME_FORMAT();
			$patient_pictures_data['created_by']                     = $record_row->created_by;
			$patient_pictures_data['created_by_reference_table']     = $record_row->created_by_reference_table;
			// Save All data Into tbl_application_medical_cases_temporary_inventory_record
			$patientPicturesID									     = 	$this->model_shared->insertRecord_SecondDB_ReturnID(ALIGNERCO_CASES_PICTURES_TABLE,$patient_pictures_data);

			if($patientPicturesID OR $patientPicturesID!='')
			{
				$update_data['push_status']                          = HARD_CODE_ID_PUSHED;
				$editWhere="push_status ='$notPushed' AND case_id!='' AND is_deleted='$notDeleted' ";
				$this->model_shared->editRecordWhere($editWhere,MEDICAL_CASE_PATIENT_PICTURES_TABLE,$update_data);

				
			}

		}
		
		echo "patient cases pictures api successfully run";
	}



	





}
?>

<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 
 
class Setups extends CI_Controller {

	  public function __construct() {
		
		parent::__construct();
		
		/* Table ID*/
		$this->accountID = $this->session->userdata(USER_ACCOUNT_ID_SESSION);
		
		/*Team Member ID*/
		$this->teamID   = $this->session->userdata(USER_TEAM_ID_SESSION);
		
		/*Team Member ID*/
		$this->HRID   = $this->session->userdata(USER_HR_ID_SESSION);
		
		/*Team Member ID*/
		$this->organizationID	= $this->session->userdata(USER_ORGANIZATION_ID_SESSION);
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		$this->load->model('cpanel/setups/model_setup');
		
		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		
		authentication(); // Calling From Login Helper 
		
		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper
		
	}
	
	function stages($page=0) {
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$recordperpage						= 200;
		
		$result['assignedRoles']			=  $assignedRoles;
		$result['accessModules']			=  $accessModules;
		
		
		$result['result']	   				= $this->model_shared->page_listing_where($page,$recordperpage,'*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'parent' => HARD_CODE_ID_PARENT_OR_INDEPENDENT),'order','ASC');
			
		$mypaing['total_rows']			 	= $this->model_shared->page_listing_num_where('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'parent' => HARD_CODE_ID_PARENT_OR_INDEPENDENT));
		
		$result['totalRows']				= $mypaing['total_rows'];
		
		$mypaing['base_url'] 				= base_url()."manage-stages/";
		$mypaing['per_page']				= $recordperpage;
		$mypaing['uri_segment']				= 2;	
		
		$mypaing['full_tag_open'] 			= "<ul class='pagination pagination-sm no-margin pull-right d-none d-sm-flex'>";
		$mypaing['full_tag_close'] 			= '</ul>';
		$mypaing['num_tag_open'] 			= '<li>';
		$mypaing['num_tag_close'] 			= '</li>';
		$mypaing['cur_tag_open'] 			= '<li><a href="javascript:void(0);" class="current">';
		$mypaing['cur_tag_close'] 			= '</a></li>';
		$mypaing['prev_tag_open'] 			= '<li>';
		$mypaing['prev_tag_close']			= '</li>';
		$mypaing['first_tag_open'] 			= '<li>';
		$mypaing['first_tag_close'] 		= '</li>';
		$mypaing['last_tag_open'] 			= '<li>';
		$mypaing['last_tag_close'] 			= '</li>';
	
		$mypaing['prev_link'] 				= 'Previous';
		$mypaing['prev_tag_open']			= '<li>';
		$mypaing['prev_tag_close'] 			= '</li>';
	
	
		$mypaing['next_link'] 				= 'Next';
		$mypaing['next_tag_open'] 			= '<li>';
		$mypaing['next_tag_close'] 			= '</li>';
 		
		$this->pagination->initialize($mypaing);
		
		$result['paginglink']				= $this->pagination->create_links();
		
		$data['userInfo']            		= $userInfo;
		
		$data['activeMenu']  				= '5';
		$data['activeSubMenu']  			= '5.1';
		
		$data['pageHeading']    			= "<i class='fa fa-delicious'></i> Production Stages";
		$data['subHeading']    				= "";
		
		$data['metaType']     				= 'internal';
		$data['pageTitle']      			= 'Production Stages | '.DEFAULT_APPLICATION_NAME;
		
		$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/stage_listing',$result,true);	
		$this->load->view('cpanel/template',$data);
		
	}
	
	function addStage() { 
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('name','name','trim|required|callback_check_already_exist_stage_name');
		$this->form_validation->set_rules('description','description','trim');
		
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');
		
			if($this->form_validation->run() === FALSE ) {
				
				$result['assignedRoles']			= $assignedRoles;
				$result['accessModules']			= $accessModules;
				
				
				$data['userInfo']            		= $userInfo;
		
				$data['activeMenu']  				= '5';
				$data['activeSubMenu']  			= '5.1';
				
				$data['pageHeading']    			= "<i class='fa fa-delicious'></i> Production Room";
				$data['subHeading']    				= "";
				
				$data['metaType']     				= 'internal';
				$data['pageTitle']      			= 'Production Rooms | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/stage_add',$result,true);	
				$this->load->view('cpanel/template',$data);
			
			} else {
				
								// Prepair Date To Store In Database
								$name								=	($this->input->post('name') ?: NULL);
								$description				    	=	($this->input->post('description') ?: NULL);
								$isDeleteAble						=	HARD_CODE_ID_YES;
							
							 // Check Sef Already Exist Or Not
							 if ($this->check_already_exist_SefURL_stage_name(SEF_URLS($name))) {
									
									 //IF Sef Already Exist Then Concatenate A Unique Number  
									 $sefURL	=	SEF_URLS_CONCAT($name); 	 // Calling From Shared Helper
						
							 } else {
									
									 //IF Sef Not Exist Then Leave It To Orignal Name				
									 $sefURL	=	SEF_URLS($name); // Calling From Shared Helper
							 }
				
									$data['name']								 =	$name;
									$data['sef_url']						     =	$sefURL; 
									$data['description']						 =	$description;
									$data['parent']								 =	HARD_CODE_ID_PARENT_OR_INDEPENDENT;
									
									$data['is_deletable']						 =	$isDeleteAble;
									
									$data['created']							 =  DATABASE_NOW_DATE_TIME_FORMAT(); 
									$data['created_by']							 =  $teamID;
									$data['created_by_reference_table']			 =  'MY_ORGANIZATION_TEAM_TABLE';
									// Save All data Into Database Table
									$inertedID									 = 	$this->model_shared->insertRecord_ReturnID(MEDICAL_PRODUCTION_STAGES_TABLE,$data);
				
									// Set Session Message If Everything Goes Fine
									$this->session->set_userdata('admin_msg',RECORD_ADDED);
									
									 if ($this->input->post('submit') == 'Save & New') {
										    redirect('add-stage/');
									 } else {
											redirect('manage-stages/');
									}
			}
	}
	
	function editStage($sefURL) { 
		
		checkEmptySefURL($sefURL);  // Calling From Shared Helper
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $sefURL));
		
		if ($record->num_rows() == 0 ) {
				
				redirect('manage-stages/');	
		}
		
		$row	 = $record->row_array();
		$ID		 = $row['id'];
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('name','name','trim|required|callback_check_exist_edit_stage_name');
		$this->form_validation->set_rules('description','nescription','trim');
		
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');
			
			if($this->form_validation->run() === FALSE ) {
								
				$result['assignedRoles']			= $assignedRoles;
				$result['accessModules']			= $accessModules;
				
				$result['row']						= $row;
				
				$data['userInfo']            		= $userInfo;
		
				$data['activeMenu']  				= '5';
				$data['activeSubMenu']  			= '5.1';
				
				$data['pageHeading']    			= "<i class='fa fa-delicious'></i> Production Room";
				$data['subHeading']    				= "";
				
				$data['metaType']     				= 'internal';
				$data['pageTitle']      			= 'Production Rooms | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/stage_edit',$result,true);	
				$this->load->view('cpanel/template',$data);
			
			} else {
			
								// Prepair Date To Store In Database
								$name								=	($this->input->post('name') ?: NULL);
								$description				    	=	($this->input->post('description') ?: NULL);
							
							if (convertAllCharactersLowercase($name) != convertAllCharactersLowercase($row['name'])) {
							
										   // Check Sef Already Exist Or Not
										   if ($this->check_already_exist_SefURL_stage_name(SEF_URLS($name))) {
												  
												   //IF Sef Already Exist Then Concatenate A Unique Number  
												   $sefURL						=	SEF_URLS_CONCAT($name); 	 // Calling From Shared Helper
												   $data['sef_url']				=	$sefURL; 
									  
										   } else {
												  
												   //IF Sef Not Exist Then Leave It To Orignal Name				
												   $sefURL						=	SEF_URLS($name); // Calling From Shared Helper
												   $data['sef_url']				=	$sefURL; 
										   }
							 }
				
				
											$data['name']						=	$name;
											$data['description']				=	$description;
											
											// Update Record Table
											$this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRODUCTION_STAGES_TABLE,$data);
											
											// Set Session Message If Everything Goes Fine
											$this->session->set_userdata('admin_msg',RECORD_UPDATED);
											redirect('manage-stages/');
			}
	}
	
	function deleteStage($sefURL) {
		
		checkEmptySefURL($sefURL);  // Calling From Shared Helper
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
					    
		$result = $this->model_shared->getRecord('*',MEDICAL_PRODUCTION_STAGES_TABLE,'sef_url',$sefURL);
		
		if ($result->num_rows == 0) {
				
				$this->session->set_userdata('admin_msg_error',RECORD_NOT_FOUND);
				redirect('manage-stages/');	
		
		} else {
		
					$row = $result->row_array();
					$ID  = $row['id'];
					
					$child  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'parent' => $ID));
		
					if ($child->num_rows > 0) {
							
							$this->session->set_userdata('admin_msg_error','Stage \''.$row['name'].'\' can\'t be deleted. Please first delete its childs.');
							redirect('manage-stages/');	
					}
		
					if ($row['is_deletable'] == HARD_CODE_ID_NO) {
						
						$this->session->set_userdata('admin_msg_error','Stage \''.$row['name'].'\' can\'t be deleted.');
						
						redirect('manage-stages/');
							
					} else {
						
							  // Soft Delete From Database Update Record IsDeleted = TRUE 
							  $update['is_deleted'] 											= HARD_CODE_ID_YES;
							  $update['deleted_by'] 											= $teamID;
							  $update['deleted_by_reference_table'] 							= 'MY_ORGANIZATION_TEAM_TABLE';
							  $update['deleted']												= DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
					  
							  $this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRODUCTION_STAGES_TABLE,$update);
							  $this->session->set_userdata('admin_msg',RECORD_SOFT_DELETED);
							 
							  redirect('manage-stages/');

					}
		}
				
	}
	
	function moreStages($parentSefURL,$page=0) {
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptyID($parentSefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url'=> $parentSefURL));
		
		if ($record->num_rows() == 0 ) {
				
				redirect('manage-stages/');	
		}
		
		$row								= $record->row_array();
		$parentID							= $row['id'];
		
		$result['assignedRoles']			= $assignedRoles;
		$result['accessModules']			= $accessModules;
		
		$result['parentID']     			= $parentID;
		$result['parentSefURL']     		= $parentSefURL;
		
		$recordperpage						= 200;
		
		$result['result']	   				= $this->model_shared->page_listing_where($page,$recordperpage,'*',MEDICAL_PRODUCTION_STAGES_TABLE,array('parent' => $parentID,'is_deleted' => HARD_CODE_ID_NO),'order','ASC');

		$totalRows			 	 			= $this->model_shared->page_listing_num_where('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('parent' => $parentID,'is_deleted' => HARD_CODE_ID_NO));
		
		$result['totalRows']				= $totalRows;	
		
		$mypaing['total_rows']				= $totalRows;
		
		$mypaing['base_url'] 				= base_url()."manage-stages/";
		$mypaing['per_page']				= $recordperpage;
		$mypaing['uri_segment']				= 2;	
		
		$mypaing['full_tag_open'] 			= "<ul class='pagination pagination-sm no-margin pull-right d-none d-sm-flex'>";
		$mypaing['full_tag_close'] 			= '</ul>';
		$mypaing['num_tag_open'] 			= '<li>';
		$mypaing['num_tag_close'] 			= '</li>';
		$mypaing['cur_tag_open'] 			= '<li><a href="javascript:void(0);" class="current">';
		$mypaing['cur_tag_close'] 			= '</a></li>';
		$mypaing['prev_tag_open'] 			= '<li>';
		$mypaing['prev_tag_close']			= '</li>';
		$mypaing['first_tag_open'] 			= '<li>';
		$mypaing['first_tag_close'] 		= '</li>';
		$mypaing['last_tag_open'] 			= '<li>';
		$mypaing['last_tag_close'] 			= '</li>';
	
		$mypaing['prev_link'] 				= 'Previous';
		$mypaing['prev_tag_open']			= '<li>';
		$mypaing['prev_tag_close'] 			= '</li>';
	
	
		$mypaing['next_link'] 				= 'Next';
		$mypaing['next_tag_open'] 			= '<li>';
		$mypaing['next_tag_close'] 			= '</li>';
		
		$this->pagination->initialize($mypaing);
		
		$result['paginglink']				= $this->pagination->create_links();
		
		$data['userInfo']            		= $userInfo;
		
		$data['activeMenu']  				= '5';
		$data['activeSubMenu']  			= '5.1';
		
		$data['pageHeading']    			= "<i class='fa fa-delicious'></i> Production Stages - ".$row['name'];
		$data['subHeading']    				= "";
		
		$data['metaType']     				= 'internal';
		$data['pageTitle']      			= 'Production Stages | '.DEFAULT_APPLICATION_NAME;
		
		$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/more_stage_listing',$result,true);	
		$this->load->view('cpanel/template',$data);
		
	}
	
	function addMoreStage($parentSefURL) { 
		
		checkEmptySefURL($parentSefURL);  // Calling From Shared Helper
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $parentSefURL));
		
		if ($record->num_rows() == 0 ) {
			
						redirect('manage-stages/');	
		}
		
		$row	 							= $record->row_array();
		$parentID							= $row['id'];
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('name','name','trim|required|callback_check_already_exist_stage_name');
		$this->form_validation->set_rules('description','description','trim');
		
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');
		
			if($this->form_validation->run() === FALSE ) {
				
				$result['assignedRoles']			= $assignedRoles;
				$result['accessModules']			= $accessModules;
				
				$result['parentID']     			= $parentID;
				$result['parentSefURL']     		= $parentSefURL;
				
				
				$data['userInfo']            		= $userInfo;
		
				$data['activeMenu']  				= '5';
				$data['activeSubMenu']  			= '5.1';
				
				$data['pageHeading']    			= "<i class='fa fa-delicious'></i> Production Room - ".$row['name'];
				$data['subHeading']    				= "";
				
				$data['metaType']     				= 'internal';
				$data['pageTitle']      			= 'Production Rooms | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/more_stage_add',$result,true);	
				$this->load->view('cpanel/template',$data);
			
			} else {
				
								// Prepair Date To Store In Database
								$name								=	($this->input->post('name') ?: NULL);
								$description				    	=	($this->input->post('description') ?: NULL);
								$isDeleteAble						=	HARD_CODE_ID_YES;
								
							 // Check Sef Already Exist Or Not
							 if ($this->check_already_exist_SefURL_stage_name(SEF_URLS($name))) {
									
									 //IF Sef Already Exist Then Concatenate A Unique Number  
									 $sefURL	=	SEF_URLS_CONCAT($name); 	 // Calling From Shared Helper
						
							 } else {
									
									 //IF Sef Not Exist Then Leave It To Orignal Name				
									 $sefURL	=	SEF_URLS($name); // Calling From Shared Helper
							 }
				
									$data['name']								 =	$name;
									$data['sef_url']							 =	$sefURL; 
									$data['description']						 =	$description;
									$data['parent']								 =	$parentID;
									
									$data['is_deletable']						 =	$isDeleteAble;
									
									$data['created']							 =  DATABASE_NOW_DATE_TIME_FORMAT(); 
									$data['created_by']							 =  $teamID;
									$data['created_by_reference_table']			 =  'MY_ORGANIZATION_TEAM_TABLE';
									
									// Save All data Into Database Table
									$inertedID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_PRODUCTION_STAGES_TABLE,$data);
				
									// Set Session Message If Everything Goes Fine
									$this->session->set_userdata('admin_msg',RECORD_ADDED);
									
									 if ($this->input->post('submit') == 'Save & New') {
										    redirect('add-more-stage/'.$parentSefURL);
									 } else {
											redirect('more-stages/'.$parentSefURL);
									}
			}
	}
	
	function editMoreStage($parentSefURL,$sefURL) { 
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		checkEmptySefURL($parentSefURL);  // Calling From Shared Helper
		checkEmptySefURL($sefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $sefURL));
		
		if ($record->num_rows() == 0 ) {
				
				redirect('more-stages/'.$parentSefURL);	
		}
		
		$row	    = $record->row_array();
		
		$parentID   = $row['parent'];
		$ID 	    = $row['id'];
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('name','name','trim|required|callback_check_exist_edit_stage_name');
		$this->form_validation->set_rules('description','description','trim');
		
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');
			
			if($this->form_validation->run() === FALSE ) {
								
				$result['assignedRoles']		= $assignedRoles;
				$result['accessModules']		= $accessModules;
				
				$result['row']					= $row;
				
				$result['parentID']     		= $parentID;
				
				$result['parentSefURL']     	= $parentSefURL;
				$result['sefURL']     			= $sefURL;
				
				$data['userInfo']            	= $userInfo;
		
				$data['activeMenu']  			= '5';
				$data['activeSubMenu']  		= '5.1';
				
				$data['pageHeading']    		= "<i class='fa fa-delicious'></i> Production Room - ".$row['name'];
				$data['subHeading']    			= "";
				
				$data['metaType']     			= 'internal';
				$data['pageTitle']      		= 'Production Rooms | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']				=	$this->load->view('cpanel/administrator/setups/stages/more_stage_edit',$result,true);	
				$this->load->view('cpanel/template',$data);
			
			} else {
			
								// Prepair Date To Store In Database
								// Prepair Date To Store In Database
								$name								=	($this->input->post('name') ?: NULL);
								$description				    	=	($this->input->post('description') ?: NULL);
							
							if (convertAllCharactersLowercase($name) != convertAllCharactersLowercase($row['name'])) {
							
										   // Check Sef Already Exist Or Not
										   if ($this->check_already_exist_SefURL_stage_name(SEF_URLS($name))) {
												  
												   //IF Sef Already Exist Then Concatenate A Unique Number  
												   $sefURL					=	SEF_URLS_CONCAT($name); 	 // Calling From Shared Helper
												   $data['sef_url']			=	$sefURL; 
									  
										   } else {
												  
												   //IF Sef Not Exist Then Leave It To Orignal Name				
												   $sefURL					=	SEF_URLS($name); // Calling From Shared Helper
												   $data['sef_url']			=	$sefURL; 
										   }
							 }
				
											$data['name']					=	$name;
											$data['description']			=	$description;
											
											// Update Record Table
											$this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRODUCTION_STAGES_TABLE,$data);
											
											// Set Session Message If Everything Goes Fine
											$this->session->set_userdata('admin_msg',RECORD_UPDATED);
											redirect('more-stages/'.$parentSefURL);
			}
	}
	
	function deleteMoreStage($parentSefURL,$sefURL) {
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptySefURL($parentSefURL);  // Calling From Shared Helper
		checkEmptySefURL($sefURL);  // Calling From Shared Helper
		
		$accountID			 = $this->accountID;
		$teamID				 = $this->teamID;
		$organizationID 	 = $this->organizationID;
		
		$userInfo 			 = userInfo($accountID); // Calling From Application Helper
					    
		$result 			 = $this->model_shared->getRecord('*',MEDICAL_PRODUCTION_STAGES_TABLE,'sef_url',$sefURL);
		
		if ($result->num_rows == 0) {
				
				$this->session->set_userdata('admin_msg_error',RECORD_NOT_FOUND);
				redirect('more-stages/'.$parentSefURL);	
		
		} else {
		
					$row = $result->row_array();
					
					$parentID  = $row['parent'];
					$ID  	   = $row['id'];
					
					$child  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'parent' => $ID));
		
					if ($child->num_rows > 0) {
							
							$this->session->set_userdata('admin_msg_error','Stage \''.$row['name'].'\' can\'t be deleted. Please first delete its childs.');
							redirect('more-stages/'.$parentSefURL);	
					}
		
					if ($row['is_deletable'] == HARD_CODE_ID_NO) {
						
						$this->session->set_userdata('admin_msg_error','More Stage \''.$row['name'].'\' can\'t be deleted.');
						
							redirect('more-stages/'.$parentSefURL);	
						
					} else {
						
							  // Soft Delete From Database Update Record IsDeleted = TRUE 
							  $update['is_deleted'] 											= HARD_CODE_ID_YES;
							  $update['deleted_by'] 											= $teamID;
							  $update['deleted_by_reference_table'] 							= 'MY_ORGANIZATION_TEAM_TABLE';
							  $update['deleted']												= DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
					  
							  $this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRODUCTION_STAGES_TABLE,$update);
							  $this->session->set_userdata('admin_msg',RECORD_SOFT_DELETED);
							 
							  redirect('more-stages/'.$parentSefURL);	

					}
		}
				
	}
	
	function check_already_exist_stage_name($txtValue) {
		
		$db_name = $this->model_shared->checkRecordExist_SoftDelete('name',MEDICAL_PRODUCTION_STAGES_TABLE,'name',$txtValue);
		
			if ($db_name->num_rows() > 0) {
			
				$this->form_validation->set_message('check_already_exist_stage_name','Error: Name \''.$txtValue.'\' already exist. Please try with different name.');
				return false;		
			
			} else 	{
				return true;	
			}
	}
	
	function check_exist_edit_stage_name() {
	
		$txtName  = $this->input->post('name');
		$oldName = $this->input->post('old_name');
	
		if ($txtName != $oldName) {
			
			$dbRecord = $this->model_shared->checkRecordEdit_SoftDelete('name',MEDICAL_PRODUCTION_STAGES_TABLE,'name',$txtName,$oldName);
				
				if ($dbRecord->num_rows()>0) {
					
					$this->form_validation->set_message('check_exist_edit_stage_name','Error: Name \''.$txtName.'\' already exist. Please try with different name.');
					
					return false;	
				
				} else {
					
					return true;	
				}
		
		} else {
			
			return true;	
		}
	}
	
	
	function stageStatus($stageSefURL,$page=0) {
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptySefURL($stageSefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$stage  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $stageSefURL));
		
		if ($stage->num_rows() == 0 ) {
				
				redirect('manage-stages/');	
		}
		
		$stage	 							= $stage->row_array();
		$stageID							= $stage['id'];
		
		$recordperpage						= 200;
		
		$result['assignedRoles']			= $assignedRoles;
		$result['accessModules']			= $accessModules;
		
		$result['stageSefURL']				= $stageSefURL;	
		
		$result['result']	   				= $this->model_shared->page_listing_where($page,$recordperpage,'*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('parent' => HARD_CODE_ID_PARENT_OR_INDEPENDENT,'is_deleted' => HARD_CODE_ID_NO,'production_stage_id' => $stageID),'order','ASC');
			
		$mypaing['total_rows']			 	= $this->model_shared->page_listing_num_where('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('parent' => HARD_CODE_ID_PARENT_OR_INDEPENDENT,'is_deleted' => HARD_CODE_ID_NO,'production_stage_id' => $stageID));
		
		$result['totalRows']				= $mypaing['total_rows'];
		
		$mypaing['base_url'] 				= base_url()."stage-status/";
		$mypaing['per_page']				= $recordperpage;
		$mypaing['uri_segment']				= 2;	
		
		$mypaing['full_tag_open'] 			= "<ul class='pagination pagination-sm no-margin pull-right d-none d-sm-flex'>";
		$mypaing['full_tag_close'] 			= '</ul>';
		$mypaing['num_tag_open'] 			= '<li>';
		$mypaing['num_tag_close'] 			= '</li>';
		$mypaing['cur_tag_open'] 			= '<li><a href="javascript:void(0);" class="current">';
		$mypaing['cur_tag_close'] 			= '</a></li>';
		$mypaing['prev_tag_open'] 			= '<li>';
		$mypaing['prev_tag_close']			= '</li>';
		$mypaing['first_tag_open'] 			= '<li>';
		$mypaing['first_tag_close'] 		= '</li>';
		$mypaing['last_tag_open'] 			= '<li>';
		$mypaing['last_tag_close'] 			= '</li>';
	
		$mypaing['prev_link'] 				= 'Previous';
		$mypaing['prev_tag_open']			= '<li>';
		$mypaing['prev_tag_close'] 			= '</li>';
	
	
		$mypaing['next_link'] 				= 'Next';
		$mypaing['next_tag_open'] 			= '<li>';
		$mypaing['next_tag_close'] 			= '</li>';
 		
		$this->pagination->initialize($mypaing);
		
		$result['paginglink']				= $this->pagination->create_links();
		
		$data['userInfo']            		= $userInfo;
		
		$data['activeMenu']  				= '5';
		$data['activeSubMenu']  			= '5.1';
		
		$data['pageHeading']    			= "<i class='fa fa-tags'></i> Status - ".$stage['name'];
		$data['subHeading']    				= "";
		
		$data['metaType']     				= 'internal';
		$data['pageTitle']      			= 'Production Room Status | '.DEFAULT_APPLICATION_NAME;
		
		$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/status/status_listing',$result,true);	
		$this->load->view('cpanel/template',$data);
		
	}
	
	function addStatus($stageSefURL) { 
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptySefURL($stageSefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$stage  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $stageSefURL));
		
		if ($stage->num_rows() == 0 ) {
				
				redirect('manage-stages/');	
		}
		
		$stage		= $stage->row_array();
		
		$stageID	= $stage['id'];
		
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('name','name','trim|required|callback_check_already_exist_status_name');
		$this->form_validation->set_rules('description','description','trim');
		
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');
		
			if($this->form_validation->run() === FALSE ) {
				
				$result['assignedRoles']			= $assignedRoles;
				$result['accessModules']			= $accessModules;
				
				$result['stageSefURL']				= $stageSefURL;							
				
				$data['userInfo']            		= $userInfo;
		
				$data['activeMenu']  				= '5';
				$data['activeSubMenu']  			= '5.1';
				
				$data['pageHeading']    			= "<i class='fa fa-tags'></i> Status -".$stage['name'];
				$data['subHeading']    				= "";
				
				$data['metaType']     				= 'internal';
				$data['pageTitle']      			= 'Room Status | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/status/status_add',$result,true);	
				$this->load->view('cpanel/template',$data);
			
			} else {
				
								// Prepair Date To Store In Database
								$name								=	($this->input->post('name') ?: NULL);
								$description				    	=	($this->input->post('description') ?: NULL);
								$isDeleteAble						=	HARD_CODE_ID_YES;
							
							 // Check Sef Already Exist Or Not
							 if ($this->check_already_exist_SefURL_status_name(SEF_URLS($name))) {
									
									 //IF Sef Already Exist Then Concatenate A Unique Number  
									 $sefURL	=	SEF_URLS_CONCAT($name); 	 // Calling From Shared Helper
						
							 } else {
									
									 //IF Sef Not Exist Then Leave It To Orignal Name				
									 $sefURL	=	SEF_URLS($name); // Calling From Shared Helper
							 }
				
									$data['production_stage_id']				 =	$stageID;
									$data['name']								 =	$name;
									$data['sef_url']						     =	$sefURL; 
									$data['description']						 =	$description;
									$data['parent']								 =	HARD_CODE_ID_PARENT_OR_INDEPENDENT;
									
									$data['is_deletable']						 =	$isDeleteAble;
									$data['publish_status']						 =  HARD_CODE_ID_PUBLISHED; 
										
									$data['created']							 =  DATABASE_NOW_DATE_TIME_FORMAT(); 
									$data['created_by']							 =  $teamID;
									$data['created_by_reference_table']			 =  'MY_ORGANIZATION_TEAM_TABLE';
									// Save All data Into Database Table
									$inertedID									 = 	$this->model_shared->insertRecord_ReturnID(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,$data);
				
									// Set Session Message If Everything Goes Fine
									$this->session->set_userdata('admin_msg',RECORD_ADDED);
									
									 if ($this->input->post('submit') == 'Save & New') {
										    redirect('add-status/'.$stageSefURL);
									 } else {
											redirect('stage-status/'.$stageSefURL);
									}
			}
	}
	
	function editStatus($stageSefURL,$sefURL) { 
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptySefURL($stageSefURL);  // Calling From Shared Helper
		checkEmptySefURL($sefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $sefURL));
		
		if ($record->num_rows() == 0 ) {
				
			redirect('/stage-status/'.$stageSefURL);	
		}
		
		$row	 = $record->row_array();
		$ID		 = $row['id'];
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('name','name','trim|required|callback_check_exist_edit_status_name');
		$this->form_validation->set_rules('description','description','trim');
		
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');
			
			if($this->form_validation->run() === FALSE ) {
								
				$result['assignedRoles']			= $assignedRoles;
				$result['accessModules']			= $accessModules;
				
				$result['row']						= $row;
				
				$result['stageSefURL']				= $stageSefURL;
				$result['sefURL']					= $sefURL;
				
				$data['userInfo']            		= $userInfo;
		
				$data['activeMenu']  				= '5';
				$data['activeSubMenu']  			= '5.1';
				
				$data['pageHeading']    			= "<i class='fa fa-tags'></i> Status - ".$row['name'];
				$data['subHeading']    				= "";
				
				$data['metaType']     				= 'internal';
				$data['pageTitle']      			= 'Room Status | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/status/status_edit',$result,true);	
				$this->load->view('cpanel/template',$data);
			
			} else {
			
								// Prepair Date To Store In Database
								$name								=	($this->input->post('name') ?: NULL);
								$description				    	=	($this->input->post('description') ?: NULL);
							
								if (convertAllCharactersLowercase($name) != convertAllCharactersLowercase($row['name'])) {
							
										   // Check Sef Already Exist Or Not
										   if ($this->check_already_exist_SefURL_status_name(SEF_URLS($name))) {
												  
												   //IF Sef Already Exist Then Concatenate A Unique Number  
												   $sefURL						=	SEF_URLS_CONCAT($name); 	 // Calling From Shared Helper
												   $data['sef_url']				=	$sefURL; 
									  
										   } else {
												  
												   //IF Sef Not Exist Then Leave It To Orignal Name				
												   $sefURL						=	SEF_URLS($name); // Calling From Shared Helper
												   $data['sef_url']				=	$sefURL; 
										   }
								 }
				
				
											$data['name']						=	$name;
											$data['description']				=	$description;
											
											// Update Record Table
											$this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,$data);
											
											// Set Session Message If Everything Goes Fine
											$this->session->set_userdata('admin_msg',RECORD_UPDATED);
											redirect('/stage-status/'.$stageSefURL);	
			}
	}
	
	function deleteStatus($stageSefURL,$sefURL) {
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptySefURL($stageSefURL);  // Calling From Shared Helper
		checkEmptySefURL($sefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
					    
		$result = $this->model_shared->getRecord('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,'sef_url',$sefURL);
		
		if ($result->num_rows == 0) {
				
				$this->session->set_userdata('admin_msg_error',RECORD_NOT_FOUND);
				redirect('stage-status/'.$stageSefURL);	
		
		} else {
		
					$row = $result->row_array();
					$ID  = $row['id'];
					
					$child  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'parent' => $ID));
		
					if ($child->num_rows > 0) {
							
							$this->session->set_userdata('admin_msg_error','Status \''.$row['name'].'\' can\'t be deleted. Please first delete its childs.');
							redirect('stage-status/'.$stageSefURL);	
					}
		
					if ($row['is_deletable'] == HARD_CODE_ID_NO) {
						
						$this->session->set_userdata('admin_msg_error','Stage \''.$row['name'].'\' can\'t be deleted.');
						
						redirect('stage-status/'.$stageSefURL);	
							
					} else {
						
							  // Soft Delete From Database Update Record IsDeleted = TRUE 
							  $update['is_deleted'] 											= HARD_CODE_ID_YES;
							  $update['deleted_by'] 											= $teamID;
							  $update['deleted_by_reference_table'] 							= 'MY_ORGANIZATION_TEAM_TABLE';
							  $update['deleted']												= DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
					  
							  $this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,$update);
							  $this->session->set_userdata('admin_msg',RECORD_SOFT_DELETED);
							 
							 redirect('stage-status/'.$stageSefURL);	

					}
		}
				
	}
	
	function moreStatus($stageSefURL,$parentSefURL,$page=0) {
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptyID($stageSefURL);  // Calling From Shared Helper
		checkEmptyID($parentSefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$stage  							= $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $stageSefURL));
		$stage								= $stage->row_array();
		
		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url'=> $parentSefURL));
		
		if ($record->num_rows() == 0 ) {
				
				redirect('stage-status/'.$stageSefURL);	
		}
		
		$row								= $record->row_array();
	
		$parentID							= $row['parent'];
		$ID									= $row['id'];
		
		
		$result['assignedRoles']			= $assignedRoles;
		$result['accessModules']			= $accessModules;
		
		$result['stageSefURL']     			= $stageSefURL;
		$result['parentSefURL']     		= $parentSefURL;
		
		$result['parentID']     			= $parentID;
		$result['ID']     					= $ID;
	
		$recordperpage						= 200;
		
		$result['result']	   				= $this->model_shared->page_listing_where($page,$recordperpage,'*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('parent' => $ID,'is_deleted' => HARD_CODE_ID_NO),'order','ASC');	
		$totalRows			 	 			= $this->model_shared->page_listing_num_where('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('parent' => $ID,'is_deleted' => HARD_CODE_ID_NO));
		
		$result['totalRows']				= $totalRows;	
		
		$mypaing['total_rows']				= $totalRows;
		
		$mypaing['base_url'] 				= base_url()."more-status/".$stageSefURL;
		$mypaing['per_page']				= $recordperpage;
		$mypaing['uri_segment']				= 3;	
		
		$mypaing['full_tag_open'] 			= "<ul class='pagination pagination-sm no-margin pull-right d-none d-sm-flex'>";
		$mypaing['full_tag_close'] 			= '</ul>';
		$mypaing['num_tag_open'] 			= '<li>';
		$mypaing['num_tag_close'] 			= '</li>';
		$mypaing['cur_tag_open'] 			= '<li><a href="javascript:void(0);" class="current">';
		$mypaing['cur_tag_close'] 			= '</a></li>';
		$mypaing['prev_tag_open'] 			= '<li>';
		$mypaing['prev_tag_close']			= '</li>';
		$mypaing['first_tag_open'] 			= '<li>';
		$mypaing['first_tag_close'] 		= '</li>';
		$mypaing['last_tag_open'] 			= '<li>';
		$mypaing['last_tag_close'] 			= '</li>';
	
		$mypaing['prev_link'] 				= 'Previous';
		$mypaing['prev_tag_open']			= '<li>';
		$mypaing['prev_tag_close'] 			= '</li>';
	
	
		$mypaing['next_link'] 				= 'Next';
		$mypaing['next_tag_open'] 			= '<li>';
		$mypaing['next_tag_close'] 			= '</li>';
		
		$this->pagination->initialize($mypaing);
		
		$result['paginglink']				= $this->pagination->create_links();
		
		$data['userInfo']            		= $userInfo;
		
		$data['activeMenu']  				= '5';
		$data['activeSubMenu']  			= '5.1';
		
		$data['pageHeading']    			= "<i class='fa fa-tags'></i> Status - ".$stage['name']." - ".$row['name'];
		$data['subHeading']    				= "";
		
		$data['metaType']     				= 'internal';
		$data['pageTitle']      			= 'Production Rooms | '.DEFAULT_APPLICATION_NAME;
		
		$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/status/more_status_listing',$result,true);	
		$this->load->view('cpanel/template',$data);
		
	}
	
	function addMoreStatus($stageSefURL,$parentSefURL) { 
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptySefURL($stageSefURL);  // Calling From Shared Helper
		checkEmptySefURL($parentSefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		$stage 								= $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $stageSefURL));
		
		$stage		 						= $stage->row_array();
		$stageID	 						= $stage['id'];
		
		$record 	 						= $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $parentSefURL));
		
		if ($record->num_rows() == 0 ) {
			
			redirect('stage-status/'.$stageSefURL);	
		}
		
		$row	 							= $record->row_array();
		$parentID							= $row['parent'];
		$ID									= $row['id'];
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('name','name','trim|required|callback_check_already_exist_stage_name');
		$this->form_validation->set_rules('description','description','trim');
		
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');
		
			if($this->form_validation->run() === FALSE ) {
				
				$result['assignedRoles']			= $assignedRoles;
				$result['accessModules']			= $accessModules;
				
				$result['stageSefURL']     			= $stageSefURL;
				$result['parentSefURL']     		= $parentSefURL;
				
				$result['parentID']     			= $parentID;
				$result['ID']     					= $ID;
				
				$data['userInfo']            		= $userInfo;
		
				$data['activeMenu']  				= '5';
				$data['activeSubMenu']  			= '5.1';
				
				$data['pageHeading']    			= "<i class='fa fa-delicious'></i> Status - ".$stage['name']." - ".$row['name'];
				$data['subHeading']    				= "";
				
				$data['metaType']     				= 'internal';
				$data['pageTitle']      			= 'Production Rooms | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']					=	$this->load->view('cpanel/administrator/setups/stages/status/more_status_add',$result,true);	
				$this->load->view('cpanel/template',$data);
			
			} else {
				
								// Prepair Date To Store In Database
								$name								=	($this->input->post('name') ?: NULL);
								$description				    	=	($this->input->post('description') ?: NULL);
								$isDeleteAble						=	HARD_CODE_ID_YES;
								
							 // Check Sef Already Exist Or Not
							 if ($this->check_already_exist_SefURL_status_name(SEF_URLS($name))) {
									
									 //IF Sef Already Exist Then Concatenate A Unique Number  
									 $sefURL	=	SEF_URLS_CONCAT($name); 	 // Calling From Shared Helper
						
							 } else {
									
									 //IF Sef Not Exist Then Leave It To Orignal Name				
									 $sefURL	=	SEF_URLS($name); // Calling From Shared Helper
							 }
				
									$data['production_stage_id']				 =	$stageID;
									$data['name']								 =	$name;
									$data['sef_url']							 =	$sefURL; 
									$data['description']						 =	$description;
									$data['parent']								 =	$ID;
									
									$data['is_deletable']						 =	$isDeleteAble;
									
									$data['created']							 =  DATABASE_NOW_DATE_TIME_FORMAT(); 
									$data['created_by']							 =  $teamID;
									$data['created_by_reference_table']			 =  'MY_ORGANIZATION_TEAM_TABLE';
									
									// Save All data Into Database Table
									$inertedID									 = 	$this->model_shared->insertRecord_ReturnID(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,$data);
				
									// Set Session Message If Everything Goes Fine
									$this->session->set_userdata('admin_msg',RECORD_ADDED);
									
									 if ($this->input->post('submit') == 'Save & New') {
										    redirect('add-more-status/'.$stageSefURL.'/'.$parentSefURL);
									 } else {
											redirect('more-status/'.$stageSefURL.'/'.$parentSefURL);
									}
			}
	}
	
	function editMoreStatus($stageSefURL,$parentSefURL,$sefURL) { 
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptySefURL($stageSefURL);  // Calling From Shared Helper
		checkEmptySefURL($parentSefURL);  // Calling From Shared Helper
		checkEmptySefURL($sefURL);  // Calling From Shared Helper
		
		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;
		
		$userInfo 							= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;
		
		
		$stage 								= $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $stageSefURL));
		$stage		 						= $stage->row_array();
		
		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'sef_url' => $sefURL));
		
		if ($record->num_rows() == 0 ) {
				
				redirect('more-status/'.$stageSefURL.'/'.$parentSefURL);	
		}
		
		$row	    = $record->row_array();
		
		$parentID   = $row['parent'];
		$ID 	    = $row['id'];
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('name','name','trim|required|callback_check_exist_edit_status_name');
		$this->form_validation->set_rules('description','description','trim');
		
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');
			
			if($this->form_validation->run() === FALSE ) {
								
				$result['assignedRoles']		= $assignedRoles;
				$result['accessModules']		= $accessModules;
				
				$result['row']					= $row;
				
				$result['stageSefURL']     		= $stageSefURL;
				$result['parentSefURL']     	= $parentSefURL;
				$result['sefURL']     			= $sefURL;
				
				
				$result['parentID']     		= $parentID;
				$result['ID']     				= $ID;
				
				
				$data['userInfo']            	= $userInfo;
		
				$data['activeMenu']  			= '5';
				$data['activeSubMenu']  		= '5.1';
				
				$data['pageHeading']    		= "<i class='fa fa-tags'></i> Status - ".$stage['name']." - ".$row['name'];
				$data['subHeading']    			= "";
				
				$data['metaType']     			= 'internal';
				$data['pageTitle']      		= 'Room Status | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']				=	$this->load->view('cpanel/administrator/setups/stages/status/more_status_edit',$result,true);	
				$this->load->view('cpanel/template',$data);
			
			} else {
			
								// Prepair Date To Store In Database
								// Prepair Date To Store In Database
								$name								=	($this->input->post('name') ?: NULL);
								$description				    	=	($this->input->post('description') ?: NULL);
							
							if (convertAllCharactersLowercase($name) != convertAllCharactersLowercase($row['name'])) {
							
										   // Check Sef Already Exist Or Not
										   if ($this->check_already_exist_SefURL_stage_name(SEF_URLS($name))) {
												  
												   //IF Sef Already Exist Then Concatenate A Unique Number  
												   $sefURL					=	SEF_URLS_CONCAT($name); 	 // Calling From Shared Helper
												   $data['sef_url']			=	$sefURL; 
									  
										   } else {
												  
												   //IF Sef Not Exist Then Leave It To Orignal Name				
												   $sefURL					=	SEF_URLS($name); // Calling From Shared Helper
												   $data['sef_url']			=	$sefURL; 
										   }
							 }
				
											$data['name']					=	$name;
											$data['description']			=	$description;
											
											// Update Record Table
											$this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,$data);
											
											// Set Session Message If Everything Goes Fine
											$this->session->set_userdata('admin_msg',RECORD_UPDATED);
											redirect('more-status/'.$stageSefURL.'/'.$parentSefURL);
			}
	}
	
	function deleteMoreStatus($stageSefURL,$parentSefURL,$sefURL) {
		
		/*Use For Access Permissions*/
		if (!in_array(MODULE_SETUP_PRODUCTION_ROOMS,$this->accessModules)) {
		
			redirect('my-dashboard/');	
		}
		
		checkEmptySefURL($stageSefURL);  // Calling From Shared Helper
		checkEmptySefURL($parentSefURL);  // Calling From Shared Helper
		checkEmptySefURL($sefURL);  // Calling From Shared Helper
		
		$accountID			 = $this->accountID;
		$teamID				 = $this->teamID;
		$organizationID 	 = $this->organizationID;
		
		$userInfo 			 = userInfo($accountID); // Calling From Application Helper
					    
		$result 			 = $this->model_shared->getRecord('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,'sef_url',$sefURL);
		
		if ($result->num_rows() == 0) {
				
				$this->session->set_userdata('admin_msg_error',RECORD_NOT_FOUND);
				redirect('more-status/'.$stageSefURL.'/'.$parentSefURL);	
		
		} else {
		
					$row = $result->row_array();
					
					$parentID  = $row['parent'];
					$ID  	   = $row['id'];
					
					$child  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'parent' => $ID));
		
					if ($child->num_rows > 0) {
							
							$this->session->set_userdata('admin_msg_error','Status \''.$row['name'].'\' can\'t be deleted. Please first delete its childs.');
							redirect('more-status/'.$stageSefURL.'/'.$parentSefURL);	
					}
		
					if ($row['is_deletable'] == HARD_CODE_ID_NO) {
						
						$this->session->set_userdata('admin_msg_error','More Status \''.$row['name'].'\' can\'t be deleted.');
						
							redirect('more-status/'.$stageSefURL.'/'.$parentSefURL);	
						
					} else {
						
							  // Soft Delete From Database Update Record IsDeleted = TRUE 
							  $update['is_deleted'] 											= HARD_CODE_ID_YES;
							  $update['deleted_by'] 											= $teamID;
							  $update['deleted_by_reference_table'] 							= 'MY_ORGANIZATION_TEAM_TABLE';
							  $update['deleted']												= DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
					  
							  $this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,$update);
							  $this->session->set_userdata('admin_msg',RECORD_SOFT_DELETED);
							 
							  redirect('more-status/'.$stageSefURL.'/'.$parentSefURL);	

					}
		}
				
	}
	
	function check_already_exist_status_name($txtValue) {
		
		return true;
		
		$db_name = $this->model_shared->checkRecordExist_SoftDelete('name',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,'name',$txtValue);
		
			if ($db_name->num_rows() > 0) {
			
				$this->form_validation->set_message('check_already_exist_status_name','Error: Name \''.$txtValue.'\' already exist. Please try with different name.');
				return false;		
			
			} else 	{
				return true;	
			}
	}
	
	function check_exist_edit_status_name() {
		
		return true;
		
		$txtName  = $this->input->post('name');
		$oldName = $this->input->post('old_name');
	
		if ($txtName != $oldName) {
			
			$dbRecord = $this->model_shared->checkRecordEdit_SoftDelete('name',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,'name',$txtName,$oldName);
				
				if ($dbRecord->num_rows()>0) {
					
					$this->form_validation->set_message('check_exist_edit_status_name','Error: Name \''.$txtName.'\' already exist. Please try with different name.');
					
					return false;	
				
				} else {
					
					return true;	
				}
		
		} else {
			
			return true;	
		}
	}
	
	//Inside Functions Calling
	private function check_already_exist_SefURL_stage_name($sefURL) {
			
			$result = $this->model_shared->getRecordMultipleWhere('id',MEDICAL_PRODUCTION_STAGES_TABLE,array('sef_url' => $sefURL));		

			if ($result->num_rows() > 0) {
			
					return true;	
			
			} else {
			
					return false;
			}				
	}
	
	private function check_already_exist_SefURL_status_name($sefURL) {
			
			$result = $this->model_shared->getRecordMultipleWhere('id',MEDICAL_PRODUCTION_STAGE_STATUS_TABLE,array('sef_url' => $sefURL));		

			if ($result->num_rows() > 0) {
			
					return true;	
			
			} else {
			
					return false;
			}				
	}
}
?>
<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');

class ShipmentApi extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		$this->load->model('cpanel/cases/model_shared');



	}

	

	function index() 
	{
		/*$case  =	($this->input->post('list_case') ?: NULL);*/
		$record  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_CASES_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED));
		/*$count  = $record->num_rows();*/

		/*if ($count > 0) 
		{*/
			foreach($record->result() as $row) 
			{
				$caseID                                        = $row->case_reference_number;
				$inventory_cases_data['case_reference_number'] = $row->case_reference_number;
				$inventory_cases_data['patient']               = $row->patient;
				$inventory_cases_data['age']                   = $row->age;
				$inventory_cases_data['gender']                = $row->gender;
				$inventory_cases_data['patient_country']       = $row->patient_country;
				$inventory_cases_data['patient_state']         = $row->patient_state;
				$inventory_cases_data['patient_city']          = $row->patient_city;
				/*$inventory_cases_data['receive_date']        = $row->receive_date;*/
				$inventory_cases_data['impression_type']       = $row->impression_type;
				$inventory_cases_data['arch_upper']            = $row->arch_upper;
				$inventory_cases_data['arch_lower']            = $row->arch_lower;
				$inventory_cases_data['country']               = $row->country;
				$inventory_cases_data['city']                  = $row->city;
				$inventory_cases_data['distributor']           = $row->distributor;
				$inventory_cases_data['description']           = $row->description;
				$inventory_cases_data['RX_form']               = $row->RX_form;
				$inventory_cases_data['x_rays_opg']            = $row->x_rays_opg;
				$inventory_cases_data['x_rays_ceph']           = $row->x_rays_ceph;
				$inventory_cases_data['file_assessment']       = $row->file_assessment;
				$inventory_cases_data['patient']               = $row->patient;
				$inventory_cases_data['patient']               = $row->patient;
				$inventory_cases_data['created']			   = $row->created;
				$inventory_cases_data['created_by']		       = $row->created_by;

				$checksAlreadyPresentCase 	= $this->model_shared->getRecord_SecondDB_MultipleWhere('id',INVENTORY_CASES_TABLE,array('case_reference_number' => $caseID));

				if ($checksAlreadyPresentCase->num_rows() > 0) {

					// update All data Into tbl_application_inventory_cases
				    $inventoryCasesID 	= $this->model_shared->updateRecordSecondDB_ReturnID(array('case_reference_number' => $caseID,'is_deleted' => HARD_CODE_ID_NO),INVENTORY_CASES_TABLE,$inventory_cases_data);
				}

				else
				{
					// Save All data Into tbl_application_inventory_cases
				    $inventoryCasesID									 = 	$this->model_shared->insertRecord_SecondDB_ReturnID(INVENTORY_CASES_TABLE,$inventory_cases_data);
				}
			}

				 
			
			$record_data_query  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_RECORD_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED));

			foreach($record_data_query->result() as $record_row)
			{
				$inventory_record_data['id']                           = $record_row->id;
				$inventory_record_data['sent_date']                    = $record_row->sent_date;
				$inventory_record_data['country']                      = $record_row->country;
				$inventory_record_data['city']                         = $record_row->city;
				$inventory_record_data['distributor']                  = $record_row->distributor;
				$inventory_record_data['number_of_cases']              = $record_row->number_of_cases;
				$inventory_record_data['delivery_service']             = $record_row->delivery_service;
				$inventory_record_data['airway_bill_number']           = $record_row->airway_bill_number;
				$inventory_record_data['airway_bill_number_formatted'] = $record_row->airway_bill_number_formatted;
				$inventory_record_data['description']		           = $record_row->description;
				$inventory_record_data['created']			           = $record_row->created;
				$inventory_record_data['created_by']		           = $record_row->created_by;

				// Save All data Into tbl_application_medical_cases_temporary_inventory_record
				$inventoryRecordID									   = 	$this->model_shared->insertRecord_SecondDB_ReturnID(INVENTORY_RECORD_TABLE,$inventory_record_data);
			}




			$items_data_query  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_ITEMS_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED));

			foreach($items_data_query->result() as $items_row)
			{
				$inventory_id                                       = $items_row->inventory_id;
				$inventory_case_reference_number                    = $items_row->case_reference_number;

				$inventory_items_data['inventory_id']               = $items_row->inventory_id;
				$inventory_items_data['case_reference_number']      = $items_row->case_reference_number;
				$inventory_items_data['description']		        = $items_row->description;
				$inventory_items_data['created']			        = $items_row->created;
				$inventory_items_data['created_by']		            = $items_row->created_by;

				// Save All data Into tbl_application_medical_cases_temporary_inventory_items
				$inventoryCount 	                                = $this->model_shared->getRecord_SecondDB_MultipleWhere('id',INVENTORY_ITEMS_TABLE,array('case_reference_number' => $inventory_case_reference_number,'inventory_id' => $inventory_id))->num_rows();

				if($inventoryCount==0)
				{
					$inventoryItemsID									= 	$this->model_shared->insertRecord_SecondDB_ReturnID(INVENTORY_ITEMS_TABLE,$inventory_items_data);
				}

				
			}

			



			$update_data['push_status']                              = HARD_CODE_ID_PUSHED;

			 // Update INVENTORY_CASES_TABLE 
			 $this->model_shared->editRecordWhere(array('push_status' => HARD_CODE_ID_NOT_PUSHED),INVENTORY_CASES_TABLE,$update_data);

			 // Update INVENTORY_RECORD_TABLE 
			 $this->model_shared->editRecordWhere(array('push_status' => HARD_CODE_ID_NOT_PUSHED),INVENTORY_RECORD_TABLE,$update_data);

			 // Update INVENTORY_ITEMS_TABLE
			 $this->model_shared->editRecordWhere(array('push_status' => HARD_CODE_ID_NOT_PUSHED),INVENTORY_ITEMS_TABLE,$update_data);


			
		}


	/*}*/



	





}
?>

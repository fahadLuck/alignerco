<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');

class ModificationApi extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		$this->load->model('cpanel/cases/model_case');



	}

	

	function index() 
	{
		/*$case  =	($this->input->post('list_case') ?: NULL);*/
		$record  = $this->model_shared->getRecordMultipleWhere('*',SETUP_MODIFICATION_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED,'is_deleted' => HARD_CODE_ID_NO));
		$count  = $record->num_rows();

		if ($count > 0) 
		{
			foreach($record->result() as $row) 
			{
				$modification_case_data['setup_link_id']                  = $row->setup_link_id;
				$modification_case_data['setup_case_id']                  = $row->setup_case_id;
				$modification_case_data['comment']                        = $row->comment;
				$modification_case_data['operator_comment']               = $row->operator_comment;
				$modification_case_data['receive_date']                   = $row->receive_date;
				$modification_case_data['created']			              = $row->created;
				$modification_case_data['created_by']		              = $row->created_by;

				/*$checksAlreadyPresentCase 	= $this->model_shared->getRecord_SecondDB_MultipleWhere('id',INVENTORY_CASES_TABLE,array('case_reference_number' => $caseID));

				if ($checksAlreadyPresentCase->num_rows() > 0) 
				{
				    $inventoryCasesID 	= $this->model_shared->updateRecordSecondDB_ReturnID(array('case_reference_number' => $caseID,'is_deleted' => HARD_CODE_ID_NO),INVENTORY_CASES_TABLE,$modification_case_data);
				}

				else
				{
				    $inventoryCasesID									 = 	$this->model_shared->insertRecord_SecondDB_ReturnID(INVENTORY_CASES_TABLE,$modification_case_data);
				}*/

				$modificationCasesID									= $this->model_shared->insertRecord_SecondDB_ReturnID(SETUP_MODIFICATION_DATA_TABLE,$modification_case_data);
			}	



			$update_data['push_status']                                 = HARD_CODE_ID_PUSHED;
			$this->model_shared->editRecordWhere(array('push_status' => HARD_CODE_ID_NOT_PUSHED),SETUP_MODIFICATION_TABLE,$update_data);
			
		}


	}



	





}
?>

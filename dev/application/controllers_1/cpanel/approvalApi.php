<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');

class ApprovalApi extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		$this->load->model('cpanel/cases/model_case');



	}

	

	function index() 
	{
		/*$case  =	($this->input->post('list_case') ?: NULL);*/
		$record  = $this->model_shared->getRecordMultipleWhere('*',SETUP_APPROVAL_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED,'is_deleted' => HARD_CODE_ID_NO));
		$count  = $record->num_rows();

		if ($count > 0) 
		{
			foreach($record->result() as $row) 
			{
				$approval_case_data['setup_link_id']                  = $row->setup_link_id;
				$approval_case_data['setup_case_id']                  = $row->setup_case_id;
				$approval_case_data['case_id']                        = $row->case_id;
				$approval_case_data['retainers_upper']                = $row->retainers_upper;
				$approval_case_data['retainers_lower']                = $row->retainers_lower;
				$approval_case_data['aligners_upper']                 = $row->aligners_upper;
				$approval_case_data['aligners_lower']                 = $row->aligners_lower;
				$approval_case_data['receive_date']                   = $row->receive_date;
				$approval_case_data['created']			              = $row->created;
				$approval_case_data['created_by']		              = $row->created_by;

				/*$checksAlreadyPresentCase 	= $this->model_shared->getRecord_SecondDB_MultipleWhere('id',INVENTORY_CASES_TABLE,array('case_reference_number' => $caseID));

				if ($checksAlreadyPresentCase->num_rows() > 0) 
				{
				    $inventoryCasesID 	= $this->model_shared->updateRecordSecondDB_ReturnID(array('case_reference_number' => $caseID,'is_deleted' => HARD_CODE_ID_NO),INVENTORY_CASES_TABLE,$approval_case_data);
				}

				else
				{
				    $inventoryCasesID									 = 	$this->model_shared->insertRecord_SecondDB_ReturnID(INVENTORY_CASES_TABLE,$approval_case_data);
				}*/

				$approvalCasesID									    = 	$this->model_shared->insertRecord_SecondDB_ReturnID(SETUP_APPROVAL_TABLE,$approval_case_data);
			}	



			$update_data['push_status']                                 = HARD_CODE_ID_PUSHED;
			$this->model_shared->editRecordWhere(array('push_status' => HARD_CODE_ID_NOT_PUSHED),SETUP_APPROVAL_TABLE,$update_data);
			
		}


	}



	





}
?>

<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');

class LinkApi extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		$this->load->model('cpanel/cases/model_case');



	}

	

	function index() 
	{
		$page                                      = 0;
		$recordperpage						       = 1;
		$result	   				                   = $this->model_shared->page_listing($page,$recordperpage,'setup_upload_data_id',CASE_SETUP_UPLOAD_DATA_TABLE,'id','DESC');
		$result_row	                               = $result->row_array();
		$setup_upload_data_id_count				   = $result->num_rows();
		if($setup_upload_data_id_count > 0)	
		{
			$latest_setup_upload_data_id           = $result_row['setup_upload_data_id'];
		}
		else
		{
			$latest_setup_upload_data_id           = 0;
		}
		

		$record  = $this->model_shared->getRecord_SecondDB_MultipleWhere('*',CASE_SETUP_UPLOAD_DATA_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id >' => $latest_setup_upload_data_id));
		$count   = $record->num_rows();

		if ($count > 0) 
		{
			foreach($record->result() as $row) 
			{
				$case_id			                = $row->case_id ;
				$getRecord                          = $this->model_shared->getRecord_SecondDB_MultipleWhere('case_reference_number',INVENTORY_CASES_TABLE,array('case_id' => $case_id));
				$getRecord_row	                    = $getRecord->row_array();

				$data['setup_upload_data_id']	    = $row->id;
				$data['case_id_fk']			        = $getRecord_row['case_reference_number'];
				$data['case_id']			        = $row->case_id;
				$data['upload_link']		        = $row->upload_link;
				$data['password']                   = $row->password;
				$data['is_deletable ']              = $row->is_deletable ;
				$data['created']                    = $row->created;
				$data['created_by']                 = $row->created_by;
				$data['created_by_reference_table'] = $row->created_by_reference_table;
				$data['is_deleted ']                = $row->is_deleted ;
				$data['deleted_by']                 = $row->deleted_by;
				/*$data['receive_date']        = $row->receive_date;*/
				$data['deleted_by_reference_table'] = $row->deleted_by_reference_table;
				$data['deleted']                    = $row->deleted;

				// Save All data Into tbl_application_inventory_cases
				$setupLinkTableID					= 	$this->model_shared->insertRecord_ReturnID(CASE_SETUP_UPLOAD_DATA_TABLE,$data);
				
			}
	 

			
		}


	}



	





}
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {


	public  function __construct() {
		
		parent::__construct();
		
		 authentication(); // Calling From Login Helper
		
		/* Table ID*/
		$this->accountID = $this->session->userdata(USER_ACCOUNT_ID_SESSION);
		
		/*ID*/
		$this->teamID   = $this->session->userdata(USER_TEAM_ID_SESSION);
		
		/*ID*/
		$this->HRID   = $this->session->userdata(USER_HR_ID_SESSION);
		
		/*ID*/
		$this->organizationID	= $this->session->userdata(USER_ORGANIZATION_ID_SESSION);
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');
		
		$this->load->model('cpanel/reports/model_report');

		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper
	}
	
	public function doctorReport($page=0) 
	{ 
				
				/*Use For Access Permissions*/
				if (!in_array(DOCTOR_REPORTS,$this->accessModules)) {
				
					redirect('my-dashboard/');	
				}
				
				/* User Roles And Permission
  
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
				
				$assignedRoles 				= $this->assignedRoles;
				$accessModules 				= $this->accessModules;
						
				$recordPerPage				= 1000;

				$accountID					= $this->accountID; 
				$organizationID				= $this->organizationID;
				
				$userInfo 					= userInfo($accountID); // Calling From Application Helper
				
				$result['assignedRoles']	=  $assignedRoles;
				$result['accessModules']	=  $accessModules;
				
				//$result['bloodGroups'] 			= getBloodGroups(); // Calling From Application Helper
				
				$searchParameters 		   = array();
				$doctor_id                 = $this->input->post('doctor',TRUE);
				$date_from                 = $this->input->post('date_from',TRUE);
				$date_to                   = $this->input->post('date_to',TRUE);

				$searchParameters['doctor_id']				= $doctor_id;

				$doctorslist				= $this->model_report->getActiveDoctorsList($organizationID,$searchParameters);

				$doctors					= $this->model_report->getActiveDoctors($organizationID);
				
						
				$result['doctors']		    = $doctors;
				$result['doctorslist']		= $doctorslist;
				$result['date_from']		= $date_from;
				$result['date_to']		    = $date_to;
				$result['selectedDoctorID'] = $doctor_id;
				/*$result['doctorsCases']	    = $doctorsCases;*/
				
				$data['pageHeading']    	= "Doctor Report";	
				/*$data['subHeading']    		= "(all)";*/		
				
				$data['userInfo']           = $userInfo;
				
				$data['activeMenu']  		= '9';
				$data['activeSubMenu']  	= '9.1';
				
				$data['metaType']     		= 'internal';
				$data['pageName']    		= 'Doctor-Report';
				$data['pageTitle']      	= 'Doctor-Report | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']	  	    = $this->load->view('cpanel/reports/doctor_report',$result,true);
				$this->load->view('cpanel/template',$data);
	}
	
	
	
}

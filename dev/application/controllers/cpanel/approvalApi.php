<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');

class ApprovalApi extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		$this->load->model('cpanel/cases/model_case');



	}

	

	function index() 
	{
		/*$case  =	($this->input->post('list_case') ?: NULL);*/
		$record  = $this->model_shared->getRecordMultipleWhere('*',SETUP_APPROVAL_TABLE,array('approval_status' => 'false','is_deleted' => HARD_CODE_ID_NO));
		$count  = $record->num_rows();
		
		if ($count > 0) 
		{
			foreach($record->result() as $row) 
			{
				$setup_upload_data_id                       = $row->setup_link_id;
				$case_id                                    = $row->setup_case_id;
				$reference_case_id                          = $row->case_id;
				$addapprovalId                              = $row->id;

				/*$approval_case_data['setup_upload_data_id'] = $row->setup_link_id;
				$approval_case_data['case_id']              = $row->setup_case_id;
				$approval_case_data['retainers_upper']      = $row->retainers_upper;
				$approval_case_data['retainers_lower']      = $row->retainers_lower;
				$approval_case_data['aligners_upper']       = $row->aligners_upper;
				$approval_case_data['aligners_lower']       = $row->aligners_lower;
				$approval_case_data['created']			    = $row->created;
				$approval_case_data['created_by']		    = $row->created_by;*/


				$result                                     = array();

			    $postRequest['setup_upload_data_id']        = $row->setup_link_id;
				$postRequest['case_id']                     = $row->setup_case_id;
				$postRequest['case_reference_number']       = $row->case_id;
				$postRequest['comments']                    = $row->$approval_comment;
				$postRequest['retainers_upper']             = $row->retainers_upper;
				$postRequest['retainers_lower']             = $row->retainers_lower;
				$postRequest['aligners_upper']              = $row->aligners_upper;
				$postRequest['aligners_lower']              = $row->aligners_lower;
				$postRequest['date']                        = date("Y-m-d");
				
				$result[]                                   = $postRequest;
				$result                                     = json_encode($result);
				$response                                   = apiPostCall(APPROVAL_TO_PRODUCTION_URL,$result); 
				$response                                   = 'True';

				$approvalUpdate['approval_status']          = $response;

				// Update Record Table
				 $this->model_shared->editRecordWhere(array('id' => $addapprovalId,'is_deleted' => HARD_CODE_ID_NO,'case_id' => $reference_case_id),SETUP_APPROVAL_TABLE,$approvalUpdate);

				



				/*$checksAlreadyPresentApproval 	                      = $this->model_shared->getRecord_SecondDB_MultipleWhere('id',SETUP_APPROVAL_TABLE,array('setup_upload_data_id' => $setup_upload_data_id, 'case_id' => $case_id));

				

				if ($checksAlreadyPresentApproval->num_rows() == 0 AND $reference_case_id!='') 
				{
					$approvalCasesID									    = 	$this->model_shared->insertRecord_SecondDB_ReturnID(SETUP_APPROVAL_TABLE,$approval_case_data);
				}


				$sentApprovalCount 	                                        = $this->model_shared->getRecord_SecondDB_MultipleWhere('id',SETUP_APPROVAL_TABLE,array('setup_upload_data_id' => $setup_upload_data_id, 'case_id' => $case_id))->num_rows();

				if($sentApprovalCount > 0)
				{
					$update_data['push_status']                                 = HARD_CODE_ID_PUSHED;
			        $this->model_shared->editRecordWhere(array('push_status' => HARD_CODE_ID_NOT_PUSHED,'case_id' => $reference_case_id,'setup_link_id' => $setup_upload_data_id),SETUP_APPROVAL_TABLE,$update_data);
				}*/

				
			}	
	
			
		}

		echo "Approval Api Successfully Run";

	}



	





}
?>

<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');

class LinkApi extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		$this->load->model('cpanel/cases/model_shared');



	}

	

	function index() 
	{
		$page                                      = 0;
		$recordperpage						       = 1;
		$result	   				                   = $this->model_shared->page_listing($page,$recordperpage,'setup_upload_data_id',CASE_SETUP_UPLOAD_DATA_TABLE,'id','DESC');
		$result_row	                               = $result->row_array();
		$setup_upload_data_id_count				   = $result->num_rows();
		if($setup_upload_data_id_count > 0)	
		{
			$latest_setup_upload_data_id           = $result_row['setup_upload_data_id'];
		}
		else
		{
			$latest_setup_upload_data_id           = 0;
		}
		

		$record  = $this->model_shared->getRecord_SecondDB_MultipleWhere('*',CASE_SETUP_UPLOAD_DATA_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id >' => $latest_setup_upload_data_id));
		$count   = $record->num_rows();

		if ($count > 0) 
		{
			foreach($record->result() as $row) 
			{
				$case_id			                = $row->case_id ;
				$getRecord                          = $this->model_shared->getRecord_SecondDB_MultipleWhere('case_reference_number,patient,distributor',INVENTORY_CASES_TABLE,array('case_id' => $case_id));
				$getRecord_row	                    = $getRecord->row_array();

				$data['setup_upload_data_id']	    = $row->id;
				$data['case_id_fk']			        = $getRecord_row['case_reference_number'];
				$data['case_id']			        = $row->case_id;
				$data['upload_link']		        = $row->upload_link;
				$data['password']                   = $row->password;
				$data['is_deletable ']              = $row->is_deletable ;
				$data['created']                    = $row->created;
				$data['created_by']                 = $row->created_by;
				$data['created_by_reference_table'] = $row->created_by_reference_table;
				$data['is_deleted ']                = $row->is_deleted ;
				$data['deleted_by']                 = $row->deleted_by;
				/*$data['receive_date']        = $row->receive_date;*/
				$data['deleted_by_reference_table'] = $row->deleted_by_reference_table;
				$data['deleted']                    = $row->deleted;

				// Save All data Into tbl_application_inventory_cases
				$setupLinkTableID					= 	$this->model_shared->insertRecord_ReturnID(CASE_SETUP_UPLOAD_DATA_TABLE,$data);

				$case_id_fk			                = $getRecord_row['case_reference_number'];
				$patient			                = $getRecord_row['patient'];
				$company 			                = $getRecord_row['distributor'];

			    if($company==ALIGNERCO)
				{
					$companyEmail                    = ALIGNERCO_EMAIL;
					$companyPassword                 = ALIGNERCO_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'AlignerCo';
					$email_data['FullDomainName']	 = 'portal.alignerco.com';
					$to_email                        = array('ele@alignerco.com', 'support@alignerco.com');
				}
				if($company==ALIGNERCO_CANADA)
				{
					$companyEmail                    = ALIGNERCO_CANADA_EMAIL;
					$companyPassword                 = ALIGNERCO_CANADA_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'AlignerCo Canada';
					$email_data['FullDomainName']	 = 'portal.alignerco.ca';
					$to_email                        = array('alice@alignerco.ca', 'team@alignerco.ca');
				}
				if($company==STRAIGHT_MY_TEETH)
				{
					$companyEmail                    = STRAIGHT_MY_TEETH_EMAIL;
					$companyPassword                 = STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'Straight My Teeth';
					$email_data['FullDomainName']	 = 'portal.straightmyteeth.com';
					$to_email                        = array('sarah@straightmyteeth.com', 'support@straightmyteeth.com');
				}
				if($company==SMILEPATH)
				{
					$companyEmail                   = SMILEPATH_EMAIL;
					$companyPassword                = SMILEPATH_EMAIL_PASSWORD;
					$email_data['companyName']	    = 'SmilePath';
					$email_data['FullDomainName']	= 'portal.smilepath.com.au';
					$to_email                       = array('jamie@smilepath.com.au', 'team@smilepath.com.au');
				}


				/*===================email code start================*/
                $this->load->library('email');
                $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => $companyEmail,
                'smtp_pass' => $companyPassword,
                'smtp_timeout'=>20,
                'mailtype'  => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
				);


                $this->load->library('email');
                $this->email->initialize($config);
                $this->email->set_mailtype("html");
                $this->email->set_newline("\r\n");

                $email_data['case']			 = $case_id_fk;
                $email_data['patient']	     = $patient;

                $message                     = $this->load->view('cpanel/emails/setup_preview_email', $email_data, true);
             
                $this->email->to($to_email);
                $this->email->subject('New Preview Uploaded');
                $this->email->message($message);

                if($this->email->send())
                    $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
                else
                    $this->session->set_flashdata("email_sent","You have encountered an error");
				/*===========================email code end====================*/

				/*$list = array('one@example.com', 'two@example.com', 'three@example.com');
                $this->email->to($list);*/
				
			}
	 

			
		}


	}



	





}
?>

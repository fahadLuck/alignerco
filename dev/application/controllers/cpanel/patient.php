<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Aws\S3\S3Client;
 require 'vendor/autoload.php';

error_reporting(0);

class Patient extends CI_Controller {

	public  function __construct() {
		
		parent::__construct();
		
		//authentication(); // Calling From Login Helper
		
		$this->load->library('user_agent');
		
		/* Table ID*/
		$this->accountID = $this->session->userdata(USER_ACCOUNT_ID_SESSION);
		
		/*Team Member ID*/
		$this->teamID   = $this->session->userdata(USER_TEAM_ID_SESSION);
		
		/*Team Member ID*/
		$this->HRID   = $this->session->userdata(USER_HR_ID_SESSION);
		
		/*Team Member Institute ID*/
		$this->organizationID	= $this->session->userdata(USER_ORGANIZATION_ID_SESSION);
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		$this->load->helper('cpanel/users/users');
		$this->load->model('cpanel/users/model_users');
		
		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');

		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		
		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper
		
		/*if (!in_array(MODULE_USERS,$this->accessModules)) {
				
				redirect('my-dashboard/');	
		}*/
		
	}
	
	
	public  function index() { 
				
				/*if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
				}*/
				
				$accountID			= $this->accountID;
				$teamID				= $this->teamID;   
				$organizationID		= $this->organizationID;
				
				/* User Roles And Permission
		
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
		
				$userInfo 						= userInfo($accountID); // Calling From Application Helper
				
				$assignedRoles 					= $this->assignedRoles;
				$accessModules 					= $this->accessModules;
					
				$searchCode						= $this->input->post('search_code');
				$searchName						= $this->input->post('search_name');
				$searchRole						= $this->input->post('search_role');
				
				$searchParms					= array();
				
				$searchParms['searchCode']		= $searchCode;
				$searchParms['searchName']		= $searchName;
				$searchParms['searchRole']		= $searchRole;
				
				$result['assignedRoles']		= $assignedRoles;
				$result['accessModules']		= $accessModules;
				
				$result['organizationID']		= $organizationID;		
				
				$result['roles']      			= getRoles($organizationID); // Calling From User Helper
				$result['patients']				= $this->model_users->getPatients($organizationID,$searchParms); 
						
				$data['pageHeading']    		= '<i class="fa fa-user-circle"></i> Patients';
				$data['subHeading']    			= "";
				
				$data['userInfo']            	= $userInfo;
				$data['activeMenu']  			= '8';
				$data['activeSubMenu']  		= '8.1';
				
				$data['metaType']     			= 'internal';
				$data['pageName']    			= 'Patient';
				$data['pageTitle']      		= 'Patient | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']	  			= $this->load->view('cpanel/patient/patient/patient_list',$result,true);
				$this->load->view('cpanel/template',$data);		
	}
	
	public  function patientAdd() { 
				
				/*if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
				}*/
				
				$accountID								= $this->accountID;
				$teamID									= $this->teamID;   
				$organizationID							= 2;
				
				
				/* User Roles And Permission
		
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
		
				$assignedRoles 		= $this->assignedRoles;
				$accessModules 		= $this->accessModules;

				// to get current domain url
				 $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

				 $FullDomainName = $_SERVER['HTTP_HOST'] . '/';
			     $domainName     = $_SERVER['HTTP_HOST'] . '/';
			     $domainName     = str_replace('https://','',$domainName);
			     $domainName     = str_replace('/','',$domainName);
			       if($domainName==ALIGNERCO_DOMAIN)
					{
						$companyEmail=ALIGNERCO_EMAIL;
						$companyPassword=ALIGNERCO_EMAIL_PASSWORD;
						$companyName	= 'AlignerCo';
					}
					if($domainName==ALIGNERCO_CANADA_DOMAIN)
					{
						$companyEmail=ALIGNERCO_CANADA_EMAIL;
						$companyPassword=ALIGNERCO_CANADA_EMAIL_PASSWORD;
						$companyName	= 'AlignerCo Canada';
					}
					if($domainName==STRAIGHT_MY_TEETH_DOMAIN)
					{
						$companyEmail=STRAIGHT_MY_TEETH_EMAIL;
						$companyPassword=STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
						$companyName	= 'Straight My Teeth';
					}
					if($domainName==SMILEPATH_DOMAIN)
					{
						$companyEmail=SMILEPATH_EMAIL;
						$companyPassword=SMILEPATH_EMAIL_PASSWORD;
						$companyName	= 'SmilePath';
					}

				
				 /* Set Form Validation Errors */ 
			  	//$this->form_validation->set_rules('employee','employee','trim|required|callback_check_already_exist_user_account');
			  	//
			 	
			 	/*capitcha code start*/
			  	$captcha_response = trim($this->input->post('g-recaptcha-response'));
				/*if($captcha_response != '')
				{*/
					$keySecret = '6Ld2josaAAAAALGhDZ21fhN_m4h_3N1puns2uLLk';

					$check = array(
						'secret'		=>	$keySecret,
						'response'		=>	$this->input->post('g-recaptcha-response')
					);

					$startProcess = curl_init();

					curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");

					curl_setopt($startProcess, CURLOPT_POST, true);

					curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));

					curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);

					curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);

					$receiveData       = curl_exec($startProcess);
					$finalResponse     = json_decode($receiveData, true);
					$captchaResponse   = $finalResponse['success'];
					 
				/*}*/	 
			  	/*capitcha code end*/	

			  	$this->form_validation->set_rules('email','email','trim|required|callback_check_already_exist_email');

			  	$this->form_validation->set_rules('username','Full Name','trim|required');

			  	$this->form_validation->set_rules('password','password','trim|required|min_length[5]');
				$this->form_validation->set_rules('confirm_password','confirm password','trim|required|matches[password]');

				//$this->form_validation->set_rules('account_status','account_status','trim|required');
			   
			  	$this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
			  
			 	if($this->form_validation->run() === FALSE OR $captchaResponse=='') {
				        
						//$userInfo 								=  userInfo($accountID); // Calling From Application Helper
						
						if($captchaResponse=='') 
						{
							$this->session->set_flashdata('error', 'Google Captcha Is Required.');
						}
						
					
						$result['assignedRoles']				=  $assignedRoles;
						$result['accessModules']				=  $accessModules;
						$result['companyName']				    =  $companyName;
						
						$result['employeesForUser']				=  getEmployeesForUsers($organizationID);  // Calling From Users Helper
						$result['roles']      			   	    =  getRoles($organizationID); // Calling From User Helper
						
						$data['pageHeading']    				=  '<i class="fa fa-user-circle"></i> Patient';
						//$data['subHeading']    					=  "(new)";
						
						//$data['userInfo']            			=  $userInfo;
						$data['activeMenu']  					=  '8';
						$data['activeSubMenu']  				=  '8.2';
				
						
						$data['metaType']     					=  'internal';
						$data['pageName']    					=  'Patient';
						$data['pageTitle']      				=  'Patient | '.DEFAULT_APPLICATION_NAME;
				
						$data['contents']	  					= $this->load->view('cpanel/patient/patient/patient_add',$result,true);
						$this->load->view('cpanel/template',$data);		
				
				} else {
					
						//$employeeID					 			= ($this->input->post('employee') ?: NULL);
						
						$email					 			    = $this->input->post('email');
						$username					 			= $this->input->post('username');
						$password								= $this->input->post('password');
						$employeePassword						= md5(md5($this->input->post('password')));
						$showPassword							= encodeString($this->input->post('password')); // Calling From General Helper
						$role									= ROLE_PATIENT;
						$accountStatus					 		= HARD_CODE_ID_ACTIVATED;					

						/*$result     							= $this->getEmployeeInfoByID($organizationID,$employeeID);
						
						$row 	   								= $result->row_array();
						
						$employeeID								= $row['employeeID'];
						$emploeeEmail							= $row['employeeEmail'];
						$emploeeMobile							= $row['employeeMobile'];
						$employeeCode							= $row['employeeCode'];*/
						
						    // Prepair Date To Store In Database
							$dataArray['organization_id']					   	=  $organizationID;
							$dataArray['type']					   	            =  HARD_CODE_ID_USER_TYPE_PATIENT;
							//$dataArray['team_id']					   			=  $employeeID;
							$dataArray['domain']					   	        =  $domainName;
							$dataArray['email']									=  $email;
							//$dataArray['code']									=  $employeeCode;
							$dataArray['username']								=  $username;
							$dataArray['password']  							=  $employeePassword;   
							$dataArray['show_password']  						=  $showPassword;   
							//$dataArray['mobile']  								=  $emploeeMobile;    
							$dataArray['account_status']  						=  $accountStatus;   
							
							$dataArray['created']  	 			 				=  DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
							//$dataArray['created_by']  	 			 			=  $teamID; 
							//$dataArray['created_by_reference_table'] 			=  'MY_ORGANIZATION_TEAM_TABLE';  
							
						     $insertedID =	$this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,$dataArray);



						     //concent fom fields
						    $concent_data['account_id']		         =  $insertedID;
							$concent_data['bonded_retainer']		 =  $this->input->post('bonded_retainer');
							$concent_data['bridgework']				 =  $this->input->post('bridgework');
							$concent_data['crowns']					 =  $this->input->post('crowns');
							$concent_data['impacted_tooth']			 =  $this->input->post('impacted_tooth');
							$concent_data['implant']				 =  $this->input->post('implant');
							$concent_data['primary_teeth']			 =  $this->input->post('primary_teeth');
							$concent_data['veneers']				 =  $this->input->post('veneers');
							$concent_data['radiograph_of_teeth']	 =  $this->input->post('radiograph_of_teeth');
							$concent_data['pain_in_teeth']			 =  $this->input->post('pain_in_teeth');
							$concent_data['sores_or_lumps']			 =  $this->input->post('sores_or_lumps');
							$concent_data['serious_dental_issue']	 =  $this->input->post('serious_dental_issue');
							$concent_data['head_neck_jaw_injuries']	 =  $this->input->post('head_neck_jaw_injuries');
							$concent_data['jaw_clicking_pain_difficulty_opening_closing'] =  $this->input->post('jaw_clicking_pain_difficulty_opening_closing');
							$concent_data['loosening_teeth']		 =  $this->input->post('loosening_teeth');
							$concent_data['allergies']				 =  $this->input->post('allergies');
							$concent_data['bisphosphonate_treatment']=  $this->input->post('bisphosphonate_treatment');
							$concent_data['acute_corticosteroids']	 =  $this->input->post('acute_corticosteroids');
							$concent_data['bone_marrow_transplant']	 =  $this->input->post('bone_marrow_transplant');
							$concent_data['patient_age']			 =  $this->input->post('patient_age');
							$concent_data['patient_name']			 =  $this->input->post('patient_name');
							$concent_data['date_signed']			 =  $this->input->post('date_signed');
							$concent_data['legally_responsible_person'] =  $this->input->post('legally_responsible_person');
							$concent_data['relationship_to_patient'] =  $this->input->post('relationship_to_patient');
							$concent_data['underage_date_signed']	 =  $this->input->post('underage_date_signed');
							$concent_data['created']	             =  DATABASE_NOW_DATE_TIME_FORMAT();

							$this->model_shared->insertRecord_ReturnID(PATIENT_CONCENT_FORM_TABLE,$concent_data);
							//concent fom fields
							 
							 	/*Assign User Multiple Roles*/
								if ($role) 
								{
												
									for ($i=0; $i<sizeof($role); $i++)  {
												
												$dataRoleAssign['organization_id']						=	$organizationID;
												$dataRoleAssign['team_official_id']						=	$insertedID;
												//$dataRoleAssign['team_id']								=	$employeeID;
												$dataRoleAssign['role_id']								=	ROLE_PATIENT;
												$dataRoleAssign['created']								=	DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
												//$dataRoleAssign['created_by']							=	$teamID;
												//$dataRoleAssign['created_by_reference_table']			=	'MY_ORGANIZATION_TEAM_TABLE';
												
												$insertedAssignRoleID  =  $this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE,$dataRoleAssign);
									}
								}

								// to get current domain url
								 $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

								 $FullDomainName = $_SERVER['HTTP_HOST'] . '/';
							     $domainName     = $_SERVER['HTTP_HOST'] . '/';
							     $domainName     = str_replace('https://','',$domainName);
							     $domainName     = str_replace('/','',$domainName);



							  
							    if($domainName==ALIGNERCO_DOMAIN)
								{
									$companyEmail    = ALIGNERCO_EMAIL;
									$companyPassword = ALIGNERCO_EMAIL_PASSWORD;
									$companyName	 = 'AlignerCo';
								}
								if($domainName==ALIGNERCO_CANADA_DOMAIN)
								{
									$companyEmail    = ALIGNERCO_CANADA_EMAIL;
									$companyPassword = ALIGNERCO_CANADA_EMAIL_PASSWORD;
									$companyName	 = 'AlignerCo Canada';
								}
								if($domainName==STRAIGHT_MY_TEETH_DOMAIN)
								{
									$companyEmail    = STRAIGHT_MY_TEETH_EMAIL;
									$companyPassword = STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
									$companyName	 = 'Straight My Teeth';
								}
								if($domainName==SMILEPATH_DOMAIN)
								{
									$companyEmail    = SMILEPATH_EMAIL;
									$companyPassword = SMILEPATH_EMAIL_PASSWORD;
									$companyName	 = 'SmilePath';
								}
							    

								/*===================email code start================*/
				
			                    $this->load->library('email');
			                    $config = Array(
			                    'protocol' => 'smtp',
				                'smtp_host' => 'ssl://smtp.gmail.com',
				                'smtp_port' => 465,
				                'smtp_user' => $companyEmail,
				                'smtp_pass' => $companyPassword,
				                'smtp_timeout'=>20,
				                'mailtype'  => 'html',
				                'charset' => 'iso-8859-1',
				                'wordwrap' => TRUE
								);


			                    $this->load->library('email');
			                    $this->email->initialize($config);
			                    $this->email->set_mailtype("html");
			                    $this->email->set_newline("\r\n");

			                    $email_data['email']			        = $email;
			                    $email_data['username']			        = $username;
			                    $email_data['password']	                = $password;
			                    $email_data['FullDomainName']	        = $FullDomainName;
			                    $email_data['companyName']	            = $companyName;

			                    $message   = $this->load->view('cpanel/emails/patient_account_email', $email_data, true);
			                    $to_email  = PRODUCTION_EMAIL;
			                    $subject   = 'Welcome To '.$companyName;
			                 
			                 	/*$this->email->from($companyEmail,$companyName);*/
			                    $this->email->to($email);
			                    $this->email->subject($subject);
			                    $this->email->message($message);

			                    if($this->email->send())
			                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
			                    else
			                        $this->session->set_flashdata("email_sent","You have encountered an error");

							

							/*===========================email code end====================*/


								
								$this->session->set_userdata('admin_success_msg','Check your email for login information.');
								redirect('patient/');
				}
	}

	public  function uploadImages() 
	{
		authentication(); // Calling From Login Helper
		
		
		$accountID			= $this->accountID;
		$teamID				= $this->teamID;   
		$organizationID		= $this->organizationID;
		$ID      			= $accountID;
		
		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		
		
		$assignedRoles 					= $this->assignedRoles;
		$accessModules 					= $this->accessModules;
		$result['assignedRoles']		= $assignedRoles;
		$result['accessModules']		= $accessModules;

		if (!in_array(PATIENT_PICTURES,$this->accessModules)) {

			redirect('my-dashboard/');
		}
			
		$searchCode						= $this->input->post('search_code');
		$searchName						= $this->input->post('search_name');
		$searchRole						= $this->input->post('search_role');		
		
		$photos		                    = $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_PATIENT_PICTURES_TABLE,array('account_id' => $ID,'is_deleted' => HARD_CODE_ID_NO),'id','DESC');

		$pictureType		                    = $this->model_shared->getRecordMultipleWhereOrderBy('*',PICTURE_TYPE_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'id','DESC');

		$Steps		                    = $this->model_shared->getRecordMultipleWhereOrderBy('*',TREATMENT_STEPS_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'id','ASC');

		$result['ID']      			    = $accountID;
		$result['photos']				= $photos;
		$result['pictureType']			= $pictureType;
		$result['Steps']				= $Steps;
				
		$data['pageHeading']    		= '<i class="fa fa-user-circle"></i> Patient Images';
		$data['subHeading']    			= "";
		
		$data['userInfo']            	= $userInfo;
		$data['activeMenu']  			= '8';
		$data['activeSubMenu']  		= '8.3';
		
		$data['metaType']     			= 'internal';
		$data['pageName']    			= 'Patient';
		$data['pageTitle']      		= 'Patient | '.DEFAULT_APPLICATION_NAME;
		
		$data['contents']	  			= $this->load->view('cpanel/patient/patient/patient_pictures',$result,true);
		$this->load->view('cpanel/template',$data);	
	}


	public  function viewImages($ID) 
	{
		//authentication(); // Calling From Login Helper
		
		
		$accountID			          = $this->accountID;
		$teamID				          = $this->teamID;   
		$organizationID		          = $this->organizationID;
		$ID      			          = decodeString($ID);
		
		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
	

		// check last id view status and then update last id view status 
		$account_ID 		            = $ID;
	    $recordperpage                  = 1;
	    $page                           = 0;
	    $status_result                  = $this->model_shared->page_listing_where($page,$recordperpage,'id,account_id,view_status',MEDICAL_CASE_PATIENT_PICTURES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'account_id' => $account_ID),'id','DESC');
	    $status_row                     = $status_result->row();
	    $viewStatus                     = $status_row->view_status;
	    $lastID                         = $status_row->id;
	    $statusDataArray['view_status'] = HARD_CODE_ID_PICTURE_SEEN;

	    $affectedRows                   = $this->model_shared->editRecordWhere(array('account_id' => $account_ID,'id' => $lastID),MEDICAL_CASE_PATIENT_PICTURES_TABLE,$statusDataArray);
	    // check last id view status and then update last id view status 

		
		
		$assignedRoles 					= $this->assignedRoles;
		$accessModules 					= $this->accessModules;
			
		$searchCode						= $this->input->post('search_code');
		$searchName						= $this->input->post('search_name');
		$searchRole						= $this->input->post('search_role');
		
		$searchParms					= array();
		
		$searchParms['searchCode']		= $searchCode;
		$searchParms['searchName']		= $searchName;
		$searchParms['searchRole']		= $searchRole;
		
		$result['assignedRoles']		= $assignedRoles;
		$result['accessModules']		= $accessModules;
		
		$result['organizationID']		= $organizationID;		
		
		$photos		                    = $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_PATIENT_PICTURES_TABLE,array('account_id' => $ID,'is_deleted' => HARD_CODE_ID_NO),'id','DESC');

		$result['ID']      			    = $accountID;
		$result['photos']				= $photos;
				
		$data['pageHeading']    		= '<i class="fa fa-user-circle"></i> Patient Images';
		$data['subHeading']    			= "";
		
		$data['userInfo']            	= $userInfo;
		$data['activeMenu']  			= '8';
		$data['activeSubMenu']  		= '8.1';
		
		$data['metaType']     			= 'internal';
		$data['pageName']    			= 'Patient';
		$data['pageTitle']      		= 'Patient | '.DEFAULT_APPLICATION_NAME;
		
		$data['contents']	  			= $this->load->view('cpanel/patient/patient/view_patient_pictures',$result,true);
		$this->load->view('cpanel/template',$data);	
	}


	function uploadPatientPictures($ID) {

		if ($_FILES['file']['name']) {
	   $filesCount = count($_FILES['file']['name']);
	   for($i = 0; $i < $filesCount; $i++){
		$client = new Aws\S3\S3Client([
		        'version' => 'latest',
		        'region'  => 'us-east-1',
		        'endpoint' => 'https://nyc3.digitaloceanspaces.com',
		        'credentials' => [
		                'key'    => 'IDXGNGG5BN4R2CF4RKHO',
		                'secret' => '+E8y7SNAtmOLO62aSr/CkdL2elAqu6cTGr81uAvyuCE',
		            ],
		]);

			$accountID				= $this->accountID;
			$teamID					= $this->teamID;
			$organizationID 		= $this->organizationID;

				$fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file']['name'][$i])); // Calling From General Helper
				$fname 			= str_replace(" ","_",$fname);
				$fname 			= str_replace("%","_",$fname);
				$nameExt 		= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['file']['name'][$i])))); // Calling From General Helper

				$fileName		= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file']['name'][$i]))); // Calling From General Helper

				$uploaddir 		= CASE_PICTURES_PATH_DIRECTORY_PATH;
				$uploadfile 	= $uploaddir .$fname;



				if($nameExt == 'jpg'  || $nameExt == 'jpeg' ||  $nameExt =='gif' || $nameExt =='png' || $nameExt =='pdf')  {

				// if($_FILES["file"]["size"] <= 1000000) {

					/*if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {*/

						$result = $client->putObject([
							     'Bucket' => 'abucket',
							     'Key'    => str_replace(' ', '', $fname),
							     'Body'   => fopen($_FILES['file']['tmp_name'][$i], 'r'),
							     'ACL'    => 'public-read',
							     'ContentType' => 'image/jpeg/png/pdf'
							]);



					   $client->waitUntil('ObjectExists', array(
					              'Bucket' => 'abucket',
					              'Key'    =>  $fname
					          ));

					   $public_url='https://abucket.nyc3.digitaloceanspaces.com/'.$fname;

						  $source_image_path    = CASE_PICTURES_PATH_DIRECTORY_PATH;
						  $source_image_name    = $fname;

						 /* $this->resizeImage($source_image_path,CASE_PICTURES_PATH_THUMBNAIL_DIRECTORY_PATH,$fname,"thumbnail_",100,100);*/

						  $pictureOf                            = $this->input->post('picture_of');
						  if($pictureOf==PICTURE_TYPE_ID_STEPS)
						  {
						  	$steps                              = $this->input->post('steps');
						  }
						  else 
						  {
						  	$steps                               = NULL;
						  }
						  	
        
						  $data['account_id']  					=   $accountID;
						  $data['picture_of']  					=   $pictureOf;
						  $data['steps']  				        =   $steps;
						  $data['account_id']  					=   $accountID;
						  $data['name']  						=   $fname;
						  $data['type']  						=   $nameExt;
						  $data['public_url']  				    =   $public_url;
						  $data['is_deletable']					=	HARD_CODE_ID_YES;
						  $data['created']						=  	DATABASE_NOW_DATE_TIME_FORMAT();
						  //$data['created_by']					=   $teamID;
						  //$data['created_by_reference_table']	=   'MY_ORGANIZATION_TEAM_TABLE';

						  // Save All data Into Database Table
						  $insertedID							= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_PATIENT_PICTURES_TABLE,$data);
					   /*}*/

				//  }
				}

			}


		}

		$this->session->set_userdata('admin_msg','Pictures Successfully Uploaded');
		redirect('patient-images/');
	}


	
	public  function patientEdit($tableID) { 
		
			/* CheckEmptySefURL($tableID);*/  // Calling From Shared Helper
			  
			 /*if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
			  }*/
			 
			  $accountID			= $this->accountID;
			  $teamID				= $this->teamID;   
			  $organizationID		= $this->organizationID;
			  
			  /* User Roles And Permission
		
				   - Check Allow Permision or Not
				   - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			  */
			  
			  $assignedRoles 		= $this->assignedRoles;
			  $accessModules 		= $this->accessModules;
			  
			  $tableID				= decodeString($tableID);
			  
			  $patient     		    = $this->model_users->getPatientDetails($organizationID,$tableID);
			  
			  $patient			   	= $patient->row_array(); 
			  
			  $tableID 				= $patient['tableID']; 
			  /*$patientID 			= $patient['accountID'];
			  $username 		    = $patient['username'];*/


			$submit                 = $this->input->post('submit');
			$familyAccount          = $this->input->post('family_account');


			if(!isset($submit))
			{
				$this->load->view('cpanel/template',$data);
			}




			else
			{
			
			  /* Set Form Validation Errors */ 
			  if($familyAccount!=HARD_CODE_ID_YES)
			  {
			  	$this->form_validation->set_rules('email','email','trim|callback_duplicate_email');
			  }
			  
			  $this->form_validation->set_rules('CaseID','CaseID','trim|callback_duplicate_CaseID');
			  $this->form_validation->set_rules('password','password','trim|min_length[5]');
			  $this->form_validation->set_rules('confirm_password','confirm password','trim|matches[password]');
			   
			  $this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
			  
			  if($this->form_validation->run() === FALSE ) {

			  	        $result['submit']                       =$submit;
		
						$userInfo 								= userInfo($accountID); // Calling From Application Helper
						
						$roles      			   	  		    =  getRoles($organizationID); // Calling From User Helper
						$employeeRoles							=  getEmployeeAssignedRoles($organizationID,$employeeID); // Calling From User Helper
						
						$result['assignedRoles']				=  $assignedRoles;
						$result['accessModules']				=  $accessModules;
						
						$result['roles'] 						= $roles;

						
						$result['employeesForUser']				=  getEmployeesForUsers($organizationID);  // Calling From Users Helper
						
						$result['patient']      				= $patient;
						$result['employeeRoles']				= $employeeRoles;
						
						$result['tableID']      				= encodeString($tableID); // Calling From General Helper
					
						$data['pageHeading']    				='<i class="fa fa-user-circle"></i> Patient';
						$data['subHeading']    					= "(edit)";
						
						$data['userInfo']            			= $userInfo;
						
						$data['activeMenu']  					= '8';
						$data['activeSubMenu']  				= '8.1';
						
						$data['metaType']     					= 'internal';
						$data['pageName']    					= 'Patient';
						$data['pageTitle']      				= 'Patient | '.DEFAULT_APPLICATION_NAME;
						
						
						$data['contents']	 = $this->load->view('cpanel/patient/patient/patient_edit',$result,true);
						$this->load->view('cpanel/template',$data);
		
			 } else {

						 $email								    = ($this->input->post('email') ?: NULL);
						 $username								= ($this->input->post('username') ?: NULL);

						 $CaseID								= ($this->input->post('CaseID') ?: NULL);
						  $password								= ($this->input->post('password') ?: NULL);
						 
						 if ($password) {
						
							 $employeePassword					= md5(md5($this->input->post('password')));
						 	 $showPassword						= encodeString($this->input->post('password')); // Calling From General Helper
						 }
						 
						 $accountStatus					 		= ($this->input->post('account_status') ?: NULL);
						 
						
						    // Prepair Date To Store In Database
							if ($password) {
								
									$dataArray['password']  		=  $employeePassword;   
									$dataArray['show_password']  	=  $showPassword;  
									
							}
							
							$dataArray['account_status']  			=  $accountStatus; 
							$dataArray['code']  					=  $CaseID;
							$dataArray['email']  					=  $email;
							$dataArray['username']  				=  $username;      
							
							$affectedRows = $this->model_shared->editRecordWhere(array('id' => $tableID),MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,$dataArray);

							//update case_id in MEDICAL_CASE_PATIENT_PICTURES_TABLE 
							$patientPictureArray['case_id']  		=  $CaseID;
							$patientPicturesAffectedRows            = $this->model_shared->editRecordWhere(array('account_id ' => $tableID),MEDICAL_CASE_PATIENT_PICTURES_TABLE,$patientPictureArray);
							
								/*Assign Employee Multiple Roles*/
								if (!empty($role)) { 
									
									$affectedRows = $this->model_shared->deleteRecordWhere(array('team_id' => $employeeID),MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE);
									
									for ($i=0; $i<sizeof($role); $i++)  {
												
												$dataRoleAssign['organization_id']						=	$organizationID;
												$dataRoleAssign['team_id']								=	$employeeID;
												$dataRoleAssign['role_id']								=	$role[$i];
												$dataRoleAssign['created']								=	DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
												$dataRoleAssign['created_by']							=	$teamID;
												$dataRoleAssign['created_by_reference_table']			=	'MY_ORGANIZATION_TEAM_TABLE';
												
												$insertedAssignRoleID 	=  $this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE,$dataRoleAssign);
									}
										
							    }
								
								$this->session->set_userdata('admin_msg','Record successfully updated.');
								
								redirect('patient-list/');
			}
		}	
						
	}





	public  function patientConcentForm($tableID) { 
		
			/* CheckEmptySefURL($tableID);*/  // Calling From Shared Helper
			  
			 /*if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
			  }*/
			 
			$accountID			                    = $this->accountID;
			$teamID				                    = $this->teamID;   
			$organizationID		                    = $this->organizationID;
			  
			  /* User Roles And Permission
		
				   - Check Allow Permision or Not
				   - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			  */
			  
			$assignedRoles 		                    = $this->assignedRoles;
			$accessModules 		                    = $this->accessModules;
			$tableID				                = decodeString($tableID);
			  
			$patient                                = $this->model_shared->getRecordMultipleWhere('*',PATIENT_CONCENT_FORM_TABLE,array('account_id' => $tableID, 'is_deleted' => HARD_CODE_ID_NO));	
			  
			$patient			   	                = $patient->row_array();   
			$tableID 				                = $patient['tableID']; 
			$userInfo 								= userInfo($accountID); // Calling From Application Helper
			
			$roles      			   	  		    =  getRoles($organizationID); // Calling From User Helper
			$employeeRoles							=  getEmployeeAssignedRoles($organizationID,$employeeID); // Calling From User Helper
			
			$result['assignedRoles']				=  $assignedRoles;
			$result['accessModules']				=  $accessModules;
			$result['roles'] 						=  $roles;

			
			$result['employeesForUser']				=  getEmployeesForUsers($organizationID);  // Calling From Users Helper
			
			$result['patient']      				= $patient;
			$result['employeeRoles']				= $employeeRoles;
			
			$result['tableID']      				= encodeString($tableID); // Calling From General Helper
		
			$data['pageHeading']    				='<i class="fa fa-user-circle"></i> Patient';
			$data['subHeading']    					= "(Consent Form)";
			
			$data['userInfo']            			= $userInfo;
			
			$data['activeMenu']  					= '8';
			$data['activeSubMenu']  				= '8.1';
			
			$data['metaType']     					= 'internal';
			$data['pageName']    					= 'Patient';
			$data['pageTitle']      				= 'Patient | '.DEFAULT_APPLICATION_NAME;
			
			
			$data['contents']	 = $this->load->view('cpanel/patient/patient/patient_concent_form',$result,true);
			$this->load->view('cpanel/template',$data);
		
			 	
						
	}




	public function pictureAction() 
    {
    	/*$client = new Aws\S3\S3Client([
		        'version' => 'latest',
		        'region'  => 'us-east-1',
		        'endpoint' => 'https://nyc3.digitaloceanspaces.com',
		        'credentials' => [
		                'key'    => 'MQAALP3WZFIMJ6M7UZMD',
		                'secret' => 'NtdVXMjxwAreF0yOdkW+rVWRKH9LCPueNNa8ulKALRQ',
		            ],
		]);*/

        /*if(isset($_POST['active_alert']) AND count($_POST['delete']) > 0)
          {*/
            $files=$_POST['delete']; 

            /*foreach($_POST['delete'] as $public_url)
            {
                echo"<br>".$public_url= $public_url;
                $file_name = $public_url;
			    $file_url = 'https://acportal-bucket.nyc3.digitaloceanspaces.com/' . $file_name;
				header('Content-Description: File Transfer');
		        header('Content-Type: application/octet-stream');
		        header('Content-Disposition: attachment; filename='.basename($file_url));
		        header('Content-Transfer-Encoding: binary');
		        header('Expires: 0');
		        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		        header('Pragma: public');
		        ob_clean();
		        flush();
		        readfile($file_url);
       
            }*/ 

	           $tmpFile = tempnam('/tmp', '');

				$zip = new ZipArchive;
				$zip->open($tmpFile, ZipArchive::CREATE);
				foreach ($files as $file) {
				    // download file
				    $fileContent = file_get_contents($file);

				    $zip->addFromString(basename($file), $fileContent);
				}
				$zip->close();

				header('Content-Type: application/zip');
				header('Content-disposition: attachment; filename=file.zip');
				header('Content-Length: ' . filesize($tmpFile));
				readfile($tmpFile);

				unlink($tmpFile);
        /* }*/ 

       /*$this->session->set_userdata('admin_msg','No File Selected.Please select a file and then download.');*/

		/*if ($this->agent->is_referral())
		{
		    $url=$this->agent->referrer();
		    redirect($url);
		}*/

        
    }


    public  function removePatientPicture($ID) 
	{
		$client = new Aws\S3\S3Client([
		        'version' => 'latest',
		        'region'  => 'us-east-1',
		        'endpoint' => 'https://nyc3.digitaloceanspaces.com',
		        'credentials' => [
		                'key'    => 'MQAALP3WZFIMJ6M7UZMD',
		                'secret' => 'NtdVXMjxwAreF0yOdkW+rVWRKH9LCPueNNa8ulKALRQ',
		            ],
		]);

		$picID              = decodeString($ID); 
		
		
		$accountID			= $this->accountID;
		$teamID				= $this->teamID;   
		$organizationID		= $this->organizationID;
		
		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		
		
		$assignedRoles 					= $this->assignedRoles;
		$accessModules 					= $this->accessModules;
		/*if (!in_array(PATIENT_PICTURES,$this->accessModules)) {

			redirect('my-dashboard/');
		}*/
					
		
		$photo  	                    = $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_PATIENT_PICTURES_TABLE,array('id' => $picID,'is_deleted' => HARD_CODE_ID_NO),'id','DESC')->row_array();
		$pic_name                       = $photo['name'];

		$client->deleteObject([
		    'Bucket' => 'acportal-bucket',
		    'Key' => $pic_name,
		]);

		$this->model_shared->deleteRecord('id',$picID,MEDICAL_CASE_PATIENT_PICTURES_TABLE);

		$this->session->set_userdata('admin_msg','Record successfully deleted.');

		if ($this->agent->is_referral())
		{
		    $url=$this->agent->referrer();
		    redirect($url);
		}

		
	}



	public  function familyAccounts() { 
				
				/*if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
				}*/
				
				$accountID			= $this->accountID;
				$teamID				= $this->teamID;   
				$organizationID		= $this->organizationID;
				
				/* User Roles And Permission
		
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
		
				$userInfo 						= userInfo($accountID); // Calling From Application Helper
				
				$assignedRoles 					= $this->assignedRoles;
				$accessModules 					= $this->accessModules;
					
				$searchCode						= $this->input->post('search_code');
				$searchName						= $this->input->post('search_name');
				$searchRole						= $this->input->post('search_role');
				
				$searchParms					= array();
				
				$searchParms['searchCode']		= $searchCode;
				$searchParms['searchName']		= $searchName;
				$searchParms['searchRole']		= $searchRole;
				
				$result['assignedRoles']		= $assignedRoles;
				$result['accessModules']		= $accessModules;
				
				$result['organizationID']		= $organizationID;		
				
				$result['roles']      			= getRoles($organizationID); // Calling From User Helper
				$result['familyAccounts']	    = $this->model_users->getPatientFamilyAccounts($organizationID,$searchParms,$accountID); 

				
						
				$data['pageHeading']    		= '<i class="fa fa-user-circle"></i> Family Accounts';
				$data['subHeading']    			= "";
				
				$data['userInfo']            	= $userInfo;
				$data['activeMenu']  			= '10';
				$data['activeSubMenu']  		= '10.1';
				
				$data['metaType']     			= 'internal';
				$data['pageName']    			= ' Family Accounts';
				$data['pageTitle']      		= ' Family Accounts | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']	  			= $this->load->view('cpanel/patient/patient/family_accounts_list',$result,true);
				$this->load->view('cpanel/template',$data);		
	}



	public  function createFamilyAccount() { 
				
				/*if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
				}*/
				
				$accountID								= $this->accountID;
				$teamID									= $this->teamID;   
				$organizationID							= 2;
				
				/* User Roles And Permission
		
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
		
				$assignedRoles 		= $this->assignedRoles;
				$accessModules 		= $this->accessModules;

				// to get current domain url
				 $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

				 $FullDomainName = $_SERVER['HTTP_HOST'] . '/';
			     $domainName     = $_SERVER['HTTP_HOST'] . '/';
			     $domainName     = str_replace('https://','',$domainName);
			     $domainName     = str_replace('/','',$domainName);
			       if($domainName==ALIGNERCO_DOMAIN)
					{
						$companyEmail=ALIGNERCO_EMAIL;
						$companyPassword=ALIGNERCO_EMAIL_PASSWORD;
						$companyName	= 'AlignerCo';
					}
					if($domainName==ALIGNERCO_CANADA_DOMAIN)
					{
						$companyEmail=ALIGNERCO_CANADA_EMAIL;
						$companyPassword=ALIGNERCO_CANADA_EMAIL_PASSWORD;
						$companyName	= 'AlignerCo Canada';
					}
					if($domainName==STRAIGHT_MY_TEETH_DOMAIN)
					{
						$companyEmail=STRAIGHT_MY_TEETH_EMAIL;
						$companyPassword=STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
						$companyName	= 'Straight My Teeth';
					}
					if($domainName==SMILEPATH_DOMAIN)
					{
						$companyEmail=SMILEPATH_EMAIL;
						$companyPassword=SMILEPATH_EMAIL_PASSWORD;
						$companyName	= 'SmilePath';
					}

				
				 /* Set Form Validation Errors */ 
			  	//$this->form_validation->set_rules('employee','employee','trim|required|callback_check_already_exist_user_account');
			  	//
			 	
			 	/*capitcha code start*/
			  	$captcha_response = trim($this->input->post('g-recaptcha-response'));
				/*if($captcha_response != '')
				{*/
					$keySecret = '6Ld2josaAAAAALGhDZ21fhN_m4h_3N1puns2uLLk';

					$check = array(
						'secret'		=>	$keySecret,
						'response'		=>	$this->input->post('g-recaptcha-response')
					);

					$startProcess = curl_init();

					curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");

					curl_setopt($startProcess, CURLOPT_POST, true);

					curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));

					curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);

					curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);

					$receiveData       = curl_exec($startProcess);
					$finalResponse     = json_decode($receiveData, true);
					$captchaResponse   = $finalResponse['success'];
					 
				/*}*/	 
			  	/*capitcha code end*/	

			  	$this->form_validation->set_rules('email','email','trim|required');

			  	$this->form_validation->set_rules('username','Full Name','trim|required');

			  	$this->form_validation->set_rules('password','password','trim|required|min_length[5]|callback_check_already_exist_password');
				$this->form_validation->set_rules('confirm_password','confirm password','trim|required|matches[password]');

				//$this->form_validation->set_rules('account_status','account_status','trim|required');
			   
			  	$this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
			  
			 	if($this->form_validation->run() === FALSE OR $captchaResponse=='') {
				
						//$userInfo 								=  userInfo($accountID); // Calling From Application Helper
						
						if($captchaResponse=='') 
						{
							$this->session->set_flashdata('error', 'Google Captcha Is Required.');
						}
						
					
						$result['assignedRoles']				=  $assignedRoles;
						$result['accessModules']				=  $accessModules;
						$result['companyName']				    =  $companyName;
						
						$result['employeesForUser']				=  getEmployeesForUsers($organizationID);  // Calling From Users Helper
						$result['roles']      			   	    =  getRoles($organizationID); // Calling From User Helper
						
						$data['pageHeading']    				=  '<i class="fa fa-user-circle"></i> Family Account';
						//$data['subHeading']    					=  "(new)";
						
						//$data['userInfo']            			=  $userInfo;
						$data['activeMenu']  					=  '10';
						$data['activeSubMenu']  				=  '10.2';
				
						
						$data['metaType']     					=  'internal';
						$data['pageName']    					=  'Family Account';
						$data['pageTitle']      				=  'Family Account | '.DEFAULT_APPLICATION_NAME;
				
						$data['contents']	  					= $this->load->view('cpanel/patient/patient/patient_family_account_add',$result,true);
						$this->load->view('cpanel/template',$data);		
				
				} else {
					
						//$employeeID					 			= ($this->input->post('employee') ?: NULL);
						
						$email					 			   = ($this->input->post('email') ?: NULL);

						$username					 			= ($this->input->post('username') ?: NULL);

						$password								= ($this->input->post('password') ?: NULL);
						$employeePassword						= md5(md5($this->input->post('password')));
						$showPassword							= encodeString($this->input->post('password')); // Calling From General Helper
						$role									= ROLE_PATIENT;
						$accountStatus					 		= HARD_CODE_ID_ACTIVATED;
						
						/*$result     							= $this->getEmployeeInfoByID($organizationID,$employeeID);
						
						$row 	   								= $result->row_array();
						
						$employeeID								= $row['employeeID'];
						$emploeeEmail							= $row['employeeEmail'];
						$emploeeMobile							= $row['employeeMobile'];
						$employeeCode							= $row['employeeCode'];*/
						
						    // Prepair Date To Store In Database
							$dataArray['organization_id']					   	=  $organizationID;
							$dataArray['type']					   	            =  HARD_CODE_ID_USER_TYPE_PATIENT;
							//$dataArray['team_id']					   			=  $employeeID;
							$dataArray['domain']					   	        =  $domainName;
							$dataArray['email']									=  $email;
							//$dataArray['code']									=  $employeeCode;
							$dataArray['username']								=  $username;
							$dataArray['password']  							=  $employeePassword;   
							$dataArray['show_password']  						=  $showPassword;   
							//$dataArray['mobile']  								=  $emploeeMobile;    
							$dataArray['account_status']  						=  $accountStatus;
							$dataArray['family_account']  						=  HARD_CODE_ID_YES;   
							
							$dataArray['created']  	 			 				=  DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
							$dataArray['created_by']  	 			 			=  $accountID; 
							//$dataArray['created_by_reference_table'] 			=  'MY_ORGANIZATION_TEAM_TABLE';  
							
						     $insertedID =	$this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,$dataArray);
							 
							 	/*Assign User Multiple Roles*/
								if ($role) 
								{
												
									for ($i=0; $i<sizeof($role); $i++)  {
												
												$dataRoleAssign['organization_id']						=	$organizationID;
												$dataRoleAssign['team_official_id']						=	$insertedID;
												//$dataRoleAssign['team_id']								=	$employeeID;
												$dataRoleAssign['role_id']								=	ROLE_PATIENT;
												$dataRoleAssign['created']								=	DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
												//$dataRoleAssign['created_by']							=	$teamID;
												//$dataRoleAssign['created_by_reference_table']			=	'MY_ORGANIZATION_TEAM_TABLE';
												
												$insertedAssignRoleID  =  $this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE,$dataRoleAssign);
									}
								}

								// to get current domain url
								 $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

								 $FullDomainName = $_SERVER['HTTP_HOST'] . '/';
							     $domainName     = $_SERVER['HTTP_HOST'] . '/';
							     $domainName     = str_replace('https://','',$domainName);
							     $domainName     = str_replace('/','',$domainName);



							  
							    if($domainName==ALIGNERCO_DOMAIN)
								{
									$companyEmail    = ALIGNERCO_EMAIL;
									$companyPassword = ALIGNERCO_EMAIL_PASSWORD;
									$companyName	 = 'AlignerCo';
								}
								if($domainName==ALIGNERCO_CANADA_DOMAIN)
								{
									$companyEmail    = ALIGNERCO_CANADA_EMAIL;
									$companyPassword = ALIGNERCO_CANADA_EMAIL_PASSWORD;
									$companyName	 = 'AlignerCo Canada';
								}
								if($domainName==STRAIGHT_MY_TEETH_DOMAIN)
								{
									$companyEmail    = STRAIGHT_MY_TEETH_EMAIL;
									$companyPassword = STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
									$companyName	 = 'Straight My Teeth';
								}
								if($domainName==SMILEPATH_DOMAIN)
								{
									$companyEmail    = SMILEPATH_EMAIL;
									$companyPassword = SMILEPATH_EMAIL_PASSWORD;
									$companyName	 = 'SmilePath';
								}
							    

								/*===================email code start================*/
				
			                    $this->load->library('email');
			                    $config = Array(
			                    'protocol' => 'smtp',
				                'smtp_host' => 'ssl://smtp.gmail.com',
				                'smtp_port' => 465,
				                'smtp_user' => $companyEmail,
				                'smtp_pass' => $companyPassword,
				                'smtp_timeout'=>20,
				                'mailtype'  => 'html',
				                'charset' => 'iso-8859-1',
				                'wordwrap' => TRUE
								);


			                    $this->load->library('email');
			                    $this->email->initialize($config);
			                    $this->email->set_mailtype("html");
			                    $this->email->set_newline("\r\n");

			                    $email_data['email']			        = $email;
			                    $email_data['username']			        = $username;
			                    $email_data['password']	                = $password;
			                    $email_data['FullDomainName']	        = $FullDomainName;
			                    $email_data['companyName']	            = $companyName;

			                    $message   = $this->load->view('cpanel/emails/patient_account_email', $email_data, true);
			                    $to_email  = PRODUCTION_EMAIL;
			                    $subject   = 'Welcome To '.$companyName;
			                 
			                 	/*$this->email->from($companyEmail,$companyName);*/
			                    $this->email->to($email);
			                    $this->email->subject($subject);
			                    $this->email->message($message);

			                    if($this->email->send())
			                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
			                    else
			                        $this->session->set_flashdata("email_sent","You have encountered an error");

							

							/*===========================email code end====================*/


								
								$this->session->set_userdata('admin_msg','Check your email for login information.');
								redirect('create-family-account/');
				}
	}






	function patientCaseDetails() {

		
		$accountID				       = $this->accountID;
		$teamID					       = $this->teamID;
		$organizationID 		       = $this->organizationID;

		$patient     		           = $this->model_users->getPatientDetails($organizationID,$accountID);  
		$patient			   	       = $patient->row_array();
		$caseID                        = $patient['CaseID'];

		$case  			               = $this->model_case->getCaseInfo($caseID);
		$caseStatus                    = $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DOCTOR_CASE_STATUS_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');


		$caseFlag 		               = false;

		if ($case->num_rows() == 0) {

			   $caseFlag  = false;

		} else {

			   $caseRow                 = $case->row_array();
			   $caseID                  = $caseRow['caseID'];
			   $distributor             = $caseRow['distributor'];
			   $patient_state           = $caseRow['patient_state'];

			   $caseFlag                = true;
		}

		$userInfo 				        = userInfo($accountID); // Calling From Application Helper

	    $recordPerPage					= 1000;
		$organizationID					= $this->organizationID;
		$searchParameters 			    = 0;
		$page                           = 0;

		/*$caseLinks        = $this->model_shared->getRecordMultipleWhereOrderBy('upload_link,password',CASE_SETUP_UPLOAD_DATA_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_id_fk' => $caseID),'id','DESC');*/

		/*$caseLinks                     = $this->model_case->getCaseSetupLinks($caseID);
		$case_data                       = $this->model_case->checkDoctorCase($caseID)->row();*/
		
		$result['case']                  = $case;
		$result['caseID']                = $caseID;
		$result['caseStatus']            = $caseStatus;
		/*$result['case_data']             = $case_data;
		$result['caseLinks']             = $caseLinks;*/
		$assignedRoles 			         = $this->assignedRoles;
		$accessModules 			         = $this->accessModules;
		$result['assignedRoles']		 = $assignedRoles;
		$result['accessModules']		 = $accessModules;
		$result['teamID']			     = $teamID;



			

		/*$caseStausComments 				   = $this->model_case->getCaseStatusComments($caseID);*/

		$caseSetups 				   = $this->model_shared->getRecordMultipleWhereOrderBy('*',CASE_SETUP_UPLOAD_DATA_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_id_fk' => $caseID),'id','DESC');

		/*$caseApproval 					   = $this->model_case->getCaseApproval($caseID)->row_array();
		$approvalCount                     = $this->model_case->getCaseApproval($caseID)->num_rows();*/

		$patientCaseStatusHistoryQuery      = $this->model_shared->getRecordMultipleWhereOrderBy('*',PATIENT_CASE_STATUS_HISTORY_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_id' => $caseID),'id','desc');
		$patientCaseStatusHistory           = $patientCaseStatusHistoryQuery->row_array();

		$result['case']            			= $case;
		$result['case_ID']                  = $case_ID;
		$result['caseSetups'] 	            = $caseSetups;
		/*$result['caseApproval'] 	  		= $caseApproval;*/
		$result['patientCaseStatusHistory'] = $patientCaseStatusHistory;
		$result['patientCaseStatusHistoryQuery'] = $patientCaseStatusHistoryQuery;
		/*$result['approvalCount'] 	  		= $approvalCount;*/
		/*$result['modificationCount'] 	    = $modificationCount;*/
		$result['deliveryServices']         = $deliveryServices;
		

		$data['userInfo']            		= $userInfo;

		$data['activeMenu']  				= '11';
		$data['activeSubMenu']  			= '11.1';

		$data['pageHeading']    			= "<i class='fa fa-medkit'></i> Case";
		$data['subHeading']    				= "# ".$caseID;

		$data['metaType']     				= 'internal';
		$data['pageTitle']      			= 'Case Details | '.DEFAULT_APPLICATION_NAME;

		$data['contents']	                = $this->load->view('cpanel/patient/patient/patient_case_details',$result,true);	
		$this->load->view('cpanel/template',$data);


	}




	function changeCaseStatus() {
		/*$connection=$this->Connection_model->connection();
		echo "here";
		echo'connection'.$connection;
		$query2="INSERT INTO services (name, description)
               VALUES ('fahad', 'test')" ;
               $result2=mysqli_query($connection, $query2) or die("Can not fetch data sfrom database".mysqli_error($connection));*/


		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 											 = $this->assignedRoles;
		$accessModules 											 = $this->accessModules;

		// Prepair Date To Store In Database
		$caseStatusID	         = $this->input->post('caseStatusID');
		$comment		         = $this->input->post('comment');
		$case                    = $this->input->post('list_case');
		$setup_upload_data_id    = $this->input->post('setup_upload_data_id');

		$getSetup                = $this->model_shared->getRecordMultipleWhereOrderBy('*',CASE_SETUP_UPLOAD_DATA_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'setup_upload_data_id' => $setup_upload_data_id),'id','desc')->row_array();

		$link                    = $getSetup['upload_link'];
		$code                    = $getSetup['password'];
		

		//check if already modification has been sent aagainst this case id, link and code and check if current ststus is modification then it will not update its status
		for($i=0; $i<sizeof($case); $i++) 
		{
			$case_id            = decodeString($case[$i]);
			$caseID             = $case_id;
		}

		/*$where="case_id ='$case_id' AND status_id='4' AND link='$link' ";						             
        $modificationCount  = $this->model_shared->page_listing_num_where('id',MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE,$where);

		if($modificationCount > 0 AND $caseStatusID==MODIFIED)
		{
			$this->session->set_flashdata('error', 'Sory Modification Could Not Be Sent As Only One Modification Could Be Sent Per Perview And You Have Already Sent One Modification For This Preview');
			redirect('patient-case-details');
		}*/
		//check if already modification has been sent aagainst this case id, link and code and check if current ststus is modification then it will not update its status

		/*else
		{*/

			/*$employees	   					 				    = $this->model_employee->getSingalActiveEmployees($organizationID,$teamID);*/

				$historyComment=$comment;

			if (!empty($case)) {

				
					/*if($caseStatusID==APPROVED)
					{*/

						 $check_case_data=$this->model_case->checkDoctorCaseForPatient($caseID);
						 $case_num_rows=$check_case_data->num_rows();

						 if($case_num_rows==0)
						 {
						 	 $data['case_id']				                =	$caseID;
							 $data['patient_approve_status']				=	$caseStatusID;
							 $data['description']							=	NULL;
							 $data['is_deletable']							=	HARD_CODE_ID_YES;
							 $data['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
							 /*$data['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
							 $data['created_by']							=   $teamID;
							 $data['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';*/
							 $this->model_shared->insertRecord(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data);
						 }
						 else
						 {
						 	 $update_data['case_id']				                =	$caseID;
							 $update_data['patient_approve_status']				    =	$caseStatusID;
							 $update_data['description']							=	NULL;
							 $update_data['is_deletable']							=	HARD_CODE_ID_YES;
						     $this->model_shared->editRecordWhere(array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$update_data);
						 }


						/*$caseID                                             = $case_id;
					    $data['patient_approve_status']					    = $caseStatusID;
						$data['comment']				                    = $comment;
						$this->model_shared->editRecordWhere(array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data);*/
					/*}*/

	                // adding history
	                $data_history['setup_upload_data_id']					=   $setup_upload_data_id;
					$data_history['case_id']								=	$case_id;
					$data_history['status_id']							    =	$caseStatusID;
					$data_history['link']				                    =	$link;
					$data_history['code']				                    =	$code;
					$data_history['comment']				                =	$historyComment;
					$data_history['description']							=	NULL;

					$data_history['is_deletable']							=	HARD_CODE_ID_YES;
					$data_history['receive_date']							=  	date("Y-m-d");

					$data_history['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
					$data_history['created_by']								=   $accountID;
					$data_history['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

					 $addhistoryId                                          =   $this->model_shared->insertRecord_ReturnID(PATIENT_CASE_STATUS_HISTORY_TABLE,$data_history);


					 	//email variables and logics
					    $doctorEmail=$employees->employeeEmail;
						$company=$employees->companyID;

						if($company==ALIGNERCO)
						{
							$companyEmail=ALIGNERCO_EMAIL;
							$companyPassword=ALIGNERCO_EMAIL_PASSWORD;
							$companyRecieverEmail=ALIGNERCO_RECIEVER_EMAIL;
							$email_data['companyName']	= 'AlignerCo';
							$email_data['FullDomainName']	    = 'portal.alignerco.com';
						}
						if($company==ALIGNERCO_CANADA)
						{
							$companyEmail=ALIGNERCO_CANADA_EMAIL;
							$companyPassword=ALIGNERCO_CANADA_EMAIL_PASSWORD;
							$companyRecieverEmail=ALIGNERCO_CANADA_RECIEVER_EMAIL;
							$email_data['companyName']	= 'AlignerCo Canada';
							$email_data['FullDomainName']	    = 'portal.alignerco.ca';
						}
						if($company==STRAIGHT_MY_TEETH)
						{
							$companyEmail=STRAIGHT_MY_TEETH_EMAIL;
							$companyPassword=STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
							$companyRecieverEmail=STRAIGHT_MY_TEETH_RECIEVER_EMAIL;
							$email_data['companyName']	= 'Straight My Teeth';
							$email_data['FullDomainName']	    = 'portal.straightmyteeth.com';
						}
						if($company==SMILEPATH)
						{
							$companyEmail=SMILEPATH_EMAIL;
							$companyPassword=SMILEPATH_EMAIL_PASSWORD;
							$companyRecieverEmail=SMILEPATH_RECIEVER_EMAIL;
							$email_data['companyName']	= 'SmilePath';
							$email_data['FullDomainName']	    = 'portal.smilepath.com.au';
						}


						if($caseStatusID==APPROVED)
						{
							$caseStatus='Approved The Case';
						}

						if($caseStatusID==MODIFIED)
						{
							$caseStatus='Asked For Modification In Case';
						}

						if($caseStatusID==PENDING)
						{
							$caseStatus='Make Case Status Pending';
						}

						$email_data['case_id']								=	$caseID;
						$email_data['link']				                    =	$link;
						$email_data['code']				                    =	$code;
						$email_data['caseStatus']				            =	$caseStatus;
						$email_data['doctorName']				            =	$employees->employeeName;


						if($caseStatusID==4)
						{
							/*===================email code start for portal================*/
		                    $this->load->library('email');
		                    $config = Array(
		                    'protocol' => 'smtp',
			                'smtp_host' => 'ssl://smtp.gmail.com',
			                'smtp_port' => 465,
			                'smtp_user' => $companyEmail,
			                'smtp_pass' => $companyPassword,
			                'smtp_timeout'=>20,
			                'mailtype'  => 'html',
			                'charset' => 'iso-8859-1',
			                'wordwrap' => TRUE
							);

		                    $this->load->library('email');
		                    $this->email->initialize($config);
		                    $this->email->set_mailtype("html");
		                    $this->email->set_newline("\r\n");

		                    $message =$this->load->view('cpanel/emails/case_modification_email', $email_data, true);


		                    $this->email->to($companyRecieverEmail);
		                    $this->email->subject('Modification on Case');
		                    $this->email->message($message);

		                    if($this->email->send())
		                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		                    else
		                        $this->session->set_flashdata("email_sent","You have encountered an error");

		                    /*echo $this->email->print_debugger(); die();*/

		                    /*===========================email code end====================*/



		                    /*===================email code start for production================*/
		                   /*$this->load->library('email');
		                    $config = Array(
		                    'protocol' => 'smtp',
			                'smtp_host' => 'ssl://smtp.gmail.com',
			                'smtp_port' => 465,
			                'smtp_user' => $companyEmail,
			                'smtp_pass' => $companyPassword,
			                'smtp_timeout'=>20,
			                'mailtype'  => 'html',
			                'charset' => 'iso-8859-1',
			                'wordwrap' => TRUE
							);

		                    $this->load->library('email');
		                    $this->email->initialize($config);
		                    $this->email->set_mailtype("html");
		                    $this->email->set_newline("\r\n");

		                    $message =$this->load->view('cpanel/emails/case_modification_email', $email_data, true);


		                    $this->email->to(PRODUCTION_EMAIL);
		                    $this->email->subject('Modification on Case');
		                    $this->email->message($message);

		                    if($this->email->send())
		                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		                    else
		                        $this->session->set_flashdata("email_sent","You have encountered an error");*/

		                    /*echo $this->email->print_debugger(); die();*/

		                    /*===========================email code end====================*/
						}
						else
						{
							/*===================email code start for portal for approval================*/
		                   /* $this->load->library('email');
		                    $config = Array(
		                    'protocol' => 'smtp',
			                'smtp_host' => 'ssl://smtp.gmail.com',
			                'smtp_port' => 465,
			                'smtp_user' => $companyEmail,
			                'smtp_pass' => $companyPassword,
			                'smtp_timeout'=>20,
			                'mailtype'  => 'html',
			                'charset' => 'iso-8859-1',
			                'wordwrap' => TRUE
							);

		                    $this->load->library('email');
		                    $this->email->initialize($config);
		                    $this->email->set_mailtype("html");
		                    $this->email->set_newline("\r\n");

		                    $message =$this->load->view('cpanel/emails/case_approval_email', $email_data, true);

		                    $this->email->to($companyRecieverEmail);
		                    $this->email->subject('Approval from Dentist');
		                    $this->email->message($message);

		                    if($this->email->send())
		                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		                    else
		                        $this->session->set_flashdata("email_sent","You have encountered an error");*/

		                    /*echo $this->email->print_debugger(); die();*/

		                    /*===========================email code end====================*/
						}


			 /*}*/

			 $this->session->set_userdata('admin_msg','Case Approval/Modification Sent Successfully');
		    redirect('patient-case-details');

      }

	}


	
	
	
	
	public  function roles() { 
			
			 /*Use For Access Permissions*/
			 if (!in_array(MODULE_ROLES_AND_PRIVILEGES,$this->accessModules)) {
				 		
						redirect('my-dashboard/');	
			 }
			 
			  $accountID				= $this->accountID; 
			  $teamID					= $this->teamID; 
			  $organizationID			= $this->organizationID;
			  
			/* User Roles And Permission
	  
				 - Check Allow Permision or Not
				 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			*/
			  
			$assignedRoles 						= $this->assignedRoles;
			$accessModules 						= $this->accessModules;
		
			$userInfo 							 = userInfo($accountID); // Calling From Application Helper
			
			$result['assignedRoles']			=  $assignedRoles;
			$result['accessModules']			=  $accessModules;
			
			$result['modules']      			=  getModules(); // Calling From User Helper
			$result['roles']      			    =  getRoles($organizationID); // Calling From User Helper
			
			$result['pageHeading']    			= "Roles & Privileges";
			
			$data['userInfo']            		= $userInfo;
			
			$data['activeMenu']  				= '4';
			$data['activeSubMenu']  			= '4.1';
			
			$data['metaType']     				= 'internal';
			$data['pageName']    				= 'Roles & Privileges';
			$data['pageTitle']      			= 'Roles & Privileges | '.DEFAULT_APPLICATION_NAME;
			
			$data['contents']	  			= $this->load->view('cpanel/users/role/roles',$result,true);
			$this->load->view('cpanel/template',$data);
	}
	
	
	
	
	
	private function getUserAccountInfoByID($organizationID,$tableID) {
			
			$result = $this->model_users->getUserAccountInfoByID($organizationID,$tableID);		
			
			if ($result) {
			
					return $result;	
			
			} else {
			
					redirect('not-found/');		
			}				
	}
	
	private function getEmployeeInfoByID($organizationID,$employeeID) {
			
			$result = $this->model_employee->getEmployeeInfoByID($organizationID,$employeeID);		
			
			if ($result) {
			
					return $result;	
			
			} else {
			
					redirect('not-found/');		
			}				
	}


	public  function duplicate_CaseID($CaseID) {
		$tableID = ($this->input->post('tableID') ?: NULL);
		$tableID=decodeString($tableID);
	
			$organizationID	  = $this->organizationID;
			$is_deleted=HARD_CODE_ID_NO;
			$USER_TYPE_PATIENT=HARD_CODE_ID_USER_TYPE_PATIENT;
			
			
			if ($CaseID) {

				$where="organization_id ='$organizationID' AND code='$CaseID' AND id!='$tableID' AND is_deleted='$is_deleted' AND type='$USER_TYPE_PATIENT' ";
				$tableName=MY_ORGANIZATION_TEAM_OFFICIALS_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_CaseID','Error: This CaseID \''.$CaseID.'\' already exists. Please try anothr CaseID.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}


public  function duplicate_email($email) {
		$tableID = ($this->input->post('tableID') ?: NULL);
		$tableID=decodeString($tableID);
	
			$organizationID	  = $this->organizationID;
			$is_deleted=HARD_CODE_ID_NO;
			
			
			if ($email) {

				$where="organization_id ='$organizationID' AND email='$email' AND id!='$tableID' AND created_by!='$tableID' AND is_deleted='$is_deleted' ";
				$tableName=MY_ORGANIZATION_TEAM_OFFICIALS_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_email','Error: This email \''.$email.'\' already exists. Please try anothr email.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}

	public  function duplicate_username($username) {
		$tableID = ($this->input->post('tableID') ?: NULL);
		$tableID=decodeString($tableID);
	
			$organizationID	  = $this->organizationID;
			$is_deleted=HARD_CODE_ID_NO;
			
			
			if ($username) {

				$where="organization_id ='$organizationID' AND username='$username' AND id!='$tableID' AND is_deleted='$is_deleted' ";
				$tableName=MY_ORGANIZATION_TEAM_OFFICIALS_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_username','Error: This UserName \''.$username.'\' already exists. Please try anothr UserName.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}



	/*Call Back Functions*/

	public  function check_already_exist_email($email) {
	
			if ($email) {
				
				  $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('email' => $email, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_email','Error: This Email already exist. Please try another Email.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}


	public  function check_already_exist_password($password) {
		$show_password                         = encodeString($password);
		$email					 			   = $this->input->post('email');
	
			if ($password) {
				
				  $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('show_password' => $show_password, 'is_deleted' => HARD_CODE_ID_NO,'email' => $email));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_password','Error: This password already exist. Please try another password.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}



	public  function check_already_exist_username($username) {
	
			if ($username) {
				
				  $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('username' => $username, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_username','Error: This UserName already exist. Please try another UserName.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	
	
	/*Call Back Functions*/
	public  function check_already_exist_user_account($employeeID) {
	
			if ($employeeID) {
				
				  $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('team_id' => $employeeID, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_user_account','Error: This user account already exist. Please try another employee.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	/*Inside Functions Calling*/
	private function getEmployeeRoles($employeeID) {
			
			$result = $this->model_hr_employees->getEmployeeRoles($employeeID);		
			
			if ($result) {
			
					return $result;	
			
			} else {
			
					return false;		
			}				
	}
	
	private function getRoleInfoBySefURL($organizationID,$sefURL) {
			
			$result = $this->model_users->getRoleInfoBySefURL($organizationID,$sefURL);		
			
			if ($result) {
			
					return $result;	
			
			} else {
			
					redirect('not-found/');		
			}				
	}
	
	private function check_Already_Exist_SefURL_Role($sefURL=NULL) {
			
			$result = $this->model_shared->getRecordMultipleWhere('id',JOB_ROLES_TABLE,array('sef_url' => $sefURL,'is_deleted' => HARD_CODE_ID_NO));		

			if ($result->num_rows() > 0) {
			
					return true;	
			
			} else {
			
					return false;
			}				
	}
	
}

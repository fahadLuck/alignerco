<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');

class apiController extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		$this->load->model('cpanel/cases/model_shared');



	}

	

	function missingCases() 
	{
		
		/*$record  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_CASES_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED));*/
		$record  = $this->model_shared->getRecord_SecondDB_MultipleWhere('*',INVENTORY_ITEMS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'status' => HARD_CODE_ID_MISSING));

		/*$count  = $record->num_rows();*/

		/*if ($count > 0) 
		{*/
			foreach($record->result() as $row) 
			{
				$caseID                                           = $row->case_reference_number;
				$inventory_id                                     = $row->inventory_id;
				$getRecord                                        = $this->model_shared->getRecord_SecondDB_MultipleWhere('delivery_service,airway_bill_number,airway_bill_number_formatted',INVENTORY_RECORD_TABLE,array('id' => $inventory_id));
				$getRecord_row	                                  = $getRecord->row_array();

				$missing_cases_data['missing_case_table_id']      = $row->id;
				$missing_cases_data['case_id']                    = $row->case_reference_number;
				$missing_cases_data['inventory_id']               = $row->inventory_id;
				$missing_cases_data['tracking_number_sending']    = $getRecord_row['airway_bill_number'];
				$missing_cases_data['tracking_number_formated']   = $getRecord_row['airway_bill_number_formatted'];
				$missing_cases_data['delivery_service_sending']   = $getRecord_row['delivery_service'];
				/*$missing_cases_data['tracking_number_receiving']  = $row->tracking_number_receiving;
				$missing_cases_data['delivery_service_receiving'] = $row->delivery_service_receiving;*/
				$missing_cases_data['status']                     = $row->status;
				$missing_cases_data['description']                = $row->description;
				$missing_cases_data['is_deletable']               = $row->is_deletable;
				$missing_cases_data['created']                    = DATABASE_NOW_DATE_TIME_FORMAT();
				$missing_cases_data['created_at']                 = $row->created;
				$missing_cases_data['created_by']                 = $row->created_by;
				$missing_cases_data['created_by_reference_table'] = $row->created_by_reference_table;
				$missing_cases_data['is_deleted']                 = $row->is_deleted;
				$missing_cases_data['deleted_by']                 = $row->deleted_by;
				$missing_cases_data['deleted_by_reference_table'] = $row->deleted_by_reference_table;

				$checkPresentMissingCases 	                      = $this->model_shared->getRecordMultipleWhere('id',MISSING_CASES_TABLE,array('case_id' => $caseID,'inventory_id' => $inventory_id));

				if ($checkPresentMissingCases->num_rows() > 0) 
				{
				    $missingCasesCasesID 	= $this->model_shared->editRecordWhere(array('case_id' => $caseID,'inventory_id' => $inventory_id),MISSING_CASES_TABLE,$missing_cases_data);
				}

				else
				{
				    $missingCasesCasesID						  = 	$this->model_shared->insertRecord_ReturnID(MISSING_CASES_TABLE,$missing_cases_data);
				}
			}
			

			if($missingCasesCasesID)
			{
				echo "missing cases api successfully run";
			}		

    }


    function missingCasesStatusUpdate() 
	{
		
		/*$record  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_CASES_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED));*/
		$record  = $this->model_shared->getRecordMultipleWhere('*',MISSING_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO));

			foreach($record->result() as $row) 
			{
				//get case id and inventory id of missing cases store in our database then check their status update in clearpath db to update their status 
				$caseID                                           = $row->case_id;
				$inventory_id                                     = $row->inventory_id;

				$getRecord                                        = $this->model_shared->getRecord_SecondDB_MultipleWhere('status',INVENTORY_ITEMS_TABLE,array('case_reference_number' => $caseID,'inventory_id' => $inventory_id));
				$getRecord_row	                                  = $getRecord->row_array();

				$missing_cases_data['status']                     = $getRecord_row['status'];

				$checkPresentMissingCases 	                      = $this->model_shared->getRecordMultipleWhere('id',MISSING_CASES_TABLE,array('case_id' => $caseID,'inventory_id' => $inventory_id));

				if ($checkPresentMissingCases->num_rows() > 0) 
				{
				    $missingCasesCasesID 	= $this->model_shared->editRecordWhere(array('case_id' => $caseID,'inventory_id' => $inventory_id),MISSING_CASES_TABLE,$missing_cases_data);
				}

				/*else
				{
				    $missingCasesCasesID						  = 	$this->model_shared->insertRecord_ReturnID(MISSING_CASES_TABLE,$missing_cases_data);
				}*/
			}
			

			/*if($missingCasesCasesID OR $missingCasesCasesID!='')
			{*/
				echo "missing cases status update api successfully run";
			/*}*/		

    }




    function existCases() 
	{
		
		/*$record  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_CASES_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED));*/
		$record  = $this->model_shared->getRecord_SecondDB_MultipleWhere('*',INVENTORY_ITEMS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'status' => HARD_CODE_ID_EXIST));

		/*$count  = $record->num_rows();*/

		/*if ($count > 0) 
		{*/
			foreach($record->result() as $row) 
			{
				$caseID                                           = $row->case_reference_number;
				$inventory_id                                     = $row->inventory_id;
				$getRecord                                        = $this->model_shared->getRecord_SecondDB_MultipleWhere('delivery_service,airway_bill_number,airway_bill_number_formatted',INVENTORY_RECORD_TABLE,array('id' => $inventory_id));
				$getRecord_row	                                  = $getRecord->row_array();

				$exist_cases_data['missing_case_table_id']      = $row->id;
				$exist_cases_data['case_id']                    = $row->case_reference_number;
				$exist_cases_data['inventory_id']               = $row->inventory_id;
				$exist_cases_data['tracking_number_sending']    = $getRecord_row['airway_bill_number'];
				$exist_cases_data['tracking_number_formated']   = $getRecord_row['airway_bill_number_formatted'];
				$exist_cases_data['delivery_service_sending']   = $getRecord_row['delivery_service'];
				/*$exist_cases_data['tracking_number_receiving']  = $row->tracking_number_receiving;
				$exist_cases_data['delivery_service_receiving'] = $row->delivery_service_receiving;*/
				$exist_cases_data['status']                     = $row->status;
				$exist_cases_data['description']                = $row->description;
				$exist_cases_data['is_deletable']               = $row->is_deletable;
				$exist_cases_data['created']                    = DATABASE_NOW_DATE_TIME_FORMAT();
				$exist_cases_data['created_at']                 = $row->created;
				$exist_cases_data['created_by']                 = $row->created_by;
				$exist_cases_data['created_by_reference_table'] = $row->created_by_reference_table;
				$exist_cases_data['is_deleted']                 = $row->is_deleted;
				$exist_cases_data['deleted_by']                 = $row->deleted_by;
				$exist_cases_data['deleted_by_reference_table'] = $row->deleted_by_reference_table;

				$checkPresentExistCases 	                      = $this->model_shared->getRecordMultipleWhere('id',EXIST_CASES_TABLE,array('case_id' => $caseID,'inventory_id' => $inventory_id));

				if ($checkPresentExistCases->num_rows() > 0) 
				{
				    $existCasesCasesID 	= $this->model_shared->editRecordWhere(array('case_id' => $caseID,'inventory_id' => $inventory_id),EXIST_CASES_TABLE,$exist_cases_data);
				}

				else
				{
				    $existCasesCasesID						  = 	$this->model_shared->insertRecord_ReturnID(EXIST_CASES_TABLE,$exist_cases_data);
				}
			}
			

			if($missingCasesCasesID)
			{
				echo "exist cases api successfully run";
			}		

    }


    function existCasesStatusUpdate() 
	{
		
		/*$record  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_CASES_TABLE,array('push_status' => HARD_CODE_ID_NOT_PUSHED));*/
		$record  = $this->model_shared->getRecordMultipleWhere('*',EXIST_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO));

			foreach($record->result() as $row) 
			{
				//get case id and inventory id of missing cases store in our database then check their status update in clearpath db to update their status 
				$caseID                                           = $row->case_id;
				$inventory_id                                     = $row->inventory_id;

				$getRecord                                        = $this->model_shared->getRecord_SecondDB_MultipleWhere('status',INVENTORY_ITEMS_TABLE,array('case_reference_number' => $caseID,'inventory_id' => $inventory_id));
				$getRecord_row	                                  = $getRecord->row_array();

				$exist_cases_data['status']                       = $getRecord_row['status'];

				$checkPresentExistCases 	                      = $this->model_shared->getRecordMultipleWhere('id',EXIST_CASES_TABLE,array('case_id' => $caseID,'inventory_id' => $inventory_id));

				if ($checkPresentExistCases->num_rows() > 0) 
				{
				    $missingCasesCasesID 	= $this->model_shared->editRecordWhere(array('case_id' => $caseID,'inventory_id' => $inventory_id),EXIST_CASES_TABLE,$exist_cases_data);
				}

				/*else
				{
				    $missingCasesCasesID						  = 	$this->model_shared->insertRecord_ReturnID(MISSING_CASES_TABLE,$missing_cases_data);
				}*/
			}
			

			/*if($missingCasesCasesID OR $missingCasesCasesID!='')
			{*/
				echo "exist cases status update api successfully run";
			/*}*/		

    }



    /*function missingCasesHistory() 
	{
		$page                                        = 0;
		$recordperpage						         = 1;
		$result	   				                     = $this->model_shared->page_listing($page,$recordperpage,'missing_cases_history_table_id',MISSING_CASES_HISTORY_TABLE,'id','DESC');
		$result_row	                                 = $result->row_array();

		$missing_cases_history_table_id_count		 = $result->num_rows();
		if($missing_cases_history_table_id_count > 0)	
		{
			$latest_missing_cases_history_table_id   = $result_row['missing_cases_history_table_id'];
		}
		else
		{
			$latest_missing_cases_history_table_id           = 0;
		}
		

		$record  = $this->model_shared->getRecord_SecondDB_MultipleWhere('*',MISSING_CASES_HISTORY_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id >' => $latest_missing_cases_history_table_id));
		$count   = $record->num_rows();

		if ($count > 0) 
		{
			foreach($record->result() as $row) 
			{

				$cases_history_data['missing_cases_history_table_id']  = $row->id;
				$cases_history_data['case_id']                    = $row->case_id;
				$cases_history_data['tracking_number_sending']    = $row->tracking_number_sending;
				$cases_history_data['delivery_service_sending']   = $row->delivery_service_sending;
				$cases_history_data['tracking_number_receiving']  = $row->tracking_number_receiving;
				$cases_history_data['delivery_service_receiving'] = $row->delivery_service_receiving;
				$cases_history_data['status']                     = $row->status;
				$cases_history_data['description']                = $row->description;
				$cases_history_data['is_deletable']               = $row->is_deletable;
				$cases_history_data['created']                    = DATABASE_NOW_DATE_TIME_FORMAT();
				$cases_history_data['created_at']                 = $row->created;
				$cases_history_data['created_by']                 = $row->created_by;
				$cases_history_data['created_by_reference_table'] = $row->created_by_reference_table;
				$cases_history_data['is_deleted']                 = $row->is_deleted;
				$cases_history_data['deleted_by']                 = $row->deleted_by;
				$cases_history_data['deleted_by_reference_table'] = $row->deleted_by_reference_table;

				$scases_historyTableID					          = 	$this->model_shared->insertRecord_ReturnID(MISSING_CASES_HISTORY_TABLE,$cases_history_data);
				
			}
			
		}


		echo "missing cases history api successfully run";

	}*/


	function casesPictures()
	{
		
		$notPushed          = HARD_CODE_ID_NOT_PUSHED;
		$notDeleted         = HARD_CODE_ID_NO;
		$where              ="push_status ='$notPushed' AND public_url!='' AND is_deleted='$notDeleted' ";
		$picture_query      = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASE_PICTURES_TABLE,$where);

		/*$picture_by_cases	= $this->model_shared->getDistinctRecordMultipleWhere('case_id',MEDICAL_CASE_PICTURES_TABLE,$where);*/

		$picture_by_cases	= $this->model_case->getCasesPatientPicturesData();

		foreach($picture_query->result() as $record_row)
		{
			$case_reference_number                                   = $record_row->case_id;
			$url                                                     = $record_row->public_url;

			$patient_pictures_data['case_reference_number']          = $record_row->case_id;
			$patient_pictures_data['name']                           = $record_row->name;
			$patient_pictures_data['type']                           = $record_row->type;
			$patient_pictures_data['url']                            = $record_row->public_url;
			$patient_pictures_data['created']                        = DATABASE_NOW_DATE_TIME_FORMAT();
			$patient_pictures_data['created_by']                     = $record_row->created_by;
			$patient_pictures_data['created_by_reference_table']     = $record_row->created_by_reference_table;

			$exitPictureCount 	= $this->model_shared->getRecord_SecondDB_MultipleWhere('id',ALIGNERCO_CASES_PICTURES_TABLE,array('case_reference_number' => $case_reference_number,'url' => $url))->num_rows();

			if($exitPictureCount==0)
			{
				$patientPicturesID									     = 	$this->model_shared->insertRecord_SecondDB_ReturnID(ALIGNERCO_CASES_PICTURES_TABLE,$patient_pictures_data);
			}	

			$sentPictureCount 	= $this->model_shared->getRecord_SecondDB_MultipleWhere('id',ALIGNERCO_CASES_PICTURES_TABLE,array('case_reference_number' => $case_reference_number,'url' => $url))->num_rows();

			if($sentPictureCount > 0)
			{
				$update_data['push_status']                          = HARD_CODE_ID_PUSHED;
				$editWhere="push_status ='$notPushed' AND public_url!='' AND is_deleted='$notDeleted' AND case_id='$case_reference_number' AND public_url='$url' ";
				$this->model_shared->editRecordWhere($editWhere,MEDICAL_CASE_PICTURES_TABLE,$update_data);
			}
			 
		}


			$companyEmail                    = ALIGNERCO_EMAIL;
			$companyPassword                 = ALIGNERCO_EMAIL_PASSWORD;
			$email_data['companyName']	     = 'AlignerCo';
			$email_data['FullDomainName']	 = 'portal.alignerco.com';
			$email_data['picture_by_cases']	 =  $picture_by_cases;
			$to_email                        = array('fahadluck688@gmail.com');

			/**/


			/*===================email code start================*/
            $this->load->library('email');
            $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => $companyEmail,
            'smtp_pass' => $companyPassword,
            'smtp_timeout'=>20,
            'mailtype'  => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
			);


            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");

            $message                 = $this->load->view('cpanel/emails/patient_pictures_notification_to_production', $email_data, true);
         
            $this->email->to($to_email);
            $this->email->subject('New Pictures Notification');
            $this->email->message($message);

            if($picture_query->num_rows() > 0)
            {
            	if($this->email->send())
                $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
	            else
	                $this->session->set_flashdata("email_sent","You have encountered an error");
				/*===========================email code end====================*/

				/*echo $this->email->print_debugger();
	                    exit;*/
            }

            

		echo "cases pictures api successfully run";
	}



	function patientCasesPictures()
	{
		$notPushed      = HARD_CODE_ID_NOT_PUSHED;
		$notDeleted     = HARD_CODE_ID_NO;
		$where="push_status ='$notPushed' AND case_id!='' AND is_deleted='$notDeleted' ";
		$patient_picture_query  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASE_PATIENT_PICTURES_TABLE,$where);

		foreach($patient_picture_query->result() as $record_row)
		{
			$patient_pictures_data['case_reference_number']                        = $record_row->case_id;
			$patient_pictures_data['name']                           = $record_row->name;
			$patient_pictures_data['type']                           = $record_row->type;
			$patient_pictures_data['url']                            = $record_row->public_url;
			$patient_pictures_data['created']                        = DATABASE_NOW_DATE_TIME_FORMAT();
			$patient_pictures_data['created_by']                     = $record_row->created_by;
			$patient_pictures_data['created_by_reference_table']     = $record_row->created_by_reference_table;
			// Save All data Into tbl_application_medical_cases_temporary_inventory_record
			$patientPicturesID									     = 	$this->model_shared->insertRecord_SecondDB_ReturnID(ALIGNERCO_CASES_PICTURES_TABLE,$patient_pictures_data);

			if($patientPicturesID OR $patientPicturesID!='')
			{
				$update_data['push_status']                          = HARD_CODE_ID_PUSHED;
				$editWhere="push_status ='$notPushed' AND case_id!='' AND is_deleted='$notDeleted' ";
				$this->model_shared->editRecordWhere($editWhere,MEDICAL_CASE_PATIENT_PICTURES_TABLE,$update_data);

				
			}

		}
		
		echo "patient cases pictures api successfully run";
	}



	function patientAccountCreatReminderEmail_STOP()
	{
		$casesQuery  = $this->model_shared->getRecordMultipleWhere('id,patient,patient_email,distributor',MEDICAL_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO));

		foreach($casesQuery->result() as $cases_row)
		{
			$case_id                        = $cases_row->id;
			$patient                        = $cases_row->patient;
			$patient_email                  = $cases_row->patient_email;
			$company                        = $cases_row->distributor;

			$notDeleted                     = HARD_CODE_ID_NO;
			$typePatient                    = HARD_CODE_ID_USER_TYPE_PATIENT;
			$where                          = "is_deleted ='$notDeleted' AND type ='$typePatient' AND(email ='$patient_email' OR code ='$case_id') ";
			$accountNum                     = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,$where)->num_rows();

			if($accountNum==0)
			{

				if($company==ALIGNERCO)
				{
					$companyEmail                    = ALIGNERCO_EMAIL;
					$companyPassword                 = ALIGNERCO_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'AlignerCo';
					$email_data['FullDomainName']	 = 'portal.alignerco.com';
					/*$to_email                        = array('ele@alignerco.com', 'support@alignerco.com');*/
				}
				if($company==ALIGNERCO_CANADA)
				{
					$companyEmail                    = ALIGNERCO_CANADA_EMAIL;
					$companyPassword                 = ALIGNERCO_CANADA_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'AlignerCo Canada';
					$email_data['FullDomainName']	 = 'portal.alignerco.ca';
					/*$to_email                        = array('alice@alignerco.ca', 'team@alignerco.ca');*/
				}
				if($company==STRAIGHT_MY_TEETH)
				{
					$companyEmail                    = STRAIGHT_MY_TEETH_EMAIL;
					$companyPassword                 = STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'Straight My Teeth';
					$email_data['FullDomainName']	 = 'portal.straightmyteeth.com';
					/*$to_email                        = array('sarah@straightmyteeth.com', 'support@straightmyteeth.com');*/
				}
				if($company==SMILEPATH)
				{
					$companyEmail                   = SMILEPATH_EMAIL;
					$companyPassword                = SMILEPATH_EMAIL_PASSWORD;
					$email_data['companyName']	    = 'SmilePath';
					$email_data['FullDomainName']	= 'portal.smilepath.com.au';
					/*$to_email                       = array('jamie@smilepath.com.au', 'team@smilepath.com.au');*/
				}

				$to_email                           = $patient_email;

				/*===================email code start================*/
                $this->load->library('email');
                $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => $companyEmail,
                'smtp_pass' => $companyPassword,
                'smtp_timeout'=>20,
                'mailtype'  => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
				);


                $this->load->library('email');
                $this->email->initialize($config);
                $this->email->set_mailtype("html");
                $this->email->set_newline("\r\n");

                $email_data['patient']	     = $patient;

                $email_type                  = 'patientAccountCreatReminderEmail';
                $emailCount                  = $this->model_shared->getRecordMultipleWhere('id',EMAIL_RECORD_TABLE,array('case_id' => $case_id,'email_type' => $email_type))->num_rows();

                if($emailCount==0)
                {
                	$message                 = $this->load->view('cpanel/emails/account_reminder_email_1', $email_data, true);
                }

                elseif($emailCount==1)
                {
                	$message                 = $this->load->view('cpanel/emails/account_reminder_email_2', $email_data, true);
                }

                else
                {
                	$message                 = $this->load->view('cpanel/emails/account_reminder_email_3', $email_data, true);
                }
             
                $this->email->to($to_email);
                $this->email->subject('Account Reminder');
                $this->email->message($message);

                if($this->email->send())
                    $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
                else
                    $this->session->set_flashdata("email_sent","You have encountered an error");
				/*===========================email code end====================*/

				$email_record_data['case_id']                = $case_id;
				$email_record_data['person_id']              = NULL;
				$email_record_data['email_type']             = 'patientAccountCreatReminderEmail';
				$email_record_data['received_date']          = date("Y-m-d");
				$email_record_data['created_at']             = DATABASE_NOW_DATE_TIME_FORMAT();

				$this->model_shared->insertRecord(EMAIL_RECORD_TABLE,$email_record_data);

			}

		}
		
		echo "account reminder api successfully run";
	}



	function patientPicturesReminderEmail_STOP()
	{
		$accountQuery  = $this->model_shared->getRecordMultipleWhere('id,email,code,username,domain',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'type' => HARD_CODE_ID_USER_TYPE_PATIENT));

		foreach($accountQuery->result() as $account_row)
		{
			$accountID                      = $account_row->id;
			$email                          = $account_row->email;
			$code                           = $account_row->code;
			$patient                        = $account_row->username;
			$domainName                     = $account_row->domain;

			$notDeleted                     = HARD_CODE_ID_NO;
			$where                          = "is_deleted ='$notDeleted' AND account_id ='$accountID' ";
			$pictureNum                     = $this->model_shared->getRecordMultipleWhere('id',MEDICAL_CASE_PATIENT_PICTURES_TABLE,$where)->num_rows();

			if($pictureNum==0)
			{

				if($domainName==ALIGNERCO_DOMAIN)
				{
					$companyEmail                    = ALIGNERCO_EMAIL;
					$companyPassword                 = ALIGNERCO_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'AlignerCo';
					$email_data['FullDomainName']	 = 'portal.alignerco.com';
				}
				if($domainName==ALIGNERCO_CANADA_DOMAIN)
				{
					$companyEmail                    = ALIGNERCO_CANADA_EMAIL;
					$companyPassword                 = ALIGNERCO_CANADA_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'AlignerCo Canada';
					$email_data['FullDomainName']	 = 'portal.alignerco.ca';
				}
				if($domainName==STRAIGHT_MY_TEETH_DOMAIN)
				{
					$companyEmail                    = STRAIGHT_MY_TEETH_EMAIL;
					$companyPassword                 = STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'Straight My Teeth';
					$email_data['FullDomainName']	 = 'portal.straightmyteeth.com';
				}
				if($domainName==SMILEPATH_DOMAIN)
				{
					$companyEmail                   = SMILEPATH_EMAIL;
					$companyPassword                = SMILEPATH_EMAIL_PASSWORD;
					$email_data['companyName']	    = 'SmilePath';
					$email_data['FullDomainName']	= 'portal.smilepath.com.au';
				}

				

				$to_email                           = $email;

				/*===================email code start================*/
                $this->load->library('email');
                $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => $companyEmail,
                'smtp_pass' => $companyPassword,
                'smtp_timeout'=>20,
                'mailtype'  => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
				);


                $this->load->library('email');
                $this->email->initialize($config);
                $this->email->set_mailtype("html");
                $this->email->set_newline("\r\n");

                $email_data['patient']	     = $patient;

                $email_type                  = 'patientPicturesReminderEmail';
                $emailCount                  = $this->model_shared->getRecordMultipleWhere('id',EMAIL_RECORD_TABLE,array('person_id' => $accountID,'email_type' => $email_type))->num_rows();

                if($emailCount==0)
                {
                	$message                 = $this->load->view('cpanel/emails/picture_reminder_email_1', $email_data, true);
                }

                elseif($emailCount==1)
                {
                	$message                 = $this->load->view('cpanel/emails/picture_reminder_email_2', $email_data, true);
                }

                else
                {
                	$message                 = $this->load->view('cpanel/emails/picture_reminder_email_3', $email_data, true);
                }
             
                $this->email->to($to_email);
                $this->email->subject('Account Reminder');
                $this->email->message($message);

                if($this->email->send())
                    $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
                else
                    $this->session->set_flashdata("email_sent","You have encountered an error");
				/*===========================email code end====================*/

				$email_record_data['case_id']                = NULL;
				$email_record_data['person_id']              = $accountID;
				$email_record_data['email_type']             = 'patientPicturesReminderEmail';
				$email_record_data['received_date']          = date("Y-m-d");
				$email_record_data['created_at']             = DATABASE_NOW_DATE_TIME_FORMAT();

				$this->model_shared->insertRecord(EMAIL_RECORD_TABLE,$email_record_data);

			}
			
		}
		
		echo "picture reminder api successfully run";
	}



	function doctorCaseReminderEmail_STOP()
	{
		$doctorCasesQuery  = $this->model_shared->getRecordMultipleWhere('case_id,doctor_id,status_id',MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'status_id' => PENDING));
        
        $count=1;
		foreach($doctorCasesQuery->result() as $cases_row)
		{
			
			$case_id                        = $cases_row->case_id;
			$doctor_id                      = $cases_row->doctor_id;
			$status_id                      = $cases_row->status_id;

			$getDoctorHRID                  = $this->model_shared->getRecordMultipleWhere('HR_id,email',MY_ORGANIZATION_TEAM_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $doctor_id))->row_array();
			$HR_ID                          = $getDoctorHRID['HR_id'];
			$doctorEmail                    = $getDoctorHRID['email']; 

			$getDoctorCompany               = $this->model_shared->getRecordMultipleWhere('company,name',APPLICATION_HR_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $HR_ID))->row_array();
			$company                        = $getDoctorCompany['company'];
			$doctorName                     = $getDoctorCompany['name'];

			$doctorCases                    = $this->model_shared->getRecordMultipleWhere('case_id,doctor_id,status_id',MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'status_id' => PENDING,'doctor_id' => $doctor_id));
			/*foreach($doctorCases->result() as $doctorCasesRow)
		    {
			 	$doctorCaseID                 = $doctorCasesRow->case_id;
		    }*/


			if($company==ALIGNERCO)
			{
				$companyEmail                    = ALIGNERCO_EMAIL;
				$companyPassword                 = ALIGNERCO_EMAIL_PASSWORD;
				$email_data['companyName']	     = 'AlignerCo';
				$email_data['FullDomainName']	 = 'portal.alignerco.com';
				/*$to_email                        = array('ele@alignerco.com', 'support@alignerco.com');*/
			}
			if($company==ALIGNERCO_CANADA)
			{
				$companyEmail                    = ALIGNERCO_CANADA_EMAIL;
				$companyPassword                 = ALIGNERCO_CANADA_EMAIL_PASSWORD;
				$email_data['companyName']	     = 'AlignerCo Canada';
				$email_data['FullDomainName']	 = 'portal.alignerco.ca';
				/*$to_email                        = array('alice@alignerco.ca', 'team@alignerco.ca');*/
			}
			if($company==STRAIGHT_MY_TEETH)
			{
				$companyEmail                    = STRAIGHT_MY_TEETH_EMAIL;
				$companyPassword                 = STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
				$email_data['companyName']	     = 'Straight My Teeth';
				$email_data['FullDomainName']	 = 'portal.straightmyteeth.com';
				/*$to_email                        = array('sarah@straightmyteeth.com', 'support@straightmyteeth.com');*/
			}
			if($company==SMILEPATH)
			{
				$companyEmail                   = SMILEPATH_EMAIL;
				$companyPassword                = SMILEPATH_EMAIL_PASSWORD;
				$email_data['companyName']	    = 'SmilePath';
				$email_data['FullDomainName']	= 'portal.smilepath.com.au';
				/*$to_email                       = array('jamie@smilepath.com.au', 'team@smilepath.com.au');*/
			}

			$to_email                           = $doctorEmail;
             
			/*if($count==1)
			{*/
				/*===================email code start================*/
	            $this->load->library('email');
	            $config = Array(
	            'protocol' => 'smtp',
	            'smtp_host' => 'ssl://smtp.gmail.com',
	            'smtp_port' => 465,
	            'smtp_user' => $companyEmail,
	            'smtp_pass' => $companyPassword,
	            'smtp_timeout'=>20,
	            'mailtype'  => 'html',
	            'charset' => 'iso-8859-1',
	            'wordwrap' => TRUE
				);


	            $this->load->library('email');
	            $this->email->initialize($config);
	            $this->email->set_mailtype("html");
	            $this->email->set_newline("\r\n");

	            $email_data['doctorCases']	  = $doctorCases;
	            $email_data['doctorName']	  = $doctorName;
	            $email_data['doctorCaseID']	  = $case_id;

	            $email_type                  = 'doctorCaseReminderEmail';
                $emailCount                  = $this->model_shared->getRecordMultipleWhere('id',EMAIL_RECORD_TABLE,array('person_id' => $doctor_id,'case_id' => $case_id,'email_type' => $email_type))->num_rows();

                if($emailCount==0)
                {
                	$message                 = $this->load->view('cpanel/emails/doctor_cases_reminder_email_1', $email_data, true);
                }

                elseif($emailCount==1)
                {
                	$message                 = $this->load->view('cpanel/emails/doctor_cases_reminder_email_2', $email_data, true);
                }

                else
                {
                	$message                 = $this->load->view('cpanel/emails/doctor_cases_reminder_email_3', $email_data, true);
                }

	         
	            $this->email->to($to_email);
	            $this->email->subject('Case Reminder');
	            $this->email->message($message);

	            if($this->email->send())
	                $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
	            else
	                $this->session->set_flashdata("email_sent","You have encountered an error");
				/*===========================email code end====================*/

				$email_record_data['case_id']                = $case_id;
				$email_record_data['person_id']              = $doctor_id;
				$email_record_data['email_type']             = 'doctorCaseReminderEmail';
				$email_record_data['received_date']          = date("Y-m-d");
				$email_record_data['created_at']             = DATABASE_NOW_DATE_TIME_FORMAT();

				$this->model_shared->insertRecord(EMAIL_RECORD_TABLE,$email_record_data);

			/*}*/


			$count++;


		}
			
		
		
		echo "doctor cases reminder api successfully run";
	}






	function patientCaseActionReminderEmail_STOP()
	{
		$accountQuery  = $this->model_shared->getRecordMultipleWhere('id,email,code,username,domain',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'type' => HARD_CODE_ID_USER_TYPE_PATIENT));

		foreach($accountQuery->result() as $account_row)
		{
			$accountID                      = $account_row->id;
			$email                          = $account_row->email;
			$code                           = $account_row->code;
			$patient                        = $account_row->username;
			$domainName                     = $account_row->domain;

			/*$notDeleted                     = HARD_CODE_ID_NO;
			$where                          = "is_deleted ='$notDeleted' AND case_id_fk ='$code' ";
			$setupNum                       = $this->model_shared->getRecordMultipleWhere('id',CASE_SETUP_UPLOAD_DATA_TABLE,$where)->num_rows();*/

			$getSetup		                = $this->model_shared->getRecordMultipleWhereOrderBy('upload_link,password',CASE_SETUP_UPLOAD_DATA_TABLE,array('case_id_fk' => $code,'is_deleted' => HARD_CODE_ID_NO),'id','DESC');
			$setupNum                       = $getSetup->num_rows();
			$setup                          = $getSetup->row_array();
			$setupLink                      = $setup['upload_link'];
			$setupCode                      = $setup['password'];

			//check that in specific link and code patient has performed action
			$caseActionNum		            = $this->model_shared->getRecordMultipleWhereOrderBy('id',MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,array('case_id' => $code,'is_deleted' => HARD_CODE_ID_NO,'link' => $setupLink,'code' => $setupCode),'id','DESC')->num_rows();

			/*$patientActionNum               = $this->model_shared->getRecordMultipleWhere('id,email,code,username,domain',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'type' => HARD_CODE_ID_USER_TYPE_PATIENT));*/

			if($setupNum > 0 AND $caseActionNum == 0)
			{

				if($domainName==ALIGNERCO_DOMAIN)
				{
					$companyEmail                    = ALIGNERCO_EMAIL;
					$companyPassword                 = ALIGNERCO_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'AlignerCo';
					$email_data['FullDomainName']	 = 'portal.alignerco.com';
				}
				if($domainName==ALIGNERCO_CANADA_DOMAIN)
				{
					$companyEmail                    = ALIGNERCO_CANADA_EMAIL;
					$companyPassword                 = ALIGNERCO_CANADA_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'AlignerCo Canada';
					$email_data['FullDomainName']	 = 'portal.alignerco.ca';
				}
				if($domainName==STRAIGHT_MY_TEETH_DOMAIN)
				{
					$companyEmail                    = STRAIGHT_MY_TEETH_EMAIL;
					$companyPassword                 = STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
					$email_data['companyName']	     = 'Straight My Teeth';
					$email_data['FullDomainName']	 = 'portal.straightmyteeth.com';
				}
				if($domainName==SMILEPATH_DOMAIN)
				{
					$companyEmail                   = SMILEPATH_EMAIL;
					$companyPassword                = SMILEPATH_EMAIL_PASSWORD;
					$email_data['companyName']	    = 'SmilePath';
					$email_data['FullDomainName']	= 'portal.smilepath.com.au';
				}

				

				$to_email                           = $email;

				/*===================email code start================*/
                $this->load->library('email');
                $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => $companyEmail,
                'smtp_pass' => $companyPassword,
                'smtp_timeout'=>20,
                'mailtype'  => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
				);


                $this->load->library('email');
                $this->email->initialize($config);
                $this->email->set_mailtype("html");
                $this->email->set_newline("\r\n");

                $email_data['patient']	     = $patient;

                $message                     = $this->load->view('cpanel/emails/picture_reminder_email', $email_data, true);
             
                $this->email->to($to_email);
                $this->email->subject('Account Reminder');
                $this->email->message($message);

                if($this->email->send())
                    $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
                else
                    $this->session->set_flashdata("email_sent","You have encountered an error");
				/*===========================email code end====================*/

			}
			
		}
		
		echo "patient case action reminder api successfully run";
	}




	/*function setupTimelineApi()
	{

		$getCaseID  = $this->model_shared->getRecordMultipleWhere('id,case_id,setup_timeline',CASE_SETUP_UPLOAD_DATA_TABLE,array('is_deleted' => HARD_CODE_ID_NO));

		foreach($getCaseID->result() as $record_row)
		{
			$case_id                                     = $record_row->case_id;
			$setup_timeline                              = $record_row->setup_timeline;
			
			$notDeleted     = HARD_CODE_ID_NO;
			$where="id ='$case_id' AND target_date_setup_timeline!='$setup_timeline' AND target_date_setup_timeline!='' AND is_deleted='$notDeleted' ";
			$timeline       = $this->model_shared->getRecord_SecondDB_MultipleWhere('target_date_setup_timeline',CLEARPATH_MEDICAL_CASE_TABLE,$where)->row_array();

			echo $update_data['setup_timeline']          = $timeline['target_date_setup_timeline']; 
			 
		}

		echo "cases pictures api successfully run";
	}*/

	/*function setupTimelineApi()
	{
		$notDeleted     = HARD_CODE_ID_NO;
		$where="distributor IN (30,49,52,59) AND is_deleted='$notDeleted' AND target_date_setup_timeline!='' ";

		$getCaseID  = $this->model_shared->getRecord_SecondDB_MultipleWhere('id,target_date_setup_timeline',CLEARPATH_MEDICAL_CASE_TABLE,$where);

		foreach($getCaseID->result() as $record_row)
		{
			$case_id                            = $record_row->id;
			$target_date_setup_timeline         = $record_row->target_date_setup_timeline;

			$update_data['setup_timeline']      = $target_date_setup_timeline;

			$updatedCasesCasesID 	            = $this->model_shared->editRecordWhere(array('case_id' => $case_id),CASE_SETUP_UPLOAD_DATA_TABLE,$update_data);
			 
		}

		echo "setupTimelineApi successfully run";
	}*/


	function setupTimelineAndAlignerTimeApi()
	{
		$notDeleted     = HARD_CODE_ID_NO;
		$where="distributor IN (30,49,52,59) AND target_date_setup_timeline!='' AND is_deleted='$notDeleted' ";

		/*$getCaseID  = $this->model_shared->getRecord_SecondDB_MultipleWhere('id,target_date_setup_timeline,target_date_aligners_timeline',CLEARPATH_MEDICAL_CASE_TABLE,$where);*/

		$getCaseID  = $this->model_shared->get_SecondDB_CaseID();

		foreach($getCaseID->result() as $record_row)
		{
			$case_reference_number              = $record_row->case_reference_number;
			$case_id                            = $record_row->id;
			$target_date_setup_timeline         = $record_row->target_date_setup_timeline;
			$target_date_aligners_timeline      = $record_row->target_date_aligners_timeline;

			/*$getRecord                          = $this->model_shared->getRecord_SecondDB_MultipleWhere('case_reference_number',INVENTORY_CASES_TABLE,array('case_id' => $case_id));
			$getRecord_row	                    = $getRecord->row_array();
			$case_id_fk			                = $getRecord_row['case_reference_number'];*/

			$update_data['case_id']             = $case_id;
			$update_data['setup_timeline']      = $target_date_setup_timeline;
			$update_data['aligners_timeline']   = $target_date_aligners_timeline;

			$updatedCasesID 	                = $this->model_shared->editRecordWhere(array('id' => $case_reference_number),MEDICAL_CASES_TABLE,$update_data);
			 
		}

		echo "setupTimelineAndAlignerTimeApi successfully run";
	}



	function changeShipedCasesPushStatus($inventory_id)
	{
		//portal.alignerco.com/dev/cpanel/apiController/changeShipedCasesPushStatus/inventory_id

		// update status in inventory record table
		$update_data['push_status']                              = HARD_CODE_ID_NOT_PUSHED;
        $this->model_shared->editRecordWhere(array('id' => $inventory_id),INVENTORY_RECORD_TABLE,$update_data);

        // update status in inventory items table
        $this->model_shared->editRecordWhere(array('inventory_id' => $inventory_id),INVENTORY_ITEMS_TABLE,$update_data);

       // update status in inventory cases table
        $items_data_query  = $this->model_shared->getRecordMultipleWhere('*',INVENTORY_ITEMS_TABLE,array('inventory_id' => $inventory_id));

		foreach($items_data_query->result() as $items_row)
		{
			$inventory_case_reference_number                    = $items_row->case_reference_number;
		    // Update INVENTORY_CASES_TABLE 
		    $this->model_shared->editRecordWhere(array('case_reference_number' => $inventory_case_reference_number),INVENTORY_CASES_TABLE,$update_data);
		}
	}


	function getAlignercoCasesPictures($case_id)
	{
		$notPushed          = HARD_CODE_ID_NOT_PUSHED;
		$notDeleted         = HARD_CODE_ID_NO;
		$where              ="public_url !='' AND is_deleted='$notDeleted' AND case_id='$case_id' ";
		$picture_query      = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASE_PICTURES_TABLE,$where);

		$picturesNum        = $picture_query->num_rows();

		$result    =   array();
		if($picturesNum > 0)
		{
			foreach($picture_query->result() as $record_row)
			{
				/*$data['case_reference_number']          = $record_row->case_id;*/
				$data['name']                           = $record_row->name;
				$data['type']                           = $record_row->type;
				$data['url']                            = $record_row->public_url;

			    $result[] = $data;
		    }

		    $result = json_encode($result);
				print_r($result);
				return $result;
		}

		else
		{
			$result = json_encode(['Status'=>404, 'Message'=>'No picture Found']);
			print_r($result);
			return $result;
		}
					
	}


	function sendModificationApi()
	{
		$data = print_r(json_decode(file_get_contents('php://input'), true));
		return $data;
	}







	





}
?>

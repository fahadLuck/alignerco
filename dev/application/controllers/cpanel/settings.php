<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {


	public  function __construct() {
		
		parent::__construct();
		
		 authentication(); // Calling From Login Helper
		
		/* Table ID*/
		$this->accountID = $this->session->userdata(USER_ACCOUNT_ID_SESSION);
		
		/*ID*/
		$this->teamID   = $this->session->userdata(USER_TEAM_ID_SESSION);
		
		/*ID*/
		$this->HRID   = $this->session->userdata(USER_HR_ID_SESSION);
		
		/*ID*/
		$this->organizationID	= $this->session->userdata(USER_ORGANIZATION_ID_SESSION);
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');
		
		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper
	}
	
	public function index($page=0) { 

				
				/*Use For Access Permissions*/
				if (!in_array(MANAGE_STATES,$this->accessModules)) {
				
					redirect('my-dashboard/');	
				}
				
				/*echo "here 1"; die();*/

				/* User Roles And Permission
  
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
				
				$assignedRoles 					= $this->assignedRoles;
				$accessModules 					= $this->accessModules;
						
				$recordPerPage					= 1000;

				$accountID						= $this->accountID; 
				$organizationID					= $this->organizationID;
				
				$userInfo 						= userInfo($accountID); // Calling From Application Helper
				
				$result['assignedRoles']		=  $assignedRoles;
				$result['accessModules']		=  $accessModules;
				
				$result['bloodGroups'] 			= getBloodGroups(); // Calling From Application Helper

				/*$result['countries']     						=   getAllCountries();
				$result['states']          						=   getAllStates(); */
						
				// Code For Search Employee
				$searchParameters 									= array();
				
				$searchBusinessUnit		    						= $this->input->post('business_unit');
				$searchDepartment		    						= $this->input->post('department');
				$searchJobTitle		    							= $this->input->post('job_title');
				$searchPosition		    							= $this->input->post('job_position');
				$searchShiftTimings		    						= $this->input->post('shift_timings');
				
				$searchCustom		    							= $this->input->post('custom_search');
				$searchCustomColumn		    						= $this->input->post('custom_search_column');
				
				$searchParameters['searchBusinessUnit']				= $searchBusinessUnit;
				$searchParameters['searchDepartment']				= $searchDepartment;
				$searchParameters['searchJobTitle']					= $searchJobTitle;
				$searchParameters['searchPosition']					= $searchPosition;
				$searchParameters['searchShiftTimings']				= $searchShiftTimings;
				
				$searchParameters['searchCustom']					= $searchCustom;
				$searchParameters['searchCustomColumn']				= $searchCustomColumn;
				
				/*$employees	   					 				    = $this->model_employee->getActiveDoctors($page,$recordPerPage,$organizationID,$searchParameters);

				$totalRows			 								=$employees->num_rows(); 
				
				$mypaing['total_rows']			 					= $totalRows;*/

				$activeStates  = $this->model_application->getStates();
				$totalRows			 								=$activeStates->num_rows();
				$mypaing['total_rows']			 					= $totalRows;
				$result['activeStates']		                        =  $activeStates;
		
				$mypaing['base_url'] 								= base_url()."states/";
				$mypaing['per_page']								= $recordPerPage;
				$mypaing['uri_segment']								= 2;	 
				
				$mypaing['full_tag_open'] 						   = '<ul  class="pagination pagination-sm pull-right">';
				$mypaing['full_tag_close'] 						   = '</ul>';

				$mypaing['first_link'] 							   = 'First';
				$mypaing['first_tag_open'] 						   = '<li>';
				$mypaing['first_tag_close'] 					   = '</li>';
				$mypaing['last_link']							   = 'Last';
				$mypaing['last_tag_open'] 						   = '<li>';
				$mypaing['last_tag_close'] 						   = '</li>';
				$mypaing['next_link'] 							   = 'Next';
				$mypaing['next_tag_open'] 						   = '<li>';
				$mypaing['next_tag_close'] 						   = '</li>';
		
				$mypaing['prev_link'] 							   = 'Previous';
				$mypaing['prev_tag_open'] 						   = '<li>';
				$mypaing['prev_tag_close'] 						   = '</li>';
					
				$mypaing['cur_tag_open'] 						   = '<li><a href="" class="current">';
				$mypaing['cur_tag_close'] 						   = '</a></li>';
					
				$mypaing['num_tag_open'] 						   = '<li>';
				$mypaing['num_tag_close'] 						   = '</li>';  	
							
				$this->pagination->initialize($mypaing);
				$paginglink											= $this->pagination->create_links();
				
				$serialNumber 										= $this->uri->segment(2)+1;

				/*$result['employees']	 							= $employees;*/
				
						
				$result['serialNumber']		= $serialNumber;
				$result['pagingLink']		= $paginglink;
				$result['totalResults']		= $totalRows;
				
				$data['pageHeading']    	= "States";	
				$data['subHeading']    		= "(all)";		
				
				$data['userInfo']           = $userInfo;
				
				$data['activeMenu']  		= '7';
				$data['activeSubMenu']  	= '7.1';
				
				$data['metaType']     		= 'internal';
				$data['pageName']    		= 'States';
				$data['pageTitle']      	= 'States | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']	  			 	= $this->load->view('cpanel/settings/state_list',$result,true);
				$this->load->view('cpanel/template',$data);
	}
	
	public function stateAdd() { 
	
			/*Use For Access Permissions*/
			if (!in_array(MANAGE_STATES,$this->accessModules)) {
				
				redirect('my-dashboard/');	
			}
			
			$accountID			= $this->accountID; 
			$teamID				= $this->teamID; 
			$organizationID		= $this->organizationID;
			
			/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			*/
			
			$assignedRoles 		= $this->assignedRoles;
			$accessModules 		= $this->accessModules;

		
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('state','state','trim|required');
		$this->form_validation->set_rules('country','country','trim|required');
					 
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
					
		if($this->form_validation->run() === FALSE ) { 

				$userInfo 										=	userInfo($accountID); // Calling From Application Helper
				
				$result['assignedRoles']						=   $assignedRoles;
				$result['accessModules']						=   $accessModules;
				
				$result['organizationID'] 						=   $organizationID;
				
				$result['prefix'] 								= 	getPrefix(); // Calling From Application Helper
				
				$result['countries']     						=   getAllCountries(); //  Calling From Shared Helper;
				
				$data['pageHeading']    						=  "State";
				$data['subHeading']    							=  "(new)";
				
				$data['userInfo']            					=   $userInfo;
				
				$data['activeMenu']  							=  '7';
				$data['activeSubMenu']  						=  '7.1'; 
				
				$data['metaType']     							=  'internal';
				$data['pageName']    							=  'New State';
				$data['pageTitle']      						=  'New State | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']	= $this->load->view('cpanel/settings/state_add',$result,true);
				$this->load->view('cpanel/template',$data);

		} 
		 else {
					
				/*$prefix 																= ($this->input->post('prefix') ?: NULL);*/
				$state 																	= ($this->input->post('state') ?: NULL);
				$country 															= ($this->input->post('country') ?: NULL);
				
				$createdDated															    = DATABASE_NOW_DATE_TIME_FORMAT();
				
				$stateAddArray['name']													    = 	$state;
				$stateAddArray['country_id']												= 	$country;
				$stateAddArray['publish_status']											= 	HARD_CODE_ID_PUBLISHED;
				$stateAddArray['created']													= 	$createdDated; 
				$stateAddArray['created_by']												= 	$teamID;
				$stateAddArray['created_by_reference_table']						  		= 	'MY_ORGANIZATION_TEAM_TABLE';
						
	              $this->model_shared->insertRecord_ReturnID(STATES_TABLE,$stateAddArray);	

				 if ($this->input->post('submit') == 'Save & New') {
								
						redirect('state-add');
				
				} else {
								
						redirect('states/');
				}
		}	}




	public function stateEdit($tableID) 
	{ 
			/*Use For Access Permissions*/
			if (!in_array(MANAGE_STATES,$this->accessModules)) {
			
				redirect('my-dashboard/');	
			}
			
			/* User Roles And Permission

				 - Check Allow Permision or Not
				 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			*/

		    $tableID = decodeString($tableID); 
			CheckEmptySefURL($tableID);  // Calling From Shared Helper

			$assignedRoles 					= $this->assignedRoles;
			$accessModules 					= $this->accessModules;
					
			$recordPerPage					= 1000;

			$accountID						= $this->accountID; 
			$organizationID					= $this->organizationID;
			
			$userInfo 						= userInfo($accountID); // Calling From Application Helper
			
			$result['assignedRoles']		=  $assignedRoles;
			$result['accessModules']		=  $accessModules;
			
			$result['bloodGroups'] 			= getBloodGroups(); // Calling From Application Helper
					
			
			$activeState  = $this->model_application->getSpecificState($tableID);
			/*foreach($assignedStates->result() as $as)
			{
				echo $team_id=$as->state_id;
			}*/

			/*$totalRows			 								= $this->model_employee->getActiveEmployeesNum($organizationID,$searchParameters);*/
			
			
			
			/*$serialNumber 										= $this->uri->segment(2)+1;*/

			$result['activeState']	 					    = $activeState;
			$result['countries']          					=   getAllCountries(); //  Calling From 
			$result['tableID']      				        = encodeString($tableID);

			/*$gender								= 	getGenderMaleFemale();
			foreach($gender->result() as $gen) 
			{
				echo"<br>".$gen->genderID;
			}
			$employees->genderName; die();*/

			
					
			/*$result['serialNumber']		= $serialNumber;
			$result['pagingLink']		= $paginglink;*/
			/*$result['totalResults']		= $totalRows;*/
			
			$data['pageHeading']    	= "State";	
			$data['subHeading']    		= "(Edit)";		
			
			$data['userInfo']           = $userInfo;
			
			$data['activeMenu']  		= '7';
			$data['activeSubMenu']  	= '7.1';
			
			$data['metaType']     		= 'internal';
			$data['pageName']    		= 'State';
			$data['pageTitle']      	= 'State | '.DEFAULT_APPLICATION_NAME;
			
			$data['contents']	  			 	= $this->load->view('cpanel/settings/state_edit',$result,true);


			$submit=$this->input->post('submit');

			if(!isset($submit))
			{
				$this->load->view('cpanel/template',$data);
			}




			else
			{
				/* Set Form Validation Errors */ 
				$this->form_validation->set_rules('state','state','trim|required');
		        $this->form_validation->set_rules('country','country','trim|required');
							 
				$this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
							
				if($this->form_validation->run() === FALSE ) { 

						$userInfo 										=	userInfo($accountID); // Calling From Application Helper
				
						$result['assignedRoles']						=   $assignedRoles;
						$result['accessModules']						=   $accessModules;
						
						$result['organizationID'] 						=   $organizationID;
						
						$result['prefix'] 								= 	getPrefix(); // Calling From Application Helper
						
						$result['countries']     						=   getAllCountries(); //  Calling From Shared Helper;
						
						$data['pageHeading']    						=  "State";
						$data['subHeading']    							=  "(edit)";
						
						$data['userInfo']            					=   $userInfo;
						
						$data['activeMenu']  							=  '7';
						$data['activeSubMenu']  						=  '7.1'; 
						
						$data['metaType']     							=  'internal';
						$data['pageName']    							=  'Edit State';
						$data['pageTitle']      						=  'Edit State | '.DEFAULT_APPLICATION_NAME;

						$result['submit']=$submit; 

						
						$data['contents']	= $this->load->view('cpanel/settings/state_edit',$result,true);
						$this->load->view('cpanel/template',$data);

				} 
				else {
					    $state 																	= ($this->input->post('state') ?: NULL);
						$country 															= ($this->input->post('country') ?: NULL);
						
						$stateUpdateArray['name']													    = 	$state;
						$stateUpdateArray['country_id']												= 	$country;

							$this->model_shared->editRecordWhere(array('id' => $tableID),STATES_TABLE,$stateUpdateArray);
						

						 redirect(base_url().'states');

					}
			}
			

	}







	
	public  function stateRemove($stateID) {
			
			   /*Use For Access Permissions*/
			   if (!in_array(MANAGE_STATES,$this->accessModules)) {
				
					redirect('my-dashboard/');	
			   }
			  
			  CheckEmptySefURL($stateID);  // Calling From Shared Helper
			  
			  $accountID			= $this->accountID;
			  $teamID				= $this->teamID;
			  $organizationID 		= $this->organizationID;
			  
			  $stateID			= decodeString($stateID); // Calling From General Helper
			 						   
			  /*$result     			= $this->getEmployeeInfoByID($organizationID,$employeeID);*/
							  
				/*if ($result) {
								  
					  $row 	   							= $result->row_array();

					  $tableID 							= $row['tableID'];
					  $employeeID 						= $row['employeeID'];
					  $employeeTeamID 					= $row['employeeTeamID'];
					  $employeeHRID 					= $row['employeeHRID'];
					  $employeeName 					= $row['employeeName'];
					  $employeePrefixName 				= $row['prefixName'];*/
					  
					  // Soft Delete From Database Update Record IsDeleted = TRUE 
					  $update['is_deleted'] 					= HARD_CODE_ID_YES;
					  $update['deleted_by'] 					= $teamID;
					  $update['deleted_by_reference_table'] 	= 'MY_ORGANIZATION_TEAM_TABLE';
					  $update['deleted']						= DATABASE_NOW_DATE_TIME_FORMAT(); // Calling From Shared Helper
		  
					  $this->model_shared->editRecordWhere(array('id' => $stateID,'is_deleted' => HARD_CODE_ID_NO),STATES_TABLE,$update);

				 /* }*/
	
				  redirect('states/');	 
	}
	
	/*Call Back Functions*/



	public  function duplicate_code($txtCode) {
		$tableID = ($this->input->post('tableID') ?: NULL);
	
			$organizationID	  = $this->organizationID;
			$is_deleted=HARD_CODE_ID_NO;
			
			
			if ($txtCode) {

				$where="organization_id ='$organizationID' AND code='$txtCode' AND id!='$tableID' AND is_deleted='$is_deleted' ";
				$tableName=MY_ORGANIZATION_TEAM_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_code','Error: This code \''.$txtCode.'\' already exists. Please try anothr code.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}


	public  function duplicate_email($txtEmail) {
		$tableID = ($this->input->post('tableID') ?: NULL);
	
			$organizationID	  = $this->organizationID;
			$is_deleted=HARD_CODE_ID_NO;
			
			
			if ($txtEmail) {

				$where="organization_id ='$organizationID' AND email='$txtEmail' AND id!='$tableID' AND is_deleted='$is_deleted' ";
				$tableName=MY_ORGANIZATION_TEAM_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_email','Error: This email \''.$txtEmail.'\' already exists. Please try anothr email.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}



	public  function duplicate_mobile($txtMobile) {
		$HRID = ($this->input->post('HRID') ?: NULL);
		$is_deleted=HARD_CODE_ID_NO;
	
			$organizationID	  = $this->organizationID;
			$mobile		= cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($txtMobile))); // Calling From General Helper
			
			if ($txtMobile) {

				$where="mobile =$mobile AND id!='$HRID' AND is_deleted='$is_deleted' ";
				$tableName=APPLICATION_HR_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_mobile','Error: This mobile \''.$txtMobile.'\' already exists. Please try anothr mobile.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}


	
	public  function check_already_exist_employee_official_email($txtEmail) {
	
			$organizationID	 = $this->organizationID;
			
			if ($txtEmail) {
				
				  $result  = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_TABLE,array('organization_id'=>$organizationID, 'email' => $txtEmail, 'is_deleted' => HARD_CODE_ID_NO));		
				
					 if ($result->num_rows() > 0) {
					  
							 $this->form_validation->set_message('check_already_exist_employee_official_email','Error: This email \''.$txtEmail.'\' already exists. Please try anothr email adddress.');
							 return false;
				  
					  } else {
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_employee_official_code($txtCode) {
	
			$organizationID	  = $this->organizationID;
			
			if ($txtCode) {
				
				  $result  				    = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_TABLE,array('organization_id'=>$organizationID, 'code' => $txtCode, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_employee_official_code','Error: This code \''.$txtCode.'\' already exists. Please try anothr code.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_employee_personal_email($txtEmail) {
	
			if ($txtEmail) {
				
				  $result  				    = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('personal_email' => $txtEmail, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_employee_personal_email','Error: This email \''.$txtEmail.'\' already exists. Please try another email adddress.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_employee_personal_CNIC($txtCNIC) {
	
			if ($txtCNIC) {
				  			
				  $CNIC						=	cleanSringFromDashes($txtCNIC); // Calling From General Helper
				  
				  $result  				    = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('identity_card' => $CNIC, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_employee_personal_CNIC','Error: This CNIC number \''.$txtCNIC.'\' already exists. Please try another number.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_employee_personal_mobile($txtMobile) {
	
			if ($txtMobile) {
				  			
				  $mobile		= cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($txtMobile))); // Calling From General Helper
				  
				  $result  		= $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('mobile' => $mobile, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   return true;
								   
								   //$this->form_validation->set_message('check_already_exist_employee_personal_mobile','Error: This mobile number \''.$txtMobile.'\' already exists. Please try another number.');
								   //return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public function check_picture_extension() {
		
		 if ($_FILES['picture_data']['name']) {
		
			  $fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['picture_data']['name'])); // Calling From General Helper
			  $fname 			= str_replace(" ","_",$fname);
			  $fname 			= str_replace("%","_",$fname);
			  
			  
			  $nameExt 			= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['picture_data']['name'])))); // Calling From General Helper

			  $fileName			= @convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['picture_data']['name']))); // Calling From General Helper
			  
			  if($nameExt !='jpg' && $nameExt !='gif' && $nameExt !='png')  {
				 
				$this->form_validation->set_message('check_picture_extension','Error: Wrong image type.');
				return false;		
			 
			 } else {
				
				return true;	 
			 }	
			
		 } else {
			 
				return true;	 
		}
	}
	
	
	public  function check_already_exist_edit_employee_official_email($txtEmail) {
		
		$organizationID	  		= $this->organizationID;
		
		if ($txtEmail) {
	
		 	$oldEmail 			= $this->input->post('exiting_official_email');
	
			if ($txtEmail != $oldEmail) {
			
				    $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_TABLE,array('organization_id'=>$organizationID, 'email' => $txtEmail, 'is_deleted' => HARD_CODE_ID_NO));		
				
						  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_edit_employee_official_email','Error: This email \''.$txtEmail.'\' already exists. Please try another email adddress.');
								   return false;
				  
						  } else 	{
						  
							  return true;	
				}
		
		} else {
			
					return true;	
			}
		} else {
				
				return true;	
		}
	}
	
	public  function check_already_exist_edit_employee_official_code($txtCode) {
		
		$organizationID	  = $this->organizationID;
		
		if ($txtCode) {
	
		 	$oldCode 			= $this->input->post('exiting_official_code');
	
			if ($txtCode != $oldCode) {
			
				    $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_TABLE,array('organization_id'=>$organizationID, 'code' => $txtCode, 'is_deleted' => HARD_CODE_ID_NO));		
				
						  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_edit_employee_official_code','Error: This code \''.$txtCode.'\' already exists. Please try another code.');
								   return false;
				  
						  } else 	{
						  
							  return true;	
				}
		
		} else {
			
					return true;	
			}
		} else {
				
				return true;	
		}
	}
		
	public  function check_already_exist_edit_employee_personal_email($txtEmail) {
	
			if ($txtEmail) {
				
				 $oldEmail 	= $this->input->post('exiting_personal_email');
				  
				  if ($txtEmail != $oldEmail) {
			
				    $result   = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('personal_email' => $txtEmail, 'is_deleted' => HARD_CODE_ID_NO));		
				
						  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_edit_employee_personal_email','Error: This email \''.$txtEmail.'\' already exists. Please try another email adddress.');
								   return false;
				  
						  } else 	{
						  
							  return true;	
						 }
		
				  } else {
						
						return true;	  
				 }
				
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_edit_employee_personal_CNIC($txtCNIC) {
	
			if ($txtCNIC) {
				  			
				  $oldCNIC 				= $this->input->post('exiting_CNIC');
				  $CNIC						=	cleanSringFromDashes($txtCNIC); // Calling From General Helper
				  
				   if ($CNIC != $oldCNIC) {
				  
							  $result    = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('identity_card' => $CNIC, 'is_deleted' => HARD_CODE_ID_NO));		
				
					 			 if ($result->num_rows() > 0) {
					  
									   $this->form_validation->set_message('check_already_exist_edit_employee_personal_CNIC','Error: This CNIC number \''.$txtCNIC.'\' already exists. Please try another number.');
								   	   return false;
				  
								 } else 	{
						  
									   return true;	
					  			}
				   } else {
						
							return true;   
				   }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_edit_employee_personal_mobile($txtMobile) {
	
			if ($txtMobile) {
				  			
				  $oldMobile 				= $this->input->post('exiting_mobile');
				  $mobile					=	cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($txtMobile))); // Calling From General Helper
				  
				  if ($mobile != $oldMobile) {
				  
				   $result  = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('mobile' => $mobile, 'is_deleted' => HARD_CODE_ID_NO));		
			
					  if ($result->num_rows() > 0) {
					  
								   return true;	
								   
								   //$this->form_validation->set_message('check_already_exist_edit_employee_personal_mobile','Error: This mobile number \''.$txtMobile.'\' already exists. Please try another number.');
								   //return false;
				  
					  } else {
						  
						  return true;	
					  }
				  
				  } else {
				  		
						return true;
				  }
			
			} else {
				
				return true;	
			}
	}
	
	
	private function getEmployeeInfoByID($organizationID,$tableID) {
			
			$result = $this->model_employee->getEmployeeInfoByID($organizationID,$tableID);		
			
			if ($result) {
			
				return $result;	
			
			} else {
			
				redirect('not-found/');		
			}				
	}
	
	
	/*AJAX  FUNCTIONS*/
	function AJAX_getDepartmentsByBusinessUnitID() { 
	
				$assignedRoles 				= $this->assignedRoles;
			 	$accessModules 				= $this->accessModules;
				
				$businessUnit         		= $this->input->post('businessUnit');
				$selectedDepartment         = $this->input->post('selectedDepartment');
				
				$HTML		= NULL;
						
				$result 	= $this->model_organization->getBusinessUnitDepartments($businessUnit);
				     
				$html = '<select id="department" name="department" class="custom-select" onchange="return showFilter(\'department\');">
							<option value="">Select Department</option>';
					            
							if ($result) {
							 
							 	foreach($result->result() as $row) {
										
											  if ($selectedDepartment == $row->departmentID) {
											  
													  $departmentSelected = 'selected = selected';	
											  
											  }	else {
													  
													  $departmentSelected = NULL;		
											  }
						  
											$html .= ' <option value="'.$row->departmentID.'" '.$departmentSelected.'>'.$row->departmentName.' '.fillBrackets($row->departmentCode).'</option>';	 
							 			
									} 
					  }
					  
				 $html .= '</select>';
				 echo $html;
	}
	
	function AJAX_getPositionsByJobTitleID() { 
				
				$assignedRoles 						= $this->assignedRoles;
			 	$accessModules 						= $this->accessModules;
				
				$jobTitleID         				=	$this->input->post('jobTitleID');
				$selectedJobPosition         		=	$this->input->post('selectedJobPosition');
				
				$HTML								= NULL;
							
				$result 							= $this->model_organization->getJobTitlePositions($jobTitleID);
				
				     
				$html = '<select name="job_position" id="job_position" class="custom-select" onchange="return showFilter(\'job-position\');">
							<option value="">Select Job Position</option>';
							
						if ($result) {
						 
							foreach($result->result() as $row) {
										
										 if ($selectedJobPosition == $row->jobPositionID) {
										  
												  $jobPositionSelected = 'selected = selected';	
										  
										  }	else {
												  
												  $jobPositionSelected = NULL;		
										  }
									
										$html .= ' <option value="'.$row->jobPositionID.'" '.$jobPositionSelected.'>'.$row->jobPositionName.' '.fillBrackets($row->jobPositionCode).'</option>';	 
									
								} 
				  }
				  
				 $html .= '</select>';
				 echo $html;
	}
	
}

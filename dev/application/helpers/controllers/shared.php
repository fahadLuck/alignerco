<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 
 
class Shared extends CI_Controller {

	public function __construct() {
		
		parent::__construct();
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		$this->load->model('public/model_public');
		
		$this->load->model('cpanel/setups/model_setup');
		
		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		
		$this->load->model('cpanel/workers/production_control/model_production_control');
		$this->load->model('cpanel/workers/doctors/model_doctor');
		$this->load->model('cpanel/workers/quality_assurance/model_quality_assurance');
		$this->load->model('cpanel/workers/operators/model_operator');
		
		$this->load->model('cpanel/reports/model_report');
		
		$this->caseStatusReadyArray     	= array(
													CASE_STATUS_EPOXY_POURING_READY,
											 		CASE_STATUS_BUTTERFLY_TECHNIQUE_READY,
											 		CASE_STATUS_QUALITY_OF_BUTTER_FLY_READY,
											 		CASE_STATUS_CUTTING_READY,
											 		CASE_STATUS_EPOXY_ARTICULATION_READY,
											 		CASE_STATUS_QUALITY_EPOXY_ARTICULATION_READY,
											 		CASE_STATUS_3D_SCANNING_EDITING_READY,
											 		CASE_STATUS_3D_SCANNING_SCALP_READY_QC_CHECKING,
											 		CASE_STATUS_3D_BUTTERFLY_READY,
											 		CASE_STATUS_3D_BUTTERFLY_QUALITY_READY,
											 		CASE_STATUS_3D_PRINTING_READY,
											 		CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_READY,
											 		CASE_STATUS_SETUP_QUALITY_CASE_READY_FOR_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_EDITING_CASE_EDITING_READY,
											 		CASE_STATUS_SETUP_EDITING_QUALITY_OF_EDITING_READY,
											 		CASE_STATUS_READY_FOR_UPLOADING_READY,
											 		CASE_STATUS_WAITING_FOR_APPROVAL_WAITING,
													CASE_STATUS_TREATMENT_SECTION_READY,
											 		CASE_STATUS_QUALITY_OF_STAGES_READY,
											 		CASE_STATUS_ALIGNER_FINISHING_READY,
											 		CASE_STATUS_QUALITY_ALIGNER_FINISHING_READY,
											 		CASE_STATUS_LASER_SHIPMENT_READY,
											 		CASE_STATUS_PACKAGING_READY,
											 		CASE_STATUS_QUALITY_OF_CASES_READY
													);
										
		$this->caseStatusProcessArray    	= array(
											 		CASE_STATUS_EPOXY_POURING_PROCESS,
											 		CASE_STATUS_BUTTERFLY_TECHNIQUE_PROCESS,
											 		CASE_STATUS_QUALITY_OF_BUTTER_FLY_PROCESS,
											 		CASE_STATUS_CUTTING_PROCESS,
											 		CASE_STATUS_EPOXY_ARTICULATION_PROCESS,
											 		CASE_STATUS_QUALITY_EPOXY_ARTICULATION_PROCESS,
											 		CASE_STATUS_3D_SCANNING_EDITING_PROCESS,
											 		CASE_STATUS_3D_SCANNING_SCALP_PROCESS_QC_CHECKING,
											 		CASE_STATUS_3D_BUTTERFLY_PROCESS,
											 		CASE_STATUS_3D_BUTTERFLY_QUALITY_PROCESS,
											 		CASE_STATUS_3D_PRINTING_PROCESS,
											 		CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_PROCESS,
											 		CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_MODIFICATION_PROCESS,
													CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_IN_HOUSE_MODIFICATION_PROCESS,
											 		CASE_STATUS_SETUP_QUALITY_CASE_PROCESS_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_EDITING_CASE_EDITING_PROCESS,
											 		CASE_STATUS_SETUP_EDITING_QUALITY_OF_EDITING_PROCESS,
											 		CASE_STATUS_READY_FOR_UPLOADING_PROCESS,
											 		CASE_STATUS_TREATMENT_SECTION_PROCESS,
											 		CASE_STATUS_QUALITY_OF_STAGES_PROCESS,
											 		CASE_STATUS_ALIGNER_FINISHING_PROCESS,
											 		CASE_STATUS_QUALITY_ALIGNER_FINISHING_PROCESS,
													 CASE_STATUS_LASER_SHIPMENT_PROCESS,
											 		CASE_STATUS_PACKAGING_PROCESS,
													CASE_STATUS_QUALITY_OF_CASES_PROCESS
													);
													
		$this->caseStatusHoldArray    		= array(
													CASE_STATUS_EPOXY_POURING_HOLD,
													CASE_STATUS_EPOXY_POURING_HOLD_REASON_A,
													CASE_STATUS_EPOXY_POURING_HOLD_REASON_B,
													CASE_STATUS_EPOXY_POURING_HOLD_REASON_C,
													CASE_STATUS_EPOXY_POURING_HOLD_REASON_D,
													CASE_STATUS_3D_SCANNING_EDITING_HOLD,
													CASE_STATUS_3D_SCANNING_EDITING_HOLD_REASON_A,
													CASE_STATUS_3D_SCANNING_EDITING_HOLD_REASON_B,
													CASE_STATUS_3D_SCANNING_EDITING_HOLD_REASON_C,
													CASE_STATUS_3D_SCANNING_EDITING_HOLD_REASON_D,
													CASE_STATUS_3D_SCANNING_EDITING_HOLD_REASON_E,
													CASE_STATUS_3D_SCANNING_EDITING_HOLD_REASON_F,
													CASE_STATUS_3D_SCANNING_EDITING_HOLD_REASON_G,
													CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_HOLD,
													CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_HOLD_REASON_A,
													CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_HOLD_REASON_B,
													CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_HOLD_REASON_C,
													CASE_STATUS_SETUP_QUALITY_CASE_QUALITY_CHECKING_HOLD,
													CASE_STATUS_SETUP_QUALITY_CASE_QUALITY_CHECKING_HOLD_REASON_A,
													CASE_STATUS_SETUP_QUALITY_CASE_QUALITY_CHECKING_HOLD_REASON_B,
													CASE_STATUS_SETUP_QUALITY_CASE_QUALITY_CHECKING_HOLD_REASON_C,
													CASE_STATUS_SETUP_QUALITY_CASE_QUALITY_CHECKING_HOLD_REASON_D,
													CASE_STATUS_SETUP_QUALITY_CASE_QUALITY_CHECKING_HOLD_REASON_E,
													CASE_STATUS_SETUP_QUALITY_CASE_QUALITY_CHECKING_HOLD_REASON_F,
													CASE_STATUS_SETUP_EDITING_CASE_EDITING_HOLD,
													CASE_STATUS_SETUP_EDITING_CASE_EDITING_HOLD_REASON_A,
													CASE_STATUS_READY_FOR_UPLOADING_HOLD,
													CASE_STATUS_READY_FOR_UPLOADING_HOLD_REASON_A,
													CASE_STATUS_TREATMENT_SECTION_HOLD,
													CASE_STATUS_TREATMENT_SECTION_HOLD_REASON_A
												    );
													
		$this->caseStatusOFFHoldArray    	= array(
													CASE_STATUS_EPOXY_POURING_OFF_HOLD,
													CASE_STATUS_3D_SCANNING_EDITING_OFF_HOLD,
													CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_OFF_HOLD,
													CASE_STATUS_SETUP_QUALITY_CASE_QUALITY_CHECKING_OFF_HOLD,
													CASE_STATUS_SETUP_EDITING_CASE_EDITING_OFF_HOLD,
													CASE_STATUS_READY_FOR_UPLOADING_OFF_HOLD,
													CASE_STATUS_TREATMENT_SECTION_OFF_HOLD
												    );
		
		$this->caseStatusModificationArray  = array(
												    CASE_STATUS_WAITING_FOR_APPROVAL_MODIFICATION
												   );
		
		$this->caseStatusDoneArray  	 	= array(
											 		CASE_STATUS_EPOXY_POURING_DONE,
											 		CASE_STATUS_EPOXY_POURING_IMPRESSIONS_NOT_DONE_BUT_PROCEEDING,
											 		CASE_STATUS_EPOXY_POURING_POURED_AFTER_APPROVAL,
											 		CASE_STATUS_BUTTERFLY_TECHNIQUE_DONE,
											 		CASE_STATUS_QUALITY_OF_BUTTER_FLY_DONE,
											 		CASE_STATUS_CUTTING_DONE,
											 		CASE_STATUS_EPOXY_ARTICULATION_DONE,
											 		CASE_STATUS_QUALITY_EPOXY_ARTICULATION_DONE,
											 		CASE_STATUS_3D_SCANNING_EDITING_DONE,
											 		CASE_STATUS_3D_SCANNING_SCALP_DONE_FOR_TREATMENT_SETUP,
											 		CASE_STATUS_3D_BUTTERFLY_DONE,
											 		CASE_STATUS_3D_BUTTERFLY_QUALITY_DONE,
											 		CASE_STATUS_3D_PRINTING_DONE,
											 		CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_DONE,
											 		CASE_STATUS_SETUP_SECTION_FOR_TREATMENT_SETUP_MODIFICATION_CANCEL,
											 		CASE_STATUS_SETUP_QUALITY_CASE_DONE_QUALITY_CHECKING,
													CASE_STATUS_SETUP_EDITING_CASE_EDITING_DONE,
											 		CASE_STATUS_SETUP_EDITING_QUALITY_OF_EDITING_DONE,
											 		CASE_STATUS_READY_FOR_UPLOADING_DONE,
											 		CASE_STATUS_WAITING_FOR_APPROVAL_APPROVED,
											 		CASE_STATUS_TREATMENT_SECTION_DONE,
													CASE_STATUS_QUALITY_OF_STAGES_DONE,
											 		CASE_STATUS_ALIGNER_FINISHING_DONE,
											 		CASE_STATUS_QUALITY_ALIGNER_FINISHING_DONE,
													CASE_STATUS_LASER_SHIPMENT_DONE,
											 		CASE_STATUS_PACKAGING_DONE,
											 		CASE_STATUS_QUALITY_OF_CASES_DONE,
												    );
		
		$this->caseStatusRejectArray  		= array(
											 		CASE_STATUS_3D_SCANNING_SCALP_REJECT,
													CASE_STATUS_3D_SCANNING_SCALP_REJECT_REASON_A,
											 		CASE_STATUS_3D_SCANNING_SCALP_REJECT_REASON_B,
													CASE_STATUS_3D_SCANNING_SCALP_REJECT_REASON_C,
											 		CASE_STATUS_3D_SCANNING_SCALP_REJECT_REASON_D,
													CASE_STATUS_3D_SCANNING_SCALP_REJECT_REASON_E,
													CASE_STATUS_SETUP_QUALITY_CASE_REJECT,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_A_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_B_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_C_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_D_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_E_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_F_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_G_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_H_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_I_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_J_QUALITY_CHECKING,
											 		CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_K_QUALITY_CHECKING,
													CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_L_QUALITY_CHECKING,
													CASE_STATUS_SETUP_QUALITY_CASE_REJECT_REASON_M_QUALITY_CHECKING
												    );
	}
	
	function setupOperatorsDailyCasesCounter() {
	
		$backgroundColors 							=  array(
															 '#00293C',
															 '#34688F',
															 '#A7424A',
															 '#6E6702',
															 '#A27C27',
															 '#00743F',
															 '#D3AE36',
															 '#720017',
															 '#4D85BD',
															 '#522E74',
															 '#51591E',
															 '#BD3E85',
															 '#8B8604',
															 '#F03725',
															 '#F1435A',
															 '#00293C',
															 '#34688F',
															 '#A7424A',
															 '#1144BF',
															 '#A27C27',
															 '#00743F',
															 '#D3AE36',
															 '#720017',
															 '#F32F09',
															 '#522E74',
															 '#51591E',
															 '#BD3E85',
															 '#8B8604',
															 '#F03725',
															 '#F1435A',
															 '#00293C',
															 '#34688F',
															 '#A7424A',
															 '#1144BF',
															 '#A27C27',
															 '#00743F',
															 '#D3AE36',
															 '#720017',
															 '#F32F09',
															 '#522E74',
															 '#51591E',
															 '#BD3E85',
															 '#8B8604',
															 '#F03725',
															 '#F1435A',
															 '#00293C',
															 '#34688F',
															 '#A7424A',
															 '#1144BF',
															 '#A27C27',
															 '#00743F',
															 '#D3AE36',
															 '#720017',
															 '#F32F09',
															 '#522E74',
															 '#51591E',
															 '#BD3E85',
															 '#8B8604',
															 '#F03725',
															 '#F1435A',
															 );
		
		$previousDate 								=  date("Y-m-d", time() - 86400); 
		$todayDate 									=  date('Y-m-d',time()); 
		$tomorrowDate 								=  date("Y-m-d", time() + 86400); 
		
		$nowTime									=  date("H", time());
		
		/*
		echo $previousDate 								=  '2019-10-15'; 
		echo $todayDate 								=  '2019-10-16';
		echo $tomorrowDate 								=  '2019-10-17';
		
		echo $nowTime									=  '00';*/
		
		if ($nowTime < 06) {
				
				$queryDateStart = $previousDate;	
				$queryDateEnd   = $todayDate;	
		
		} else {
				
				$queryDateStart = $todayDate;	
				$queryDateEnd   = $tomorrowDate;	
		}
		
		
		$dayStart 									=  $queryDateStart.' 06:00:00';
		$dayEnd										=  $queryDateEnd.' 05:59:59';
		
		$setupOperatorsIDs 							=  $this->model_public->getSetupSectionOperatorIDs();
		
		$operatorCasesPerDay						=  $this->model_public->getOperatorsDoneCasesToday($dayStart,$dayEnd,$setupOperatorsIDs); 
		
		$targetCases								= 7;
		$caseOperatorArray 							= array();
		
		if ($operatorCasesPerDay->num_rows() > 0) {
				
				foreach($operatorCasesPerDay->result() as $caseOperator) {
						
						$caseOperatorArray[] = $caseOperator->employeeTeamID;
				}	
		}
		
		$missingOperators = array_diff($setupOperatorsIDs,$caseOperatorArray);
		
		$result['operatorCasesPerDay']				= $operatorCasesPerDay;
		$result['backgroundColors']					= $backgroundColors;
		
		$result['targetCases']						= $targetCases;
		
		$data['pageHeading']    					= "Setup Section Cases - ".date('D, d M Y ',strtotime($queryDateStart));
		
		$data['contents']							= $this->load->view('public/reports/daily_setup_cases_statistics',$result,true);	
		$this->load->view('public/template',$data);
		
	}
	
	function AJAX_setupOperatorsDailyCasesCounter() {
	
		$backgroundColors 							=  array(
															 '#00293C',
															 '#34688F',
															 '#A7424A',
															 '#6E6702',
															 '#A27C27',
															 '#00743F',
															 '#8C489F',
															 '#720017',
															 '#4D85BD',
															 '#522E74',
															 '#51591E',
															 '#BD3E85',
															 '#8B8604',
															 '#F03725',
															 '#F1435A',
															 '#00293C',
															 '#34688F',
															 '#A7424A',
															 '#1144BF',
															 '#A27C27',
															 '#00743F',
															 '#D3AE36',
															 '#720017',
															 '#F32F09',
															 '#522E74',
															 '#51591E',
															 '#BD3E85',
															 '#8B8604',
															 '#F03725',
															 '#F1435A',
															  '#00293C',
															 '#34688F',
															 '#A7424A',
															 '#1144BF',
															 '#A27C27',
															 '#00743F',
															 '#8C489F',
															 '#720017',
															 '#F32F09',
															 '#522E74',
															 '#51591E',
															 '#BD3E85',
															 '#8B8604',
															 '#F03725',
															 '#F1435A',
															  '#00293C',
															 '#34688F',
															 '#A7424A',
															 '#1144BF',
															 '#A27C27',
															 '#00743F',
															 '#D3AE36',
															 '#720017',
															 '#F32F09',
															 '#522E74',
															 '#51591E',
															 '#BD3E85',
															 '#8B8604',
															 '#F03725',
															 '#F1435A',
															 );
															 
		$previousDate 								=  date("Y-m-d", time() - 86400); 
		$todayDate 									=  date('Y-m-d',time()); 
		$tomorrowDate 								=  date("Y-m-d", time() + 86400); 
		
		$nowTime									=  date("H", time());
	
		
		/*echo $previousDate 								=  '2019-10-14'; 
		echo $todayDate 								=  '2019-10-15';
		echo $tomorrowDate 								=  '2019-10-16';
		
		echo $nowTime									=  '23';
	*/
		if ($nowTime < 06) {
				
				$queryDateStart = $previousDate;	
				$queryDateEnd   = $todayDate;	
		
		} else {
				
				$queryDateStart = $todayDate;	
				$queryDateEnd   = $tomorrowDate;	
		}
		
		
		 $dayStart 									=  $queryDateStart.' 06:00:00';
		
		 $dayEnd									=  $queryDateEnd.' 05:59:59';
		
		$setupOperatorsIDs 							=  $this->model_public->getSetupSectionOperatorIDs();
		
		$operatorCasesPerDay						=  $this->model_public->getOperatorsDoneCasesToday($dayStart,$dayEnd,$setupOperatorsIDs); 
		
		$targetCases								= 7;
		$caseOperatorArray 							= array();
		
		if ($operatorCasesPerDay->num_rows() > 0) {
				
				foreach($operatorCasesPerDay->result() as $caseOperator) {
						
						$caseOperatorArray[] = $caseOperator->employeeTeamID;
				}	
		}
		
		$missingOperators = array_diff($setupOperatorsIDs,$caseOperatorArray);
		
		$result['operatorCasesPerDay']				= $operatorCasesPerDay;
		$result['backgroundColors']					= $backgroundColors;
		
		$result['targetCases']						= $targetCases;
		
		$data['pageHeading']    					= "Setup Section Cases - ".date('D, d M Y ',strtotime($queryDateStart));
		
		$this->load->view('public/reports/ajax_daily_setup_cases_statistics',$result,false);	
	}
	
	
	
}
?>
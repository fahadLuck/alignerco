<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public  function __construct() {
		
		parent::__construct();
		
		authentication(); // Calling From Login Helper
		
		/* Table ID*/
		$this->accountID = $this->session->userdata(USER_ACCOUNT_ID_SESSION);
		
		/*Team Member ID*/
		$this->teamID   = $this->session->userdata(USER_TEAM_ID_SESSION);
		
		/*Team Member ID*/
		$this->HRID   = $this->session->userdata(USER_HR_ID_SESSION);
		
		/*Team Member Institute ID*/
		$this->organizationID	= $this->session->userdata(USER_ORGANIZATION_ID_SESSION);
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		$this->load->helper('cpanel/users/users');
		$this->load->model('cpanel/users/model_users');
		
		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');
		
		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper
		
		if (!in_array(MODULE_USERS,$this->accessModules)) {
				
				redirect('my-dashboard/');	
		}
		
	}
	
	
	public  function index() { 
				
				if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
				}
				
				$accountID			= $this->accountID;
				$teamID				= $this->teamID;   
				$organizationID		= $this->organizationID;
				
				/* User Roles And Permission
		
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
		
				$userInfo 						= userInfo($accountID); // Calling From Application Helper
				
				$assignedRoles 					= $this->assignedRoles;
				$accessModules 					= $this->accessModules;
					
				$searchCode						= $this->input->post('search_code');
				$searchName						= $this->input->post('search_name');
				$searchRole						= $this->input->post('search_role');
				
				$searchParms					= array();
				
				$searchParms['searchCode']		= $searchCode;
				$searchParms['searchName']		= $searchName;
				$searchParms['searchRole']		= $searchRole;
				
				$result['assignedRoles']		= $assignedRoles;
				$result['accessModules']		= $accessModules;
				
				$result['organizationID']		= $organizationID;		
				
				$result['roles']      			= getRoles($organizationID); // Calling From User Helper
				$result['users']				= getOfficialEmployees($organizationID,$searchParms); // Calling From Users Helper
						
				$data['pageHeading']    		= '<i class="fa fa-user-circle"></i> Users';
				$data['subHeading']    			= "";
				
				$data['userInfo']            	= $userInfo;
				$data['activeMenu']  			= '2';
				$data['activeSubMenu']  		= '2.1';
				
				$data['metaType']     			= 'internal';
				$data['pageName']    			= 'Users';
				$data['pageTitle']      		= 'Users | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']	  			= $this->load->view('cpanel/users/users/users',$result,true);
				$this->load->view('cpanel/template',$data);		
	}
	
	public  function userAdd() { 
				
				if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
				}
				
				$accountID								= $this->accountID;
				$teamID									= $this->teamID;   
				$organizationID							= $this->organizationID;
				
				/* User Roles And Permission
		
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
		
				$assignedRoles 		= $this->assignedRoles;
				$accessModules 		= $this->accessModules;
				
				 /* Set Form Validation Errors */ 
			  	$this->form_validation->set_rules('employee','employee','trim|required|callback_check_already_exist_user_account');
			  	$this->form_validation->set_rules('password','password','trim|required|min_length[5]');
				$this->form_validation->set_rules('confirm_password','confirm password','trim|required|matches[password]');
				$this->form_validation->set_rules('account_status','account_status','trim|required');
			   
			  	$this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
			  
			 	if($this->form_validation->run() === FALSE ) {
				
						$userInfo 								=  userInfo($accountID); // Calling From Application Helper
					
						$result['assignedRoles']				=  $assignedRoles;
						$result['accessModules']				=  $accessModules;
						
						$result['employeesForUser']				=  getEmployeesForUsers($organizationID);  // Calling From Users Helper
						$result['roles']      			   	    =  getRoles($organizationID); // Calling From User Helper
						
						$data['pageHeading']    				=  '<i class="fa fa-user-circle"></i> User';
						$data['subHeading']    					=  "(new)";
						
						$data['userInfo']            			=  $userInfo;
						$data['activeMenu']  					=  '2';
						$data['activeSubMenu']  				=  '2.1';
				
						
						$data['metaType']     					=  'internal';
						$data['pageName']    					=  'Users';
						$data['pageTitle']      				=  'Users | '.DEFAULT_APPLICATION_NAME;
				
						$data['contents']	  					= $this->load->view('cpanel/users/users/user_add',$result,true);
						$this->load->view('cpanel/template',$data);		
				
				} else {
					
						$employeeID					 			= ($this->input->post('employee') ?: NULL);
						$password								= ($this->input->post('password') ?: NULL);
						$employeePassword						= md5(md5($this->input->post('password')));
						$showPassword							= encodeString($this->input->post('password')); // Calling From General Helper
						$role									= $this->input->post('role');
						$accountStatus					 		= ($this->input->post('account_status') ?: NULL);
						
						$result     							= $this->getEmployeeInfoByID($organizationID,$employeeID);
						
						$row 	   								= $result->row_array();
						
						$employeeID								= $row['employeeID'];
						$emploeeEmail							= $row['employeeEmail'];
						$emploeeMobile							= $row['employeeMobile'];
						$employeeCode							= $row['employeeCode'];
						
						    // Prepair Date To Store In Database
							$dataArray['organization_id']					   	=  $organizationID;
							$dataArray['team_id']					   			=  $employeeID;
							$dataArray['email']									=  $emploeeEmail;
							$dataArray['code']									=  $employeeCode;
							$dataArray['password']  							=  $employeePassword;   
							$dataArray['show_password']  						=  $showPassword;   
							$dataArray['mobile']  								=  $emploeeMobile;    
							$dataArray['account_status']  						=  $accountStatus;   
							
							$dataArray['created']  	 			 				=  DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
							$dataArray['created_by']  	 			 			=  $teamID; 
							$dataArray['created_by_reference_table'] 			=  'MY_ORGANIZATION_TEAM_TABLE';  
							
						     $insertedID =	$this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,$dataArray);
							 
							 	/*Assign User Multiple Roles*/
								if ($role) {
												
									for ($i=0; $i<sizeof($role); $i++)  {
												
												$dataRoleAssign['organization_id']						=	$organizationID;
												$dataRoleAssign['team_official_id']						=	$insertedID;
												$dataRoleAssign['team_id']								=	$employeeID;
												$dataRoleAssign['role_id']								=	$role[$i];
												$dataRoleAssign['created']								=	DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
												$dataRoleAssign['created_by']							=	$teamID;
												$dataRoleAssign['created_by_reference_table']			=	'MY_ORGANIZATION_TEAM_TABLE';
												
												$insertedAssignRoleID  =  $this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE,$dataRoleAssign);
									}
								}
								
								$this->session->set_userdata('admin_msg','Record successfully added.');
								redirect('users/');
				}
	}
	
	public  function userEdit($tableID) { 
		
			 CheckEmptySefURL($tableID);  // Calling From Shared Helper
			  
			 if (!in_array(MODULE_USERS,$this->accessModules)) {
				
						redirect('my-dashboard/');	
			  }
			 
			  $accountID			= $this->accountID;
			  $teamID				= $this->teamID;   
			  $organizationID		= $this->organizationID;
			  
			  /* User Roles And Permission
		
				   - Check Allow Permision or Not
				   - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			  */
			  
			  $assignedRoles 		= $this->assignedRoles;
			  $accessModules 		= $this->accessModules;
			  
			  $tableID				= decodeString($tableID);
			  
			  $employee     		= $this->getUserAccountInfoByID($organizationID,$tableID);
			  
			  $employee			   	= $employee->row_array(); 
			  
			  $tableID 				= $employee['tableID'];
			  $employeeID 			= $employee['employeeID'];
			  $employeeCode 		= $employee['employeeCode'];
			
			  /* Set Form Validation Errors */ 
			  $this->form_validation->set_rules('employee','employee','trim|required');
			  $this->form_validation->set_rules('password','password','trim|min_length[5]');
			  $this->form_validation->set_rules('confirm_password','confirm password','trim|matches[password]');
			  $this->form_validation->set_rules('account_status','account_status','trim|required');
			   
			  $this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
			  
			  if($this->form_validation->run() === FALSE ) { 
		
						$userInfo 								= userInfo($accountID); // Calling From Application Helper
						
						$roles      			   	  		    =  getRoles($organizationID); // Calling From User Helper
						$employeeRoles							=  getEmployeeAssignedRoles($organizationID,$employeeID); // Calling From User Helper
						
						$result['assignedRoles']				=  $assignedRoles;
						$result['accessModules']				=  $accessModules;
						
						$result['roles'] 						= $roles;
						
						$result['employeesForUser']				=  getEmployeesForUsers($organizationID);  // Calling From Users Helper
						
						$result['employee']      				= $employee;
						$result['employeeRoles']				= $employeeRoles;
						
						$result['tableID']      				= encodeString($tableID); // Calling From General Helper
					
						$data['pageHeading']    				='<i class="fa fa-user-circle"></i> User';
						$data['subHeading']    					= "(edit)";
						
						$data['userInfo']            			= $userInfo;
						
						$data['activeMenu']  					= '2';
						$data['activeSubMenu']  				= '2.1';
						
						$data['metaType']     					= 'internal';
						$data['pageName']    					= 'Users';
						$data['pageTitle']      				= 'Users | '.DEFAULT_APPLICATION_NAME;
						
						
						$data['contents']	 = $this->load->view('cpanel/users/users/user_edit',$result,true);
						$this->load->view('cpanel/template',$data);
		
			 } else {
						 
						 $password								= ($this->input->post('password') ?: NULL);
						 
						 if ($password) {
						
							 $employeePassword					= md5(md5($this->input->post('password')));
						 	 $showPassword						= encodeString($this->input->post('password')); // Calling From General Helper
						 }
						 
						 $accountStatus					 		= ($this->input->post('account_status') ?: NULL);
						 
						 $role									= $this->input->post('role');
						
						    // Prepair Date To Store In Database
							if ($password) {
								
									$dataArray['password']  		=  $employeePassword;   
									$dataArray['show_password']  	=  $showPassword;  
									
							}
							
							$dataArray['account_status']  			=  $accountStatus; 
							$dataArray['code']  					=  $employeeCode;      
							
							$affectedRows = $this->model_shared->editRecordWhere(array('id' => $tableID),MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,$dataArray);
							
								/*Assign Employee Multiple Roles*/
								if (!empty($role)) { 
									
									$affectedRows = $this->model_shared->deleteRecordWhere(array('team_id' => $employeeID),MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE);
									
									for ($i=0; $i<sizeof($role); $i++)  {
												
												$dataRoleAssign['organization_id']						=	$organizationID;
												$dataRoleAssign['team_id']								=	$employeeID;
												$dataRoleAssign['role_id']								=	$role[$i];
												$dataRoleAssign['created']								=	DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
												$dataRoleAssign['created_by']							=	$teamID;
												$dataRoleAssign['created_by_reference_table']			=	'MY_ORGANIZATION_TEAM_TABLE';
												
												$insertedAssignRoleID 	=  $this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE,$dataRoleAssign);
									}
										
							    }
								
								$this->session->set_userdata('admin_msg','Record successfully updated.');
								
								redirect('users/');
			}
						
	}
	
	public  function deleteUser($tableID) {
					   
			 CheckEmptySefURL($tableID);  // Calling From Shared Helper
			 
			 if (!in_array(MODULE_USERS,$this->accessModules)) {
				
					redirect('my-dashboard/');	
			  }
			 
			 $accountID							= $this->accountID;
			 $teamID							= $this->teamID;  
			 $organizationID 					= $this->organizationID;
			
			  $tableID							= decodeString($tableID);
			  $result		     				= $this->getUserAccountInfoByID($organizationID,$tableID);
			 
			  if ($result) {
				    
					  $row 					 	= $result->row_array();
					  
					  $tableID 				 	= $row['tableID'];
					  $employeeID 		 		= $row['employeeID'];
					   
					   // Code for Permanent Delete from Database
					   $this->model_shared->deleteRecordWhere(array('team_id' => $employeeID,'is_deleted' => HARD_CODE_ID_NO),MY_ORGANIZATION_TEAM_OFFICIALS_TABLE);
					   $this->model_shared->deleteRecordWhere(array('team_id' => $employeeID,'is_deleted' => HARD_CODE_ID_NO),MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE);
					
					 
					  
					  // Soft Delete From Database Update Record IsDeleted = TRUE 
					  $update['is_deleted'] 								= HARD_CODE_ID_YES;
					  $update['deleted_by'] 								= $teamID;
					  $update['deleted_by_reference_table'] 				= 'MY_ORGANIZATION_TEAM_TABLE';
					  $update['deleted']									= DATABASE_NOW_DATE_TIME_FORMAT(); // Calling From Shared Helper
			  
					  //$this->model_shared->editRecordWhere(array('id' => $tableID,'is_deleted' => HARD_CODE_ID_NO),MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,$update);
					  
					  // Delete User Assigned Roles
					  $updateAssignedRoles['is_deleted'] 					= HARD_CODE_ID_YES;
					  $updateAssignedRoles['deleted_by'] 					= $teamID;
					  $updateAssignedRoles['deleted_by_reference_table'] 	= 'MY_ORGANIZATION_TEAM_TABLE';
					  $updateAssignedRoles['deleted']						= DATABASE_NOW_DATE_TIME_FORMAT(); // Calling From Shared Helper
			  
					  //$this->model_shared->editRecordWhere(array('team_id' => $employeeID,'is_deleted' => HARD_CODE_ID_NO),MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE,$updateAssignedRoles);
					  $this->session->set_userdata('admin_msg','Record successfully deleted.');
				 }
				
				 redirect('users/');
	}
	
	
	public  function roles() { 
			
			 /*Use For Access Permissions*/
			 if (!in_array(MODULE_ROLES_AND_PRIVILEGES,$this->accessModules)) {
				 		
						redirect('my-dashboard/');	
			 }
			 
			  $accountID				= $this->accountID; 
			  $teamID					= $this->teamID; 
			  $organizationID			= $this->organizationID;
			  
			/* User Roles And Permission
	  
				 - Check Allow Permision or Not
				 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			*/
			  
			$assignedRoles 						= $this->assignedRoles;
			$accessModules 						= $this->accessModules;
		
			$userInfo 							 = userInfo($accountID); // Calling From Application Helper
			
			$result['assignedRoles']			=  $assignedRoles;
			$result['accessModules']			=  $accessModules;
			
			$result['modules']      			=  getModules(); // Calling From User Helper
			$result['roles']      			    =  getRoles($organizationID); // Calling From User Helper
			
			$result['pageHeading']    			= "Roles & Privileges";
			
			$data['userInfo']            		= $userInfo;
			
			$data['activeMenu']  				= '4';
			$data['activeSubMenu']  			= '4.1';
			
			$data['metaType']     				= 'internal';
			$data['pageName']    				= 'Roles & Privileges';
			$data['pageTitle']      			= 'Roles & Privileges | '.DEFAULT_APPLICATION_NAME;
			
			$data['contents']	  			= $this->load->view('cpanel/users/role/roles',$result,true);
			$this->load->view('cpanel/template',$data);
	}
	
	public  function roleAdd() { 
			
			 /*Use For Access Permissions*/
			 if (!in_array(MODULE_ROLES_AND_PRIVILEGES,$this->accessModules)) {
				 		
						redirect('my-dashboard/');	
			 }
			 
			  $accountID				= $this->accountID; 
			  $teamID					= $this->teamID; 
			  $organizationID			= $this->organizationID;
			  
			  /* User Roles And Permission
		
				   - Check Allow Permision or Not
				   - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			  */
			  
			  $assignedRoles 					= $this->assignedRoles;
			  $accessModules 					= $this->accessModules;
		
		
		 	  /* Set Form Validation Errors */ 
			  $this->form_validation->set_rules('name','name','trim|required');
			  $this->form_validation->set_rules('description','description','trim');
			   
			  $this->form_validation->set_error_delimiters('<div style="font-size: 80%; color:#dc3545">', '</div>');
			  
			  if($this->form_validation->run() === FALSE ) { 
		
						$userInfo 							 = userInfo($accountID); // Calling From Application Helper
						
						$result['assignedRoles']			=  $assignedRoles;
						$result['accessModules']			=  $accessModules;
						
						$result['pageHeading']    			= "Roles & Privileges";
						
						$data['userInfo']            		= $userInfo;
						
						$data['activeMenu']  				= '4';
						$data['activeSubMenu']  			= '4.1';
						
						$data['metaType']     				= 'internal';
						$data['pageName']    				= 'Roles & Privileges';
						$data['pageTitle']      			= 'Roles & Privileges | '.DEFAULT_APPLICATION_NAME;
						
						$data['contents']	  			= $this->load->view('cpanel/users/role/role_add',$result,true);
						$this->load->view('cpanel/template',$data);
		
			   } else {
						
						$name					 					= ($this->input->post('name') ?: NULL);
						$description					 			= ($this->input->post('description') ?: NULL);
						
						// Prepair Date To Store In Database
						$dataArray['name']							= $name;
						
						      // Check Sef Already Exist Or Not
							 if ($this->check_Already_Exist_SefURL_Role(SEF_URLS($name))) {
									
									 //IF Sef Already Exist Then Concatenate A Unique Number  
									 $dataArray['sef_url']	=	SEF_URLS_CONCAT($name); 	// Calling From General Helper
						
							 } else {
									
									 //IF Sef Not Exist Then Leave It To Orignal Name				
									 $dataArray['sef_url']	=	SEF_URLS($name); // Calling From General Helper
							 }
							   
							$dataArray['description']					   	=  $description;
							$dataArray['publish_status']  					=  HARD_CODE_ID_PUBLISHED;   // Default PUBLISHED 
							
							$dataArray['created']  	 			 			=  DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper
							$dataArray['created_by']  	 			 		=  $teamID;
							$dataArray['created_by_reference_table']  		= 'MY_ORGANIZATION_TEAM_TABLE';
							
						    $insertedID		   								=  $this->model_shared->insertRecord_ReturnID(JOB_ROLES_TABLE,$dataArray);
								
							$dataRow['organization_id']  					=  $organizationID;
							$dataRow['role_id']  							=  $insertedID;
								
						    $insertedAssignID	=	$this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_JOB_ROLES_TABLE,$dataRow);
							
							redirect('roles/');
			}
						
	}
	
	public  function roleEdit($sefURL=NULL) { 
				
			/*Use For Access Permissions*/
			if (!in_array(MODULE_ROLES_AND_PRIVILEGES,$this->accessModules)) {
				 		
						redirect('my-dashboard/');	
			 }
			 
			 CheckEmptySefURL($sefURL);  // Calling From Shared Helper
			  
			  $accountID							=  $this->accountID;
			  $organizationID 						=  $this->organizationID;
			  
			  /* User Roles And Permission
		
				   - Check Allow Permision or Not
				   - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			  */
			  
			  $assignedRoles 						= $this->assignedRoles;
			  $accessModules 						= $this->accessModules;
			  
			  $role     							= $this->getRoleInfoBySefURL($organizationID,$sefURL);
			  
			  $role					   			    = $role->row_array(); 
			  
			  $tableID 								= $role['tableID'];
			  $roleID 								= $role['roleID'];
			  $roleName 							= $role['roleName'];
			  $roleSefURL 							= $role['roleSefURL'];

		 	  /* Set Form Validation Errors */ 
			  $this->form_validation->set_rules('name','name','trim|required');
			  $this->form_validation->set_rules('description','description','trim');
			   
			  $this->form_validation->set_error_delimiters('<div style="font-size: 80%; color:#dc3545">', '</div>');
			  
			  if($this->form_validation->run() === FALSE ) { 
		
						$userInfo 							= userInfo($accountID); // Calling From Application Helper
						
						$result['assignedRoles']			= $assignedRoles;
						$result['accessModules']			= $accessModules;
						
						$result['roleName'] 		  		= $roleName;
						$result['roleSefURL'] 		   		= $roleSefURL;
						$result['roleDescription']    		= $role['roleDescription'];
				  
						$result['pageHeading']    			= "Roles & Privileges";
						
						$data['userInfo']            		= $userInfo;
						
						$data['activeMenu']  				= '4';
						$data['activeSubMenu']  			= '4.1';
						
						$data['metaType']     				= 'internal';
						$data['pageName']    				= 'Roles & Privileges';
						$data['pageTitle']      			= 'Roles & Privileges | '.DEFAULT_APPLICATION_NAME;
						
						
						$data['contents']	  				= $this->load->view('cpanel/users/role/role_edit',$result,true);
						$this->load->view('cpanel/template',$data);
		
			 } else {
						 
						 $name					 			= ($this->input->post('name') ?: NULL);
						 $description					 	= ($this->input->post('description') ?: NULL);
						 	
						  $dataArray['name']   		 	 	 = $name;
						  $dataArray['description']   	 	 = $description;
					  
					  	  	   if ($name != $roleName) {
						
								 	//Check Tab Sef Already Exist Or Not
						 			if ($this->check_Already_Exist_SefURL_Role(SEF_URLS($name))) {
								
											//IF Sef Already Exist Then Concatenate A Unique Number  
									  		$dataArray['sef_url']		 =	 SEF_URLS_CONCAT($name); // Calling From General Helper
											
							 		} else {
									  
									    	//IF Sef Not Exist Then Leave It To Orignal Name				
							    	    	$dataArray['sef_url']		=	SEF_URLS($name); // Calling From General Helper
						 	       }
							  }
							
							 $affectedRows = $this->model_shared->editRecordWhere(array('id' => $roleID),JOB_ROLES_TABLE,$dataArray);
								
							 redirect('roles/');
			}
						
	}
	
	public  function deleteRole($sefURL=NULL) {
					
				       /*Use For Access Permissions*/
					   if (!in_array(MODULE_ROLES_AND_PRIVILEGES,$this->accessModules)) {
				 		
							redirect('my-dashboard/');	
					   }
					   
					   CheckEmptySefURL($sefURL);  // Calling From Shared Helper
					   
					    $accountID						= $this->accountID;
					    $teamID							= $this->teamID;  
						$organizationID 				= $this->organizationID;
					  
					    $result     					= $this->getRoleInfoBySefURL($organizationID,$sefURL);
 						
						if ($result) {
							
								$row 					= $result->row_array();
								
								$tableID 				= $row['tableID'];
								$roleID 				= $row['roleID'];
								$roleName 				= $row['roleName'];
								
								// Soft Delete From Database Update Record IsDeleted = TRUE 
								$update['is_deleted'] 								= HARD_CODE_ID_YES;
								$update['deleted_by'] 								= $teamID;
								$update['deleted_by_reference_table'] 				= 'MY_ORGANIZATION_TEAM_TABLE';
								$update['deleted']									= DATABASE_NOW_DATE_TIME_FORMAT(); // Calling From Shared Helper
						
								$this->model_shared->editRecordWhere(array('role_id' => $roleID,'organization_id' => $organizationID,'is_deleted' => HARD_CODE_ID_NO),MY_ORGANIZATION_JOB_ROLES_TABLE,$update);
								
								$this->model_shared->editRecordWhere(array('id' => $roleID,'is_deleted' => HARD_CODE_ID_NO),JOB_ROLES_TABLE,$update);
								$this->session->set_userdata('admin_msg','Record successfully deleted.');	 	 
						}
				
				 redirect('roles/');
	}
	
	
	
	private function getUserAccountInfoByID($organizationID,$tableID) {
			
			$result = $this->model_users->getUserAccountInfoByID($organizationID,$tableID);		
			
			if ($result) {
			
					return $result;	
			
			} else {
			
					redirect('not-found/');		
			}				
	}
	
	private function getEmployeeInfoByID($organizationID,$employeeID) {
			
			$result = $this->model_employee->getEmployeeInfoByID($organizationID,$employeeID);		
			
			if ($result) {
			
					return $result;	
			
			} else {
			
					redirect('not-found/');		
			}				
	}
	
	
	
	/*Call Back Functions*/
	public  function check_already_exist_user_account($employeeID) {
	
			if ($employeeID) {
				
				  $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_OFFICIALS_TABLE,array('team_id' => $employeeID, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_user_account','Error: This user account already exist. Please try another employee.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	/*Inside Functions Calling*/
	private function getEmployeeRoles($employeeID) {
			
			$result = $this->model_hr_employees->getEmployeeRoles($employeeID);		
			
			if ($result) {
			
					return $result;	
			
			} else {
			
					return false;		
			}				
	}
	
	private function getRoleInfoBySefURL($organizationID,$sefURL) {
			
			$result = $this->model_users->getRoleInfoBySefURL($organizationID,$sefURL);		
			
			if ($result) {
			
					return $result;	
			
			} else {
			
					redirect('not-found/');		
			}				
	}
	
	private function check_Already_Exist_SefURL_Role($sefURL=NULL) {
			
			$result = $this->model_shared->getRecordMultipleWhere('id',JOB_ROLES_TABLE,array('sef_url' => $sefURL,'is_deleted' => HARD_CODE_ID_NO));		

			if ($result->num_rows() > 0) {
			
					return true;	
			
			} else {
			
					return false;
			}				
	}
	
}

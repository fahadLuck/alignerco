<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doctors extends CI_Controller {


	public  function __construct() {
		
		parent::__construct();
		
		 authentication(); // Calling From Login Helper
		
		/* Table ID*/
		$this->accountID = $this->session->userdata(USER_ACCOUNT_ID_SESSION);
		
		/*ID*/
		$this->teamID   = $this->session->userdata(USER_TEAM_ID_SESSION);
		
		/*ID*/
		$this->HRID   = $this->session->userdata(USER_HR_ID_SESSION);
		
		/*ID*/
		$this->organizationID	= $this->session->userdata(USER_ORGANIZATION_ID_SESSION);
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');
		
		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper
	}
	
	public function index($page=0) { 

				
				/*Use For Access Permissions*/
				if (!in_array(MODULE_DOCTOR,$this->accessModules)) {
				
					redirect('my-dashboard/');	
				}
				
				/*echo "here 1"; die();*/

				/* User Roles And Permission
  
					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/
				
				$assignedRoles 					= $this->assignedRoles;
				$accessModules 					= $this->accessModules;
						
				$recordPerPage					= 1000;

				$accountID						= $this->accountID; 
				$organizationID					= $this->organizationID;
				
				$userInfo 						= userInfo($accountID); // Calling From Application Helper
				
				$result['assignedRoles']		=  $assignedRoles;
				$result['accessModules']		=  $accessModules;
				
				$result['bloodGroups'] 			= getBloodGroups(); // Calling From Application Helper
						
				// Code For Search Employee
				$searchParameters 									= array();
				
				$searchBusinessUnit		    						= $this->input->post('business_unit');
				$searchDepartment		    						= $this->input->post('department');
				$searchJobTitle		    							= $this->input->post('job_title');
				$searchPosition		    							= $this->input->post('job_position');
				$searchShiftTimings		    						= $this->input->post('shift_timings');
				
				$searchCustom		    							= $this->input->post('custom_search');
				$searchCustomColumn		    						= $this->input->post('custom_search_column');
				
				$searchParameters['searchBusinessUnit']				= $searchBusinessUnit;
				$searchParameters['searchDepartment']				= $searchDepartment;
				$searchParameters['searchJobTitle']					= $searchJobTitle;
				$searchParameters['searchPosition']					= $searchPosition;
				$searchParameters['searchShiftTimings']				= $searchShiftTimings;
				
				$searchParameters['searchCustom']					= $searchCustom;
				$searchParameters['searchCustomColumn']				= $searchCustomColumn;
				
				$employees	   					 				    = $this->model_employee->getActiveDoctors($page,$recordPerPage,$organizationID,$searchParameters);

				$totalRows			 								=$employees->num_rows(); ;
				
				$mypaing['total_rows']			 					= $totalRows;
		
				$mypaing['base_url'] 								= base_url()."employees/";
				$mypaing['per_page']								= $recordPerPage;
				$mypaing['uri_segment']								= 2;	 
				
				$mypaing['full_tag_open'] 						   = '<ul  class="pagination pagination-sm pull-right">';
				$mypaing['full_tag_close'] 						   = '</ul>';

				$mypaing['first_link'] 							   = 'First';
				$mypaing['first_tag_open'] 						   = '<li>';
				$mypaing['first_tag_close'] 					   = '</li>';
				$mypaing['last_link']							   = 'Last';
				$mypaing['last_tag_open'] 						   = '<li>';
				$mypaing['last_tag_close'] 						   = '</li>';
				$mypaing['next_link'] 							   = 'Next';
				$mypaing['next_tag_open'] 						   = '<li>';
				$mypaing['next_tag_close'] 						   = '</li>';
		
				$mypaing['prev_link'] 							   = 'Previous';
				$mypaing['prev_tag_open'] 						   = '<li>';
				$mypaing['prev_tag_close'] 						   = '</li>';
					
				$mypaing['cur_tag_open'] 						   = '<li><a href="" class="current">';
				$mypaing['cur_tag_close'] 						   = '</a></li>';
					
				$mypaing['num_tag_open'] 						   = '<li>';
				$mypaing['num_tag_close'] 						   = '</li>';  	
							
				$this->pagination->initialize($mypaing);
				$paginglink											= $this->pagination->create_links();
				
				$serialNumber 										= $this->uri->segment(2)+1;

				$result['employees']	 							= $employees;
				
						
				$result['serialNumber']		= $serialNumber;
				$result['pagingLink']		= $paginglink;
				$result['totalResults']		= $totalRows;
				
				$data['pageHeading']    	= "Dentist";	
				$data['subHeading']    		= "(all)";		
				
				$data['userInfo']           = $userInfo;
				
				$data['activeMenu']  		= '6';
				$data['activeSubMenu']  	= '6.1';
				
				$data['metaType']     		= 'internal';
				$data['pageName']    		= 'Dentist';
				$data['pageTitle']      	= 'Dentist | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']	  			 	= $this->load->view('cpanel/doctors/active_doctors_list',$result,true);
				$this->load->view('cpanel/template',$data);
	}
	
	public function doctorAdd() { 
	
			/*Use For Access Permissions*/
			if (!in_array(MODULE_DOCTOR,$this->accessModules)) {
				
				redirect('my-dashboard/');	
			}
			
			$accountID			= $this->accountID; 
			$teamID				= $this->teamID; 
			$organizationID		= $this->organizationID;
			
			/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			*/
			
			$assignedRoles 		= $this->assignedRoles;
			$accessModules 		= $this->accessModules;

		
		
		/* Set Form Validation Errors */ 
		$this->form_validation->set_rules('prefix','prefix','trim');
		$this->form_validation->set_rules('name','name','trim|required');
		$this->form_validation->set_rules('company','company','trim|required');
		$this->form_validation->set_rules('father_name','father name','trim');
		
		$this->form_validation->set_rules('email','email','trim|valid_email|callback_check_already_exist_employee_official_email');
		$this->form_validation->set_rules('employee_code','employee code','trim|numaric|callback_check_already_exist_employee_official_code');
					
		$this->form_validation->set_rules('employment_status','employment status','trim');
		$this->form_validation->set_rules('employee_join_date','date of joining','trim');
		
		$this->form_validation->set_rules('gender','gender','trim|required');
		$this->form_validation->set_rules('blood_group','blood group','trim');
		$this->form_validation->set_rules('nationality','nationality','trim');
		
		$this->form_validation->set_rules('date_of_birth','date of birth','trim');
		$this->form_validation->set_rules('personal_email','personal email','trim|valid_email|callback_check_already_exist_employee_personal_email');
		$this->form_validation->set_rules('mobile','mobile','trim|required|callback_check_already_exist_employee_personal_mobile');
		$this->form_validation->set_rules('street_address','street address','trim');
		$this->form_validation->set_rules('country','country','trim');
		/*$this->form_validation->set_rules('state','state','trim');*/
		$this->form_validation->set_rules('city','city','trim');
					 
		$this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
					
		if($this->form_validation->run() === FALSE ) { 

				$userInfo 										=	userInfo($accountID); // Calling From Application Helper
				
				$result['assignedRoles']						=   $assignedRoles;
				$result['accessModules']						=   $accessModules;
				
				$result['organizationID'] 						=   $organizationID;
				
				$result['prefix'] 								= 	getPrefix(); // Calling From Application Helper
				$result['modeOfEmployment'] 					= 	getModeOfEmployment(); // Calling From Application Helper
				$result['employmentStatus'] 					= 	getEmploymentStatus(); // Calling From Application Helper
				$result['payFrequency'] 						= 	getPayFrequency(); // Calling From Application Helper
				
				$result['gender'] 								= 	getGenderMaleFemale(); // Calling From Application Helper
				$result['maritalStatus'] 						= 	getMaritalStatus(); // Calling From Application Helper
				$result['nationalities'] 						= 	getNationalities(); // Calling From Application Helper
				$result['languages'] 							= 	getLanguages(); // Calling From Application Helper
				$result['bloodGroups'] 							= 	getBloodGroups(); // Calling From Application Helper
				
				$result['countries']     						=   getAllCountries(); //  Calling From Shared Helper;
				$result['states']          						=   getAllStates(); //  Calling From Shared Helper;
				$result['cities']           					=   getAllCities(); //  Calling From Shared Helper;

				$result['companies']                            =   getAllCompanies(); //  Calling From Shared Helper;
				
				$data['pageHeading']    						=  "Dentist";
				$data['subHeading']    							=  "(new)";
				
				$data['userInfo']            					=   $userInfo;
				
				$data['activeMenu']  							=  '6';
				$data['activeSubMenu']  						=  '6.1'; 
				
				$data['metaType']     							=  'internal';
				$data['pageName']    							=  'New Dentist';
				$data['pageTitle']      						=  'New Dentist | '.DEFAULT_APPLICATION_NAME;
				
				$data['contents']	= $this->load->view('cpanel/doctors/doctor_add',$result,true);
				$this->load->view('cpanel/template',$data);

		} 
		else {
					
				$prefix 																= ($this->input->post('prefix') ?: NULL);
				$name 																	= ($this->input->post('name') ?: NULL);
				$fatherName 															= ($this->input->post('father_name') ?: NULL);
				$modeOfEmployment 														= ($this->input->post('mode_of_employment') ?: NULL);
				$email					 												= ($this->input->post('email') ?: NULL);
				$employeeCode					 										= ($this->input->post('employee_code') ?: NULL);
				
				$employmentStatus 														= ($this->input->post('employment_status') ?: NULL);
				$joinDate 																= DATABASE_DATE_FORMAT($this->input->post('employee_join_date')); // Calling From Shared Helper
						
				$gender 																= ($this->input->post('gender') ?: NULL);
				$dateOfBirth 															= DATABASE_DATE_FORMAT($this->input->post('date_of_birth')); // Calling From Shared Helper
				$bloodGroup 															= ($this->input->post('blood_group') ?: NULL);
				$nationality 															= ($this->input->post('nationality') ?: NULL);
				
				$personalEmail														    = ($this->input->post('personal_email') ?: NULL);
				$mobileFormat					    									= ($this->input->post('mobile') ?: NULL);
				$mobile					    											= cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($this->input->post('mobile')))); // Calling From General Helper
				$country														    	= ($this->input->post('country') ?: NULL);
				
				/*$state					    											= ($this->input->post('state') ?: NULL);*/

				$company					    											= ($this->input->post('company') ?: NULL);

				$city					   												= ($this->input->post('city') ?: NULL);
				$streetAddress															= ($this->input->post('street_address') ?: NULL);
				
				$createdDated															= DATABASE_NOW_DATE_TIME_FORMAT();
			
				$EMCode 																= $employeeCode;
				$EMNumber 																= NULL;
						
				// Prepair Date To Store In Database
				$dataArrayHR['prefix']								 					=	$prefix;	
				$dataArrayHR['name']													=	$name;
				$dataArrayHR['father_name']								 				=	$fatherName;	
				$dataArrayHR['gender']								 					=	$gender;	
				$dataArrayHR['marital_status']										 	=	NULL;
				$dataArrayHR['nationality']								 				=	$nationality;	
				$dataArrayHR['date_of_birth']											=	$dateOfBirth;	
				$dataArrayHR['blood_group']											 	=	$bloodGroup;	
				
				$dataArrayHR['personal_email']								 		 	=	$personalEmail;	
				$dataArrayHR['mobile_formatted']								 	 	=	$mobileFormat;
				$dataArrayHR['mobile']								 					=	$mobile;
				$dataArrayHR['mobile_verified_status']									=	HARD_CODE_ID_VERIFIED;
				$dataArrayHR['country']								 		 			=	$country;	
				/*$dataArrayHR['state']								 		 			=	$state;*/

				$dataArrayHR['company']								 		 	        =	$company;

				$dataArrayHR['city']								 					=	$city;	
				$dataArrayHR['street_address']								 			=	$streetAddress;	
				
				$dataArrayHR['created']													= 	$createdDated; 
				$dataArrayHR['created_by']												= 	$teamID;
				$dataArrayHR['created_by_reference_table']						  		= 	'MY_ORGANIZATION_TEAM_TABLE';
						
				$HRID_Exist  = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('mobile' => $mobile, 'is_deleted' => HARD_CODE_ID_NO));		
				
				if ($HRID_Exist->num_rows() > 0) {
					
					$HRID = $HRID_Exist->row_array();
					
					$insertedHRID 	= $HRID['id'];
				
				} else {
				
					$insertedHRID 	= $this->model_shared->insertRecord_ReturnID(APPLICATION_HR_TABLE,$dataArrayHR);
				}
				
				$employmentStatus 														= HARD_CODE_ID_PERMANENT;
					
				$dataArray['organization_id']   		 			 					=	$organizationID;
				$dataArray['HR_id']   		 			 							 	=	$insertedHRID;
				$dataArray['code']								 					  	= 	$EMCode;	
				$dataArray['employee_number']						 		  	  	 	= 	$EMNumber;	
				$dataArray['mode_of_employment']								 	 	=	1; // 1 = Direct
				$dataArray['email']														=	$email;	
				
				$dataArray['employment_status']								 	 		=	$employmentStatus;	
				$dataArray['date_of_joining']										 	=	$joinDate;
				
				$dataArray['created']													= 	$createdDated; 
				$dataArray['created_by']												= 	$teamID;

				$dataArray['employType_ID']													= 	HARD_CODE_ID_EMPLOYTYPE_DOCTOR;


				$dataArray['created_by_reference_table']						  	 	= 	'MY_ORGANIZATION_TEAM_TABLE';
					
				$insertedTeamID 	= $this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_TABLE,$dataArray);
							
				/*Assign Employee HR Group*/
				 $dataGroupAssign['organization_id']   	=  $organizationID;
				 $dataGroupAssign['team_id']			=  $insertedTeamID;
				 $dataGroupAssign['group_id']			=  HARD_CODE_ID_APPLICATION_HR_GROUP_EMPLOYEE;
			  
				 $insertedAssignGroupID 	 			=  $this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_GROUPS_TABLE,$dataGroupAssign);

				 $state2=$this->input->post('state');
				 $licience=$this->input->post('licience');

				 for($i=0; $i<sizeof($state2); $i++) 
				 {
				 	// Prepair Date To Store In Database
					
					$dataArrayState['state_id']								 		 		=	$state2[$i];

					$dataArrayState['licience']								 		 		=	$licience[$i];

					$dataArrayState['team_id']								 		 	        =	$insertedTeamID;

					$dataArrayState['hr_id']								 					=	$insertedHRID;	
						
					$dataArrayState['created']													= 	$createdDated; 
					$dataArrayState['created_by']												= 	$teamID;
					$dataArrayState['created_by_reference_table']						  		= 	'MY_ORGANIZATION_TEAM_TABLE';
							
					$insertedStateID 	= $this->model_shared->insertRecord_ReturnID(ASSIGNED_STATES_TABLE,$dataArrayState);
				 }


				 /*for($i=0; $i<sizeof($licience); $i++) 
				 {
					
					$dataArraylicience['licience']				=	$licience[$i];
							
					$this->model_shared->updateRecord_ReturnID(ASSIGNED_STATES_TABLE,$dataArraylicience,$hr_id);
				 }*/

				 if ($this->input->post('submit') == 'Save & New') {
								
						redirect('doctor-add');
				
				} else {
								
						redirect('doctors/');
				}
			}
	}








	public function doctorEdit($tableID) 
	{ 
			/*Use For Access Permissions*/
			if (!in_array(MODULE_DOCTOR,$this->accessModules)) {
			
				redirect('my-dashboard/');	
			}
			
			/* User Roles And Permission

				 - Check Allow Permision or Not
				 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
			*/

			$tableID = decodeString($tableID);
			CheckEmptySefURL($tableID);  // Calling From Shared Helper

			$assignedRoles 					= $this->assignedRoles;
			$accessModules 					= $this->accessModules;
					
			$recordPerPage					= 1000;

			$accountID						= $this->accountID; 
			$organizationID					= $this->organizationID;
			
			$userInfo 						= userInfo($accountID); // Calling From Application Helper
			
			$result['assignedRoles']		=  $assignedRoles;
			$result['accessModules']		=  $accessModules;
			
			$result['bloodGroups'] 			= getBloodGroups(); // Calling From Application Helper
					
			
			$employees	   					 				    = $this->model_employee->getSingalActiveEmployees($organizationID,$tableID);

			$assignedStates	   					 				    =  $this->model_shared->getRecordMultipleWhere('state_id,licience,id',ASSIGNED_STATES_TABLE,array('team_id' => $tableID));

			/*foreach($assignedStates->result() as $as)
			{
				echo $team_id=$as->state_id;
			}*/

			/*$totalRows			 								= $this->model_employee->getActiveEmployeesNum($organizationID,$searchParameters);*/
			
			
			
			/*$serialNumber 										= $this->uri->segment(2)+1;*/

			$result['employees']	 							= $employees;
			$result['assignedStates']	 							= $assignedStates;
			$result['states']          						=   getAllStates(); //  Calling From 
			$result['gender'] 								= 	getGenderMaleFemale();
			$result['tableID']      				= encodeString($tableID);

			/*$gender								= 	getGenderMaleFemale();
			foreach($gender->result() as $gen) 
			{
				echo"<br>".$gen->genderID;
			}
			$employees->genderName; die();*/

			
					
			/*$result['serialNumber']		= $serialNumber;
			$result['pagingLink']		= $paginglink;*/
			/*$result['totalResults']		= $totalRows;*/
			
			$data['pageHeading']    	= "Dentist";	
			$data['subHeading']    		= "(Edit)";		
			
			$data['userInfo']           = $userInfo;
			
			$data['activeMenu']  		= '6';
			$data['activeSubMenu']  	= '6.1';
			
			$data['metaType']     		= 'internal';
			$data['pageName']    		= 'Dentist';
			$data['pageTitle']      	= 'Dentist | '.DEFAULT_APPLICATION_NAME;
			
			$data['contents']	  			 	= $this->load->view('cpanel/doctors/active_doctors_edit',$result,true);


			$submit=$this->input->post('submit');

			if(!isset($submit))
			{
				$this->load->view('cpanel/template',$data);
			}




			else
			{
				/* Set Form Validation Errors */ 
				$this->form_validation->set_rules('prefix','prefix','trim');
				$this->form_validation->set_rules('name','name','trim|required');
				$this->form_validation->set_rules('father_name','father name','trim');
				
				$this->form_validation->set_rules('email','email','trim|required|callback_duplicate_email');

				$this->form_validation->set_rules('employee_code','employee code','trim|numaric|required|callback_duplicate_code');
							
				$this->form_validation->set_rules('employment_status','employment status','trim');
				$this->form_validation->set_rules('employee_join_date','date of joining','trim');
				
				$this->form_validation->set_rules('gender','gender','trim|required');
				$this->form_validation->set_rules('blood_group','blood group','trim');
				$this->form_validation->set_rules('nationality','nationality','trim');
				
				$this->form_validation->set_rules('date_of_birth','date of birth','trim');
				/*$this->form_validation->set_rules('personal_email','personal email','trim|required');*/
				$this->form_validation->set_rules('mobile','mobile','trim|numaric|required|callback_duplicate_mobile');
				$this->form_validation->set_rules('street_address','street address','trim');
				$this->form_validation->set_rules('country','country','trim');
				/*$this->form_validation->set_rules('state','state','trim');*/
				$this->form_validation->set_rules('city','city','trim');
							 
				$this->form_validation->set_error_delimiters('<span class="help-block text-red">','<span>');
							
				if($this->form_validation->run() === FALSE ) { 

						$userInfo 										=	userInfo($accountID); // Calling From Application Helper
						
						$result['assignedRoles']						=   $assignedRoles;
						$result['accessModules']						=   $accessModules;
						
						$result['organizationID'] 						=   $organizationID;
						
						$result['prefix'] 								= 	getPrefix(); // Calling From Application Helper
						$result['modeOfEmployment'] 					= 	getModeOfEmployment(); // Calling From Application Helper
						$result['employmentStatus'] 					= 	getEmploymentStatus(); // Calling From Application Helper
						$result['payFrequency'] 						= 	getPayFrequency(); // Calling From Application Helper
						
						$result['gender'] 								= 	getGenderMaleFemale(); // Calling From Application Helper
						$result['maritalStatus'] 						= 	getMaritalStatus(); // Calling From Application Helper
						$result['nationalities'] 						= 	getNationalities(); // Calling From Application Helper
						$result['languages'] 							= 	getLanguages(); // Calling From Application Helper
						$result['bloodGroups'] 							= 	getBloodGroups(); // Calling From Application Helper
						
						$result['countries']     						=   getAllCountries(); //  Calling From Shared Helper;
						$result['states']          						=   getAllStates(); //  Calling From Shared Helper;
						$result['cities']           					=   getAllCities(); //  Calling From Shared Helper;

						$result['companies']                            =   getAllCompanies(); //  Calling From Shared Helper;
						
						$data['pageHeading']    						=  "Dentist";
						$data['subHeading']    							=  "(edit)";
						
						$data['userInfo']            					=   $userInfo;
						
						$data['activeMenu']  							=  '6';
						$data['activeSubMenu']  						=  '6.1'; 
						
						$data['metaType']     							=  'internal';
						$data['pageName']    							=  'Edit Dentist';
						$data['pageTitle']      						=  'Edit Dentist | '.DEFAULT_APPLICATION_NAME;


						$prefix 																= ($this->input->post('prefix') ?: NULL);
						$name 																	= ($this->input->post('name') ?: NULL);

							$result['name']=$name; 
							$result['submit']=$submit; 


						$fatherName 															= ($this->input->post('father_name') ?: NULL);
						$modeOfEmployment 														= ($this->input->post('mode_of_employment') ?: NULL);
						$email					 												= ($this->input->post('email') ?: NULL);
						$employeeCode					 										= ($this->input->post('employee_code') ?: NULL);
						
						$employmentStatus 														= ($this->input->post('employment_status') ?: NULL);
						$joinDate 																= DATABASE_DATE_FORMAT($this->input->post('employee_join_date')); // Calling From Shared Helper
								
						$gender 																= ($this->input->post('gender') ?: NULL);
						$dateOfBirth 															= DATABASE_DATE_FORMAT($this->input->post('date_of_birth')); // Calling From Shared Helper
						$bloodGroup 															= ($this->input->post('blood_group') ?: NULL);
						$nationality 															= ($this->input->post('nationality') ?: NULL);
						
						$personalEmail														    = ($this->input->post('personal_email') ?: NULL);
						$mobileFormat					    									= ($this->input->post('mobile') ?: NULL);
						$mobile					    											= cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($this->input->post('mobile')))); // Calling From General Helper
						$country														    	= ($this->input->post('country') ?: NULL);
						$state					    											= ($this->input->post('state') ?: NULL);
						$company					    											= ($this->input->post('company') ?: NULL);
						$city					   												= ($this->input->post('city') ?: NULL);
						$streetAddress															= ($this->input->post('street_address') ?: NULL);
						
						$createdDated															= DATABASE_NOW_DATE_TIME_FORMAT();

						
						$data['contents']	= $this->load->view('cpanel/doctors/active_doctors_edit',$result,true);
						$this->load->view('cpanel/template',$data);

				} 
				else {
					    $HRID 																	= ($this->input->post('HRID') ?: NULL);

					    $tableID 																	= ($this->input->post('tableID') ?: NULL);
							
						$prefix 																= ($this->input->post('prefix') ?: NULL);
						$name 																	= ($this->input->post('name') ?: NULL);
						$fatherName 															= ($this->input->post('father_name') ?: NULL);
						$modeOfEmployment 														= ($this->input->post('mode_of_employment') ?: NULL);
						$email					 												= ($this->input->post('email') ?: NULL);
						$employeeCode					 										= ($this->input->post('employee_code') ?: NULL);
						
						$employmentStatus 														= ($this->input->post('employment_status') ?: NULL);
						$joinDate 																= DATABASE_DATE_FORMAT($this->input->post('employee_join_date')); // Calling From Shared Helper
								
						$gender 																= ($this->input->post('gender') ?: NULL);
						$dateOfBirth 															= DATABASE_DATE_FORMAT($this->input->post('date_of_birth')); // Calling From Shared Helper
						$bloodGroup 															= ($this->input->post('blood_group') ?: NULL);
						$nationality 															= ($this->input->post('nationality') ?: NULL);
						
						$personalEmail														    = ($this->input->post('personal_email') ?: NULL);
						$mobileFormat					    									= ($this->input->post('mobile') ?: NULL);
						$mobile					    											= cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($this->input->post('mobile')))); // Calling From General Helper
						$country														    	= ($this->input->post('country') ?: NULL);
						$state					    											= ($this->input->post('state') ?: NULL);

						$company					    											= ($this->input->post('company') ?: NULL);

						$city					   												= ($this->input->post('city') ?: NULL);
						$streetAddress															= ($this->input->post('street_address') ?: NULL);
						
						$createdDated															= DATABASE_NOW_DATE_TIME_FORMAT();
					
						$EMCode 																= $employeeCode;
						$EMNumber 																= NULL;
								
						// Prepair Date To Store In Database
						$dataArrayHR['prefix']								 					=	$prefix;	
						$dataArrayHR['name']													=	$name;
						$dataArrayHR['father_name']								 				=	$fatherName;	
						$dataArrayHR['gender']								 					=	$gender;	
						$dataArrayHR['marital_status']										 	=	NULL;
						$dataArrayHR['nationality']								 				=	$nationality;	
						$dataArrayHR['date_of_birth']											=	$dateOfBirth;	
						$dataArrayHR['blood_group']											 	=	$bloodGroup;	
						
						$dataArrayHR['personal_email']								 		 	=	$personalEmail;	
						$dataArrayHR['mobile_formatted']								 	 	=	$mobileFormat;
						$dataArrayHR['mobile']								 					=	$mobile;
						$dataArrayHR['mobile_verified_status']									=	HARD_CODE_ID_VERIFIED;
						$dataArrayHR['country']								 		 			=	$country;	
						/*$dataArrayHR['state']								 		 			=	$state;*/
						$dataArrayHR['company']								 		 			=	$company;		
						$dataArrayHR['city']								 					=	$city;	
						$dataArrayHR['street_address']								 			=	$streetAddress;	
						
						$dataArrayHR['created']													= 	$createdDated; 
						$dataArrayHR['created_by']												= 	$teamID;
						$dataArrayHR['created_by_reference_table']						  		= 	'MY_ORGANIZATION_TEAM_TABLE';
								
						/*$HRID_Exist  = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('mobile' => $mobile, 'is_deleted' => HARD_CODE_ID_NO));		
						
						if ($HRID_Exist->num_rows() > 0) {
							
							$HRID = $HRID_Exist->row_array();
							
							$insertedHRID 	= $HRID['id'];
						
						} else {*/
						
							$insertedHRID 	= $this->model_shared->updateRecord_ReturnID(APPLICATION_HR_TABLE,$dataArrayHR,$HRID);
						/*}*/
						
						$employmentStatus 														= HARD_CODE_ID_PERMANENT;
							
						$dataArray['organization_id']   		 			 					=	$organizationID;
						$dataArray['HR_id']   		 			 							 	=	$HRID;
						$dataArray['code']								 					  	= 	$EMCode;	
						$dataArray['employee_number']						 		  	  	 	= 	$EMNumber;	
						$dataArray['mode_of_employment']								 	 	=	1; // 1 = Direct
						$dataArray['email']														=	$email;	
						
						$dataArray['employment_status']								 	 		=	$employmentStatus;	
						$dataArray['date_of_joining']										 	=	$joinDate;
						
						$dataArray['created']													= 	$createdDated; 
						$dataArray['created_by']												= 	$teamID;
						$dataArray['created_by_reference_table']						  	 	= 	'MY_ORGANIZATION_TEAM_TABLE';
							
						$insertedTeamID 	= $this->model_shared->updateRecord_ReturnID(MY_ORGANIZATION_TEAM_TABLE,$dataArray,$tableID);

						 $assignedStateID=$this->input->post('assignedStateID');

						 $stateUpdate=$this->input->post('state');
						 $licienceUpdate=$this->input->post('licience');

						

						 for($i=0; $i<sizeof($stateUpdate); $i++) 
						 {
						 	// Prepair Date To Store In Database
							
							$updateArrayState['state_id']								 		 		=	$stateUpdate[$i];

							$updateArrayState['licience']								 		 		=	$licienceUpdate[$i];

							$id	 =	$assignedStateID[$i];

							$this->model_shared->editRecordWhere(array('id' => $id),ASSIGNED_STATES_TABLE,$updateArrayState);
						 } 


						 $state_2=$this->input->post('state_2');
						 $licience_2=$this->input->post('licience_2');

						 for($i=0; $i<sizeof($state_2); $i++) 
						 {
						 	// Prepair Date To Store In Database
							
							$dataUpdateArrayState['state_id']								 		 		=	$state_2[$i];

							$dataUpdateArrayState['licience']								 		 		=	$licience_2[$i];

							$dataUpdateArrayState['team_id']								 		 	        =	$tableID;

							$dataUpdateArrayState['hr_id']								 					=	$HRID;	
								
							$dataUpdateArrayState['created']													= 	$createdDated; 
							$dataUpdateArrayState['created_by']												= 	$teamID;
							$dataUpdateArrayState['created_by_reference_table']						  		= 	'MY_ORGANIZATION_TEAM_TABLE';
									
							$this->model_shared->insertRecord_ReturnID(ASSIGNED_STATES_TABLE,$dataUpdateArrayState);
						 }

									
						/*Assign Employee HR Group*/
						 /*$dataGroupAssign['organization_id']   	=  $organizationID;
						 $dataGroupAssign['team_id']			=  $insertedTeamID;
						 $dataGroupAssign['group_id']			=  HARD_CODE_ID_APPLICATION_HR_GROUP_EMPLOYEE;
					  
						 $insertedAssignGroupID 	 			=  $this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_GROUPS_TABLE,$dataGroupAssign);*/

						 redirect(base_url().'doctors');
					
					
						 /*if ($this->input->post('submit') == 'Save & New') {
										
								redirect('employee-add');
						
						} else {
										
								redirect('employees/');
						}*/
					}
			}
			

	}







	
	public  function doctorRemove($employeeID) {
			
			   /*Use For Access Permissions*/
			   if (!in_array(MODULE_DOCTOR,$this->accessModules)) {
				
					redirect('my-dashboard/');	
			   }
			  
			  CheckEmptySefURL($employeeID);  // Calling From Shared Helper
			  
			  $accountID			= $this->accountID;
			  $teamID				= $this->teamID;
			  $organizationID 		= $this->organizationID;
			  
			  $employeeID			= decodeString($employeeID); // Calling From General Helper
			 						   
			  $result     			= $this->getEmployeeInfoByID($organizationID,$employeeID);
							  
				if ($result) {
								  
					  $row 	   							= $result->row_array();

					  $tableID 							= $row['tableID'];
					  $employeeID 						= $row['employeeID'];
					  $employeeTeamID 					= $row['employeeTeamID'];
					  $employeeHRID 					= $row['employeeHRID'];
					  $employeeName 					= $row['employeeName'];
					  $employeePrefixName 				= $row['prefixName'];
					  
					  // Soft Delete From Database Update Record IsDeleted = TRUE 
					  $update['is_deleted'] 					= HARD_CODE_ID_YES;
					  $update['deleted_by'] 					= $teamID;
					  $update['deleted_by_reference_table'] 	= 'MY_ORGANIZATION_TEAM_TABLE';
					  $update['deleted']						= DATABASE_NOW_DATE_TIME_FORMAT(); // Calling From Shared Helper
		  
					  $this->model_shared->editRecordWhere(array('id' => $employeeID,'organization_id' => $organizationID,'is_deleted' => HARD_CODE_ID_NO),MY_ORGANIZATION_TEAM_TABLE,$update);
					  $this->model_shared->editRecordWhere(array('id' => $employeeHRID,'is_deleted' => HARD_CODE_ID_NO),APPLICATION_HR_TABLE,$update);
				  }
	
				  redirect('doctors/');	 
	}
	
	/*Call Back Functions*/



	public  function duplicate_code($txtCode) {
		$tableID = ($this->input->post('tableID') ?: NULL);
	
			$organizationID	  = $this->organizationID;
			$is_deleted=HARD_CODE_ID_NO;
			
			
			if ($txtCode) {

				$where="organization_id ='$organizationID' AND code='$txtCode' AND id!='$tableID' AND is_deleted='$is_deleted' ";
				$tableName=MY_ORGANIZATION_TEAM_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_code','Error: This code \''.$txtCode.'\' already exists. Please try anothr code.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}


	public  function duplicate_email($txtEmail) {
		$tableID = ($this->input->post('tableID') ?: NULL);
	
			$organizationID	  = $this->organizationID;
			$is_deleted=HARD_CODE_ID_NO;
			
			
			if ($txtEmail) {

				$where="organization_id ='$organizationID' AND email='$txtEmail' AND id!='$tableID' AND is_deleted='$is_deleted' ";
				$tableName=MY_ORGANIZATION_TEAM_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_email','Error: This email \''.$txtEmail.'\' already exists. Please try anothr email.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}



	public  function duplicate_mobile($txtMobile) {
		$HRID = ($this->input->post('HRID') ?: NULL);
		$is_deleted=HARD_CODE_ID_NO;
	
			$organizationID	  = $this->organizationID;
			$mobile		= cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($txtMobile))); // Calling From General Helper
			
			if ($txtMobile) {

				$where="mobile =$mobile AND id!='$HRID' AND is_deleted='$is_deleted' ";
				$tableName=APPLICATION_HR_TABLE;
				  $result = $this->model_shared->check_duplicate($tableName,$where);

					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('duplicate_mobile','Error: This mobile \''.$txtMobile.'\' already exists. Please try anothr mobile.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}


	
	public  function check_already_exist_employee_official_email($txtEmail) {
	
			$organizationID	 = $this->organizationID;
			
			if ($txtEmail) {
				
				  $result  = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_TABLE,array('organization_id'=>$organizationID, 'email' => $txtEmail, 'is_deleted' => HARD_CODE_ID_NO));		
				
					 if ($result->num_rows() > 0) {
					  
							 $this->form_validation->set_message('check_already_exist_employee_official_email','Error: This email \''.$txtEmail.'\' already exists. Please try anothr email adddress.');
							 return false;
				  
					  } else {
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_employee_official_code($txtCode) {
	
			$organizationID	  = $this->organizationID;
			
			if ($txtCode) {
				
				  $result  				    = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_TABLE,array('organization_id'=>$organizationID, 'code' => $txtCode, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_employee_official_code','Error: This code \''.$txtCode.'\' already exists. Please try anothr code.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_employee_personal_email($txtEmail) {
	
			if ($txtEmail) {
				
				  $result  				    = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('personal_email' => $txtEmail, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_employee_personal_email','Error: This email \''.$txtEmail.'\' already exists. Please try another email adddress.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_employee_personal_CNIC($txtCNIC) {
	
			if ($txtCNIC) {
				  			
				  $CNIC						=	cleanSringFromDashes($txtCNIC); // Calling From General Helper
				  
				  $result  				    = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('identity_card' => $CNIC, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_employee_personal_CNIC','Error: This CNIC number \''.$txtCNIC.'\' already exists. Please try another number.');
								   return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_employee_personal_mobile($txtMobile) {
	
			if ($txtMobile) {
				  			
				  $mobile		= cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($txtMobile))); // Calling From General Helper
				  
				  $result  		= $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('mobile' => $mobile, 'is_deleted' => HARD_CODE_ID_NO));		
				
					  if ($result->num_rows() > 0) {
					  
								   return true;
								   
								   //$this->form_validation->set_message('check_already_exist_employee_personal_mobile','Error: This mobile number \''.$txtMobile.'\' already exists. Please try another number.');
								   //return false;
				  
					  } else 	{
						  
						  return true;	
					  }
			
			} else {
				
				return true;	
			}

	}
	
	public function check_picture_extension() {
		
		 if ($_FILES['picture_data']['name']) {
		
			  $fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['picture_data']['name'])); // Calling From General Helper
			  $fname 			= str_replace(" ","_",$fname);
			  $fname 			= str_replace("%","_",$fname);
			  
			  
			  $nameExt 			= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['picture_data']['name'])))); // Calling From General Helper

			  $fileName			= @convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['picture_data']['name']))); // Calling From General Helper
			  
			  if($nameExt !='jpg' && $nameExt !='gif' && $nameExt !='png')  {
				 
				$this->form_validation->set_message('check_picture_extension','Error: Wrong image type.');
				return false;		
			 
			 } else {
				
				return true;	 
			 }	
			
		 } else {
			 
				return true;	 
		}
	}
	
	
	public  function check_already_exist_edit_employee_official_email($txtEmail) {
		
		$organizationID	  		= $this->organizationID;
		
		if ($txtEmail) {
	
		 	$oldEmail 			= $this->input->post('exiting_official_email');
	
			if ($txtEmail != $oldEmail) {
			
				    $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_TABLE,array('organization_id'=>$organizationID, 'email' => $txtEmail, 'is_deleted' => HARD_CODE_ID_NO));		
				
						  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_edit_employee_official_email','Error: This email \''.$txtEmail.'\' already exists. Please try another email adddress.');
								   return false;
				  
						  } else 	{
						  
							  return true;	
				}
		
		} else {
			
					return true;	
			}
		} else {
				
				return true;	
		}
	}
	
	public  function check_already_exist_edit_employee_official_code($txtCode) {
		
		$organizationID	  = $this->organizationID;
		
		if ($txtCode) {
	
		 	$oldCode 			= $this->input->post('exiting_official_code');
	
			if ($txtCode != $oldCode) {
			
				    $result   = $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_TABLE,array('organization_id'=>$organizationID, 'code' => $txtCode, 'is_deleted' => HARD_CODE_ID_NO));		
				
						  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_edit_employee_official_code','Error: This code \''.$txtCode.'\' already exists. Please try another code.');
								   return false;
				  
						  } else 	{
						  
							  return true;	
				}
		
		} else {
			
					return true;	
			}
		} else {
				
				return true;	
		}
	}
		
	public  function check_already_exist_edit_employee_personal_email($txtEmail) {
	
			if ($txtEmail) {
				
				 $oldEmail 	= $this->input->post('exiting_personal_email');
				  
				  if ($txtEmail != $oldEmail) {
			
				    $result   = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('personal_email' => $txtEmail, 'is_deleted' => HARD_CODE_ID_NO));		
				
						  if ($result->num_rows() > 0) {
					  
								   $this->form_validation->set_message('check_already_exist_edit_employee_personal_email','Error: This email \''.$txtEmail.'\' already exists. Please try another email adddress.');
								   return false;
				  
						  } else 	{
						  
							  return true;	
						 }
		
				  } else {
						
						return true;	  
				 }
				
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_edit_employee_personal_CNIC($txtCNIC) {
	
			if ($txtCNIC) {
				  			
				  $oldCNIC 				= $this->input->post('exiting_CNIC');
				  $CNIC						=	cleanSringFromDashes($txtCNIC); // Calling From General Helper
				  
				   if ($CNIC != $oldCNIC) {
				  
							  $result    = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('identity_card' => $CNIC, 'is_deleted' => HARD_CODE_ID_NO));		
				
					 			 if ($result->num_rows() > 0) {
					  
									   $this->form_validation->set_message('check_already_exist_edit_employee_personal_CNIC','Error: This CNIC number \''.$txtCNIC.'\' already exists. Please try another number.');
								   	   return false;
				  
								 } else 	{
						  
									   return true;	
					  			}
				   } else {
						
							return true;   
				   }
			
			} else {
				
				return true;	
			}

	}
	
	public  function check_already_exist_edit_employee_personal_mobile($txtMobile) {
	
			if ($txtMobile) {
				  			
				  $oldMobile 				= $this->input->post('exiting_mobile');
				  $mobile					=	cleanPhoneStartingZero(cleanSringFromBrackets(removeAllSpacesFromString($txtMobile))); // Calling From General Helper
				  
				  if ($mobile != $oldMobile) {
				  
				   $result  = $this->model_shared->getRecordMultipleWhere('id',APPLICATION_HR_TABLE,array('mobile' => $mobile, 'is_deleted' => HARD_CODE_ID_NO));		
			
					  if ($result->num_rows() > 0) {
					  
								   return true;	
								   
								   //$this->form_validation->set_message('check_already_exist_edit_employee_personal_mobile','Error: This mobile number \''.$txtMobile.'\' already exists. Please try another number.');
								   //return false;
				  
					  } else {
						  
						  return true;	
					  }
				  
				  } else {
				  		
						return true;
				  }
			
			} else {
				
				return true;	
			}
	}
	
	
	private function getEmployeeInfoByID($organizationID,$tableID) {
			
			$result = $this->model_employee->getEmployeeInfoByID($organizationID,$tableID);		
			
			if ($result) {
			
				return $result;	
			
			} else {
			
				redirect('not-found/');		
			}				
	}
	
	
	/*AJAX  FUNCTIONS*/
	function AJAX_getDepartmentsByBusinessUnitID() { 
	
				$assignedRoles 				= $this->assignedRoles;
			 	$accessModules 				= $this->accessModules;
				
				$businessUnit         		= $this->input->post('businessUnit');
				$selectedDepartment         = $this->input->post('selectedDepartment');
				
				$HTML		= NULL;
						
				$result 	= $this->model_organization->getBusinessUnitDepartments($businessUnit);
				     
				$html = '<select id="department" name="department" class="custom-select" onchange="return showFilter(\'department\');">
							<option value="">Select Department</option>';
					            
							if ($result) {
							 
							 	foreach($result->result() as $row) {
										
											  if ($selectedDepartment == $row->departmentID) {
											  
													  $departmentSelected = 'selected = selected';	
											  
											  }	else {
													  
													  $departmentSelected = NULL;		
											  }
						  
											$html .= ' <option value="'.$row->departmentID.'" '.$departmentSelected.'>'.$row->departmentName.' '.fillBrackets($row->departmentCode).'</option>';	 
							 			
									} 
					  }
					  
				 $html .= '</select>';
				 echo $html;
	}
	
	function AJAX_getPositionsByJobTitleID() { 
				
				$assignedRoles 						= $this->assignedRoles;
			 	$accessModules 						= $this->accessModules;
				
				$jobTitleID         				=	$this->input->post('jobTitleID');
				$selectedJobPosition         		=	$this->input->post('selectedJobPosition');
				
				$HTML								= NULL;
							
				$result 							= $this->model_organization->getJobTitlePositions($jobTitleID);
				
				     
				$html = '<select name="job_position" id="job_position" class="custom-select" onchange="return showFilter(\'job-position\');">
							<option value="">Select Job Position</option>';
							
						if ($result) {
						 
							foreach($result->result() as $row) {
										
										 if ($selectedJobPosition == $row->jobPositionID) {
										  
												  $jobPositionSelected = 'selected = selected';	
										  
										  }	else {
												  
												  $jobPositionSelected = NULL;		
										  }
									
										$html .= ' <option value="'.$row->jobPositionID.'" '.$jobPositionSelected.'>'.$row->jobPositionName.' '.fillBrackets($row->jobPositionCode).'</option>';	 
									
								} 
				  }
				  
				 $html .= '</select>';
				 echo $html;
	}
	
}

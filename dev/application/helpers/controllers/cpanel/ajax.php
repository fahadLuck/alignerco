<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 
 
class Ajax extends CI_Controller {

	  public function __construct() {
		
		parent::__construct();
		
		/* Table ID*/
		$this->accountID = $this->session->userdata(USER_ACCOUNT_ID_SESSION);
		
		/*Team Member ID*/
		$this->teamID   = $this->session->userdata(USER_TEAM_ID_SESSION);
		
		/*Team Member ID*/
		$this->HRID   = $this->session->userdata(USER_HR_ID_SESSION);
		
		/*Team Member ID*/
		$this->organizationID	= $this->session->userdata(USER_ORGANIZATION_ID_SESSION);
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');
		
		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper
		
		$this->caseStatusReadyArray     	= array(
													CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY,
													CASE_STATUS_KIT_TO_CUSTOMER_IN_TRANSIT,
													CASE_STATUS_IMPRESSIONS_TO_NY_READY,
													CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_READY,
													CASE_STATUS_READY_FOR_UPLOADING_READY,
													CASE_STATUS_WAITING_FOR_APPROVAL_WAITING,
													CASE_STATUS_PRODUCTION_TO_NY_READY,
													CASE_STATUS_ALIGNERS_TO_CUSTOMER_READY
													);
										
		$this->caseStatusTransitArray    	= array(
													CASE_STATUS_IMPRESSIONS_TO_NY_IN_TRANSIT,
													CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT,
													CASE_STATUS_PRODUCTION_TO_NY_IN_TRANSIT,
													CASE_STATUS_ALIGNERS_TO_CUSTOMER_IN_TRANSIT
													);
		
		$this->caseStatusProcessArray    	= array();
													
		$this->caseStatusHoldArray    		= array();
													
		$this->caseStatusOFFHoldArray    	= array();
		
		$this->caseStatusModificationArray  = array(
													CASE_STATUS_WAITING_FOR_APPROVAL_MODIFICATION
													);
		
		$this->caseStatusDoneArray  	 	= array(
											 		CASE_STATUS_KIT_SHIP_TO_CUSTOMER_SHIPPED,
													CASE_STATUS_KIT_TO_CUSTOMER_RECEIVED,
													CASE_STATUS_IMPRESSIONS_TO_NY_RECEIVED,
													CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_RECEIVED,
													CASE_STATUS_READY_FOR_UPLOADING_DONE,
													CASE_STATUS_WAITING_FOR_APPROVAL_APPROVED,
													CASE_STATUS_PRODUCTION_TO_NY_RECEIVED,
													CASE_STATUS_ALIGNERS_TO_CUSTOMER_RECEIVED
												    );
		
		$this->caseStatusRejectArray  		= array();
	}
	
	
	/*Application Functions */
	function AJAX_getCompaniesByCountry() { 
	
				$country         		=	$this->input->post('country');
				$selectedCompany        =	$this->input->post('selectedCompany');
				
				$HTML		= NULL;
				
				if ($country) {
					
					$result				= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_COMPANIES_TABLE,array('country'=> $country,'publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');
				
				} else {
					
					$result				= false;
				
				}
					 $html = '<select name="company" id="company" class="form-control select2" style="width: 100%;" onchange="return getCaseTypes(this.value);">';
					            
							if ($result) {
							 
							 	foreach($result->result() as $row) {
										
											  if ($row->id == 49 || $row->id == 52 || $row->id == 59 || $row->id == 60) {
											  } else { continue; }
											  
											  if ($selectedCompany == $row->id) {
											  
													  $companySelected = 'selected = selected';	
											  
											  }	else {
													  
													  $companySelected = NULL;		
											  }
						  
											$html .= ' <option value="'.$row->id.'" setupTimelineDays="'.$row->setup_timeline_days.'" '.$companySelected.'>'.$row->name.'</option>';	 
							 			
									} 
					  }
					  
					 $html .= '</select>';
					 echo $html;
	}
	
	/*End Application Functions*/
	
	
	/*New Case Registration */
	function AJAX_getCompanyCaseTypes() { 
	
				$company         		=	$this->input->post('company');
				$selectedCaseType        =	$this->input->post('selectedCaseType');
				
				$HTML		= NULL;
				
				if ($company) {
					
					$result				= $this->model_case->getCompanyCaseTypes($company);
				
				}
					 $html = '<select name="case_type" id="case_type" class="form-control select2" style="width: 100%;">
								<option value="">Choose a case type</option>';
					            
							if ($result) {
							 
							 	foreach($result->result() as $row) {
										
											  if ($selectedCaseType == $row->caseTypeID) {
											  
													  $selected = 'selected = selected';	
											  
											  }	else {
													  
													  $selected = NULL;		
											  }
						  
											$html .= ' <option value="'.$row->caseTypeID.'" '.$selected.'>'.$row->caseTypeName.'</option>';	 
							 			
									} 
					  }
					  
					 $html .= '</select>';
					 echo $html;
	}
	
	/*Cases listings page. To show all cases in different production stages and child production stages with ajax load cases listing*/
	function AJAX_chidProductionStages() {
	
					 $parentID 				= $this->input->post('stageID');
					
					  $checkChilds			= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('parent'=> $parentID,'is_deleted'=> HARD_CODE_ID_NO,'publish_status' => HARD_CODE_ID_PUBLISHED),'order','ASC');
					  $totalChilds  		= $checkChilds->num_rows();
	 				 
					   if ($totalChilds > 0) {
	 
   ?>
                                <div class="row">
                                  
   <?php 
                                              $colorCounter = 1;
                                              foreach($checkChilds->result() as $child) {
                                                  
                                                  $stageID			 = $child->id;
                                                  $stageName 		 = $child->name;
                                                  
                                                  $totalCounterChild = $this->model_case->getStageCases($stageID);
                                                  $totalCounterChild = $totalCounterChild->num_rows();
                                                  
                                                  if ($colorCounter == 1) {
                                                  
                                                      $colorClass = 'bg-green';	
                                              
                                                   } elseif($colorCounter == 2) {
                                                            
                                                            $colorClass = 'bg-purple';
                                                    
                                                   } elseif($colorCounter == 3) {
                                                            
                                                            $colorClass = 'bg-red';	
                                                            
                                                   } elseif($colorCounter == 4) {
                                                            
                                                            $colorClass = 'bg-blue';
                                                   }
                                                    
                                                    if($colorCounter == 4) {
                                                            
                                                            $colorCounter = 1;
                                                   }
          ?>
                                       <div class="col-xl-<?php if ($totalChilds == 1) { echo 4; } elseif ($totalChilds == 3) { echo 4; } else { echo 3;} ?>">
                                          <div class="info-box <?php echo $colorClass; ?>"  id="selected-stage-<?php echo $stageID; ?>">
                                            <span class="info-box-icon"><i class="fa fa-medkit"></i></span>
                                            
                                            <div class="info-box-content">
                                                <span class="info-box-text"><a style="color:#FFF" href="javascript:void(0)" class="search_production_stage" key="<?php echo $stageID; ?>" type="child"><?php echo $stageName; ?></a></span>
                                                <span class="info-box-number">Total : <?php echo leadingZeroBeforeSingleDigitNumber($totalCounterChild); // Calling From General Helper ?></span>
                    
                                                <div class="progress">
                                                   <div class="progress-bar" style="width: 0%"></div>
                                                </div>
                                                <span class="progress-description">
                                                    &nbsp;
                                                </span>
                                             </div>
                                           
                                          </div>
                                         
                                        </div>
                                  
                                 	<?php $colorCounter++; } ?>
                              
                                </div>
   <?php
					   }
	}
	
	
	/*Update case status (Case TimeLine Page) */
	function AJAX_getStageStatus() {
		
					$caseStatusReadyArray 	= $this->caseStatusReadyArray;
					
					$stageID 				= $this->input->post('stageID');
					
					$return['result']		=	FALSE;
					
					$resultData = $this->model_case->getStageStatus($stageID);
					
					$HTML  =  ' <div class="form-group row">
                        		 <label for="example-text-input" class="col-sm-2 col-form-label">Status:</label>
                        			<div class="col-sm-10">';
									
                   $HTML  .=	 '<select name="status" id="status" class="form-control select2 cls-status" onChange="return getStatusChilds(this.value);">';
                      	            
												if ($resultData) {
																
														$HTML .=  '<option value="">Choose a status...</option>';	
																
																foreach($resultData->result() as $data) {
																	
																		if (in_array($data->statusID,$this->caseStatusReadyArray) || in_array($data->statusID,$this->caseStatusProcessArray)) {
																					
																					continue;	
																		}
																		
																		$HTML .=  '<option value="'.$data->statusID.'"> '.$data->statusName.'</option>';	
																}
																
																$return['result']	=	TRUE;
												}
								    
                                    
                            $HTML .= '</select>';
                            
							$HTML .= '    </div>
										</div>';
								  
					$return['HTML'] = $HTML;
					
					echo json_encode($return);
		
	}
	
	/*Update case status (Case TimeLine Page) */
	function AJAX_getStatusChilds() {
		
					$statusID 				= $this->input->post('statusID');
					
					$return['result']		=	FALSE;
					
					$resultData = $this->model_case->getStatusChilds($statusID);
					
					$HTML  =  ' <div class="form-group row">
                        		 <label for="example-text-input" class="col-sm-2 col-form-label">&nbsp;</label>
                        			<div class="col-sm-10">';
									
                   $HTML  .=	 '<select name="status_child" id="status_child" class="form-control select2" onChange="return setCaseStatus(this.value);">';
                      	            
												if ($resultData) {
																
														$HTML .=  '<option value="">Choose a status...</option>';	
																
																foreach($resultData->result() as $data) {
																		
																		$HTML .=  '<option value="'.$data->statusID.'"> '.$data->statusName.'</option>';	
																}
																
																$return['result']	=	TRUE;
												}
								    
                                    
                            $HTML .= '</select>';
                            
							$HTML .= '    </div>
										</div>';
								  
					$return['HTML'] = $HTML;
					
					echo json_encode($return);
		
	}
	
	function AJAX_getStageOperators() {
		
					$stageID 				= $this->input->post('stageID');
					
					$return['result']		=	FALSE;
					
					$resultData = $this->model_case->getStageOperators($stageID);
					
					$HTML  =  ' <div class="form-group row">
                        		 <label for="example-text-input" class="col-sm-2 col-form-label">Operator:</label>
                        			<div class="col-sm-10">';
									
                   $HTML  .=	 '<select name="operator" id="operator" class="form-control select2 cls-operator">';
                      	            
												if ($resultData) {
																
														$HTML .=  '<option value="">Choose a operator...</option>';	
																
																foreach($resultData->result() as $data) {
																		
																		$employeeAssignJobs		=   getEmployeeAssignJobs($data->employeeID); // Calling From HR Employees Helper
										
																		if ($employeeAssignJobs) {
																	  
																				$employeeAssignJob 					= $employeeAssignJobs->row_array();
																				$employeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
															  
														  
																		  } else {
																  
																			  $employeeAssignJobDescription 		= NULL;
																		  }	   		
																	
																		
																		$HTML .=  '<option value="'.$data->employeeID.'"> '.$data->employeeCode.' - '.$data->employeeName.'</option>';	
																}
																
																$return['result']	=	TRUE;
												}
								    
                                    
                            $HTML .= '</select>';
                            
							$HTML .= '    </div>
										</div>';
								  
					$return['HTML'] = $HTML;
					
					echo json_encode($return);
		
	}
	
	/* AJAX Pagination Functions*/
	function AJAX_Pagination_cases($page=0) {
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 			= $this->assignedRoles;
		$accessModules 			= $this->accessModules;
		
		$recordPerPage										= 100;
			
		// Code For Search Case
		$searchParameters 									= array();
		
		$searchCaseID		    							= $this->input->post('case_id');
		$searchPatientName		    						= $this->input->post('patient_name');
		$searchCountry		    							= $this->input->post('country');
		$searchDistributor		    						= $this->input->post('company');
		
		$searchReceivedDate		    						= $this->input->post('search_received_date');
		$searchReceivedDateTo	    						= $this->input->post('search_received_date_to');
		
		
	    $searchStage		    							= $this->input->post('search_stage');

		// Case on (READY / HOLD / OFF-HOLD - PROCESS - MODIFICATION)
		$searchStatus		    							= $this->input->post('search_status');
		
		// Case Complete (Delay / After Time / Before Time)
		$searchTimePeriod	    							= $this->input->post('search_time_period');
		
		$searchParameters['searchCaseID']					= $searchCaseID;
		$searchParameters['searchPatientName']				= $searchPatientName;
		
		$searchParameters['searchCountry']					= $searchCountry;
		
		$searchParameters['searchDistributor']				= $searchDistributor;
		
		$searchParameters['searchReceivedDate']				= $searchReceivedDate;
		$searchParameters['searchReceivedDateTo']			= $searchReceivedDateTo;
		
		$searchParameters['searchStage']					= $searchStage;
		$searchParameters['searchStatus']					= $searchStatus;
		$searchParameters['searchTimePeriod']				= $searchTimePeriod;
		
		$cases	   					 				  	   = $this->model_case->casesListing($page,$recordPerPage,$searchParameters);	
		$totalRows			 						   	   = $this->model_case->casesNum($searchParameters);



        // change by fahad
		$missing_cases	   					 				  	   = $this->model_case->getMissingCasesList($page,$recordPerPage,$searchParameters);	
		$missing_totalRows			 						   	   = $missing_cases->num_rows();


		
		$mypaing['total_rows']			 				   = $totalRows;
		
		$mypaing['base_url'] 							   = base_url()."cases-list/";
		$mypaing['per_page']							   = $recordPerPage;
		$mypaing['uri_segment']							   = 2;	 
				
		$mypaing['full_tag_open'] 						   = '<ul  class="pagination pagination-sm pull-right">';
		$mypaing['full_tag_close'] 						   = '</ul>';

		$mypaing['first_link'] 							   = 'First';
		$mypaing['first_tag_open'] 						   = '<li>';
		$mypaing['first_tag_close'] 					   = '</li>';
		$mypaing['last_link']							   = 'Last';
		$mypaing['last_tag_open'] 						   = '<li>';
		$mypaing['last_tag_close'] 						   = '</li>';
		$mypaing['next_link'] 							   = 'Next';
		$mypaing['next_tag_open'] 						   = '<li>';
		$mypaing['next_tag_close'] 						   = '</li>';

		$mypaing['prev_link'] 							   = 'Previous';
		$mypaing['prev_tag_open'] 						   = '<li>';
		$mypaing['prev_tag_close'] 						   = '</li>';
			
		$mypaing['cur_tag_open'] 						   = '<li><a href="" class="current ajax-pagination">';
		$mypaing['cur_tag_close'] 						   = '</a></li>';
			
		$mypaing['num_tag_open'] 						   = '<li>';
		$mypaing['num_tag_close'] 						   = '</li>';  	
							
		$this->ajax_pagination->initialize($mypaing);
		$paginglink										   = $this->ajax_pagination->create_links();
				
		$serialNumber 									   = $this->uri->segment(2)+1;
		
		$result['assignedRoles']						   = $assignedRoles;
		$result['accessModules']						   = $accessModules;
		
		$result['cases']								   = $cases;
		$result['totalCases']							   = $totalRows;

		// change by fahad
		$result['missing_cases']						   = $missing_cases;
		$result['missing_totalRows']					   = $missing_totalRows;
		
		$result['serialNumber']							   = $serialNumber;
		$result['pagingLink']							   = $paginglink;
		
		if ($searchStage && $searchStage == IMPRESSIONS_TO_PRODUCTION_SEND_RECEIVE) {
			
			$deliveryServices							   = $this->model_shared->getRecordMultipleWhereOrderBy('*',DELIVERY_SERVICES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');
			$result['deliveryServices']            		   = $deliveryServices;
			
			$this->load->view('cpanel/cases/ajax/ajax_data_send_to_production_cases',$result);
		
		}

		elseif($searchStage && $searchStage == WAITING_FOR_APPROVAL) {

			$recordPerPage					= 1000; 
			$organizationID					= $this->organizationID;
			$searchParameters 			    = 0;
			$page=0;

			$employees = $this->model_employee->getActiveDoctors($page,$recordPerPage,$organizationID,$searchParameters);

			$result['employees']            		   = $employees;
				
			$this->load->view('cpanel/cases/ajax/ajax_data_waiting_for_approvel_cases',$result);
		
		}

		 else 
		 {
			
			$this->load->view('cpanel/cases/ajax/ajax_data_cases',$result);
		 }
			
		
	
	}



	/* AJAX Pagination Functions*/
	function AJAX_Pagination_doctor_cases($page=0) {
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 			= $this->assignedRoles;
		$accessModules 			= $this->accessModules;
		$teamID					= $this->teamID;
		
		$recordPerPage										= 100;
			
		// Code For Search Case
		$searchParameters 									= array();
		
		$searchCaseID		    							= $this->input->post('case_id');
		$searchPatientName		    						= $this->input->post('patient_name');
		$searchCountry		    							= $this->input->post('country');
		$searchDistributor		    						= $this->input->post('company');
		
		$searchReceivedDate		    						= $this->input->post('search_received_date');
		$searchReceivedDateTo	    						= $this->input->post('search_received_date_to');
		
		
	    $searchStage		    							= $this->input->post('search_stage');

		// Case on (READY / HOLD / OFF-HOLD - PROCESS - MODIFICATION)
		$searchStatus		    							= $this->input->post('search_status');
		
		// Case Complete (Delay / After Time / Before Time)
		$searchTimePeriod	    							= $this->input->post('search_time_period');
		
		$searchParameters['searchCaseID']					= $searchCaseID;
		$searchParameters['searchPatientName']				= $searchPatientName;
		
		$searchParameters['searchCountry']					= $searchCountry;
		
		$searchParameters['searchDistributor']				= $searchDistributor;
		
		$searchParameters['searchReceivedDate']				= $searchReceivedDate;
		$searchParameters['searchReceivedDateTo']			= $searchReceivedDateTo;
		
		$searchParameters['searchStage']					= $searchStage;
		$searchParameters['searchStatus']					= $searchStatus;
		$searchParameters['searchTimePeriod']				= $searchTimePeriod;
		
		$cases	   					 				  	   = $this->model_case->doctorCasesListing($page,$recordPerPage,$searchParameters);	
		$totalRows			 						   	   = $this->model_case->casesNum($searchParameters);



        // change by fahad
		$missing_cases	   					 				  	   = $this->model_case->getMissingCasesList($page,$recordPerPage,$searchParameters);	
		$missing_totalRows			 						   	   = $missing_cases->num_rows();


		
		$mypaing['total_rows']			 				   = $totalRows;
		
		$mypaing['base_url'] 							   = base_url()."cases-list/";
		$mypaing['per_page']							   = $recordPerPage;
		$mypaing['uri_segment']							   = 2;	 
				
		$mypaing['full_tag_open'] 						   = '<ul  class="pagination pagination-sm pull-right">';
		$mypaing['full_tag_close'] 						   = '</ul>';

		$mypaing['first_link'] 							   = 'First';
		$mypaing['first_tag_open'] 						   = '<li>';
		$mypaing['first_tag_close'] 					   = '</li>';
		$mypaing['last_link']							   = 'Last';
		$mypaing['last_tag_open'] 						   = '<li>';
		$mypaing['last_tag_close'] 						   = '</li>';
		$mypaing['next_link'] 							   = 'Next';
		$mypaing['next_tag_open'] 						   = '<li>';
		$mypaing['next_tag_close'] 						   = '</li>';

		$mypaing['prev_link'] 							   = 'Previous';
		$mypaing['prev_tag_open'] 						   = '<li>';
		$mypaing['prev_tag_close'] 						   = '</li>';
			
		$mypaing['cur_tag_open'] 						   = '<li><a href="" class="current ajax-pagination">';
		$mypaing['cur_tag_close'] 						   = '</a></li>';
			
		$mypaing['num_tag_open'] 						   = '<li>';
		$mypaing['num_tag_close'] 						   = '</li>';  	
							
		$this->ajax_pagination->initialize($mypaing);
		$paginglink										   = $this->ajax_pagination->create_links();
				
		$serialNumber 									   = $this->uri->segment(2)+1;
		
		$result['assignedRoles']						   = $assignedRoles;
		$result['accessModules']						   = $accessModules;
		
		$result['cases']								   = $cases;
		$result['totalCases']							   = $totalRows;
		$result['teamID']							       = $teamID;
		$result['searchStage']							   = $searchStage;

		// change by fahad
		$result['missing_cases']						   = $missing_cases;
		$result['missing_totalRows']					   = $missing_totalRows;
		
		$result['serialNumber']							   = $serialNumber;
		$result['pagingLink']							   = $paginglink;
		
		/*if ($searchStage && $searchStage == IMPRESSIONS_TO_PRODUCTION_SEND_RECEIVE) {
			
			$deliveryServices							   = $this->model_shared->getRecordMultipleWhereOrderBy('*',DELIVERY_SERVICES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');
			$result['deliveryServices']            		   = $deliveryServices;
			
			$this->load->view('cpanel/cases/ajax/ajax_data_send_to_production_cases',$result);
		
		}

		elseif($searchStage && $searchStage == WAITING_FOR_APPROVAL) {

			$recordPerPage					= 1000; 
			$organizationID					= $this->organizationID;
			$searchParameters 			    = 0;
			$page=0;

			$employees = $this->model_employee->getActiveDoctors($page,$recordPerPage,$organizationID,$searchParameters);

			$result['employees']            		   = $employees;
				
			$this->load->view('cpanel/cases/ajax/ajax_data_waiting_for_approvel_cases',$result);
		
		}

		 else 
		 {*/
			
			
		 /*}*/

		 if($searchStage=='' OR $searchStage==ASSIGNED)
		 {
		 	$this->load->view('cpanel/cases/ajax/ajax_data_doctor_cases',$result);
		 }
		 else
		 {
		 	$this->load->view('cpanel/cases/ajax/ajax_data_doctor_stage_cases',$result);
		 }
			
		
	
	}



	function getDoctors()
	{
		error_reporting(0);
		$doctorID=$this->input->post('doctorID');
		$organizationID					= $this->organizationID;

		$employees = $this->model_employee->getSingalActiveEmployees($organizationID,$doctorID);
        $doctorName  = $employees->employeeName;
            
		$cases = $this->model_case->getDoctorCases($doctorID);
		$counter=$cases->num_rows();

		$pendingCases=0;
		$approvedCases=0;
		$modifiedCases=0;	
		foreach($cases->result() as $dc)
		{
			$doctorName=$dc->employName;
			$caseStatusID=$dc->caseStatusID;
			if($caseStatusID==PENDING)
			{
				$pendingCases++;
			}

			if($caseStatusID==APPROVED)
			{
				$approvedCases++;
			}

			if($caseStatusID==MODIFIED)
			{
				$modifiedCases++;
			}
		}

		if($doctorID !='')
		{	
		?>

		<div class="box box-widget widget-user-4">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active"   style="background: url('../images/photo1.png') center center;">
               <div class="overlay dark">
            <!-- <h3 class="widget-user-username">James Anderson</h3> -->
            <h5 class="widget-user-desc"><?= $doctorName ?> Statics</h5>
               </div>
            </div>
           
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><span style="color: blue;"><?= $counter ?></span></h5>
                    <span class="description-text">Assigned Cases</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><span style="color: red;"><?= $pendingCases ?></span></h5>
                    <span class="description-text">Pending Cases</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3">
                  <div class="description-block">
                    <h5 class="description-header"><span style="color: green;"><?= $approvedCases ?></span></h5>
                    <span class="description-text">Approved Cases</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->

                <!-- /.col -->
                <div class="col-sm-3">
                  <div class="description-block">
                    <h5 class="description-header"><span style="color: orange;"><?= $modifiedCases ?></span></h5>
                    <span class="description-text">Modified Cases</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->

              </div>
              <!-- /.row -->
            </div>
          </div>

		<?php
	}

}



function showStates()
{
	$tableID=decodeString($this->input->post('tableID'));
	$assignedStates	   					 				    =  $this->model_shared->getRecordMultipleWhere('state_id,licience,id',ASSIGNED_STATES_TABLE,array('team_id' => $tableID));

	    	?>
	    	<table class="table">
			  <thead>
			    <tr>
			      <th scope="col">State</th>
			      <th scope="col">Licience</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php 
			  	foreach($assignedStates->result() as $as)
				{
					$state_id=$as->state_id;
					$licience=$as->licience;

					$States	  =  $this->model_shared->getRecordMultipleWhere('name',STATES_TABLE,array('id' => $state_id)); 
			  	foreach($States->result() as $st)
			    {
			    	$name=$st->name;
			    ?>	
			    <tr>
			      <td><?= $name; ?></td>
			      <td><?= $licience; ?></td>
			    </tr>
			   <?php } } ?>
		    </tbody>
			</table>
			<?php
	    
	
}




}
?>	
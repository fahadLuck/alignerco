<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/** STARTED ON 1 MAY 2016 Saturday BY WAQAS ALI
	*/
	 
	public function __construct() {
		
		 parent::__construct();
		
		/* Table ID*/
		$this->accountID = $this->session->userdata(USER_ACCOUNT_ID_SESSION);
		
		/*Team Member ID*/
		$this->teamID   = $this->session->userdata(USER_TEAM_ID_SESSION);
		
		/*Team Member ID*/
		$this->HRID   = $this->session->userdata(USER_HR_ID_SESSION);
		
		/*Team Member ID*/
		$this->organizationID	= $this->session->userdata(USER_ORGANIZATION_ID_SESSION);
		
		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');
		
		authentication(); // Calling From Login Helper 
		
		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper
		
	}
	
	function index() { 
		
		$this->checkLoggedinUserRole();
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 					= $this->assignedRoles;
		$accessModules 					= $this->accessModules;
		
		// Super Administrator Dashboard Code
		if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles)) {
			
				$this->administratorDashboard();
		
		// Manager Dashboard Code
		} elseif (in_array(ROLE_MANAGER,$assignedRoles)) {
			
				$this->managerDashboard();
		} elseif (in_array(ROLE_OPERATOR,$assignedRoles)) {
			
				$this->managerDashboard();
		}

		elseif (in_array(ROLE_DOCTOR,$assignedRoles)) {
			
				$this->managerDashboard();
		}
	}
	
	function administratorDashboard() { 
		
		$accountID						= $this->accountID;
		$teamID							= $this->teamID;
		$organizationID 				= $this->organizationID;
		
		$userInfo 						= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 										= $this->assignedRoles;
		$accessModules 										= $this->accessModules;
		
		$result												= array();
			
		$result['assignedRoles']							= $assignedRoles;
		$result['accessModules']							= $accessModules;
		
		$data['userInfo']            						= $userInfo;
		
		$data['activeMenu']  								= '1';
		$data['activeSubMenu']  							= '1.1';
	
		$data['pageHeading']    							= "Dashboard";
		$data['subHeading']    								= "Control Panel";
	
		$data['metaType']     								= 'internal';
		$data['pageName']    								= 'My Dashboard';
		$data['pageTitle']      							= 'My Dashboard | '.DEFAULT_APPLICATION_NAME;
		
		$data['contents']  = $this->load->view('cpanel/administrator/dashboard',$result,true);
		$this->load->view('cpanel/template',$data);
		
	}
	
	function managerDashboard() { 
		
		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;
		
		$userInfo 				= userInfo($accountID); // Calling From Application Helper
		
		/* User Roles And Permission
		
			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/
		
		$assignedRoles 										= $this->assignedRoles;
		$accessModules 										= $this->accessModules;
		
		$result												= array();
			
		$result['assignedRoles']							= $assignedRoles;
		$result['accessModules']							= $accessModules;
		
		$data['userInfo']            						= $userInfo;
		
		$data['activeMenu']  								= '1';
		$data['activeSubMenu']  							= '1.1';
	
		$data['pageHeading']    							= "Dashboard";
		$data['subHeading']    								= "Control Panel";
	
		$data['metaType']     								= 'internal';
		$data['pageName']    								= 'My Dashboard';
		$data['pageTitle']      							= 'My Dashboard | '.DEFAULT_APPLICATION_NAME;
		
		$data['contents']  = $this->load->view('cpanel/administrator/dashboard',$result,true);
		$this->load->view('cpanel/template',$data);
		
	}
	
	function checkLoggedinUserRole() {
		
		$accountID		= $this->accountID;
		$teamID			= $this->teamID;
		
		if ($teamID) {
		
			$dbRecord 		= $this->model_shared->getRecordMultipleWhere('id',MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE,array('team_id' => $teamID,'is_deleted' => HARD_CODE_ID_NO));
			
				if ($dbRecord->num_rows() > 0) {
				
					return true;
				
				} else 	{
					
					$data['team_id']							=	$teamID;
					$data['role_id']							=	ROLE_OPERATOR;
					
					$data['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT(); 
					$data['created_by']							=   $teamID;
					$data['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';
					
					// Save All data Into Database Table
					$insertedID								= 	$this->model_shared->insertRecord_ReturnID(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE,$data);
				}
				
				redirect('my-dashboard');
		
		} else {
			
			return true;	
		}
	}
}

<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 
	
	function productionStages() {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('parent' => 0,'is_deleted' => HARD_CODE_ID_NO),'order','ASC');	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }

    function doctorCaseStatus() {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DOCTOR_CASE_STATUS_TABLE,array('parent' => 0,'is_deleted' => HARD_CODE_ID_NO),'order','ASC');	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function productionStageCases($productionStageID) {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_case->getStageCases($productionStageID);;	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function casePresentStage($caseID) {
	 		
			$CIH =  &get_instance();
			
			$result  =  $CIH->model_case->getCasePresentStage($caseID);	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function casePresentStatus($caseID) {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_case->getCasePresentStatus($caseID);	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function casePresentOperator($caseID) {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_case->getCasePresentOperator($caseID);	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function caseChangeStatusHistory($caseStageID,$caseID) {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_case->getCaseChangeStatusHistory($caseStageID,$caseID);	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function caseChangeOperatorHistory($caseID,$caseStageID,$caseStatusID) {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_case->getCaseChangeOperatorHistory($caseID,$caseStageID,$caseStatusID);	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function getCaseOperatorCreatedBy($teamID) {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_case->getCaseOperatorCreatedBy($teamID);	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function statusInfoByID($statusID) {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_case->statusInfoByID($statusID);	
			
			if ($result->num_rows() > 0) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	function statusName($statusID) {
	 		
			$CIH     =  &get_instance();
			
			$result  =  $CIH->model_case->statusName($statusID);	
			
			if ($result->num_rows() > 0) {
			 
			 $result = $result->row_array();
			 
			 return $result['statusName']; 
			 
		 } else {
			
			 return false;	
		}
    }
	
?>
<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

	function getModules() {
	 
		$CIH      =   & get_instance();
		$return  =   array();
		
		$result  =   $CIH->model_users->getModules();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 	}
	
	function getRoles($organizationID) {
	 
		$CIH      =   & get_instance();
		$return  =   array();
		
		$result  =   $CIH->model_users->getRoles($organizationID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }
 
 	function getOfficialEmployees($organizationID,$searchParms) {
	 
		$CIH     =   & get_instance();
	
		$return  =   array();
		
		$result  =   $CIH->model_users->getOfficialEmployees($organizationID,$searchParms);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
	}
	
	function getEmployeeAssignedRoles($organizationID,$employeeID) {
	 
		$CIH     =   & get_instance();
	
		$return  =   array();
		
		$result  =   $CIH->model_users->getEmployeeAssignedRoles($organizationID,$employeeID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
	}
	
	function getEmployeesForUsers($organizationID) {
	 
		$CIH     =   & get_instance();
	
		$return  =   array();
		
		$result  =   $CIH->model_users->getEmployeesForUsers($organizationID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
	}
	
	function getRoleInfoByID($organizationID,$roleID) {
	 
		$CIH     =   & get_instance();
	
		$return  =   array();
		
		$result  =   $CIH->model_users->getRoleInfoByID($organizationID,$roleID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
	}

?>
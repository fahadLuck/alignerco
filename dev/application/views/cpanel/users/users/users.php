<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Users</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           
           <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
           
           
          <div class="box">
            <div class="box-header">
              
              <?php 
			  		
					$totalUsers = 0;
					
					if ($users) {
						
						$totalUsers = $users->num_rows();	
					}
			  ?>
            
              <h3 class="box-title">Users <small>(<?php echo $totalUsers; ?>)</small></h3>
			    <button onclick="window.location.href='<?php echo base_url(); ?>user-add/'"type="button" class="btn btn-blue pull-right"><i class="fa fa-plus"></i> Create User</button>
                <div class="box-tools">
                
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="employee" class="table table-responsive" style="width:100%">
              	<thead>
                <tr>
                  <th width="32">Sr.</th>
                  <th width="118">Code</th>
                  <th width="122">Name</th>
                  <th width="144">Official Email</th>
                  <th width="144">UserName</th>
                  <th width="123">Password</th>
                  <th width="131">Role</th>
                  <th width="159">Moblie</th>
                  <th width="83">Status</th>
                  <th width="108">&nbsp;</th>
                </tr>
            </thead>
               <tbody>
               
               <?php if ($users) {
                        
						$counter = 1;
						foreach ($users->result() as $user) {
							
							$tableID 								=   encodeString($user->tableID);
							$username   							=   $user->username;
							$employeeID   							=   $user->employeeID;
							$employeeCode   						=   $user->employeeCode;
							$employeePrefix   						=   $user->prefixName;
							$employeeName   						=   $user->employeeName;
							$employeeMobileFormatted   				=   $user->employeeMobileFormatted;
							$employeePhoto   						=   $user->employeePhoto;
							$officialAccountEmail   				=   $user->officialAccountEmail;
							$officialAccountMobile   				=   $user->officialAccountMobile;
							$officialAccountPassword   				=   decodeString($user->officialAccountPasswordVisible);  // Calling From General Helper
							$officialAccountStatus   				=   getStatusName($user->officialAccountStatus); // Calling From Shared Helper
							
							$assignRoleID   						=   $user->roleID; 
									
							$employeeAssignJobs					 	=   getEmployeeAssignJobs($employeeID); // Calling From Employee Helper
							
							$assignRole					 			=   getEmployeeAssignedRoles($organizationID,$employeeID); // Calling From User Helper
							
							if ($employeeAssignJobs) {
					  
								 $employeeAssignJob 				=   $employeeAssignJobs->row_array();
								 $employeeAssignDepartment			=   $employeeAssignJob['departmentName'];
								 $employeeAssignJobPosition			=   $employeeAssignJob['jobPositionName'];
								
							 } else {
				  
								 $employeeAssignDepartment 			= 	'No department assign';
								 $employeeAssignJobPosition			= 	'No position assign';
							 }
							 
							 
							 if ($assignRole) {
								
								 $employeeAssignRoleName	= NULL;
					  
								 foreach($assignRole->result() as $roleAssign) {
										
										$employeeAssignRoleName	  .=   $roleAssign->roleName;
										$employeeAssignRoleName	 .=  ', ';
								 }
								 
								 $employeeAssignRoleName = rtrim($employeeAssignRoleName,', ');
								
							 } else {
				  
								  $employeeAssignRoleName  = 'No role assign';
							 }
                            ?>
                                    
                                    <tr>
                                    <td><?php echo $counter; ?></td>
                                    <td><?php echo $employeeCode; ?></td>
                                    <td><?php echo $employeeName; ?></td>
                                    <td><?php echo $officialAccountEmail; ?></td>
                                     <td><?php echo $username; ?></td>
                                    <?php
	                                    if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles)) 
	                                    {
											?><td><?php echo $officialAccountPassword; ?></td><?php
										} 
										else
										{
											?><?php ?><td><?php echo '••••••'; ?></td><?php ?><?php
										}
                                    ?>
                                    
                                    <td><?php echo $employeeAssignRoleName; ?></td>
                                    <td><?php echo $employeeMobileFormatted; ?></td>
                                    <td><?php echo $officialAccountStatus; ?></td>
                                    <td><div class="pull-right">
                                    		<a href="<?php echo base_url(); ?>user-edit/<?php echo $tableID; ?>/" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
                                   			<a href="<?php echo base_url(); ?>user-remove/<?php echo $tableID;?>/" onclick="return confirm('Are you sure you want to remove the <?php echo $employeePrefix.' '.$employeeName; ?> (<?php echo $employeeCode; ?>) user?');" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                    	</div>
                                    </td>
                                </tr>
                                    
                    
                    <?php $counter++; } } ?>
               
               
               </tbody>
               <!-- <tfoot>
                      <tr>
                        <th width="32">Sr.</th>
		                  <th width="118">Code</th>
		                  <th width="122">Name</th>
		                  <th width="144">Official Email</th>
		                  <th width="123">Password</th>
		                  <th width="131">Role</th>
		                  <th width="159">Moblie</th>
                      </tr>
                  </tfoot> -->
              </table>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function() {
    $('#employee').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
</script>
   
   
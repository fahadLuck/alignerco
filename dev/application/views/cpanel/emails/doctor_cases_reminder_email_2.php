 <html>
<head>
<style type="text/css">
/* Basics */
        body {Margin: 0;padding: 0; min-width: 100%;background-color: #e2e2e2;}
        table {border-spacing: 0;font-family: sans-serif;color: #333333;}
        td {padding: 0;}
        img {border: 0;}
        .wrapper {width: 100%;table-layout: fixed;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;}
        .webkit {max-width: 600px;}
        .outer {Margin: 0 auto; width: 100%;max-width: 600px;}
        .inner {padding: 10px;}
        a { color: #ee6a56; text-decoration: underline;}
        .h1 {font-size: 21px;font-weight: bold; Margin-bottom: 18px;}
        .h2 {font-size: 18px;font-weight: bold; Margin-bottom: 12px;}
        .full-width-image img {width: 100%; max-width: 600px;height: auto;}
        /* One column layout */
        .one-column .contents { text-align: left;}
        .one-column p { font-size: 14px;Margin-bottom: 10px;}
        
        /*Media Queries*/
        @media screen and (max-width: 400px) {
            .two-column .column,
            .three-column .column {
                max-width: 100% !important;
            }
            .two-column img {
                max-width: 100% !important;
            }
            .three-column img {
                max-width: 50% !important;
            }
        }
        
        @media screen and (min-width: 401px) and (max-width: 620px) {
            .three-column .column {
                max-width: 33% !important;
            }
            .two-column .column {
                max-width: 50% !important;
            }
        }

</style>
</head>
<body>
        <center class="wrapper">
            <div class="webkit">
                <!--[if (gte mso 9)|(IE)]>
                <table width="600" align="center">
                <tr>
                <td>
                <![endif]-->
                <table class="outer" align="center" >
                    <!-- <tr>
                        <td class="full-width-image">
                            <img src="<?= base_url() ?>uploads/store_item/<?= $item_pic ?>" width="600" height="450" class="img img-responsive" alt="Gaadiexpert ogo" />
                        </td>
                    </tr> -->
                    <tr>
                        <td class="one-column">
                            <table width="100%">
                                <tr>
                                    <td class="inner contents" bgcolor="ffffff">

<p class="h1">Hi Dr.<?= $doctorName ?>,<br>

We hope you are doing great. Its a gentle reminder to review the case from <?= $companyName ?> that was assigned to you over a day ago. Please login to your provider portal to review it now so we can start aligners fabrication, click below to login:<br>


    <!-- <?php
        $case             = ($this->input->post('list_case') ?: NULL);
        $number_of_cases  = count(($this->input->post('list_case') ?: NULL));                           
        foreach($doctorCases->result() as $cases_row)
        {
            $case_id                 = $cases_row->case_id;
            ?> Case ID : <?= $case_id ?> <?php
            
        } 
    ?> -->

    <?= $FullDomainName ?><br>
    Should you require any assistance, please feel free to call or email us.<br>
    

Regards,<br>
Ele <br>
Smile Crew Member <br>
<?= $companyName ?> <br>
Tel: (877) 840-1561 <br>
</p>

                                       <!--  <p class="h1"> Case ID : </p>
                                        <p> <?php echo $case_id; ?></p>

                                        <p class="h1">Link : </p>
                                        <p> <?php echo $link; ?></p>

                                        <p class="h1">Code : </p>
                                        <p> <?php echo $code; ?></p> -->

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="one-column" bgcolor="#f2f2f2">
                            <table width="100%">
                                <tr>
                                    <td class="inner contents">
                                       
                                        <p>Copyright &copy;2021 | <?= $companyName ?> |  All Rights Reserved.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                   
                </table>
            </div>
        </center>
    </body>
</html>
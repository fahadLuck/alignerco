 <html>
<head>
<style type="text/css">
/* Basics */
        body {Margin: 0;padding: 0; min-width: 100%;background-color: #e2e2e2;}
        table {border-spacing: 0;font-family: sans-serif;color: #333333;}
        td {padding: 0;}
        img {border: 0;}
        .wrapper {width: 100%;table-layout: fixed;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;}
        .webkit {max-width: 600px;}
        .outer {Margin: 0 auto; width: 100%;max-width: 600px;}
        .inner {padding: 10px;}
        a { color: #ee6a56; text-decoration: underline;}
        .h1 {font-size: 21px;font-weight: bold; Margin-bottom: 18px;}
        .h2 {font-size: 18px;font-weight: bold; Margin-bottom: 12px;}
        .full-width-image img {width: 100%; max-width: 600px;height: auto;}
        /* One column layout */
        .one-column .contents { text-align: left;}
        .one-column p { font-size: 14px;Margin-bottom: 10px;}
        
        /*Media Queries*/
        @media screen and (max-width: 400px) {
            .two-column .column,
            .three-column .column {
                max-width: 100% !important;
            }
            .two-column img {
                max-width: 100% !important;
            }
            .three-column img {
                max-width: 50% !important;
            }
        }
        
        @media screen and (min-width: 401px) and (max-width: 620px) {
            .three-column .column {
                max-width: 33% !important;
            }
            .two-column .column {
                max-width: 50% !important;
            }
        }

</style>
</head>
<body>
        <center class="wrapper">
            <div class="webkit">
                <!--[if (gte mso 9)|(IE)]>
                <table width="600" align="center">
                <tr>
                <td>
                <![endif]-->
                <table class="outer" align="center" >
                    <!-- <tr>
                        <td class="full-width-image">
                            <img src="<?= base_url() ?>uploads/store_item/<?= $item_pic ?>" width="600" height="450" class="img img-responsive" alt="Gaadiexpert ogo" />
                        </td>
                    </tr> -->
                    <tr>
                        <td class="one-column">
                            <table width="100%">
                                <tr>
                                    <td class="inner contents" bgcolor="ffffff">

<p class="h1">Hi <?= $patient ?>,<br>

This is Ele with <?= $companyName ?>, a smile crew member assigned to your case. Its a gentle reminder to create an account with us so you can upload your teeth pictures and receive your smile projection on time. Please be informed, we will not be able to create a smile projection if we do not have your teeth pictures.<br> 
In case you missed it earlier, below is the link to create an account:
<br>

<?= $FullDomainName ?>/patient <br>

While creating an account please enter information as per your order with <?= $companyName ?>. Should you require any assistance, please feel free to call us.<br>

Your support is greatly appreciated in this matter.<br>

Regards,<br>
Ele <br>
Smile Crew Member <br>
<?= $companyName ?> <br>
Tel: (877) 840-1561 <br>

</p>

                                       <!--  <p class="h1"> Case ID : </p>
                                        <p> <?php echo $case_id; ?></p>

                                        <p class="h1">Link : </p>
                                        <p> <?php echo $link; ?></p>

                                        <p class="h1">Code : </p>
                                        <p> <?php echo $code; ?></p> -->

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="one-column" bgcolor="#f2f2f2">
                            <table width="100%">
                                <tr>
                                    <td class="inner contents">
                                       
                                        <p>Copyright &copy;2021 | <?= $companyName ?> |  All Rights Reserved.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                   
                </table>
            </div>
        </center>
    </body>
</html>
<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>

<style>
    @media screen and (min-width: 676px) {
        .modal-dialog {
          max-width: 1100px; /* New width for default modal */
        }
    }
</style>


	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Cases</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           

           
           
          <div class="box">
            <div class="box-header">
              
            
              <h3 class="box-title">Shipments<small>(<?php echo $counter; ?>)</small></h3>


                <div class="box-tools">
                  
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="table-responsive">
              <table id="example" data-page-length='100' class="display" style="width:100%">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Shipment Number</th>
                  <th>Delievery Services</th>
                  <th>No Of Cases </th>
                  <th>Shipping Date </th>
                  <!-- <th>Shipping Time Ago</th> -->
                  <th>Shipping Operator </th>
                  <th>Action</th>
                  <!-- <th width="305">Official Email</th>
                  <th width="179">Mobile</th>
                  <th width="90">Action</th> -->
                </tr>
                <thead>
               <tbody>
                  <?php
                  $count=1;
                  foreach($shippedCases->result() as $sc)
                    {
                      $counter                         = $count++;
                      $sent_date                       = $sc->sent_date;
                      $number_of_cases                 = $sc->number_of_cases;
                      $delivery_service                = $sc->delivery_service;
                      $airway_bill_number              = $sc->airway_bill_number;
                      $airway_bill_number_formatted    = $sc->airway_bill_number_formatted;
                      $deliveryServiceSendName         = $sc->deliveryServiceSendName;
                      $created                         = $sc->created;
                      $created_by                      = $sc->created_by;
                      $employeeName                    = $sc->employeeName;
                      $employeeCode                    = $sc->employeeCode;
                      $tableID                         = $sc->tableID;

                      $createdDate                     = date('d M. Y',$created);
                      $createdTime                     = date('h:i A',$created);
                      $createdTimeAgo                  = timeAgo($created).' ago';

                      $shippingCreatorAssignJobs       = getEmployeeAssignJobs($created_by); // Calling From HR Employees Helper
                    
                      if ($shippingCreatorAssignJobs) 
                      {
                    
                          $shippingCreatorAssignJobs = $shippingCreatorAssignJobs->row_array();
                          $shippingCreatorAssignJobs = $shippingCreatorAssignJobs['jobPositionName'];
                
                      } 

                      else 
                      {
                          $shippingCreatorAssignJobs = NULL;
                      } 


                      /*$deleveryService              = $this->model_shared->getRecordMultipleWhere('name',DELIVERY_SERVICES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $delivery_service))->row_array();*/
                    ?>      
                  <tr>
                    <td><?php echo $counter; ?></td>
                    <td><?php echo $airway_bill_number; ?></td>
                    <td><?php echo $deliveryServiceSendName; ?></td>
                    <td><?php echo $number_of_cases; ?></td>
                    <td><?php echo $createdDate; ?> <?php echo $createdTime; ?> (<?php echo $createdTimeAgo; ?>)</td>
                    <!-- <td><?php echo $createdTimeAgo; ?></td> -->
                    <td><?php echo $employeeName; ?>(<?php echo $employeeCode; ?>)</td>
                    <td style="text-align: center">
                      <button style="border-radius: 50%; outline: none;" type="button" name="tableID" id="view_cases" value="<?= $tableID ?>" class="view_cases btn btn-link"><i style="font-weight: bold; font-size: 25px; color: #E0A800" class="fa fa-folder" aria-hidden="true" ></i></button>  
                    </td>
                  </tr>
              <?php } ?>
                              
               
               </tbody>
                   <!-- <tfoot>
                      <tr>
                        <th>Sr.</th>
                        <th>Shipment Number</th>
                        <th>Delievery Services</th>
                        <th>No Of Cases Missing</th>
                      </tr>
                  </tfoot> -->
              </table>
            </div>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>

<!-- <div class="modal fade bs-example-modal-lg" id="modificationModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header"> -->

    <!-- Modal -->
  <div class="modal fade bs-example-modal-lg" id="modificationModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Shipped Cases Details</h4>
              </div>
              <div class="modal-body" id="modificationDetails">
              </div>
              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div> -->
            </div>
        </div>
      </div>
   <!-- Modal -->



<script>
$(document).ready(function(){

    $('.view_cases').click(function(){

             var table_ID=$(this).val();
             
             $.ajax({
                     url:"<?php echo base_url() ?>shipped-cases-details",
                     method:"post",
                     data:{table_ID :table_ID},
                    success: function(data)
                    {
                      $('#modificationDetails').html(data);
                      $('#modificationModal').modal("show");
                    },
                     error: function(xhr, status, error) 
                     {
                         alert("this is error "+error);
                      },
                });

         });

        
    });  
</script>


<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
   
   
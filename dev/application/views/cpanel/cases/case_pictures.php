
<link href="<?php echo base_url(); ?>backend_css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>backend_js/dropzone.js"></script>

<script>

function removePhotos() {
	
	if (confirm('Are sure want to remove case pictures?')) {
    	
		var URL = '<?php echo base_url(); ?>remove-case-pictures/<?php echo encodeString($ID); ?>';
		
		window.location.href = URL;
		
	} else {
		
		return false;
	}
	
}

</script>

<!-- fancybox -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend_assets/vendor_components/lightbox-master/dist/ekko-lightbox.css" />

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Manage Pictures</a></li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     	
        				<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
    <?php 
			 	if ($caseID) {
						
						$caseID = encodeString($caseID);	
				}
	 ?>
     
     <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-cloud-upload"></i> Upload Intra &amp; Extra Oral Pictures</h3>
          	<div class="pull-right box-tools">
              <a href="<?php echo base_url(); ?>manage-case-pictures/<?php echo encodeString($ID); ?>"><i class="fa fa-refresh"></i></a>
            </div>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
             <form action="<?php echo base_url(); ?>upload-case-pictures/<?php echo encodeString($ID); ?>/" class="dropzone"></form>
	        </div>
          </div>
        </div>
 	  </div>
      
      <?php if ($photos->num_rows() > 0) {  ?>
      
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-camera"></i> Intra &amp; Extra Oral Pictures</h3>
          <div class="pull-right box-tools">
            	<a href="javascript:void(0);" onClick="return removePhotos();"><i class="fa fa-trash"></i></a>
             </div>
      </div>

      <div class="timeline-body">
		                  
         <?php foreach($photos->result() as $photo) {
          $pic_type=$photo->type; 
          $public_url=$photo->public_url;
         	?>
         	
         	<?php

         	    if($public_url!='')
         	    {
         	    	if($pic_type=='pdf')
		         	{
		         		?><a href="<?= $public_url ?>" download="file">
						  <img src="https://cdn3.iconfinder.com/data/icons/brands-applications/512/File-512.png" alt="file" style="height: 100px;" width="104" height="142">
						</a><?php
		         	} 
		         	else
		         	{
		         		?><a href="<?= $public_url ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo $row['patient']; ?>"><img  style="padding-top:10px; padding-left:5px height: 100px; width: 100px;" src="<?= $public_url ?>" alt="gallery" ></a><?php
		         	}
         	    }
         	    
         	    else
         	    {
         	    	if($pic_type=='pdf')
		         	{
		         		?><a href="<?= base_url() ?>pictures_cases/<?php echo $photo->name; ?>" download="file">
						  <img src="https://cdn3.iconfinder.com/data/icons/brands-applications/512/File-512.png" alt="file" style="height: 100px;" width="104" height="142">
						</a><?php
		         	} 
		         	else
		         	{
		         		?><a href="<?php echo CASE_PICTURES_URL_PATH ?><?php echo $photo->name; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo $row['patient']; ?>"><img  style="padding-top:10px; padding-left:5px;" src="<?php echo CASE_PICTURES_URL_PATH ?>thumbnail/thumbnail_<?php echo $photo->name; ?>" alt="gallery" ></a><?php
		         	}
         	    }
	         	
         	?>
                 
         <?php } ?>
		
        </div>
          <br />
       <br />
 	  </div>
      
      <?php } ?>
   
    </section>
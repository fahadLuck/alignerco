<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>
<script language="javascript" type="text/javascript">
		
		 $(document).ready(function () {
				  
				  $("input[name='impression'").change(function() {
							
							var impression = $(this).val();
							
							$('input[name="impression_particulars"]').prop('checked', false); 
							
							if (impression == 1) {
								
								$('#impression_particulars-area').show();	
							
							} else {
								
								$('#impression_particulars-area').hide();		
							}
							
							return false;
							
				  });
				  
				  $('#country').change(function() {
							
							var country = $(this).val();
							
							if (country == 168) {
									
									$('#city-area').show();	
							} else {
									
									$('#city').val(null).trigger('change');
									$('#city-area').hide();	
							}
							
							$('#setup_timeline_days').val('');
							$('#case-type-area').hide();	
				  });
				  
				  $('#country').change(function() {
							
							
							var country 		= $(this).val();
							var selectedCompany = 0;
							
							<?php if ($this->input->post('company')) { ?>
							
								 selectedCompany = <?php echo $this->input->post('company'); ?>;
							
							<?php } ?>
							
								$.ajax({
									  type   	  : "POST",
									  dataType 	  : "HTML",
									  url		  : "<?php echo base_url();?>load-companies-by-country/",
									  data		  : {  
									  						country 			: country,
									  						selectedCompany 	: selectedCompany
													}
									
										 }).done(function( rowData ) {
									
									  			if (rowData) {
						  								
														$('#company-area').html(rowData);
						   						}
									  
								}); // END Ajax Request
							
				  });
				  
				  $(document).on('change', '#case_type', function() {
							
						var caseType 		= $(this).val();
						var caseTypeName	= $("#case_type :selected").text();
						
						if (caseType == 8 || caseType == 9) {
							  
							    $('#case_type_particulars').empty();
							  	$('#case_type_particulars').append('<option value="both">'+caseTypeName+'-Both (U,L)</option><option value="upper">'+caseTypeName+'-U</option><option value="upper">'+caseTypeName+'-L</option>'); 
								
								$('#case-class-area').show();
						
						} else {
						
								$('#case-class-area').hide();
							
						}
						
						
				  });
		 });
		 
		 function getCaseTypes(company) {
				
				if (company) {
					
							var setupTimelineDays   = $('#company option:selected').attr('setupTimelineDays');
					
							$('#setup_timeline_days').val(setupTimelineDays);
							
							var selectedCaseType = 0;
							
							<?php if ($this->input->post('case_type')) { ?>
							
								 selectedCaseType = <?php echo $this->input->post('case_type'); ?>;
							
							<?php } ?>
							
								$.ajax({
									
									  type   	  : "POST",
									  dataType 	  : "HTML",
									  url		  : "<?php echo  base_url();?>load-company-case-types/",
									  data		  : {  
									  						company 			: company,
									  						selectedCaseType 	: selectedCaseType
													}
									
										 }).done(function( rowData ) {
									
									  			if (rowData) {
						  								
														$('#case-types').html(rowData);
														//$('#case-type-area').show();	
														
														if (selectedCaseType) {
															  
															  if (selectedCaseType == 8 || selectedCaseType == 9) {
																
																	//$('#case-class-area').show();			
															  }
														}
														
						   						}
									  
								}); // END Ajax Request
								
				} else {
								
						$('#setup_timeline_days').val('');
						$('#case-type-area').hide();
						$('#case-types').val(null).trigger('change');	
						$('#case-class-area').hide();	
				}
		 }
		 
		 function setMissingTeeth_LDC(Quadrant,AreaName,TeethNumber){
				
					 var flag = true;
					 
					 $('#LDC_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html('<strike style="color:#fc4b6c">'+TeethNumber+'</strike>');
					 
					 $('.TR_LDC_TMQ'+Quadrant+'_'+TeethNumber).html('<span title="Missing Teeth">x</span>');
					 
					 var HiddenInputArray   		= $('input[name="LDC_missing_teeth[]"]');
					 
					 var HiddenInputArrayLength		= HiddenInputArray.length; 
					 
					 var Input_Value				= Quadrant+'_'+TeethNumber;
					 
					 if (HiddenInputArrayLength > 0) {
							
							var javaScriptArray = new Array();
							
							HiddenInputArray.map(function() {
   								 javaScriptArray.push(this.value);
   							 }).get();
							 
						
						if(jQuery.inArray(Input_Value,javaScriptArray) == -1) {
  
   								var Hidden_Input	= '<input type="hidden" name="LDC_missing_teeth[]" value="'+Input_Value+'" id="LDC_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
					    		$('#loose-dental-crowns-missing-teeth-data-area').append(Hidden_Input);
								 
						} else {
								
								var checkbox_TMQ  = '<input id="LDC_TMQ'+Quadrant+'_'+TeethNumber+'" name="LDC_TMQ'+Quadrant+'[]" value="'+TeethNumber+'" class="chk-col-purple TM" type="checkbox"><label title="Teeth Movement" for="LDC_TMQ'+Quadrant+'_'+TeethNumber+'" style="padding-left:0px; line-height: 0px;">&nbsp;</label>';
								
								
								$('#LDC_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html(TeethNumber);
								
								$('.TR_LDC_TMQ'+Quadrant+'_'+TeethNumber).html(checkbox_TMQ);
								
								$('#LDC_input_'+Quadrant+'_'+TeethNumber).remove();
						}
					 }
					
					if (HiddenInputArrayLength == 0) {
					 
				    	var Hidden_Input  = '<input type="hidden" name="LDC_missing_teeth[]" value="'+Input_Value+'" id="LDC_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
				    	$('#loose-dental-crowns-missing-teeth-data-area').append(Hidden_Input);
					}
					 
				return;
		}
		
		 function setMissingTeeth_FILLING(Quadrant,AreaName,TeethNumber){
			
					 var flag = true;
					 
					 $('#FILLING_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html('<strike style="color:#fc4b6c">'+TeethNumber+'</strike>');
					 
					 $('.TR_FILLING_TMQ'+Quadrant+'_'+TeethNumber).html('<span title="Missing Teeth">x</span>');
					 
					 var HiddenInputArray   		= $('input[name="FILLING_missing_teeth[]"]');
					 
					 var HiddenInputArrayLength		= HiddenInputArray.length; 
					 
					 var Input_Value				= Quadrant+'_'+TeethNumber;
					 
					 if (HiddenInputArrayLength > 0) {
							
							var javaScriptArray = new Array();
							
							HiddenInputArray.map(function() {
   								 javaScriptArray.push(this.value);
   							 }).get();
							 
						
						if(jQuery.inArray(Input_Value,javaScriptArray) == -1) {
  
   								var Hidden_Input	= '<input type="hidden" name="FILLING_missing_teeth[]" value="'+Input_Value+'" id="FILLING_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
					    		$('#fillings-missing-teeth-data-area').append(Hidden_Input);
								 
						} else {
								
								var checkbox_TMQ  = '<input id="FILLING_TMQ'+Quadrant+'_'+TeethNumber+'" name="FILLING_TMQ'+Quadrant+'[]" value="'+TeethNumber+'" class="chk-col-purple TM" type="checkbox"><label title="Teeth Movement" for="FILLING_TMQ'+Quadrant+'_'+TeethNumber+'" style="padding-left:0px; line-height: 0px;">&nbsp;</label>';
								
								
								$('#FILLING_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html(TeethNumber);
								
								$('.TR_FILLING_TMQ'+Quadrant+'_'+TeethNumber).html(checkbox_TMQ);
								
								$('#FILLING_input_'+Quadrant+'_'+TeethNumber).remove();
						}
					 }
					
					if (HiddenInputArrayLength == 0) {
					 
				    	var Hidden_Input  = '<input type="hidden" name="FILLING_missing_teeth[]" value="'+Input_Value+'" id="FILLING_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
				    	$('#fillings-missing-teeth-data-area').append(Hidden_Input);
					}
					 
				return;
		}
		
		 function setMissingTeeth_WV(Quadrant,AreaName,TeethNumber){
				
					 var flag = true;
					 
					 $('#WV_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html('<strike style="color:#fc4b6c">'+TeethNumber+'</strike>');
					 
					 $('.TR_WV_TMQ'+Quadrant+'_'+TeethNumber).html('<span title="Missing Teeth">x</span>');
					 
					 var HiddenInputArray   		= $('input[name="WV_missing_teeth[]"]');
					 
					 var HiddenInputArrayLength		= HiddenInputArray.length; 
					 
					 var Input_Value				= Quadrant+'_'+TeethNumber;
					 
					 if (HiddenInputArrayLength > 0) {
							
							var javaScriptArray = new Array();
							
							HiddenInputArray.map(function() {
   								 javaScriptArray.push(this.value);
   							 }).get();
							 
						
						if(jQuery.inArray(Input_Value,javaScriptArray) == -1) {
  
   								var Hidden_Input	= '<input type="hidden" name="WV_missing_teeth[]" value="'+Input_Value+'" id="WV_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
					    		$('#wear-veneers-missing-teeth-data-area').append(Hidden_Input);
								 
						} else {
								
								var checkbox_TMQ  = '<input id="WV_TMQ'+Quadrant+'_'+TeethNumber+'" name="WV_TMQ'+Quadrant+'[]" value="'+TeethNumber+'" class="chk-col-yellow TM" type="checkbox"><label title="Teeth Movement" for="WV_TMQ'+Quadrant+'_'+TeethNumber+'" style="padding-left:0px; line-height: 0px;">&nbsp;</label>';
								
								
								$('#WV_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html(TeethNumber);
								
								$('.TR_WV_TMQ'+Quadrant+'_'+TeethNumber).html(checkbox_TMQ);
								
								$('#WV_input_'+Quadrant+'_'+TeethNumber).remove();
						}
					 }
					
					if (HiddenInputArrayLength == 0) {
					 
				    	var Hidden_Input  = '<input type="hidden" name="WV_missing_teeth[]" value="'+Input_Value+'" id="WV_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
				    	$('#wear-veneers-missing-teeth-data-area').append(Hidden_Input);
					}
					 
				return;
		}
		
		 function setMissingTeeth_URCP(Quadrant,AreaName,TeethNumber){
				
					 var flag = true;
					 
					 $('#URCP_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html('<strike style="color:#fc4b6c">'+TeethNumber+'</strike>');
					 
					 $('.TR_URCP_TMQ'+Quadrant+'_'+TeethNumber).html('<span title="Missing Teeth">x</span>');
					 
					 var HiddenInputArray   		= $('input[name="URCP_missing_teeth[]"]');
					 
					 var HiddenInputArrayLength		= HiddenInputArray.length; 
					 
					 var Input_Value				= Quadrant+'_'+TeethNumber;
					 
					 if (HiddenInputArrayLength > 0) {
							
							var javaScriptArray = new Array();
							
							HiddenInputArray.map(function() {
   								 javaScriptArray.push(this.value);
   							 }).get();
							 
						
						if(jQuery.inArray(Input_Value,javaScriptArray) == -1) {
  
   								var Hidden_Input	= '<input type="hidden" name="URCP_missing_teeth[]" value="'+Input_Value+'" id="URCP_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
					    		$('#undertake-root-canal-procedure-missing-teeth-data-area').append(Hidden_Input);
								 
						} else {
								
								var checkbox_TMQ  = '<input id="URCP_TMQ'+Quadrant+'_'+TeethNumber+'" name="URCP_TMQ'+Quadrant+'[]" value="'+TeethNumber+'" class="chk-col-green TM" type="checkbox"><label title="Teeth Movement" for="URCP_TMQ'+Quadrant+'_'+TeethNumber+'" style="padding-left:0px; line-height: 0px;">&nbsp;</label>';
								
								
								$('#URCP_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html(TeethNumber);
								
								$('.TR_URCP_TMQ'+Quadrant+'_'+TeethNumber).html(checkbox_TMQ);
								
								$('#URCP_input_'+Quadrant+'_'+TeethNumber).remove();
						}
					 }
					
					if (HiddenInputArrayLength == 0) {
					 
				    	var Hidden_Input  = '<input type="hidden" name="URCP_missing_teeth[]" value="'+Input_Value+'" id="URCP_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
				    	$('#undertake-root-canal-procedure-missing-teeth-data-area').append(Hidden_Input);
					}
					 
				return;
		}
		 
		 function setMissingTeeth_BW(Quadrant,AreaName,TeethNumber){
				
					 var flag = true;
					 
					 $('#BW_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html('<strike style="color:#fc4b6c">'+TeethNumber+'</strike>');
					 
					 $('.TR_BW_TMQ'+Quadrant+'_'+TeethNumber).html('<span title="Missing Teeth">x</span>');
					 
					 var HiddenInputArray   		= $('input[name="BW_missing_teeth[]"]');
					 
					 var HiddenInputArrayLength		= HiddenInputArray.length; 
					 
					 var Input_Value				= Quadrant+'_'+TeethNumber;
					 
					 if (HiddenInputArrayLength > 0) {
							
							var javaScriptArray = new Array();
							
							HiddenInputArray.map(function() {
   								 javaScriptArray.push(this.value);
   							 }).get();
							 
						
						if(jQuery.inArray(Input_Value,javaScriptArray) == -1) {
  
   								var Hidden_Input	= '<input type="hidden" name="BW_missing_teeth[]" value="'+Input_Value+'" id="BW_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
					    		$('#bridge-work-missing-teeth-data-area').append(Hidden_Input);
								 
						} else {
								
								var checkbox_TMQ  = '<input id="BW_TMQ'+Quadrant+'_'+TeethNumber+'" name="BW_TMQ'+Quadrant+'[]" value="'+TeethNumber+'" class="chk-col-blue TM" type="checkbox"><label title="Teeth Movement" for="BW_TMQ'+Quadrant+'_'+TeethNumber+'" style="padding-left:0px; line-height: 0px;">&nbsp;</label>';
								
								
								$('#BW_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html(TeethNumber);
								
								$('.TR_BW_TMQ'+Quadrant+'_'+TeethNumber).html(checkbox_TMQ);
								
								$('#BW_input_'+Quadrant+'_'+TeethNumber).remove();
						}
					 }
					
					if (HiddenInputArrayLength == 0) {
					 
				    	var Hidden_Input  = '<input type="hidden" name="BW_missing_teeth[]" value="'+Input_Value+'" id="BW_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
				    	$('#bridge-work-missing-teeth-data-area').append(Hidden_Input);
					}
					 
				return;
		}
		 
		 function setMissingTeeth_IMPLANT(Quadrant,AreaName,TeethNumber){
			
					 var flag = true;
					 
					 $('#IMPLANT_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html('<strike style="color:#fc4b6c">'+TeethNumber+'</strike>');
					 
					 $('.TR_IMPLANT_TMQ'+Quadrant+'_'+TeethNumber).html('<span title="Missing Teeth">x</span>');
					 
					 var HiddenInputArray   		= $('input[name="IMPLANT_missing_teeth[]"]');
					 
					 var HiddenInputArrayLength		= HiddenInputArray.length; 
					 
					 var Input_Value				= Quadrant+'_'+TeethNumber;
					 
					 if (HiddenInputArrayLength > 0) {
							
							var javaScriptArray = new Array();
							
							HiddenInputArray.map(function() {
   								 javaScriptArray.push(this.value);
   							 }).get();
							 
						
						if(jQuery.inArray(Input_Value,javaScriptArray) == -1) {
  
   								var Hidden_Input	= '<input type="hidden" name="IMPLANT_missing_teeth[]" value="'+Input_Value+'" id="IMPLANT_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
					    		$('#implant-missing-teeth-data-area').append(Hidden_Input);
								 
						} else {
								
								var checkbox_TMQ  = '<input id="IMPLANT_TMQ'+Quadrant+'_'+TeethNumber+'" name="IMPLANT_TMQ'+Quadrant+'[]" value="'+TeethNumber+'" class="chk-col-purple TM" type="checkbox"><label title="Teeth Movement" for="IMPLANT_TMQ'+Quadrant+'_'+TeethNumber+'" style="padding-left:0px; line-height: 0px;">&nbsp;</label>';
								
								
								$('#IMPLANT_TMQ'+Quadrant+'_MISSING_'+TeethNumber).html(TeethNumber);
								
								$('.TR_IMPLANT_TMQ'+Quadrant+'_'+TeethNumber).html(checkbox_TMQ);
								
								$('#IMPLANT_input_'+Quadrant+'_'+TeethNumber).remove();
						}
					 }
					
					if (HiddenInputArrayLength == 0) {
					 
				    	var Hidden_Input  = '<input type="hidden" name="IMPLANT_missing_teeth[]" value="'+Input_Value+'" id="IMPLANT_input_'+Quadrant+'_'+TeethNumber+'" />';
					 
				    	$('#implant-missing-teeth-data-area').append(Hidden_Input);
					}
					 
				return;
		}
			
</script>
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>new-case/">New Case</a></li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     	
        				<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
                        
                        
     <form id="frm" name="frm" action="<?php echo base_url(); ?>case-add/" method="post" enctype="multipart/form-data" autocomplete="off">
     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-heartbeat"></i> Patient</h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Patient Name</label>
				  <div class="col-sm-10">
					 <input type="text" class="form-control" name="patient_name" value="<?php echo $this->input->post('patient_name'); ?>" />
                     <?php if (form_error('patient_name')) { echo form_error('patient_name'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Patient Age</label>
				  <div class="col-sm-10">
					 <input type="number" min="1" class="form-control" name="age" value="<?php echo $this->input->post('age'); ?>" />
                     <?php if (form_error('age')) { echo form_error('age'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Gender</label>
                    <div class="col-sm-10">
                      <div class="radio">
                        <input name="gender" value="<?php echo HARD_CODE_ID_MALE; ?>" id="Option_8" type="radio" class="radio-col-purple" <?php if ($this->input->post('gender') == HARD_CODE_ID_MALE) { echo 'checked="checked" '; } ?>>
                        <label for="Option_8">Male</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="gender" value="<?php echo HARD_CODE_ID_FEMALE; ?>" id="Option_9" type="radio" class="radio-col-purple" <?php if ($this->input->post('gender') == HARD_CODE_ID_FEMALE) { echo 'checked="checked" '; } ?>>
                        <label for="Option_9">Female</label>              
                      </div>
                      
                      <?php if (form_error('gender')) { echo form_error('gender'); } ?>
                    </div>
                </div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Country</label>
				  <div class="col-sm-10">
					<select name="patient_country" id="patient_country" class="form-control select2" style="width: 100%;">
                       <option value="">Select a country</option>
                       <?php 
	                      if ($countries->num_rows() > 0) {
	                                                
                                    foreach($countries->result() as $country) {
                                        
                                                $countryID				=	$country->id;
                                                $countryName			=	$country->name;
                                                  
                                                    if ($this->input->post('patient_country') == $countryID) {
                                                    
                                                            $selectedCountry = 'selected = selected';	
                                                    
                                                    }	else {
                                                            
                                                            $selectedCountry = NULL;		
                                                    }
                                                
                                                echo '<option value="'.$countryID.'" '.$selectedCountry.'>'.$countryName.'</option>';
                                    }	
                            }
                       ?>
                      </select>
                      <?php if (form_error('patient_country')) { echo form_error('patient_country'); } ?>
                  </div>
				</div>
                
                <!-- <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">State</label>
                    <div class="col-sm-10">
					 <input type="text" id="administrative_area_level_1" class="form-control" name="patient_state" value="<?php echo $this->input->post('patient_state'); ?>" />
                     <?php if (form_error('patient_state')) { echo form_error('patient_state'); } ?>
				  </div>
                </div> -->

        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">State</label>
                <div class="col-sm-10">
					<select name="patient_state" id="patient_state" class="form-control select2" style="width: 100%;">
                       <option value="">Select a State</option>
                       <?php 
	                      if ($states->num_rows() > 0) {
	                                                
                                    foreach($states->result() as $state) {
                                        
                                                $stateID			=	$state->id;
                                                $stateName			=	$state->name;
                                                  
                                                    if ($this->input->post('patient_state') == $stateID) {
                                                    
                                                            $selectedstate = 'selected = selected';	
                                                    
                                                    }	else {
                                                            
                                                            $selectedstate = NULL;		
                                                    }
                                                
                                                echo '<option value="'.$stateID.'" '.$selectedstate.'>'.$stateName.'</option>';
                                    }	
                            }
                       ?>
                      </select>
                     <?php if (form_error('patient_state')) { echo form_error('patient_state'); } ?>
              </div>
        </div>
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">City</label>
                    <div class="col-sm-10">
					 <input type="text" id="locality" class="form-control" name="patient_city" value="<?php echo $this->input->post('patient_city'); ?>" />
                     <?php if (form_error('patient_city')) { echo form_error('patient_city'); } ?>
				  </div>
                </div>
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Zip Code</label>
                    <div class="col-sm-10">
					 <input type="text" id="postal_code" class="form-control" name="patient_zip_code" value="<?php echo $this->input->post('patient_zip_code'); ?>" />
                     <?php if (form_error('patient_zip_code')) { echo form_error('patient_zip_code'); } ?>
				  </div>
                </div>


                <!-- <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Street Number</label>
                    <div class="col-sm-10">
					 <input type="text" id="street_number" class="form-control" name="patient_zip_code" value="<?php echo $this->input->post('patient_zip_code'); ?>" />
                     <?php if (form_error('patient_zip_code')) { echo form_error('patient_zip_code'); } ?>
				  </div>
                </div> -->

                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
					 <input type="text" class="form-control" name="patient_email" value="<?php echo $this->input->post('patient_email'); ?>" />
                     <?php if (form_error('patient_email')) { echo form_error('patient_email'); } ?>
				  </div>
                </div>
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
					 <input type="text" class="form-control" name="patient_phone" value="<?php echo $this->input->post('patient_phone'); ?>" />
                     <?php if (form_error('patient_phone')) { echo form_error('patient_phone'); } ?>
				  </div>
                </div>
               
 				
            </div>
          </div>
        </div>
 
     </div>
     
     <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-globe"></i><h3 class="box-title">Address</h3>
        </div>
        <div class="box-body form-element">
          <div class="row">
          
            <div class="col-sm-3" id="locationField">
              <input id="autocomplete" onFocus="geolocate()" name="patient_address_1" value="<?php echo $this->input->post('patient_address_1'); ?>" class="form-control" placeholder="Address 1" type="text" />
            </div>
            <div class="col-sm-4">
              <input name="patient_address_2" value="<?php echo $this->input->post('patient_address_2'); ?>" class="form-control" placeholder="Address 2" type="text" />
            </div>
            <div class="col-sm-5">
             <input name="patient_address_3" value="<?php echo $this->input->post('patient_address_3'); ?>" class="form-control" placeholder="Address 3" type="text" />
            </div>
          </div>
        </div>
      </div>
     
     <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-medkit"></i> Arches</h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
                
              <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Arch (es) </label>
				  <div class="col-sm-8">

                      <div class="demo-checkbox">
                         <input id="md_checkbox_1" name="arch_upper" value="<?php echo HARD_CODE_ID_YES; ?>" class="chk-col-black" type="checkbox" <?php if ($this->input->post('arch_upper') == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                         <label for="md_checkbox_1">Upper</label>
                            
                          <input id="md_checkbox_2" name="arch_lower" value="<?php echo HARD_CODE_ID_YES; ?>" class="chk-col-black" type="checkbox" <?php if ($this->input->post('arch_lower') == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                          <label for="md_checkbox_2">Lower</label>
                      </div>                 
                   <?php if (form_error('arch')) { echo form_error('arch'); } ?>
                  </div>
				</div>
                
				
            </div>
          </div>
        </div>
 
     </div>
     
     <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-calendar"></i> Case Received</h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Case Receive Date</label>
				  <div class="col-sm-10">
					<?php if ($this->input->post('receive_date')) {
                        
                                  $receiveDate = 	$this->input->post('receive_date');	  
                          } else {
                                  $receiveDate = date('Y-m-d',time());	
                          }
                    ?>
                    <input class="form-control" name="receive_date" id="receive_date" type="date" value="<?php echo $receiveDate; ?>">
                    <?php if (form_error('receive_date')) { echo form_error('receive_date'); } ?>
                  </div>
			   </div>
				
            </div>
          </div>
        </div>
 
     </div>
     
     <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-money"></i> Transaction</h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Mode</label>
                    <div class="col-sm-10">
                      <div class="radio">
                        <input name="transaction" value="kit" id="kit_sale" type="radio" class="radio-col-red" <?php if ($this->input->post('transaction') == 'kit') { echo 'checked="checked" '; } ?>>
                        <label for="kit_sale">Sale Kit</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="transaction" value="instalment" id="instalment" type="radio" class="radio-col-red" <?php if ($this->input->post('transaction') == 'instalment') { echo 'checked="checked" '; } ?>>
                        <label for="instalment">Instalment</label>   
                         &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="transaction" value="case_purchase" id="case_purchase" type="radio" class="radio-col-red" <?php if ($this->input->post('transaction') == 'case_purchase') { echo 'checked="checked" '; } ?>>
                        <label for="case_purchase">Full Case</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="transaction" value="retainer_only" id="retainer_only" type="radio" class="radio-col-red" <?php if ($this->input->post('transaction') == 'retainer_only') { echo 'checked="checked" '; } ?>>
                        <label for="retainer_only">Retainer Only</label>   
                                   
                      </div>
                      
                      <?php if (form_error('transaction')) { echo form_error('transaction'); } ?>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Treatment</label>
                    <div class="col-sm-10">
                      <div class="radio">
                        <input name="treatment_type" value="night_time" id="night_time" type="radio" class="radio-col-navy" <?php if ($this->input->post('treatment_type') == 'night_time') { echo 'checked="checked" '; } ?>>
                        <label for="night_time">Night Time</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="treatment_type" value="regular" id="regular" type="radio" class="radio-col-navy" <?php if ($this->input->post('treatment_type') == 'regular') { echo 'checked="checked" '; } ?>>
                        <label for="regular">Regular</label>   
                      </div>
                      
                      <?php if (form_error('treatment_type')) { echo form_error('treatment_type'); } ?>
                    </div>
                </div>
				
            </div>
          </div>
        </div>
 
     </div>
     
     <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-map-marker"></i> Location</h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
              	
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Country</label>
				  <div class="col-sm-10">
					<select name="country" id="country" class="form-control select2" style="width: 100%;">
                       <option value="">Select a country</option>
                       <?php 
                              if ($countries->num_rows() > 0) {
                                                        
                                                        foreach($countries->result() as $country) {
                                                            
                                                                    $countryID				=	$country->id;
                                                                    $countryName			=	$country->name;
                                                                      
																	    if ($countryID == 40 || $countryID == 236  || $countryID == 235 || $countryID == 14) {
																		} else { continue; }
																	    
                                                                        if ($this->input->post('country') == $countryID) {
                                                                        
                                                                                $selectedCountry = 'selected = selected';	
                                                                        
                                                                        }	else {
                                                                                
                                                                                $selectedCountry = NULL;		
                                                                        }
                                                                    
                                                                    echo '<option value="'.$countryID.'" '.$selectedCountry.'>'.$countryName.'</option>';
                                                        }	
                                                }
                       ?>
                      </select>
                      <?php if (form_error('country')) { echo form_error('country'); } ?>
                  </div>
				</div>
                
                <div class="form-group row" id="city-area" style=" <?php if ($this->input->post('country') == 168) { echo 'display:block;'; } else { echo 'display:none;';} ?>">
				  <label for="" class="col-sm-2 col-form-label">City</label>
				  <div class="col-sm-10">
                      <select name="city" id="city" class="form-control select2" style="width: 100%;">
                      <option value="">Select a city</option>
                       <?php 
                              if ($cities->num_rows() > 0) {
                                                        
                                          foreach($cities->result() as $city) {
                                              
                                                      $cityID				=	$city->id;
                                                      $cityName				=	$city->name;
                                                          
                                                          if ($this->input->post('city') == $cityID) {
                                                          
                                                                  $selectedCity = 'selected = selected';	
                                                          
                                                          }	else {
                                                                  
                                                                  $selectedCity = NULL;		
                                                          }
                                                      
                                                      echo '<option value="'.$cityID.'" '.$selectedCity.'>'.$cityName.'</option>';
                                          }	
                                  }
                       ?>
                    </select>      
                    <?php if (form_error('city')) { echo form_error('city'); } ?>
                  </div>
                  
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Distributor</label>
				  <div class="col-sm-10">
					<div id="company-area">
                       <select name="company" id="company" class="form-control select2" style="width: 100%;" onchange="return getCaseTypes(this.value);">
                        <option value="">Select a company</option>
                      </select>
                       <?php if (form_error('company')) { echo form_error('company'); } ?>
                     </div>
                  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Setups Timeline (Days)</label>
				  <div class="col-sm-10">
					    <input type="text" class="form-control" name="setup_timeline_days" id="setup_timeline_days" value="<?php echo $this->input->post('setup_timeline_days'); ?>" readonly style="background:#FFF" />  
                        <?php if (form_error('setup_timeline_days')) { echo form_error('setup_timeline_days'); } ?>
                    
                  </div>
				</div>
                
                <div class="form-group row" id="case-type-area" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Case Type</label>
				  <div class="col-sm-10">
					<div id="case-types"></div>
                     <?php if (form_error('case_type')) { echo form_error('case_type'); } ?>
                  </div>
				</div>
                
                <div class="form-group row" id="case-class-area" style="display:none;">
                    <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                    <div class="col-sm-10">
                    	<?php 
								if ($this->input->post('case_type') == 8) {
										
										$upperName = 'CPB-U';
										$lowerName = 'CPB-L';	
										$bothName  = 'CPB- Both (U,L)';	
								
								} else if ($this->input->post('case_type') == 9) {
										
										$upperName = 'CPE-U';
										$lowerName = 'CPE-L';	
										$bothName  = 'CPB-Both (U,L)';	
								
									
								}
						?>
                    
                       <select name="case_type_particulars" id="case_type_particulars" class="form-control select2 type-class" style="width: 100%;">
                        <option value="both" <?php if ($this->input->post('case_type_particulars') == 'both') { echo 'selected = selected'; } ?>><?php echo $bothName; ?></option>
                        <option value="upper" <?php if ($this->input->post('case_type_particulars') == 'upper') { echo 'selected = selected'; } ?>><?php echo $upperName; ?></option>
                        <option value="lower" <?php if ($this->input->post('case_type_particulars') == 'lower') { echo 'selected = selected'; } ?>><?php echo $lowerName; ?></option>
                       </select>
                      
                      <?php if (form_error('case_type_particulars')) { echo form_error('case_type_particulars'); } ?>
                    </div>
                 </div>
				
            </div>
          </div>
        </div>
 
     </div>
    
    
     
     <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Medical History</h3>
        </div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-6 col-12">
         
              <div class="form-group">
                <label>Do you have loose dental restoration-crowns, etc?</label>
                <div class="radio">
                  <input name="medical_history_loose_crowns" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_loose_crowns_yes" type="radio" class="radio-col-purple" <?php if ($this->input->post('medical_history_loose_crowns') == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                  <label for="medical_history_loose_crowns_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_loose_crowns" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_loose_crowns_no" type="radio" class="radio-col-purple" <?php if ($this->input->post('medical_history_loose_crowns') == HARD_CODE_ID_NO) { echo 'checked="checked" '; } ?>>
                  <label for="medical_history_loose_crowns_no">No</label>              
                </div>
                <?php if (form_error('medical_history_loose_crowns')) { echo form_error('medical_history_loose_crowns'); } ?>
              </div>
              
              <div class="form-group">
               <div id="loose-dental-crowns-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="LDC_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_LDC('1','LDC_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                 <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_LDC_TMQ1_<?php echo $i; ?>">
                                                <input id="LDC_TMQ1_<?php echo $i; ?>" name="LDC_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-purple TM" type="checkbox">
                                                <label title="Teeth Movement" for="LDC_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="LDC_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_LDC('2','LDC_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                   <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_LDC_TMQ2_<?php echo $i; ?>">
                                                 <input id="LDC_TMQ2_<?php echo $i; ?>" name="LDC_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-purple TM" type="checkbox">
                                                 <label for="LDC_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="LDC_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_LDC('4','LDC_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_LDC_TMQ4_<?php echo $i; ?>">
                                                <input id="LDC_TMQ4_<?php echo $i; ?>" name="LDC_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-purple TM" type="checkbox">
                                                <label for="LDC_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="LDC_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_LDC('3','LDC_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_LDC_TMQ3_<?php echo $i; ?>">
                                                 <input id="LDC_TMQ3_<?php echo $i; ?>" name="LDC_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-purple TM" type="checkbox">
                                                 <label for="LDC_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group">
                <label>Do you have fillings?</label>
                <div class="radio">
                  <input name="medical_history_fillings" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_fillings_yes" type="radio" class="radio-col-orange" <?php if ($this->input->post('medical_history_fillings') == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                  <label for="medical_history_fillings_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_fillings" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_fillings_no" type="radio" class="radio-col-orange" <?php if ($this->input->post('medical_history_fillings') == HARD_CODE_ID_NO) { echo 'checked="checked" '; } ?>>
                  <label for="medical_history_fillings_no">No</label>              
                </div>
                <?php if (form_error('medical_history_fillings')) { echo form_error('medical_history_fillings'); } ?>
              </div>
              
              <div class="form-group">
               <div id="fillings-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="FILLING_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_FILLING('1','FILLING_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                 <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_FILLING_TMQ1_<?php echo $i; ?>">
                                                <input id="FILLING_TMQ1_<?php echo $i; ?>" name="FILLING_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-orange TM" type="checkbox">
                                                <label title="Teeth Movement" for="FILLING_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="FILLING_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_FILLING('2','FILLING_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_FILLING_TMQ2_<?php echo $i; ?>">
                                                 <input id="FILLING_TMQ2_<?php echo $i; ?>" name="FILLING_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-orange TM" type="checkbox">
                                                 <label for="FILLING_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="FILLING_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_FILLING('4','FILLING_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_FILLING_TMQ4_<?php echo $i; ?>">
                                                <input id="FILLING_TMQ4_<?php echo $i; ?>" name="FILLING_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-orange TM" type="checkbox">
                                                <label for="FILLING_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="FILLING_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_FILLING('3','FILLING_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_FILLING_TMQ3_<?php echo $i; ?>">
                                                 <input id="FILLING_TMQ3_<?php echo $i; ?>" name="FILLING_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-orange TM" type="checkbox">
                                                 <label for="FILLING_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group" style="padding-top:50px;">
                <label>Did you undertake a root canal procedure in the past?</label>
                <div class="radio">
                  <input name="medical_history_undertake_root_canal_procedure" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_undertake_root_canal_procedure_yes" type="radio" class="radio-col-green" <?php if ($this->input->post('medical_history_undertake_root_canal_procedure') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_undertake_root_canal_procedure_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_undertake_root_canal_procedure" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_undertake_root_canal_procedure_no" type="radio" class="radio-col-green" <?php if ($this->input->post('medical_history_undertake_root_canal_procedure') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_undertake_root_canal_procedure_no">No</label>              
                </div>
                <?php if (form_error('medical_history_undertake_root_canal_procedure')) { echo form_error('medical_history_undertake_root_canal_procedure'); } ?>
              </div>
              
              <div class="form-group">
               <div id="undertake-root-canal-procedure-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="URCP_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_URCP('1','URCP_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_URCP_TMQ1_<?php echo $i; ?>">
                                                <input id="URCP_TMQ1_<?php echo $i; ?>" name="URCP_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-green TM" type="checkbox">
                                                <label title="Teeth Movement" for="URCP_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="URCP_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_URCP('2','URCP_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_URCP_TMQ2_<?php echo $i; ?>">
                                                 <input id="URCP_TMQ2_<?php echo $i; ?>" name="URCP_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-green TM" type="checkbox">
                                                 <label for="URCP_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="URCP_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_URCP('4','URCP_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_URCP_TMQ4_<?php echo $i; ?>">
                                                <input id="URCP_TMQ4_<?php echo $i; ?>" name="URCP_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-green TM" type="checkbox">
                                                <label for="URCP_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="URCP_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_URCP('3','URCP_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_URCP_TMQ3_<?php echo $i; ?>">
                                                 <input id="URCP_TMQ3_<?php echo $i; ?>" name="URCP_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-green TM" type="checkbox">
                                                 <label for="URCP_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group" style="padding-top:50px;">
                <label>Do your teeth hurt?</label>
                
                <div class="radio">
                  <input name="medical_history_teeth_hurt" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_teeth_hurt_yes" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_teeth_hurt') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_teeth_hurt_yes">Yes</label>      
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_teeth_hurt" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_teeth_hurt_no" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_teeth_hurt') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_teeth_hurt_no">No</label>              
                 </div>
              </div>
             
              <div class="form-group">
                <label>Do you have any wounds or swellings in your mouth?</label>
                <div class="radio">
                  <input name="medical_history_wounds_swellings" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_wounds_swellings_yes" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_wounds_swellings') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_wounds_swellings_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_wounds_swellings" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_wounds_swellings_no" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_wounds_swellings') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_wounds_swellings_no">No</label>              
                </div>
                <?php if (form_error('medical_history_wounds_swellings')) { echo form_error('medical_history_wounds_swellings'); } ?>
                            
              </div>
              
              <div class="form-group">
                <label>Do you have problems with your gums? (E.g. bleeding, declining or inflammed gums?)</label>
                <div class="radio">
                  <input name="medical_history_gums_problem" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_gums_problem_yes" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_gums_problem') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_gums_problem_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_gums_problem" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_gums_problem_no" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_gums_problem') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_gums_problem_no">No</label>              
                </div>
                <?php if (form_error('medical_history_gums_problem')) { echo form_error('medical_history_gums_problem'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you wear a retainer?</label>
                <div class="radio">
                  <input name="medical_history_wear_retainer" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_wear_retainer_yes" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_wear_retainer') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_wear_retainer_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_wear_retainer" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_wear_retainer_no" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_wear_retainer') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_wear_retainer_no">No</label>              
                </div>
                
                <?php if (form_error('medical_history_wear_retainer')) { echo form_error('medical_history_wear_retainer'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you have untreated gum disease?</label>
                <div class="radio">
                  <input name="medical_history_untreated_gum_disease" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_untreated_gum_disease_yes" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_untreated_gum_disease') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_untreated_gum_disease_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_untreated_gum_disease" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_untreated_gum_disease_no" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_untreated_gum_disease') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_untreated_gum_disease_no">No</label>              
                </div>
                <?php if (form_error('medical_history_untreated_gum_disease')) { echo form_error('medical_history_untreated_gum_disease'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you have recent history of jaw locking or severe jaw pain? (past 6 months)</label>
                <div class="radio">
                  <input name="medical_history_jaw_pain" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_jaw_pain_yes" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_jaw_pain') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_jaw_pain_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_jaw_pain" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_jaw_pain_no" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_jaw_pain') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_jaw_pain_no">No</label>              
                </div>
                <?php if (form_error('medical_history_jaw_pain')) { echo form_error('medical_history_jaw_pain'); } ?>
              </div>

            </div>

            <div class="col-md-6 col-12">
              
              <div class="form-group">
                <label>Do you wear veneers?</label>
                <div class="radio">
                  <input name="medical_history_wear_veneers" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_wear_veneers_yes" type="radio" class="radio-col-yellow" <?php if ($this->input->post('medical_history_wear_veneers') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_wear_veneers_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_wear_veneers" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_wear_veneers_no" type="radio" class="radio-col-yellow" <?php if ($this->input->post('medical_history_wear_veneers') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_wear_veneers_no">No</label>              
                </div>
                <?php if (form_error('medical_history_wear_veneers')) { echo form_error('medical_history_wear_veneers'); } ?>
              </div>
              
              <div class="form-group">
               <div id="wear-veneers-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="WV_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_WV('1','WV_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                 <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_WV_TMQ1_<?php echo $i; ?>">
                                                <input id="WV_TMQ1_<?php echo $i; ?>" name="WV_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-yellow TM" type="checkbox">
                                                <label title="Teeth Movement" for="WV_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="WV_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_WV('2','WV_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_WV_TMQ2_<?php echo $i; ?>">
                                                 <input id="WV_TMQ2_<?php echo $i; ?>" name="WV_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-yellow TM" type="checkbox">
                                                 <label for="WV_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                 <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="WV_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_WV('4','WV_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_WV_TMQ4_<?php echo $i; ?>">
                                                <input id="WV_TMQ4_<?php echo $i; ?>" name="WV_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-yellow TM" type="checkbox">
                                                <label for="WV_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="WV_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_WV('3','WV_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_WV_TMQ3_<?php echo $i; ?>">
                                                 <input id="WV_TMQ3_<?php echo $i; ?>" name="WV_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-yellow TM" type="checkbox">
                                                 <label for="WV_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group">
                <label>Do you have implant?</label>
                <div class="radio">
                  <input name="medical_history_implant" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_implant_yes" type="radio" class="radio-col-maroon" <?php if ($this->input->post('medical_history_implant') == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                  <label for="medical_history_implant_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_implant" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_implant_no" type="radio" class="radio-col-maroon" <?php if ($this->input->post('medical_history_implant') == HARD_CODE_ID_NO) { echo 'checked="checked" '; } ?>>
                  <label for="medical_history_implant_no">No</label>              
                </div>
                <?php if (form_error('medical_history_implant')) { echo form_error('medical_history_implant'); } ?>
              </div>
              
              <div class="form-group">
               <div id="implant-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="IMPLANT_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_IMPLANT('1','IMPLANT_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_IMPLANT_TMQ1_<?php echo $i; ?>">
                                                <input id="IMPLANT_TMQ1_<?php echo $i; ?>" name="IMPLANT_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-maroon TM" type="checkbox">
                                                <label title="Teeth Movement" for="IMPLANT_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                 <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="IMPLANT_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_IMPLANT('2','IMPLANT_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_IMPLANT_TMQ2_<?php echo $i; ?>">
                                                 <input id="IMPLANT_TMQ2_<?php echo $i; ?>" name="IMPLANT_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-maroon TM" type="checkbox">
                                                 <label for="IMPLANT_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                 <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="IMPLANT_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_IMPLANT('4','IMPLANT_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                 <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_IMPLANT_TMQ4_<?php echo $i; ?>">
                                                <input id="IMPLANT_TMQ4_<?php echo $i; ?>" name="IMPLANT_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-maroon TM" type="checkbox">
                                                <label for="IMPLANT_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="IMPLANT_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_IMPLANT('3','IMPLANT_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                 <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_IMPLANT_TMQ3_<?php echo $i; ?>">
                                                 <input id="IMPLANT_TMQ3_<?php echo $i; ?>" name="IMPLANT_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-maroon TM" type="checkbox">
                                                 <label for="IMPLANT_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
            
              <div class="form-group" style="padding-top:50px;">
                <label>Do you have bridge work?</label>
               <div class="radio">
                  <input name="medical_history_bridge_work" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_bridge_work_yes" type="radio" class="radio-col-blue" <?php if ($this->input->post('medical_history_bridge_work') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_bridge_work_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_bridge_work" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_bridge_work_no" type="radio" class="radio-col-blue" <?php if ($this->input->post('medical_history_bridge_work') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_bridge_work_no">No</label>              
                </div>
               <?php if (form_error('medical_history_bridge_work')) { echo form_error('medical_history_bridge_work'); } ?>
              </div>
              
              <div class="form-group">
               <div id="bridge-work-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="BW_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_BW('1','BW_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_BW_TMQ1_<?php echo $i; ?>">
                                                <input id="BW_TMQ1_<?php echo $i; ?>" name="BW_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-blue TM" type="checkbox">
                                                <label title="Teeth Movement" for="BW_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                   <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="BW_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_BW('2','BW_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                   <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_BW_TMQ2_<?php echo $i; ?>">
                                                 <input id="BW_TMQ2_<?php echo $i; ?>" name="BW_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-blue TM" type="checkbox">
                                                 <label for="BW_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="BW_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_BW('4','BW_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_BW_TMQ4_<?php echo $i; ?>">
                                                <input id="BW_TMQ4_<?php echo $i; ?>" name="BW_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-blue TM" type="checkbox">
                                                <label for="BW_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                 <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="BW_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_BW('3','BW_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_BW_TMQ3_<?php echo $i; ?>">
                                                 <input id="BW_TMQ3_<?php echo $i; ?>" name="BW_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-blue TM" type="checkbox">
                                                 <label for="BW_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group" style="padding-top:50px;">
                <label>Have you visited dentist in past 6 months?</label>
                <div class="radio">
                  <input name="medical_history_visited_dentist_past_months" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_visited_dentist_past_months_yes" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_visited_dentist_past_months') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_visited_dentist_past_months_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_visited_dentist_past_months" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_visited_dentist_past_months_no" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_visited_dentist_past_months') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_visited_dentist_past_months_no">No</label>              
                </div>
                <?php if (form_error('medical_history_visited_dentist_past_months')) { echo form_error('medical_history_visited_dentist_past_months'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you have any other problems with you teeth?</label>
                <input type="text" class="form-control" name="medical_history_other_problem_teeth" value="<?php echo $this->input->post('medical_history_other_problem_teeth'); ?>" />
        	    <?php if (form_error('medical_history_other_problem_teeth')) { echo form_error('medical_history_other_problem_teeth'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you take any medications?</label>
                <input type="text" class="form-control" name="medical_history_medications" value="<?php echo $this->input->post('medical_history_medications'); ?>" />
                <?php if (form_error('medical_history_medications')) { echo form_error('medical_history_medications'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you have any allergies?</label>
                <input type="text" class="form-control" name="medical_history_allergies" value="<?php echo $this->input->post('medical_history_allergies'); ?>" />
                <?php if (form_error('medical_history_allergies')) { echo form_error('medical_history_allergies'); } ?>
              </div>
              
              <div class="form-group">
                <label>Are you pregnant?</label>
                <div class="radio">
                  <input name="medical_history_pregnant" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_pregnant_yes" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_pregnant') == HARD_CODE_ID_YES) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_pregnant_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_pregnant" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_pregnant_no" type="radio" class="radio-col-navy" <?php if ($this->input->post('medical_history_pregnant') == HARD_CODE_ID_NO) { echo 'checked="checked"'; } ?>>
                  <label for="medical_history_pregnant_no">No</label>              
                </div>
                <?php if (form_error('medical_history_pregnant')) { echo form_error('medical_history_visited_dentist_past_months'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you have any of the following diseases?</label>
                <?php 
					if ($diseases) {
						
						$medicalDiseases = $this->input->post('medical_history_diseases');
							
						foreach($diseases->result() as $disease) {
							
							
							if ($this->input->post('medical_history_diseases')) {
								 
								  if (in_array($disease->id,$medicalDiseases)) {

										  $checkedDiseases = 'checked';	
								  } else {

										  $checkedDiseases = NULL;
								  }
							} else {
								
								 $checkedDiseases = NULL;	
							}
							
				?>
						
						<div class="checkbox">
							<input type="checkbox" name="medical_history_diseases[]" id="disease_<?php echo $disease->id; ?>" <?php echo $checkedDiseases; ?> value="<?php echo $disease->id; ?>">
							<label for="disease_<?php echo $disease->id; ?>"><?php echo $disease->name; ?></label>
						</div>
                        <?php if (form_error('medical_history_diseases')) { echo form_error('medical_history_diseases'); } ?>
				
				<?php	
						}	
					}
				?>
                </div>
               
              </div>

            </div>
         </div>
       
      </div>
     


     
    <!--  <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-paperclip"></i>  Attachments</h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
              	
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">RX Form</label>
				  <div class="col-sm-10">
					<input id="file_data_rx" name="file_data_rx" type="file">
 					<?php if (form_error('file_data_rx')) { echo ' <p class="help-block text-red">'.form_error('file_data_rx').'</p>'; } ?> 
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">X-rays (OPG)</label>
				  <div class="col-sm-10">
					<input id="file_data_x_ray_opg" name="file_data_x_ray_opg" type="file">
 					<?php if (form_error('file_data_x_ray_opg')) { echo ' <p class="help-block text-red">'.form_error('file_data_x_ray_opg').'</p>'; } ?> 
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">X-rays (Ceph)</label>
				  <div class="col-sm-10">
					<input id="file_data_x_ray_ceph" name="file_data_x_ray_ceph" type="file">
 					<?php if (form_error('file_data_x_ray_ceph')) { echo ' <p class="help-block text-red">'.form_error('file_data_x_ray_ceph').'</p>'; } ?> 
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Assessment File</label>
				  <div class="col-sm-10">
					<input id="file_data_assessment" name="file_data_assessment" type="file">
 					<?php if (form_error('file_data_assessment')) { echo ' <p class="help-block text-red">'.form_error('file_data_assessment').'</p>'; } ?> 
				  </div>
				</div>
                
            </div>
          </div>
        </div>
 
     </div> -->




     
     <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-comment"></i> Feedback </h3>
    </div>
              
      <div class="box-body">
        <div class="row">
          <div class="col-12">
             
              <div class="form-group row" style="display:none;">
                <label for="" class="col-sm-2 col-form-label">Priority</label>
                <div class="col-sm-10">
                  <div class="radio">
                      <input name="priority" value="<?php echo PRIORITY_HIGH; ?>" id="Option_5" type="radio" class="radio-col-red" <?php if ($this->input->post('priority') == PRIORITY_HIGH) { echo 'checked="checked" '; } ?>>
                      <label for="Option_5">High</label>                    
                  </div>
                  <div class="radio">
                        <input name="priority" value="<?php echo PRIORITY_MEDIUM; ?>" id="Option_6" type="radio" class="radio-col-yellow" <?php if ($this->input->post('priority') == PRIORITY_MEDIUM) { echo 'checked="checked" '; } else { echo 'checked=checked'; } ?>>
                        <label for="Option_6">Medium</label>                    
                  </div>
                  <div class="radio">
                        <input name="priority" value="<?php echo PRIORITY_LOW; ?>" id="Option_7" type="radio" class="radio-col-green" <?php if ($this->input->post('priority') == PRIORITY_LOW) { echo 'checked="checked" '; } ?>>
                        <label for="Option_7">Low</label>                    
                  </div>
                  <?php if (form_error('priority')) { echo form_error('priority'); } ?>
                </div>
              </div>
              
              <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Primary Concerns</label>
                <div class="col-sm-10">
                   <textarea name="comments" class="form-control" rows="10" placeholder=""><?php echo $this->input->post('comments'); ?></textarea>
                   <?php if (form_error('comments')) { echo form_error('comments'); } ?>
                </div>
              </div>
              
              
              <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                <div class="col-sm-10 offset-md-2">
                   <button type="submit" name="submit" value="Save" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
                   <button type="submit" name="submit" value="Save & New" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-open"></i> Save & New</button>
                   <button type="button" class="btn btn-warning " onclick="window.location.href='<?php echo base_url(); ?>cases-list/'">Cancel</button>
                </div>
              </div>
             
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>

   </div>
    
     </form>
	

    </section>
    <!-- /.content -->






    
    <?php 
		   if ($this->input->post('impression') && $this->input->post('impression') == CASE_IMPRESSION_TYPE_PHYSICAL) {
			   		
					 echo '<script language="javascript" type="text/javascript">
					  
								  $(document).ready(function() {
									  
									  $("#impression_particulars-area").show();
								  });
							
							</script>';
		   }
		   
		   echo '<script language="javascript" type="text/javascript">
					  
					  $(document).ready(function() {
						  
						   $("#country").trigger("change");
					  });
				</script>';
				
			if ($this->input->post('company')) {
					
					echo '<script language="javascript" type="text/javascript">
								
								getCaseTypes('.$this->input->post('company').');
							
						  </script>';
			}
			
			
	?>
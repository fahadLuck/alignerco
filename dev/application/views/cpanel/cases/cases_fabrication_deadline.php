<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>


	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Cases</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           

           
           
          <div class="box">
            <div class="box-header">
              
            
              <h3 class="box-title">Deadline Cases<small>(<?php echo $counter; ?>)</small></h3>


                <div class="box-tools">
                  
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="table-responsive">
              <table id="example" data-page-length='100' class="display" style="width:100%">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Case ID</th>
                  <th>Patient Name</th>
                  <th>Fabrication Deadline</th>
                </tr>
                <thead>
               <tbody>
                  <?php
                  $count=1;
                  foreach($fabricationDeadlineCases->result() as $fdlc)
                    {
                      $counter                         = $count++;
                      $caseID                          = $fdlc->id;
                      $patient                         = $fdlc->patient;
                      $aligners_timeline               = $fdlc->aligners_timeline;
                      $link                            = base_url().'case/'.encodeString($caseID);
                                      
                      /*if ($statusID == CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY) 
                      {
                          $link = base_url().'kit-send-to-customer/'.encodeString($caseID);
                      }*/

                      /*$setupCount  = $this->model_shared->getRecordMultipleWhere('id',CASE_SETUP_UPLOAD_DATA_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_id_fk' => $caseID))->num_rows();
                      if($setupCount==0)
                      {*/
                     
                    ?>      
                  <tr>
                    <td><?php echo $counter; ?></td>
                    <td><a target="_blank" href="<?php echo $link;?>"><?php echo $caseID; ?></a></td>
                    <td><a target="_blank" href="<?php echo $link;?>"><?php echo $patient; ?></a></td>
                    <td><?php echo $aligners_timeline; ?></td>
                  </tr>
              <?php } /*}*/ ?>
                              
               
               </tbody>
                   <!-- <tfoot>
                      <tr>
                        <th>Sr.</th>
                        <th>Shipment Number</th>
                        <th>Delievery Services</th>
                        <th>No Of Cases Missing</th>
                      </tr>
                  </tfoot> -->
              </table>
            </div>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>


    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" id="modificationModal"  aria-labelledby="myLargeModalLabel" aria-hidden="true">
           <div class="modal-dialog modal-lg">
              <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Shipped Cases Details</h4>
              </div>
              <div class="modal-body" id="modificationDetails">
              </div>
              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div> -->
            </div>
        </div>
      </div>
   <!-- Modal -->



<script>
$(document).ready(function(){

    $('.view_cases').click(function(){

             var table_ID=$(this).val();
             
             $.ajax({
                     url:"<?php echo base_url() ?>shipped-cases-details",
                     method:"post",
                     data:{table_ID :table_ID},
                    success: function(data)
                    {
                      $('#modificationDetails').html(data);
                      $('#modificationModal').modal("show");
                    },
                     error: function(xhr, status, error) 
                     {
                         alert("this is error "+error);
                      },
                });

         });

        
    });  
</script>


<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
   
   
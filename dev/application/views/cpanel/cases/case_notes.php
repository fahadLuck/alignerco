<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>
 <script type="text/javascript" language="javascript">
        
    $(document).ready(function(){
		
		 $('.case-add-note-popUp').click(function() {
						
						$('#txt_notes').val('');
						$('#form-add-note-error-message').hide().html('');
						$('#form-add-note-success-message').show().html('');  
				  });
				  
		 $('.add-note-case').click(function() {
				 
					 var noteTxt = $('#txt_notes').val();
					 
					 if (noteTxt == '') {
								
						$('#form-add-note-error-message').show().html('<i class="glyphicon glyphicon-warning-sign"></i> Please add note.');
						return false;
					 }
					
					 var message = 'Are you sure ADD NOTE for this case# <?php echo $case['caseID']; ?>?';
					 
					 if (confirm(message)) {
								
								addNote();	 
					 } else {
							
								return false;	 
					  }
		  });
	});
	
	function addNote() {
						 
			var caseID  		=  <?php echo $case['caseID']; ?>;
			var noteTxt 		= $('#txt_notes').val();
			var redirectPageURL = '<?php echo current_url(); ?>'; 
			
			
			$.ajax({
  
					 method		: "POST",
					 url		: "<?php echo base_url(); ?>case-add-note/",
					 dataType	: "JSON",
					 data		: {
									caseID 	 		: caseID,
									noteTxt	 		: noteTxt,
									redirectPageURL : redirectPageURL
								  },
  
					beforeSend	: function(){
  
							$('#form-add-note-error-message').hide().html('');
							$('#add-note-btn').hide();
							$('#add-note-loader').show();
						
										},
					success: function(data) {
							
								if (data.error == true) {
								
									 if(data.errorMessage == 'local_session_expire') {
										
										var errorMsg = 'Your session has been expired. Please login again for continue.';
									 }
								
								} else {
								
								$('#txt_notes').val('');
								 
									var successMsg = 'Note has been added successfully.';
							
									$('#form-add-note-error-message').hide().html('');
									$('#form-add-note-success-message').show().html('<i class="fa  fa-check"></i> '+successMsg);
									$('#add-note-loader').hide();
									$('#add-note-btn').show();
  
							
									 setTimeout(function(){location.reload()}, 2000);
									 
									 
									 
								}
							}
			   });
  
				 /* return false;*/
				 setTimeout(function(){location.reload()}, 2000);
						 
		 }  

   </script>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?><?php echo encodeString($case['caseID']); ?>/">Case Notes</a></li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     	
        				<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
     
     <?php 
	 		$todayDate 						= date('d-m-Y',time());
			
			$caseID 						= $case['caseID'];
			$patientName 					= $case['patientName'];
			$portalNumber 					= $case['portalNumber'];
			$doctorName 					= $case['doctorName'];
			
			$grandTotalNotes				= 0;
	 ?>
     
      <!--POP Model for Add Notes -->
      <div class="modal fade bs-example-modal-lg caseAddNotePopUp" role="dialog" aria-labelledby="caseNoteAddPopUp" aria-hidden="true" style="display:none;">
        <div class="modal-dialog modal-lg">
         
        <!-- Case Add Note Form Submit-->
        <form id="form-add-note" class="form-horizontal" method="post" action="<?php echo base_url(); ?>case-add-note/">  
         <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
         <div class="modal-content">
            
              <div class="modal-header">
                  <h4 class="modal-title" id="caseAddNotePopUp"><span id=""></span><i class="fa fa-commenting"></i> Add Case Notes</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              
              <div class="modal-body">
                         
                   <div class="row">
                        <div class="col-12">
                        
                        <div class="form-group">
                          <textarea class="textarea" id="txt_notes" name="txt_notes" rows="3" cols="5" placeholder="Add a note..." style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                         </div>
                   </div>
                    
              </div>
               
              <div class="modal-footer">
                   <button type="button" class="btn" id="add-note-loader" style="background-color:transparent; display:none;"><img src="<?php echo base_url();?>backend_images/ajax-loader.gif" /> Please Wait</button>
                   <button type="button" class="btn bg-orange add-note-case" id="add-note-btn" style="background-color:#5C6BC0;">Add Note</button>
                   <button type="button" class="btn btn-warning waves-effect text-left" data-dismiss="modal">Cancel</button>
                   <span id="form-add-note-error-message" class="help-block" style="color:#fc4b6c; display:none;"></span>
                   <span id="form-add-note-success-message" class="help-block" style="color:#7DAB2E; display:none;"></span>
              </div>
            
        </div>
        </form> 
        
       
         
       </div>
      </div>
      
      
      <div class="row">
        <div class="col-md-12">
         
               <div class="box">
                  <div class="box-body">
                  <div class="pull-left"><span style="font-size: 18px;">Total Notes (<span id="total-notes-area">0</span>)</span></div>
                  <div class="pull-right">
                 
                   
                   
                    <a href="javascript:void(0);" class="case-add-note-popUp" data-target=".caseAddNotePopUp" data-toggle="modal"><i class="fa fa-commenting"></i></a>
                   
				   
                   
                    <a style="margin-left:10px;" href="<?php echo base_url(); ?>case/<?php echo encodeString($case['caseID']); ?>/"><i class="fa fa-medkit"></i></a>
                    </div>
                  </div>
                </div>
         
          <ul class="timeline">
            
           <?php if ($notesDates->num_rows() > 0) { 
		   			
					foreach($notesDates->result() as $noteDate) {
							
							$addDate  = $noteDate->addDate;
							
							$addedDate 	 = date('F d, Y',strtotime($addDate));
							
							$caseNotes  = $this->model_case->getCaseNotesByDate($caseID,$addDate); 
		   ?>
                            
                            <li class="time-label">
                                  <span class="bg-red">
                                   <?php echo $addedDate; ?>
                                  </span>
                            </li>
                          
                           <?php if ($caseNotes->num_rows() > 0) {
							   			
										$counter         = 1;
										foreach($caseNotes->result() as $caseNote) {
											
											$grandTotalNotes++;
											
											$tableID 							 = $caseNote->tableID;
											$note 			 		 			 = $caseNote->note;
											
											$addDate 			 				 = $caseNote->addDate;
											$created 			 				 = $caseNote->created;
											
											$creatorID 			 				= $caseNote->creatorEmployeeID;
											$creatorName 			 			= $caseNote->creatorEmployeeName;
											$creatorCode 			 			= $caseNote->creatorEmployeeCode;
											
											$employeeAssignJobs					= getEmployeeAssignJobs($creatorID); // Calling From HR Employees Helper
														
											if ($employeeAssignJobs) {
																		
												 $employeeAssignJob 					= $employeeAssignJobs->row_array();
												 $creatorEmployeeAssignJobDescription	= $employeeAssignJob['jobPositionName'];
												  
											  
											 } else {
													  
												 $creatorEmployeeAssignJobDescription 	= NULL;
											 }
											
											
											$addDateTimeStamp					 = strtotime($addDate);
											
											$caseCreatedDate				 	 = date('F d, Y',$created);
											$caseCreatedTime		 		 	 = date('h:i A',$created);
											$caseCreatedTimeAgo				 	 = timeAgo($created).' ago'; // Calling From Shared Helper
											
											$caseAddedDate					 	 = date('l, F d, Y',$created);
											$caseAddedTime		 	 		 	 = date('h:i A',$created);
											$caseAddedTimeAgo				 	 = timeAgo($created).' ago'; // Calling From Shared Helper
											
											$caseNoteHTML  						= $caseNote->noteHTML;
											
											if ($caseNoteHTML == '') {
													
													$caseNote = $note;
											
											} else {
												
													$caseNote = $caseNoteHTML;
											}
											
											$title 	= '&nbsp;';
											$icon	= '<i class="ion ion-chatbubble-working bg-blue"></i>';
											
											 $title	= '<a href="javascript:void(0);">'.$creatorName.' - '.$creatorCode.' ('.$creatorEmployeeAssignJobDescription.')</a>'.'</small>';
											 $icon 	= '<i class="ion ion-chatbubble-working bg-green"></i>';
							?>
                            
                                             <li>
												<?php echo $icon; ?>
                                  
                                                <div class="timeline-item">
                                                  <span class="time"> <i class="fa fa-clock-o"></i> <span data-toggle="tooltip" title="" data-original-title="<?php echo $caseAddedTimeAgo; ?>"><?php echo $caseAddedDate ?> at <?php echo $caseAddedTime; ?></span></span>
                                  
                                                  <h3 class="timeline-header"><?php echo $title; ?></h3>
                                  
                                                  <div class="timeline-body">
                                                    <?php echo $caseNote; ?>
                                                  </div>
                                               
                                                </div>
                                            </li>
                          
                            
                            <?php	$counter++;
										}
								 } 
						   ?>
                          
            
           <?php } ?>
           
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
           
           <?php } ?>
           
          </ul>
        </div>
      </div>
    
    </section>
    
    <script language="javascript">
	
	 $(document).ready(function(){
			
			$('#total-notes-area').text(<?php echo $grandTotalNotes; ?>);	 
	 })
	
	</script>
    
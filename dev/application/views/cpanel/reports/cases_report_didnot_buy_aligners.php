<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Reports</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           
           <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>






      <div class="box box-default" id="waiting-for-approval-frm-area">
        <div class="box-header with-border">
          <h3 class="box-title">Search Filters </h3>

          <a class="pull-right box-tools" href="<?php echo base_url(); ?>cases-report"><i style="font-weight: bold;" class="fa fa-refresh"></i></a>
        </div>

       

        <!-- /.box-header -->
      
      <form id="" name="frm" action="<?php echo base_url(); ?>cases-report" method="post" enctype="multipart/form-data" autocomplete="off">
        <div id='waiting_checkboxes'></div>
        <div class="box-body">

          <div class="row">


            <div class="col-md-3 col-3">
              <div class="form-group">
                <label>Date From</label>
                    <input class="form-control" type="date" name="date_from" value="<?= $date_from ?>">
              </div>
            </div>


            <div class="col-md-3 col-3">
              <div class="form-group">
                <label>Date To</label>
                    <input class="form-control" type="date" name="date_to" value="<?= $date_to ?>">
              </div>
            </div>

            <div class="col-md-3 col-3">
              <div class="form-group">
                <label>Report</label>
                <select name="report" id="report" class="form-control" style="width: 100%;" >
                      <option value="">Select Report</option>
                      <option <?php echo (isset($report) && $report == 1) ? ' selected="selected"' : '' ; ?> value="1">kit was sold but the customer didn’t buy aligners</option>

                      <option <?php echo (isset($report) && $report == 2) ? ' selected="selected"' : '' ; ?> value="2">preview provided but didn’t buy aligners</option> 

                      <option <?php echo (isset($report) && $report == 3) ? ' selected="selected"' : '' ; ?> value="3">case was purchased but customer never got back to us</option>
                </select>
              </div>
            </div>



             <div class="col-md-2 col-2" style="margin-top: 30px;">
               <div class="form-group">
                 <div >
                    <button type="submit"  type="button" onClick="retun abc();" class="btn btn-blue"><i class="fa fa-search"></i> Search</button>
                 </div>
               </div> 
            </div>


          </div>
           
        </div>
      </form>   
  </div>






           
           
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="example" data-page-length='100' class="table table-responsive" style="width:100%">
                <thead>
                <tr>
                  <th width="">Sr.</th>
                  <th width="">Case ID</th>
                  <th width="">Patient</th>
                  <th width="">Recieve Date</th>
                </tr>
              </thead>
               <tbody>
               
          <?php if ($cases) {
                        
						$counter       = 0;
						foreach($cases->result() as $case) {
			  								
                  
									$caseID                                 = $case->caseID;
                  $patient                                = $case->patientName;
                  $receive_date                           = $case->receiveDate;
                  $link                                   = base_url().'case/'.encodeString($caseID);
                                      
                      /*if ($statusID == CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY) 
                      {
                          $link = base_url().'kit-send-to-customer/'.encodeString($caseID);
                      }*/ 

                    //check if preview is uploaded against this caseID 
                    $setupPreviewQuery                    = $this->model_shared->getRecordMultipleWhere('id,created',CASE_SETUP_UPLOAD_DATA_TABLE,array('case_id_fk' => $caseID,'is_deleted' => HARD_CODE_ID_NO));

                    $setupPreviewCount                    = $setupPreviewQuery->num_rows();
                    $setupPreview                         = $setupPreviewQuery->row_array();
                    $created                              = $setupPreview['created'];
                    if($created!='')
                    {
                        $previewCreatedDate               = date('Y-m-d',$created);
                    }
                    //check if preview is uploaded against this caseID


                    // check patient approve status against this caseID
                    $patientApproveStatusQuery            = $this->model_shared->getRecordMultipleWhere('id,patient_approve_status',MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO));

                    $patientApproveStatusCount            = $patientApproveStatusQuery->num_rows();
                    $ApproveStatus                        = $patientApproveStatusQuery->row_array();
                    $patientApproveStatus                 = $ApproveStatus['patient_approve_status'];
                    
                    
                  if($setupPreviewCount !=0 AND ($previewCreatedDate >= $date_from AND $previewCreatedDate<=$date_to) AND ($patientApproveStatusCount ==0 OR $patientApproveStatus!=APPROVED))
                  {  
                     $counter++; 
                        ?>
                                
                            <tr>

                                <td><?php echo $counter; ?></td>
                                <td><a target="_blank" href="<?php echo $link;?>"><?php echo $caseID; ?></a></td>
                                <td><a target="_blank" href="<?php echo $link;?>"><?= $patient ?></a></td>
                                <td><?= $previewCreatedDate ?></td>
                                
                            </tr>
			
              					 <?php 
                        }
                      }
                    }
                  ?>
               
               
               </tbody>
                  <!-- <tfoot>
                      <tr>
                          <th width="44">Sr.</th>
                          <th width="54">Doctor</th>
                          <th width="192">Assigned Cases</th>
                          <th width="164">Pending Cases</th>
                          <th width="305">Approved Cases</th>
                          <th width="179">Modified Cases</th>
                      </tr>
                  </tfoot> -->
              </table>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>





<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
   
   
   
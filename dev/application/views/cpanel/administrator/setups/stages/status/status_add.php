
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item">Setups</li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>manage-stages/">Producion Rooms</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>stage-status/<?php echo $stageSefURL; ?>">Status</a></li>
        <li class="breadcrumb-item active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      					<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
     
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="glyphicon glyphicon-plus"></i> Status</h3>
      </div>
   	  
      <form id="frm" name="frm" action="<?php echo base_url(); ?>add-status/<?php echo $stageSefURL; ?>" method="post" enctype="multipart/form-data" >
					
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	<div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Name</label>
				  <div class="col-sm-10">
					<input name="name" class="form-control" type="text" value="<?php echo $this->input->post('name'); ?>" id="example-text-input">
                    <?php if (form_error('name')) { echo form_error('name'); } ?>
				  </div>
				</div>
                
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Description</label>
				  <div class="col-sm-10">
					<textarea name="description" class="form-control" rows="3" placeholder=""><?php echo $this->input->post('description'); ?></textarea>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">&nbsp;</label>
				  <div class="col-sm-10 offset-md-2">
					 <button type="submit" name="submit" value="Save" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
                     <button type="submit" name="submit" value="Save & New" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-open"></i> Save & New</button>
                     <button type="button" class="btn btn-warning " onclick="window.location.href='<?php echo base_url(); ?>stage-status/<?php echo $stageSefURL; ?>'">Cancel</button>
				  </div>
				</div>
 				
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
     
      </form>   
        
        
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
  
    </section>
    <!-- /.content -->
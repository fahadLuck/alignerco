<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- <span class="logo-mini">AlCo</span> -->
      <!-- logo for regular state and mobile devices -->

      <?php

          /*$assignedRoles          = $this->assignedRoles;
          if (in_array(ROLE_DOCTOR,$assignedRoles))
          {
             ?><span class="logo-lg"><b><?php echo $userInfo['company']; ?></b></span><?php
          }
          else
          {
            ?><span class="logo-lg"><b>Portal</b></span><?php
          } */



          // to get current domain url
         $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

          $FullDomainName = $_SERVER['HTTP_HOST'] . '/';
           $domainName     = $_SERVER['HTTP_HOST'] . '/';
           $domainName     = str_replace('https://','',$domainName);
           $domainName     = str_replace('/','',$domainName);
           if($domainName==ALIGNERCO_DOMAIN)
          {
             $imageURL = base_url().'backend_images/alignerco-logo.png';
             ?><span ><image src="<?=  $imageURL ?>" style="width: 200px;"></span><?php
          }
          if($domainName==ALIGNERCO_CANADA_DOMAIN)
          {
             $imageURL = base_url().'backend_images/alignerco-logo.png';
             ?><span ><image src="<?=  $imageURL ?>" style="width: 200px;"></span><?php
          }
          if($domainName==STRAIGHT_MY_TEETH_DOMAIN)
          {
             $imageURL = base_url().'backend_images/smt-logo.png';
             ?><span ><image src="<?=  $imageURL ?>" style="width: 200px;"></span><?php
          }
          if($domainName==SMILEPATH_DOMAIN)
          {
             $imageURL = base_url().'backend_images/SMILEPATH-logo.png';
             ?><span ><image src="<?=  $imageURL ?>" style="width: 200px;"></span><?php
          }

          
          
     ?>

    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-power-off"></i>
             <span class="d-none d-sm-inline-block"><?php echo $userInfo['name']; ?></span>
            </a>
            
            <ul class="dropdown-menu scale-up">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url() ?>backend_images/user2-280x90.jpg" class="img-fluid" alt="User Image">

                <p>
                	<?php 
							
							if ($employeeAssignDepartment) {
									
										echo $employeeAssignDepartment;
							
							} else {
								
										echo NOT_AVAILABLE;	
							}
			
				     ?>
                  
                  <small><?php 
				  				if ($employeeAssignJobPosition) {
									
										echo $employeeAssignJobPosition;
							
							} else {
								
										echo NOT_AVAILABLE;	
							}
				  ?>
                </small>
                </p>
              </li>
              <li class="user-body">
                <div class="row">
                  <div class="col text-center">
                   <a href="#">Logged in as 
                   	<?php 
							
							if ($userInfo['designation']) {
									
										echo $userInfo['designation'];
							
							} else {
								
										echo NOT_AVAILABLE;	
							}
			
				   ?>
                   </a>
                  </div>
                 </div>
                <!-- /.row -->
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <div class="user-footer">
               
                <div class="pull-right">
                  <a href="<?php echo base_url(); ?>logout/" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </div>
            </ul>
            
            
          </li>
          
          <!-- Messages: style can be found in dropdown.less-->
          <!--<li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope"></i>
              <span class="label label-success">0</span>
            </a>
            <ul class="dropdown-menu scale-up">
              <li class="header">You have 0 messages</li>
            
              
            </ul>
          </li>-->
          <!-- Notifications: style can be found in dropdown.less -->
         <!-- <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell"></i>
              <span class="label label-warning">0</span>
            </a>
            <ul class="dropdown-menu scale-up">
              <li class="header">You have 0 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
             <!-- </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>-->
          <!-- Tasks: style can be found in dropdown.less -->
          <!--<li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag"></i>
              <span class="label label-danger">0</span>
            </a>
            <ul class="dropdown-menu scale-up">
              <li class="header">You have 0 tasks</li>
              
            </ul>
          </li>          -->
        </ul>
      </div>
    </nav>
  </header>
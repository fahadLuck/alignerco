<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="<?php echo base_url(); ?>backend_images/favicon.png"> -->

    <title>Portal - Log in </title>
  
	<!-- Bootstrap v4.0.0-beta -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/font-awesome/css/font-awesome.min.css">

	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/Ionicons/css/ionicons.min.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/master_style.css">

	<!-- maximum_admin Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/skins/_all-skins.css">	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

</head>

<?php
// to get current domain url
 $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

  $FullDomainName  = $_SERVER['HTTP_HOST'] . '/';
   $domainName     = $_SERVER['HTTP_HOST'] . '/';
   $domainName     = str_replace('https://','',$domainName);
   $domainName     = str_replace('/','',$domainName);

   if($domainName==ALIGNERCO_DOMAIN)
  {
     $wallpapers	 = array('1.jpg'); 
     $imageURL = base_url().'backend_images/alignerco-logo.png';
  }
  if($domainName==ALIGNERCO_CANADA_DOMAIN)
  {
     $wallpapers	 = array('1111.jpg'); 
     $imageURL = base_url().'backend_images/alignerco-logo.png';
  }
  if($domainName==STRAIGHT_MY_TEETH_DOMAIN)
  {
     $wallpapers	 = array('3.jpg');
     $imageURL = base_url().'backend_images/smt-logo.png'; 
  }
  if($domainName==SMILEPATH_DOMAIN)
  {
     $wallpapers	 = array('4.jpg'); 
     $imageURL = base_url().'backend_images/SMILEPATH-logo.png';
  }
  /*else
  {
     $wallpapers  = array('1.jpg'); 
     $imageURL = base_url().'backend_images/alignerco-logo.png';
  }*/


 
	  $k		  	 = array_rand($wallpapers);
	  $v 		  	 = $wallpapers[$k];
	
	  $wallpaperName = 'login_wallpapers/'.$v;
?>

<body class="hold-transition login-page" style="background: url(<?php echo base_url(); ?>backend_images/<?php echo $wallpaperName; ?>) center center no-repeat #d2d6de; background-size: cover;">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url(); ?>" style="color:#000;"><span ><image src="<?=  $imageURL ?>" style="width: 200px;"></span></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg has-error">Sign in to start your session</p>
    
     
    <?php  if ( validation_errors() !='' ) {
					
			      echo ' <p class="login-box-msg has-error">'.validation_errors().'</p>';
			} 
	?>
     
    <form action="<?php echo base_url()?>cpanel/" method="post" class="form-element" autocomplete="off">
      <div class="form-group has-feedback">
        <input name="signin_email" type="text" class="form-control" placeholder="user" value="<?php echo $this->input->post('signin_email'); ?>">
        <span class="ion ion-email form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="signin_password" type="password" class="form-control" placeholder="password">
        <span class="ion ion-locked form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-12 text-center">
          <button type="submit" class="btn btn-success btn-block btn-flat margin-top-10">SIGN IN</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    
    <!-- /.social-auth-links -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


	<!-- jQuery 3 -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.min.js"></script>
	
	<!-- popper -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap v4.0.0-beta -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>

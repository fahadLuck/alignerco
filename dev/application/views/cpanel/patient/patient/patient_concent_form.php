<script src='https://www.google.com/recaptcha/api.js'></script>

	<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Patient</li>
      
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           
           <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Successful';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
        <form id="frm" name="frm" action="<?php echo base_url(); ?>patient/" method="post" enctype="multipart/form-data" autocomplete="off">
	       
         

    	 <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-file"></i> Consent Form  </h3>
          </div>
      			
          <div class="box-body">
            <div class="row">
              <div class="col-12">

               
                   <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">1.  I have a bonded retainer:</label>
                    <div class="col-sm-2">
                      <input name="bonded_retainer" class="form-control" type="text" value="<?php echo $patient['bonded_retainer']; ?>" id="example-text-input" required>
                      <?php if (form_error('bonded_retainer')) { echo form_error('bonded_retainer'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">2.  I have bridgework:</label>
                    <div class="col-sm-2">
                      <input name="bridgework" class="form-control" type="text" value="<?php echo $patient['bridgework']; ?>" id="example-text-input" required>
                      <?php if (form_error('bridgework')) { echo form_error('bridgework'); } ?>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">3.  I have crowns:</label>
                    <div class="col-sm-2">
                      <input name="crowns" class="form-control" type="text" value="<?php echo $patient['crowns']; ?>" id="example-text-input" required>
                      <?php if (form_error('crowns')) { echo form_error('crowns'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">4.  I have an impacted tooth:</label>
                    <div class="col-sm-2">
                      <input name="impacted_tooth" class="form-control" type="text" value="<?php echo $patient['impacted_tooth']; ?>" id="example-text-input" required>
                      <?php if (form_error('impacted_tooth')) { echo form_error('impacted_tooth'); } ?>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">5.  I have an implant:</label>
                    <div class="col-sm-2">
                      <input name="implant" class="form-control" type="text" value="<?php echo $patient['implant']; ?>" id="example-text-input" required>
                      <?php if (form_error('implant')) { echo form_error('implant'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">6.  I have primary (baby) teeth:</label>
                    <div class="col-sm-2">
                      <input name="primary_teeth" class="form-control" type="text" value="<?php echo $patient['primary_teeth']; ?>" id="example-text-input" required>
                      <?php if (form_error('primary_teeth')) { echo form_error('primary_teeth'); } ?>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">7.  I have veneers:</label>
                    <div class="col-sm-2">
                      <input name="veneers" class="form-control" type="text" value="<?php echo $patient['veneers']; ?>" id="example-text-input" required>
                      <?php if (form_error('veneers')) { echo form_error('veneers'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">8.  I have a recent radiograph of my teeth:</label>
                    <div class="col-sm-2">
                      <input name="radiograph_of_teeth" class="form-control" type="text" value="<?php echo $patient['radiograph_of_teeth']; ?>" id="example-text-input" required>
                      <?php if (form_error('radiograph_of_teeth')) { echo form_error('radiograph_of_teeth'); } ?>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">9.  Do you feel pain in any of your teeth?</label>
                    <div class="col-sm-2">
                      <input name="pain_in_teeth" class="form-control" type="text" value="<?php echo $patient['pain_in_teeth']; ?>" id="example-text-input" required>
                      <?php if (form_error('pain_in_teeth')) { echo form_error('pain_in_teeth'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">10. Do you have any sores or lumps in or near your mouth?</label>
                    <div class="col-sm-2">
                      <input name="sores_or_lumps" class="form-control" type="text" value="<?php echo $patient['sores_or_lumps']; ?>" id="example-text-input" required>
                      <?php if (form_error('sores_or_lumps')) { echo form_error('sores_or_lumps'); } ?>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">11. During your visit to a dentist in the last 6 months, was there any indication of a serious dental issue identified?</label>
                    <div class="col-sm-2">
                      <input name="serious_dental_issue" class="form-control" type="text" value="<?php echo $patient['serious_dental_issue']; ?>" id="example-text-input" required>
                      <?php if (form_error('serious_dental_issue')) { echo form_error('serious_dental_issue'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">12. Do you currently have any head, neck, or jaw injuries?</label>
                    <div class="col-sm-2">
                      <input name="head_neck_jaw_injuries" class="form-control" type="text" value="<?php echo $patient['head_neck_jaw_injuries']; ?>" id="example-text-input" required>
                      <?php if (form_error('head_neck_jaw_injuries')) { echo form_error('head_neck_jaw_injuries'); } ?>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">13. Do you currently experience: jaw clicking, pain, difficulty opening and/or closing or difficulty chewing?</label>
                    <div class="col-sm-2">
                      <input name="jaw_clicking_pain_difficulty_opening_closing" class="form-control" type="text" value="<?php echo $patient['jaw_clicking_pain_difficulty_opening_closing']; ?>" id="example-text-input" required>
                      <?php if (form_error('jaw_clicking_pain_difficulty_opening_closing')) { echo form_error('jaw_clicking_pain_difficulty_opening_closing'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">14. Have you noticed any loosening of your teeth or do you have untreated periodontal disease?</label>
                    <div class="col-sm-2">
                      <input name="loosening_teeth" class="form-control" type="text" value="<?php echo $patient['loosening_teeth']; ?>" id="example-text-input" required>
                      <?php if (form_error('loosening_teeth')) { echo form_error('loosening_teeth'); } ?>
                    </div>
                  </div>


                   <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">15. Do you have any known allergies to any dental materials?</label>
                    <div class="col-sm-2">
                      <input name="allergies" class="form-control" type="text" value="<?php echo $patient['allergies']; ?>" id="example-text-input" required>
                      <?php if (form_error('allergies')) { echo form_error('allergies'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">16. I have a history of IV bisphosphonate treatment:</label>
                    <div class="col-sm-2">
                      <input name="bisphosphonate_treatment" class="form-control" type="text" value="<?php echo $patient['bisphosphonate_treatment']; ?>" id="example-text-input" required>
                      <?php if (form_error('bisphosphonate_treatment')) { echo form_error('bisphosphonate_treatment'); } ?>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">17. I am currently on acute corticosteroids or in immunosuppression, chemotherapy, or radiation of head / neck:</label>
                    <div class="col-sm-2">
                      <input name="acute_corticosteroids" class="form-control" type="text" value="<?php echo $patient['acute_corticosteroids']; ?>" id="example-text-input" required>
                      <?php if (form_error('acute_corticosteroids')) { echo form_error('acute_corticosteroids'); } ?>
                    </div>

                    <div class="col-sm-2"></div>

                    <label for="" class="col-sm-3 col-form-label">18. I have had a bone marrow transplant or treatment of hematological malignancies (blood cancers) within the past 2 years:</label>
                    <div class="col-sm-2">
                      <input name="bone_marrow_transplant" class="form-control" type="text" value="<?php echo $patient['bone_marrow_transplant']; ?>" id="example-text-input" required>
                      <?php if (form_error('bone_marrow_transplant')) { echo form_error('bone_marrow_transplant'); } ?>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">19. How old the patient is?</label>
                    <div class="col-sm-2">
                      <input name="patient_age" class="form-control" type="text" value="<?php echo $patient['patient_age']; ?>" id="example-text-input" required>
                      <?php if (form_error('patient_age')) { echo form_error('patient_age'); } ?>
                    </div>
                  </div>


                  <br><br><br>


                  <div class="form-group row">
                    <div class="col-sm-12">
                      <p>I hereby give my consent to get teledentistry treatment from ALIGNERCO. By signing this consent, I certify that during last visit to my dentist within the past six months no serious dental issue was identified, e.g. gum recession, inflamed gums, or tooth mobility etc. I also do not have periodontal or gum disease.<br><br>

You may experience discomfort/ pain (pain has no definition, it is a perception and depends upon an individual that how he / she perceives it), and a lisp at the beginning of the treatment / when you change to the next step of the aligners. The lisp or discomfort will disappear in a few days. You may also experience some irritation when you start with aligner treatment, while your mouth adjusts with the aligners.<br>
In the beginning of your treatment you may experience temporary changes in your speech that is due to the presence of the aligner in your mouth but it settles down as you get along with the aligner treatment.<br>
The person of contact during your aligner treatment will be your assigned Smile Crew Member. You may contact him / her with any concerns and it will be forwarded to the dentist assigned to your treatment. The Smile Crew Member will get back to you with the response from your assigned dentist.<br>
During the aligner treatment with ALIGNERCO this may occur that you are experiencing jaw pain / jaw clicking causing headache. In this case, do not wide open your mouth so it can settle down; if problem persists please see your local dentist. <br><br>
During the course of orthodontic treatment there can be pre-mature contacts as teeth will be moving to the new position which may cause some of the teeth to be out of contact temporarily, this settles down as treatment progresses / at later stages of the treatment, depending on each case.<br>  
When teeth which overlapped initially are aligned a black triangle may appear, which can be referred as a gap below the interproximal contact, since gingival (soft tissue) wasn't there before. After the treatment, it may take some time depending on multiple factors like oral hygiene etc, that gingival may grow and cover the black triangle. The treatment preview created by ALIGNERCO may not project the formation of black triangles in the smile simulation.<br>
Aligner treatment is designed to work in stages. Teeth shift step by step. The teeth shifting process occurs as per pre-designed orthodontic movements by applying precise force to achieve the desired movement on each particular step. Every step is designed to move your teeth to a certain extent, in some cases in order to align teeth we may need to create some room (gaps between teeth) to achieve the movement, which eventually will be closed once you are moving to the advanced stages. I also understand, the spaces which will be created during the aligner treatment may be visible or teeth may show misalignment but it will be as per the pre-determined treatment plan that leads toward the final outcome of the treatment eventually.<br> 
I understand ALIGNERCO designs each treatment plan with diligence and aligners are fabricated to achieve the contiguous results that will be projected in the treatment preview with a smile projection although, final outcome of the orthodontic treatment depends on multiple factors including but not limited to having followed stipulated instructions, medication an individual is on, bone density etc. Eighty to ninety percent of the projection is estimated to be achieved if having followed all the stipulated instructions and if maintaining good oral hygiene.<br> 
During the aligner treatment if you do not maintain good oral hygiene (if you do not brush & floss before inserting the aligners after you had a drink or eaten anything) or if you eat food and drink beverages that are high in sugar content, cavities, gingival recession, tooth decay, or inflammation of the gums may appear or accelerate. Furthermore, in some instances discoloration or white spots may occur; small cavities may increase in size, causing sensitivity and, in some cases, pain or tooth breakage; causing soreness and/or bleeding. If any of these occurs you may have to discontinue the aligner treatment if advised by your local dentist.<br><br>
ALIGNERCO uses medically approved biodegradable material to fabricate aligners that is BPA free and is 100% safe. If you are allergic to any dental material, aligners may cause a reaction but this is highly unlikely. If you experience any reaction, contact your doctor immediately.
Although ALIGNERCO aligners are made out of the material approved for aligners manufacturing but still aligner may break. In case you have swallowed a piece of it, you may experience an allergic reaction. 
Fake teeth e.g. implants and bridges can’t be shifted with ALIGNERCO aligner treatment. Furthermore, any dental work, fillings, crowns, veneers, and composite bondings may require to be restored due to teeth movement and alignerco offers no coverage for such expenses.
Any remaining substances from previous orthodontic treatment e.g. buttons or fixed retainer (lingual wire) should be removed prior to making teeth impressions.<br><br>

My signature below means that I have read and understand this consent form; I agree to everything explained above.
.<br><br>
</p>
                    
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Patient’s Name: </label>
                    <div class="col-sm-10">
                      <input name="patient_name" class="form-control" type="text" value="<?php echo $patient['patient_name']; ?>" id="example-text-input" required>
                      <?php if (form_error('patient_name')) { echo form_error('patient_name'); } ?>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Date Signed: </label>
                    <div class="col-sm-10">
                      <input name="date_signed" class="form-control" type="date" value="<?php echo $patient['date_signed']; ?>" id="example-text-input" required>
                      <?php if (form_error('date_signed')) { echo form_error('date_signed'); } ?>
                    </div>
                </div>


                <div class="form-group row">
                	<div class="col-sm-2"></div>
                    <label for="" class="col-sm-10 col-form-label"><b>If patient is not able to consent (is under the age of 18), complete the following:</b></label>
                </div>

                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Legally Responsible Person: </label>
                    <div class="col-sm-10">
                      <input name="legally_responsible_person" class="form-control" type="text" value="<?php echo $patient['legally_responsible_person']; ?>" id="example-text-input">
                      <?php if (form_error('legally_responsible_person')) { echo form_error('legally_responsible_person'); } ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Relationship to patient: </label>
                    <div class="col-sm-10">
                      <input name="relationship_to_patient" class="form-control" type="text" value="<?php echo $patient['relationship_to_patient']; ?>" id="example-text-input">
                      <?php if (form_error('relationship_to_patient')) { echo form_error('relationship_to_patient'); } ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Date Signed: </label>
                    <div class="col-sm-10">
                      <input name="underage_date_signed" class="form-control" type="date" value="<?php echo $patient['underage_date_signed']; ?>" id="example-text-input">
                      <?php if (form_error('underage_date_signed')) { echo form_error('underage_date_signed'); } ?>
                    </div>
                </div>


                    </div>
                  </div>



                <!--   <br><br>

                  <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                      <img src="<?= base_url() ?>backend_images/concent_image.png" alt="">
                    </div>
                  </div>

                  <br><br>

                   <div class="form-group row">
                      <div class="col-sm-2"></div>
                      <b>Note: Your right is left in the picture and vice versa.</b>
                  </div> -->

                
                 
             
              </div>
            </div>
          </div>
    	 </div>



          
    	</form>      
		
        </div>
      </div>
     
    </section>
   
   
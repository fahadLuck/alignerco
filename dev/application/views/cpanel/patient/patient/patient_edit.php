
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Patient</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           
           <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
        <form id="frm" name="frm" action="<?php echo base_url(); ?>patient-edit/<?php echo $tableID; ?>" method="post" enctype="multipart/form-data" autocomplete="off">

          <input type="hidden" name="tableID" value="<?php echo $tableID;  ?>">
	       
           <?php 
							  	
				  $employeeID 	= $patient['employeeID'];
				  $employeeName	= $patient['employeeName'];
												  
				  $employeeCode 	= $patient['employeeCode'];
          $username       = $patient['username'];
          $email          = $patient['email'];
          $CaseID         = $patient['CaseID'];
												  
													  
				  $employeeRolesArray 	= array();
	  
					  if ($employeeRoles) {
		  
							foreach($employeeRoles->result() as $employeeRole) {
										
								$employeeRolesArray[]  =  $employeeRole->roleID;
							}
					   }
			?>
          
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-pencil"></i> Update Patient Account </h3>
          </div>
      			
          <div class="box-body">
            <div class="row">
              <div class="col-12">

                <input type="hidden" name="family_account" value="<?= $patient['family_account'] ?>">

                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">CaseID</label>
                    <div class="col-sm-10">
                      <input name="CaseID" class="form-control" type="text" value="<?php if(isset($patient) and $submit==''){ echo $patient['CaseID']; } if(isset($submit) AND $patient){ echo $this->input->post('CaseID'); } ?>" id="example-text-input">
                      <?php if (form_error('CaseID')) { echo form_error('CaseID'); } ?>
                    </div>
                  </div>

                  


                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input name="email" class="form-control" type="text" value="<?php if(isset($patient) and $submit==''){ echo $patient['email']; } if(isset($submit) AND $patient){ echo $this->input->post('email'); } ?>" id="example-text-input">
                      <?php if (form_error('email')) { echo form_error('email'); } ?>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">UserName</label>
                    <div class="col-sm-10">
                      <input name="username" class="form-control" type="text" value="<?php echo $username; ?>" id="example-text-input">
                      <?php if (form_error('username')) { echo form_error('username'); } ?>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                      <input name="password" class="form-control" type="password" value="<?php echo $this->input->post('name'); ?>" id="example-text-input">
                      <?php if (form_error('password')) { echo form_error('password'); } ?>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Confirm password</label>
                    <div class="col-sm-10">
                      <input name="confirm_password" class="form-control" type="password" value="<?php echo $this->input->post('confirm_password'); ?>" id="example-text-input">
                      <?php if (form_error('confirm_password')) { echo form_error('confirm_password'); } ?>
                    </div>
                  </div>
                  
                  
                  
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                      <select name="account_status" id="account_status" class="form-control select2">
                        <option value="<?php echo HARD_CODE_ID_ACTIVATED; ?>" <?php if ($patient['officialAccountStatus'] == HARD_CODE_ID_ACTIVATED) { echo 'selected=selected'; } ?>><?php echo getStatusName(HARD_CODE_ID_ACTIVATED);?></option>
						<option value="<?php echo HARD_CODE_ID_INACTIVE; ?>" <?php if ($patient['officialAccountStatus'] == HARD_CODE_ID_INACTIVE) { echo 'selected=selected'; } ?>><?php echo getStatusName(HARD_CODE_ID_INACTIVE);?></option>
                        <option value="<?php echo HARD_CODE_ID_BLOCKED; ?>" <?php if ($patient['officialAccountStatus'] == HARD_CODE_ID_BLOCKED) { echo 'selected=selected'; } ?>><?php echo getStatusName(HARD_CODE_ID_BLOCKED);?></option>
 	                  </select>
                      <?php if (form_error('account_status')) { echo form_error('account_status'); } ?>
                    </div>
                  </div>
                  
                  <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
				  <div class="col-sm-10 offset-md-2">
					 <button type="submit" name="submit" value="Save" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
                     <button type="button" class="btn btn-warning " onclick="window.location.href='<?php echo base_url(); ?>patient-list/'">Cancel</button>
				  </div>
				</div>
             
              </div>
            </div>
          </div>
 
    	 </div>
          
    	</form>      
		
        </div>
      </div>
     
    </section>
   
   
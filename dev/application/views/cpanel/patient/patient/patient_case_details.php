<!-- jQuery 3 -->

<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> -->
 <!--  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
 <script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>
  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>


  <script>
      $(document).ready(function() 
      {

        if ($('.setup_link').is(':checked')) 
          {
            $('#steps_form').show(1000);
          }

        $('.setup_link').click(function() {

          if ($(this).is(':checked')) 
          {
             $('#steps_form').show(1000);
          }
          else
          {
            $('#steps_form').hide(1000);
          }

        });

    });
</script>


 <?php
         if ($this->session->userdata('admin_msg') !='') {
          
            $alertClass   = 'alert-info';
            $alertHeading   = 'Success';   
            $alertMessage = $this->session->userdata('admin_msg');
            
            $alertIcon    = 'icon fa fa-check';    
         
         } else if ($this->session->userdata('admin_msg_error')!='') {
        
             $alertClass    = 'alert-danger';  
             $alertHeading  = 'ERROR';  
             $alertMessage  = $this->session->userdata('admin_msg_error');
             
             $alertIcon     = 'icon fa fa-ban'; 
         }
        
         if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
    ?>
          <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                <?php echo $alertMessage; ?>
                          </div>
      
    <?php 
          
            $this->session->unset_userdata('admin_msg');
            $this->session->unset_userdata('admin_msg_error');
          
          } 
  ?>

  
<form id="" name="frm" action="<?php echo base_url(); ?>patient-case_status/" method="post" enctype="multipart/form-data" autocomplete="off">

<div class="row">
       <div class="col-12 table-responsive">
          <br />
         <p class="lead" style="color:#000;"><b>Case Details</b></p>
         <div class="table-responsive">
         <table class="table table-striped" style="width:100%">
           <thead>
            <tr>
              <th width="">#</th>

             
              <th width="">&nbsp;</th>
              <th width="">Link</th>
              <th width="">Code</th>
             </tr>
           </thead>
           
           <tbody>
           
            <?php 
          
        if($caseSetups->num_rows() > 0) {
            
            $counter = 1;
            foreach($caseSetups->result() as $setup) {
              
              $setup_upload_data_id      = $setup->setup_upload_data_id;
              $case_Link                 = $setup->upload_link;
              $caseLink                  = $setup->upload_link;
              $caseLink                  = str_replace('https://','',$caseLink);
              $caseLink                  = 'https://'.$caseLink;
              $caseLink                  = trim($caseLink);
              $caseLink                  = str_replace(' ','',$caseLink);
              $caseCode                  = $setup->password;

          ?>
                        
                       <tr <?php if ($counter == 1) { ?> style="background-color:#cbe091; color:#000;" <?php } ?>>
                        <td <?php if ($counter == 1) { ?> style="color:#000;" <?php } ?>><small style="font-size:13px;"><?php echo $counter; ?></small></td>

                            <input type="hidden" name="list_case[]" value="<?= encodeString($caseID); ?>">
                           <!--  <input type="hidden" name="link" value="<?= $caseLink ?>">
                            <input type="hidden" name="code" value="<?= $caseCode; ?>"> -->
                            
                   
                        <td>
                          <input type='checkbox' name='setup_upload_data_id' class="setup_link" value="<?php echo $setup_upload_data_id; ?>" id="setup_link_<?php echo $counter; ?>" <?php if(isset($patientCaseStatusHistory) and $patientCaseStatusHistory['setup_upload_data_id'] == $setup_upload_data_id) { echo 'checked="checked" '; } ?>>
                            <label for="setup_link_<?php echo $counter; ?>"></label>
                        </td>
                       

                        <td>
                          <a href="<?php echo $caseLink; ?>">
                            <small style="font-size:10px;"><?php echo $caseLink; ?></small>
                            </a>
                        </td>

                        <td><?php echo $caseCode; ?></small></td>

                        <!-- <td><small style="font-size:14px;">Case <?= $doctorCaseStatusName ?> by <?php echo $CreatedName; ?> (<?php echo $CreatedCode; ?>)</small>
                        </td> -->

                        

                        <!-- Modal -->
                        <!-- <?php
                        if($counter==1 AND $caseStatusID==MODIFIED)
                        {
                          ?><div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                            
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Reply For Modification</h4>
                                </div>
                                <div class="modal-body">
                                  <form method="post" action="<?= base_url() ?>modification-reply">
                                    <input type="hidden" name="list_case[]" value="<?= encodeString($caseID); ?>">
                                    <input type="hidden" name="code" value="<?= $caseCode ?>">
                                    <input type="hidden" name="link" value="<?= $caseLink ?>">
                                    <input type="hidden" name="caseStatusID" value="<?= HARD_CODE_ID_STATUS_REPLY ?>">

                                    <textarea id="summernote1" name="comment">
                                       
                                    </textarea>

                                    <button style="float: right; margin-top: 10px;" type="submit" class="btn btn-info">Reply
                                      <i style="font-weight: bold;" class="fa fa-reply" aria-hidden="true" ></i>
                                    </button>
                                  </form>
                                </div>
                                
                              </div>
                              
                            </div>
                          </div><?php
                      }
                      ?> -->

                       <!-- Modal -->


                      </tr>



             
             <?php
          $counter++;
          
            }
          
        } else {
      ?>
              
               <tr>
                  <td colspan="7">No history found.</td>
               </tr>
            
           <?php } ?>
           
           </tbody>
         </table>
       </div>
          
        </div>
        <!-- /.col -->
      </div>



       <div class="box box-default" id="steps_form" style="display: none;">
        <div class="box-header with-border">
          <h3 class="box-title">Case Status </h3>
        </div>
        <!-- /.box-header -->
      
      
        <div id='waiting_checkboxes'></div>
        <div class="box-body">
          <div class="row">

           


            <div class="col-md-6 col-6">
              <div class="form-group">
                <label>Case Status</label>
                <select name="caseStatusID" id="caseStatusID" class="form-control select2" style="width: 100%;" required="">
                      <option value="">Select Status</option>
                          <?php 
                                if ($caseStatus->num_rows() > 0) 
                                {
                                        
                                    foreach($caseStatus->result() as $status) 
                                    {
                                      
                                      $caseStatusID  = $status->id;
                                      $caseName  = $status->name;
                                      if($caseStatusID !=ASSIGNED AND $caseStatusID !=PENDING)
                                      {
                                        
                                        if ($this->input->post('doctor') == $caseStatusID) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } 

                                        elseif ($patientCaseStatusHistory['status_id'] == $caseStatusID) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } 

                                        else {
                                            
                                            $selectedService = NULL;    
                                        }
                                          
                                          echo '<option value="'.$caseStatusID.'" '.$selectedService.'>'.$caseName.'</option>';
                                      } 
                                  }
                                }
                           ?>                 
                      
                   </select>
              </div>
            </div>

            


            <!-- <div class="col-md-4 col-6" id="comment" style="display: none;">
              <div class="form-group">
                <label>Comment</label>
                  <textarea id="comment" name="comment" rows="9" cols="60">
                   <?php if(isset($case_data)){ echo trim($case_data->caseComment); } ?>   
                  </textarea>
              </div>
            </div>  -->

            <div class="col-md-6 col-6" id="comment" >
              <div class="form-group">
                <label>Comment</label>
                   <!-- <div id="summernote" style="display: none;"><p><?php if(isset($case_data)){ echo trim($case_data->caseComment); } ?></p></div> -->

                   <textarea id="summernote" name="comment" >
                   <?php if(isset($patientCaseStatusHistory)){ echo trim($patientCaseStatusHistory['comment']); } ?>   
                  </textarea>

              </div>
            </div> 


          </div>
      
           <div class="pull-right">
              <button type="submit" id="save-btn-area" type="button" onClick="retun abc();" class="btn btn-blue pull-right"><i class="fa fa-send"></i>Update Status</button>
           </div> 
           
        </div>
  </div>

   </form>   




   <div class="row">
       <div class="col-12 table-responsive">
          <br />
         <p class="lead" style="color:#000;"><b>Patient Approval/Modification History</b></p>
         <div class="table-responsive">
         <table class="table table-striped" style="width:100%">
           <thead>
            <tr>
              <th width="">#</th>
              <th width="">Link</th>
              <th width="">Code</th>
              <th width="">status</th>
              <th width="">comment</th>
              <th width="">Date</th>
             </tr>
           </thead>
           
           <tbody>
           
            <?php 
          
        if($patientCaseStatusHistoryQuery->num_rows() > 0) {
            
            $counter = 1;
            foreach($patientCaseStatusHistoryQuery->result() as $statusHistory) {
              
              $statusHistory_setup_upload_data_id      = $statusHistory->setup_upload_data_id;
              $statusHistory_case_Link                 = $statusHistory->link;
              $statusHistory_caseLink                  = $statusHistory->link;
              $statusHistory_caseLink                  = str_replace('https://','',$statusHistory_caseLink);
              $statusHistory_caseLink                  = 'https://'.$statusHistory_caseLink;
              $statusHistory_caseLink                  = trim($statusHistory_caseLink);
              $statusHistory_caseLink                  = str_replace(' ','',$statusHistory_caseLink);
              $statusHistory_caseCode                  = $statusHistory->code;
              $statusHistory_comment                   = $statusHistory->comment;
              $statusHistory_receive_date              = $statusHistory->receive_date;

              $statusHistory_status_id                 = $statusHistory->status_id;
              if($statusHistory_status_id==MODIFIED)
              {
                $historyStatus                        = 'Modification';
              }
              if($statusHistory_status_id==APPROVED)
              {
                $historyStatus                        = 'APPROVED';
              }

              $created                                 = $statusHistory->created;
              $createdDate                             = date('d M. Y',$created);
              $createdTime                             = date('h:i A',$created);
              $createdTimeAgo                          = timeAgo($created).' ago';

          ?>
                        
                       <tr <?php if ($counter == 1) { ?> style="background-color:#cbe091; color:#000;" <?php } ?>>
                        <td <?php if ($counter == 1) { ?> style="color:#000;" <?php } ?>><small style="font-size:13px;"><?php echo $counter; ?></small></td>
                       

                        <td>
                          <a href="<?php echo $statusHistory_caseLink; ?>">
                            <small style="font-size:10px;"><?php echo $statusHistory_case_Link; ?></small>
                            </a>
                        </td>

                        <td><?php echo $statusHistory_caseCode; ?></small></td>
                        <td><?php echo $historyStatus; ?></small></td>
                        <td><?php echo $statusHistory_comment; ?></small></td>
                        <td><?php echo $createdDate; ?> <?php echo $createdTime; ?> (<?php echo $createdTimeAgo; ?>)</td>

                        <!-- <td><small style="font-size:14px;">Case <?= $doctorCaseStatusName ?> by <?php echo $CreatedName; ?> (<?php echo $CreatedCode; ?>)</small>
                        </td> -->

                        

                        <!-- Modal -->
                        <!-- <?php
                        if($counter==1 AND $caseStatusID==MODIFIED)
                        {
                          ?><div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                            
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Reply For Modification</h4>
                                </div>
                                <div class="modal-body">
                                  <form method="post" action="<?= base_url() ?>modification-reply">
                                    <input type="hidden" name="list_case[]" value="<?= encodeString($caseID); ?>">
                                    <input type="hidden" name="code" value="<?= $caseCode ?>">
                                    <input type="hidden" name="link" value="<?= $caseLink ?>">
                                    <input type="hidden" name="caseStatusID" value="<?= HARD_CODE_ID_STATUS_REPLY ?>">

                                    <textarea id="summernote1" name="comment">
                                       
                                    </textarea>

                                    <button style="float: right; margin-top: 10px;" type="submit" class="btn btn-info">Reply
                                      <i style="font-weight: bold;" class="fa fa-reply" aria-hidden="true" ></i>
                                    </button>
                                  </form>
                                </div>
                                
                              </div>
                              
                            </div>
                          </div><?php
                      }
                      ?> -->

                       <!-- Modal -->


                      </tr>



             
             <?php
          $counter++;
          
            }
          
        } else {
      ?>
              
               <tr>
                  <td colspan="7">No history found.</td>
               </tr>
            
           <?php } ?>
           
           </tbody>
         </table>
       </div>
          
        </div>
        <!-- /.col -->
      </div>


  <script>
    $(document).ready(function() {
        $('#summernote').summernote({
      height: 150,   //set editable area's height
      codemirror: { // codemirror options
        theme: 'monokai'
      }
    });
        
    });
  </script>

  <script>
    $(document).ready(function() {
        $('#summernote1').summernote({
      height: 150,   //set editable area's height
      codemirror: { // codemirror options
        theme: 'monokai'
      }
    });
        
    });
  </script>
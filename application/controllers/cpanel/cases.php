<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow');
error_reporting(0);

class Cases extends CI_Controller {

	public function __construct() {

		parent::__construct();

		/* Table ID*/
		$this->accountID 	    = $this->session->userdata(USER_ACCOUNT_ID_SESSION);

		/*Team Member ID*/
		$this->teamID   	    = $this->session->userdata(USER_TEAM_ID_SESSION);

		/*Team Member ID*/
		$this->HRID  		    = $this->session->userdata(USER_HR_ID_SESSION);

		/*Team Member ID*/
		$this->organizationID   = $this->session->userdata(USER_ORGANIZATION_ID_SESSION);

		$this->load->helper('cpanel/application/application');
		$this->load->model('cpanel/application/model_application');

		$this->load->helper('cpanel/employees/employee');
		$this->load->model('cpanel/employees/model_employee');


		$this->load->helper('cpanel/cases/case');
		$this->load->model('cpanel/cases/model_case');
		/*$this->load->model('Connection_model');*/

		authentication(); // Calling From Login Helper

		/*Use For Access Permissions*/
		$this->assignedRoles 		= userAssignedRolesIDs(); // Calling From Application Helper
		$this->accessModules 		= userModuleAccessPremissions(); // Calling From Application Helper

		$this->caseStatusReadyArray     	= array(
													CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY,
													CASE_STATUS_KIT_TO_CUSTOMER_IN_TRANSIT,
													CASE_STATUS_IMPRESSIONS_TO_NY_READY,
													CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_READY,
													CASE_STATUS_READY_FOR_UPLOADING_READY,
													CASE_STATUS_WAITING_FOR_APPROVAL_WAITING,
													CASE_STATUS_PRODUCTION_TO_NY_READY,
													CASE_STATUS_ALIGNERS_TO_CUSTOMER_READY
													);

		$this->caseStatusTransitArray    	= array(
													CASE_STATUS_IMPRESSIONS_TO_NY_IN_TRANSIT,
													CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT,
													CASE_STATUS_PRODUCTION_TO_NY_IN_TRANSIT,
													CASE_STATUS_ALIGNERS_TO_CUSTOMER_IN_TRANSIT
													);

		$this->caseStatusProcessArray    	= array();

		$this->caseStatusHoldArray    		= array();

		$this->caseStatusOFFHoldArray    	= array();

		$this->caseStatusModificationArray  = array(
													CASE_STATUS_WAITING_FOR_APPROVAL_MODIFICATION
													);

		$this->caseStatusDoneArray  	 	= array(
											 		CASE_STATUS_KIT_SHIP_TO_CUSTOMER_SHIPPED,
													CASE_STATUS_KIT_TO_CUSTOMER_RECEIVED,
													CASE_STATUS_IMPRESSIONS_TO_NY_RECEIVED,
													CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_RECEIVED,
													CASE_STATUS_READY_FOR_UPLOADING_DONE,
													CASE_STATUS_WAITING_FOR_APPROVAL_APPROVED,
													CASE_STATUS_PRODUCTION_TO_NY_RECEIVED,
													CASE_STATUS_ALIGNERS_TO_CUSTOMER_RECEIVED
												    );

		$this->caseStatusRejectArray  		= array();

		$this->caseStatusUpdateArray  = array(
													CASE_STATUS_UPDATE_CASE_DETAILS
													);

	}

	function convertDate()
	{
		$result	   				= $this->model_shared->getAllRecords('*',MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE);
		foreach($result->result() as $row)
		{
			$created               = $row->created;
			$data['receive_date']  = date('Y-m-d',$created);

			$this->model_shared->editRecordWhere(array('created' => $created),MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE,$data);
		}

		$result_2	   				= $this->model_shared->getAllRecords('*',MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE);
		foreach($result_2->result() as $row_2)
		{
			$created_2               = $row_2->created;
			$data_2['receive_date']  = date('Y-m-d',$created_2);

			$this->model_shared->editRecordWhere(array('created' => $created_2),MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data_2);
		}
	}

	function caseAdd() {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_ADD_NEW,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 			= $this->assignedRoles;
		$accessModules 			= $this->accessModules;

		/* Set Form Validation Errors */
		$this->form_validation->set_rules('patient_name','patient name','trim|required');
		/*$this->form_validation->set_rules('age','patient age','trim|required');
		$this->form_validation->set_rules('gender','gender','trim|required');*/
		$this->form_validation->set_rules('patient_country','patient country','trim');
		$this->form_validation->set_rules('patient_state','patient state','trim');
		$this->form_validation->set_rules('patient_city','patient city','trim');
		$this->form_validation->set_rules('patient_zip','patient zip','trim');
		$this->form_validation->set_rules('patient_email','patient email','trim');
		$this->form_validation->set_rules('patient_phone','patient phone','trim');

		$this->form_validation->set_rules('patient_address_1','address 1','trim');
		$this->form_validation->set_rules('patient_address_2','address 2','trim');
		$this->form_validation->set_rules('patient_address_3','address 3','trim');

		$archFlag = false;

		if ($this->input->post('arch_upper') == HARD_CODE_ID_YES) {

					$archFlag = true;
		}

		if ($this->input->post('arch_lower') == HARD_CODE_ID_YES) {

					$archFlag = true;
		}

		if ($archFlag == false) {

			$this->form_validation->set_rules('arch','arach upper or lower','trim|required');
		}


 		$this->form_validation->set_rules('bite','bite','trim');
		$this->form_validation->set_rules('impressions_workable_upper','workable upper','trim');
		$this->form_validation->set_rules('impressions_workable_lower','workable lower','trim');
		$this->form_validation->set_rules('transaction','transaction mode','trim|required');
		$this->form_validation->set_rules('receive_date','receive date','trim|required');
		$this->form_validation->set_rules('country','country','required');
		$this->form_validation->set_rules('company','distributor','required');

		$this->form_validation->set_rules('file_data_rx','','trim|callback_check_case_RX_form');
		$this->form_validation->set_rules('file_data_x_ray_ceph','','trim|callback_check_x_ray_opg');
		$this->form_validation->set_rules('file_data_x_ray_ceph','','trim|callback_check_x_ray_ceph');

		$this->form_validation->set_rules('priority','priority','trim');
		$this->form_validation->set_rules('airway_bill_number','airway_bill_number','trim');
		$this->form_validation->set_rules('comments','comments','trim');

		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');

			if($this->form_validation->run() === FALSE ) {

				  $accountID								= $this->accountID;
				  $teamID									= $this->teamID;
				  $organizationID 							= $this->organizationID;

				  $userInfo 								= userInfo($accountID); // Calling From Application Helper

				  $impressionTypes							= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_IMPRESSION_TYPES_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');
				  $doctors									= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DOCTORS_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');
				  $countries	   							= $this->model_shared->getRecordMultipleWhereOrderBy('*',COUNTRIES_TABLE,array('id !=' => 0),'name','ASC');
				  $cities	   								= $this->model_shared->getRecordMultipleWhereOrderBy('*',CITIES_TABLE,array('id !=' => 0),'name','ASC');
				  $companies								= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_COMPANIES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');

				  $diseases									= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DISEASES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'order','ASC');


				  $result['assignedRoles']					= $assignedRoles;
				  $result['accessModules']					= $accessModules;

				  $result['impressionTypes']  				= $impressionTypes;
				  $result['doctors']  						= $doctors;
				  $result['countries']  					= $countries;
				  $result['states']          				=   getAllStates(); //  Calling From
				  $result['cities']  						= $cities;
				  $result['companies']  					= $companies;

				  $result['diseases']  						= $diseases;

				  $data['userInfo']            				= $userInfo;

				  $data['activeMenu']  						= '4';
				  $data['activeSubMenu']  					= '4.1';

				  $data['pageHeading']    					= "<i class='fa fa-medkit'></i> Case <small>(New)</small>";
				  $data['subHeading']    					= "";

				  $data['metaType']     					= 'internal';
				  $data['pageTitle']      					= 'New Case | '.DEFAULT_APPLICATION_NAME;

				  $data['contents']							= $this->load->view('cpanel/cases/case_add',$result,true);
				  $this->load->view('cpanel/template',$data);

			} else {

								// Prepair Date To Store In Database
								$patientName				    			= ($this->input->post('patient_name') ?: NULL);
								$age				    					= ($this->input->post('age') ?: NULL);
								$gender				    					= ($this->input->post('gender') ?: NULL);
								$patientCountry				    			= ($this->input->post('patient_country') ?: NULL);
								$patientState				    			= ($this->input->post('patient_state') ?: NULL);
								$patientCity				    			= ($this->input->post('patient_city') ?: NULL);
								$patientZipCode				    			= ($this->input->post('patient_zip_code') ?: NULL);
								$patientEmail				    			= ($this->input->post('patient_email') ?: NULL);
								$patientPhone				    			= ($this->input->post('patient_phone') ?: NULL);

								$patientAddress_1				    		= ($this->input->post('patient_address_1') ?: NULL);
								$patientAddress_2				    		= ($this->input->post('patient_address_2') ?: NULL);
								$patientAddress_3				    		= ($this->input->post('patient_address_3') ?: NULL);

								$treatmentType				    			= ($this->input->post('treatment_type') ?: NULL);


								$impression				   					= CASE_IMPRESSION_TYPE_PHYSICAL;
								$archUpper				   					= ($this->input->post('arch_upper') ?: NULL);
								$archLower				   					= ($this->input->post('arch_lower') ?: NULL);
								$impressionsWorkableUpper				 	= ($this->input->post('impressions_workable_upper') ?: NULL);
								$impressionsWorkableLower				 	= ($this->input->post('impressions_workable_lower') ?: NULL);

								$modePurchase				 				= ($this->input->post('transaction') ?: NULL);

								$receiveDate				 				= ($this->input->post('receive_date') ?: NULL);
								$country			 						= ($this->input->post('country') ?: NULL);
								$city				 						= ($this->input->post('city') ?: NULL);
								$company				 					= ($this->input->post('company') ?: NULL);

								$looseCrowns								= ($this->input->post('medical_history_loose_crowns') ?: NULL);

								$LDC_missingTeeth							= $this->input->post('LDC_missing_teeth');

		 						$LDC_TMQ1									= $this->input->post('LDC_TMQ1');
		 			 			$LDC_TMQ2									= $this->input->post('LDC_TMQ2');
		  						$LDC_TMQ3									= $this->input->post('LDC_TMQ3');
		 						$LDC_TMQ4									= $this->input->post('LDC_TMQ4');

								$fillings									= ($this->input->post('medical_history_fillings') ?: NULL);

								$FILLING_missingTeeth						= $this->input->post('FILLING_missing_teeth');

		 						$FILLING_TMQ1								= $this->input->post('FILLING_TMQ1');
		 			 			$FILLING_TMQ2								= $this->input->post('FILLING_TMQ2');
		  						$FILLING_TMQ3								= $this->input->post('FILLING_TMQ3');
		 						$FILLING_TMQ4								= $this->input->post('FILLING_TMQ4');

								$implant									= ($this->input->post('medical_history_implant') ?: NULL);

								$IMPLANT_missingTeeth						= $this->input->post('IMPLANT_missing_teeth');

		 						$IMPLANT_TMQ1								= $this->input->post('IMPLANT_TMQ1');
		 			 			$IMPLANT_TMQ2								= $this->input->post('IMPLANT_TMQ2');
		  						$IMPLANT_TMQ3								= $this->input->post('IMPLANT_TMQ3');
		 						$IMPLANT_TMQ4								= $this->input->post('IMPLANT_TMQ4');

								$undertakeRootCanalProcedure				= ($this->input->post('medical_history_undertake_root_canal_procedure') ?: NULL);

								$URCP_missingTeeth							= $this->input->post('URCP_missing_teeth');

		 						$URCP_TMQ1									= $this->input->post('URCP_TMQ1');
		 			 			$URCP_TMQ2									= $this->input->post('URCP_TMQ2');
		  						$URCP_TMQ3									= $this->input->post('URCP_TMQ3');
		 						$URCP_TMQ4									= $this->input->post('URCP_TMQ4');


								$wearVeneers								= ($this->input->post('medical_history_wear_veneers') ?: NULL);

								$WV_missingTeeth							=  $this->input->post('WV_missing_teeth');

		 						$WV_TMQ1									=  $this->input->post('WV_TMQ1');
		 			 			$WV_TMQ2									=  $this->input->post('WV_TMQ2');
		  						$WV_TMQ3									=  $this->input->post('WV_TMQ3');
		 						$WV_TMQ4									=  $this->input->post('WV_TMQ4');

								$bridgeWork									=  ($this->input->post('medical_history_bridge_work') ?: NULL);

								$BW_missingTeeth							=  $this->input->post('BW_missing_teeth');

		 						$BW_TMQ1									=  $this->input->post('BW_TMQ1');
		 			 			$BW_TMQ2									=  $this->input->post('BW_TMQ2');
		  						$BW_TMQ3									=  $this->input->post('BW_TMQ3');
		 						$BW_TMQ4									=  $this->input->post('BW_TMQ4');

								$priority				 					= ($this->input->post('priority') ?: NULL);
								$comments				 					= ($this->input->post('comments') ?: NULL);

								$isDeleteAble								= HARD_CODE_ID_YES;

								$doctorID  									= NULL;

								$LDC_DataArray								= array();

							    $LDC_DataArray['TM'] 		 				= array('TMQ1' => $LDC_TMQ1, 'TMQ2' => $LDC_TMQ2,'TMQ3' => $LDC_TMQ3, 'TMQ4' => $LDC_TMQ4);
		  					    $LDC_DataArray['MISSING']  					= array('MISSING_TEETH' => $LDC_missingTeeth);

		  					    $LDC_DataArrayArray_JSON 					= json_encode($LDC_DataArray);


								$FILLING_DataArray							= array();

							    $FILLING_DataArray['TM'] 		 			= array('TMQ1' => $FILLING_TMQ1, 'TMQ2' => $FILLING_TMQ2,'TMQ3' => $FILLING_TMQ3, 'TMQ4' => $FILLING_TMQ4);
		  					    $FILLING_DataArray['MISSING']  				= array('MISSING_TEETH' => $FILLING_missingTeeth);

		  					    $FILLING_DataArrayArray_JSON 				= json_encode($FILLING_DataArray);


								$IMPLANT_DataArray							= array();

							    $IMPLANT_DataArray['TM'] 		 			= array('TMQ1' => $IMPLANT_TMQ1, 'TMQ2' => $IMPLANT_TMQ2,'TMQ3' => $IMPLANT_TMQ3, 'TMQ4' => $IMPLANT_TMQ4);
		  					    $IMPLANT_DataArray['MISSING']  				= array('MISSING_TEETH' => $IMPLANT_missingTeeth);

		  					    $IMPLANT_DataArrayArray_JSON 				= json_encode($IMPLANT_DataArray);




								$URCP_DataArray								= array();

							    $URCP_DataArray['TM'] 		 				= array('TMQ1' => $URCP_TMQ1, 'TMQ2' => $URCP_TMQ2,'TMQ3' => $URCP_TMQ3, 'TMQ4' => $URCP_TMQ4);
		  					    $URCP_DataArray['MISSING']  				= array('MISSING_TEETH' => $URCP_missingTeeth);

		  					    $URCP_DataArrayArray_JSON 					= json_encode($URCP_DataArray);


								$WV_DataArray								= array();

							    $WV_DataArray['TM'] 		 				= array('TMQ1' => $WV_TMQ1, 'TMQ2' => $WV_TMQ2,'TMQ3' => $WV_TMQ3, 'TMQ4' => $WV_TMQ4);
		  					    $WV_DataArray['MISSING']  					= array('MISSING_TEETH' => $WV_missingTeeth);

		  					    $WV_DataArrayArray_JSON 					= json_encode($WV_DataArray);


								$BW_DataArray								= array();

							    $BW_DataArray['TM'] 		 				= array('TMQ1' => $BW_TMQ1, 'TMQ2' => $BW_TMQ2,'TMQ3' => $BW_TMQ3, 'TMQ4' => $BW_TMQ4);
		  					    $BW_DataArray['MISSING']  					= array('MISSING_TEETH' => $BW_missingTeeth);

		  					    $BW_DataArrayArray_JSON 					= json_encode($BW_DataArray);


								$teethHurt									= ($this->input->post('medical_history_teeth_hurt') ?: NULL);
								$woundsSwellings							= ($this->input->post('medical_history_wounds_swellings') ?: NULL);
								$gumsProblem								= ($this->input->post('medical_history_gums_problem') ?: NULL);
								$medications								= ($this->input->post('medical_history_medications') ?: NULL);

								$wearRetainer								= ($this->input->post('medical_history_wear_retainer') ?: NULL);
								$untreatedGumDisease						= ($this->input->post('medical_history_untreated_gum_disease') ?: NULL);
								$jawPain									= ($this->input->post('medical_history_jaw_pain') ?: NULL);

								$visitedDentistPastMonths					= ($this->input->post('medical_history_visited_dentist_past_months') ?: NULL);
								$otherProblemTeeth							= ($this->input->post('medical_history_other_problem_teeth') ?: NULL);
								$allergies									= ($this->input->post('medical_history_allergies') ?: NULL);
								$pregnant									= ($this->input->post('medical_history_pregnant') ?: NULL);

								$diseases									= ($this->input->post('medical_history_diseases') ?: NULL);

								// Upload RX code
								if ($_FILES['file_data_rx']['name']) {

										$fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_rx']['name'])); // Calling From General Helper
										$fname 			= str_replace(" ","_",$fname);
										$fname 			= str_replace("%","_",$fname);
										$nameExt 		= convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_rx']['name'])))); // Calling From General Helper

										$fileName		= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_rx']['name']))); // Calling From General Helper

										$uploaddir 		= CASE_RX_FORM_DIRECTORY_PATH;
										$uploadfile 	= $uploaddir .$fname;

										if (move_uploaded_file($_FILES['file_data_rx']['tmp_name'], $uploadfile)) {

											 $data['RX_form']  =	$fname;

											  $source_image_path   = CASE_RX_FORM_DIRECTORY_PATH;
											  $source_image_name   = $fname;

											  $this->resizeImage($source_image_path,CASE_RX_FORM_THUMBNAIL_DIRECTORY_PATH,$fname,"thumbnail_",100,100);
										}
									}

								// Upload X-rays (OPG)
								if ($_FILES['file_data_x_ray_opg']['name']) {

											$fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_x_ray_opg']['name'])); // Calling From General Helper
											$fname 			= str_replace(" ","_",$fname);
											$fname 			= str_replace("%","_",$fname);
											$nameExt 		= convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_x_ray_opg']['name'])))); // Calling From General Helper

											$fileName		= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_x_ray_opg']['name']))); // Calling From General Helper

											$uploaddir 		= CASE_X_RAY_OPG_PATH_DIRECTORY_PATH;
											$uploadfile 	= $uploaddir .$fname;

											if (move_uploaded_file($_FILES['file_data_x_ray_opg']['tmp_name'], $uploadfile)) {

												  $data['x_rays_opg']  = $fname;

												  $source_image_path   = CASE_X_RAY_OPG_PATH_DIRECTORY_PATH;
												  $source_image_name   = $fname;

												  $this->resizeImage($source_image_path,CASE_X_RAY_OPG_PATH_THUMBNAIL_DIRECTORY_PATH,$fname,"thumbnail_",100,100);
											}
								 	}

								// Upload X-rays (Ceph)
								if ($_FILES['file_data_x_ray_ceph']['name']) {

										  $fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_x_ray_ceph']['name'])); // Calling From General Helper
										  $fname 			= str_replace(" ","_",$fname);
										  $fname 			= str_replace("%","_",$fname);
										  $nameExt 			= convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_x_ray_ceph']['name'])))); // Calling From General Helper

										  $fileName			= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_x_ray_ceph']['name']))); // Calling From General Helper

										  $uploaddir 		= CASE_X_RAY_CEPH_PATH_DIRECTORY_PATH;
										  $uploadfile 		= $uploaddir .$fname;

										  if (move_uploaded_file($_FILES['file_data_x_ray_ceph']['tmp_name'], $uploadfile)) {

												$data['x_rays_ceph']  = $fname;

												$source_image_path    = CASE_X_RAY_CEPH_PATH_DIRECTORY_PATH;
												$source_image_name    = $fname;

												$this->resizeImage($source_image_path,CASE_X_RAY_CEPH_PATH_THUMBNAIL_DIRECTORY_PATH,$fname,"thumbnail_",100,100);
										  }
								  }

									$stateID = NULL;
									$cityID  = NULL;

									/*if ($patientState) {

											$stateExist = $this->model_shared->getRecordMultipleWhere('id',STATES_TABLE,array('lower(name)' => convertAllCharactersLowercase($patientState)));

											if ($stateExist->num_rows() > 0) {

													$stateExist  = $stateExist->row_array();
													$stateID     = $stateExist['id'];

											 } else {

													// Prepair Date To Store In Database
													$stateName							=	$patientState;

													$dataState['name']					=	$stateName;
													$dataState['sef_url']				=	NULL;
													$dataState['country_id']			=	$patientCountry;
												    $dataState['publish_status']		=	HARD_CODE_ID_PUBLISHED;

													if ($stateName) {

														$stateID  = $insertedStateID	=  $this->model_shared->insertRecord_ReturnID(STATES_TABLE,$dataState);
													 }
											  }
									}*/

									if ($patientCity) {

											$cityExist = $this->model_shared->getRecordMultipleWhere('id',CITIES_TABLE,array('lower(name)' => convertAllCharactersLowercase($patientCity)));

											if ($cityExist->num_rows() > 0) {

													$cityExist   = $cityExist->row_array();
													$cityID      = $cityExist['id'];

											 } else {

													// Prepair Date To Store In Database
													$cityName							=	$patientCity;

													$dataCity['state_id']				=	$stateID;
													$dataCity['name']					=	$cityName;
													$dataCity['sef_url']				=	NULL;
													$dataCity['type']					=	55;
												    $dataCity['publish_status']			=	HARD_CODE_ID_PUBLISHED;

													if ($cityName) {

														$cityID  = $insertedCityID	=  $this->model_shared->insertRecord_ReturnID(CITIES_TABLE,$dataCity);
													 }
											  }
										}

									$data['patient']						  			=	ucwords(strtolower($patientName));
									$data['age']						  				=	$age;
									$data['gender']						  				=	$gender;
									$data['patient_country']						  	=	$patientCountry;
									$data['patient_state']						  		=	$patientState;
									$data['patient_city']						  		=	$cityID;
									$data['patient_zip_code']						  	=	$patientZipCode;
									$data['patient_email']						  		=	$patientEmail;
									$data['patient_phone']						  		=	$patientPhone;
									$data['patient_address_1']						  	=	$patientAddress_1;
									$data['patient_address_2']						  	=	$patientAddress_2;
									$data['patient_address_3']						  	=	$patientAddress_3;
									$data['doctor']						 				=	NULL;
									$data['receive_date']								=	DATABASE_DATE_FORMAT($receiveDate);
									$data['impression_type']							=	$impression;
									$data['arch_upper']									=	$archUpper;
									$data['arch_lower']									=	$archLower;

									if ($modePurchase == 'kit') {

										$data['sale_kit']								=	HARD_CODE_ID_YES;

									} else if ($modePurchase == 'instalment') {

										$data['instalment']								=	HARD_CODE_ID_YES;

									} else if ($modePurchase == 'case_purchase') {

										$data['full_case_purchase']						=	HARD_CODE_ID_YES;
									} else if ($modePurchase == 'retainer_only') {

										$data['retainer_only']						=	HARD_CODE_ID_YES;
									}

									$data['treatment_type']								=	$treatmentType;

									$data['country']									=	$country;
									$data['city']					 					=	NULL;
									$data['Distributor']			 					=	$company;
									$data['case_type']			 						=	NULL;

									$data['loose_crowns']			 					=	$looseCrowns;
									$data['loose_crowns_data']			 				=	$LDC_DataArrayArray_JSON;

									$data['fillings']			 						=	$fillings;
									$data['fillings_data']			 					=	$FILLING_DataArrayArray_JSON;

									$data['implant']			 						=	$implant;
									$data['implant_data']			 					=	$IMPLANT_DataArrayArray_JSON;

									$data['undertake_root_canal_procedure']			 	=	$undertakeRootCanalProcedure;
									$data['undertake_root_canal_procedure_data']		=	$URCP_DataArrayArray_JSON;

									$data['wear_veneers']			 					=	$wearVeneers;
									$data['wear_veneers_data']						    =	$WV_DataArrayArray_JSON;

									$data['bridge_work']			 					=	$bridgeWork;
									$data['bridge_work_data']						    =	$BW_DataArrayArray_JSON;

									$data['teeth_hurt']			 						=	$teethHurt;
									$data['wounds_swellings']						    =	$woundsSwellings;
									$data['gums_problem']			 					=	$gumsProblem;
									$data['medications']			 					=	$medications;
									$data['wear_retainer']						   		=	$wearRetainer;
									$data['untreated_gum_disease']			 			=	$untreatedGumDisease;
									$data['jaw_pain']						   			=	$jawPain;
									$data['visited_dentist_past_months']				=	$visitedDentistPastMonths;
									$data['other_problem_teeth']						=	$otherProblemTeeth;

									$data['allergies']						    		=	$allergies;
									$data['pregnant']						    		=	$pregnant;

									$data['priority']									=	NULL;
									$data['description']								=	$comments;

									$data['is_deletable']								=	$isDeleteAble;

									$data['created']									=  	DATABASE_NOW_DATE_TIME_FORMAT();
									$data['created_by']									=   $teamID;
									$data['created_by_reference_table']					=   'MY_ORGANIZATION_TEAM_TABLE';

									// Save all data into database table
									$insertedCaseID										= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASES_TABLE,$data);

									//Add patient diseases data
									if (!empty($diseases)) {

											for($i=0; $i<sizeof($diseases); $i++) {

													$dataDiseases['case_id']							=	$insertedCaseID;
													$dataDiseases['disease_id']							=	$diseases[$i];
													$dataDiseases['description']						=	NULL;

													$dataDiseases['is_deletable']						=	HARD_CODE_ID_YES;

													$dataDiseases['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
													$dataDiseases['created_by']							=   $teamID;
													$dataDiseases['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';

													$insertedDiseasesID									= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_PATIENT_DISEASES_TABLE,$dataDiseases);
											}
									}

									//Add transaction mode history data
									if ($modePurchase) {

										$dataModePurchase['case_id']							=	$insertedCaseID;
										$dataModePurchase['mode']								=	$modePurchase;
										$dataModePurchase['description']						=	NULL;

										$dataModePurchase['is_deletable']						=	HARD_CODE_ID_YES;

										$dataModePurchase['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
										$dataModePurchase['created_by']							=   $teamID;
										$dataModePurchase['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';

										$insertedModePurchaseID									= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_TRANSACTION_MODE_TABLE,$dataModePurchase);
									}

									//Add treatment type history data
									if ($treatmentType) {

										$dataTreatmentType['case_id']							=	$insertedCaseID;
										$dataTreatmentType['mode']								=	$treatmentType;
										$dataTreatmentType['description']						=	NULL;

										$dataTreatmentType['is_deletable']						=	HARD_CODE_ID_YES;

										$dataTreatmentType['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
										$dataTreatmentType['created_by']						=   $teamID;
										$dataTreatmentType['created_by_reference_table']		=   'MY_ORGANIZATION_TEAM_TABLE';

										$insertedTreatmentTypeID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_TREATMENT_MODE_TABLE,$dataTreatmentType);
									}

									// Add Case Stage By Default Receive & Order Entry Stage
									$caseStage['case_id']								=	$insertedCaseID;
									$caseStage['production_stage_id']					=	RECEIVE_ORDER_ENTRY;
									$caseStage['description']							=	NULL;

									$caseStage['is_deletable']							=	$isDeleteAble;

									$caseStage['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
									$caseStage['created_by']							=   $teamID;
									$caseStage['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';

									// Save All data Into Database Table
									$insertedCaseStageID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_PRODUCTION_STAGE_TABLE,$caseStage);

									// Add Status By Default Case Received
									$caseStatus['case_stage_id']						=	$insertedCaseStageID;
									$caseStatus['case_id']								=	$insertedCaseID;
									$caseStatus['status_id']							=	CASE_STATUS_CASE_RECEIVED;
									$caseStatus['description']							=	NULL;

									$caseStatus['is_deletable']							=	$isDeleteAble;

									$caseStatus['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
									$caseStatus['created_by']							=   $teamID;
									$caseStatus['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';

									// Save All data Into Database Table
									$insertedCaseStatusID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$caseStatus);


									// Update Case Table After Assign Stage And Status
									$caseUpdate['present_case_stage_id']				=	$insertedCaseStageID;
									$caseUpdate['present_case_status_id']				=	$insertedCaseStatusID;

									// Update Record Table
									$this->model_shared->editRecordWhere(array('id' => $insertedCaseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$caseUpdate);


									// Case AutoMove
									$this->autoMoveCase($insertedCaseID);

									// Set Session Message If Everything Goes Fine
									$this->session->set_userdata('admin_msg',RECORD_ADDED.' (Case ID: '.$insertedCaseID.' With Patient Name: '.$patientName.')');

									 if ($this->input->post('submit') == 'Save & New') {

										    redirect('case-add/');

									 } else {

											redirect('cases-list/');
									}
			}
	}







	function deleteCase($ID) {

		/*Use For Access Permissions*/
		/*if (!in_array(MODULE_CASE_DELETE,$this->accessModules)) {

			redirect('my-dashboard/');
		}*/

		if (!in_array(MODULE_CASE_EDIT,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;

		$userInfo 							= userInfo($accountID); // Calling From Application Helper

		/*checkEmptyID($productionStageSefURL);*/  // Calling From Shared Helper
		checkEmptyID($ID);  // Calling From Shared Helper

		$ID	 				= decodeString($ID); // Calling From General Helper

		/*$productionStage  	= $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRODUCTION_STAGES_TABLE,array('sef_url' => $productionStageSefURL,'is_deleted' => HARD_CODE_ID_NO));

		if ($productionStage->num_rows() ==0) {

			redirect('cases-list');
		} */

		$result = $this->model_shared->getRecord('*',MEDICAL_CASES_TABLE,'id',$ID);

		if ($result->num_rows == 0) {

				$this->session->set_userdata('admin_msg_error',RECORD_NOT_FOUND);
				redirect('cases-list/');

		} else {

					$row = $result->row_array();

					if ($row['is_deletable'] == HARD_CODE_ID_NO) {

						$this->session->set_userdata('admin_msg_error','Case \''.$row['name'].'\' can\'t be deleted.');

						redirect('cases-list/');

					} else {

							  // Soft Delete From Database Update Record IsDeleted = TRUE
							  $update['is_deleted'] 											= HARD_CODE_ID_YES;
							  $update['deleted_by'] 											= $teamID;
							  $update['deleted_by_reference_table'] 							= 'MY_ORGANIZATION_TEAM_TABLE';
							  $update['deleted']												= DATABASE_NOW_DATE_TIME_FORMAT();  // Calling From Shared Helper

							  $this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$update);
							  $this->session->set_userdata('admin_msg',RECORD_SOFT_DELETED);

							  redirect('cases-list');

					}
		}

	}








	function old_editCase_1($ID) {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_SALES_MARKETING_CASE_EDIT,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		checkEmptyID($ID);  // Calling From Shared Helper

		$ID									= decodeString($ID); // Calling From General Helper

		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;

		$userInfo 							= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;

		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_PRIMARY_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $ID));

		if ($record->num_rows() == 0 ) {

				redirect('cases-listing/');
		}

		$row	 = $record->row_array();

		$caseID  = $row['case_id'];

		//check case forwarded or processed
		$caseUpdateFlag 	= true;
		$caseProcessedFlag  = false;

		if ($row['forwarded_production'] == HARD_CODE_ID_YES || $row['processed_production'] == HARD_CODE_ID_YES) {

				$caseUpdateFlag = false;

				if ($row['processed_production'] == HARD_CODE_ID_YES) {

						$caseProcessedFlag = true;
				}
		}

		/* Set Form Validation Errors */

		$archFlag = false;

			if ($this->input->post('arch_upper') == HARD_CODE_ID_YES) {

					$archFlag = true;
			}

			if ($this->input->post('arch_lower') == HARD_CODE_ID_YES) {

					$archFlag = true;
			}

		if ($caseUpdateFlag == true) {

			$this->form_validation->set_rules('patient_name','patient name','trim|required');
			$this->form_validation->set_rules('impression','impression type','trim|required');

			if ($this->input->post('impression') == CASE_IMPRESSION_TYPE_PHYSICAL) {

				$this->form_validation->set_rules('impression_particulars','impression','trim|required');
			}

			if ($archFlag == false) {

				$this->form_validation->set_rules('arch','arach upper or lower','trim|required');
			}

			$this->form_validation->set_rules('nature_of_patient','nature of patient','trim');
			$this->form_validation->set_rules('receive_date','receive date','trim|required');
			$this->form_validation->set_rules('country','country','required');
			$this->form_validation->set_rules('city','city','trim|required');
			$this->form_validation->set_rules('company','distributor','required');

			if ($this->input->post('company')) {

				$this->form_validation->set_rules('case_type','case type','trim|required');
			}
		}

		$this->form_validation->set_rules('age','patient age','trim|required');
		$this->form_validation->set_rules('gender','gender','trim|required');

		$radioGraph = false;

		if ($this->input->post('radio_graph_opg') == HARD_CODE_ID_YES) {

					$radioGraph = true;
		}

		if ($this->input->post('radio_graph_ceph') == HARD_CODE_ID_YES) {

					$radioGraph = true;
		}

		if ($radioGraph == false) {

			$this->form_validation->set_rules('radio_graph','radio graph OPG or Ceph','trim');
		}

 		$this->form_validation->set_rules('bite','bite','trim');
		$this->form_validation->set_rules('impressions_workable_upper','workable upper','trim');
		$this->form_validation->set_rules('impressions_workable_lower','workable lower','trim');
		$this->form_validation->set_rules('intra_extra_oral_pictures','intra extra oral pictures','trim|required');


		$this->form_validation->set_rules('file_data_rx','','trim|callback_check_case_RX_form');
		$this->form_validation->set_rules('file_data_x_ray_ceph','','trim|callback_check_x_ray_opg');
		$this->form_validation->set_rules('file_data_x_ray_ceph','','trim|callback_check_x_ray_ceph');
		$this->form_validation->set_rules('file_data_x_ray_ceph','','trim|callback_check_assessment_file');

		$this->form_validation->set_rules('priority','priority','trim');
		$this->form_validation->set_rules('airway_bill_number','airway_bill_number','trim');
		$this->form_validation->set_rules('comments','comments','trim');

		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');

			if($this->form_validation->run() === FALSE ) {

				$impressionTypes					= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_IMPRESSION_TYPES_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');
				$doctors							= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DOCTORS_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');
				$countries	   						= $this->model_shared->getRecordMultipleWhereOrderBy('*',COUNTRIES_TABLE,array('id' => 168),'name','ASC');
				$cities	   							= $this->model_shared->getRecordMultipleWhereOrderBy('*',CITIES_TABLE,array('id !=' => 0),'name','ASC');
				$companies							= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_COMPANIES_TABLE,array('country' => 168,'publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');

				$result['assignedRoles']			= $assignedRoles;
				$result['accessModules']			= $accessModules;

				$result['row']						= $row;

				$result['impressionTypes']			= $impressionTypes;
				$result['doctors']					= $doctors;
				$result['countries']				= $countries;
				$result['cities']					= $cities;
				$result['companies']				= $companies;

				$result['caseUpdateFlag']			= $caseUpdateFlag;


				$data['userInfo']            		= $userInfo;

				$data['activeMenu']  				= '20';
				$data['activeSubMenu']  			= '';

				$data['pageHeading']    			= "<i class='fa fa-medkit'></i> Case - ".$row['patient'];
				$data['subHeading']    				= "";

				$data['metaType']     				= 'internal';
				$data['pageTitle']      			= 'Cases | '.DEFAULT_APPLICATION_NAME;

				$data['contents']					=	$this->load->view('cpanel/sales_marketing/cases/case_edit',$result,true);
				$this->load->view('cpanel/template',$data);

			} else {

								// Prepair Date To Store In Database
								$patientName				    			=	($this->input->post('patient_name') ?: NULL);
								$age				    					=	($this->input->post('age') ?: NULL);
								$gender				    					=	($this->input->post('gender') ?: NULL);
								$doctorID				 					=	($this->input->post('doctor_id') ?: NULL);
								$impression				   					=	($this->input->post('impression') ?: NULL);
								$impressionParticulars				   		=	($this->input->post('impression_particulars') ?: NULL);
								$archUpper				   					=	($this->input->post('arch_upper') ?: NULL);
								$archLower				   					=	($this->input->post('arch_lower') ?: NULL);
								$radioGraphOPG				   				=	($this->input->post('radio_graph_opg') ?: NULL);
								$radioGraphCeph				   				=	($this->input->post('radio_graph_ceph') ?: NULL);

								$bite				 						=	($this->input->post('bite') ?: NULL);
								$impressionsWorkableUpper				 	=	($this->input->post('impressions_workable_upper') ?: NULL);
								$impressionsWorkableLower				 	=	($this->input->post('impressions_workable_lower') ?: NULL);
								$intraExtraOralPictures				 		=	($this->input->post('intra_extra_oral_pictures') ?: NULL);

								$nature_of_patient				 			=	($this->input->post('nature_of_patient') ?: NULL);
								$receiveDate				 				=	($this->input->post('receive_date') ?: NULL);
								$country			 						=	($this->input->post('country') ?: NULL);

								$city				 						=	($this->input->post('city') ?: NULL);
								$company				 					=	($this->input->post('company') ?: NULL);
								$caseType				 					=	($this->input->post('case_type') ?: NULL);
								$caseTypeParticulars				 		=	($this->input->post('case_type_particulars') ?: NULL);
								$priority				 					=	($this->input->post('priority') ?: NULL);
								$airwayBillNumber				 			=	($this->input->post('airway_bill_number') ?: NULL);
								$comments				 					=	($this->input->post('comments') ?: NULL);


								if ($caseUpdateFlag == true) {

										$data['patient']						    =	ucwords(strtolower($patientName));
										$data['receive_date']						=	DATABASE_DATE_FORMAT($receiveDate);
										$data['impression_type']					=	$impression;

										if ($impression == CASE_IMPRESSION_TYPE_PHYSICAL) {

											$data['impression_particulars']			=	$impressionParticulars;

										} else {

											$data['impression_particulars']			=	NULL;
										}

										$data['arch_upper']							=	$archUpper;
										$data['arch_lower']							=	$archLower;

										$data['country']							=	$country;
										$data['city']					 			=	$city;
										$data['Distributor']			 			=	$company;
										$data['case_type']			 				=	$caseType;

										if ($caseType == 8 || $caseType == 9) {

											$data['case_type_particulars']			=	$caseTypeParticulars;

										} else {

											$data['case_type_particulars']			=	NULL;
										}


										$data['nature_of_patient']					=	$nature_of_patient;
								}


								// Upload RX code
								if ($_FILES['file_data_rx']['name']) {

											$fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_rx']['name'])); // Calling From General Helper
											$fname 			= str_replace(" ","_",$fname);
											$fname 			= str_replace("%","_",$fname);
											$nameExt 		= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_rx']['name'])))); // Calling From General Helper

											$fileName		= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_rx']['name']))); // Calling From General Helper

											$uploaddir 		= CASE_RX_FORM_DIRECTORY_PATH;
											$uploadfile 	= $uploaddir .$fname;

											if (move_uploaded_file($_FILES['file_data_rx']['tmp_name'], $uploadfile)) {

												  $data['RX_form']			  =	$fname;
												  $dataProduction['RX_form']  =	$fname;

												  $source_image_path   = CASE_RX_FORM_DIRECTORY_PATH;
												  $source_image_name   = $fname;

												  $this->resizeImage($source_image_path,CASE_RX_FORM_THUMBNAIL_DIRECTORY_PATH,$fname,"thumbnail_",100,100);

												  @unlink(CASE_RX_FORM_DIRECTORY_PATH.$this->input->post('old_rx'));
												  @unlink(CASE_RX_FORM_THUMBNAIL_DIRECTORY_PATH."thumbnail_".$this->input->post('old_rx'));
											}
								}

								// Upload X-rays (OPG)
								if ($_FILES['file_data_x_ray_opg']['name']) {

											$fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_x_ray_opg']['name'])); // Calling From General Helper
											$fname 			= str_replace(" ","_",$fname);
											$fname 			= str_replace("%","_",$fname);
											$nameExt 		= convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_x_ray_opg']['name'])))); // Calling From General Helper

											$fileName		= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_x_ray_opg']['name']))); // Calling From General Helper

											$uploaddir 		= CASE_X_RAY_OPG_PATH_DIRECTORY_PATH;
											$uploadfile 	= $uploaddir .$fname;

											if (move_uploaded_file($_FILES['file_data_x_ray_opg']['tmp_name'], $uploadfile)) {

												  $data['x_rays_opg']  		  	 = $fname;
												  $dataProduction['x_rays_opg']  =	$fname;

												  $source_image_path   = CASE_X_RAY_OPG_PATH_DIRECTORY_PATH;
												  $source_image_name   = $fname;

												  $this->resizeImage($source_image_path,CASE_X_RAY_OPG_PATH_THUMBNAIL_DIRECTORY_PATH,$fname,"thumbnail_",100,100);

												   @unlink(CASE_X_RAY_OPG_PATH_DIRECTORY_PATH.$this->input->post('old_x_ray_opg'));
												   @unlink(CASE_X_RAY_OPG_PATH_THUMBNAIL_DIRECTORY_PATH."thumbnail_".$this->input->post('old_x_ray_opg'));
											}
								 	}

								// Upload X-rays (Ceph)
								if ($_FILES['file_data_x_ray_ceph']['name']) {

											$fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_x_ray_ceph']['name'])); // Calling From General Helper
											$fname 			= str_replace(" ","_",$fname);
											$fname 			= str_replace("%","_",$fname);
											$nameExt 		= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_x_ray_ceph']['name'])))); // Calling From General Helper

											$fileName		= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_x_ray_ceph']['name']))); // Calling From General Helper

											$uploaddir 		= CASE_X_RAY_CEPH_PATH_DIRECTORY_PATH;
											$uploadfile 	= $uploaddir .$fname;

											if (move_uploaded_file($_FILES['file_data_x_ray_ceph']['tmp_name'], $uploadfile)) {

												  $data['x_rays_ceph']  		 = $fname;
												  $dataProduction['x_rays_ceph'] =	$fname;

												  $source_image_path    = CASE_X_RAY_CEPH_PATH_DIRECTORY_PATH;
												  $source_image_name    = $fname;

												  $this->resizeImage($source_image_path,CASE_X_RAY_CEPH_PATH_THUMBNAIL_DIRECTORY_PATH,$fname,"thumbnail_",100,100);
												  @unlink(CASE_X_RAY_CEPH_PATH_DIRECTORY_PATH.$this->input->post('old_x_ray_ceph'));
												  @unlink(CASE_X_RAY_CEPH_PATH_THUMBNAIL_DIRECTORY_PATH."thumbnail_".$this->input->post('old_x_ray_ceph'));

											}
								 	}

								// Upload Assessment File
								if ($_FILES['file_data_assessment']['name']) {

											$fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_assessment']['name'])); // Calling From General Helper
											$fname 			= str_replace(" ","_",$fname);
											$fname 			= str_replace("%","_",$fname);
											$nameExt 		= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_assessment']['name'])))); // Calling From General Helper

											$fileName		= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_assessment']['name']))); // Calling From General Helper

											$uploaddir 		= CASE_FILE_ASSESSMENT_PATH_DIRECTORY_PATH;
											$uploadfile 	= $uploaddir .$fname;

											if (move_uploaded_file($_FILES['file_data_assessment']['tmp_name'], $uploadfile)) {

												  $data['file_assessment']  		   = $fname;
												  $dataProduction['file_assessment']   = $fname;

												  @unlink(CASE_FILE_ASSESSMENT_PATH_DIRECTORY_PATH.$this->input->post('old_file_assessment'));
											}
								 	}

								$data['age']						  		=	$age;
								$data['gender']						  		=	$gender;
								$data['doctor']						 		=	$doctorID;
								$data['radio_graphs_opg']					=	$radioGraphOPG;
								$data['radio_graphs_ceph']					=	$radioGraphCeph;
								$data['bite']								=	$bite;
								$data['impression_workable_upper']			=	$impressionsWorkableUpper;
								$data['impression_workable_lower']			=	$impressionsWorkableLower;
								$data['intra_extra_oral_pictures']			=	$intraExtraOralPictures;

								$data['priority']							=	 $priority;
								$data['airway_bill_number']					=	 $airwayBillNumber;
								$data['description']						=	 $comments;

								// Update Record Table
								$this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PRIMARY_CASES_TABLE,$data);

								if ($caseProcessedFlag == true) {

										// Update Record Table
										$this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$dataProduction);
								}

								// Set Session Message If Everything Goes Fine
								$this->session->set_userdata('admin_msg',RECORD_UPDATED);

								 if ($this->input->post('submit') == 'Save & Close') {

										redirect('cases-listing/');

								 } else {

										redirect('case-edit/'.encodeString($ID));
								 }
		}
	}









	function editCase($ID) {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_EDIT,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		checkEmptyID($ID);  // Calling From Shared Helper

		$ID									= decodeString($ID); // Calling From General Helper

		$accountID							= $this->accountID;
		$teamID								= $this->teamID;
		$organizationID 					= $this->organizationID;

		$userInfo 							= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;

		$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $ID));

		if ($record->num_rows() == 0 ) {

				redirect('cases-list/');
		}

		$row	 = $record->row_array();


		//for current stage id start
		$caseStageID 	= $this->model_shared->getRecordMultipleWhere('present_case_stage_id',MEDICAL_CASES_TABLE,array('id' => $ID));

			if ($caseStageID->num_rows() > 0) {

				$caseStageID							= $caseStageID->row_array();
				$caseStageID 							= $caseStageID['present_case_stage_id'];
			}
		//for current stage id ends


        //for lose crowns start
		$loose_crowns_data_encoded = $row['loose_crowns_data'];
		$loose_crowns_data=json_decode($loose_crowns_data_encoded);
		/*echo "<pre>";*/
		$LDC_TM=$loose_crowns_data->TM;
		$LDC_TMQ1=$LDC_TM->TMQ1;
		$LDC_TMQ2=$LDC_TM->TMQ2;
		$LDC_TMQ3=$LDC_TM->TMQ3;
		$LDC_TMQ4=$LDC_TM->TMQ4;
		/*print_r($LDC_TM);
		echo "</pre>";*/
		$result['LDC_TMQ1']	= $LDC_TMQ1;
		$result['LDC_TMQ2']	= $LDC_TMQ2;
		$result['LDC_TMQ3']	= $LDC_TMQ3;
		$result['LDC_TMQ4']	= $LDC_TMQ4;
		//for lose crowns end


		//for fillings start
		$fillings_data_encoded = $row['fillings_data'];
		$fillings_data=json_decode($fillings_data_encoded);
		/*echo "<pre>";*/
		$FILLING_TM=$fillings_data->TM;
		$FILLING_TMQ1=$FILLING_TM->TMQ1;
		$FILLING_TMQ2=$FILLING_TM->TMQ2;
		$FILLING_TMQ3=$FILLING_TM->TMQ3;
		$FILLING_TMQ4=$FILLING_TM->TMQ4;
		/*print_r($FILLING_TM);
		echo "</pre>"; */
		$result['FILLING_TMQ1']	= $FILLING_TMQ1;
		$result['FILLING_TMQ2']	= $FILLING_TMQ2;
		$result['FILLING_TMQ3']	= $FILLING_TMQ3;
		$result['FILLING_TMQ4']	= $FILLING_TMQ4;
		//for fillings end


		//for implant start
		$implant_data_encoded = $row['implant_data'];
		$implant_data=json_decode($implant_data_encoded);
		/*echo "<pre>";*/
		$IMPLANT_TM=$implant_data->TM;
		$IMPLANT_TMQ1=$IMPLANT_TM->TMQ1;
		$IMPLANT_TMQ2=$IMPLANT_TM->TMQ2;
		$IMPLANT_TMQ3=$IMPLANT_TM->TMQ3;
		$IMPLANT_TMQ4=$IMPLANT_TM->TMQ4;
		/*print_r($IMPLANT_TM);
		echo "</pre>"; */
		$result['IMPLANT_TMQ1']	= $IMPLANT_TMQ1;
		$result['IMPLANT_TMQ2']	= $IMPLANT_TMQ2;
		$result['IMPLANT_TMQ3']	= $IMPLANT_TMQ3;
		$result['IMPLANT_TMQ4']	= $IMPLANT_TMQ4;
		//for implant end


		//for undertake a root canal procedure start
		$undertake_root_canal_procedure_data_encoded = $row['undertake_root_canal_procedure_data'];
		$undertake_root_canal_procedure_data=json_decode($undertake_root_canal_procedure_data_encoded);
		/*echo "<pre>";*/
		$URCP_TM=$undertake_root_canal_procedure_data->TM;
		$URCP_TMQ1=$URCP_TM->TMQ1;
		$URCP_TMQ2=$URCP_TM->TMQ2;
		$URCP_TMQ3=$URCP_TM->TMQ3;
		$URCP_TMQ4=$URCP_TM->TMQ4;
		/*print_r($URCP_TM);
		echo "</pre>";*/
		$result['URCP_TMQ1']	= $URCP_TMQ1;
		$result['URCP_TMQ2']	= $URCP_TMQ2;
		$result['URCP_TMQ3']	= $URCP_TMQ3;
		$result['URCP_TMQ4']	= $URCP_TMQ4;
		//for undertake a root canal procedure end



		//for wear veneers? start
		$wear_veneers_data_encoded = $row['wear_veneers_data'];
		$wear_veneers_data=json_decode($wear_veneers_data_encoded);
		/*echo "<pre>";*/
		$WV_TM=$wear_veneers_data->TM;
		$WV_TMQ1=$WV_TM->TMQ1;
		$WV_TMQ2=$WV_TM->TMQ2;
		$WV_TMQ3=$WV_TM->TMQ3;
		$WV_TMQ4=$WV_TM->TMQ4;
		/*print_r($WV_TM);
		echo "</pre>";*/
		$result['WV_TMQ1']	= $WV_TMQ1;
		$result['WV_TMQ2']	= $WV_TMQ2;
		$result['WV_TMQ3']	= $WV_TMQ3;
		$result['WV_TMQ4']	= $WV_TMQ4;
		//for wear veneers? end


		//for bridge work start
		$bridge_work_data_encoded = $row['bridge_work_data'];
		$bridge_work_data=json_decode($bridge_work_data_encoded);
		/*echo "<pre>";*/
		$BW_TM=$bridge_work_data->TM;
		$BW_TMQ1=$BW_TM->TMQ1;
		$BW_TMQ2=$BW_TM->TMQ2;
		$BW_TMQ3=$BW_TM->TMQ3;
		$BW_TMQ4=$BW_TM->TMQ4;
		/*print_r($BW_TM);
		echo "</pre>"; */
		$result['BW_TMQ1']	= $BW_TMQ1;
		$result['BW_TMQ2']	= $BW_TMQ2;
		$result['BW_TMQ3']	= $BW_TMQ3;
		$result['BW_TMQ4']	= $BW_TMQ4;
		//for bridge work end


		/* Set Form Validation Errors */
		$this->form_validation->set_rules('portal_number','portal number','trim|callback_check_exist_edit_case_portal_number');
		$this->form_validation->set_rules('patient_name','patient name','trim|required');
		$this->form_validation->set_rules('case_revision_id','already exist.','trim|callback_check_already_exist_edit_case_revision_id');
		//$this->form_validation->set_rules('impression','impression type','trim|required');

		$archFlag = false;

		if ($this->input->post('arch_upper') == HARD_CODE_ID_YES) {

					$archFlag = true;
		}

		if ($this->input->post('arch_lower') == HARD_CODE_ID_YES) {

					$archFlag = true;
		}

		if ($archFlag == false) {

			$this->form_validation->set_rules('arch','arach upper or lower','trim|required');
		}

 		$this->form_validation->set_rules('nature_of_patient','nature of patient','trim');
		$this->form_validation->set_rules('receive_date','receive date','trim|required');
		$this->form_validation->set_rules('country','country','required');
		$this->form_validation->set_rules('city','city','trim');
		$this->form_validation->set_rules('company','Distributor','required');

		if ($this->input->post('company')) {

			$this->form_validation->set_rules('case_type','case type','trim');
		}

		$this->form_validation->set_rules('priority','priority','trim');
		$this->form_validation->set_rules('airway_bill_number','airway_bill_number','trim');
		$this->form_validation->set_rules('comments','comments','trim');

		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');

			if($this->form_validation->run() === FALSE ) {

				$casePhotos							= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_PICTURES_TABLE,array('case_id' => $ID,'is_deleted' => HARD_CODE_ID_NO),'id','ASC');

				$impressionTypes					= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_IMPRESSION_TYPES_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');

				$doctors							= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DOCTORS_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');

				$countries	   						= $this->model_shared->getRecordMultipleWhereOrderBy('*',COUNTRIES_TABLE,array('id !=' => 0),'name','ASC');
				$cities	   							= $this->model_shared->getRecordMultipleWhereOrderBy('*',CITIES_TABLE,array('id !=' => 0),'name','ASC');

				$companies							= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_COMPANIES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');

				$transactionMode  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASE_TRANSACTION_MODE_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_id' => $ID))->row();

				$diseases									= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DISEASES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'order','ASC');

				$addedDiseases									= $this->model_shared->getRecordMultipleWhereOrderBy('disease_id',MEDICAL_PATIENT_DISEASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_id' => $ID),'id','ASC');

				/*foreach($addedDiseases->result() as $ad)
				{
					echo $ad->disease_id;
				}*/
				/*print_r($addedDiseases);*/

				$result['assignedRoles']			= $assignedRoles;
				$result['accessModules']			= $accessModules;

				$result['row']						= $row;
				$result['casePhotos']				= $casePhotos;

				$result['impressionTypes']			= $impressionTypes;
				$result['doctors']					= $doctors;
				$result['countries']				= $countries;
				$result['cities']					= $cities;
				$result['companies']				= $companies;
				$result['transactionMode']			= $transactionMode;
				$result['diseases']			        = $diseases;
				$result['addedDiseases']			= $addedDiseases;
				$result['states']          			=   getAllStates(); //  Calling From


				$data['userInfo']            		= $userInfo;

				$data['activeMenu']  				= '4';
				$data['activeSubMenu']  			= '';

				$data['pageHeading']    			= "<i class='fa fa-medkit'></i> Case - ".$row['patient'];
				$data['subHeading']    				= "";

				$data['metaType']     				= 'internal';
				$data['pageTitle']      			= 'Cases | '.DEFAULT_APPLICATION_NAME;

				$data['contents']					=	$this->load->view('cpanel/cases/case_edit',$result,true);
				$this->load->view('cpanel/template',$data);

			} else {

								// Prepair Date To Store In Database
								$portalNumber								=	($this->input->post('portal_number') ?: NULL);
								$patientName				    			=	($this->input->post('patient_name') ?: NULL);

								$patientAge				    			=	($this->input->post('age') ?: NULL);

								$gender				    					=	($this->input->post('gender') ?: NULL);

								$patientState				    			= ($this->input->post('patient_state') ?: NULL);

								$patientCountry				    			= ($this->input->post('patient_country') ?: NULL);

								$patientCity				    					=	($this->input->post('patient_city') ?: NULL);

								$patientZipCode				    					=	($this->input->post('patient_zip_code') ?: NULL);

								$patientEmail				    					=	($this->input->post('patient_email') ?: NULL);

								$patientPhone				    					=	($this->input->post('patient_phone') ?: NULL);

								$patientAddress1				    					=	($this->input->post('patient_address_1') ?: NULL);

								$patientAddress2				    					=	($this->input->post('patient_address_2') ?: NULL);

								$patientAddress3				    					=	($this->input->post('patient_address_3') ?: NULL);

								$treatmentType				    					=	($this->input->post('treatment_type') ?: NULL);

								$transaction				    					=	($this->input->post('transaction') ?: NULL);




								$doctorID				 					=	($this->input->post('doctor_id') ?: NULL);
								$doctorAccountName				 			=	($this->input->post('doctor_account_name') ?: NULL);
								$impression				   					=	($this->input->post('impression') ?: NULL);
								$archUpper				   					=	($this->input->post('arch_upper') ?: NULL);
								$archLower				   					=	($this->input->post('arch_lower') ?: NULL);

								$nature_of_patient				 			=	($this->input->post('nature_of_patient') ?: NULL);
								$receiveDate				 				=	($this->input->post('receive_date') ?: NULL);
								$country			 						=	($this->input->post('country') ?: NULL);

								$city				 						=	($this->input->post('city') ?: NULL);
								$company				 					=	($this->input->post('company') ?: NULL);
								$caseType				 					=	($this->input->post('case_type') ?: NULL);
								$priority				 					=	($this->input->post('priority') ?: NULL);
								$airwayBillNumber				 			=	($this->input->post('airway_bill_number') ?: NULL);
								$comments				 					=	($this->input->post('comments') ?: NULL);

								$caseRevisionID				 				=	($this->input->post('case_revision_id') ?: NULL);






								//medical history start
								$looseCrowns								= ($this->input->post('medical_history_loose_crowns') ?: NULL);

								$LDC_missingTeeth							= $this->input->post('LDC_missing_teeth');

		 						$LDC_TMQ1									= $this->input->post('LDC_TMQ1');
		 			 			$LDC_TMQ2									= $this->input->post('LDC_TMQ2');
		  						$LDC_TMQ3									= $this->input->post('LDC_TMQ3');
		 						$LDC_TMQ4									= $this->input->post('LDC_TMQ4');

								$fillings									= ($this->input->post('medical_history_fillings') ?: NULL);

								$FILLING_missingTeeth						= $this->input->post('FILLING_missing_teeth');

		 						$FILLING_TMQ1								= $this->input->post('FILLING_TMQ1');
		 			 			$FILLING_TMQ2								= $this->input->post('FILLING_TMQ2');
		  						$FILLING_TMQ3								= $this->input->post('FILLING_TMQ3');
		 						$FILLING_TMQ4								= $this->input->post('FILLING_TMQ4');

								$implant									= ($this->input->post('medical_history_implant') ?: NULL);

								$IMPLANT_missingTeeth						= $this->input->post('IMPLANT_missing_teeth');

		 						$IMPLANT_TMQ1								= $this->input->post('IMPLANT_TMQ1');
		 			 			$IMPLANT_TMQ2								= $this->input->post('IMPLANT_TMQ2');
		  						$IMPLANT_TMQ3								= $this->input->post('IMPLANT_TMQ3');
		 						$IMPLANT_TMQ4								= $this->input->post('IMPLANT_TMQ4');

								$undertakeRootCanalProcedure				= ($this->input->post('medical_history_undertake_root_canal_procedure') ?: NULL);

								$URCP_missingTeeth							= $this->input->post('URCP_missing_teeth');

		 						$URCP_TMQ1									= $this->input->post('URCP_TMQ1');
		 			 			$URCP_TMQ2									= $this->input->post('URCP_TMQ2');
		  						$URCP_TMQ3									= $this->input->post('URCP_TMQ3');
		 						$URCP_TMQ4									= $this->input->post('URCP_TMQ4');


								$wearVeneers								= ($this->input->post('medical_history_wear_veneers') ?: NULL);

								$WV_missingTeeth							=  $this->input->post('WV_missing_teeth');

		 						$WV_TMQ1									=  $this->input->post('WV_TMQ1');
		 			 			$WV_TMQ2									=  $this->input->post('WV_TMQ2');
		  						$WV_TMQ3									=  $this->input->post('WV_TMQ3');
		 						$WV_TMQ4									=  $this->input->post('WV_TMQ4');

								$bridgeWork									=  ($this->input->post('medical_history_bridge_work') ?: NULL);

								$BW_missingTeeth							=  $this->input->post('BW_missing_teeth');

		 						$BW_TMQ1									=  $this->input->post('BW_TMQ1');
		 			 			$BW_TMQ2									=  $this->input->post('BW_TMQ2');
		  						$BW_TMQ3									=  $this->input->post('BW_TMQ3');
		 						$BW_TMQ4									=  $this->input->post('BW_TMQ4');

								$priority				 					= ($this->input->post('priority') ?: NULL);
								$comments				 					= ($this->input->post('comments') ?: NULL);

								$diseases									= ($this->input->post('medical_history_diseases') ?: NULL);







								$LDC_DataArray								= array();

							    $LDC_DataArray['TM'] 		 				= array('TMQ1' => $LDC_TMQ1, 'TMQ2' => $LDC_TMQ2,'TMQ3' => $LDC_TMQ3, 'TMQ4' => $LDC_TMQ4);
		  					    $LDC_DataArray['MISSING']  					= array('MISSING_TEETH' => $LDC_missingTeeth);

		  					    $LDC_DataArrayArray_JSON 					= json_encode($LDC_DataArray);


								$FILLING_DataArray							= array();

							    $FILLING_DataArray['TM'] 		 			= array('TMQ1' => $FILLING_TMQ1, 'TMQ2' => $FILLING_TMQ2,'TMQ3' => $FILLING_TMQ3, 'TMQ4' => $FILLING_TMQ4);
		  					    $FILLING_DataArray['MISSING']  				= array('MISSING_TEETH' => $FILLING_missingTeeth);

		  					    $FILLING_DataArrayArray_JSON 				= json_encode($FILLING_DataArray);


								$IMPLANT_DataArray							= array();

							    $IMPLANT_DataArray['TM'] 		 			= array('TMQ1' => $IMPLANT_TMQ1, 'TMQ2' => $IMPLANT_TMQ2,'TMQ3' => $IMPLANT_TMQ3, 'TMQ4' => $IMPLANT_TMQ4);
		  					    $IMPLANT_DataArray['MISSING']  				= array('MISSING_TEETH' => $IMPLANT_missingTeeth);

		  					    $IMPLANT_DataArrayArray_JSON 				= json_encode($IMPLANT_DataArray);




								$URCP_DataArray								= array();

							    $URCP_DataArray['TM'] 		 				= array('TMQ1' => $URCP_TMQ1, 'TMQ2' => $URCP_TMQ2,'TMQ3' => $URCP_TMQ3, 'TMQ4' => $URCP_TMQ4);
		  					    $URCP_DataArray['MISSING']  				= array('MISSING_TEETH' => $URCP_missingTeeth);

		  					    $URCP_DataArrayArray_JSON 					= json_encode($URCP_DataArray);


								$WV_DataArray								= array();

							    $WV_DataArray['TM'] 		 				= array('TMQ1' => $WV_TMQ1, 'TMQ2' => $WV_TMQ2,'TMQ3' => $WV_TMQ3, 'TMQ4' => $WV_TMQ4);
		  					    $WV_DataArray['MISSING']  					= array('MISSING_TEETH' => $WV_missingTeeth);

		  					    $WV_DataArrayArray_JSON 					= json_encode($WV_DataArray);


								$BW_DataArray								= array();

							    $BW_DataArray['TM'] 		 				= array('TMQ1' => $BW_TMQ1, 'TMQ2' => $BW_TMQ2,'TMQ3' => $BW_TMQ3, 'TMQ4' => $BW_TMQ4);
		  					    $BW_DataArray['MISSING']  					= array('MISSING_TEETH' => $BW_missingTeeth);

		  					    $BW_DataArrayArray_JSON 					= json_encode($BW_DataArray);


								$teethHurt									= ($this->input->post('medical_history_teeth_hurt') ?: NULL);
								$woundsSwellings							= ($this->input->post('medical_history_wounds_swellings') ?: NULL);
								$gumsProblem								= ($this->input->post('medical_history_gums_problem') ?: NULL);
								$medications								= ($this->input->post('medical_history_medications') ?: NULL);

								$wearRetainer								= ($this->input->post('medical_history_wear_retainer') ?: NULL);
								$untreatedGumDisease						= ($this->input->post('medical_history_untreated_gum_disease') ?: NULL);
								$jawPain									= ($this->input->post('medical_history_jaw_pain') ?: NULL);

								$visitedDentistPastMonths					= ($this->input->post('medical_history_visited_dentist_past_months') ?: NULL);
								$otherProblemTeeth							= ($this->input->post('medical_history_other_problem_teeth') ?: NULL);
								$allergies									= ($this->input->post('medical_history_allergies') ?: NULL);
								$pregnant									= ($this->input->post('medical_history_pregnant') ?: NULL);

								$diseases									= ($this->input->post('medical_history_diseases') ?: NULL);

								/*print_r($diseases); die();*/
								//medical history end









                                //medical history data start
                                if ($modePurchase == 'kit') {

										$data['sale_kit']								=	HARD_CODE_ID_YES;

									} else if ($modePurchase == 'instalment') {

										$data['instalment']								=	HARD_CODE_ID_YES;

									} else if ($modePurchase == 'case_purchase') {

										$data['full_case_purchase']						=	HARD_CODE_ID_YES;
									}

									$data['treatment_type']								=	$treatmentType;
									$data['patient_state']						  		=	$patientState;
									$data['patient_country']						  		=	$patientCountry;
									$data['country']									=	$country;
									$data['city']					 					=	NULL;
									$data['Distributor']			 					=	$company;
									$data['case_type']			 						=	NULL;

									$data['loose_crowns']			 					=	$looseCrowns;
									$data['loose_crowns_data']			 				=	$LDC_DataArrayArray_JSON;

									$data['fillings']			 						=	$fillings;
									$data['fillings_data']			 					=	$FILLING_DataArrayArray_JSON;

									$data['implant']			 						=	$implant;
									$data['implant_data']			 					=	$IMPLANT_DataArrayArray_JSON;

									$data['undertake_root_canal_procedure']			 	=	$undertakeRootCanalProcedure;
									$data['undertake_root_canal_procedure_data']		=	$URCP_DataArrayArray_JSON;

									$data['wear_veneers']			 					=	$wearVeneers;
									$data['wear_veneers_data']						    =	$WV_DataArrayArray_JSON;

									$data['bridge_work']			 					=	$bridgeWork;
									$data['bridge_work_data']						    =	$BW_DataArrayArray_JSON;

									$data['teeth_hurt']			 						=	$teethHurt;
									$data['wounds_swellings']						    =	$woundsSwellings;
									$data['gums_problem']			 					=	$gumsProblem;
									$data['medications']			 					=	$medications;
									$data['wear_retainer']						   		=	$wearRetainer;
									$data['untreated_gum_disease']			 			=	$untreatedGumDisease;
									$data['jaw_pain']						   			=	$jawPain;
									$data['visited_dentist_past_months']				=	$visitedDentistPastMonths;
									$data['other_problem_teeth']						=	$otherProblemTeeth;

									$data['allergies']						    		=	$allergies;
									$data['pregnant']						    		=	$pregnant;
                                //medical history data end








								/*$data['parent_case']						=	$caseRevisionID;*/

								/*$data['portal_number']						=	$portalNumber;*/
								$data['patient']						    =	ucwords(strtolower($patientName));

								$data['age']						        =	$patientAge;
								$data['gender']						  		=	$gender;
								$data['patient_city']						=	$patientCity;
								$data['patient_zip_code']					=	$patientZipCode;
								$data['patient_email']					    =	$patientEmail;
								$data['patient_phone']					    =	$patientPhone;
								$data['patient_address_1']					=	$patientAddress1;
								$data['patient_address_2']					=	$patientAddress2;
								$data['patient_address_3']					=	$patientAddress3;
								$data['treatment_type']					    =	$treatmentType;



								$data['doctor']						 		=	$doctorID;
								//$data['receive_date']						=	DATABASE_DATE_FORMAT($receiveDate);
								//$data['impression_type']					=	$impression;
								$data['arch_upper']							=	$archUpper;
								$data['arch_lower']							=	$archLower;
								$data['country']							=	$country;
								$data['city']					 			=	$city;
								$data['Distributor']			 			=	$company;
								$data['case_type']			 				=	$caseType;
								/*$data['nature_of_patient']					=	$nature_of_patient;
								$data['priority']							=	$priority;
								$data['airway_bill_number']					=	$airwayBillNumber;*/
								$data['description']						=	$comments;

								$dataMode['mode']					        =	$transaction;

								// Update Record Table
								$this->model_shared->editRecordWhere(array('id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$data);


								/*for update record activity history start*/
								/*$dataActivityHistory['case_stage_id ']					=	$caseStageID;
								$dataActivityHistory['case_id']							=	$ID;
								$dataActivityHistory['status_id']						=	CASE_STATUS_UPDATE_CASE_DETAILS;
								$dataActivityHistory['description']						=	NULL;

								$dataActivityHistory['is_deletable']					=	HARD_CODE_ID_YES;

								$dataActivityHistory['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
								$dataActivityHistory['created_by']						=   $teamID;
								$dataActivityHistory['created_by_reference_table']		=   'MY_ORGANIZATION_TEAM_TABLE';

								$insertedActivituyHistoryID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$dataActivityHistory);*/
								/*for update record activity history ends*/




								// Update tbl_application_alignerco_case_transaction_mode_history Table
								$this->model_shared->editRecordWhere(array('case_id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASE_TRANSACTION_MODE_TABLE,$dataMode);


								if ($doctorID) {

									$updateDoctor['account_name'] =	$doctorAccountName;

									// Update Record Table
									$this->model_shared->editRecordWhere(array('id' => $doctorID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_DOCTORS_TABLE,$updateDoctor);
								}


								//update patient diseases data
									if (!empty($diseases)) {

										$dataUpdateDiscease['is_deleted']=HARD_CODE_ID_YES;

										 $this->model_shared->deleteRecordWhere(array('case_id' => $ID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_PATIENT_DISEASES_TABLE);


											for($i=0; $i<sizeof($diseases); $i++) {

													$dataDiseases['case_id']							=	$ID;
													$dataDiseases['disease_id']							=	$diseases[$i];
													$dataDiseases['description']						=	NULL;

													$dataDiseases['is_deletable']						=	HARD_CODE_ID_YES;

													$dataDiseases['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
													$dataDiseases['created_by']							=   $teamID;
													$dataDiseases['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';

													$insertedDiseasesID									= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_PATIENT_DISEASES_TABLE,$dataDiseases);
											}
									}


								// Set Session Message If Everything Goes Fine
								$this->session->set_userdata('admin_msg',RECORD_UPDATED);

								 if ($this->input->post('submit') == 'Save & Close') {

										redirect('cases-list/');

								 } else {

										redirect('edit-case/'.encodeString($ID));
								 }
			}
	}

	function check_exist_edit_case_portal_number() {

		$txtPortalNumber  = $this->input->post('portal_number');
		$oldPortalNumber  = $this->input->post('old_portal_number');

		if ($txtPortalNumber) {

				  if ($txtPortalNumber != $oldPortalNumber) {

					  $dbRecord = $this->model_shared->checkRecordEdit_SoftDelete('portal_number',MEDICAL_CASES_TABLE,'portal_number',$txtPortalNumber,$oldPortalNumber);

						  if ($dbRecord->num_rows()>0) {

							  $this->form_validation->set_message('check_exist_edit_case_portal_number','Error:  portal number \''.$txtPortalNumber.'\' already exist. Please try with different number.');

							  return false;

						  } else {

							  return true;
						  }

				  } else {

					  return true;
				  }

			} else {

				return true;
			}
	}

	function check_already_exist_edit_case_revision_id() {

		$openCaseID  = $this->input->post('case_id');

		$txtRevisionID  = $this->input->post('case_revision_id');
		$oldRevisionID  = $this->input->post('old_case_revision_id');

		if ($openCaseID == $txtRevisionID) {

			 $this->form_validation->set_message('check_already_exist_edit_case_revision_id','Error: Invalid Case ID \''.$txtRevisionID.'\'. Please try with different ID.');
			 return false;
		}

		if ($txtRevisionID) {

			  $db_name = $this->model_shared->checkRecordExist_SoftDelete('id',MEDICAL_CASES_TABLE,'parent_case',$txtRevisionID);

				 if ($txtRevisionID != $oldRevisionID) {

				  if ($db_name->num_rows() > 0) {

					  $this->form_validation->set_message('check_already_exist_edit_case_revision_id','Error: Case ID \''.$txtRevisionID.'\' already exist. Please try with different ID.');
					  return false;

				  } else {

					  return true;
				  }

				 } else {

					return true;
				 }
		} else {

			return true;
		}
	}










	function check_case_RX_form() {

		 if ($_FILES['file_data_rx']['name']) {

			  $fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_rx']['name'])); // Calling From General Helper
			  $fname 			= str_replace(" ","_",$fname);
			  $fname 			= str_replace("%","_",$fname);
			  $nameExt 			= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_rx']['name'])))); // Calling From General Helper

			  $fileName			= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_rx']['name']))); // Calling From General Helper

			  if($nameExt!='jpg' && $nameExt!='jpeg' && $nameExt!='gif' && $nameExt!='png')  {

				$this->form_validation->set_message('check_case_RX_form','Error: Format Not Supported.Only .jpg .jpeg .png files are accepted.');
				return false;

			 } else {

				if($_FILES["file_data_rx"]["size"] > 2000000) {

					$this->form_validation->set_message('check_case_RX_form','Error: Image size exceeds 2MB');
					return false;

				} else {

					return true;
				}
			 }

		 } else {

				return true;
		}
	}

	function check_x_ray_opg() {

		 if ($_FILES['file_data_x_ray_opg']['name']) {

			  $fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_x_ray_opg']['name'])); // Calling From General Helper
			  $fname 			= str_replace(" ","_",$fname);
			  $fname 			= str_replace("%","_",$fname);
			  $nameExt 			= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_x_ray_opg']['name'])))); // Calling From General Helper

			  $fileName			= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_x_ray_opg']['name']))); // Calling From General Helper

			  if($nameExt!='jpg'  && $nameExt!='jpeg' && $nameExt!='gif' && $nameExt!='png')  {

				$this->form_validation->set_message('check_x_ray_opg','Error: Format Not Supported.Only .jpg .png files are accepted.');
				return false;

			 } else {

				if($_FILES["file_data_x_ray_opg"]["size"] > 2000000) {

					$this->form_validation->set_message('check_x_ray_opg','Error: Image size exceeds 2MB');
					return false;

				} else {

					return true;
				}
			 }

		 } else {

				return true;
		}
	}

	function check_x_ray_ceph() {

		 if ($_FILES['file_data_x_ray_ceph']['name']) {

			  $fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_x_ray_ceph']['name'])); // Calling From General Helper
			  $fname 			= str_replace(" ","_",$fname);
			  $fname 			= str_replace("%","_",$fname);
			  $nameExt 			= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_x_ray_ceph']['name'])))); // Calling From General Helper

			  $fileName			= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_x_ray_ceph']['name']))); // Calling From General Helper

			  if($nameExt!='jpg'  && $nameExt!='jpeg' && $nameExt!='gif' && $nameExt!='png')  {

				$this->form_validation->set_message('check_x_ray_ceph','Error: Format Not Supported.Only .jpg .png files are accepted.');
				return false;

			 } else {

				if($_FILES["file_data_x_ray_ceph"]["size"] > 2000000) {

					$this->form_validation->set_message('check_x_ray_ceph','Error: Image size exceeds 2MB');
					return false;

				} else {

					return true;
				}
			 }

		 } else {

				return true;
		}
	}

	function check_assessment_file() {

		 if ($_FILES['file_data_assessment']['name']) {

			  $fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file_data_assessment']['name'])); // Calling From General Helper
			  $fname 			= str_replace(" ","_",$fname);
			  $fname 			= str_replace("%","_",$fname);
			  $nameExt 			= convertAllCharactersLowercase(end(explode(".", basename($_FILES['file_data_assessment']['name'])))); // Calling From General Helper

			  $fileName			= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file_data_assessment']['name']))); // Calling From General Helper

			  if($nameExt!='doc'  && $nameExt!='docx' && $nameExt!='pdf' && $nameExt!='txt')  {

				$this->form_validation->set_message('check_assessment_file','Error: Format Not Supported.Only .doc .docx .pdf .txt files are accepted.');
				return false;

			 } else {

				if($_FILES["file_data_assessment"]["size"] > 2000000) {

					$this->form_validation->set_message('check_assessment_file','Error: File size exceeds 2MB');
					return false;

				} else {

					return true;
				}
			 }

		 } else {

				return true;
		}
	}





	function medicalCases() {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_ROOMS,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 							= $this->assignedRoles;
		$accessModules 							= $this->accessModules;

		$productionStages						= productionStages(); // Calling From Case Helper

		$impressionTypes						= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_IMPRESSION_TYPES_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');
		$doctors								= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DOCTORS_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');
		$countries	   							= $this->model_shared->getRecordMultipleWhereOrderBy('*',COUNTRIES_TABLE,array('id !=' => 0),'name','ASC');
		$cities	   								= $this->model_shared->getRecordMultipleWhereOrderBy('*',CITIES_TABLE,array('id !=' => 0),'name','ASC');
		$companies								= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_COMPANIES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');

		$result['assignedRoles']				= $assignedRoles;
		$result['accessModules']				= $accessModules;

		$result['productionStages']  			= $productionStages;

		$result['impressionTypes']  			= $impressionTypes;
		$result['doctors']  					= $doctors;
		$result['countries']  					= $countries;
		$result['cities']  						= $cities;
		$result['companies']  					= $companies;

		$result['productionStages']				= $productionStages;


		$data['userInfo']            			= $userInfo;

		$data['activeMenu']  					= '4';
		$data['activeSubMenu']  				= '4.2';

		$data['pageHeading']    				= "<i class='fa fa-medkit'></i> Cases";
		$data['subHeading']    					= "";

		$data['metaType']     					= 'internal';
		$data['pageTitle']      				= 'Cases | '.DEFAULT_APPLICATION_NAME;

		if (in_array(ROLE_DOCTOR,$assignedRoles))
		{
			$doctorCaseStatus						= doctorCaseStatus(); // Calling From Case Helper
			$result['doctorCaseStatus']  			= $doctorCaseStatus;
			$data['contents']						= $this->load->view('cpanel/cases/doctor-cases',$result,true);
		}
		else
		{
			$data['contents']						= $this->load->view('cpanel/cases/cases',$result,true);
		}


		$this->load->view('cpanel/template',$data);

	}



	function medicalDoctorCases($selectedDoctorID) {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_ROOMS,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;
		$selectedDoctorID       = decodeString($selectedDoctorID);

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 							= $this->assignedRoles;
		$accessModules 							= $this->accessModules;

		$productionStages						= productionStages(); // Calling From Case Helper

		$impressionTypes						= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_IMPRESSION_TYPES_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');
		$doctors								= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DOCTORS_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');
		$countries	   							= $this->model_shared->getRecordMultipleWhereOrderBy('*',COUNTRIES_TABLE,array('id !=' => 0),'name','ASC');
		$cities	   								= $this->model_shared->getRecordMultipleWhereOrderBy('*',CITIES_TABLE,array('id !=' => 0),'name','ASC');
		$companies								= $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_COMPANIES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');

		$result['assignedRoles']				= $assignedRoles;
		$result['accessModules']				= $accessModules;

		$result['productionStages']  			= $productionStages;

		$result['impressionTypes']  			= $impressionTypes;
		$result['doctors']  					= $doctors;
		$result['countries']  					= $countries;
		$result['cities']  						= $cities;
		$result['companies']  					= $companies;

		$result['productionStages']				= $productionStages;
		$result['selectedDoctorID']				= $selectedDoctorID;


		$data['userInfo']            			= $userInfo;

		$data['activeMenu']  					= '4';
		$data['activeSubMenu']  				= '4.2';

		$data['pageHeading']    				= "<i class='fa fa-medkit'></i> Cases";
		$data['subHeading']    					= "";

		$data['metaType']     					= 'internal';
		$data['pageTitle']      				= 'Cases | '.DEFAULT_APPLICATION_NAME;

		if (in_array(ROLE_DOCTOR,$assignedRoles) OR $selectedDoctorID!='')
		{
			$doctorCaseStatus						= doctorCaseStatus(); // Calling From Case Helper
			$result['doctorCaseStatus']  			= $doctorCaseStatus;
			$data['contents']						= $this->load->view('cpanel/cases/doctor-cases',$result,true);
		}
		else
		{
			$data['contents']						= $this->load->view('cpanel/cases/cases',$result,true);
		}


		$this->load->view('cpanel/template',$data);

	}



	function caseDetails($caseID=NULL) {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_DETAILS,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		if ($caseID === NULL) {

			$caseID	 = $this->input->post('case_id');

		} else {

			$caseID = decodeString($caseID); // Calling From General Helper
		}

		if ($caseID === NULL) {

			redirect('my-dashboard/');
		}

		$case  			= $this->model_case->getCaseInfo($caseID);


		$caseFlag 		= false;

		if ($case->num_rows() == 0) {

			   $caseFlag  = false;

		} else {

			 $caseRow = $case->row_array();

			   $caseID = $caseRow['caseID'];
			   $distributor = $caseRow['distributor'];
			   $patient_state = $caseRow['patient_state'];

			   $caseFlag  = true;
		}

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

	    $recordPerPage					= 1000;
		$organizationID					= $this->organizationID;
		$searchParameters 			    = 0;
		$page=0;

		/*$employees = $this->model_employee->getActiveDoctors($page,$recordPerPage,$organizationID,$searchParameters);*/

		$employees          = $this->model_employee->getActiveDoctorsAssignedStateWise($page,$recordPerPage,$organizationID,$searchParameters,$patient_state);

		$caseStatus         = $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_DOCTOR_CASE_STATUS_TABLE,array('is_deleted' => HARD_CODE_ID_NO),'name','ASC');

		$caseLinks          = $this->model_shared->getRecordMultipleWhereOrderBy('upload_link,password',CASE_SETUP_UPLOAD_DATA_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_id_fk' => $caseID),'id','DESC');

		$caseShipmentCount  = $this->model_shared->getRecordMultipleWhereOrderBy('id',INVENTORY_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_reference_number' => $caseID),'id','DESC')->num_rows();

		$case_data  = $this->model_case->checkDoctorCase($caseID)->row();

		$result['employees']            		                 = $employees;
		$result['caseStatus']            		                 = $caseStatus;
		$result['case_data']            		                 = $case_data;
		$result['distributor']            		                 = $distributor;
		$result['patient_state']            	                 = $patient_state;
		$result['caseLinks']            	                     = $caseLinks;
		$result['caseShipmentCount']            	             = $caseShipmentCount;

		$assignedRoles 											 = $this->assignedRoles;
		$accessModules 											 = $this->accessModules;


		$result['assignedRoles']							  	 = $assignedRoles;
		$result['accessModules']							  	 = $accessModules;

		$result['teamID']							  	 		 = $teamID;

		if ($caseFlag == true) {

				$caseStatusReadyArray							 = $this->caseStatusReadyArray;
				$caseStatusProcessArray 						 = $this->caseStatusProcessArray;
				$caseStatusHoldArray 							 = $this->caseStatusHoldArray;
				$caseStatusOFFHoldArray 						 = $this->caseStatusOFFHoldArray;
				$caseStatusDoneArray 							 = $this->caseStatusDoneArray;
				$caseStatusModificationArray 					 = $this->caseStatusModificationArray;
				$caseStatusRejectArray 							 = $this->caseStatusRejectArray;

				$case 											 = $case->row_array();

				$presentStage									 = casePresentStage($caseID); // Calling From Case Helper
				$presentStatus									 = casePresentStatus($caseID); // Calling From Case Helper
				$presentOperator								 = casePresentOperator($caseID); // Calling From Case Helper

				$deliveryServices								 = $this->model_shared->getRecordMultipleWhereOrderBy('*',DELIVERY_SERVICES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');


				$presentCaseOperatorProcessingDone  			 = 0;

				if ($presentStage) {

					$presentStage 		 						 = $presentStage->row_array();
					$presentCaseStageID 			 			 = $presentStage['caseStageID'];
					$presentProductionStageID 					 = $presentStage['productionStageID'];
					$presentProductionStageName 				 = $presentStage['productionStageName'];
				}

				if ($presentStatus) {

					$presentStatus 		 						 = $presentStatus->row_array();
					$presentCaseStatusID 						 = $presentStatus['caseStatusID'];
					$presentStatusID 							 = $presentStatus['statusID'];
					$presentStatusParentID 						 = $presentStatus['statusParentID'];
					$presentStatusName 							 = $presentStatus['statusName'];

				}

				if ($presentOperator) {

					$presentOperator 		 		 			 = $presentOperator->row_array();
					$presentCaseOperatorEmployeeID 				 = $presentOperator['employeeID'];
					$presentCaseOperatorEmployeeCode 			 = $presentOperator['employeeCode'];
					$presentCaseOperatorEmployeeName 			 = $presentOperator['employeeName'];

					$presentCaseOperatorProcessingDone 		 	 = $presentOperator['caseOperatorProcessingDone'];

					$employeeAssignJobs							 = getEmployeeAssignJobs($presentCaseOperatorEmployeeID); // Calling From HR Employees Helper

					if ($employeeAssignJobs) {

						$employeeAssignJob 						 = $employeeAssignJobs->row_array();
						$presentCaseOperatorEmployeeJob			 = $employeeAssignJob['jobPositionName'];

					  } else {

						$presentCaseOperatorEmployeeJob 		 = NULL;
					  }

				} else {

					$presentCaseOperatorEmployeeID 				 = NULL;
					$presentCaseOperatorEmployeeCode 			 = NULL;
					$presentCaseOperatorEmployeeName 			 = NULL;
					$presentCaseOperatorEmployeeJob				 = NULL;

				}

			$caseStausComments 						 	 	  = $this->model_case->getCaseStatusComments($caseID);

			$kitSendReceiveCustomerHistory 					  = $this->model_case->getKitSendReceiveCustomerHistory($caseID);
			$impressionsSendReceiveProductionHistory 		  = $this->model_case->getImpressionsSendReceiveProductionHistory($caseID);
			$alignersSendReceiveCustomerHistory 		 	  = $this->model_case->getAlignersSendReceiveCustomerHistory($caseID);

			$casePhotos										  = $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_PICTURES_TABLE,array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),'id','ASC');

			$doctorCaseHistory 					= $this->model_case->getDoctorCaseHistory($caseID);
			$patientCasePhotos 					= $this->model_case->getPatientCasePhotos($caseID);

			/*print_r($this->db->last_query());*/


			$result['case']            			 			  		 = $case;
			$result['case_ID']            			 			  		 = $case_ID;

			$result['caseStatusReadyArray']					 		 = $caseStatusReadyArray;
			$result['caseStatusProcessArray']				 		 = $caseStatusProcessArray;
			$result['caseStatusHoldArray']				 	 		 = $caseStatusHoldArray;
			$result['caseStatusOFFHoldArray']				 		 = $caseStatusOFFHoldArray;
			$result['caseStatusDoneArray']					 		 = $caseStatusDoneArray;
			$result['caseStatusModificationArray']			 		 = $caseStatusModificationArray;
			$result['caseStatusRejectArray']					     = $caseStatusRejectArray;

			$result['case']            			 					 = $case;

			$result['caseStausComments'] 		  		 			 = $caseStausComments;

			$result['presentCaseStageID']    	 					 = $presentCaseStageID;
			$result['presentProductionStageID'] 			 		 = $presentProductionStageID;
			$result['presentProductionStageName'] 			 		 = $presentProductionStageName;

			$result['presentCaseStatusID']    				  		 = $presentCaseStatusID;
			$result['presentStatusID']    					  		 = $presentStatusID;
			$result['presentStatusParentID']    			  		 = $presentStatusParentID;
			$result['presentStatusName'] 	 				  		 = $presentStatusName;

			$result['presentCaseOperatorEmployeeID']    	  		 = $presentCaseOperatorEmployeeID;
			$result['presentCaseOperatorEmployeeCode'] 		  		 = $presentCaseOperatorEmployeeCode;
			$result['presentCaseOperatorEmployeeName'] 		  		 = $presentCaseOperatorEmployeeName;
			$result['presentCaseOperatorEmployeeJob'] 		  		 = $presentCaseOperatorEmployeeJob;

			$result['presentCaseOperatorProcessingDone'] 	  		 = $presentCaseOperatorProcessingDone;

			$result['kitSendReceiveCustomerHistory'] 	  	  		 = $kitSendReceiveCustomerHistory;
			$result['impressionsSendReceiveProductionHistory'] 	  	 = $impressionsSendReceiveProductionHistory;
			$result['alignersSendReceiveCustomerHistory'] 	  		 = $alignersSendReceiveCustomerHistory;

			$result['doctorCaseHistory'] 	  		 = $doctorCaseHistory;

			$result['casePhotos'] 	  		 						= $casePhotos;
			$result['patientCasePhotos'] 	  		 				= $patientCasePhotos;

			$result['deliveryServices']  = $deliveryServices;
		}

		$result['caseFlag']          						  = $caseFlag;

		$data['userInfo']            						  = $userInfo;

		$data['activeMenu']  								  = '4';
		$data['activeSubMenu']  							  = '';

		$data['pageHeading']    							  = "<i class='fa fa-medkit'></i> Case";
		$data['subHeading']    								  = "# ".$caseID;

		$data['metaType']     								  = 'internal';
		$data['pageTitle']      							  = 'Case Details | '.DEFAULT_APPLICATION_NAME;

		$data['contents']	= $this->load->view('cpanel/cases/case_details',$result,true);	;
		$this->load->view('cpanel/template',$data);

	}

	function caseTimeline($caseID) {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_TIMELINE,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		checkEmptyID($caseID);  // Calling From Shared Helper

		$accountID						= $this->accountID;
		$teamID							= $this->teamID;
		$organizationID 				= $this->organizationID;

		$userInfo 						= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;

		$caseStatusReadyArray				= $this->caseStatusReadyArray;
		$caseStatusProcessArray 			= $this->caseStatusProcessArray;
		$caseStatusHoldArray 				= $this->caseStatusHoldArray;
		$caseStatusOFFHoldArray 			= $this->caseStatusOFFHoldArray;
		$caseStatusDoneArray 				= $this->caseStatusDoneArray;
		$caseStatusModificationArray 		= $this->caseStatusModificationArray;
		$caseStatusRejectArray 				= $this->caseStatusRejectArray;
		$caseStatusUpdateArray 		        = $this->caseStatusUpdateArray;

		$caseID	 							= decodeString($caseID); // Calling From General Helper

		$presentCaseStageID					= 0;
		$presentProductionStageID       	= 0;
		$presentCaseStatusID				= 0;
		$presentStatusID					= 0;
		$presentStatusParentID				= 0;
		$presentStatusName					= NULL;

		$presentCaseOperatorProcessingDone  = 0;

		$case  								= $this->model_case->getCaseInfo($caseID);
		$doctorCaseHistory 					= $this->model_case->getDoctorCaseHistory($caseID);

		if ($case->num_rows() == 0) {

			redirect('my-dashboard');
		}

		$case 											 = $case->row_array();

		$presentStage									 = casePresentStage($caseID); // Calling From Case Helper
		$presentStatus									 = casePresentStatus($caseID); // Calling From Case Helper
		$presentOperator								 = casePresentOperator($caseID); // Calling From Case Helper

		$caseStageHistory								 = $this->model_case->getCaseChangeStageHistory($caseID);

		if ($presentStage) {

			$presentStage 		 						 = $presentStage->row_array();
			$presentCaseStageID 			 			 = $presentStage['caseStageID'];
			$presentProductionStageID 					 = $presentStage['productionStageID'];
		}

		if ($presentStatus) {

			$presentStatus 		 						 = $presentStatus->row_array();
			$presentCaseStatusID 						 = $presentStatus['caseStatusID'];
			$presentStatusID 							 = $presentStatus['statusID'];
			$presentStatusParentID 						 = $presentStatus['statusParentID'];
			$presentStatusName 							 = $presentStatus['statusName'];
		}

		if ($presentOperator) {

			$presentOperator 		 		 			 = $presentOperator->row_array();
			$presentCaseOperatorEmployeeID 				 = $presentOperator['employeeID'];
			$presentCaseOperatorEmployeeCode 			 = $presentOperator['employeeCode'];
			$presentCaseOperatorEmployeeName 			 = $presentOperator['employeeName'];

			$presentCaseOperatorProcessingDone 		 	 = $presentOperator['caseOperatorProcessingDone'];

			$employeeAssignJobs							 = getEmployeeAssignJobs($presentCaseOperatorEmployeeID); // Calling From HR Employees Helper

			if ($employeeAssignJobs) {

				$employeeAssignJob 						 = $employeeAssignJobs->row_array();
				$presentCaseOperatorEmployeeJob			 = $employeeAssignJob['jobPositionName'];

			  } else {

				$presentCaseOperatorEmployeeJob 		 = NULL;
			  }

		} else {

			$presentCaseOperatorEmployeeID 				  = NULL;
			$presentCaseOperatorEmployeeCode 			  = NULL;
			$presentCaseOperatorEmployeeName 			  = NULL;
			$presentCaseOperatorEmployeeJob				  = NULL;

		}
		$result['assignedRoles']						  = $assignedRoles;
		$result['accessModules']						  = $accessModules;

		$result['caseStatusReadyArray']					  = $caseStatusReadyArray;
		$result['caseStatusProcessArray']				  = $caseStatusProcessArray;
		$result['caseStatusHoldArray']				 	  = $caseStatusHoldArray;
		$result['caseStatusOFFHoldArray']				  = $caseStatusOFFHoldArray;
		$result['caseStatusDoneArray']					  = $caseStatusDoneArray;
		$result['caseStatusModificationArray']			  = $caseStatusModificationArray;
		$result['caseStatusRejectArray']				  = $caseStatusRejectArray;
		$result['caseStatusUpdateArray']				  = $caseStatusUpdateArray;

		$result['case']            			 			  = $case;
		$result['doctorCaseHistory']            		  = $doctorCaseHistory;

		$result['presentCaseStageID']    	 			  = $presentCaseStageID;
		$result['presentProductionStageID'] 			  = $presentProductionStageID;

		$result['presentCaseStatusID']    				  = $presentCaseStatusID;
		$result['presentStatusID']    					  = $presentStatusID;
		$result['presentStatusParentID']    			  = $presentStatusParentID;
		$result['presentStatusName'] 	 				  = $presentStatusName;

		$result['presentCaseOperatorEmployeeID']    	  = $presentCaseOperatorEmployeeID;
		$result['presentCaseOperatorEmployeeCode'] 		  = $presentCaseOperatorEmployeeCode;
		$result['presentCaseOperatorEmployeeName'] 		  = $presentCaseOperatorEmployeeName;
		$result['presentCaseOperatorEmployeeJob'] 		  = $presentCaseOperatorEmployeeJob;

		$result['presentCaseOperatorProcessingDone'] 	  = $presentCaseOperatorProcessingDone;

		$result['caseStageHistory']    					  = $caseStageHistory;

		$data['userInfo']            	= $userInfo;

		$data['activeMenu']  			= '4';
		$data['activeSubMenu']  		= '4.1';

		$data['pageHeading']    		= "<i class='fa fa-medkit'></i> Case Timeline - ".$case['patientName'];
		$data['subHeading']    			= "";

		$data['metaType']     			= 'internal';
		$data['pageTitle']      		= 'Cases Timeline | '.DEFAULT_APPLICATION_NAME;

		$data['contents']				= $this->load->view('cpanel/cases/case_timeline',$result,true);
		$this->load->view('cpanel/template',$data);

	}

	function caseTimelineReadOnly($caseID) {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_TIMELINE_READ_ONLY,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		checkEmptyID($caseID);  // Calling From Shared Helper

		$accountID						= $this->accountID;
		$teamID							= $this->teamID;
		$organizationID 				= $this->organizationID;

		$userInfo 						= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 						= $this->assignedRoles;
		$accessModules 						= $this->accessModules;

		$caseStatusReadyArray				= $this->caseStatusReadyArray;
		$caseStatusProcessArray 			= $this->caseStatusProcessArray;
		$caseStatusHoldArray 				= $this->caseStatusHoldArray;
		$caseStatusOFFHoldArray 			= $this->caseStatusOFFHoldArray;
		$caseStatusDoneArray 				= $this->caseStatusDoneArray;
		$caseStatusModificationArray 		= $this->caseStatusModificationArray;
		$caseStatusRejectArray 				= $this->caseStatusRejectArray;

		$caseID	 							= decodeString($caseID); // Calling From General Helper

		$presentCaseStageID					= 0;
		$presentProductionStageID       	= 0;
		$presentCaseStatusID				= 0;
		$presentStatusID					= 0;
		$presentStatusParentID				= 0;
		$presentStatusName					= NULL;

		$presentCaseOperatorProcessingDone  = 0;

		$case  								= $this->model_case->getCaseInfo($caseID);

		if ($case->num_rows() == 0) {

			redirect('my-dashboard');
		}

		$case 											 = $case->row_array();

		$presentStage									 = casePresentStage($caseID); // Calling From Case Helper
		$presentStatus									 = casePresentStatus($caseID); // Calling From Case Helper
		$presentOperator								 = casePresentOperator($caseID); // Calling From Case Helper

		$presentCaseOperatorProcessingDone  			 = 0;

		$caseSetupData									 = $this->model_case->getCaseSetupData($caseID);
		$caseTreatmentData								 = $this->model_case->getCaseTreatmentData($caseID);

		$caseStageHistory								 = $this->model_case->getCaseChangeStageHistory($caseID);

		if ($presentStage) {

			$presentStage 		 						 = $presentStage->row_array();
			$presentCaseStageID 			 			 = $presentStage['caseStageID'];
			$presentProductionStageID 					 = $presentStage['productionStageID'];
		}

		if ($presentStatus) {

			$presentStatus 		 						 = $presentStatus->row_array();
			$presentCaseStatusID 						 = $presentStatus['caseStatusID'];
			$presentStatusID 							 = $presentStatus['statusID'];
			$presentStatusParentID 						 = $presentStatus['statusParentID'];
			$presentStatusName 							 = $presentStatus['statusName'];

		}

		if ($presentOperator) {

			$presentOperator 		 		 			 = $presentOperator->row_array();
			$presentCaseOperatorEmployeeID 				 = $presentOperator['employeeID'];
			$presentCaseOperatorEmployeeCode 			 = $presentOperator['employeeCode'];
			$presentCaseOperatorEmployeeName 			 = $presentOperator['employeeName'];

			$presentCaseOperatorProcessingDone 		 	 = $presentOperator['caseOperatorProcessingDone'];

			$employeeAssignJobs							 = getEmployeeAssignJobs($presentCaseOperatorEmployeeID); // Calling From HR Employees Helper

			if ($employeeAssignJobs) {

				$employeeAssignJob 						 = $employeeAssignJobs->row_array();
				$presentCaseOperatorEmployeeJob			 = $employeeAssignJob['jobPositionName'];

			  } else {

				$presentCaseOperatorEmployeeJob 		 = NULL;
			  }

		} else {

			$presentCaseOperatorEmployeeID 				  = NULL;
			$presentCaseOperatorEmployeeCode 			  = NULL;
			$presentCaseOperatorEmployeeName 			  = NULL;
			$presentCaseOperatorEmployeeJob				  = NULL;

		}

		$result['assignedRoles']						  = $assignedRoles;
		$result['accessModules']						  = $accessModules;

		$result['caseStatusReadyArray']					  = $caseStatusReadyArray;
		$result['caseStatusProcessArray']				  = $caseStatusProcessArray;
		$result['caseStatusHoldArray']				 	  = $caseStatusHoldArray;
		$result['caseStatusOFFHoldArray']				  = $caseStatusOFFHoldArray;
		$result['caseStatusDoneArray']					  = $caseStatusDoneArray;
		$result['caseStatusModificationArray']			  = $caseStatusModificationArray;
		$result['caseStatusRejectArray']				  = $caseStatusRejectArray;


		$result['case']            			 			  = $case;
		$result['caseSetupData']            			  = $caseSetupData;
		$result['caseTreatmentData']            		  = $caseTreatmentData;

		$result['presentCaseStageID']    	 			  = $presentCaseStageID;
		$result['presentProductionStageID'] 			  = $presentProductionStageID;

		$result['presentCaseStatusID']    				  = $presentCaseStatusID;
		$result['presentStatusID']    					  = $presentStatusID;
		$result['presentStatusParentID']    			  = $presentStatusParentID;
		$result['presentStatusName'] 	 				  = $presentStatusName;

		$result['presentCaseOperatorEmployeeID']    	  = $presentCaseOperatorEmployeeID;
		$result['presentCaseOperatorEmployeeCode'] 		  = $presentCaseOperatorEmployeeCode;
		$result['presentCaseOperatorEmployeeName'] 		  = $presentCaseOperatorEmployeeName;
		$result['presentCaseOperatorEmployeeJob'] 		  = $presentCaseOperatorEmployeeJob;

		$result['presentCaseOperatorProcessingDone'] 	  = $presentCaseOperatorProcessingDone;

		$result['caseStageHistory']    					  = $caseStageHistory;

		$data['userInfo']            					  = $userInfo;

		$data['activeMenu']  							  = '3';
		$data['activeSubMenu']  						  = '';

		$data['pageHeading']    						  = "<i class='fa fa fa-clock-o'></i> Timeline - ".$case['patientName'];
		$data['subHeading']    							  = "";

		$data['metaType']     							 = 'internal';
		$data['pageTitle']      						 = 'Cases Timeline | '.DEFAULT_APPLICATION_NAME;

		$data['contents']				= $this->load->view('cpanel/cases/case_timeline_read_only',$result,true);
		$this->load->view('cpanel/template',$data);

	}


	function casePictures($ID) {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_ADD_NEW,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		checkEmptyID($ID);  // Calling From Shared Helper

		$ID						= decodeString($ID); // Calling From General Helper

		$record  = $this->model_shared->getRecordMultipleWhere('id,patient',MEDICAL_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $ID));

		if ($record->num_rows() == 0 ) {

				redirect('cases-list/');
		}

		$row					= $record->row_array();

		$caseID					= $row['id'];

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 					= $this->assignedRoles;
		$accessModules 					= $this->accessModules;

		$accountID						= $this->accountID;
		$teamID							= $this->teamID;
		$organizationID 				= $this->organizationID;

		$userInfo 						= userInfo($accountID); // Calling From Application Helper

		$result['assignedRoles']		= $assignedRoles;
		$result['accessModules']		= $accessModules;

		$photos		 = $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_PICTURES_TABLE,array('case_id' => $ID,'is_deleted' => HARD_CODE_ID_NO),'id','ASC');

		$result['ID']					= $ID;
		$result['row']					= $row;
		$result['caseID']				= $caseID;
		$result['photos']				= $photos;

		$data['userInfo']            	= $userInfo;

		$data['activeMenu']  			= '4';
		$data['activeSubMenu']  		= '';

		$data['pageHeading']    		= "<i class='fa fa-camera'></i> Manage Pictures <small>(".$row['patient'].")</small>";
		$data['subHeading']    			= "";

		$data['metaType']     			= 'internal';
		$data['pageTitle']      		= 'Manage Cases | '.DEFAULT_APPLICATION_NAME;

		$data['contents']				= $this->load->view('cpanel/cases/case_pictures',$result,true);
		$this->load->view('cpanel/template',$data);
	}

	function uploadCasePictures($ID) {

			$accountID				= $this->accountID;
			$teamID					= $this->teamID;
			$organizationID 		= $this->organizationID;

			$ID						= decodeString($ID); // Calling From General Helper

			if ($caseID) {

				$caseID				= decodeString($caseID); // Calling From General Helper
			}

			if ($_FILES['file']['name']) {

				$fname			= convertAllCharactersLowercase(time().'_'.basename($_FILES['file']['name'])); // Calling From General Helper
				$fname 			= str_replace(" ","_",$fname);
				$fname 			= str_replace("%","_",$fname);
				$nameExt 		= @convertAllCharactersLowercase(end(explode(".", basename($_FILES['file']['name'])))); // Calling From General Helper

				$fileName		= convertAllCharactersLowercase(str_replace('.'.$nameExt,'',basename($_FILES['file']['name']))); // Calling From General Helper

				$uploaddir 		= CASE_PICTURES_PATH_DIRECTORY_PATH;
				$uploadfile 	= $uploaddir .$fname;

				if($nameExt == 'jpg'  || $nameExt == 'jpeg' ||  $nameExt =='gif' || $nameExt =='png' || $nameExt =='pdf')  {

				// if($_FILES["file"]["size"] <= 1000000) {

					if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {

						  $source_image_path    = CASE_PICTURES_PATH_DIRECTORY_PATH;
						  $source_image_name    = $fname;

						  $this->resizeImage($source_image_path,CASE_PICTURES_PATH_THUMBNAIL_DIRECTORY_PATH,$fname,"thumbnail_",100,100);

						  $data['case_id']  								=   $ID;
						  $data['name']  									=   $fname;
						  $data['type']  									=   $nameExt;
						  $data['is_deletable']								=	HARD_CODE_ID_YES;
						  $data['created']									=  	DATABASE_NOW_DATE_TIME_FORMAT();
						  $data['created_by']								=   $teamID;
						  $data['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

						  // Save All data Into Database Table
						  $insertedID									= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_PICTURES_TABLE,$data);
					   }

				//  }
				}


		}
	}

	function removeCasePictures($ID) {

			$accountID				= $this->accountID;
			$teamID					= $this->teamID;
			$organizationID 		= $this->organizationID;

			if ($ID) {

				$caseID	= decodeString($ID); // Calling From General Helper

			} else {

				 redirect('cases-list/');
			}

			$record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $caseID));

			if ($record->num_rows() == 0 ) {

				redirect('cases-list/');
			}

			$casePhotos	 = $this->model_shared->getRecordMultipleWhereOrderBy('*',MEDICAL_CASE_PICTURES_TABLE,array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),'id','ASC');

			 if ($casePhotos->num_rows() > 0) {

					 foreach($casePhotos->result() as $photo) {

						  @unlink(CASE_PICTURES_PATH_DIRECTORY_PATH.$photo->name);
						  @unlink(CASE_PICTURES_PATH_THUMBNAIL_DIRECTORY_PATH."thumbnail_".$photo->name);
					 }

				$this->model_shared->deleteRecord('case_id',$caseID,MEDICAL_CASE_PICTURES_TABLE);

				// Set Session Message If Everything Goes Fine
				$this->session->set_userdata('admin_msg','Photos deleted successfully.');

			 }

			 redirect('manage-case-pictures/'.encodeString($caseID));
	}


	function kitSendToCustomer($caseID=NULL) {

		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_DETAILS,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		if ($caseID === NULL) {

			redirect('my-dashboard/');
		}

		$caseID = decodeString($caseID); // Calling From General Helper

		$case  			= $this->model_case->getCaseInfo($caseID);

		$caseFlag 		= false;

		if ($case->num_rows() == 0) {

			   $caseRow = $case->row_array();

				$caseID = $caseRow['caseID'];

				$caseFlag  = true;

		} else {

			$caseFlag  = true;
		}

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 											 = $this->assignedRoles;
		$accessModules 											 = $this->accessModules;


		/* Set Form Validation Errors */
		$this->form_validation->set_rules('tracking_number_send_customer','tracking number','trim|required');
		$this->form_validation->set_rules('delivery_service_send_customer','courier service','trim|required');
		$this->form_validation->set_rules('tracking_number_receive_customer','tracking number','trim|required');
		$this->form_validation->set_rules('delivery_service_receive_customer','courier service','trim|required');
		$this->form_validation->set_rules('comments','comments','trim');

		$this->form_validation->set_error_delimiters('<span class="help-block text-red">', '<span>');

		if($this->form_validation->run() === FALSE ) {


				$result['assignedRoles']							 = $assignedRoles;
				$result['accessModules']						     = $accessModules;

				if ($caseFlag == true) {

						$caseStatusReadyArray							 = $this->caseStatusReadyArray;
						$caseStatusProcessArray 						 = $this->caseStatusProcessArray;
						$caseStatusHoldArray 							 = $this->caseStatusHoldArray;
						$caseStatusOFFHoldArray 						 = $this->caseStatusOFFHoldArray;
						$caseStatusDoneArray 							 = $this->caseStatusDoneArray;
						$caseStatusModificationArray 					 = $this->caseStatusModificationArray;
						$caseStatusRejectArray 							 = $this->caseStatusRejectArray;

						$case 											 = $case->row_array();

						$presentStage									 = casePresentStage($caseID); // Calling From Case Helper
						$presentStatus									 = casePresentStatus($caseID); // Calling From Case Helper
						$presentOperator								 = casePresentOperator($caseID); // Calling From Case Helper

						$deliveryServices								 = $this->model_shared->getRecordMultipleWhereOrderBy('*',DELIVERY_SERVICES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED,'is_deleted' => HARD_CODE_ID_NO),'name','ASC');


						$presentCaseOperatorProcessingDone  			 = 0;

						if ($presentStage) {

							$presentStage 		 						 = $presentStage->row_array();
							$presentCaseStageID 			 			 = $presentStage['caseStageID'];
							$presentProductionStageID 					 = $presentStage['productionStageID'];
							$presentProductionStageName 				 = $presentStage['productionStageName'];
						}

						if ($presentStatus) {

							$presentStatus 		 						 = $presentStatus->row_array();
							$presentCaseStatusID 						 = $presentStatus['caseStatusID'];
							$presentStatusID 							 = $presentStatus['statusID'];
							$presentStatusParentID 						 = $presentStatus['statusParentID'];
							$presentStatusName 							 = $presentStatus['statusName'];

						}

						if ($presentOperator) {

							$presentOperator 		 		 			 = $presentOperator->row_array();
							$presentCaseOperatorEmployeeID 				 = $presentOperator['employeeID'];
							$presentCaseOperatorEmployeeCode 			 = $presentOperator['employeeCode'];
							$presentCaseOperatorEmployeeName 			 = $presentOperator['employeeName'];

							$presentCaseOperatorProcessingDone 		 	 = $presentOperator['caseOperatorProcessingDone'];

							$employeeAssignJobs							 = getEmployeeAssignJobs($presentCaseOperatorEmployeeID); // Calling From HR Employees Helper

							if ($employeeAssignJobs) {

								$employeeAssignJob 						 = $employeeAssignJobs->row_array();
								$presentCaseOperatorEmployeeJob			 = $employeeAssignJob['jobPositionName'];

							  } else {

								$presentCaseOperatorEmployeeJob 		 = NULL;
							  }

						} else {

							$presentCaseOperatorEmployeeID 				 = NULL;
							$presentCaseOperatorEmployeeCode 			 = NULL;
							$presentCaseOperatorEmployeeName 			 = NULL;
							$presentCaseOperatorEmployeeJob				 = NULL;

						}

						if ($presentStatusID != CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY) {

							redirect('cases-list/');
						}

					$result['case']            			 			  = $case;

					$result['caseStatusReadyArray']					  = $caseStatusReadyArray;
					$result['caseStatusProcessArray']				  = $caseStatusProcessArray;
					$result['caseStatusHoldArray']				 	  = $caseStatusHoldArray;
					$result['caseStatusOFFHoldArray']				  = $caseStatusOFFHoldArray;
					$result['caseStatusDoneArray']					  = $caseStatusDoneArray;
					$result['caseStatusModificationArray']			  = $caseStatusModificationArray;
					$result['caseStatusRejectArray']				  = $caseStatusRejectArray;

					$result['case']            			 			  = $case;

					$result['presentCaseStageID']    	 			  = $presentCaseStageID;
					$result['presentProductionStageID'] 			  = $presentProductionStageID;
					$result['presentProductionStageName'] 			  = $presentProductionStageName;

					$result['presentCaseStatusID']    				  = $presentCaseStatusID;
					$result['presentStatusID']    					  = $presentStatusID;
					$result['presentStatusParentID']    			  = $presentStatusParentID;
					$result['presentStatusName'] 	 				  = $presentStatusName;

					$result['presentCaseOperatorEmployeeID']    	  = $presentCaseOperatorEmployeeID;
					$result['presentCaseOperatorEmployeeCode'] 		  = $presentCaseOperatorEmployeeCode;
					$result['presentCaseOperatorEmployeeName'] 		  = $presentCaseOperatorEmployeeName;
					$result['presentCaseOperatorEmployeeJob'] 		  = $presentCaseOperatorEmployeeJob;

					$result['deliveryServices']            			  = $deliveryServices;
				}

				$result['caseFlag']          						  = $caseFlag;

				$data['userInfo']            						  = $userInfo;

				$data['activeMenu']  								  = '4';
				$data['activeSubMenu']  							  = '';

				$data['pageHeading']    							  = "<i class='fa fa-medkit'></i> Case";
				$data['subHeading']    								  = "# ".$caseID;

				$data['metaType']     								  = 'internal';
				$data['pageTitle']      							  = 'Kit Send to Customer | '.DEFAULT_APPLICATION_NAME;

				$data['contents']	= $this->load->view('cpanel/cases/kit_send_to_customer',$result,true);	;
				$this->load->view('cpanel/template',$data);

		} else {

				// Prepair Date To Store In Database
				$caseStageID						= ($this->input->post('presentCaseStageID') ?: NULL);

				$trackingNumberSendCustomer			= ($this->input->post('tracking_number_send_customer') ?: NULL);
				$deliveryServiceSendCustomer		= ($this->input->post('delivery_service_send_customer') ?: NULL);

				$trackingNumberReceiveCustomer		= ($this->input->post('tracking_number_receive_customer') ?: NULL);
				$deliveryServiceReceiveCustomer		= ($this->input->post('delivery_service_receive_customer') ?: NULL);

				$comments				   			= ($this->input->post('comments') ?: NULL);

				$data['case_id']								=	$caseID;
				$data['tracking_number_sending']				=	$trackingNumberSendCustomer;
				$data['delivery_service_sending']				=	$deliveryServiceSendCustomer;
				$data['tracking_number_receiving']				=	$trackingNumberReceiveCustomer;
				$data['delivery_service_receiving']				=	$deliveryServiceReceiveCustomer;
				$data['description']							=	$comments;

				$data['is_deletable']							=	HARD_CODE_ID_YES;

				$data['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
				$data['created_by']								=   $teamID;
				$data['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

				// Save All data Into Database Table
				$insertedD										= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_KIT_SEND_RECEIVE_CUSTOMER_TABLE,$data);


				 $dataStatus['case_id']							=	$caseID;
				 $dataStatus['case_stage_id']					=	$caseStageID;
				 $dataStatus['status_id']						=	CASE_STATUS_KIT_SHIP_TO_CUSTOMER_SHIPPED;
				 $dataStatus['description']						=	$comments;

				 $dataStatus['is_deletable']					=	HARD_CODE_ID_YES;

				 $dataStatus['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
				 $dataStatus['created_by']						=   $teamID;
				 $dataStatus['created_by_reference_table']		=   'MY_ORGANIZATION_TEAM_TABLE';

				 // Save All data Into Database Table
				 $insertedStatusID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$dataStatus);

				 // Update Case Table After Assign Stage And Status
				 $caseUpdate['present_case_status_id']	=	$insertedStatusID;

				 // Update Record Table
				 $this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$caseUpdate);


				 // Case AutoMove
				 $this->autoMoveCase($caseID);

				 redirect('kit-send-to-customer/'.encodeString($caseID));

		}
	}

	function impressionsSendToProduction() {

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 											 = $this->assignedRoles;
		$accessModules 											 = $this->accessModules;

		// Prepair Date To Store In Database
		$trackingNumber			= ($this->input->post('tracking_number') ?: NULL);
		$deliveryService		= ($this->input->post('courier_service') ?: NULL);

		$deliveryServiceQuery   = $this->model_shared->getRecordMultipleWhere('name',DELIVERY_SERVICES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $deliveryService));
	    $ds_row	                = $deliveryServiceQuery->row_array();
	    $deliveryServiceName    =  $ds_row['name'];

		$case  =	($this->input->post('list_case') ?: NULL);
		$number_of_cases  =	count($this->input->post('list_case',TRUE));

		if (!empty($case)) {

			for($i=0; $i<sizeof($case); $i++) {

			  	$caseID  = decodeString($case[$i]);

				$caseStageID 	= $this->model_shared->getRecordMultipleWhere('present_case_stage_id',MEDICAL_CASES_TABLE,array('id' => $caseID));

				if ($caseStageID->num_rows() > 0) {

					$caseStageID							= $caseStageID->row_array();
					$caseStageID 							= $caseStageID['present_case_stage_id'];
				}





				//for sending data into other portal(clearpath portal) db
				 $record  = $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $caseID));
				 $row	  = $record->row_array();



				$inventory_cases_data['case_reference_number']    = $caseID;
				$inventory_cases_data['patient']                  = $row['patient'];
				$inventory_cases_data['age']                      = $row['age'];
				$inventory_cases_data['gender']                   = $row['gender'];
				$inventory_cases_data['patient_country']          = $row['patient_country'];
				$inventory_cases_data['patient_state']            = $row['patient_state'];
				$inventory_cases_data['patient_city']             = $row['patient_city'];
				/*$inventory_cases_data['receive_date']             = $row['receive_date'];*/
				$inventory_cases_data['impression_type']          = $row['impression_type'];
				$inventory_cases_data['arch_upper']               = $row['arch_upper'];
				$inventory_cases_data['arch_lower']               = $row['arch_lower'];
				$inventory_cases_data['country']                  = $row['country'];
				$inventory_cases_data['city']                     = $row['city'];
				$inventory_cases_data['distributor']              = $row['distributor'];
				$inventory_cases_data['description']              = $row['description'];
				$inventory_cases_data['RX_form']                  = $row['RX_form'];
				$inventory_cases_data['x_rays_opg']               = $row['x_rays_opg'];
				$inventory_cases_data['x_rays_ceph']              = $row['x_rays_ceph'];
				$inventory_cases_data['file_assessment']          = $row['file_assessment'];
				$inventory_cases_data['patient']                  = $row['patient'];
				$inventory_cases_data['patient']                  = $row['patient'];
				$inventory_cases_data['created']			      = DATABASE_NOW_DATE_TIME_FORMAT();
				$inventory_cases_data['created_by']		          =  $teamID;

				$checksAlreadyPresentCase 	= $this->model_shared->getRecordMultipleWhere('id',INVENTORY_CASES_TABLE,array('case_reference_number' => $caseID));

				if ($checksAlreadyPresentCase->num_rows() > 0) {
					$inventory_cases_data['push_status']              = HARD_CODE_ID_NOT_PUSHED;
					// update All data Into tbl_application_inventory_cases
				    $inventoryCasesID 	= $this->model_shared->updateRecordReturnID(array('case_reference_number' => $caseID,'is_deleted' => HARD_CODE_ID_NO),INVENTORY_CASES_TABLE,$inventory_cases_data);
				}

				else
				{
					// Save All data Into tbl_application_inventory_cases
				    $inventoryCasesID									 = 	$this->model_shared->insertRecord_ReturnID(INVENTORY_CASES_TABLE,$inventory_cases_data);
				}




				$inventory_record_data['sent_date']                    = DATABASE_NOW_DATE_FORMAT();
				$inventory_record_data['country']                      = $row['patient_country'];
				$inventory_record_data['city']                         = $row['patient_city'];
				$inventory_record_data['distributor']                  = $row['distributor'];
				$inventory_record_data['number_of_cases']              = $number_of_cases;
				$inventory_record_data['delivery_service']             = $deliveryService;
				$inventory_record_data['airway_bill_number']           = removeAllSpacesFromString(cleanSringFromDashes(cleanSringFromBrackets($trackingNumber)));
				$inventory_record_data['airway_bill_number_formatted'] = $trackingNumber;
				$inventory_record_data['description']		           =	NULL;
				$inventory_record_data['created']			           = DATABASE_NOW_DATE_TIME_FORMAT();
				$inventory_record_data['created_by']		           =  $teamID;

				// Save All data Into tbl_application_medical_cases_temporary_inventory_record
				if($i==0)
				{

				 $inventoryRecordID									 = 	$this->model_shared->insertRecord_ReturnID(INVENTORY_RECORD_TABLE,$inventory_record_data);
				}


				$checksAlreadyPresentInventoryItems 	= $this->model_shared->getRecordMultipleWhere('id',INVENTORY_ITEMS_TABLE,array('inventory_id' => $inventoryRecordID,'case_reference_number' => $caseID));

				if ($checksAlreadyPresentInventoryItems->num_rows() == 0) 
				{
					$inventory_items_data['inventory_id']               = $inventoryRecordID;
					$inventory_items_data['case_reference_number']      = $caseID;
					$inventory_items_data['description']		        =	NULL;
					$inventory_items_data['created']			        = DATABASE_NOW_DATE_TIME_FORMAT();
					$inventory_items_data['created_by']		            =  $teamID;

					// Save All data Into tbl_application_medical_cases_temporary_inventory_items
					 $inventoryItemsID									= 	$this->model_shared->insertRecord_ReturnID(INVENTORY_ITEMS_TABLE,$inventory_items_data);
				}


				// extra check to make sure that number_of_cases colum value be correct start
				$casesCount 	= $this->model_shared->getRecordMultipleWhere('id',INVENTORY_ITEMS_TABLE,array('inventory_id' => $inventoryRecordID))->num_rows();

				$inventory_record_update_data['number_of_cases']              = $casesCount;
				$this->model_shared->updateRecordReturnID(array('id' => $inventoryRecordID,'is_deleted' => HARD_CODE_ID_NO),INVENTORY_RECORD_TABLE,$inventory_record_update_data);
				// extra check to make sure that number_of_cases colum value be correct end

				 //for sending data into other portal(clearpath portal) db ends






				$data['case_id']								=	$caseID;
				$data['tracking_number_sending']				=	$trackingNumber;
				$data['delivery_service_sending']				=	$deliveryService;
				$data['description']							=	NULL;

				$data['is_deletable']							=	HARD_CODE_ID_YES;

				$data['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
				$data['created_by']								=   $teamID;
				$data['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

				 // Save All data Into Database Table
				 $insertedD										= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_IMPRESSIONS_SEND_RECEIVE_PRODUCTION_TABLE,$data);


				 // Update Case Status
				 $dataStatus['case_id']							=	$caseID;
				 $dataStatus['case_stage_id']					=	$caseStageID;
				 $dataStatus['status_id']						=	CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT;
				 $dataStatus['description']						=	NULL;

				 $dataStatus['is_deletable']					=	HARD_CODE_ID_YES;

				 $dataStatus['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
				 $dataStatus['created_by']						=   $teamID;
				 $dataStatus['created_by_reference_table']		=   'MY_ORGANIZATION_TEAM_TABLE';

				 // Save All data Into Database Table
				 $insertedStatusID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$dataStatus);

				 // Update Case Table After Assign Stage And Status
				 $caseUpdate['present_case_status_id']	=	$insertedStatusID;

				 // Update Record Table
				 $this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$caseUpdate);

				$company =  $row['distributor'];
			    if($company==ALIGNERCO)
				{
					$companyEmail=ALIGNERCO_EMAIL;
					$companyPassword=ALIGNERCO_EMAIL_PASSWORD;
					$email_data['companyName']	= 'AlignerCo';
				}
				if($company==ALIGNERCO_CANADA)
				{
					$companyEmail=ALIGNERCO_CANADA_EMAIL;
					$companyPassword=ALIGNERCO_CANADA_EMAIL_PASSWORD;
					$email_data['companyName']	= 'AlignerCo Canada';
				}
				if($company==STRAIGHT_MY_TEETH)
				{
					$companyEmail=STRAIGHT_MY_TEETH_EMAIL;
					$companyPassword=STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
					$email_data['companyName']	= 'Straight My Teeth';
				}
				if($company==SMILEPATH)
				{
					$companyEmail=SMILEPATH_EMAIL;
					$companyPassword=SMILEPATH_EMAIL_PASSWORD;
					$email_data['companyName']	= 'SmilePath';
				}


				 /*===================email code start================*/
				if($i==0)
				{
                   $this->load->library('email');
                    $config = Array(
                    'protocol' => 'smtp',
	                'smtp_host' => 'ssl://smtp.gmail.com',
	                'smtp_port' => 465,
	                'smtp_user' => $companyEmail,
	                'smtp_pass' => $companyPassword,
	                'smtp_timeout'=>20,
	                'mailtype'  => 'html',
	                'charset' => 'iso-8859-1',
	                'wordwrap' => TRUE
					);


                    $this->load->library('email');
                    $this->email->initialize($config);
                    $this->email->set_mailtype("html");
                    $this->email->set_newline("\r\n");

                    $email_data['case']			            = $case;
                    $email_data['trackingNumber']			= $trackingNumber;
                    $email_data['deliveryServiceName']	    = $deliveryServiceName;

                    $message =$this->load->view('cpanel/emails/production_email', $email_data, true);
                    $to_email = PRODUCTION_EMAIL;
                 
                    $this->email->to($to_email);
                    $this->email->subject('New Shipment');
                    $this->email->message($message);

                    if($this->email->send())
                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
                    else
                        $this->session->set_flashdata("email_sent","You have encountered an error");

				}

				/*===========================email code end====================*/


			}
		 }



	redirect('cases-list');

	}



	function assignCaseToDoctor() {


		/*$this->load->library('email');
        $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'no-reply@clearpathortho.com',
                'smtp_pass' => 'Pak!stan', // change it to yours
                'smtp_timeout'=>20,
                'mailtype' => 'text',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
               );

         $this->load->library('email');
         $this->email->initialize($config);// add this line

        $this->email->set_newline("\r\n");
        $this->email->from('noreplay.nagios@gmail.com', 'CP');
        $this->email->to('fahadluck688@gmail.com');
        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');
        $this->email->send();
        $this->email->print_debugger();*/



		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 											 = $this->assignedRoles;
		$accessModules 											 = $this->accessModules;

		// Prepair Date To Store In Database
		$link			= ($this->input->post('link') ?: NULL);
		$code		= ($this->input->post('code') ?: NULL);
		$doctor		= ($this->input->post('doctor') ?: NULL);
		$case  =	($this->input->post('list_case') ?: NULL);

		$employees	   					 				    = $this->model_employee->getSingalActiveEmployees($organizationID,$doctor);

		$doctor_approve_status  =	($this->input->post('doctor_approve_status') ?: NULL);
		$patient_approve_status  =	($this->input->post('patient_approve_status') ?: NULL);

		if(isset($doctor_approve_status))
		{
			$status_id=$doctor_approve_status;
		}
		else
		{
			$status_id=PENDING;
		}

		if(isset($patient_approve_status))
		{
			$patientApproveStatus=$patient_approve_status;
		}
		else
		{
			$patientApproveStatus=NULL;
		}

		if (!empty($case)) {

			for($i=0; $i<sizeof($case); $i++) {

			  	$caseID  = decodeString($case[$i]);
				$caseStageID 	= $this->model_shared->getRecordMultipleWhere('present_case_stage_id',MEDICAL_CASES_TABLE,array('id' => $caseID));

				if ($caseStageID->num_rows() > 0) {

					$caseStageID							= $caseStageID->row_array();
					$caseStageID 							= $caseStageID['present_case_stage_id'];
				}

				$check_case_data=$this->model_case->checkDoctorCase($caseID);
				$case_num_rows=$check_case_data->num_rows();

				if($case_num_rows==0 || true)
				{
					$data['case_id']								=	$caseID;
					$data['doctor_id']								=	$doctor;
					$data['status_id']							    =	$status_id;
					/*$data['patient_approve_status']					=	$patientApproveStatus;*/
					$data['link']				                    =	$link;
					$data['code']				                    =	$code;
					$data['description']							=	NULL;

					$data['is_deletable']							=	HARD_CODE_ID_YES;
					$data['receive_date']							=  	date("Y-m-d");

					$data['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
					$data['created_by']								=   $teamID;
					$data['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

					// Save All data Into Database Table

				    /*$insertedD										= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data);*/

				    $this->model_shared->editRecordWhere(array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data);

				    $doctorEmail=$employees->employeeEmail;
                    //$doctorEmail='naveed.arif@clearpathortho.com';

					$company=$employees->companyID;

					if($company==ALIGNERCO)
					{
						$companyEmail=ALIGNERCO_EMAIL;
						$companyPassword=ALIGNERCO_EMAIL_PASSWORD;
						$email_data['companyName']	= 'AlignerCo';
					}
					if($company==ALIGNERCO_CANADA)
					{
						$companyEmail=ALIGNERCO_CANADA_EMAIL;
						$companyPassword=ALIGNERCO_CANADA_EMAIL_PASSWORD;
						$email_data['companyName']	= 'AlignerCo Canada';
					}
					if($company==STRAIGHT_MY_TEETH)
					{
						$companyEmail=STRAIGHT_MY_TEETH_EMAIL;
						$companyPassword=STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
						$email_data['companyName']	= 'Straight My Teeth';
					}
					if($company==SMILEPATH)
					{
						$companyEmail=SMILEPATH_EMAIL;
						$companyPassword=SMILEPATH_EMAIL_PASSWORD;
						$email_data['companyName']	= 'SmilePath';
					}

					$email_data['case_id']								=	$caseID;
					$email_data['link']				                    =	$link;
					$email_data['code']				                    =	$code;
					$email_data['doctorName']				            =	$employees->employeeName;

				    /*===================email code start================*/
                /*    $this->load->library('email');
                    $config = Array(
                    'protocol' => 'smtp',
	                'smtp_host' => 'ssl://smtp.gmail.com',
	                'smtp_port' => 465,
	                'smtp_user' => 'no-reply@clearpathortho.com',
	                'smtp_pass' => 'Pak!stan',
	                'smtp_timeout'=>20,
	                'mailtype'  => 'html',
	                'charset' => 'iso-8859-1',
	                'wordwrap' => TRUE
                    );
                */



					$this->load->library('email');
                    $config = Array(
                    'protocol' => 'smtp',
	                'smtp_host' => 'ssl://smtp.gmail.com',
	                'smtp_port' => 465,
	                'smtp_user' => $companyEmail,
	                'smtp_pass' => $companyPassword,
	                'smtp_timeout'=>20,
	                'mailtype'  => 'html',
	                'charset' => 'iso-8859-1',
	                'wordwrap' => TRUE
					);


                    $this->load->library('email');
                    $this->email->initialize($config);
                    $this->email->set_mailtype("html");
                    $this->email->set_newline("\r\n");

                    $message =$this->load->view('cpanel/emails/case_assign_email', $email_data, true);

                    $this->email->to($doctorEmail);
                    $this->email->subject('New Case Assign');
                    $this->email->message($message);

                    if($this->email->send())
                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
                    else
                        $this->session->set_flashdata("email_sent","You have encountered an error");

//                    echo $this->email->print_debugger();  die();

                    /*===========================email code end====================*/

				}
				else
				{
					 $data['case_id']								=	$caseID;
					 $data['doctor_id']								=	$doctor;
					 $data['status_id']							    =	$status_id;
					/* $data['patient_approve_status']				=	$patientApproveStatus;*/
					 $data['link']				                    =	$link;
					 $data['code']				                    =	$code;
					 $data['description']							=	NULL;

					  $this->model_shared->editRecordWhere(array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data);
				}


				$data_history['case_id']								=	$caseID;
				$data_history['status_id']							    =	$status_id;
			    /*$data_history['patient_approve_status']					=	$patientApproveStatus;*/
				$data_history['link']				                    =	$link;
				$data_history['code']				                    =	$code;
				$data_history['description']							=	NULL;

				$data_history['is_deletable']							=	HARD_CODE_ID_YES;
				$data_history['receive_date']							=  	date("Y-m-d");

				$data_history['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
				$data_history['created_by']								=   $teamID;
				$data_history['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

				 $addhistoryId= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE,$data_history);


				 // Update Case Status
				 /*$dataStatus['case_id']							=	$caseID;
				 $dataStatus['case_stage_id']					=	$caseStageID;
				 $dataStatus['status_id']						=	CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT;
				 $dataStatus['description']						=	NULL;

				 $dataStatus['is_deletable']					=	HARD_CODE_ID_YES;

				 $dataStatus['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
				 $dataStatus['created_by']						=   $teamID;
				 $dataStatus['created_by_reference_table']		=   'MY_ORGANIZATION_TEAM_TABLE';

				 $insertedStatusID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$dataStatus);*/

				 // Update Case Table After Assign Stage And Status
				/* $caseUpdate['present_case_status_id']	=	$insertedStatusID;*/

				 // Update Record Table
				 /*$this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$caseUpdate);*/

			}
		 }




	redirect('cases-list');

	}


	function updatepatientApproveStatus()
	{
		 $teamID					                    = $this->teamID;
		 $caseID=$this->input->post('caseID',TRUE);
		 $check_case_data=$this->model_case->checkDoctorCaseForPatient($caseID);
		 $case_num_rows=$check_case_data->num_rows();

		 if($case_num_rows==0)
		 {
		 	 $data['case_id']				                =	$caseID;
			 $data['patient_approve_status']				=	APPROVED;
			 $data['description']							=	NULL;
			 $data['is_deletable']							=	HARD_CODE_ID_YES;
			 /*$data['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
			 $data['created_by']							=   $teamID;
			 $data['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';*/
			 $this->model_shared->insertRecord(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data);
		 }
		 else
		 {
		 	 $update_data['case_id']				                =	$caseID;
			 $update_data['patient_approve_status']				    =	APPROVED;
			 $update_data['description']							=	NULL;
			 $update_data['is_deletable']							=	HARD_CODE_ID_YES;
		     $this->model_shared->editRecordWhere(array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$update_data);
		 }

	}

	function removePatientApproveStatus()
	{
		 $caseID=$this->input->post('caseID',TRUE);
		 $data['patient_approve_status']				=	NULL;
		 $this->model_shared->editRecordWhere(array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data);

	}



	function changeDoctorCaseStatus() {

		/*$connection=$this->Connection_model->connection();
		echo "here";
		echo'connection'.$connection;
		$query2="INSERT INTO services (name, description)
               VALUES ('fahad', 'test')" ;
               $result2=mysqli_query($connection, $query2) or die("Can not fetch data sfrom database".mysqli_error($connection));*/


		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 											 = $this->assignedRoles;
		$accessModules 											 = $this->accessModules;

		// Prepair Date To Store In Database
		$caseStatusID			= ($this->input->post('caseStatusID') ?: NULL);
		$comment		= ($this->input->post('comment') ?: NULL);
		$case  =	($this->input->post('list_case') ?: NULL);
		$link		= ($this->input->post('link') ?: NULL);
		$code		= ($this->input->post('code') ?: NULL);

		$employees	   					 				    = $this->model_employee->getSingalActiveEmployees($organizationID,$teamID);

		if($caseStatusID==4)
		{
			$historyComment=$comment;
		}
		else
		{

		}


		if (!empty($case)) {

			for($i=0; $i<sizeof($case); $i++) {

			  	  $caseID  = decodeString($case[$i]);

				 $data['status_id']								=$caseStatusID;
				 $data['comment']				                    =$comment;

				  $this->model_shared->editRecordWhere(array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE,$data);

                // adding history
				$data_history['case_id']								=	$caseID;
				$data_history['status_id']							    =	$caseStatusID;
				$data_history['link']				                    =	$link;
				$data_history['code']				                    =	$code;
				$data_history['comment']				                =	$historyComment;
				$data_history['description']							=	NULL;

				$data_history['is_deletable']							=	HARD_CODE_ID_YES;
				$data_history['receive_date']							=  	date("Y-m-d");

				$data_history['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
				$data_history['created_by']								=   $teamID;
				$data_history['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

				 $addhistoryId= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE,$data_history);


				 	//email variables and logics
				    $doctorEmail=$employees->employeeEmail;
					$company=$employees->companyID;

					if($company==ALIGNERCO)
					{
						$companyEmail=ALIGNERCO_EMAIL;
						$companyPassword=ALIGNERCO_EMAIL_PASSWORD;
						$companyRecieverEmail=ALIGNERCO_RECIEVER_EMAIL;
						$email_data['companyName']	= 'AlignerCo';
					}
					if($company==ALIGNERCO_CANADA)
					{
						$companyEmail=ALIGNERCO_CANADA_EMAIL;
						$companyPassword=ALIGNERCO_CANADA_EMAIL_PASSWORD;
						$companyRecieverEmail=ALIGNERCO_CANADA_RECIEVER_EMAIL;
						$email_data['companyName']	= 'AlignerCo Canada';
					}
					if($company==STRAIGHT_MY_TEETH)
					{
						$companyEmail=STRAIGHT_MY_TEETH_EMAIL;
						$companyPassword=STRAIGHT_MY_TEETH_EMAIL_PASSWORD;
						$companyRecieverEmail=STRAIGHT_MY_TEETH_RECIEVER_EMAIL;
						$email_data['companyName']	= 'Straight My Teeth';
					}
					if($company==SMILEPATH)
					{
						$companyEmail=SMILEPATH_EMAIL;
						$companyPassword=SMILEPATH_EMAIL_PASSWORD;
						$companyRecieverEmail=SMILEPATH_RECIEVER_EMAIL;
						$email_data['companyName']	= 'SmilePath';
					}


					if($caseStatusID==APPROVED)
					{
						$caseStatus='Approved The Case';
					}

					if($caseStatusID==MODIFIED)
					{
						$caseStatus='Asked For Modification In Case';
					}

					if($caseStatusID==PENDING)
					{
						$caseStatus='Make Case Status Pending';
					}

					$email_data['case_id']								=	$caseID;
					$email_data['link']				                    =	$link;
					$email_data['code']				                    =	$code;
					$email_data['caseStatus']				            =	$caseStatus;
					$email_data['doctorName']				            =	$employees->employeeName;


					if($caseStatusID==4)
					{
						/*===================email code start for portal================*/
	                    $this->load->library('email');
	                    $config = Array(
	                    'protocol' => 'smtp',
		                'smtp_host' => 'ssl://smtp.gmail.com',
		                'smtp_port' => 465,
		                'smtp_user' => $companyEmail,
		                'smtp_pass' => $companyPassword,
		                'smtp_timeout'=>20,
		                'mailtype'  => 'html',
		                'charset' => 'iso-8859-1',
		                'wordwrap' => TRUE
						);

	                    $this->load->library('email');
	                    $this->email->initialize($config);
	                    $this->email->set_mailtype("html");
	                    $this->email->set_newline("\r\n");

	                    $message =$this->load->view('cpanel/emails/case_modification_email', $email_data, true);


	                    $this->email->to($companyRecieverEmail);
	                    $this->email->subject('Modification on Case');
	                    $this->email->message($message);

	                    if($this->email->send())
	                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
	                    else
	                        $this->session->set_flashdata("email_sent","You have encountered an error");

	                    /*echo $this->email->print_debugger(); die();*/

	                    /*===========================email code end====================*/



	                    /*===================email code start for production================*/
	                   $this->load->library('email');
	                    $config = Array(
	                    'protocol' => 'smtp',
		                'smtp_host' => 'ssl://smtp.gmail.com',
		                'smtp_port' => 465,
		                'smtp_user' => $companyEmail,
		                'smtp_pass' => $companyPassword,
		                'smtp_timeout'=>20,
		                'mailtype'  => 'html',
		                'charset' => 'iso-8859-1',
		                'wordwrap' => TRUE
						);

	                    $this->load->library('email');
	                    $this->email->initialize($config);
	                    $this->email->set_mailtype("html");
	                    $this->email->set_newline("\r\n");

	                    $message =$this->load->view('cpanel/emails/case_modification_email', $email_data, true);


	                    $this->email->to(PRODUCTION_EMAIL);
	                    $this->email->subject('Modification on Case');
	                    $this->email->message($message);

	                    if($this->email->send())
	                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
	                    else
	                        $this->session->set_flashdata("email_sent","You have encountered an error");

	                    /*echo $this->email->print_debugger(); die();*/

	                    /*===========================email code end====================*/
					}
					else
					{
						/*===================email code start for portal for approval================*/
	                    $this->load->library('email');
	                    $config = Array(
	                    'protocol' => 'smtp',
		                'smtp_host' => 'ssl://smtp.gmail.com',
		                'smtp_port' => 465,
		                'smtp_user' => $companyEmail,
		                'smtp_pass' => $companyPassword,
		                'smtp_timeout'=>20,
		                'mailtype'  => 'html',
		                'charset' => 'iso-8859-1',
		                'wordwrap' => TRUE
						);

	                    $this->load->library('email');
	                    $this->email->initialize($config);
	                    $this->email->set_mailtype("html");
	                    $this->email->set_newline("\r\n");

	                    $message =$this->load->view('cpanel/emails/case_approval_email', $email_data, true);

	                    $this->email->to($companyRecieverEmail);
	                    $this->email->subject('Approval from Dentist');
	                    $this->email->message($message);

	                    if($this->email->send())
	                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
	                    else
	                        $this->session->set_flashdata("email_sent","You have encountered an error");

	                    /*echo $this->email->print_debugger(); die();*/

	                    /*===========================email code end====================*/
					}




			}
		 }




	redirect('cases-list');

	}



	function alignersSendToCustomer() {

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		/* User Roles And Permission

			 - Check Allow Permision or Not
			 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
		*/

		$assignedRoles 											 = $this->assignedRoles;
		$accessModules 											 = $this->accessModules;

				// Prepair Date To Store In Database
				$caseID								= ($this->input->post('case_id') ?: NULL);
				$productionStageID					= ($this->input->post('production_stage_id') ?: NULL);
				$caseStageID						= ($this->input->post('case_stage_id') ?: NULL);
				$caseStatusID						= ($this->input->post('case_status_id') ?: NULL);
				$statusID							= ($this->input->post('status_id') ?: NULL);

				$trackingNumberSendCustomer			= ($this->input->post('tracking_number_send_customer') ?: NULL);
				$deliveryServiceSendCustomer		= ($this->input->post('delivery_service_send_customer') ?: NULL);

				$comments				   			= ($this->input->post('comments') ?: NULL);

				$data['case_id']								=	$caseID;
				$data['tracking_number_sending']				=	$trackingNumberSendCustomer;
				$data['delivery_service_sending']				=	$deliveryServiceSendCustomer;
				$data['description']							=	$comments;

				$data['is_deletable']							=	HARD_CODE_ID_YES;

				$data['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
				$data['created_by']								=   $teamID;
				$data['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

				// Save All data Into Database Table
				$insertedD										= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_ALIGNERS_SEND_RECEIVE_PRODUCTION_TABLE,$data);


				 $dataStatus['case_id']							=	$caseID;
				 $dataStatus['case_stage_id']					=	$caseStageID;
				 $dataStatus['status_id']						=	$statusID;
				 $dataStatus['description']						=	$comments;

				 $dataStatus['is_deletable']					=	HARD_CODE_ID_YES;

				 $dataStatus['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
				 $dataStatus['created_by']						=   $teamID;
				 $dataStatus['created_by_reference_table']		=   'MY_ORGANIZATION_TEAM_TABLE';

				 // Save All data Into Database Table
				 $insertedStatusID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$dataStatus);

				 // Update Case Table After Assign Stage And Status
				 $caseUpdate['present_case_status_id']	=	$insertedStatusID;

				 // Update Record Table
				 $this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$caseUpdate);

				 redirect('case/'.encodeString($caseID));
	}

	function changeCaseStatus() {

		  /*Use For Access Permissions*/
		  if (!in_array(MODULE_CASE_UPDATE_STATUS,$this->accessModules)) {

				redirect('my-dashboard/');
		  }

		  $createdDate   	=  DATABASE_NOW_DATE_TIME_FORMAT();

		  $startTime   		= date("Y-m-d H:i:s",$createdDate);
		  $cenvertedTime 	= date('H:i:s',strtotime('-20 minutes',strtotime($startTime)));
		  $reverseTime	 	= strtotime($cenvertedTime);

		  $accountID		= $this->accountID;
		  $teamID			= $this->teamID;
		  $organizationID 	= $this->organizationID;

		  $assignedRoles 	= $this->assignedRoles;

		  // Prepair Date To Store In Database
		  $caseID								= ($this->input->post('case_id') ?: NULL);
		  $productionStageID					= ($this->input->post('production_stage_id') ?: NULL);
		  $caseStageID							= ($this->input->post('case_stage_id') ?: NULL);
		  $caseStatusID							= ($this->input->post('case_status_id') ?: NULL);
		  $statusID								= ($this->input->post('status_id') ?: NULL);
		  $comments								= ($this->input->post('comments') ?: NULL);

		  // check already case status updated by another authority like (Manager , Production Contorl, Doctor, ETC)
		  $presentStatus			 			= casePresentStatus($caseID); // Calling From Case Helper
		  if ($presentStatus) {

				$presentStatus 		 		 = $presentStatus->row_array();
				$presentCaseStatusID 		 = $presentStatus['caseStatusID'];
				$presentStatusID 			 = $presentStatus['statusID'];
				$presentStatusParentID 		 = $presentStatus['statusParentID'];
				$presentStatusName 			 = $presentStatus['statusName'];

				if ($presentCaseStatusID != $caseStatusID) {

					  if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles) || in_array(ROLE_MANAGER,$assignedRoles)) {

		 					redirect('case/'.encodeString($caseID));

		   			  } else {

							redirect('case/'.encodeString($caseID));
		  			  }
				}
			}

		   // chceck status already saved
		   $statusExist	 = 	$this->model_shared->getRecordMultipleWhere('id',MEDICAL_CASE_STATUS_TABLE,array('case_id' => $caseID,'case_stage_id' => $caseStageID,'status_id' => $statusID,'description' => $comments,'created_by' => $teamID,'is_deleted' => HARD_CODE_ID_NO,'created >=' => $reverseTime,'created <=' => $createdDate));

			if ($statusExist->num_rows() == 0) {

				  if ($caseID && $caseStageID && $statusID) {

						  $data['case_id']						=	$caseID;
						  $data['case_stage_id']				=	$caseStageID;
						  $data['status_id']					=	$statusID;
						  $data['description']					=	$comments;

						  $data['is_deletable']					=	HARD_CODE_ID_YES;

						  $data['created']						=  	$createdDate;
						  $data['created_by']					=   $teamID;
						  $data['created_by_reference_table']	=   'MY_ORGANIZATION_TEAM_TABLE';

						  // Save All data Into Database Table
						  $insertedID							= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$data);

						  // Update Case Table After Assign Stage And Status
						  $caseUpdate['present_case_status_id']	=	$insertedID;

						  // Update Record Table
						  $this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$caseUpdate);

						  if (in_array($statusID,$this->caseStatusDoneArray)) {

									$this->autoMoveCase($caseID);
						   }

						   // Set Session Message If Everything Goes Fine
						   $this->session->set_userdata('admin_msg',RECORD_UPDATED .' (Case ID: '.$caseID.' with Status: '.$statusName.')');
				  }
			}

		   if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles) || in_array(ROLE_MANAGER,$assignedRoles)) {

		 		redirect('case/'.encodeString($caseID));

		   } else {

				redirect('case/'.encodeString($caseID));
		  }
	}

	function updateCaseStatus() {

		  /*Use For Access Permissions*/
		  if (!in_array(MODULE_CASE_UPDATE_STATUS,$this->accessModules)) {

				redirect('my-dashboard/');
		  }

		  $createdDate   	=  DATABASE_NOW_DATE_TIME_FORMAT();

		  $startTime   		= date("Y-m-d H:i:s",$createdDate);
		  $cenvertedTime 	= date('H:i:s',strtotime('-20 minutes',strtotime($startTime)));
		  $reverseTime	 	= strtotime($cenvertedTime);

		  $accountID		= $this->accountID;
		  $teamID			= $this->teamID;
		  $organizationID 	= $this->organizationID;

		  $assignedRoles 	= $this->assignedRoles;

		  // Prepair Date To Store In Database
		  $caseID								= ($this->input->post('case_id') ?: NULL);
		  $productionStageID					= ($this->input->post('production_stage_id') ?: NULL);
		  $caseStageID							= ($this->input->post('case_stage_id') ?: NULL);
		  $caseStatusID							= ($this->input->post('case_status_id') ?: NULL);
		  $statusID								= ($this->input->post('status_id') ?: NULL);
		  $comments								= ($this->input->post('comments') ?: NULL);

		  // check already case status updated by another authority like (Manager , Production Contorl, Doctor, ETC)
		  $presentStatus			 			= casePresentStatus($caseID); // Calling From Case Helper
		  if ($presentStatus) {

				$presentStatus 		 		 = $presentStatus->row_array();
				$presentCaseStatusID 		 = $presentStatus['caseStatusID'];
				$presentStatusID 			 = $presentStatus['statusID'];
				$presentStatusParentID 		 = $presentStatus['statusParentID'];
				$presentStatusName 			 = $presentStatus['statusName'];

				if ($presentCaseStatusID != $caseStatusID) {

					  if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles) || in_array(ROLE_MANAGER,$assignedRoles)) {

		 					redirect('case-timeline/'.encodeString($caseID));

		   			  } else {

							redirect('my-cases/');
		  			  }
				}
			}

		   // chceck status already saved
		   $statusExist	 = 	$this->model_shared->getRecordMultipleWhere('id',MEDICAL_CASE_STATUS_TABLE,array('case_id' => $caseID,'case_stage_id' => $caseStageID,'status_id' => $statusID,'description' => $comments,'created_by' => $teamID,'is_deleted' => HARD_CODE_ID_NO,'created >=' => $reverseTime,'created <=' => $createdDate));

			if ($statusExist->num_rows() == 0) {

				  if ($caseID && $caseStageID && $statusID) {

						  $data['case_id']						=	$caseID;
						  $data['case_stage_id']				=	$caseStageID;
						  $data['status_id']					=	$statusID;
						  $data['description']					=	$comments;

						  $data['is_deletable']					=	HARD_CODE_ID_YES;

						  $data['created']						=  	$createdDate;
						  $data['created_by']					=   $teamID;
						  $data['created_by_reference_table']	=   'MY_ORGANIZATION_TEAM_TABLE';

						  // Save All data Into Database Table
						  $insertedID							= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$data);

						  // Update Case Table After Assign Stage And Status
						  $caseUpdate['present_case_status_id']	=	$insertedID;

						  // Update Record Table
						  $this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$caseUpdate);


						   // Check who is updating the status if as Operator
						   $operatorExist	= 	$this->model_shared->getRecordMultipleWhere('id',MEDICAL_CASE_OPERATOR_TABLE,array('case_id' => $caseID,'case_stage_id' => $caseStageID,'team_id' => $teamID,'is_deleted' => HARD_CODE_ID_NO));

						   if ($operatorExist->num_rows() > 0) {

									// Update Record Table
									$dataUpdate['case_status_id_latest']			=	$insertedID;

									if (in_array($statusID,$this->caseStatusHoldArray)) {

										$dataUpdate['processing_done']				=	HARD_CODE_ID_NO;

									} else {

										$dataUpdate['processing_done']				=	HARD_CODE_ID_YES;
									}

									// if QA reject the case then update the case status id which was as done / complete by setup operator.
									if (in_array($statusID,$this->caseStatusRejectArray)) {

										 $dataUpdate['case_status_id_rejected']		=	$setupErrorCaseStatusID;
									}

									$this->model_shared->editRecordWhere(array('case_id' => $caseID,'case_stage_id' => $caseStageID,'team_id' => $teamID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASE_OPERATOR_TABLE,$dataUpdate);

						   }


						   if (in_array($statusID,$this->caseStatusHoldArray)) {

									// Update the case hold falg
									$dataCaseUpdate['hold']	 =	HARD_CODE_ID_YES;
									$this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$dataCaseUpdate);

										 // Update Operatoer Latest Case Status Column
										 $presentOperator									= casePresentOperator($caseID); // Calling From Case Helper

										 if ($presentOperator) {

												$presentOperator 		 		 			 = $presentOperator->row_array();
												$presentCaseOperatorEmployeeID 				 = $presentOperator['employeeID'];

												// Update Record Table
												$dataUpdateOperator['case_status_id_latest']		 = $insertedID;
												$this->model_shared->editRecordWhere(array('case_id' => $caseID,'case_stage_id' => $caseStageID,'team_id' => $presentCaseOperatorEmployeeID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASE_OPERATOR_TABLE,$dataUpdateOperator);

										 }
						   }

						   if (in_array($statusID,$this->caseStatusOFFHoldArray)) {

									// Update the case hold falg
									$dataCaseUpdate['hold']	 =	HARD_CODE_ID_NO;

									/* Update case Target Date

											-  when is before waiting for approval
									*/
									if ($productionStageID == EPOXY_POURING || $productionStageID == _3D_SCANNING_EDITING || $productionStageID == QUALITY_OF_SCANNED_EDIT_CASES || $productionStageID == SETUP_SECTION || $productionStageID == QUALITY_OF_SETUP_CASES || $productionStageID == SETUP_EDITING || $productionStageID == QUALITY_OF_EDITING || $productionStageID == READY_FOR_UPLOADING)

											$setupTargetDate = $this->getOffHoldCaseTargetDateSetupTimelineDays($caseID);

											if ($setupTargetDate) {

												// Add Case Target Date SetupTimeline (Waiting for approval)
												$dataTargetDate['case_id']							=	$caseID;
												$dataTargetDate['production_stage_id']				=	WAITING_FOR_APPROVAL;
												$dataTargetDate['date']								=	$setupTargetDate;
												$dataTargetDate['description']						=	'Case hold reason';

												$dataTargetDate['is_deletable']						=	HARD_CODE_ID_YES;

												$dataTargetDate['created']							=   DATABASE_NOW_DATE_TIME_FORMAT();
												$dataTargetDate['created_by']						=   $teamID;
												$dataTargetDate['created_by_reference_table']		=   'MY_ORGANIZATION_TEAM_TABLE';

												// Save All data Into Database Table
												$insertedTargetDateID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASES_PRODUCTION_STAGE_TARGET_DATES_TABLE,$dataTargetDate);


												// Update the case target date
												$dataCaseUpdate['target_date_setup_timeline']	 =	$setupTargetDate;
									}

									// Update cases table
									$this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$dataCaseUpdate);

									// Update Operatoer Latest Case Status Column
									$presentOperator									= casePresentOperator($caseID); // Calling From Case Helper

									 if ($presentOperator) {

											$presentOperator 		 		 			 = $presentOperator->row_array();
											$presentCaseOperatorEmployeeID 				 = $presentOperator['employeeID'];

											// Update Record Table
											$dataUpdateOperator['case_status_id_latest']		 = $insertedID;
											$this->model_shared->editRecordWhere(array('case_id' => $caseID,'case_stage_id' => $caseStageID,'team_id' => $presentCaseOperatorEmployeeID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASE_OPERATOR_TABLE,$dataUpdateOperator);

									 }
						   }

						   if (in_array($statusID,$this->caseStatusDoneArray)) {

									$this->autoMoveCase($caseID);

									if (in_array(MODULE_CASE_CHANGE_OPERATOR,$accessModules)) {

										$dataUpdate['processing_done']	 =	HARD_CODE_ID_YES;
										$this->model_shared->editRecordWhere(array('case_id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASE_OPERATOR_TABLE,$dataUpdate);
						 			 }
						   }

						   // Modification marked by foreign Doctor
						   if (in_array($statusID,$this->caseStatusModificationArray)) {

									/* Update case Target Date

											-  when is before waiting for approval
									*/
									$setupTargetDate = $this->getModificationCaseTargetDateSetupTimelineDays($caseID);

									if ($setupTargetDate) {

										  // Add Case Target Date SetupTimeline (Waiting for approval)
										  $dataTargetDate['case_id']							=	$caseID;
										  $dataTargetDate['production_stage_id']				=	WAITING_FOR_APPROVAL;
										  $dataTargetDate['date']								=	$setupTargetDate;
										  $dataTargetDate['description']						=	'Case modification';

										  $dataTargetDate['is_deletable']						=	$isDeleteAble;

										  $dataTargetDate['created']							=   DATABASE_NOW_DATE_TIME_FORMAT();
										  $dataTargetDate['created_by']							=   $teamID;
										  $dataTargetDate['created_by_reference_table']			=   'MY_ORGANIZATION_TEAM_TABLE';

										  // Save All data Into Database Table
										  $insertedTargetDateID									= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASES_PRODUCTION_STAGE_TARGET_DATES_TABLE,$dataTargetDate);


										  // Update the case target date
										  $dataCaseUpdate['target_date_setup_timeline']			 =	$setupTargetDate;

										  // Update cases table
										  $this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$dataCaseUpdate);
									}

									$this->autoPushBackCase($caseID);
						   }

						   if (in_array($statusID,$this->caseStatusRejectArray)) {

									//$this->autoPushBackCase($caseID);
						   }

						   $statusInfo = statusInfoByID($statusID); // Calling From Case Helper

						   $statusName  = NOT_AVAILABLE;

						   if ($statusInfo) {

									$statusInfo = $statusInfo->row_array();

									$statusID 				 			 = $statusInfo['statusID'];
									$statusName 			 			 = $statusInfo['statusName'];
									$statusParentID 		 			 = $statusInfo['statusParentID'];

									if ($statusParentID) {

											$statusName = '('.statusName($statusParentID).') '.$statusName;
									}
						   }

						   // Set Session Message If Everything Goes Fine
						   $this->session->set_userdata('admin_msg',RECORD_UPDATED .' (Case ID: '.$caseID.' with Status: '.$statusName.')');
				  }
			}

		     $caseApprovedByCaseDetailPage = $this->input->post('approved_by_case_detail_page');

			 if ($caseApprovedByCaseDetailPage == 'approved_by_case_detail_page') {

					redirect('case/'.encodeString($caseID));
			 }

		   if (in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles) || in_array(ROLE_MANAGER,$assignedRoles)) {

		 		redirect('case/'.encodeString($caseID));

		   } else {

				redirect('my-cases/');
		  }
	}

	function autoMoveCase($caseID) {

			$accountID				= $this->accountID;
			$teamID					= $this->teamID;
			$organizationID 		= $this->organizationID;

			$casePresentStageID 	= 0;
			$casePresentStatusID 	= 0;

			$caseDetails			= $this->model_shared->getRecordMultipleWhere('*',MEDICAL_CASES_TABLE,array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO));

			if ($caseDetails->num_rows() > 0) {

					$casePresentStage  		= casePresentStage($caseID);
					$casePresentStatus 		= casePresentStatus($caseID);

					$caseDetails 			= $caseDetails->row_array();

					$caseDistributorID 		= $caseDetails['distributor'];


					// Find Case Present Production Stage ID
					if ($casePresentStage) {

						$casePresentStage   = $casePresentStage->row_array();

						$casePresentStageID = $casePresentStage['productionStageID'];
					}

					// Find Case Present Status Stage ID
					if ($casePresentStatus) {

						$casePresentStatus 	 = $casePresentStatus->row_array();
						$casePresentStatusID = $casePresentStatus['statusID'];

						 if (in_array($casePresentStatusID,$this->caseStatusReadyArray) || in_array($casePresentStatusID,$this->caseStatusProcessArray)) {

									return;
						 }
					}

					if ($casePresentStageID && $casePresentStatusID) {

								/*
									STEP ONE - 1

										- IF Case Newly Added [RECEIVE & ORDER ENTRY]
								*/
								if ($casePresentStageID == RECEIVE_ORDER_ENTRY)	{

										// IF Case Newly Added
										if ($casePresentStatusID == CASE_STATUS_CASE_RECEIVED) {

														$caseMove['production_stage_id']			=	KIT_SHIP_TO_CUSTOMER;
														$caseMoveStatus['status_id']				=	CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY;
										}
								}

								/*
									STEP TWO - 2

										- IF Case Exist

												[ SEND TO CUSTOMER ]

														 OR

												[ SEND TO PRODUCTION ]
								*/
								if ($casePresentStageID == KIT_SHIP_TO_CUSTOMER){

											$caseMove['production_stage_id']	=	KIT_TO_CUSTOMER_SEND_RECEIVE;
											$caseMoveStatus['status_id']		=	CASE_STATUS_KIT_TO_CUSTOMER_IN_TRANSIT;

								} else if ($casePresentStageID == KIT_TO_CUSTOMER_SEND_RECEIVE){

											$caseMove['production_stage_id']	=	IMPRESSIONS_TO_NY_SEND_RECEIVE;
											$caseMoveStatus['status_id']		=	CASE_STATUS_IMPRESSIONS_TO_NY_READY;

								} else if ($casePresentStageID == IMPRESSIONS_TO_NY_SEND_RECEIVE && $casePresentStatusID == CASE_STATUS_IMPRESSIONS_TO_NY_RECEIVED){

											$caseMove['production_stage_id']	=	IMPRESSIONS_TO_PRODUCTION_SEND_RECEIVE;
											$caseMoveStatus['status_id']		=	CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_READY;

								} else if ($casePresentStageID == IMPRESSIONS_TO_PRODUCTION_SEND_RECEIVE && $casePresentStatusID == CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_RECEIVED){

											$caseMove['production_stage_id']	=	READY_FOR_UPLOADING;
											$caseMoveStatus['status_id']		=	CASE_STATUS_READY_FOR_UPLOADING_READY;

								} else if ($casePresentStageID == READY_FOR_UPLOADING){

											$caseMove['production_stage_id']	=	WAITING_FOR_APPROVAL;
											$caseMoveStatus['status_id']		=	CASE_STATUS_WAITING_FOR_APPROVAL_WAITING;

								} else if ($casePresentStageID == WAITING_FOR_APPROVAL){

											$caseMove['production_stage_id']	=	PRODUCTION_TO_NY_SEND_RECEIVE;
											$caseMoveStatus['status_id']		=	CASE_STATUS_PRODUCTION_TO_NY_READY;

								} else if ($casePresentStageID == PRODUCTION_TO_NY_SEND_RECEIVE){

											$caseMove['production_stage_id']	=	ALIGNERS_TO_CUSTOMER_SEND_RECEIVE;
											$caseMoveStatus['status_id']		=	CASE_STATUS_ALIGNERS_TO_CUSTOMER_READY;

								} else if ($casePresentStageID == ALIGNERS_TO_CUSTOMER_SEND_RECEIVE && $casePresentStatusID == CASE_STATUS_ALIGNERS_TO_CUSTOMER_RECEIVED){

										return;
								}

								// Case Auto Move to Next Production Stage
								$caseMove['case_id']								=	$caseID;
								$caseMove['description']							=	NULL;
								$caseMove['auto_move']								=	HARD_CODE_ID_YES;

								$caseMove['is_deletable']							=	HARD_CODE_ID_YES;

								$caseMove['created']								=  	DATABASE_NOW_DATE_TIME_FORMAT();
								$caseMove['created_by']								=   $teamID;
								$caseMove['created_by_reference_table']				=   'MY_ORGANIZATION_TEAM_TABLE';

								// Save All data Into Database Table
								$insertedCaseStageID								= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_PRODUCTION_STAGE_TABLE,$caseMove);


								// Case Auto Status Next Production Stage
								$caseMoveStatus['case_stage_id']					=	$insertedCaseStageID;
								$caseMoveStatus['case_id']							=	$caseID;
								$caseMoveStatus['description']						=	NULL;
								$caseMoveStatus['auto_move']						=	HARD_CODE_ID_YES;

								$caseMoveStatus['is_deletable']						=	HARD_CODE_ID_YES;

								$caseMoveStatus['created']							=  	DATABASE_NOW_DATE_TIME_FORMAT();
								$caseMoveStatus['created_by']						=   $teamID;
								$caseMoveStatus['created_by_reference_table']		=   'MY_ORGANIZATION_TEAM_TABLE';

								$insertedStatusID									= 	$this->model_shared->insertRecord_ReturnID(MEDICAL_CASE_STATUS_TABLE,$caseMoveStatus);

								// Update Case Table After Assign Stage And Status
								$caseUpdate['present_case_stage_id']				=	$insertedCaseStageID;
								$caseUpdate['present_case_status_id']				=	$insertedStatusID;

								// Update Record Table
								$this->model_shared->editRecordWhere(array('id' => $caseID,'is_deleted' => HARD_CODE_ID_NO),MEDICAL_CASES_TABLE,$caseUpdate);
					}
			}

		return true;
	}

	//Inside Functions Calling
	function resizeImage($source_image_path,$upload_image_path,$source_image_name,$new_image_name,$width,$height) {

		$config['image_library']	 = 'GD2';
		$config['source_image'] 	 = $source_image_path.$source_image_name;
		$config['create_thumb'] 	 = false;
		$config['maintain_ratio'] 	 = TRUE;
		$config['quality'] 			 = '100%';
		$config['new_image'] 		 = $upload_image_path.$new_image_name.$source_image_name;
		$config['width'] 			 = $width;
		$config['height'] 			 = $height;
		$this->load->library('image_lib', $config);
		$this->image_lib->initialize($config);
		if ( ! $this->image_lib->resize())
		{
			echo $this->image_lib->display_errors();
		}
	}




	public function caseMissing() {

				/*Use For Access Permissions*/
				if (!in_array(MODULE_CASE_ROOMS,$this->accessModules))
				{
					redirect('my-dashboard/');
		        }

				/* User Roles And Permission

					 - Check Allow Permision or Not
					 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
				*/


					$accountID				= $this->accountID;
					$teamID					= $this->teamID;
					$organizationID 		= $this->organizationID;

					$userInfo 				= userInfo($accountID); // Calling From Application Helper

					/* User Roles And Permission

						 - Check Allow Permision or Not
						 - Access Limitation Control SUPER ADMIN | MANAGER | HR | OTHERS
					*/

					$assignedRoles 							= $this->assignedRoles;
					$accessModules 							= $this->accessModules;
					$productionStages						= productionStages(); // Calling From Case Helper


				$recordPerPage					= 1000;

				$result['assignedRoles']		=  $assignedRoles;
				$result['accessModules']		=  $accessModules;

				$result['bloodGroups'] 			= getBloodGroups(); // Calling From Application Helper

				$missingCases= $this->model_case->getMissingCases();

				$count=1;
				foreach($missingCases->result() as $mc)
				{
					$counter=$count++;
					/*$tracking_number=$mc->totalGames;*/
				}

				$result['missingCases']	 							= $missingCases;
				$result['counter']	 							= $counter;



				/*$result['totalResults']		= $totalRows;*/

				$data['pageHeading']    	= "Cases";
				$data['subHeading']    		= "(missing)";

				$data['userInfo']           = $userInfo;

				$data['activeMenu']  		= '4';
				$data['activeSubMenu']  	= '4.3';

				$data['metaType']     		= 'internal';
				$data['pageName']    		= 'Cases';
				$data['pageTitle']      	= 'Cases | '.DEFAULT_APPLICATION_NAME;

				$data['contents']						= $this->load->view('cpanel/cases/cases_missing',$result,true);
		       $this->load->view('cpanel/template',$data);
	}



	function caseMissingDetails($tableID) {

		$tableID = decodeString($tableID);
		/*Use For Access Permissions*/
		if (!in_array(MODULE_CASE_ROOMS,$this->accessModules)) {

			redirect('my-dashboard/');
		}

		$accountID				= $this->accountID;
		$teamID					= $this->teamID;
		$organizationID 		= $this->organizationID;

		$userInfo 				= userInfo($accountID); // Calling From Application Helper

		$assignedRoles 							= $this->assignedRoles;
		$accessModules 							= $this->accessModules;

		/*$productionStages						= productionStages();*/ // Calling From Case Helper

		$cases = $this->model_case->getMissingCasesDetails($tableID);

		$count=1;
		foreach($cases->result() as $mc)
		{
			$counter=$count++;
			/*$tracking_number=$mc->totalGames;*/
		}

		$result['assignedRoles']				= $assignedRoles;
		$result['accessModules']				= $accessModules;
		$result['counter']				= $counter;

		/*$result['productionStages']  			= $productionStages;*/

		$result['cases']  					= $cases;


		$data['userInfo']            			= $userInfo;

		$data['activeMenu']  					= '4';
		$data['activeSubMenu']  				= '4.3';

		$data['pageHeading']    				= "<i class='fa fa-medkit'></i> Cases";
		$data['subHeading']    					= "";

		$data['metaType']     					= 'internal';
		$data['pageTitle']      				= 'Cases | '.DEFAULT_APPLICATION_NAME;

		$data['contents']						= $this->load->view('cpanel/cases/cases_missing_details',$result,true);	;
		$this->load->view('cpanel/template',$data);

	}





}
?>

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*
|--------------------------------------------------------------------------
| Messages
|--------------------------------------------------------------------------
|
| List of messages to display in website 
|
*/
define('DEFAULT_RESPONSE_MESSAGE','Sorry!, request not available.');
define('IMPROPER_INPUT','Your input is either missing or not in proper format.');
define('LOGGEDIN','Login successful.');
define('LOGIN_UNAUTH','Please provide correct email and password combination.');
define('RECORD_UPDATED','Record updated.');
define('REGISTERED','Your account has been registered.');
define('INVALID_AUTH_CODE','Authentication code is invalid.');
define('INVALID_SESSION_TOKEN','Session token in not valid');
define('USER_ALREADY_EXIST','An account already exists against this email address. Please try with another email address.');
define('RECORD_EXIST','Already exists.');
define('NOT_REGISTERED','Your account was not registered. Please try again later.');
define('RECORD_ADDED','Record added successfully.');
define('RECORD_NOT_ADDED','Record was not added.');
define('RECORD_NOT_FOUND','No record found.');
define('RECORD_SOFT_DELETED','Record deleted.');
define('DUPLICATE_ENTRY','Record already exist.');
define('NOT_DELETED','Could not delete.');
define('RECORD_DELETED','Record Deleted.');
define('INACTIVE_ACCOUNT','Your account is not activeted.');
define('BLOCKED_ACCOUNT','Your account has been blocked. Please contact with support.');
define('PASSWORD_INVALID','Current password incorrect');
define('BROWSE_PICTURE','Please select an image to upload.');
define('MOBILE_AUTH_FAILED','The phone number you entered does not belong to any account.');
define('MOBILE_ALREADY_EXIST','This phone already exist.');
define('MOBILE_INVALID_FORMAT','Please enter a valid phone number.');
define('MOBILE_CODE_SENT','Code has been sent.');
define('MOBILE_CODE_INVALID','That code didn\'t work. Please check the code and try again.');
define('WRONG_PICTURE_FORMAT','Your image couldn\'t be uploaded. Image should be less than 4 MB and saved as JPG, JPEG or GIF or PNG files.');
define('WRONG_EMAIL_FORMAT','The email must contain a valid email address.');
define('DEFAULT_LOCKED_BY_ADMIN_MESSAGE','Sorry!, This has been locked.');
define('DEFAULT_BLOCK_MESSAGE','Sorry!, This name is blocked. Please try another.');
define('DEFAULT_BLOCK_BY_ADMIN_MESSAGE','Sorry!, This has been blocked.');
define('DEFAULT_ADD_PERMISSION_FAILED_MESSAGE','Sorry!, You can not add this. please try with different.');
define('DEFAULT_REMOVE_PERMISSION_FAILED_MESSAGE','Sorry!, You can not remove this. For more see our add / update policy.' );
define('NOT_AVAILABLE','(not available)');
define('NOT_AVAILABLE_SHORT_CODE','(N/A)');


define('DEFAULT_APPLICATION_NAME','Portal');

define('APPLICATION_FILE_URL_PATH',PATH_DIR.'files/');
define('APPLICATION_FILE_DIRECTORY_PATH',PATH_DIR.'files/');

define('OFFICIALS_USERS_URL_PATH',LIVE_URL.'images/cpanel_users/');
define('OFFICIALS_USERS_DIRECTORY_PATH',PATH_DIR.'images/cpanel_users/');

define('COMPANIES_LOGO_URL_PATH',LIVE_URL.'backend_images/companies/');
define('COMPANIES_LOGO_DIRECTORY_PATH',PATH_DIR.'backend_images/companies/');

define('CPANEL_IMAGES_URL_PATH',LIVE_URL.'backend_images/');

define('USER_DEFAULT_IMAGE_URL_PATH',LIVE_URL.'backend_images/user101-128x128.png');
define('HR_OFFICIALS_USERS_URL_PATH',LIVE_URL.'images/cpanel_users/');


define('CASE_PICTURES_URL_PATH',LIVE_URL.'pictures_cases/');
define('CASE_PICTURES_PATH_DIRECTORY_PATH',PATH_DIR.'pictures_cases/');
define('CASE_PICTURES_PATH_THUMBNAIL_DIRECTORY_PATH',PATH_DIR.'pictures_cases/thumbnail/');


/*
|--------------------------------------------------------------------------
| CONSTENT VALUES
|--------------------------------------------------------------------------
|
| CONTANTS VALUES FOR SITE
|
*/

define('WORD_LIMTER_SMALL_TEXT',25);
define('WORD_LIMTER_MEDIUM_TEXT',45);
define('WORD_LIMTER_LONG_TEXT',50);
define('RECORDS_PER_PAGE_STANDRED',20);


define('USER_ID_SESSION','alignerCO_logged_in_member_id');
define('USER_ACCOUNT_ID_SESSION','alignerCO_logged_in_member_id');
define('USER_TEAM_ID_SESSION','alignerCO_logged_in_member_team_id');
define('USER_HR_ID_SESSION','alignerCO_logged_in_member_hr_id');
define('USER_ORGANIZATION_ID_SESSION','alignerCO_logged_in_member_organization_id');
define('USER_ACCOUNT_STATUS_SESSION','alignerCO_account_status');
define('USER_NAME_SESSION','alignerCO_logged_in_member_naming');
define('USER_PHOTO_SESSION','alignerCO_logged_in_member_photo');
define('USER_LOGIN_SESSION','alignerCO_logged_in');


define('CASE_ATTACHMENTS_URL_PATH',LIVE_URL.'attachments/');
define('CASE_ATTACHMENTS_DIRECTORY_PATH',PATH_DIR.'attachments/');
define('CASE_ATTACHMENTS_THUMBNAIL_DIRECTORY_PATH',PATH_DIR.'attachments/thumbnail/');


define('CASE_RX_FORM_URL_PATH',LIVE_URL.'rx/');
define('CASE_RX_FORM_DIRECTORY_PATH',PATH_DIR.'rx/');
define('CASE_RX_FORM_THUMBNAIL_DIRECTORY_PATH',PATH_DIR.'rx/thumbnail/');

define('CASE_X_RAY_OPG_URL_PATH',LIVE_URL.'x_rays/opg');
define('CASE_X_RAY_OPG_PATH_DIRECTORY_PATH',PATH_DIR.'x_rays/opg/');
define('CASE_X_RAY_OPG_PATH_THUMBNAIL_DIRECTORY_PATH',PATH_DIR.'x_rays/opg/thumbnail/');

define('CASE_X_RAY_CEPH_URL_PATH',LIVE_URL.'x_rays/ceph');
define('CASE_X_RAY_CEPH_PATH_DIRECTORY_PATH',PATH_DIR.'x_rays/ceph/');
define('CASE_X_RAY_CEPH_PATH_THUMBNAIL_DIRECTORY_PATH',PATH_DIR.'x_rays/ceph/thumbnail/');

define('CASE_FILE_ASSESSMENT_URL_PATH',LIVE_URL.'assessments/');
define('CASE_FILE_ASSESSMENT_PATH_DIRECTORY_PATH',PATH_DIR.'assessments/');





define('GENERAL_SETTINGS_HARD_CODE_ID', 1); /*IT COMES FROM tbl_application_supreme_application_template_settings_general*/

define('HARD_CODE_ID_ADMINISTRATOR',1); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_CLOSE',5); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_PUBLISHED',9); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_PENDING',10); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_BLOCKED',11); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_CANCEL',12); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_UNPUBLISHED',13); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_APPROVED',15); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_OPEN',16); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_ACTIVATED',25); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_INACTIVE',92); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_COMPLETE',38); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_YES', 41); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_NO', 42); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_VERIFIED',50); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_NOT_VERIFIED',51); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_NEW',58); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_SEEN',59); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_NATIONAL',60); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_LOCKED',62); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_ALLOW',63); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_CHILD_OR_DEPENDENT',73); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_SUPER_ADMINISTRATOR',79); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_NOT_REQUIRED',81); /*IT COMES FROM tbl_application_setups_statuses_lookup*/
define('HARD_CODE_ID_NOT_OTHER',82); /*IT COMES FROM tbl_application_setups_statuses_lookup*/

define('HARD_CODE_PICK',102); /*IT COMES FROM tbl_education_setups_statuses_lookup*/
define('HARD_CODE_ASSIGNED',103); /*IT COMES FROM tbl_education_setups_statuses_lookup*/
define('HARD_CODE_AUTO_ASSIGNED',104); /*IT COMES FROM tbl_education_setups_statuses_lookup*/

define('HARD_CODE_ID_AM',100); /*IT COMES FROM tbl_education_setups_statuses_lookup*/
define('HARD_CODE_ID_PM',101); /*IT COMES FROM tbl_education_setups_statuses_lookup*/

define('HARD_CODE_ID_MAJOR',105); /*IT COMES FROM tbl_education_setups_statuses_lookup*/
define('HARD_CODE_ID_MINOR',106); /*IT COMES FROM tbl_education_setups_statuses_lookup*/


define('HARD_CODE_ID_APPLICATION_HR_GROUP_EMPLOYEE',1); /*IT COMES FROM tbl_application_hr_groups*/
define('HARD_CODE_ID_APPLICATION_HR_GROUP_HIGHER_MANAGEMENT',2); /*IT COMES FROM tbl_application_hr_groups*/
define('HARD_CODE_ID_APPLICATION_HR_GROUP_MANAGEMENT',3); /*IT COMES FROM tbl_application_hr_groups*/
define('HARD_CODE_ID_APPLICATION_HR_GROUP_EXECUTIVE_STAFF',4); /*IT COMES FROM tbl_application_hr_groups*/
define('HARD_CODE_ID_APPLICATION_HR_GROUP_DIRECTOR',5); /*IT COMES FROM tbl_application_hr_groups*/



define('HARD_CODE_ID_MALE',1); /*IT COMES FROM tbl_education_setups_gender*/
define('HARD_CODE_ID_FEMALE',2); /*IT COMES FROM tbl_education_setups_gender*/
define('HARD_CODE_ID_BOY',3); /*IT COMES FROM tbl_education_setups_gender*/
define('HARD_CODE_ID_GIRL',4); /*IT COMES FROM tbl_education_setups_gender*/


define('HARD_CODE_ID_MARRIED',14); /*IT COMES FROM tbl_education_setups_marital_statuses*/
define('HARD_CODE_ID_SINGLE',17); /*IT COMES FROM tbl_education_setups_marital_statuses*/


define('HARD_CODE_ID_COUNTRY_PAKISTAN',168); /*IT COMES FORM tbl_setups_countries*/
define('HARD_CODE_ID_STATE_PUNJAB',2);  /*IT COMES FORM tbl_setups_states*/
define('HARD_CODE_ID_CITY_LAHORE',1); /*IT COMES FORM tbl_setups_cities*/

define('HARD_CODE_ID_PARENT_OR_INDEPENDENT',0); 
define('SHOW_RECORD_PER_PAGE',20); 


define('PRIORITY_HIGH',1); /*IT COMES FORM tbl_application_setups_priority*/
define('PRIORITY_MEDIUM',2); /*IT COMES FORM tbl_application_setups_priority*/
define('PRIORITY_LOW',3); /*IT COMES FORM tbl_application_setups_priority*/


define('HARD_CODE_ID_PROBATION',7); /*IT COMES FORM tbl_application_setups_employment_statuses*/
define('HARD_CODE_ID_PERMANENT',8); /*IT COMES FORM tbl_application_setups_employment_statuses*/
define('HARD_CODE_ID_SUSPENDED',5); /*IT COMES FORM tbl_application_setups_employment_statuses*/
define('HARD_CODE_ID_RESIGNED',6); /*IT COMES FORM tbl_application_setups_employment_statuses*/
define('HARD_CODE_ID_TERMINATE',14); /*IT COMES FORM tbl_application_setups_employment_statuses*/
define('HARD_CODE_ID_LAYOFF',15); /*IT COMES FORM tbl_application_setups_employment_statuses*/
define('HARD_CODE_ID_REJOINING_PER',16); /*IT COMES FORM tbl_application_setups_employment_statuses*/
define('HARD_CODE_ID_REJOINING_PROB',17); /*IT COMES FORM tbl_application_setups_employment_statuses*/

define('HARD_CODE_ID_REFERENCE',3); /*IT COMES FORM tbl_application_setups_employment_mode*/

define('HARD_CODE_ID_EMPLOYTYPE_EMPLOY',1); /*IT COMES FORM tbl_application_setups_employ_types*/
define('HARD_CODE_ID_EMPLOYTYPE_DOCTOR',2); /*IT COMES FORM tbl_application_setups_employ_types*/
define('HARD_CODE_ID_CLEAR',38); 
define('HARD_CODE_ID_MISSING',107);
define('HARD_CODE_ID_PUSHED',100);
define('HARD_CODE_ID_NOT_PUSHED',99);

define('HARD_CODE_ID_USER_TYPE_USER',1); 
define('HARD_CODE_ID_USER_TYPE_PATIENT',2);

define('HARD_CODE_ID_PICTURE_SEEN',50); 
define('HARD_CODE_ID_PICTURE_NOT_SEEN',49);


/*Modules Tables*/

define('MODULE_DASHBOARD',1); /*IT COMES FROM tbl_application_alignerco_modules*/
define('MODULE_DASHBOARD_HOME',2); /*IT COMES FROM tbl_application_alignerco_modules*/
define('MODULE_DASHBOARD_LOGOUT',4); /*IT COMES FROM tbl_application_alignerco_modules*/

define('MODULE_USERS',5); /*IT COMES FROM tbl_application_alignerco_modules*/

define('MODULE_EMPLOYEE',6); /*IT COMES FROM tbl_application_modules*/

define('MODULE_SETUPS',11); /*IT COMES FROM tbl_application_medical_modules*/
define('MODULE_SETUP_PRODUCTION_ROOMS',12); /*IT COMES FROM tbl_application_medical_modules*/

define('MODULE_CASES',7); /*IT COMES FROM tbl_application_medical_modules*/
define('MODULE_CASE_ADD_NEW',8); /*IT COMES FROM tbl_application_medical_modules*/
define('MODULE_CASE_EDIT',9); /*IT COMES FROM tbl_application_medical_modules*/
define('MODULE_CASE_DELETE',10); /*IT COMES FROM tbl_application_medical_modules*/

define('MODULE_CASE_ROOMS',9); /*IT COMES FROM tbl_application_medical_modules*/

define('MODULE_CASE_DETAILS',14); /*IT COMES FROM tbl_application_medical_modules*/

define('MODULE_CASE_UPDATE_STATUS',17); /*IT COMES FROM tbl_application_medical_modules*/

define('MODULE_CASE_TIMELINE',15); /*IT COMES FROM tbl_application_medical_modules*/
define('MODULE_CASE_TIMELINE_READ_ONLY',16); /*IT COMES FROM tbl_application_medical_modules*/

define('MODULE_DOCTOR',18);   /*IT COMES FROM tbl_application_medical_modules*/
define('ASSIGN_CASE_TO_DOCTOR',19);   /*IT COMES FROM tbl_application_medical_modules*/
define('DOCTOR_CASE_UPDATE_STATUS',20);   /*IT COMES FROM tbl_application_medical_modules*/
define('CASE_MAIN_TIMELINE_PAGE',21);   /*IT COMES FROM tbl_application_medical_modules*/
define('DOCTOR_CASE_TIMELINE_PAGE',22);   /*IT COMES FROM tbl_application_medical_modules*/
define('MANAGE_STATES',24);   /*IT COMES FROM tbl_application_medical_modules*/
define('PATIENT_PICTURES',25);   /*IT COMES FROM tbl_application_medical_modules*/
define('PATIENT_LIST',26);   /*IT COMES FROM tbl_application_medical_modules*/

define('MODULE_REPORTS',27);   /*IT COMES FROM tbl_application_medical_modules*/
define('DOCTOR_REPORTS',28);   /*IT COMES FROM tbl_application_medical_modules*/



/*Modules Actions Tables*/
define('PERFOMED_ACTION_CREATED',1); /*IT COMES FROM tbl_application_actions*/
define('PERFOMED_ACTION_DELETED',4); /*IT COMES FROM tbl_application_actions*/
define('PERFOMED_ACTION_ASSIGNED',5); /*IT COMES FROM tbl_application_actions*/
define('PERFOMED_ACTION_MARKED',6); /*IT COMES FROM tbl_application_actions*/


define('ROLE_SUPER_ADMINISTRATOR',1); /*IT COMES FROM  tbl_application_alignerco_team_roles*/
define('ROLE_MANAGER',2); /*IT COMES FROM  tbl_application_alignerco_team_roles*/
define('ROLE_OPERATOR',3); /*IT COMES FROM  tbl_application_alignerco_team_roles*/
define('ROLE_DOCTOR',4); /*IT COMES FROM  tbl_application_alignerco_team_roles*/
define('ROLE_PATIENT',5); /*IT COMES FROM  tbl_application_alignerco_team_roles*/

define('CASE_IMPRESSION_TYPE_PHYSICAL',1); /*IT COMES FROM tbl_application_medical_setups_case_impression_types*/
define('CASE_IMPRESSION_TYPE_STL',2); /*IT COMES FROM tbl_application_medical_setups_case_impression_types*/



define('STATES_LOOKUP_TABLE','tbl_application_setups_statuses_lookup');
define('COUNTRIES_TABLE','tbl_application_setups_countries');
define('STATES_TABLE','tbl_application_setups_states');
define('CITIES_TABLE','tbl_application_setups_cities');
define('NATIONALITIES_TABLE','tbl_application_setups_nationalities');
define('RELIGIONS_TABLE','tbl_application_setups_religions');
define('RELATIONS_TABLE','tbl_application_setups_relations');

define('PREFIX_TABLE','tbl_application_setups_prefixes');
define('GENDER_TABLE','tbl_application_setups_gender');
define('MARITAL_STATUS_TABLE','tbl_application_setups_marital_statuses');
define('BLOOD_GROUPS_TABLE','tbl_application_setups_blood_groups');
define('CURRENCY_TABLE','tbl_application_setups_currencies');
define('DATE_FORMATS_TABLE','tbl_application_setups_date_formats');
define('TIME_FORMATS_TABLE','tbl_application_setups_time_formats');
define('LANGUAGES_TABLE','tbl_application_setups_languages');
define('IDENTITY_CODES_TABLE','tbl_application_setups_identity_codes');
define('EMPLOYMENT_STATUSES_TABLE','tbl_application_setups_employment_statuses');
define('MODE_OF_EMPLOYMENT_TABLE','tbl_application_setups_employment_mode');
define('PAY_FREQUENCY_TABLE','tbl_application_setups_pay_frequency');
define('JOB_TITLES_TABLE','tbl_application_setups_job_titles');
define('JOB_POSITIONS_TABLE','tbl_application_setups_job_positions');
// changes
/*define('JOB_ROLES_TABLE','tbl_application_setups_job_roles');*/
define('JOB_ROLES_TABLE','tbl_application_alignerco_setups_job_roles');
define('BANKS_TABLE','tbl_application_setups_banks');
define('WEEKDAYS_TABLE','tbl_application_setups_weekdays');

define('DELIVERY_SERVICES_TABLE','tbl_application_setups_delivery_services');



define('APPLICATION_HR_TABLE','tbl_application_hr');
define('APPLICATION_HR_GROUPS_TABLE','tbl_application_hr_groups');


define('ORGANIZATIONS_TABLE','tbl_applictaion_organizations');
define('MY_APPLICATION_CONFIGURATION_TABLE','tbl_application_configuration');

define('MY_ORGANIZATION_BUSINESS_UNITS_TABLE','tbl_application_my_organization_business_units');
define('MY_ORGANIZATION_BUSINESS_UNITS_DEPARTMENTS_TABLE','tbl_application_my_organization_business_unit_departments');
define('MY_ORGANIZATION_DEPARTMENTS_TABLE','tbl_application_my_organization_departments');


define('MY_ORGANIZATION_JOB_TITLES_TABLE','tbl_application_alignerco_job_titles');
define('MY_ORGANIZATION_JOB_POSITIONS_TABLE','tbl_application_alignerco_job_positions');

define('MY_ORGANIZATION_JOB_ROLES_TABLE','tbl_application_alignerco_job_roles');
define('MY_ORGANIZATION_JOB_ROLES_MODULE_ACCESS_TABLE','tbl_application_alignerco_job_roles_module_access');

define('MY_ORGANIZATION_TEAM_TABLE','tbl_application_alignerco_team');
define('MY_ORGANIZATION_TEAM_GROUPS_TABLE','tbl_application_alignerco_team_groups');
define('MY_ORGANIZATION_TEAM_JOBS_TABLE','tbl_application_alignerco_team_jobs');

define('MY_ORGANIZATION_TEAM_OFFICIALS_TABLE','tbl_application_alignerco_team_officials');
define('MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE','tbl_application_alignerco_team_officials_roles');
define('MY_ORGANIZATION_TEAM_OFFICIALS_MODULE_ACCESS_TABLE','tbl_application_alignerco_officials_module_access');

/*PRODUCTION STAGES*/
define('RECEIVE_ORDER_ENTRY',86); /*IT COMES FROM tbl_application_medical_setups_production_stages*/
define('KIT_SHIP_TO_CUSTOMER',89); /*IT COMES FROM tbl_application_medical_setups_production_stages*/
define('KIT_TO_CUSTOMER_SEND_RECEIVE',104); /*IT COMES FROM tbl_application_medical_setups_production_stages*/
define('IMPRESSIONS_TO_NY_SEND_RECEIVE',105); /*IT COMES FROM tbl_application_medical_setups_production_stages*/
define('IMPRESSIONS_TO_PRODUCTION_SEND_RECEIVE',106); /*IT COMES FROM tbl_application_medical_setups_production_stages*/
define('READY_FOR_UPLOADING',107); /*IT COMES FROM tbl_application_medical_setups_production_stages*/
define('WAITING_FOR_APPROVAL',108); /*IT COMES FROM tbl_application_medical_setups_production_stages*/
define('PRODUCTION_TO_NY_SEND_RECEIVE',109); /*IT COMES FROM tbl_application_medical_setups_production_stages*/
define('ALIGNERS_TO_CUSTOMER_SEND_RECEIVE',110); /*IT COMES FROM tbl_application_medical_setups_production_stages*/

/*DOCTOR CASE STATUS*/
define('ASSIGNED',1); /*IT COMES FROM tbl_application_alignerco_doctor_case_status*/
define('PENDING',2); /*IT COMES FROM tbl_application_alignerco_doctor_case_status*/
define('APPROVED',3); /*IT COMES FROM tbl_application_alignerco_doctor_case_status*/
define('MODIFIED',4); /*IT COMES FROM tbl_application_alignerco_doctor_case_status*/


/*PRODUCTION STAGE STATUS*/
define('CASE_STATUS_CASE_RECEIVED',4); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY',5); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_KIT_SHIP_TO_CUSTOMER_SHIPPED',6); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_STATUS_KIT_TO_CUSTOMER_IN_TRANSIT',40); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_KIT_TO_CUSTOMER_RECEIVED',41); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_STATUS_IMPRESSIONS_TO_NY_READY',56); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_IMPRESSIONS_TO_NY_IN_TRANSIT',42); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_IMPRESSIONS_TO_NY_RECEIVED',43); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_READY',55); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT',44); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_RECEIVED',45); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_STATUS_READY_FOR_UPLOADING_READY',46); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_READY_FOR_UPLOADING_DONE',47); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_STATUS_WAITING_FOR_APPROVAL_WAITING',48); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_WAITING_FOR_APPROVAL_APPROVED',49); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_WAITING_FOR_APPROVAL_MODIFICATION',50); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_STATUS_PRODUCTION_TO_NY_READY',51); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_PRODUCTION_TO_NY_IN_TRANSIT',57); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_PRODUCTION_TO_NY_RECEIVED',52); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_STATUS_ALIGNERS_TO_CUSTOMER_READY',53); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_ALIGNERS_TO_CUSTOMER_IN_TRANSIT',58); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_ALIGNERS_TO_CUSTOMER_RECEIVED',54); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/
define('CASE_STATUS_UPDATE_CASE_DETAILS',60); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/

define('CASE_PRESENT_STAGE_ID',59); /*IT COMES FROM tbl_application_medical_setups_production_stage_status*/


/*Database Tables*/

define('MEDICAL_COMPANIES_TABLE','tbl_application_medical_setups_companies');
define('MEDICAL_COMPANY_CASE_TYPES_TABLE','tbl_application_medical_company_case_types');
define('MEDICAL_COMPANY_CONTACTS_TABLE','tbl_application_medical_company_contacts');

define('MEDICAL_DOCTORS_TABLE','tbl_application_medical_setups_doctors');
define('MEDICAL_CASE_IMPRESSION_TYPES_TABLE','tbl_application_medical_setups_case_impression_types');

define('MEDICAL_DISEASES_TABLE','tbl_application_medical_setups_diseases');

define('MEDICAL_CASE_TYPE_TABLE','tbl_application_medical_setups_case_types');

define('MEDICAL_PRODUCTION_STAGES_TABLE','tbl_application_alignerco_setups_production_stages');
define('MEDICAL_PRODUCTION_STAGE_STATUS_TABLE','tbl_application_alignerco_setups_production_stage_status');

define('MEDICAL_CASES_TABLE','tbl_application_alignerco_cases');
define('MISSING_CASES_TABLE','tbl_application_alignerco_missing_cases');
define('MEDICAL_CASE_PICTURES_TABLE','tbl_application_alignerco_case_pictures');
define('MEDICAL_CASE_PATIENT_PICTURES_TABLE','tbl_application_alignerco_patient_case_pictures');
define('MEDICAL_CASE_PRODUCTION_STAGE_TABLE','tbl_application_alignerco_case_stage');
define('MEDICAL_CASE_STATUS_TABLE','tbl_application_alignerco_case_status');
define('MEDICAL_CASE_OPERATOR_TABLE','tbl_application_alignerco_case_operator');

define('MEDICAL_KIT_SEND_RECEIVE_CUSTOMER_TABLE','tbl_application_alignerco_kit_send_receive_customer');
define('MEDICAL_IMPRESSIONS_SEND_RECEIVE_PRODUCTION_TABLE','tbl_application_alignerco_impressions_send_receive_production');
define('MEDICAL_ALIGNERS_SEND_RECEIVE_PRODUCTION_TABLE','tbl_application_alignerco_aligners_send_receive_customer');
define('MEDICAL_CASE_TRANSACTION_MODE_TABLE','tbl_application_alignerco_case_transaction_mode_history');
define('MEDICAL_CASE_TREATMENT_MODE_TABLE','tbl_application_alignerco_case_treatment_mode_history');
define('MEDICAL_PATIENT_DISEASES_TABLE','tbl_application_alignerco_patient_diseases');
define('MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE','tbl_application_alignerco_doctor_assign_cases');
define('MEDICAL_DOCTOR_CASE_STATUS_TABLE','tbl_application_alignerco_doctor_case_status');
define('MEDICAL_DOCTOR_ASSIGN_CASE_STATUS_TABLE','tbl_application_alignerco_doctor_assign_cases_status');
define('MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE','tbl_application_alignerco_doctor_assign_cases_history');
define('PATIENT_APPROVE_STATUS_TABLE','tbl_application_alignerco_patient_approve_status');

define('ASSIGNED_STATES_TABLE','tbl_application_setups_assigned_states');
define('INVENTORY_RECORD_TABLE','tbl_application_medical_cases_temporary_inventory_record');
define('INVENTORY_ITEMS_TABLE','tbl_application_medical_cases_temporary_inventory_items');
define('INVENTORY_CASES_TABLE','tbl_application_medical_inventory_cases');
define('CASE_SETUP_UPLOAD_DATA_TABLE','tbl_application_medical_case_setup_upload_data');

//email constants
define('ALIGNERCO_EMAIL','noreply@alignerco.com');
define('ALIGNERCO_EMAIL_PASSWORD','#AC#2020#');
define('ALIGNERCO_RECIEVER_EMAIL','team@alignerco.com');
define('ALIGNERCO_DOMAIN','portal.alignerco.com');

define('ALIGNERCO_CANADA_EMAIL','noreply@alignerco.ca');
define('ALIGNERCO_CANADA_EMAIL_PASSWORD','#AC#2020#');
define('ALIGNERCO_CANADA_RECIEVER_EMAIL','team@alignerco.ca');
define('ALIGNERCO_CANADA_DOMAIN','portal.alignerco.ca');

define('STRAIGHT_MY_TEETH_EMAIL','noreply@straightmyteeth.com');
define('STRAIGHT_MY_TEETH_EMAIL_PASSWORD','#SMT#2020#');
define('STRAIGHT_MY_TEETH_RECIEVER_EMAIL','info@straightmyteeth.com');
define('STRAIGHT_MY_TEETH_DOMAIN','portal.straightmyteeth.com');

define('SMILEPATH_EMAIL','noreply@smilepath.com.au');
define('SMILEPATH_EMAIL_PASSWORD','#SP#2020#');
define('SMILEPATH_RECIEVER_EMAIL','team@smilepath.com.au');
define('SMILEPATH_DOMAIN','portal.smilepath.com.au');


/*define('COMPANY_EMAIL','fahad.akbar@clearpathortho.com');*/
define('PRODUCTION_EMAIL','production@clearpathortho.com');

//company
define('ALIGNERCO',49);
define('ALIGNERCO_CANADA',52);
define('STRAIGHT_MY_TEETH',30);
define('SMILEPATH',59);



/* End of file constants.php */
/* Location: ./application/config/constants.php */
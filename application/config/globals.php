<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	    
/*
|--------------------------------------------------------------------------
| GLOBAL VERIABLES FOR APPLICATION
|--------------------------------------------------------------------------
|
| CUSTOM VARIABLES FOR APPLICATION MANAGE
|
*/

		/*Template Page Setting Veriables*/
		$config['pageName']					= NULL;
		$config['pageTitle']				= NULL;
		$config['pageDescription']			= NULL;
		$config['pageKeywords']				= NULL;
		
		
		$config['searchPanel']				= NULL;
		
		
		$config['maximumDays']				= 30;
		$config['maximumWeeks']				= 4;
		$config['maximumMonths']			= 12;
		
		
		
?>
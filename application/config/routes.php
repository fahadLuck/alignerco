<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] 																	= "cpanel/login/index/";
$route['404_override'] 																			= '';


/**  LOGIN / LOGOUT -  URL */
$route['cpanel'] 																				=  "cpanel/login/index/";
$route['logout'] 																				=  "cpanel/login/signOut/";

$route['operator-not-exist']    																=  "cpanel/login/operatorNotFound";
$route['not-found']				    															=  "cpanel/login/page404/";

$route['my-dashboard']    	 																	=  "cpanel/dashboard/index/";

/*$route['my-dashboard']    	 																	=  "cpanel/cases/medicalCases";*/



/** USERS, ROLES & PERMISSIONS -  URL */

$route['users']    		    																	=  "cpanel/users/index/";

$route['user-add']    		    																=  "cpanel/users/userAdd/";

$route['user-edit']    	 																		=  "cpanel/users/userEdit/";
$route['user-edit/(:any)']    																	=  "cpanel/users/userEdit/$1";

$route['user-remove']    		    															=  "cpanel/users/deleteUser/";
$route['user-remove/(:any)']      																=  "cpanel/users/deleteUser/$1";


/**  EMPLOYEES WIZARD -  URL */
$route['employees']    		    																=  "cpanel/hr_employees/index/";
$route['employees/(:any)']    		    														=  "cpanel/hr_employees/index/$1";

$route['employee-add']    		    															=  "cpanel/hr_employees/employeeAdd/";

$route['employee-edit/(:any)']    		    															=  "cpanel/hr_employees/employeeEdit/$1";

$route['employee-edit-processes/(:any)']    		    															=  "cpanel/hr_employees/employeeEditProcesses/$1";

$route['employee-remove']    		    														=  "cpanel/hr_employees/employeeRemove/";
$route['employee-remove/(:any)']    															=  "cpanel/hr_employees/employeeRemove/$1";



/**  DOCTORS -  URL */
$route['doctors']    		    																=  "cpanel/doctors/index/";

/*$route['employees/(:any)']    		    														=  "cpanel/hr_employees/index/$1";*/

$route['doctor-add']    		    															=  "cpanel/doctors/doctorAdd/";

$route['doctor-edit/(:any)']    		    															=  "cpanel/doctors/doctorEdit/$1";

$route['doctor-edit-processes/(:any)']    		    															=  "cpanel/doctors/doctorEditProcesses/$1";

$route['doctor-remove']    		    														=  "cpanel/doctors/doctorRemove/";
$route['doctor-remove/(:any)']    															=  "cpanel/doctors/doctorRemove/$1";




/** SETUPS URLS */
$route['manage-stages']    	 																	=  "cpanel/setups/stages/";
$route['manage-stages/(:any)']    	 															=  "cpanel/setups/stages/$1";

$route['add-stage']    	 																		=  "cpanel/setups/addStage/";

$route['edit-stage']    	 																	=  "cpanel/setups/editStage";
$route['edit-stage/(:any)']    	 																=  "cpanel/setups/editStage/$1";

$route['remove-stage']    	 																	=  "cpanel/setups/deleteStage";
$route['remove-stage/(:any)']    	 															=  "cpanel/setups/deleteStage/$1";

$route['more-stages']    	 																	=  "cpanel/setups/moreStages/";
$route['more-stages/(:any)']    	 															=  "cpanel/setups/moreStages/$1";
$route['more-stages/(:any)/(:any)']    	 														=  "cpanel/setups/moreStages/$1/$1";


$route['add-more-stage']    	 																=  "cpanel/setups/addMoreStage/";
$route['add-more-stage/(:any)']    	 															=  "cpanel/setups/addMoreStage/$1";

$route['edit-more-stage']    	 																=  "cpanel/setups/editMoreStage";
$route['edit-more-stage/(:any)']    	 														=  "cpanel/setups/editMoreStage/$1";

$route['remove-more-stage']    	 																=  "cpanel/setups/deleteMoreStage/";
$route['remove-more-stage/(:any)']    	 														=  "cpanel/setups/deleteMoreStage/$1";
$route['remove-more-stage/(:any)/(:any)']    	 												=  "cpanel/setups/deleteMoreStage/$1/$1";


$route['stage-status']    	 																	=  "cpanel/setups/stageStatus";
$route['stage-status/(:any)']    	 															=  "cpanel/setups/stageStatus/$1";

$route['add-status']    	 																	=  "cpanel/setups/addStatus/";
$route['add-status/(:any)']    	 																=  "cpanel/setups/addStatus/$1";

$route['edit-status']    	 																	=  "cpanel/setups/editStatus/";
$route['edit-status/(:any)']    	 															=  "cpanel/setups/editStatus/$1";
$route['edit-status/(:any)/(:any)']    	 														=  "cpanel/setups/editStatus/$1/$1";

$route['remove-status']    	 																	=  "cpanel/setups/deleteStatus/";
$route['remove-status/(:any)']    	 															=  "cpanel/setups/deleteStatus/$1";
$route['remove-status/(:any)/(:any)']    	 													=  "cpanel/setups/deleteStatus/$1/$1";

$route['more-status']    	 																	=  "cpanel/setups/moreStatus/";
$route['more-status/(:any)']    	 															=  "cpanel/setups/moreStatus/$1";

$route['add-more-status']    	 																=  "cpanel/setups/addMoreStatus/";
$route['add-more-status/(:any)']    	 														=  "cpanel/setups/addMoreStatus/$1";
$route['add-more-status/(:any)/(:any)']    	 													=  "cpanel/setups/addMoreStatus/$1/$1";

$route['edit-more-status']    	 																=  "cpanel/setups/editMoreStatus/";
$route['edit-more-status/(:any)']    	 														=  "cpanel/setups/editMoreStatus/$1";
$route['edit-more-status/(:any)/(:any)']    	 												=  "cpanel/setups/editMoreStatus/$1/$1";

$route['remove-more-status']    	 															=  "cpanel/setups/deleteMoreStatus/";
$route['remove-more-status/(:any)']    	 														=  "cpanel/setups/deleteMoreStatus/$1";
$route['remove-more-status/(:any)/(:any)']    	 												=  "cpanel/setups/deleteMoreStatus/$1/$1";


/** MANAGE CASES URLS */

$route['case-add']    	 																		=  "cpanel/cases/caseAdd/";

$route['cases-missing']    	 																	=  "cpanel/cases/caseMissing/";

$route['cases-missing-details/(:any)']    		    	                                        =  "cpanel/cases/caseMissingDetails/$1";

$route['manage-case-pictures']    	 															=  "cpanel/cases/casePictures/";
$route['manage-case-pictures/(:any)']    	 													=  "cpanel/cases/casePictures/$1";

$route['upload-case-pictures']    	 															=  "cpanel/cases/uploadCasePictures/";
$route['upload-case-pictures/(:any)']    	 													=  "cpanel/cases/uploadCasePictures/$1";
$route['upload-case-pictures/(:any)/(:any)']    	 											=  "cpanel/cases/uploadCasePictures/$1/$1";


$route['remove-case-pictures']    	 															=  "cpanel/cases/removeCasePictures/";
$route['remove-case-pictures/(:any)']    	 													=  "cpanel/cases/removeCasePictures/$1";


/*$route['case-edit']    	 																		=  "cpanel/cases/caseEdit";
$route['case-edit/(:any)']    	 																=  "cpanel/cases/caseEdit/$1";*/

$route['edit-case']    	 																		=  "cpanel/cases/editCase";
$route['edit-case/(:any)']    	 																=  "cpanel/cases/editCase/$1";
$route['edit-case/(:any)/(:any)']    	 														=  "cpanel/cases/editCase/$1/$1";

$route['case-remove']    	 																	=  "cpanel/cases/deleteCase";
$route['case-remove/(:any)']    	 															=  "cpanel/cases/deleteCase/$1";

$route['cases-list']    	 																	=  "cpanel/cases/medicalCases";
$route['doctor-cases-list/(:any)']    	 												        =  "cpanel/cases/medicalDoctorCases/$1";

$route['cases-date-convert']    	 															=  "cpanel/cases/convertDate";


$route['cases-data-with-pagination']    	 													=  "cpanel/ajax/AJAX_Pagination_cases";
$route['cases-data-with-pagination/(:any)']    	 												=  "cpanel/ajax/AJAX_Pagination_cases/$1";


$route['doctor-cases-data-with-pagination']    	 												=  "cpanel/ajax/AJAX_Pagination_doctor_cases";
$route['doctor-cases-data-with-pagination/(:any)']    	 										=  "cpanel/ajax/AJAX_Pagination_doctor_cases/$1";


$route['case']    	 																			=  "cpanel/cases/caseDetails";
$route['case/(:any)']    	 																	=  "cpanel/cases/caseDetails/$1";

$route['case-timeline']    	 																	=  "cpanel/cases/caseTimeline";
$route['case-timeline/(:any)']    	 															=  "cpanel/cases/caseTimeline/$1";

$route['case-timeline-read-out']    	 														=  "cpanel/cases/caseTimelineReadOnly";
$route['case-timeline-read-out/(:any)']    	 													=  "cpanel/cases/caseTimelineReadOnly/$1";


$route['kit-send-to-customer']    	 															=  "cpanel/cases/kitSendToCustomer";
$route['kit-send-to-customer/(:any)']    	 													=  "cpanel/cases/kitSendToCustomer/$1";

$route['impressions-send-to-production']    	 												=  "cpanel/cases/impressionsSendToProduction";

$route['case-assign-to-doctor']    	 												            =  "cpanel/cases/assignCaseToDoctor";

$route['change-doctor-case-status']    	 												        =  "cpanel/cases/changeDoctorCaseStatus";

$route['aligners-send-to-customer']    	 														=  "cpanel/cases/alignersSendToCustomer";
$route['aligners-send-to-customer/(:any)']    	 												=  "cpanel/cases/alignersSendToCustomer/$1";



$route['change-case-status']    	 															=  "cpanel/cases/changeCaseStatus";
$route['update-case-status']    	 															=  "cpanel/cases/updateCaseStatus";


$route['send-to-production-cases']    	 														=  "cpanel/cases/sendToProductionCases";
$route['send-to-production-cases-data-with-pagination']    	 									=  "cpanel/ajax/AJAX_Pagination_sendToProductionCases";
$route['send-to-production-cases-data-with-pagination/(:any)']    	 							=  "cpanel/ajax/AJAX_Pagination_sendToProductionCases/$1";


$route['get-child-production-stages']    	 													=  "cpanel/ajax/AJAX_chidProductionStages/";
$route['load-more-stage']    	 																=  "cpanel/ajax/AJAX_DataTypeContentParentsChilds/";

$route['load-stage-status']    																	=  "cpanel/ajax/AJAX_getStageStatus/";
$route['load-status-childs']    																=  "cpanel/ajax/AJAX_getStatusChilds/";
$route['load-stage-operators']    																=  "cpanel/ajax/AJAX_getStageOperators/";

$route['cases-data-with-pagination']    	 													=  "cpanel/ajax/AJAX_Pagination_cases";
$route['cases-data-with-pagination/(:any)']    	 												=  "cpanel/ajax/AJAX_Pagination_cases/$1";

$route['load-companies-by-country']    															=  "cpanel/ajax/AJAX_getCompaniesByCountry/";
$route['load-states-by-country']    															=  "cpanel/ajax/AJAX_getStatesByCountry/";
$route['load-company-case-types']    															=  "cpanel/ajax/AJAX_getCompanyCaseTypes/";
$route['load-company-case-types']    															=  "cpanel/ajax/AJAX_getCompanyCaseTypes/";

$route['get-doctors']    															            =  "cpanel/ajax/getDoctors/";
$route['get-doctors-states']    													            =  "cpanel/ajax/getDoctorStates/";

$route['show-states']    															            =  "cpanel/ajax/showStates/";

$route['update-patient-approve-status']    														=  "cpanel/cases/updatepatientApproveStatus/";

$route['remove-patient-approve-status']    														=  "cpanel/cases/removePatientApproveStatus/";



/**  States -  URL */
$route['states']    		    																=  "cpanel/settings/index/";

/*$route['employees/(:any)']    		    														=  "cpanel/hr_employees/index/$1";*/

$route['state-add']    		    															    =  "cpanel/settings/stateAdd";

$route['state-edit/(:any)']    		    														=  "cpanel/settings/stateEdit/$1";

/*$route['doctor-edit-processes/(:any)']    		    															=  "cpanel/doctors/doctorEditProcesses/$1";*/

$route['state-remove']    		    														     =  "cpanel/settings/stateRemove/";
$route['state-remove/(:any)']    															     =  "cpanel/settings/stateRemove/$1";


/** Patients  URL */

$route['patient-list']    		    															=  "cpanel/patient/index/";
$route['patient']    		    																=  "cpanel/patient/patientAdd/";
$route['patient-images']    		    													    =  "cpanel/patient/uploadImages/";

$route['upload-patient-pictures']    	 													    =  "cpanel/patient/uploadPatientPictures/";
$route['upload-patient-pictures/(:any)']    	 												=  "cpanel/patient/uploadPatientPictures/$1";
$route['upload-patient-pictures/(:any)/(:any)']    	 											=  "cpanel/patient/uploadPatientPictures/$1/$1";

$route['view-images/(:any)']    	 												            =  "cpanel/patient/viewImages/$1";
$route['patient-edit']    	 																	=  "cpanel/patient/patientEdit/";
$route['patient-edit/(:any)']    																=  "cpanel/patient/patientEdit/$1";
$route['picture-action']    																    =  "cpanel/patient/pictureAction/";
$route['remove-patient-picture/(:any)']    														=  "cpanel/patient/removePatientPicture/$1";


/** Reports  URL */

$route['doctor-report']    		    															=  "cpanel/reports/doctorReport/";



//APIs URLS FOR TESTING 
$route['shipment-api']    	 																	  =  "cpanel/shipmentApi/index";
$route['link-api']    	 																	      =  "cpanel/linkApi/index";
$route['upload-picture']    	 															      =  "cpanel/FileUpload/index/";
$route['upload']    	 															              =  "cpanel/FileUpload/upload_file/";





/* End of file routes.php */
/* Location: ./application/config/routes.php */
<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

	$dataArray = array();
	
	function authentication() { 
		
		$CIH = & get_instance(); 
		
		if($CIH->session->userdata(USER_LOGIN_SESSION) == FALSE) {
		
			  redirect('cpanel/');
		
		} else {

			  return true;	 
		}
	}
	
	function checkLogin() {
		
		$CIH = & get_instance();
		
		if($CIH->session->userdata(USER_LOGIN_SESSION)) {
		
			redirect('my-dashboard');
		
		
		} else {
			
			 return true;	
		}
	}
	
	function loggedIn() {
		 
		$CIH = & get_instance();
			
			$accountID = $CIH->session->userdata(USER_ACCOUNT_ID_SESSION);
		
				  if ($accountID == '') { 
						  
						  return false;
				  } else {
						  
						  return true;
				  }
	}
	
?>
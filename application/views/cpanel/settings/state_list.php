<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">States</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           
           <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
           
           
          <div class="box">
            <div class="box-header">
              
             
            
              <h3 class="box-title">State <small>(<?php echo $totalResults; ?>)</small></h3>
		      <button onclick="window.location.href='<?php echo base_url(); ?>state-add/'"type="button" class="btn btn-blue pull-right"><i class="fa fa-plus"></i> Add States</button>
                <div class="box-tools">
                  <?php echo $pagingLink;?>
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="example" class="table table-responsive" style="width:100%">
                <thead>
                <tr>
                  <th width="124">Sr.</th>
                  <th width="134">Country</th>
                  <th width="182">State</th>
                  <th width="90">&nbsp;</th>
                </tr>
              </thead>
               <tbody>
               
               <?php if ($activeStates) {
                        
						$counter = $serialNumber;
						foreach($activeStates->result() as $st) {
			  								
											$tableID 								=   encodeString($st->tableID); // Calling 
                      $countryName            = $st->countryname; 
                      $stateName            = $st->stateName; 
                            ?>
                                    
                              <tr>
                                    <td><?php echo $counter; ?></td>
                                    <td><?php echo $countryName; ?></td>
                                    <td><?php echo $stateName; ?></td>

                                    <td><div class="pull-right"><a href="<?php echo base_url(); ?>state-edit/<?php echo $tableID; ?>/" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
                                      <a href="<?php echo base_url(); ?>state-remove/<?php echo $tableID;?>/" onclick="return confirm('Are you sure you want to remove state <?php echo $stateName.' ('.$countryName.')'; ?> ?');" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                        </div>
                                    </td>
                                    
                                </tr>
                                    
                    
                    <?php $counter++;
					
						 /*}*/
            }
					  }
				?>
               
               
               </tbody>
                  <tfoot>
                      <tr>
                        <tr>
                          <th width="24">Sr.</th>
                          <th width="34">Country</th>
                          <th width="182">State</th>
                          <th width="90">&nbsp;</th>
                  
                        </tr>
                      </tr>
                  </tfoot>
              </table>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>


    <!--================================== model  star=================================== -->

<div id="dataModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-transform: capitalize; font-family: arial;">State & Licience</h4>
      </div>
      <div class="modal-body" id="vid_details">
       <!-- body start here --> 



      </div>
    
    </div>

  </div>
</div>
<!--================================= model end=======================================  -->



    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
</script>


<script>
   $(document).ready(function () {


        $('.view_state').click(function() {

          var tableID = $(this).attr("id")

          $.ajax({
            url:"<?= base_url() ?>show-states",
            method:"post",
            data:{tableID :tableID},
            success: function(data){
              /*alert(data);*/
              $('#vid_details').html(data);
              $('#dataModal').modal("show");
            }

          });
            

        });

  });
   
</script>
   
   
   
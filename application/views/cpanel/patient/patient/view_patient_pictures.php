<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>backend_css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>backend_js/dropzone.js"></script>


<script>

function removePhotos() {
	
	if (confirm('Are sure want to remove case pictures?')) {
    	
		var URL = '<?php echo base_url(); ?>remove-case-pictures/<?php echo encodeString($ID); ?>';
		
		window.location.href = URL;
		
	} else {
		
		return false;
	}
	
}

</script>

<!-- fancybox -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend_assets/vendor_components/lightbox-master/dist/ekko-lightbox.css" />

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">View Pictures</a></li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     	
        				<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
    <?php 
			 	if ($ID) {
						
						$ID = encodeString($ID);	
				}
	 ?>
     
     <!-- <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-cloud-upload"></i> Upload Intra &amp; Extra Oral Pictures</h3>
          	<div class="pull-right box-tools">
              <a href="<?php echo base_url(); ?>patient-images"><i class="fa fa-refresh"></i></a>
            </div>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
             <form action="<?php echo base_url(); ?>upload-patient-pictures/<?php echo encodeString($ID); ?>/" class="dropzone"></form>
	        </div>
          </div>
        </div>
 	  </div> -->




 	  <?php if ($photos->num_rows() > 0) {  ?>
      
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-camera"></i> Intra &amp; Extra Oral Pictures</h3>
          <!-- <div class="pull-right box-tools">
            	<a href="javascript:void(0);" onClick="return removePhotos();"><i class="fa fa-trash"></i></a>
             </div> -->
      </div>

      <div class="timeline-body">
		                  
        <!--  <?php foreach($photos->result() as $photo) {
          $pic_type=$photo->type; 
          $public_url=$photo->public_url;
         	?>
         	<?php
	         	if($pic_type=='pdf')
	         	{
	         		?><a href="<?= $public_url ?>" download="file">
					  <img src="https://cdn3.iconfinder.com/data/icons/brands-applications/512/File-512.png" alt="file" style="height: 100px;" width="104" height="142">
					</a><?php
	         	} 
	         	else
	         	{
	         		?><a href="<?= $public_url ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="image_name"><img  style="padding-top:10px; padding-left:5px height: 100px; width: 100px;" src="<?= $public_url ?>" alt="gallery" ></a><?php
	         	}
         	?>
                 
         <?php } ?> -->

         <div class="box-body no-padding">

         	<form method="post" action="<?= base_url() ?>picture-action">

            <!-- <input type='submit' class="btn btn-info" value='Bulk Voucher' name='but_delete'> -->

            <br><br>

           <div class="row" style="margin-left: 20px;">
                <div class="checkbox">
                      <input type='checkbox' id="checkAll" >
                      <label for="checkAll">Check all</label>

                      <button style="margin-left: 20px;" type='submit' class="btn btn-success" value='Download Selected Pictures' name='active_alert'>
                        <span class="glyphicon glyphicon-download-alt"></span> Download Selected Pictures
                      </button>

                      <!-- <button style="margin-left: 20px;" type='submit' class="btn btn-danger" value='Download Selected Pictures' name='delete_alert'>
                        <span class="glyphicon glyphicon-trash"></span> Delete Selected Pictures
                      </button> -->
                </div>
            </div> 


              <table id="employee" class="table table-responsive" style="width:100%">
              	<thead>
                <tr>
                  <th>&nbsp;</th>
                  <th width="">Sr.</th>
                  <th width="">Picture</th>
                  <th width="">Type</th>
                  <th width="">Date</th>
                  <th width="">Time</th>
                </tr>
            </thead>
               <tbody>
               	<?php 
               	$count=1;
               	foreach($photos->result() as $photo) 
               	{
		            $pic_type                            = $photo->type;
                $pic_name                            = $photo->name; 
		            $picID                               = $photo->id;
		            $created                             = $photo->created; 
		            $public_url                          = $photo->public_url;
		            $counter                             = $count++;
		            $CreatedDate				 = date('F d, Y',$created);
	                $CreatedTime		 		 = date('h:i A',$created);
	                $CreatedTimeAgo			 = timeAgo($created).' ago'; // Calling From Shared Helper
			    ?>
                               
                    <tr>
                    

                    <td>
                        <div class="row checkbox" style="margin-left: 10px;">
  	                        <input type='checkbox' name='delete[]' value="<?= $public_url; ?>" id="<?= $picID; ?>" >
              						  <label for="<?= $picID; ?>"></label>

                            <a href="<?= $public_url ?>" download="file" style="color: green; font-size: 20px;"><span class="glyphicon glyphicon-download-alt"></span></a>

                            <a href="<?php echo base_url(); ?>remove-patient-picture/<?php echo encodeString($picID); ?>/"  style="margin-left: 10px; color: red; font-size: 20px;"><span class="glyphicon glyphicon-trash"></span></a>
  	                    </div>
                    </td>

                    <td><?= $counter ?></td>
                    <td>
	                    <?php
				         	if($pic_type=='pdf')
				         	{
				         		?><a href="<?= $public_url ?>" download="file">
								  <img src="https://cdn3.iconfinder.com/data/icons/brands-applications/512/File-512.png" alt="file" style="height: 100px;" width="104" height="142">
								</a><?php
				         	} 
				         	else
				         	{
				         		?><a href="<?= $public_url ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?= $CreatedDate; ?>"><img  style="padding-top:10px; padding-left:5px height: 100px; width: 100px;" src="<?= $public_url ?>" alt="gallery" ></a><?php
				         	}
				         ?>
                    </td>
                    <td><?= $pic_type ?></td>
                    <td><?= $CreatedDate ?></td>
                    <td><?= $CreatedTimeAgo ?></td>
                    </td>
                </tr>
                <?php } ?>
                                   
               
               </tbody>
               <!-- <tfoot>
                      <tr>
                        <th width="32">Sr.</th>
		                  <th width="118">Code</th>
		                  <th width="122">Name</th>
		                  <th width="144">Official Email</th>
		                  <th width="123">Password</th>
		                  <th width="131">Role</th>
		                  <th width="159">Moblie</th>
                      </tr>
                  </tfoot> -->
              </table>
          </form>
            </div>
		
        </div>
          <br />
       <br />
 	  </div>
      
      <?php } ?>
      

   
    </section>


     <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
  $("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
</script>


<script>
    $(document).ready(function() {
    $('#employee').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
</script>
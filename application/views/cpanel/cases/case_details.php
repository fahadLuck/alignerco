<!-- jQuery 3 -->

<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> -->
 <!--  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
 <script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>
  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>




<script language="javascript" type="text/javascript">

 $(document).ready(function () {
 
   $('.update-status').click(function() {
              
     var presentStageID   = '<?php echo $presentProductionStageID; ?>';
     var presentStageName   = $('#present-stage-name').text();
     
     var presentStatus    = '<?php echo $presentStatusID; ?>';
     var presentStatusName  = '<?php echo $presentStatusName; ?>';
    
    var message = 'Are you sure change status case# <?php echo $case['caseID']; ?> "'+presentStatusName+'?"';
      
      if (confirm(message)) {
       
          $("#form-update-status").submit();
          return false;
         
      } else {
        
          return false;  
      }
   });
 
   $('#aligners-send-to-customer').click(function() {
        
     var presentStageID   = '<?php echo $presentProductionStageID; ?>';
     var presentStageName   = $('#present-stage-name').text();
     
     var presentStatus    = '<?php echo $presentStatusID; ?>';
     var presentStatusName  = '<?php echo $presentStatusName; ?>';
    
    var message = 'Are you sure want to send aligners to customer?"';
      
      if (confirm(message)) {
       
          $("#frm-aligners-send-to-customer").submit();
          return false;
         
      } else {
        
          return false;  
      }
    
    });
 });

</script>




<script>
$(document).ready(function(){

  doctorID=$('#doctor').val();  
         $.ajax({
                 url:"<?php echo base_url() ?>get-doctors",
                 method:"post",
                // dataType: 'json',
                data:{doctorID :doctorID},
                // dataType: 'json',
                success: function(data)
                {
                  /*alert(doctorID);*/
                  $("#stat").html(data);   
                },
                 error: function(xhr, status, error) 
                 {
                     // check status && error
                     alert("this is error "+error);
                  },
            });


    $('#state').change(function(){

        /*var caseID     = $(this).val();
        var caseNumber = $(this).attr('key');*/ 
         stateID=$(this).val();
         doctorID=$('#doctor').val();
         $.ajax({
                 url:"<?php echo base_url() ?>get-doctors",
                 method:"post",
                // dataType: 'json',
                data:{doctorID :doctorID,stateID :stateID},
                // dataType: 'json',
                success: function(data)
                {
                  /*alert(doctorID);*/
                  $("#stat").html(data);   
                },
                 error: function(xhr, status, error) 
                 {
                     // check status && error
                     alert("this is error "+error);
                  },
            });

         });


    /*$('#doctor').change(function(){
 
         doctorID=$(this).val();
         stateID=$('#state').val();
         $.ajax({
                 url:"<?php echo base_url() ?>get-doctors",
                 method:"post",,
                data:{doctorID :doctorID,stateID :stateID},

                success: function(data)
                {

                  $("#stat").html(data);   
                },
                 error: function(xhr, status, error) 
                 {
                     alert("this is error "+error);
                  },  
            });

         });*/


        
      });  
</script>




<script>
$(document).ready(function(){

  doctorID=$('#caseStatusID').val();  
         if (doctorID == <?= MODIFIED ?> || doctorID == <?= APPROVED ?>)
         {
          $('#comment').show(1000);
         }

         if (doctorID != <?= MODIFIED ?> && doctorID != <?= APPROVED ?>)
         {
          $('#comment').hide(1000);
         }


    $('#caseStatusID').change(function(){

        /*var caseID     = $(this).val();
        var caseNumber = $(this).attr('key');*/ 
         doctorID=$(this).val();
         if (doctorID == <?= MODIFIED ?> || doctorID == <?= APPROVED ?>)
         {
          $('#comment').show(1000);
         }

         if (doctorID != <?= MODIFIED ?> && doctorID != <?= APPROVED ?>)
         {
          $('#comment').hide(1000);
         }
         

         });
        
      });  
</script>


<!-- for doctor states -->
<script>
$(document).ready(function(){

  doctorID=$('#doctor').val();  
         $.ajax({
                 url:"<?php echo base_url() ?>get-doctors-states",
                 method:"post",
                // dataType: 'json',
                data:{doctorID :doctorID},
                // dataType: 'json',
                success: function(data)
                {
                  $("#state").html(data);   
                },
                 error: function(xhr, status, error) 
                 {
                     // check status && error
                     alert("this is error "+error);
                  },
            });


    $('#doctor').change(function(){

        /*var caseID     = $(this).val();
        var caseNumber = $(this).attr('key');*/ 
         doctorID=$(this).val();
        
         $.ajax({
                 url:"<?php echo base_url() ?>get-doctors-states",
                 method:"post",
                // dataType: 'json',
                data:{doctorID :doctorID},
                // dataType: 'json',
                success: function(data)
                {
                  $("#state").html(data);   
                },
                 error: function(xhr, status, error) 
                 {
                     // check status && error
                     alert("this is error "+error);
                  },
            });

         });
        
      });  
</script>
<!-- for doctor states ends -->






<script>
  /*md_checkbox_2*/
  $(document).ready(function() {

  /*$('#textbox1').val($(this).is(':checked'));

  $('#checkbox1').change(function() {
    $('#textbox1').val($(this).is(':checked'));
  });*/


  if ($('#md_checkbox_2').is(':checked')) 
    {
      $('#approved_div').show(1000);
      $('#assign_div').show(1000);
    }

  $('#md_checkbox_2').click(function() {

    if ($(this).is(':checked')) 
    {
       caseID=$('#md_checkbox_2').val();
       /*$('#approved_div').show(1000);*/
       $.ajax({
                 url:"<?php echo base_url() ?>update-patient-approve-status",
                 method:"post",
                // dataType: 'json',
                data:{caseID :caseID},
                // dataType: 'json',
                success: function(data)
                {
                  /*alert(data);*/
                  $('#approved_div').show(1000);  
                  $('#assign_div').show(1000); 
                },
                 error: function(xhr, status, error) 
                 {
                     // check status && error
                     alert("this is error "+error);
                  },
            });
    }
    else
    {
      caseID=$('#md_checkbox_2').val();
       $.ajax({
                 url:"<?php echo base_url() ?>remove-patient-approve-status",
                 method:"post",
                // dataType: 'json',
                data:{caseID :caseID},
                // dataType: 'json',
                success: function(data)
                {
                  /*alert(data);*/
                  $('#approved_div').hide(1000);  
                  $('#assign_div').hide(1000);
                },
                 error: function(xhr, status, error) 
                 {
                     // check status && error
                     alert("this is error "+error);
                  },
            });
    }

  });

});
</script>





  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="breadcrumb-item">Case Details</li>
  </ol>
 </section>

  <?php if ($caseFlag == false) { ?>
    
  <div class="pad margin no-print">
      <div class="callout callout-danger" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> ERROR:</h4>
        No case found.
      </div>
    </div>
    
    <?php } ?>
  
    <?php if ($caseFlag == true) { 
      
      $encodeCaseID         = encodeString($case['caseID']); // Calling From General Helper
      
      $caseID             = $case['caseID'];
      $patientName          = $case['patientName'];
      $patientAge           = $case['patientAge'];
      $genderName           = $case['genderName'];
      $archUpper            = $case['archUpper'];
      $archLower            = $case['archLower'];
      $companyID            = $case['companyID'];
      $companyName          = $case['companyName'];
      $companyLogo          = $case['companyLogo'];
      $countryName          = $case['countryName'];
      $caseComments         = $case['caseComments'];
      $caseReceiveDate        = $case['receiveDate'];
      $caseCreated          = $case['caseCreated'];
      $caseCreatedEmployeeName    = $case['employeeName'];
      $caseCreatedEmployeeCode    = $case['employeeCode'];
      $caseCreatedEmployeeID      = $case['employeeID'];
      
      $employeeAssignJobs       = getEmployeeAssignJobs($caseCreatedEmployeeID); // Calling From HR Employees Helper
                    
      if ($employeeAssignJobs) {
            
        $employeeAssignJob                = $employeeAssignJobs->row_array();
        $caseCreatedEmployeeAssignJobDescription    = $employeeAssignJob['jobPositionName'];
        
      
      } else {
          
          $caseCreatedEmployeeAssignJobDescription  = NULL;
      }       
      
      $caseCreatedDate    = date('d F. Y',$caseCreated);
      $caseCreatedTime    = date('h:i A',$caseCreated);
      $caseCreatedTimeAgo   = timeAgo($caseCreated).' ago';
      
      $caseReceived     = date('l, d F Y',strtotime($caseReceiveDate));
      $caseReceivedTimeAgo  = timeAgo(strtotime($caseReceiveDate)).' ago';
      
      if (empty($caseComments)) {
        
          $caseComments  = '';
      }
  ?>
   
    <!-- Main content -->
    <section class="invoice printableArea">
    
               <?php
                 if ($this->session->userdata('admin_msg') !='') {
                  
                    $alertClass   = 'alert-info';
                    $alertHeading   = 'Success';   
                    $alertMessage = $this->session->userdata('admin_msg');
                    
                    $alertIcon    = 'icon fa fa-check';    
                 
                 } else if ($this->session->userdata('admin_msg_error')!='') {
                
                     $alertClass    = 'alert-danger';  
                     $alertHeading  = 'ERROR';  
                     $alertMessage  = $this->session->userdata('admin_msg_error');
                     
                     $alertIcon     = 'icon fa fa-ban'; 
                 }
                
                 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
            ?>
                  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
              
            <?php 
                  
                    $this->session->unset_userdata('admin_msg');
                    $this->session->unset_userdata('admin_msg_error');
                  
                  } 
              ?>
      
      <div class="row">
        <div class="col-12">
          <h2 class="page-header">
            CASE RECEIVED
            <small class="pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $caseReceivedTimeAgo; ?>"><?php echo $caseReceived; ?></small>
          </h2>
        </div>
      </div>
    
      <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
          Case ID: <?php echo $caseID; ?>
          
          <address>
            <span data-toggle="tooltip" title="" data-original-title="Patient Name" data-placement="top"><strong class="text-blue"><?php echo $patientName; ?></strong></span><br>
            <span data-toggle="tooltip" title="" data-original-title="Age" data-placement="top">Age: <?php echo $patientAge; ?></span><br />
            <span data-toggle="tooltip" title="" data-original-title="Age" data-placement="top"><?php echo $genderName; ?></span><br />
            <span data-toggle="tooltip" title="" data-original-title="Arch(es)" data-placement="top"> 
               
         <?php if ($archUpper == HARD_CODE_ID_YES && $archLower == HARD_CODE_ID_YES) {
                             echo 'Upper, Lower'; 
                     } else {
                        
                        if ($archUpper == HARD_CODE_ID_YES) {
                                
                                echo 'Upper'; 
                        }
                        
                        if ($archLower == HARD_CODE_ID_YES) {
                                
                                echo 'Lower'; 
                        }  
                     }
               ?>
               </span> 
              
          </address>
          
        </div>
        <!-- /.col -->


        <?php  
  if (in_array(CASE_MAIN_TIMELINE_PAGE,$accessModules)) { ?>
        <div class="col-sm-6 invoice-col text-right">
         
          <address>
             <?php if ($companyLogo) { ?>
              <img class="profile-user-img img-fluid mx-auto" style="margin:0px;" src="<?php echo base_url(); ?>backend_images/companies/<?php echo $companyLogo; ?>" alt="Company Logo"><br>
             <?php } ?> 
             <strong class="text-green"><?php echo $companyName; ?></strong><br>
            
      <?php echo $countryName; ?><br />
            
            <?php if ($caseComments) { ?>
            
            Comments / Remarks: <?php echo $caseComments; ?>
            
            <?php } ?>
           <br />
           <small>Uploaded by:</small><br>
           <span data-toggle="tooltip" title="" data-original-title="<?php echo $caseCreatedEmployeeAssignJobDescription; ?>"><?php echo $caseCreatedEmployeeName; ?></span><br>
           <span data-toggle="tooltip" title="" data-original-title="<?php echo $caseCreatedTimeAgo; ?>"><?php echo $caseCreatedDate; ?> <?php echo $caseCreatedTime; ?></span><br>

          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-12 invoice-col">
      <div class="invoice-details row no-margin" style="background-color:#FFF;">
        <div class="col-md-6 col-lg-4" style="color:#000"><b>Production Stage: </b> <span style="margin-left:10px;" id="present-stage-name"><?php echo $presentProductionStageName; ?></span></div>
        <div class="col-md-6 col-lg-4" style="color:#000"><b>Status:</b> <span style="margin-left:10px;"><?php echo $presentStatusName; ?></span></div>
        <div class="col-md-6 col-lg-4" style="color:#000"><b>Operator:</b> <span style="margin-left:10px;">
              <?php
                  
                if ($presentCaseOperatorEmployeeID) {
                    
                    echo '<span style="color:#000" data-toggle="tooltip" title="" data-original-title="'.$presentCaseOperatorEmployeeJob.'">'.$presentCaseOperatorEmployeeName.' ('.$presentCaseOperatorEmployeeCode.')</span>';
                } else {
                    echo '----------------';  
                }
              ?>
                        </span>
               </div>
      </div>
    </div>
    <?php 
        }
     ?>

      </div>


      <?php  
  if (in_array(CASE_MAIN_TIMELINE_PAGE,$accessModules)) { ?>
     
     <div class="row">
       <div class="col-12 table-responsive">
          <br />
         <p class="lead" style="color:#000;"><b> Kit Send  /  Receive History</b></p>
         <table class="table table-striped" style="width:100%">
           <thead>
            <tr>
              <th width="4%">#</th>
              <th width="20%">Tracking Number (Send)</th>
              <th width="12%">Service</th>
              <th width="22%">Tracking Number (Receive)</th>
              <th width="10%">Service</th>
              <th width="15%">Operator</th>
              <th width="17%">&nbsp;</th>
             </tr>
           </thead>
           
           <tbody>
           
            <?php 
          
        if($kitSendReceiveCustomerHistory->num_rows() > 0) {
            
            $counter = 1;
            foreach($kitSendReceiveCustomerHistory->result() as $kitSendReceiveHistory) {
              
              $tableID              = $kitSendReceiveHistory->tableID;
              $trackingNumberSending      = $kitSendReceiveHistory->trackingNumberSending;
              $trackingNumberReceiving    = $kitSendReceiveHistory->trackingNumberReceiving;
              $deliveryServiceSendName    = $kitSendReceiveHistory->deliveryServiceSendName;
              $deliveryServiceReceiveName   = $kitSendReceiveHistory->deliveryServiceReceiveName;
              $kitSendReceiveCreated      = $kitSendReceiveHistory->kitSendReceiveCreated;
              $kitSendReceiveCreatedBy    = $kitSendReceiveHistory->kitSendReceiveCreatedBy;
              $kitDataCreatedName       = $kitSendReceiveHistory->employeeName;
              $kitDataCreatedCode         = $kitSendReceiveHistory->employeeCode;
              
              
              $kitDataCreatorAssignJobs   = getEmployeeAssignJobs($kitSendReceiveCreatedBy); // Calling From HR Employees Helper
                    
              if ($kitDataCreatorAssignJobs) {
            
                  $kitDataCreatorAssignJobs   = $kitDataCreatorAssignJobs->row_array();
                  $kitDataCreatorAssignJobs = $kitDataCreatorAssignJobs['jobPositionName'];
        
              } else {
          
                  $kitDataCreatorAssignJobs   = NULL;
              }       
      
              $kitDataCreatedDate     = date('d M. Y',$kitSendReceiveCreated);
              $kitDataCreatedTime     = date('h:i A',$kitSendReceiveCreated);
              $kitDataCreatedTimeAgo    = timeAgo($kitSendReceiveCreated).' ago';
       ?>
             
                        
                       <tr <?php if ($counter == 1) { ?> style="background-color:#cbe091; color:#000;" <?php } ?>>
                        <td <?php if ($counter == 1) { ?> style="color:#000;" <?php } ?>><small style="font-size:14px;"><?php echo $counter; ?></small></td>
                        <td><small style="font-size:14px;"><?php echo $trackingNumberSending; ?></small></td>
                        <td><?php echo $deliveryServiceSendName; ?></small></td>
                        <td><small style="font-size:14px;"><?php echo $trackingNumberReceiving; ?></small>
                        </td>
                        <td><?php echo $deliveryServiceReceiveName; ?></td>
                        <td><small style="font-size:14px;"><span data-toggle="tooltip" title="" data-original-title="<?php echo $kitDataCreatorAssignJobs; ?>"><?php echo $kitDataCreatedName; ?> (<?php echo $kitDataCreatedCode; ?>)</span></small></td>
                        <td><small style="font-size:14px;"><span data-toggle="tooltip" title="" data-original-title="<?php echo $kitDataCreatedTimeAgo; ?>"><?php echo $kitDataCreatedDate; ?> <?php echo $kitDataCreatedTime; ?></span></small></td>
                      </tr>
             
             <?php
          $counter++;
          
            }
          
        } else {
      ?>
              
               <tr>
                  <td colspan="7">No history found.</td>
               </tr>
            
           <?php } ?>
           
           </tbody>
         </table>
          
        </div>
        <!-- /.col -->
      </div>




      
     <div class="row">
       <div class="col-12 table-responsive">
          <br />
         <p class="lead" style="color:#000;"><b> Impressions Send  to Production History</b></p>
         <table class="table table-striped" style="width:100%">
           <thead>
            <tr>
              <th width="4%">#</th>
              <th width="20%">Tracking Number (Send)</th>
              <th width="12%">Service</th>
              <th width="22%">Tracking Number (Receive)</th>
              <th width="10%">Service</th>
              <th width="15%">Operator</th>
              <th width="17%">&nbsp;</th>
             </tr>
           </thead>
           
           <tbody>
           
            <?php 
          
        if($impressionsSendReceiveProductionHistory->num_rows() > 0) {
            
            $counter = 1;
            foreach($impressionsSendReceiveProductionHistory->result() as $impressionsSendReceiveHistory) {
              
              $tableID                  = $impressionsSendReceiveHistory->tableID;
              $trackingNumberSending          = $impressionsSendReceiveHistory->trackingNumberSending;
              $trackingNumberReceiving        = $impressionsSendReceiveHistory->trackingNumberReceiving;
              $deliveryServiceSendName        = $impressionsSendReceiveHistory->deliveryServiceSendName;
              $deliveryServiceReceiveName       = $impressionsSendReceiveHistory->deliveryServiceReceiveName;
              $impressionsSendReceiveCreated      = $impressionsSendReceiveHistory->impressionsSendReceiveCreated;
              $impressionsSendReceiveCreatedBy    = $impressionsSendReceiveHistory->impressionsSendReceiveCreatedBy;
              $impressionsDataCreatedName       = $impressionsSendReceiveHistory->employeeName;
              $impressionsDataCreatedCode         = $impressionsSendReceiveHistory->employeeCode;
              
              
              $impressionsDataCreatorAssignJobs   = getEmployeeAssignJobs($impressionsSendReceiveCreatedBy); // Calling From HR Employees Helper
                    
              if ($impressionsDataCreatorAssignJobs) {
            
                  $impressionsDataCreatorAssignJobs   = $impressionsDataCreatorAssignJobs->row_array();
                  $impressionsDataCreatorAssignJobs = $impressionsDataCreatorAssignJobs['jobPositionName'];
        
              } else {
          
                  $impressionsDataCreatorAssignJobs   = NULL;
              }       
      
              $impressionsDataCreatedDate     = date('d M. Y',$impressionsSendReceiveCreated);
              $impressionsDataCreatedTime     = date('h:i A',$impressionsSendReceiveCreated);
              $impressionsDataCreatedTimeAgo    = timeAgo($impressionsSendReceiveCreated).' ago';
       ?>
             
                        
                       <tr <?php if ($counter == 1) { ?> style="background-color:#cbe091; color:#000;" <?php } ?>>
                        <td <?php if ($counter == 1) { ?> style="color:#000;" <?php } ?>><small style="font-size:14px;"><?php echo $counter; ?></small></td>
                        <td><small style="font-size:14px;"><?php echo $trackingNumberSending; ?></small></td>
                        <td><?php echo $deliveryServiceSendName; ?></small></td>
                        <td><small style="font-size:14px;"><?php echo $trackingNumberReceiving; ?></small>
                        </td>
                        <td><?php echo $deliveryServiceReceiveName; ?></td>
                        <td><small style="font-size:14px;"><span data-toggle="tooltip" title="" data-original-title="<?php echo $impressionsDataCreatorAssignJobs; ?>"><?php echo $impressionsDataCreatedName; ?> (<?php echo $impressionsDataCreatedCode; ?>)</span></small></td>
                        <td><small style="font-size:14px;"><span data-toggle="tooltip" title="" data-original-title="<?php echo $impressionsDataCreatedTimeAgo; ?>"><?php echo $impressionsDataCreatedDate; ?> <?php echo $impressionsDataCreatedTime; ?></span></small></td>
                      </tr>
             
             <?php
          $counter++;
          
            }
          
        } else {
      ?>
              
               <tr>
                  <td colspan="7">No history found.</td>
               </tr>
            
           <?php } ?>
           
           </tbody>
         </table>
          
        </div>
        <!-- /.col -->
      </div>



      
     <div class="row">
       <div class="col-12 table-responsive">
          <br />
         <p class="lead" style="color:#000;"><b> Aligners Send to Customer History</b></p>
         <table class="table table-striped" style="width:100%">
           <thead>
            <tr>
              <th width="4%">#</th>
              <th width="20%">Tracking Number (Send)</th>
              <th width="12%">Service</th>
              <th width="15%">Operator</th>
              <th width="17%">&nbsp;</th>
             </tr>
           </thead>
           
           <tbody>
           
            <?php 
          
        if($alignersSendReceiveCustomerHistory->num_rows() > 0) {
            
            $counter = 1;
            foreach($alignersSendReceiveCustomerHistory->result() as $alignersSendReceiveHistory) {
              
              $tableID                  = $alignersSendReceiveHistory->tableID;
              $trackingNumberSending          = $alignersSendReceiveHistory->trackingNumberSending;
              $deliveryServiceSendName        = $alignersSendReceiveHistory->deliveryServiceSendName;
              $alignersSendReceiveCreated     = $alignersSendReceiveHistory->alignersSendReceiveCreated;
              $alignersSendReceiveCreatedBy     = $alignersSendReceiveHistory->alignersSendReceiveCreatedBy;
              $alignersDataCreatedName        = $alignersSendReceiveHistory->employeeName;
              $alignersDataCreatedCode          = $alignersSendReceiveHistory->employeeCode;
              
              
              $alignersDataCreatorAssignJobs    = getEmployeeAssignJobs($alignersSendReceiveCreatedBy); // Calling From HR Employees Helper
                    
              if ($alignersDataCreatorAssignJobs) {
            
                  $alignersDataCreatorAssignJobs  = $alignersDataCreatorAssignJobs->row_array();
                  $alignersDataCreatorAssignJobs  = $alignersDataCreatorAssignJobs['jobPositionName'];
        
              } else {
          
                  $$alignersDataCreatorAssignJobs   = NULL;
              }       
      
              $alignersDataCreatedDate      = date('d M. Y',$alignersSendReceiveCreated);
              $alignersDataCreatedTime      = date('h:i A',$alignersSendReceiveCreated);
              $alignersDataCreatedTimeAgo     = timeAgo($alignersSendReceiveCreated).' ago';
       ?>
             
                        
                       <tr <?php if ($counter == 1) { ?> style="background-color:#cbe091; color:#000;" <?php } ?>>
                        <td <?php if ($counter == 1) { ?> style="color:#000;" <?php } ?>><small style="font-size:14px;"><?php echo $counter; ?></small></td>
                        <td><small style="font-size:14px;"><?php echo $trackingNumberSending; ?></small></td>
                        <td><?php echo $deliveryServiceSendName; ?></small></td>
                        <td><small style="font-size:14px;"><span data-toggle="tooltip" title="" data-original-title="<?php echo $alignersDataCreatorAssignJobs; ?>"><?php echo $alignersDataCreatedName; ?> (<?php echo $alignersDataCreatedCode; ?>)</span></small></td>
                        <td><small style="font-size:14px;"><span data-toggle="tooltip" title="" data-original-title="<?php echo $alignersDataCreatedTimeAgo; ?>"><?php echo $alignersDataCreatedDate; ?> <?php echo $alignersDataCreatedTime; ?></span></small></td>
                      </tr>
             
             <?php
          $counter++;
          
            }
          
        } else {
      ?>
               
               <tr>
                  <td colspan="5">No history found.</td>
               </tr>
            
           <?php } ?>
           
           </tbody>
         </table>
          
        </div>
      </div>
<?php 
} 
?>



<?php  
  if (in_array(DOCTOR_CASE_TIMELINE_PAGE,$accessModules)) { ?>

 <div class="row">
       <div class="col-12 table-responsive">
          <br />
         <p class="lead" style="color:#000;"><b>Dentist Case History</b></p>
         <table class="table table-striped" style="width:100%">
           <thead>
            <tr>
              <th width="4%">#</th>
              <th width="20%">Link</th>
              <th width="12%">Code</th>

              <?php
                if (!in_array(ROLE_DOCTOR,$assignedRoles))
        {
          ?><th width="22%">By Employee</th><?php
        } 
              ?>
              

              <th width="15%">Status</th>
              <th width="15%">Comment</th>
              <th width="17%">&nbsp;</th>
             </tr>
           </thead>
           
           <tbody>
           
            <?php 
          
        if($doctorCaseHistory->num_rows() > 0) {
            
            $counter = 1;
            foreach($doctorCaseHistory->result() as $DcHistory) {
              
              /*$tableID              = $DcHistory->tableID;*/
              $caseLink     = $DcHistory->caseLink;
              $caseLink     = str_replace('https://','',$caseLink);
              $caseLink     = 'https://'.$caseLink;
              $caseLink     = trim($caseLink);
              $caseLink     = str_replace(' ','',$caseLink);

              $caseCode   = $DcHistory->caseCode;
              $caseStatusID   = $DcHistory->caseStatusID;
              $caseComment    = $DcHistory->caseComment;
              $doctorCaseStatusNames    = $DcHistory->doctorCaseStatusName;
              if($doctorCaseStatusNames=='Pending')
              {
                $doctorCaseStatusName='Assign';
              }
              else
              {
                $doctorCaseStatusName=$doctorCaseStatusNames;
              }

              if(isset($caseComment))
              {
                $caseHistoryComment=$caseComment;
              }
              else
              {
                $caseHistoryComment='-----';
              }

              $doctorCaseHistoryCreated     = $DcHistory->doctorCaseHistoryCreated;
              $doctorCaseHistoryCreatedBy   = $DcHistory->doctorCaseHistoryCreatedBy;
              $CreatedName        = $DcHistory->employeeName;
              $CreatedCode          = $DcHistory->employeeCode;
              
              
              $caseHistoryCreatorAssignJobs   = getEmployeeAssignJobs($doctorCaseHistoryCreatedBy); // Calling From HR Employees Helper
                    
              if ($caseHistoryCreatorAssignJobs) {
            
                  $caseHistoryCreatorAssignJobs   = $caseHistoryCreatorAssignJobs->row_array();
                  $caseHistoryCreatorAssignJobs = $caseHistoryCreatorAssignJobs['jobPositionName'];
        
              } else {
          
                  $caseHistoryCreatorAssignJobs   = NULL;
              }       
      
              $doctorCaseHistoryCreatedDate     = date('d M. Y',$doctorCaseHistoryCreated);
              $doctorCaseHistoryCreatedTime     = date('h:i A',$doctorCaseHistoryCreated);
              $doctorCaseHistoryCreatedTimeAgo    = timeAgo($doctorCaseHistoryCreated).' ago';
       ?>
                        
                       <tr <?php if ($counter == 1) { ?> style="background-color:#cbe091; color:#000;" <?php } ?>>
                        <td <?php if ($counter == 1) { ?> style="color:#000;" <?php } ?>><small style="font-size:13px;"><?php echo $counter; ?></small></td>

                        <td>
                          <a href="<?php echo $caseLink; ?>">
                            <small style="font-size:10px;"><?php echo $caseLink; ?></small>
                            </a>
                        </td>

                        <td><?php echo $caseCode; ?></small></td>

                        <!-- <td><small style="font-size:14px;">Case <?= $doctorCaseStatusName ?> by <?php echo $CreatedName; ?> (<?php echo $CreatedCode; ?>)</small>
                        </td> -->

                        <?php
                      if (!in_array(ROLE_DOCTOR,$assignedRoles))
              {
                ?>
                 <td><small style="font-size:13px;"><?php echo $CreatedName; ?> (<?php echo $CreatedCode; ?>)</small>
                            </td>
                            <?php
              } 
                  ?>


                        <td><?php echo $doctorCaseStatusName; ?></td>
                        <td><?php echo $caseHistoryComment; ?></td>



                        <!-- <td><small style="font-size:14px;"><span data-toggle="tooltip" title="" data-original-title="<?php echo $kitDataCreatorAssignJobs; ?>"><?php echo $CreatedName; ?> (<?php echo $CreatedCode; ?>)</span></small></td> -->

                        <td><small style="font-size:13px;"><span data-toggle="tooltip" title="" data-original-title="<?php echo $doctorCaseHistoryCreatedTimeAgo; ?>"><?php echo $doctorCaseHistoryCreatedDate; ?> <?php echo $doctorCaseHistoryCreatedTime; ?></span></small></td>
                      </tr>



             
             <?php
          $counter++;
          
            }
          
        } else {
      ?>
              
               <tr>
                  <td colspan="7">No history found.</td>
               </tr>
            
           <?php } ?>
           
           </tbody>
         </table>
          
        </div>
        <!-- /.col -->
      </div>

  <?php 
   } 
  ?>




  <?php  
  if ((in_array(ASSIGN_CASE_TO_DOCTOR,$accessModules)) && ($presentProductionStageID  == WAITING_FOR_APPROVAL)) { ?>

      <div class="box box-default" id="waiting-for-approval-frm-area">
        <div class="box-header with-border">
          <h3 class="box-title">Assign Case To Doctor </h3>
        </div>

      
      <form id="frm-case-assign-to-doctor" name="frm" action="<?php echo base_url(); ?>case-assign-to-doctor/" method="post" enctype="multipart/form-data" autocomplete="off">
        <div id='waiting_checkboxes'></div>
        <div class="box-body">
          <div class="row">

            <input type="hidden" name="list_case[]" value="<?= encodeString($caseID); ?>">

        
              <div class="col-md-6 col-6">
                <div class="form-group">
                  <label>Link</label>
                  <input name="link" id="link" value="<?php if(isset($case_data)){ echo $case_data->caseLink; } ?>" class="form-control" type="text" value="<?php echo $this->input->post('link'); ?>" id="example-text-input" placeholder="Enter Link Here" required>
                <?php if (form_error('link')) { echo form_error('link'); } ?>
                </div>
              </div>


               <div class="col-md-2 col-6">
                <div class="form-group">
                  <label>Code</label>
                  <input name="code" id="code" value="<?php if(isset($case_data)){ echo $case_data->caseCode; } ?>"  class="form-control" type="text" value="<?php echo $this->input->post('code'); ?>" id="example-text-input" placeholder="Enter Code Here" required>
                  <?php if (form_error('code')) { echo form_error('code'); } ?>
                </div>
              </div>
         
            


            <div class="col-md-2 col-6">
              <div class="form-group">
                <label>Doctor</label>
                <select name="doctor" id="doctor" class="form-control select2" style="width: 100%;" required>
                      <option value="">Select Doctor</option>
                          <?php 
                                if ($employees->num_rows() > 0) 
                                {
                                        
                                    foreach($employees->result() as $doctor) 
                                    {
                                      
                                        $doctorID  = $doctor->employeeID;
                                        $doctorName  = $doctor->employeeName;
                                        $company  = $doctor->company;
                                          if($company==$distributor)
                                          {

                                              if ($this->input->post('doctor') == $doctorID) {
                                              
                                                  $selectedService = 'selected = selected'; 
                                              
                                              } 

                                              elseif ($case_data->doctorID == $doctorID) {
                                              
                                                  $selectedService = 'selected = selected'; 
                                              
                                              } 

                                              else {
                                                  
                                                  $selectedService = NULL;    
                                              }
                                                
                                                echo '<option value="'.$doctorID.'" '.$selectedService.'>'.$doctorName.'</option>';

                                          }
                                        
                                      } 
                                }
                           ?>                 
                      
                   </select>
              </div>
            </div>



            <div class="col-md-2 col-6">
              <div class="form-group">
                <label>State</label>
                    <select name="state" id="state" class="form-control select2" style="width: 100%;">
                                       
                      
                   </select>
              </div>
            </div>

            



          </div>



          <div class="row" style="margin-top: 30px;">

            <div class="col-md-9 col-6" id="stat">
              
            </div>


            <div class="col-3">
                
            <div class="form-group row">
              <div class="col-sm-3">

                      <div class="demo-checkbox">
                         <input id="md_checkbox_1" name="doctor_approve_status" value="<?php echo APPROVED; ?>" class="chk-col-black" type="checkbox" <?php if(isset($case_data) and $case_data->caseStatusID== APPROVED) { echo 'checked="checked" '; } ?>>
                         <label for="md_checkbox_1">Approved By Doctor</label>
                            
                          <input id="md_checkbox_2" name="patient_approve_status" value="<?php  echo $caseID;  ?>" class="chk-col-black" type="checkbox" <?php if(isset($case_data) and $case_data->patientStatusID== APPROVED) { echo 'checked="checked" '; } ?>>
                          <label for="md_checkbox_2">Approved By Patient</label>
                      </div>                 
                   <?php if (form_error('arch')) { echo form_error('arch'); } ?>
                  </div>
               </div>
                
        
            </div>
          </div>



      
           <div class="pull-right" id="assign_div" style="display: none;">
              <button type="submit" id="save-btn-area" type="button" onClick="retun abc();" class="btn btn-blue pull-right"><i class="fa fa-send"></i> Assign</button>
           </div> 
           
        </div>
      </form>   
  </div><?php
}
?>





<!-- <?php  
  if ((in_array(ASSIGN_CASE_TO_DOCTOR,$accessModules)) && ($presentProductionStageID  == WAITING_FOR_APPROVAL)) { ?>

      <div class="box box-default" id="waiting-for-approval-frm-area">
        <div class="box-header with-border">
          <h3 class="box-title">Assign Case To Doctor </h3>
        </div>

      
      <form id="frm-case-assign-to-doctor" name="frm" action="<?php echo base_url(); ?>case-assign-to-doctor/" method="post" enctype="multipart/form-data" autocomplete="off">
        <div id='waiting_checkboxes'></div>
        <div class="box-body">
          <div class="row">

            <input type="hidden" name="list_case[]" value="<?= encodeString($caseID); ?>">

        <?php
          if($caseShipmentCount == 0)
          {
              ?><div class="col-md-6 col-6">
                <div class="form-group">
                  <label>Link</label>
                  <input name="link" id="link" value="<?php if(isset($case_data)){ echo $case_data->caseLink; } ?>" class="form-control" type="text" value="<?php echo $this->input->post('link'); ?>" id="example-text-input" placeholder="Enter Link Here" required>
                <?php if (form_error('link')) { echo form_error('link'); } ?>
                </div>
              </div>


               <div class="col-md-2 col-6">
                <div class="form-group">
                  <label>Code</label>
                  <input name="code" id="code" value="<?php if(isset($case_data)){ echo $case_data->caseCode; } ?>"  class="form-control" type="text" value="<?php echo $this->input->post('code'); ?>" id="example-text-input" placeholder="Enter Code Here" required>
                  <?php if (form_error('code')) { echo form_error('code'); } ?>
                </div>
              </div><?php 
          }

          else
          {
              ?><div class="col-md-6 col-6">
              <div class="form-group">
                <label>Link</label>
                <select name="link" id="link" class="form-control select2" style="width: 100%;" required>
                      <option value="">Select Link</option>
                          <?php 
                                if ($caseLinks->num_rows() > 0) 
                                {
                                        
                                    foreach($caseLinks->result() as $link) 
                                    {
                                      
                                        $caseLinksID  = $link->upload_link;
                                        $upload_link  = $link->upload_link;
                                          

                                        if ($this->input->post('link') == $caseLinksID) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } 

                                        elseif ($case_data->caseLink == $caseLinksID) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } 

                                        else {
                                            
                                            $selectedService = NULL;    
                                        }
                                          
                                          echo '<option value="'.$caseLinksID.'" '.$selectedService.'>'.$upload_link.'</option>';

                                          
                                        
                                      } 
                                }
                           ?>                 
                      
                   </select>
              </div>
            </div>


           



              <div class="col-md-2 col-6">
              <div class="form-group">
                <label>Code</label>
                <select name="code" id="code" class="form-control select2" style="width: 100%;" required>
                      <option value="">Select Code</option>
                          <?php 
                                if ($caseLinks->num_rows() > 0) 
                                {
                                        
                                    foreach($caseLinks->result() as $link) 
                                    {
                                      
                                        $casePassword = $link->password;
                                        $password     = $link->password;
                                          

                                        if ($this->input->post('code') == $casePassword) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } 

                                        elseif ($case_data->caseCode == $casePassword) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } 

                                        else {
                                            
                                            $selectedService = NULL;    
                                        }
                                          
                                          echo '<option value="'.$casePassword.'" '.$selectedService.'>'.$password.'</option>';

                                          
                                        
                                      } 
                                }
                           ?>                 
                      
                   </select>
              </div>
            </div><?php
          } 
        ?>

            


            <div class="col-md-2 col-6">
              <div class="form-group">
                <label>Doctor</label>
                <select name="doctor" id="doctor" class="form-control select2" style="width: 100%;" required>
                      <option value="">Select Doctor</option>
                          <?php 
                                if ($employees->num_rows() > 0) 
                                {
                                        
                                    foreach($employees->result() as $doctor) 
                                    {
                                      
                                        $doctorID  = $doctor->employeeID;
                                        $doctorName  = $doctor->employeeName;
                                        $company  = $doctor->company;
                                          if($company==$distributor)
                                          {

                                              if ($this->input->post('doctor') == $doctorID) {
                                              
                                                  $selectedService = 'selected = selected'; 
                                              
                                              } 

                                              elseif ($case_data->doctorID == $doctorID) {
                                              
                                                  $selectedService = 'selected = selected'; 
                                              
                                              } 

                                              else {
                                                  
                                                  $selectedService = NULL;    
                                              }
                                                
                                                echo '<option value="'.$doctorID.'" '.$selectedService.'>'.$doctorName.'</option>';

                                          }
                                        
                                      } 
                                }
                           ?>                 
                      
                   </select>
              </div>
            </div>



            <div class="col-md-2 col-6">
              <div class="form-group">
                <label>State</label>
                    <select name="state" id="state" class="form-control select2" style="width: 100%;">
                                       
                      
                   </select>
              </div>
            </div>

            



          </div>



          <div class="row" style="margin-top: 30px;">

            <div class="col-md-9 col-6" id="stat">
              
            </div>


            <div class="col-3">
                
            <div class="form-group row">
              <div class="col-sm-3">

                      <div class="demo-checkbox">
                         <input id="md_checkbox_1" name="doctor_approve_status" value="<?php echo APPROVED; ?>" class="chk-col-black" type="checkbox" <?php if(isset($case_data) and $case_data->caseStatusID== APPROVED) { echo 'checked="checked" '; } ?>>
                         <label for="md_checkbox_1">Approved By Doctor</label>
                            
                          <input id="md_checkbox_2" name="patient_approve_status" value="<?php  echo $caseID;  ?>" class="chk-col-black" type="checkbox" <?php if(isset($case_data) and $case_data->patientStatusID== APPROVED) { echo 'checked="checked" '; } ?>>
                          <label for="md_checkbox_2">Approved By Patient</label>
                      </div>                 
                   <?php if (form_error('arch')) { echo form_error('arch'); } ?>
                  </div>
               </div>
                
        
            </div>
          </div>



      
           <div class="pull-right" id="assign_div" style="display: none;">
              <button type="submit" id="save-btn-area" type="button" onClick="retun abc();" class="btn btn-blue pull-right"><i class="fa fa-send"></i> Assign</button>
           </div> 
           
        </div>
      </form>   
  </div><?php
}
?>
 -->





<?php  
  if ((in_array(DOCTOR_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID  == WAITING_FOR_APPROVAL)) { ?>

      <div class="box box-default" id="waiting-for-approval-frm-area">
        <div class="box-header with-border">
          <h3 class="box-title">Case Status </h3>
        </div>
        <!-- /.box-header -->
      
      <form id="frm-case-assign-to-doctor" name="frm" action="<?php echo base_url(); ?>change-doctor-case-status/" method="post" enctype="multipart/form-data" autocomplete="off">
        <div id='waiting_checkboxes'></div>
        <div class="box-body">
          <div class="row">

            <input type="hidden" name="list_case[]" value="<?= encodeString($caseID); ?>">
            <input type="hidden" name="link" value="<?= $case_data->caseLink; ?>">
            <input type="hidden" name="code" value="<?= $case_data->caseCode; ?>">


            <div class="col-md-6 col-6">
              <div class="form-group">
                <label>Case Status</label>
                <select name="caseStatusID" id="caseStatusID" class="form-control select2" style="width: 100%;">
                      <option value="">Select Status</option>
                          <?php 
                                if ($caseStatus->num_rows() > 0) 
                                {
                                        
                                    foreach($caseStatus->result() as $status) 
                                    {
                                      
                                      $caseStatusID  = $status->id;
                                      $caseName  = $status->name;
                                      if($caseStatusID !=ASSIGNED)
                                      {
                                        
                                        if ($this->input->post('doctor') == $caseStatusID) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } 

                                        elseif ($case_data->caseStatusID == $caseStatusID) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } 

                                        else {
                                            
                                            $selectedService = NULL;    
                                        }
                                          
                                          echo '<option value="'.$caseStatusID.'" '.$selectedService.'>'.$caseName.'</option>';
                                      } 
                                  }
                                }
                           ?>                 
                      
                   </select>
              </div>
            </div>

            


            <!-- <div class="col-md-4 col-6" id="comment" style="display: none;">
              <div class="form-group">
                <label>Comment</label>
                  <textarea id="comment" name="comment" rows="9" cols="60">
                   <?php if(isset($case_data)){ echo trim($case_data->caseComment); } ?>   
                  </textarea>
              </div>
            </div>  -->

            <div class="col-md-6 col-6" id="comment" style="display: none;">
              <div class="form-group">
                <label>Comment</label>
                   <!-- <div id="summernote" style="display: none;"><p><?php if(isset($case_data)){ echo trim($case_data->caseComment); } ?></p></div> -->

                   <textarea id="summernote" name="comment"  style="display: none;">
                   <?php if(isset($case_data)){ echo trim($case_data->caseComment); } ?>   
                  </textarea>

              </div>
            </div> 


          </div>
      
           <div class="pull-right">
              <button type="submit" id="save-btn-area" type="button" onClick="retun abc();" class="btn btn-blue pull-right"><i class="fa fa-send"></i>Update Status</button>
           </div> 
           
        </div>
      </form>   
  </div><?php
}
?>





      
     <?php if ($case['RX_form'] || $case['x_rays_opg'] || $case['x_rays_ceph'] || $case['file_assessment']) { ?>
      
      <div class="row">
        <div class="col-12 table-responsive">
          <br />
          <p class="lead" style="color:#000;"><b>Attachments</b></p>
          <table class="table" style="width:100%; margin-bottom:0px;">
            
            <tr>
              <?php if ($case['RX_form']) { ?> 
               <td><a href="<?php echo CASE_RX_FORM_URL_PATH.$case['RX_form']; ?>" download data-toggle="tooltip" title="" data-original-title="Download"><i class="fa fa-cloud-download" aria-hidden="true"></i></a> <a href="<?php echo CASE_RX_FORM_URL_PATH.$case['RX_form']; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="RX Form" style="color:#001A35"> RX Form</a></td>
              <?php } ?>
             
              <?php if ($case['x_rays_opg']) { ?> 
               <td><a href="<?php echo CASE_X_RAY_OPG_URL_PATH.'/'.$case['x_rays_opg']; ?>" download data-toggle="tooltip" title="" data-original-title="Download"><i class="fa fa-cloud-download" aria-hidden="true"></i></a> <a href="<?php echo CASE_X_RAY_OPG_URL_PATH.'/'.$case['x_rays_opg']; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="X-rays (OPG)" style="color:#001A35">X-ray OPG</a></td>
        <?php } ?>
             
              <?php if ($case['x_rays_ceph']) { ?> 
                <td><a href="<?php echo CASE_X_RAY_CEPH_URL_PATH.'/'.$case['x_rays_ceph']; ?>" download data-toggle="tooltip" title="" data-original-title="Download"><i class="fa fa-cloud-download" aria-hidden="true"></i></a> <a href="<?php echo CASE_X_RAY_CEPH_URL_PATH.'/'.$case['x_rays_ceph']; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="X-rays (Ceph)" style="color:#001A35">X-ray CEPH</a></td>
              <?php } ?>
              
              <?php if ($case['file_assessment']) { ?> 
                <td><a href="<?php echo CASE_FILE_ASSESSMENT_URL_PATH; ?><?php echo $case['file_assessment']; ?>" style="color:#001A35"><i class="fa fa-cloud-download" aria-hidden="true"></i> Assessment File</a></td>
              <?php } ?>
            </tr>
            <tr>
              <td colspan="6">&nbsp;</td>
            </tr>
           
            </tbody>
          </table>
          
        </div>
        <!-- /.col -->
      </div>
      
      <?php } ?>

      
      <!-- common heading for both photos types -->
      <?php 
      if ($patientCasePhotos OR $casePhotos->num_rows() > 0) 
        {
          ?><p class="lead"><b>Intra & Extra Oral Pictures:</b></p><?php
        } 
      ?>
      <!-- common heading for both photos types -->

      <!-- patient cases photos -->
    <?php if ($patientCasePhotos) { ?>
      
      <div class="row">
     <div class="col-12">
         <!-- fancybox -->
     <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend_assets/vendor_components/lightbox-master/dist/ekko-lightbox.css" />
         
         <!-- <p class="lead"><b>Intra & Extra Oral Pictures:</b></p> -->
         
                <?php if ($patientCasePhotos->num_rows() > 0) {  
                            
                                     foreach($patientCasePhotos->result() as $patientPhotos) 
                                      {
                                        $public_url                     = $patientPhotos->picturePublicURL;
                         ?>
                          
                                        <!-- <a href="<?php echo CASE_PICTURES_URL_PATH ?><?php echo $photo->name; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo $case['patientName']; ?>"><img style="padding-bottom:10px;" data-toggle="tooltip" title="" data-original-title="Intra & Extra Oral Pictures" src="<?php echo CASE_PICTURES_URL_PATH ?>thumbnail/thumbnail_<?php echo $photo->name; ?>" alt="gallery" ></a> -->

                                        <?php
                                        if($pic_type=='pdf')
                                        {
                                          ?><a href="<?= $public_url ?>" download="file">
                                        <img src="https://cdn3.iconfinder.com/data/icons/brands-applications/512/File-512.png" alt="file" style="height: 100px;" width="104" height="142">
                                      </a><?php
                                        } 
                                        else
                                        {
                                          ?><a href="<?= $public_url ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="image_name"><img  style="padding-top:10px; padding-left:5px height: 100px; width: 100px;" src="<?= $public_url ?>" alt="gallery" ></a><?php
                                        }
                                      ?>


          
                         <?php
                                     } 
                                 } 
                         ?>
        </div>
      </div>
  
    <?php } ?>
    <!-- patient cases photos -->

<br><br>


      <!--cases photos -->
      <?php if ($casePhotos->num_rows() > 0) { ?>
      
      <div class="row">
     <div class="col-12">
         <!-- fancybox -->
     <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend_assets/vendor_components/lightbox-master/dist/ekko-lightbox.css" />
         
         <!-- <p class="lead"><b>Intra & Extra Oral Pictures:</b></p> -->
         
                <?php if ($casePhotos->num_rows() > 0) {  
                            
                                     foreach($casePhotos->result() as $photo) { 
                                     $pic_type=$photo->type;
                         ?>
                          
                                        <!-- <a href="<?php echo CASE_PICTURES_URL_PATH ?><?php echo $photo->name; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo $case['patientName']; ?>"><img style="padding-bottom:10px;" data-toggle="tooltip" title="" data-original-title="Intra & Extra Oral Pictures" src="<?php echo CASE_PICTURES_URL_PATH ?>thumbnail/thumbnail_<?php echo $photo->name; ?>" alt="gallery" ></a> -->

                                        <?php
                          if($pic_type=='pdf')
                          {
                            ?><a href="<?= base_url() ?>pictures_cases/<?php echo $photo->name; ?>" download="file">
                          <img src="https://cdn3.iconfinder.com/data/icons/brands-applications/512/File-512.png" alt="file" style="height: 100px;" width="104" height="142">
                        </a><?php
                          } 
                          else
                          {
                            ?><a href="<?php echo CASE_PICTURES_URL_PATH ?><?php echo $photo->name; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo $row['patient']; ?>"><img  style="padding-top:10px; padding-left:5px;" src="<?php echo CASE_PICTURES_URL_PATH ?>thumbnail/thumbnail_<?php echo $photo->name; ?>" alt="gallery" ></a><?php
                          }
                        ?>


          
                         <?php
                                     } 
                                 } 
                         ?>
        </div>
      </div>
  
    <?php } ?>
     <!--cases photos -->

    
    
      <div class="row">
    
        <div class="col-12 col-sm-6">
     
         <?php if ($caseStausComments->num_rows() >0) { ?>
            
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px; text-align:justify;">
            
            <?php if ($caseStausComments->num_rows() > 0) {
            
            $caseStausComments = $caseStausComments->row_array();
            
            $caseComments        =  $caseStausComments['caseStatusComments'];
            $commentsCreatedBy     =  $caseStausComments['caseStatusCreatedBy'];
            $commentsCreator         =  $caseStausComments['employeeName'];
            $commentsCreatorCode     =  $caseStausComments['employeeCode'];
            
            
            
            $commentsCreated      = $caseStausComments['caseStatusCreated'];
            
            $commentsCreatorAssignJobs        = getEmployeeAssignJobs($commentsCreatedBy); // Calling From HR Employees Helper
                    
              if ($commentsCreatorAssignJobs) {
            
                  $commentsCreatorAssignJobs    = $commentsCreatorAssignJobs->row_array();
                  $commentsCreatorAssignJobs    = $commentsCreatorAssignJobs['jobPositionName'];
        
              } else {
          
                  $commentsCreatorAssignJobs  = NULL;
              }       
            
            $caseCommentsCreatedDate      = date('d F. Y',$commentsCreated);
            $caseCommentsCreatedTime      = date('h:i A',$commentsCreated);
            $caseCommentsCreatedTimeAgo     = timeAgo($commentsCreated).' ago';
          }
       ?>
             
             <i class="fa fa-comments"></i>"<?php echo $caseComments; ?>"<br /> <br />
             <small>
             <?php echo $commentsCreator; ?> (<?php echo $commentsCreatorCode; ?>) <br>
             <?php echo $commentsCreatorAssignJobs; ?><br />
             <?php echo $caseCommentsCreatedDate; ?> <?php echo $caseCommentsCreatedTime; ?>
             </small>
             
          
          </p>
      
        <?php } ?>
         
        </div>

      </div>
      
      <!--ALIGNERS SENT TO CUSTOMER BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == ALIGNERS_TO_CUSTOMER_SEND_RECEIVE && $presentStatusID == CASE_STATUS_ALIGNERS_TO_CUSTOMER_READY)) { ?>
            
                    <form id="frm-aligners-send-to-customer" action="<?php echo base_url(); ?>aligners-send-to-customer/<?php echo encodeString($caseID); ?>" method="post" autocomplete="off">
             <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                     <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                     <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                     <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                     <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_ALIGNERS_TO_CUSTOMER_IN_TRANSIT; ?>">
                    
       <div class="row">
          
          <div class="col-12 col-sm-6">
              
              <p class="lead"><b>Aligners Send to Customer:</b></p>
  
                  <div class="form-group">
                    <input type="text" class="form-control" name="tracking_number_send_customer" id="tracking_number_send_customer" value="<?php echo $this->input->post('tracking_number_send_customer'); ?>" placeholder="Tracking Number" />
                    <?php if (form_error('tracking_number_send_customer')) { echo form_error('tracking_number_send_customer'); } ?>    
                  </div>
                  
                  <div class="form-group">
                   <select name="delivery_service_send_customer" id="delivery_service_send_customer" class="form-control select2" style="width: 100%;">
                      <option value="">Courier Service</option>
            <?php 
                              if ($deliveryServices->num_rows() > 0) {
                                      
                                      foreach($deliveryServices->result() as $deliveryService) {
                                          
                                                  $deliveryServiceID        = $deliveryService->id;
                                                  $deliveryServiceName        = $deliveryService->name;
                                                      
                                                      if ($this->input->post('delivery_service_send_customer') == $deliveryServiceID) {
                                                      
                                                              $selectedService = 'selected = selected'; 
                                                      
                                                      } else {
                                                              
                                                              $selectedService = NULL;    
                                                      }
                                                  
                                                  echo '<option value="'.$deliveryServiceID.'" '.$selectedService.'>'.$deliveryServiceName.'</option>';
                                      } 
                              }
                       ?>                 
                      
                   </select>
                   <?php if (form_error('delivery_service_send_customer')) { echo form_error('delivery_service_send_customer'); } ?>      
                  </div>
                  
                  <div class="form-group">
                  
                    <div class="box-footer clearfix pull-right" style="padding:0px;">
                     <button type="button"  class="btn btn-blue" id="aligners-send-to-customer"><i class="glyphicon glyphicon-send"></i> Send</button>
                    </div> 
                          
                </div>
               
          </div>
         
       </div>
     
     </form>
                       
     <?php } ?>
    <!--ALIGNERS SENT TO CUSTOMER BUTTON END-->
   
   <div class="row no-print" style="margin-top:10px;">
      <div class="col-12">

         <?php  
         if (in_array(CASE_MAIN_TIMELINE_PAGE,$accessModules)) { ?>
          
          <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
           <!-- Eidt Case Button -->
           <button type="button" class="btn btn-primary pull-left" style="margin-right: 5px;" onClick="window.location.href='<?php echo base_url(); ?>edit-case/<?php echo $encodeCaseID; ?>'" />
             <i class="fa fa fa-edit"></i> Edit Case
           </button>
           
          <?php } ?>
          
           <!-- Pictures Case Button -->
           <button type="button" class="btn btn-warning pull-left" style="margin-right: 5px;" onClick="window.location.href='<?php echo base_url(); ?>manage-case-pictures/<?php echo $encodeCaseID; ?>'" />
             <i class="fa fa fa-camera"></i> Photos
           </button>

           <?php
           } 
            ?>
        
          <!--BUTTONS AREA 
              
                - CASE ACTIVITY
           -->
      
         <!--BUTTON ACTIVITY CASE START-->
         <?php 
          
        if (in_array(MODULE_CASE_TIMELINE,$accessModules) || in_array(MODULE_CASE_TIMELINE_READ_ONLY,$accessModules)) {
        
          if (((in_array(ROLE_SUPER_ADMINISTRATOR,$assignedRoles) || in_array(ROLE_MANAGER,$assignedRoles)) || (in_array($presentStatusID ,$caseStatusProcessArray) && ($presentCaseOperatorProcessingDone == HARD_CODE_ID_NO) && $presentCaseOperatorEmployeeID == $teamID))) {
            
              $timelineLink = base_url().'case-timeline/'.$encodeCaseID;
                    
           } else {
              
              $timelineLink = base_url().'case-timeline-read-out/'.$encodeCaseID;
           }
      ?>
          <button type="button" class="notification btn btn-success pull-right" style="margin-right:5px;" onClick="window.location.href='<?php echo $timelineLink; ?>'"><i class="fa fa-clock-o"></i> Case Activity</button> 
          
      <?php } ?>
        
          <!--BUTTON ACTIVITY CASE END-->   
        
         <!--CUSTOMER SEND KIT BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == KIT_SHIP_TO_CUSTOMER && $presentStatusID == CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY)) { ?>
            
                      <button type="button" class="btn bg-olive pull-right" id="" style="margin-right: 5px;" onClick="window.location.href='<?php echo base_url(); ?>kit-send-to-customer/<?php echo encodeString($caseID); ?>'"><i class="fa fa-check-square-o"></i> Kit Send to Customer</button>
         <?php } ?>
         <!--CUSTOMER SEND KIT BUTTON END-->
       
       
        <!--CUSTOMER RECEIVE KIT BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == KIT_TO_CUSTOMER_SEND_RECEIVE && $presentStatusID == CASE_STATUS_KIT_TO_CUSTOMER_IN_TRANSIT)) { ?>
            
                      <button type="button" class="btn bg-olive pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-check-square-o"></i> Kit Received to Customer</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_KIT_TO_CUSTOMER_RECEIVED; ?>">
                     </form>
                       
         <?php } ?>
         <!--CUSTOMER RECEIVE KIT BUTTON END-->
         
         <!--IMPRESSIONS IN TRANSIT FROM CUSTOMER BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == IMPRESSIONS_TO_NY_SEND_RECEIVE && $presentStatusID == CASE_STATUS_IMPRESSIONS_TO_NY_READY)) { ?>
            
                      <button type="button" class="btn bg-orange pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-plane"></i> Impressions In-Transit to NY</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_IMPRESSIONS_TO_NY_IN_TRANSIT; ?>">
                     </form>
                       
         <?php } ?>
         <!--IMPRESSIONS  IN TRANSIT FROM CUSTOMER END-->
         
         <!--IMPRESSIONS RECEIVED FROM CUSTOMER BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == IMPRESSIONS_TO_NY_SEND_RECEIVE && $presentStatusID == CASE_STATUS_IMPRESSIONS_TO_NY_IN_TRANSIT)) { ?>
            
                      <button type="button" class="btn bg-olive pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-check"></i> Impressions Received to NY</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_IMPRESSIONS_TO_NY_RECEIVED; ?>">
                     </form>
                       
         <?php } ?>
         <!--IMPRESSIONS RECEIVED FROM CUSTOMER END-->
        
         <!--IMPRESSIONS RECEIVED PRODUCTION BUTTON-->
     <?php 
    /* $caseID;
     $recordperpage     = 1;
     $page              = 0;
     $result            = $this->model_shared->page_listing_SecondDB_where($page,$recordperpage,'*',INVENTORY_ITEMS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_reference_number' => $caseID),'id','DESC');
     $row               = $result->row();
     $inventoryStatus   = $row->status;

     if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == IMPRESSIONS_TO_PRODUCTION_SEND_RECEIVE && $presentStatusID == CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT && $inventoryStatus==HARD_CODE_ID_CLEAR))*/


        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == IMPRESSIONS_TO_PRODUCTION_SEND_RECEIVE && $presentStatusID == CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT)) { ?>
            
                      <button type="button" class="btn bg-olive pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-check"></i> Impressions Received to Production</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_RECEIVED; ?>">
                     </form>
                       
         <?php } ?>
         <!--IMPRESSIONS RECEIVED PRODUCTION END-->
         
         <!--SETUP UPLOADED BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == READY_FOR_UPLOADING && $presentStatusID == CASE_STATUS_READY_FOR_UPLOADING_READY)) { ?>
            
                      <button type="button" class="btn bg-olive pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-check"></i> Setup Uploaded</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_READY_FOR_UPLOADING_DONE; ?>">
                     </form>
                       
         <?php } ?>
         <!--SETUP UPLOADED END-->
         
         <!--SETUP APPROVED BUTTON-->
         
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == WAITING_FOR_APPROVAL && $presentStatusID == CASE_STATUS_WAITING_FOR_APPROVAL_WAITING) ) { ?>
            
          <div id="approved_div" style="display: none;">  
                      <button type="button" class="btn bg-olive pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-check"></i> Approved</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_WAITING_FOR_APPROVAL_APPROVED; ?>">
                     </form>
                  </div>
                       
         <?php } ?>
         <!--SETUP APPROVED END-->
         
         <!--MANUFACTURING IN TRANSIT BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == PRODUCTION_TO_NY_SEND_RECEIVE && $presentStatusID == CASE_STATUS_PRODUCTION_TO_NY_READY)) { ?>
            
                      <button type="button" class="btn bg-olive pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-check"></i> In Transit To NY</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_PRODUCTION_TO_NY_IN_TRANSIT; ?>">
                     </form>
                       
         <?php } ?>
         <!--MANUFACTURING IN TRANSIT END-->
         
         <!--MANUFACTURING RECEIVED TO NY BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == PRODUCTION_TO_NY_SEND_RECEIVE && $presentStatusID == CASE_STATUS_PRODUCTION_TO_NY_IN_TRANSIT)) { ?>
            
                      <button type="button" class="btn bg-olive pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-check"></i> Aligners Received from Production</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_PRODUCTION_TO_NY_RECEIVED; ?>">
                     </form>
                       
         <?php } ?>
         <!--MANUFACTURING RECEIVED TO NY BUTTON-->
         
         <!--ALIGNERS IN TRANIST TO CUSTOMER BUTTON-->
     <?php  
        if ((in_array(MODULE_CASE_UPDATE_STATUS,$accessModules)) && ($presentProductionStageID == ALIGNERS_TO_CUSTOMER_SEND_RECEIVE && $presentStatusID == CASE_STATUS_ALIGNERS_TO_CUSTOMER_IN_TRANSIT)) { ?>
            
                      <button type="button" class="btn bg-olive pull-right update-status" id="" style="margin-right: 5px;"><i class="fa fa-check"></i> Aligners Received by Customer</button>
                   
                      <form id="form-update-status" class="form-horizontal" method="post" action="<?php echo base_url(); ?>change-case-status/">  
                                                    
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo $caseID; ?>">
                           <input type="hidden" name="production_stage_id" id="<?php echo $presentProductionStageID; ?>" value="<?php echo $presentProductionStageID; ?>">
                           <input type="hidden" name="case_stage_id" id="case_stage_id" value="<?php echo $presentCaseStageID; ?>">
                           <input type="hidden" name="case_status_id" id="case_status_id" value="<?php echo $presentCaseStatusID; ?>">
                           <input type="hidden" name="status_id" id="status_id" value="<?php echo CASE_STATUS_ALIGNERS_TO_CUSTOMER_RECEIVED; ?>">
                     </form>
                       
         <?php } ?>
         <!--ALIGNERS IN TRANIST TO CUSTOMER BUTTON-->
    
        </div>
      </div>
      
    </section>
    <!-- /.content -->
    
    <?php } ?>
   <script>
    $(document).ready(function() {
        $('#summernote').summernote({
      height: 150,   //set editable area's height
      codemirror: { // codemirror options
        theme: 'monokai'
      }
    });
        
    });
  </script>
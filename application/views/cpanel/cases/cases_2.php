<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>

<script language="javascript" type="text/javascript">
		
		  var currentRequest = null;    
		  
		  $(document).ready(function () {
				  
				  $('#selected-stage-'+<?php echo RECEIVE_ORDER_ENTRY; ?>).addClass('bg-yellow');
				  $("#search-status-all").prepend('<span class="badge bg-aqua search-status-tag">Checked</span>');
				  getData();
				 
				  $('#country').change(function() {
							
							var country = $(this).val();
							
							if (country == 168) {
									
									$('#city-area').show();	
							} else {
									
									$('#city').val(null).trigger('change');
									$('#city-area').hide();	
							}
							
							$('#case-type-area').hide();	
				  });
				  
				  $('#country').change(function() {
							
							var country 		= $(this).val();
							var selectedCompany = 0;
							
							<?php if ($this->input->post('company')) { ?>
							
								 selectedCompany = <?php echo $this->input->post('company'); ?>;
							
							<?php } ?>
							
								$.ajax({
									  type   	  : "POST",
									  dataType 	  : "HTML",
									  url		  : "<?php echo base_url();?>load-companies-by-country/",
									  data		  : {  
									  						country 			: country,
									  						selectedCompany 	: selectedCompany
													}
									
										 }).done(function( rowData ) {
									
									  			if (rowData) {
						  								
														$('#company-area').html(rowData);
												}
									  
								}); // END Ajax Request
							
				  });
				  
				  $(document).on( "click", ".search_production_stage", function() {
				 
  						var stageID 	= $(this).attr("key");
						var stageType	= $(this).attr("type");
						
						if (stageID == <?php echo RECEIVE_ORDER_ENTRY; ?>) {
								
								$('#search-status-modification').show();
								$('#search-status-waiting').show();
						
						} else {
								
								$('#search-status-modification').hide();
								$('#search-status-waiting').hide();
						}	
					
						if (stageType == 'parent') {
						
							$('.selected-stage').removeClass('bg-yellow');
						    $('#selected-stage-'+stageID).addClass('bg-yellow');
						}
						
						
						$('#search_stage').val(stageID);
						
						getChildStages(stageID,stageType);
						
						getData();
						
						 $('html, body').animate({
		 					   scrollTop: $("div.case-data").offset().top
  	  					 }, 1000)
				  });
				  
				  $(document).on( "click", ".find-status-btn", function() {
					  
					  caseStatus = $(this).attr('value');
					 
					  $(".search-status-tag").remove();
					  $("#search-status-"+caseStatus).prepend('<span class="badge bg-aqua search-status-tag">Checked</span>');
					 
					  $('#search_status').val(caseStatus);
					
					  getData();
					  
				  });
				  
				  $(document).on( "click", ".find-time-period-btn", function() {
					  
					  var timePeriod = $(this).attr('value');
					  
					  // Check already checked button or not 
					  var alreadyChecked = $('#search_time_period').val();
					  
					  if (timePeriod == alreadyChecked) {
						
							 $(".search-time-period-tag").remove();	
							 $('#search_time_period').val('');
							 
							  getData();
							  
							  return false;
					  } 
					 
					  $(".search-time-period-tag").remove();
					  $("#search-time-period-"+timePeriod).prepend('<span class="badge bg-aqua search-time-period-tag">Checked</span>');
					 
					  $('#search_time_period').val(timePeriod);
					
					  getData();
					  
				  });
				  
				  $(document).on( "click", ".ajax-pagination", function() {
				 
  						var pageNumber = $(this).attr('key');
						
						getData(pageNumber);
						 
			
				});
				
				  
				  $('#frm_filters_case_search_submit').click(function() {
			
					$('.searchCasesPopUp').modal('hide');
					getData();
				 });
				 
				  $('#reset_search_form').click(function() {
					
					$('#frm_search')[0].reset();
					
					$('#impression').val(null).trigger('change');
					$('#doctor').val(null).trigger('change');
					$('#nature_of_patient').val(null).trigger('change');
					$('#country').val(null).trigger('change');
					$('#city').val(null).trigger('change');
					$('#company').val(null).trigger('change');
					
					$('.searchCasesPopUp').modal('hide');
					getData();
				 });
				  
		  });
		  
		  function getChildStages(stageID,stageType) {
					
					if (stageType == 'parent') {
							  	
						$('#production-stage-child-level').val(0);
						$('.child-stage-data').remove();
					 }
					
			$.ajax({
			
					   method		: "POST",
					   url			: "<?php echo base_url(); ?>get-child-production-stages/",
					   data			: {
										stageID  : stageID,
									  },
  
					   beforeSend	: function(){
							
							
					  },
					  success: function(data){
						  
						   if(data) {
							
							  if (stageType == 'parent') {
								  
								   var childLevel  = parseInt($('#production-stage-child-level').val());
									   childLevel  = parseInt(childLevel) + 1;
									   
								   $('#production-stage-child-level').val(childLevel);
								   
								   $("#child-stages-area-"+childLevel).remove(); 
								  
								   $('#parent-stages-area').after('<div class="child-stage-data" id="child-stages-area-'+childLevel+'"></div>');
								   $('#child-stages-area-'+childLevel).html(data);
								  
							  } else if (stageType == 'child') {
								
									var childLevel  	= parseInt($('#production-stage-child-level').val());
								 	    newChildLevel 	= parseInt(childLevel) + 1;
									
									$('#production-stage-child-level').val(childLevel);
									
									$("#child-stages-area-"+newChildLevel).remove(); 
									$('#child-stages-area-'+childLevel).after('<div class="child-stage-data" id="child-stages-area-'+newChildLevel+'"></div>');
									
									$('#child-stages-area-'+newChildLevel).html(data);
							}
							
					  } else {
							 
							if (stageType == 'parent') {
								
							} else if (stageType == 'child') {
								
								var childLevel  	= parseInt($('#production-stage-child-level').val());
								 	preChildLevel 	= parseInt(childLevel) - 1;
									nxtChildLevel 	= parseInt(childLevel) + 1;
								 
								 $("#child-stages-area-"+nxtChildLevel).remove(); 
								 $("#child-stages-area-"+preChildLevel).remove(); 
							} 
						  }
					  }
			});
		 
		  }
		  
		  function getData(pageNumber=null) {
				
			if (pageNumber == null) {
				 
				 pageNumber = 0;
			}
 						
			currentRequest =  $.ajax({
			
					   method		: "POST",
					   url			: "<?php echo base_url(); ?>cases-data-with-pagination/"+pageNumber,
					   data			: {
										pageNumber						: pageNumber,
										case_id		    				: $("#case_id").val(),
										portal_number		   			: $("#portal_number").val(),
										patient_name		    		: $("#patient_name").val(),
										doctor		    				: $("#doctor option:selected").val(),
										impression		   		    	: $("#impression option:selected" ).val(),
										nature_of_patient		  		: $("#nature_of_patient option:selected").val(),
										country		   					: $("#country option:selected").val(),
										city		    				: $("#city option:selected").val(),
										company		    				: $("#company option:selected").val(),
										priority		   		  	    : $("#priority option:selected").val(),
										search_received_date	   		: $("#search_received_date").val(),
										search_received_date_to	   		: $("#search_received_date_to").val(),
										search_stage	    			: $("#search_stage").val(),
										search_status	    			: $("#search_status").val(),
										search_time_period	    		: $("#search_time_period").val(),
									 },
  
					   beforeSend	: function(){
							
							$('#pagination-result-data-area').html('<p style="margin-left:36%"><img src="<?php echo base_url(); ?>backend_images/loading.gif" /></p>');
					  		
							if(currentRequest != null) {
           						 currentRequest.abort();
       						}
							
					  },
					  success: function(data){
						  
						   $('#pagination-result-data-area').html(data);
						   $('[data-toggle="tooltip"]').tooltip();
					  }
			});
		  }
		  
		  function getCaseTypes(company) {
			
				return true;	
		  }
		  
		  function excelReportCases() {
			
			$('#frm_search').attr('action','<?php echo base_url(); ?>excel-report-cases/');
			$("#frm_search").submit();
			
			$('#frm_cases').attr('action','');
			
			return false;
  		}

</script>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-dashboard"></i> Home</li>
        <li class="breadcrumb-item">Cases</li>
    </ol>

    </section>

    <!-- Main content -->
    <section class="content">
     
     				 <?php
								if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ($this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='')  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
     
     
   <div class="modal fade bs-example-modal-lg searchCasesPopUp" role="dialog" aria-labelledby="searchCaseFilters" aria-hidden="true" style="display:none;">
    <div class="modal-dialog modal-lg">
       
       <form name="frm_search" id="frm_search" method="post" action="" autocomplete="off">
		<input type="hidden" name="search_stage" id="search_stage" value="<?php echo $this->input->post('search_stage'); ?>">
        <input type="hidden" name="search_status" id="search_status" value="<?php echo $this->input->post('search_status'); ?>">
        <input type="hidden" name="search_time_period" id="search_time_period" value="<?php echo $this->input->post('search_time_period'); ?>">
       
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="searchCaseFilters"><i class="fa fa-filter"></i> Search Filters</h4>
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
           
        <div class="modal-body">
                
                <div class="row">
                  <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label>Case ID</label>
                    <input type="text" name="case_id" id="case_id" class="form-control" value="<?php echo $this->input->post('case_id'); ?>" />
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Patient Name</label>
                    <input type="text" class="form-control" name="patient_name" id="patient_name" value="<?php echo $this->input->post('patient_name'); ?>" />
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Doctor</label>
                    <select class="form-control select2" name="doctor" id="doctor" style="width:100%;">
                    <option value="">Select a doctor</option>
                        <?php 
                                      if ($doctors->num_rows() > 0) {
                                              
                                              foreach($doctors->result() as $doctor) {
                                                  
                                                          $doctorID				=	$doctor->id;
                                                          $doctorName			=	$doctor->name;
                                                              
                                                              if ($this->input->post('doctor') == $doctorID) {
                                                              
                                                                      $selectedDoctor = 'selected = selected';	
                                                              
                                                              }	else {
                                                                      
                                                                      $selectedDoctor = NULL;		
                                                              }
                                                          
                                                          echo '<option value="'.$doctorID.'" '.$selectedDoctor.'>'.$doctorName.'</option>';
                                              }	
                                      }
                        ?>                 
                    </select>
                  </div>
                  
                  <div class="form-group">
                          <label>Country</label>
                          <select class="form-control select2" name="country" id="country" style="width:100%;">
                          <option value="">Select a country</option>
                             <?php 
                                    if ($countries->num_rows() > 0) {
                                                              
                                                              foreach($countries->result() as $country) {
                                                                  
                                                                          $countryID			=	$country->id;
                                                                          $countryName			=	$country->name;
                                                                              
                                                                              if ($this->input->post('country') == $countryID) {
                                                                              
                                                                                      $selectedCountry = 'selected = selected';	
                                                                              
                                                                              }	else {
                                                                                      
                                                                                      $selectedCountry = NULL;		
                                                                              }
                                                                          
                                                                          echo '<option value="'.$countryID.'" '.$selectedCountry.'>'.$countryName.'</option>';
                                                              }	
                                                      }
                             ?>
                          </select>
                 </div>
                  
                 
                  <div class="form-group">
                    <label>Received Date From</label>
                    <?php if ($this->input->post('search_received_date')) {
                        
                                  $searchReceivedDate = 	$this->input->post('search_received_date');	  
                          } else {
                                  $searchReceivedDate = NULL;	
                          }
                    ?>
                     <input class="form-control" name="search_received_date" id="search_received_date" type="date" value="<?php echo $searchReceivedDate; ?>">
                  </div>
                 
                  </div>

                  <div class="col-md-6 col-12">
                    <div class="form-group">
                      <label>Portal Number</label>
                      <input type="text" class="form-control" name="portal_number" id="portal_number" value="<?php echo $this->input->post('portal_number'); ?>" />
                    </div>
                   
                      <div class="form-group">
                        <label>Impression</label>
                        <select class="form-control select2" name="impression" id="impression" style="width:100%;">
                        <option value="">Select a impression type</option>
                            <?php 
                                          if ($impressionTypes->num_rows() > 0) {
                                                  
                                                  foreach($impressionTypes->result() as $impressionType) {
                                                      
                                                              $impressionTypeID				=	$impressionType->id;
                                                              $impressionTypeName			=	$impressionType->name;
                                                                  
                                                                  if ($this->input->post('impression') == $impressionTypeID) {
                                                                  
                                                                          $selectedImpression = 'selected = selected';	
                                                                  
                                                                  }	else {
                                                                          
                                                                          $selectedImpression = NULL;		
                                                                  }
                                                              
                                                              echo '<option value="'.$impressionTypeID.'" '.$selectedImpression.'>'.$impressionTypeName.'</option>';
                                                  }	
                                          }
                            ?>                 
                        </select>
                      </div>
                    
                      <div class="form-group">
                          <label>Nature Of Patient</label>
                          <select class="form-control select2" name="nature_of_patient" id="nature_of_patient" style="width:100%;">
                             <option value="">Select a patient nature</option>
                             <option value="Overseas" <?php if ($this->input->post('nature_of_patient') == 'Overseas') { echo 'selected=selected'; } ?>>Overseas</option>
                             <option value="Local" <?php if ($this->input->post('nature_of_patient') == 'Local') { echo 'selected=selected'; } ?>>Local</option>
                          </select>
                        </div>
                    
                      <div class="form-group">
                      <label>Distributor</label>
                       <div id="company-area">
                          <select class="form-control select2" name="company" id="company" style="width: 100%;">
                          <option value="">Select a company</option>
                             <?php 
                                    if ($companies->num_rows() > 0) {
                                                              
                                                              foreach($companies->result() as $company) {
                                                                  
                                                                          $companyID				=	$company->id;
                                                                          $companyName				=	$company->name;
                                                                              
                                                                              if ($this->input->post('company') == $companyID) {
                                                                              
                                                                                      $selectedCompany = 'selected = selected';	
                                                                              
                                                                              }	else {
                                                                                      
                                                                                      $selectedCompany = NULL;		
                                                                              }
                                                                          
                                                                          echo '<option value="'.$companyID.'" '.$selectedCompany.'>'.$companyName.'</option>';
                                                              }	
                                                      }
                             ?>
                          </select>
                     </div>
                    </div>
                    
                      <div class="form-group">
                        <label>Received Date To</label>
                        <?php if ($this->input->post('search_received_date_to')) {
                            
                                      $searchReceivedDateTo = 	$this->input->post('search_received_date_to');	  
                              } else {
                                      $searchReceivedDateTo = NULL;	
                              }
                        ?>
                         <input class="form-control" name="search_received_date_to" id="search_received_date_to" type="date" value="<?php echo $searchReceivedDate; ?>">
                     </div>
                   
                     </div>

				</div>
        </div>
        <div class="modal-footer">
             <button type="button" id="frm_filters_case_search_submit" class="btn btn-info text-left"><i class="glyphicon glyphicon-search"></i> Search</button>
             <button type="button" id="reset_search_form" class="btn btn-success waves-effect text-left">Reset</button>
             <button type="button" class="btn btn-warning waves-effect text-left" data-dismiss="modal">Cancel</button>
         </div>
           
        </div>
        </form>
        <!-- /.modal-content -->
     </div>
   </div>


   
     
   <div class="row" id="parent-stages-area"> 
      
      <input type="hidden" id="production-stage-child-level" value="0" />       
      
              <?php
                  
                  if ($productionStages->num_rows() > 0) {
                          
                      $colorCounter = 1;
                      foreach($productionStages->result() as $row) {
                          
                          $stageID		= $row->id;	
                          $stageName	= $row->name;	
                          $stageSefURL	= $row->sef_url;
                          
                          $totalCounter = $this->model_case->countProductionStageCases($stageID);
                         
                          if ($colorCounter == 1) {
                              
                                  $colorClass = 'bg-blue';	
                          
                          } elseif($colorCounter == 2) {
                                  
                                  $colorClass = 'bg-green';
                          
                          } elseif($colorCounter == 3) {
                                  
                                  $colorClass = 'bg-purple';	
                          
                          } elseif($colorCounter == 4) {
                                  
                                  $colorClass = 'bg-red';
                          
                          } elseif($colorCounter == 5) {
                                  
                                  $colorClass = 'bg-olive';
                          
                          } elseif($colorCounter == 6) {
                                  
                                  $colorClass = 'bg-orange';
                                  
                          } elseif($colorCounter == 7) {
                                  
                                  $colorClass = 'bg-navy';
                                  
                          } elseif($colorCounter == 8) {
                                  
                                  $colorClass = 'bg-maroon';
                          }
                          
                          if($colorCounter == 7) {
                                  
                                  $colorCounter = 0;
                          }
               ?>
          
                          <div class="col-12 col-md-6 col-lg-4">
                            <div class="info-box selected-stage" id="selected-stage-<?php echo $stageID; ?>">
                              <span class="info-box-icon <?php echo $colorClass; ?>"><i class="fa fa-medkit"></i></span>
                  
                              <div class="info-box-content">
                               <span class="info-box-number" style="font-size:16px;"><?php if ($totalCounter) { echo number_format($totalCounter); } ?></span>
                                <span class="info-box-text" style="margin-top:6px;"><a href="javascript:void(0)" class="search_production_stage" key="<?php echo $stageID; ?>" type="parent"><?php echo $stageName; ?></a></span>
                              </div>
                            </div>
                          </div>		
              <?php 
                  
				  	$colorCounter++;		
              
                      }	
                  }
          	  ?>
              
             <!--DDX Cases-->
			 <?php if (in_array(MODULE_DDX_CASE_ADD_NEW,$accessModules)) { ?>
             
             <?php /*?> <div class="col-12 col-md-6 col-lg-4">
                  <div class="info-box" id="">
                    <span class="info-box-icon bg-red" style="background-color:#009EE2"><i class="fa fa-medkit"></i></span>
        
                    <div class="info-box-content">
                     <span class="info-box-number" style="font-size:16px;">0</span>
                      <span class="info-box-text" style="margin-top:6px;"><a href="<?php echo base_url(); ?>ddx-cases/" class="" key="" type="">DDx STLs</a></span>
                    </div>
                  </div>
                </div>
                
              <div class="col-12 col-md-6 col-lg-4">
                  <div class="info-box" id="">
                    <span class="info-box-icon bg-red" style="background-color:#009EE2"><i class="fa fa-medkit"></i></span>
        
                    <div class="info-box-content">
                     <span class="info-box-number" style="font-size:16px;">0</span>
                      <span class="info-box-text" style="margin-top:6px;"><a href="<?php echo base_url(); ?>ddx-cases/" class="" key="" type="">DDx Physical Impressions</a></span>
                    </div>
                  </div>
                </div><?php */?>
                
             <?php } ?>
       
       </div>
   
   <div id="child-stages-area-1"></div>
     

     
<!--    <div class="row case-data">
      <div class="col-xl-12">
       
        <div class="box">
        
          <div class="box-header">
             <div class="pull-right box-tools">
             	
                <button type="button" class="btn btn-white btn-sm" onclick="return excelReportCases();">
                  <i class="fa fa-file-excel-o" data-toggle="tooltip" title="" data-original-title="Downlaod Excel"></i>
                </button>
                
                <a href="JavaScript:Void(0);" data-target=".searchCasesPopUp" data-toggle="modal"><i class="fa fa-filter" data-toggle="tooltip" title="" data-original-title="Filters"></i></a>
             </div>
             
              <h3 class="box-title"><i class='fa fa-tag'></i> Status</h3>
              
         </div>
          
          <div class="box-body">
          <div class="pull-left">
             <a id="search-status-all" href="javascript:void(0);" class="btn btn-app find-status-btn" value="all">
              <i class="fa fa fa-list-ol"></i> All
            </a>
            <a id="search-status-ready" href="javascript:void(0)" class="btn btn-app find-status-btn" value="ready">
              <i class="fa fa-check"></i> Ready
            </a>
            <a id="search-status-hold" href="javascript:void(0)" class="btn btn-app find-status-btn" value="hold">
              <i class="fa fa-pause"></i> Hold
            </a>
            <a id="search-status-process" href="javascript:void(0)" class="btn btn-app find-status-btn" value="process">
              <i class="fa fa-gears"></i> Process
            </a>
            <a id="search-status-modification" href="javascript:void(0)" class="btn btn-app find-status-btn" value="modification">
              <i class="fa fa-mail-reply"></i> Modification
            </a>
            <a id="search-status-waiting" href="javascript:void(0)" class="btn btn-app find-status-btn" value="waiting">
              <i class="fa fa-clock-o"></i> Waiting
            </a>
            
           
            
           </div>
           
           <div class="pull-right">
           	 
             <a id="search-time-period-before" href="javascript:void(0)" class="btn btn-app find-time-period-btn" value="before" style="background-color:#7F3B81; color:#FFF;">
              <i class="fa fa-clock-o"></i> Before
            </a>
             
             <a id="search-time-period-due" href="javascript:void(0)" class="btn bg-yellow btn-app find-time-period-btn" value="due">
              <i class="fa fa-clock-o"></i> Due
            </a>
             
             <a id="search-time-period-late-complete" href="javascript:void(0)" class="btn bg-blue btn-app find-time-period-btn" value="late-complete">
              <i class="fa fa-clock-o"></i> Late
            </a>
            
            <a id="search-time-period-incomplete" href="javascript:void(0)" class="btn bg-red btn-app find-time-period-btn" value="incomplete">
              <i class="fa fa-clock-o"></i> Delay
            </a>
             
           </div>
           
          </div>
         
        </div>
      </div>
  </div> -->
    





     
   <div class="row">
        <div class="col-xl-12">
           <div class="box" id="pagination-result-data-area">
          </div>
      	</div>
    </div>
  
    </section>
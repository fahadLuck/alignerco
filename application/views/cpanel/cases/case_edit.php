<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>

<script language="javascript" type="text/javascript">
		
		 $(document).ready(function () {
		  
				  $('#doctor_id').change(function() {
							
							var doctorID 	= $(this).val();
							var accountName = $('option:selected', this).attr('account-name');
							
							if (doctorID == '') {
									
								$('#doctor_account_name').val('');
							
							} else {
									
								$('#doctor_account_name').val(accountName);
							}
				  });
				 
				  $('#country').change(function() {
							
							var country = $(this).val();
							
							if (country == 168) {
									
									$('#city-area').show();	
							} else {
									
									$('#city').val(null).trigger('change');
									$('#city-area').hide();	
							}
							
							//$('#case-type-area').hide();	
				  });
				  
				  $('#country').change(function() {
							
							var country 		= $(this).val();
							var selectedCompany = 0;
							
							<?php 
									
									if ($this->input->post('submit') || $this->input->post('company')) {
				  			
											$companySelected = $this->input->post('company');	
							
									} else {
							
											$companySelected = $row['distributor'];
									}
					 		
							?>
							
							<?php if ($companySelected) { ?>
							
								 selectedCompany = <?php echo $companySelected; ?>;
							
							<?php } ?>
							
								$.ajax({
									  type   	  : "POST",
									  dataType 	  : "HTML",
									  url		  : "<?php echo  base_url();?>load-companies-by-country/",
									  data		  : {  
									  						country 			: country,
									  						selectedCompany 	: selectedCompany
													}
									
										 }).done(function( rowData ) {
									
									  			if (rowData) {
						  								
														$('#company-area').html(rowData);
						   						}
									  
								}); // END Ajax Request
							
				  });
				  
				  $("#case_revision").keyup(function() {
				
							$('#case_revision_id').val('');
							
							inputValue = $(this).val();
							
							   if (inputValue.length > 2) {
								
									  $.ajax({
										  
										   type	: "POST",
										   url	: "<?php echo base_url(); ?>get-case-IDs-information/",
										   data	: {
																	  
													  searchType	: 'departure',
													  caseID		: inputValue,
													},
										  
										  beforeSend: function(){
										  
												  $('#case-revision-info-loading').html('<img src="<?php echo base_url(); ?>backend_images/ajax-loader.gif" />');
										  },
										  
										  success: function(data){
								  				
												$("#case_revision-id-suggesstion-box").show();
												$("#case_revision-id-suggesstion-box").html(data);
												$("#case_revision").css("background","#FFF");
												
												$('#case-revision-info-loading').html('');
										 }	
									  });
							}
				});
				
				  $(document).mouseup(function (e){
   					
					 var container = $("#case_revision-id-suggesstion-box");

   					 if (container.has(e.target).length === 0) {
       				 	   container.hide();
						  
						   selectedID = $("#case_revision_id").val();
						   
						   if (selectedID == '') {
						   
						   		$("#case_revision").val('');
						   		$("#case_revision_id").val('');
						   }
    				}
				});
		 });
		 
		 function selectRevisionCase(caseID,patientName,companyName) {
		
				$("#case_revision-id-suggesstion-box").hide();
		
				$("#case_revision").val(caseID+' - '+patientName+' ('+companyName+')');
				$("#case_revision_id").val(caseID);
		
				$("#case-revision-info-loading").html('');
		 }
		 
		 function getCaseTypes(company) {
				
				if (company) {
				
					$('#case-type-area').show();	
						
							var selectedCaseType = 0;
							
							<?php 
									
									if ($this->input->post('submit') || $this->input->post('case_type')) {
				  			
											$caseTypeSelected = $this->input->post('case_type');	
							
									} else {
							
											$caseTypeSelected = $row['case_type'];
									}
					 		
							?>
							
							
							<?php if ($caseTypeSelected) { ?>
							
								 selectedCaseType = <?php echo $caseTypeSelected; ?>;
							
							<?php } ?>
							
								$.ajax({
									
									  type   	  : "POST",
									  dataType 	  : "HTML",
									  url		  : "<?php echo base_url();?>load-company-case-types/",
									  data		  : {  
									  						company 			: company,
									  						selectedCaseType 	: selectedCaseType
													}
									
										 }).done(function( rowData ) {
									
									  			if (rowData) {
						  								
														$('#case-types').html(rowData);
						   						}
									  
								}); // END Ajax Request
								
				} else {
								
						$('#case-type-area').hide();
						$('#case-types').val(null).trigger('change');	
				}
		 }

</script>

<style>
.frmSearch {
	border: 1px solid #a8d4b1;
	background-color: #c6f7d0;
	margin: 2px 0px;
	padding:40px;
	border-radius:4px;
}

.cases-IDs-list{
	float:left;
	list-style:none;
	margin-top:0px;
	padding:0;
	max-width:500px;
	position: absolute;
	z-index:999;
	}
	
.cases-IDs-list li{
	padding: 10px;
	background: #FFF;
	border-bottom: #bbb9b9 1px solid;
	border-left: #bbb9b9 1px solid;
	border-right: #bbb9b9 1px solid;
}

.cases-IDs-list li:hover{
	background:#ece3d2;
	cursor: pointer;
}

.search-box{
	padding: 10px;
	border: #a8d4b1 1px solid;
	border-radius:4px;
}
</style>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>manage-cases/">Cases</a></li>
        <li class="breadcrumb-item active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      					<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
     
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="glyphicon glyphicon-pencil"></i> Case</h3>
      </div>
   	  
      <form id="frm" name="frm" action="<?php echo base_url(); ?>edit-case/<?php echo encodeString($row['id']); ?>" method="post" autocomplete="off">
      <input type="hidden" name="case_id" value="<?php echo $row['id']; ?>" />
      <input type="hidden" name="old_portal_number" value="<?php echo $row['portal_number']; ?>" />
      <input type="hidden" name="old_patient_name" value="<?php echo $row['patient']; ?>" />
      <input type="hidden" name="old_case_revision_id" value="<?php echo $row['parent_case']; ?>" />
									
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	<!-- <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Portal Number</label>
				  <div class="col-sm-10">
                  <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('portal_number')) {
				  			
									$portalNumber = $this->input->post('portal_number');	
							
						} else {
							
									$portalNumber = $row['portal_number'];
						}
				   ?>
				   <input name="portal_number" class="form-control" type="text" value="<?php echo $portalNumber; ?>">
                   <?php if (form_error('portal_number')) { echo form_error('portal_number'); } ?>
				  </div>
				</div> -->
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Patient Name</label>
				  <div class="col-sm-10">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('patient_name')) {
				  			
									$patientName = $this->input->post('patient_name');	
							
						} else {
							
									$patientName = $row['patient'];
						}
				   ?>
                    
                    <input name="patient_name" placeholder="Patient Name" class="form-control" type="text" value="<?php echo $patientName; ?>">
                    <?php if (form_error('patient_name')) { echo form_error('patient_name'); } ?>
				  </div>
				</div>


				 <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Age</label>
				  <div class="col-sm-10">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('age')) {
				  			
									$patientAge = $this->input->post('age');	
							
						} else {
							
									$patientAge = $row['age'];
						}
				   ?>
                    
                    <input name="age" placeholder="Age" class="form-control" type="number" min="1" value="<?php echo $patientAge; ?>">
                    <?php if (form_error('age')) { echo form_error('age'); } ?>
				  </div>
				</div>



				<div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Gender</label>
                    <div class="col-sm-10">
                      <div class="radio">
                      
                      <?php 
                    
                          if ($this->input->post('gender')) {
                              
                                      $gender = $this->input->post('gender');	
                              
                           } else {
                              
                                      $gender = $row['gender'];
                           }
                  ?>
                      
                      
                        <input name="gender" value="<?php echo HARD_CODE_ID_MALE; ?>" id="Option_8" type="radio" class="radio-col-purple" <?php if ($gender == HARD_CODE_ID_MALE) { echo 'checked="checked" '; } ?>>
                        <label for="Option_8">Male</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="gender" value="<?php echo HARD_CODE_ID_FEMALE; ?>" id="Option_9" type="radio" class="radio-col-purple" <?php if ($gender == HARD_CODE_ID_FEMALE) { echo 'checked="checked" '; } ?>>
                        <label for="Option_9">Female</label>              
                      </div>
                      
                      <?php if (form_error('gender')) { echo form_error('gender'); } ?>
                    </div>
                 </div>


                  <div class="form-group row">
		            <label for="" class="col-sm-2 col-form-label">Country</label>
		                <div class="col-sm-10">
							<select name="patient_country" id="patient_country" class="form-control select2" style="width: 100%;">
		                       <option value="">Select a Country</option>
		                       <?php 
					
									if ($this->input->post('submit') || $this->input->post('patient_country')) {
							  			
												$countrySelected = $this->input->post('patient_country');	
										
									} else {
										
												$countrySelected = $row['patient_country'];
									}
										
									if ($countries->num_rows() > 0) {
															  
											  foreach($countries->result() as $country) {
												  
														  $countryID				=	$country->id;
														  $countryName				=	$country->name;
															  
															  if ($countrySelected == $countryID) {
															  
																	  $selectedcountry = 'selected = selected';	
															  
															  }	else {
																	  
																	  $selectedcountry = NULL;		
															  }
														  
														  echo '<option value="'.$countryID.'" '.$selectedcountry.'>'.$countryName.'</option>';
											  }	
									  }
								 ?>
		                      </select>
		                     <?php if (form_error('patient_country')) { echo form_error('patient_country'); } ?>
		              </div>
		        </div>



                 <div class="form-group row">
		            <label for="" class="col-sm-2 col-form-label">State</label>
		                <div class="col-sm-10">
							<select name="patient_state" id="patient_state" class="form-control select2" style="width: 100%;">
		                       <option value="">Select a State</option>
		                       <?php 
					
									if ($this->input->post('submit') || $this->input->post('patient_state')) {
							  			
												$stateSelected = $this->input->post('patient_state');	
										
									} else {
										
												$stateSelected = $row['patient_state'];
									}
										
									if ($states->num_rows() > 0) {
															  
											  foreach($states->result() as $state) {
												  
														  $stateID				=	$state->id;
														  $stateName				=	$state->name;
															  
															  if ($stateSelected == $stateID) {
															  
																	  $selectedstate = 'selected = selected';	
															  
															  }	else {
																	  
																	  $selectedstate = NULL;		
															  }
														  
														  echo '<option value="'.$stateID.'" '.$selectedstate.'>'.$stateName.'</option>';
											  }	
									  }
								 ?>
		                      </select>
		                     <?php if (form_error('patient_state')) { echo form_error('patient_state'); } ?>
		              </div>
		        </div>




				<div>
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">City</label>
				  <div class="col-sm-10">
					
                    <select name="patient_city" id="patient_city" class="form-control select2" style="width: 100%;">
                     <option value="">Select a city</option>
					 <?php 
					
						if ($this->input->post('submit') || $this->input->post('patient_city')) {
				  			
									$citySelected = $this->input->post('patient_city');	
							
						} else {
							
									$citySelected = $row['patient_city'];
						}
							
						if ($cities->num_rows() > 0) {
												  
								  foreach($cities->result() as $city) {
									  
											  $cityID				=	$city->id;
											  $cityName				=	$city->name;
												  
												  if ($citySelected == $cityID) {
												  
														  $selectedCity = 'selected = selected';	
												  
												  }	else {
														  
														  $selectedCity = NULL;		
												  }
											  
											  echo '<option value="'.$cityID.'" '.$selectedCity.'>'.$cityName.'</option>';
								  }	
						  }
					 ?>
                   </select>
                    <?php if (form_error('city')) { echo form_error('city'); } ?>
                  </div>
				</div>
                </div>



                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Zip Code</label>
				  <div class="col-sm-10">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('patient_zip_code')) {
				  			
									$patientZipCode = $this->input->post('patient_zip_code');	
							
						} else {
							
									$patientZipCode = $row['patient_zip_code'];
						}
				   ?>
                    
                    <input name="patient_zip_code" placeholder="Zip Code" class="form-control" type="text"  value="<?php echo $patientZipCode; ?>">
                    <?php if (form_error('patient_zip_code')) { echo form_error('patient_zip_code'); } ?>
				  </div>
				</div>



				<div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Email</label>
				  <div class="col-sm-10">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('patient_email')) {
				  			
									$patientEmail = $this->input->post('patient_email');	
							
						} else {
							
									$patientEmail = $row['patient_email'];
						}
				   ?>
                    
                    <input name="patient_email" placeholder="Email" class="form-control" type="text"  value="<?php echo $patientEmail; ?>">
                    <?php if (form_error('patient_email')) { echo form_error('patient_email'); } ?>
				  </div>
				</div>


				<div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Phone</label>
				  <div class="col-sm-10">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('patient_phone')) {
				  			
									$patientPhone = $this->input->post('patient_phone');	
							
						} else {
							
									$patientPhone = $row['patient_phone'];
						}
				   ?>
                    
                    <input name="patient_phone" placeholder="Phone" class="form-control" type="text"  value="<?php echo $patientPhone; ?>">
                    <?php if (form_error('patient_phone')) { echo form_error('patient_phone'); } ?>
				  </div>
				</div>


				<div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Address</label>
				  <div class="col-sm-4">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('patient_address_1')) {
				  			
									$patientAddress1 = $this->input->post('patient_address_1');	
							
						} else {
							
									$patientAddress1 = $row['patient_address_1'];
						}
				   ?>
                    
                    <input name="patient_address_1" placeholder="Address 1" class="form-control" type="text"  value="<?php echo $patientAddress1; ?>">
                    <?php if (form_error('patient_address_1')) { echo form_error('patient_address_1'); } ?>
				  </div>


				  <div class="col-sm-3">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('patient_address_2')) {
				  			
									$patientAddress2 = $this->input->post('patient_address_2');	
							
						} else {
							
									$patientAddress2 = $row['patient_address_2'];
						}
				   ?>
                    
                    <input name="patient_address_2" placeholder="Address 2" class="form-control" type="text"  value="<?php echo $patientAddress2; ?>">
                    <?php if (form_error('patient_address_2')) { echo form_error('patient_address_2'); } ?>
				  </div>

				  <div class="col-sm-3">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('patient_address_3')) {
				  			
									$patientAddress3 = $this->input->post('patient_address_3');	
							
						} else {
							
									$patientAddress3 = $row['patient_address_3'];
						}
				   ?>
                    
                    <input name="patient_address_3" placeholder="Address 3" class="form-control" type="text"  value="<?php echo $patientAddress3; ?>">
                    <?php if (form_error('patient_address_3')) { echo form_error('patient_address_3'); } ?>
				  </div>

				</div>

                
                <!-- <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Doctor</label>
				  <div class="col-sm-10">
					
					 <?php 
                    
                          if ($this->input->post('doctor_id')) {
                              
                                      $doctorSelected = $this->input->post('doctor_id');	
                              
                              } else {
                              
                                      $doctorSelected = $row['doctor'];
                              }
                     ?>
                    
                    <select name="doctor_id" id="doctor_id" class="form-control select2" style="width: 100%;">
                    <option value="">Select a doctor</option>
 								<?php 
                                              if ($doctors->num_rows() > 0) {
                                                      
                                                      foreach($doctors->result() as $doctor) {
                                                          
                                                                  $doctorID				=	$doctor->id;
                                                                  $doctorName			=	$doctor->name;
																  $accountName			=	$doctor->account_name;
                                                                      
                                                                      if ($doctorSelected == $doctorID) {
                                                                      
                                                                              $selectedDoctor = 'selected = selected';	
                                                                      
                                                                      }	else {
                                                                              
                                                                              $selectedDoctor = NULL;		
                                                                      }
                                                                  
                                                                  echo '<option value="'.$doctorID.'" '.$selectedDoctor.' account-name="'.$accountName.'">'.$doctorName.'</option>';
                                                      }	
                                              }
                                ?>                 
                   </select>
                    <?php if (form_error('doctor_id')) { echo form_error('doctor_id'); } ?>
				  </div>
				</div> -->
                
                <!-- <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Doctor Account</label>
				  <div class="col-sm-10">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('doctor_account_name')) {
				  			
									$doctorAccountName = $this->input->post('doctor_account_name');	
							
						} else {
							
									if ($doctorSelected) {
									
										$doctorDetails  	= $this->model_shared->getRecordMultipleWhere('account_name',MEDICAL_DOCTORS_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'id' => $doctorSelected));
										$doctorDetails  	= $doctorDetails->row_array();
										
										$doctorAccountName  = $doctorDetails['account_name'];
									
									} else {
										
										$doctorAccountName  = NULL;	
									}
		
									 
						}
				   ?>
                    
                    <input name="doctor_account_name" id="doctor_account_name" class="form-control" type="text" value="<?php echo $doctorAccountName; ?>">
                    <?php if (form_error('doctor_account_name')) { echo form_error('doctor_account_name'); } ?>
				  </div>
				</div> -->
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Receive Date</label>
				  <div class="col-sm-10">
				  
                  <?php 
                    
                          if ($this->input->post('receive_date')) {
                              
                                      $receiveDate = $this->input->post('receive_date');	
                              
                           } else {
                              
                                      $receiveDate = $row['receive_date'];
                           }
                  ?>
				  
                  <input class="form-control" name="receive_date" type="date" value="<?php echo $receiveDate; ?>" id="" readonly>
                  <?php if (form_error('receive_date')) { echo form_error('receive_date'); } ?>
				  </div>
				</div>
                
                <!-- <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Nature Of Patient</label>
				  <div class="col-sm-10">
					
                   <?php 
                    
                          if ($this->input->post('nature_of_patient')) {
                              
                                      $natureOfPatient = $this->input->post('nature_of_patient');	
                              
                           } else {
                              
                                      $natureOfPatient = $row['nature_of_patient'];
                           }
                  ?>
                    
                    
                    <div class="radio">
                  	  <input name="nature_of_patient" value="Overseas" id="Option_1" type="radio" class="radio-col-yellow" <?php if ($natureOfPatient == 'Overseas') { echo 'checked="checked" '; } ?>>
					  <label for="Option_1">Overseas</label>                    
                  </div>
                    <div class="radio">
                        <input name="nature_of_patient" value="Local" id="Option_2" type="radio" class="radio-col-yellow" <?php if ($natureOfPatient == 'Local') { echo 'checked="checked" '; } ?>>
                        <label for="Option_2">Local</label>                    
                    </div>
                    <?php if (form_error('nature_of_patient')) { echo form_error('nature_of_patient'); } ?>
                  </div>
				</div> -->
                
                <!-- <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Impression Type</label>
				  <div class="col-sm-10">
                   <?php 
                    
                          if ($this->input->post('impression')) {
                              
                                      $impression = $this->input->post('impression');	
                              
                           } else {
                              
                                      $impression = $row['impression_type'];
                           }
							
						 if ($impressionTypes->num_rows() > 0) {
                                                      
                               	$counterImpression = 3;
							    foreach($impressionTypes->result() as $impressionType) {
					?>
                    			
                                 <div class="radio">
                                      <input name="impression" value="<?php echo $impressionType->id; ?>" id="Option_<?php echo $counterImpression; ?>" type="radio" class="radio-col-blue" <?php if ($impression == $impressionType->id) { echo 'checked="checked" '; } ?> disabled>
                                      <label for="Option_<?php echo $counterImpression; ?>"><?php echo $impressionType->name; ?></label>                    
                                 </div>
                    
                    <?php	
				   					$counterImpression++;
								}
						 }
				    ?>
                    <?php if (form_error('impression')) { echo form_error('impression'); } ?>
                  </div>
				</div> -->
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Arch (es)</label>
				  <div class="col-sm-10">
					  
                      <?php 
                    
                          if ($this->input->post('submit') || $this->input->post('arch_upper')) {
                              
                                      $archUpper = $this->input->post('arch_upper');	
                              
                           } else {
                              
                                      $archUpper = $row['arch_upper'];
                           }
						   
						   if ($this->input->post('submit') || $this->input->post('arch_lower')) {
                              
                                      $archLower = $this->input->post('arch_lower');	
                              
                           } else {
                              
                                      $archLower = $row['arch_lower'];
                           }
                  ?>
                      
                      <div class="demo-checkbox">
                          <input id="md_checkbox_1" name="arch_upper" value="<?php echo HARD_CODE_ID_YES ?>" class="chk-col-green" type="checkbox" <?php if ($archUpper == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                          <label for="md_checkbox_1">Upper</label>
                          
                          <input id="md_checkbox_2" name="arch_lower" value="<?php echo HARD_CODE_ID_YES ?>" class="chk-col-green" type="checkbox" <?php if ($archLower == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                          <label for="md_checkbox_2">Lower</label>
                      </div>                 
                   <?php if (form_error('arch')) { echo form_error('arch'); } ?>
                  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Country</label>
				  <div class="col-sm-10">
					
                    <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('country')) {
				  			
									$countrySelected = $this->input->post('country');	
							
						} else {
							
									$countrySelected = $row['country'];
						}
				   ?>
                    
                    <select name="country" id="country" class="form-control select2" style="width: 100%;">
                     <option value="">Select a country</option>
					 <?php 
					 		if ($countries->num_rows() > 0) {
                                                      
                                                      foreach($countries->result() as $country) {
                                                          
                                                               $countryID				=	$country->id;
                                                                $countryName			=	$country->name;
                                                                  
																    if ($countryID == 40 || $countryID == 236  || $countryID == 235 || $countryID == 14) {
																	} else { continue; }
                                                                      
                                                                      if ($countrySelected == $countryID) {
                                                                      
                                                                              $selectedCountry = 'selected = selected';	
                                                                      
                                                                      }	else {
                                                                              
                                                                              $selectedCountry = NULL;		
                                                                      }
                                                                  
                                                                  echo '<option value="'.$countryID.'" '.$selectedCountry.'>'.$countryName.'</option>';
                                                      }	
                                              }
					 ?>
                    </select>
                    <?php if (form_error('country')) { echo form_error('country'); } ?>
                  </div>
				</div>
                
                <div id="city-area" <?php if ($countrySelected == 168) { echo 'style="display:block;"'; } else { echo 'style="display:none;"';} ?>>
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">City</label>
				  <div class="col-sm-10">
					
                    <select name="city" id="city" class="form-control select2" style="width: 100%;">
                   <option value="">Select a city</option>
					 <?php 
					
						if ($this->input->post('submit') || $this->input->post('city')) {
				  			
									$citySelected = $this->input->post('city');	
							
						} else {
							
									$citySelected = $row['city'];
						}
							
						if ($cities->num_rows() > 0) {
												  
								  foreach($cities->result() as $city) {
									  
											  $cityID				=	$city->id;
											  $cityName				=	$city->name;
												  
												  if ($citySelected == $cityID) {
												  
														  $selectedCity = 'selected = selected';	
												  
												  }	else {
														  
														  $selectedCity = NULL;		
												  }
											  
											  echo '<option value="'.$cityID.'" '.$selectedCity.'>'.$cityName.'</option>';
								  }	
						  }
					 ?>
                   </select>
                    <?php if (form_error('city')) { echo form_error('city'); } ?>
                  </div>
				</div>
                </div>
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Distributor</label>
				  <div class="col-sm-10">
				   <div id="company-area">
                    <select name="company" id="company" class="form-control select2" style="width: 100%;" onchange="return getCaseTypes(this.value);">
                    <option value="">Select a company</option>
					 <?php 
					 		if ($this->input->post('submit') || $this->input->post('company')) {
				  			
									$companySelected = $this->input->post('company');	
							
							} else {
							
									$companySelected = $row['distributor'];
							}
					 		
							if ($companies->num_rows() > 0) {
                                                      
									foreach($companies->result() as $company) {
										
												$companyID				=	$company->id;
												$companyName			=	$company->name;
													
													if ($companySelected == $companyID) {
													
															$selectedCompany = 'selected = selected';	
													
													}	else {
															
															$selectedCompany = NULL;		
													}
												
												echo '<option value="'.$companyID.'" '.$selectedCompany.'>'.$companyName.'</option>';
									}	
							}
  						 ?>
                    </select>
                  </div>
                   <?php if (form_error('company')) { echo form_error('company'); } ?>
				  </div>
				</div>
                
                <!-- <div class="form-group row" id="case-type-area" style="display:none;">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Case Type</label>
				  <div class="col-sm-10">
				   <div id="case-types"></div>
                   <?php if (form_error('case_type')) { echo form_error('case_type'); } ?>
				  </div>
				</div> -->
                
               <!--  <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Airway Bill Number</label>
				  <div class="col-sm-10">
					
                    <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('airway_bill_number')) {
				  			
									$airwayBillNumber = $this->input->post('airway_bill_number');	
							
						} else {
							
									$airwayBillNumber = $row['airway_bill_number'];
						}
				   ?>
                    
                    <input name="airway_bill_number" class="form-control" type="text" value="<?php echo $airwayBillNumber; ?>" id="">
                    <?php if (form_error('airway_bill_number')) { echo form_error('airway_bill_number'); } ?>
				  </div>
				</div> -->
                
                <!-- <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Priority</label>
				  <div class="col-sm-10">
					
                    <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('priority')) {
				  			
									$priority = $this->input->post('priority');	
							
						} else {
							
									$priority = $row['priority'];
						}
				   ?>
                    
                    <div class="radio">
                  	  <input name="priority" value="<?php echo PRIORITY_HIGH; ?>" id="Option_11" type="radio" class="radio-col-red" <?php if ($priority == PRIORITY_HIGH) { echo 'checked="checked" '; } ?>>
					  <label for="Option_11">High</label>                    
                  	</div>
                    
                    <div class="radio">
                  	  <input name="priority" value="<?php echo PRIORITY_MEDIUM; ?>" id="Option_12" type="radio" class="radio-col-yellow" <?php if ($priority == PRIORITY_MEDIUM) { echo 'checked="checked" '; } ?>>
					  <label for="Option_12">Medium</label>                    
                  	</div>
                    
                    <div class="radio">
                  	  <input name="priority" value="<?php echo PRIORITY_LOW; ?>" id="Option_13" type="radio" class="radio-col-green" <?php if ($priority == PRIORITY_LOW) { echo 'checked="checked" '; } ?>>
					  <label for="Option_13">Low</label>                    
                  	</div>
                     
                    <?php if (form_error('priority')) { echo form_error('priority'); } ?>
                  </div>
				</div> -->
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Remarks / Comments</label>
				  <div class="col-sm-10">
					
					<?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('comments')) {
				  			
									$description = $this->input->post('comments');	
							
						} else {
							
									$description = $row['description'];
						}
				   ?>
                    
                    <textarea name="comments" class="form-control" rows="3" placeholder=""><?php echo $description; ?></textarea>
                    <?php if (form_error('comments')) { echo form_error('comments'); } ?>
				  </div>
				</div>
                
                <!-- <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Revision Case ID <span id="case-revision-info-loading"></span></label>
				  <div class="col-sm-10">
					
                    <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('case_revision_id')) {
				  			
									$caseRevision	 = $this->input->post('case_revision');	
									$caseRevisionID  = $this->input->post('case_revision_id');	
							
						} else {
								
								if ($row['parent_case']) {
									
									$caseRevision	 = $row['parent_case'];	
									$caseRevisionID  = $row['parent_case'];
								
								} else {
									
									$caseRevision	 = $this->input->post('case_revision');	
									$caseRevisionID  = $this->input->post('case_revision_id');	
								}
						}
				   ?>
                    
                     <input type="text" class="form-control" name="case_revision" id="case_revision" value="<?php echo $caseRevision; ?>" />
               		 <input type="hidden" name="case_revision_id" id="case_revision_id" value="<?php echo $caseRevisionID; ?>" />
               		 <div id="case_revision-id-suggesstion-box"></div>
					 <?php if (form_error('case_revision_id')) { echo form_error('case_revision_id'); } ?>
				  </div>
				</div> -->
                
                
               <?php if (($row['RX_form'] || $row['x_rays_opg'] || $row['x_rays_ceph'] || $row['file_assessment']) || ($casePhotos->num_rows() > 0) ) { ?>  
               
                 <!-- fancybox -->
				<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend_assets/vendor_components/lightbox-master/dist/ekko-lightbox.css" />
     
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Attachments</label>
				  <div class="col-sm-10">
					<div class="timeline-body">
                                     
						 <?php if ($casePhotos->num_rows() > 0) {  
                            
                                     foreach($casePhotos->result() as $photo) { 
                         ?>
                          
                                        <a href="<?php echo CASE_PICTURES_URL_PATH ?><?php echo $photo->name; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo $row['patient']; ?>"><img data-toggle="tooltip" title="" data-original-title="Intra & Extra Oral Pictures" style="padding-top:10px; padding-left:5px;" src="<?php echo CASE_PICTURES_URL_PATH ?>thumbnail/thumbnail_<?php echo $photo->name; ?>" alt="gallery" ></a>
                         <?php
                                     } 
                                 } 
                         ?>
                         
                         <?php if ($row['RX_form']) { ?> 
                                 <a href="<?php echo CASE_RX_FORM_URL_PATH.$row['RX_form']; ?>" data-toggle="lightbox" data-gallery="multiimages"  data-title="RX Form"><img data-toggle="tooltip" title="" data-original-title="RX Form" src="<?php echo CASE_RX_FORM_URL_PATH.'thumbnail/thumbnail_'.$row['RX_form']; ?>" alt="" class="margin" /></a>
                          <?php } ?>
                         
                          <?php if ($row['x_rays_opg']) { ?> 
                                 <a href="<?php echo CASE_X_RAY_OPG_URL_PATH.'/'.$row['x_rays_opg']; ?>" data-toggle="lightbox" data-gallery="multiimages"  data-title="X-rays (OPG)"><img data-toggle="tooltip" title="" data-original-title="X-rays (OPG)" src="<?php echo CASE_X_RAY_OPG_URL_PATH.'/thumbnail/thumbnail_'.$row['x_rays_opg']; ?>" alt="" class="margin" /></a>
                          <?php } ?>
                          
                          <?php if ($row['x_rays_ceph']) { ?> 
                                 <a href="<?php echo CASE_X_RAY_CEPH_URL_PATH.'/'.$row['x_rays_ceph']; ?>" data-toggle="lightbox" data-gallery="multiimages"  data-title="X-rays (Ceph)"><img data-toggle="tooltip" title="" data-original-title="X-rays (Ceph)" src="<?php echo CASE_X_RAY_CEPH_URL_PATH.'/thumbnail/thumbnail_'.$row['x_rays_ceph']; ?>" alt="" class="margin" /></a>
                          <?php } ?>
                          
                          <?php if ($row['file_assessment']) { ?> 
                                 <a href="<?php echo CASE_FILE_ASSESSMENT_URL_PATH; ?><?php echo $row['file_assessment']; ?>"><img data-toggle="tooltip" title="" data-original-title="Case Assessment File" src="<?php echo base_url() ?>backend_images/document_icon.png" alt="" class="margin" /></a>
                          <?php } ?>
                          
                        </div>
				  </div>
				</div>
                
			    <?php } ?>
                
                
                
                
				
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
          
        
        <!-- /.box-body -->
      </div>
      <!-- /.box -->


      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-money"></i> Transaction</h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Mode</label>
                    <div class="col-sm-10">
                      <div class="radio">

                        <input name="transaction" value="kit" id="kit_sale" type="radio" class="radio-col-red" 
                        <?php 
                        if ($this->input->post('transaction') == 'kit') 
                        	{ 
                        		echo 'checked="checked" '; 
                        	} 

                        	elseif($transactionMode->mode == 'kit') 
	                        {
	                         	echo 'checked="checked" '; 
	                        }
                        ?>>
                        <label for="kit_sale">Sale Kit</label>

                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="transaction" value="instalment" id="instalment" type="radio" class="radio-col-red" 
                        <?php 
                        if ($this->input->post('transaction') == 'instalment') 
                        	{ 
                        		echo 'checked="checked" '; 
                        	} 

                        	elseif($transactionMode->mode == 'instalment') 
	                        {
	                         	echo 'checked="checked" '; 
	                        }
                        ?>>
                        <label for="instalment">Instalment</label> 

                         &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="transaction" value="case_purchase" id="case_purchase" type="radio" class="radio-col-red" 
                        <?php 
                        if ($this->input->post('transaction') == 'case_purchase') 
                        	{ 
                        		echo 'checked="checked" '; 
                        	} 

                        	elseif($transactionMode->mode == 'case_purchase') 
	                        {
	                         	echo 'checked="checked" '; 
	                        }
                        ?>>
                        <label for="case_purchase">Full Case</label> 

                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="transaction" value="retainer_only" id="retainer_only" type="radio" class="radio-col-red" 
                        <?php 
                        if ($this->input->post('transaction') == 'retainer_only') 
                        	{ 
                        		echo 'checked="checked" '; 
                        	} 

                        	elseif($transactionMode->mode == 'retainer_only') 
	                        {
	                         	echo 'checked="checked" '; 
	                        }
                        ?>>
                        <label for="retainer_only">Retainer Only</label>  
                                   
                      </div>
                      
                      <?php if (form_error('transaction')) { echo form_error('transaction'); } ?>
                    </div>
                </div>
                


                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Treatment</label>
                    <div class="col-sm-10">
                      <div class="radio">
                        <input name="treatment_type" value="night_time" id="night_time" type="radio" class="radio-col-navy" 
                        <?php

	                        if ($this->input->post('treatment_type') == 'night_time') 
	                        { 
	                        	echo 'checked="checked" '; 
	                        }
	                        elseif($row['treatment_type'] == 'night_time') 
	                        {
	                         	echo 'checked="checked" '; 
	                        } 

                        ?>
                        >
                        <label for="night_time">Night Time</label>

                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="treatment_type" value="regular" id="regular" type="radio" class="radio-col-navy" 
                        <?php

                        if ($this->input->post('treatment_type') == 'regular') 
                        	{ 
                        		echo 'checked="checked" '; 
                        	}

                        	elseif($row['treatment_type'] == 'regular') 
	                        {
	                         	echo 'checked="checked" '; 
	                        }  
                        ?>
                        >
                        <label for="regular">Regular</label>   
                      </div>
                      
                      <?php if (form_error('treatment_type')) { echo form_error('treatment_type'); } ?>
                    </div>
                </div>              
				
            </div>
          </div>
        </div>
 
     </div>









     <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Medical History</h3>
        </div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-6 col-12">
         
              <div class="form-group">
                <label>Do you have loose dental restoration-crowns, etc?</label>
                <div class="radio">
                  <input name="medical_history_loose_crowns" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_loose_crowns_yes" type="radio" class="radio-col-purple" 
                  <?php 
	                  if ($this->input->post('medical_history_loose_crowns') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked" '; 
	                  	}
	                  	elseif ($row['loose_crowns'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>
                  >
                  <label for="medical_history_loose_crowns_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_loose_crowns" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_loose_crowns_no" type="radio" class="radio-col-purple" 
                  <?php 
	                   if ($this->input->post('medical_history_loose_crowns') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked" '; 
	                  	}
	              	   elseif ($row['loose_crowns'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>
                  >
                  <label for="medical_history_loose_crowns_no">No</label>              
                </div>
                <?php if (form_error('medical_history_loose_crowns')) { echo form_error('medical_history_loose_crowns'); } ?>
              </div>
              
              <div class="form-group">
               <div id="loose-dental-crowns-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="LDC_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_LDC('1','LDC_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                 <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_LDC_TMQ1_<?php echo $i; ?>">
                                                <input id="LDC_TMQ1_<?php echo $i; ?>" name="LDC_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-purple TM" type="checkbox"
                                                <?php 
                                                	foreach($LDC_TMQ1 as $LDC_tm1)
													{
													   if($LDC_tm1==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>
                                                >
                                                <label title="Teeth Movement" for="LDC_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="LDC_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_LDC('2','LDC_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                   <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_LDC_TMQ2_<?php echo $i; ?>">
                                                 <input id="LDC_TMQ2_<?php echo $i; ?>" name="LDC_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-purple TM" type="checkbox" 
                                                 <?php 
                                                	foreach($LDC_TMQ2 as $LDC_tm2)
													{
													   if($LDC_tm2==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>
                                                >
                                                 <label for="LDC_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="LDC_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_LDC('4','LDC_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_LDC_TMQ4_<?php echo $i; ?>">
                                                <input id="LDC_TMQ4_<?php echo $i; ?>" name="LDC_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-purple TM" type="checkbox" 
                                                <?php 
                                                	foreach($LDC_TMQ4 as $LDC_tm4)
													{
													   if($LDC_tm4==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>
                                                >
                                                <label for="LDC_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="LDC_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_LDC('3','LDC_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_LDC_TMQ3_<?php echo $i; ?>">
                                                 <input id="LDC_TMQ3_<?php echo $i; ?>" name="LDC_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-purple TM" type="checkbox" 
                                                 <?php 
                                                	foreach($LDC_TMQ3 as $LDC_tm3)
													{
													   if($LDC_tm3==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>
                                                >
                                                 <label for="LDC_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group">
                <label>Do you have fillings?</label>
                <div class="radio">
                  <input name="medical_history_fillings" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_fillings_yes" type="radio" class="radio-col-orange" 
                  <?php 
	                  if ($this->input->post('medical_history_fillings') == HARD_CODE_ID_YES) 
	                  { 
	                  	echo 'checked="checked" '; 
	                  }
	                  elseif ($row['fillings'] == HARD_CODE_ID_YES) 
                  	  {
                  	 	  echo 'checked="checked" '; 
                  	  } 
                  ?>>
                  <label for="medical_history_fillings_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_fillings" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_fillings_no" type="radio" class="radio-col-orange" 
                  <?php 
	                  if ($this->input->post('medical_history_fillings') == HARD_CODE_ID_NO) 
	                  { 
	                  	echo 'checked="checked" '; 
	                  }
	                  elseif ($row['fillings'] == HARD_CODE_ID_NO) 
                  	  {
                  	 	  echo 'checked="checked" '; 
                  	  } 
                  ?>>
                  <label for="medical_history_fillings_no">No</label>              
                </div>
                <?php if (form_error('medical_history_fillings')) { echo form_error('medical_history_fillings'); } ?>
              </div>
              
              <div class="form-group">
               <div id="fillings-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="FILLING_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_FILLING('1','FILLING_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                 <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_FILLING_TMQ1_<?php echo $i; ?>">
                                                <input id="FILLING_TMQ1_<?php echo $i; ?>" name="FILLING_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-orange TM" type="checkbox" 
                                                <?php 
                                                	foreach($FILLING_TMQ1 as $FILLING_tm1)
													{
													   if($FILLING_tm1==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label title="Teeth Movement" for="FILLING_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="FILLING_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_FILLING('2','FILLING_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_FILLING_TMQ2_<?php echo $i; ?>">
                                                 <input id="FILLING_TMQ2_<?php echo $i; ?>" name="FILLING_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-orange TM" type="checkbox"
                                                 <?php 
                                                	foreach($FILLING_TMQ2 as $FILLING_tm2)
													{
													   if($FILLING_tm2==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="FILLING_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="FILLING_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_FILLING('4','FILLING_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_FILLING_TMQ4_<?php echo $i; ?>">
                                                <input id="FILLING_TMQ4_<?php echo $i; ?>" name="FILLING_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-orange TM" type="checkbox" 
                                                <?php 
                                                	foreach($FILLING_TMQ4 as $FILLING_tm4)
													{
													   if($FILLING_tm4==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label for="FILLING_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="FILLING_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_FILLING('3','FILLING_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_FILLING_TMQ3_<?php echo $i; ?>">
                                                 <input id="FILLING_TMQ3_<?php echo $i; ?>" name="FILLING_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-orange TM" type="checkbox" 
                                                 <?php 
                                                	foreach($FILLING_TMQ3 as $FILLING_tm3)
													{
													   if($FILLING_tm3==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="FILLING_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group" style="padding-top:50px;">
                <label>Did you undertake a root canal procedure in the past?</label>
                <div class="radio">
                  <input name="medical_history_undertake_root_canal_procedure" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_undertake_root_canal_procedure_yes" type="radio" class="radio-col-green" 
                  <?php 
	                   if($this->input->post('medical_history_undertake_root_canal_procedure') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['undertake_root_canal_procedure'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_undertake_root_canal_procedure_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_undertake_root_canal_procedure" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_undertake_root_canal_procedure_no" type="radio" class="radio-col-green" 
                  <?php 
	                   if($this->input->post('medical_history_undertake_root_canal_procedure') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['undertake_root_canal_procedure'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_undertake_root_canal_procedure_no">No</label>              
                </div>
                <?php if (form_error('medical_history_undertake_root_canal_procedure')) { echo form_error('medical_history_undertake_root_canal_procedure'); } ?>
              </div>
              
              <div class="form-group">
               <div id="undertake-root-canal-procedure-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="URCP_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_URCP('1','URCP_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_URCP_TMQ1_<?php echo $i; ?>">
                                                <input id="URCP_TMQ1_<?php echo $i; ?>" name="URCP_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-green TM" type="checkbox" 
                                                <?php 
                                                	foreach($URCP_TMQ1 as $URCP_tm1)
													{
													   if($URCP_tm1==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label title="Teeth Movement" for="URCP_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="URCP_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_URCP('2','URCP_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_URCP_TMQ2_<?php echo $i; ?>">
                                                 <input id="URCP_TMQ2_<?php echo $i; ?>" name="URCP_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-green TM" type="checkbox"
                                                 <?php 
                                                	foreach($URCP_TMQ2 as $URCP_tm2)
													{
													   if($URCP_tm2==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="URCP_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="URCP_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_URCP('4','URCP_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_URCP_TMQ4_<?php echo $i; ?>">
                                                <input id="URCP_TMQ4_<?php echo $i; ?>" name="URCP_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-green TM" type="checkbox"
                                                <?php 
                                                	foreach($URCP_TMQ4 as $URCP_tm4)
													{
													   if($URCP_tm4==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label for="URCP_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="URCP_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_URCP('3','URCP_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_URCP_TMQ3_<?php echo $i; ?>">
                                                 <input id="URCP_TMQ3_<?php echo $i; ?>" name="URCP_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-green TM" type="checkbox"
                                                 <?php 
                                                	foreach($URCP_TMQ3 as $URCP_tm3)
													{
													   if($URCP_tm3==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="URCP_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group" style="padding-top:50px;">
                <label>Do your teeth hurt?</label>
                
                <div class="radio">
                  <input name="medical_history_teeth_hurt" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_teeth_hurt_yes" type="radio" class="radio-col-navy" 
                  <?php 
	                  if ($this->input->post('medical_history_teeth_hurt') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['teeth_hurt'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_teeth_hurt_yes">Yes</label>      
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_teeth_hurt" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_teeth_hurt_no" type="radio" class="radio-col-navy" 
                  <?php 
	                  if ($this->input->post('medical_history_teeth_hurt') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['teeth_hurt'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_teeth_hurt_no">No</label>              
                 </div>
              </div>
             
              <div class="form-group">
                <label>Do you have any wounds or swellings in your mouth?</label>
                <div class="radio">
                  <input name="medical_history_wounds_swellings" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_wounds_swellings_yes" type="radio" class="radio-col-navy" <?php 
	                  if ($this->input->post('medical_history_wounds_swellings') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['wounds_swellings'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_wounds_swellings_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_wounds_swellings" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_wounds_swellings_no" type="radio" class="radio-col-navy" <?php 
	                  if ($this->input->post('medical_history_wounds_swellings') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['wounds_swellings'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_wounds_swellings_no">No</label>              
                </div>
                <?php if (form_error('medical_history_wounds_swellings')) { echo form_error('medical_history_wounds_swellings'); } ?>
                            
              </div>
              
              <div class="form-group">
                <label>Do you have problems with your gums? (E.g. bleeding, declining or inflammed gums?)</label>
                <div class="radio">
                  <input name="medical_history_gums_problem" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_gums_problem_yes" type="radio" class="radio-col-navy" 
                  <?php 
	                  if ($this->input->post('medical_history_gums_problem') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['gums_problem'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_gums_problem_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_gums_problem" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_gums_problem_no" type="radio" class="radio-col-navy" 
                 <?php 
	                  if ($this->input->post('medical_history_gums_problem') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['gums_problem'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_gums_problem_no">No</label>              
                </div>
                <?php if (form_error('medical_history_gums_problem')) { echo form_error('medical_history_gums_problem'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you wear a retainer?</label>
                <div class="radio">
                  <input name="medical_history_wear_retainer" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_wear_retainer_yes" type="radio" class="radio-col-navy" 
                  <?php 
                  if ($this->input->post('medical_history_wear_retainer') == HARD_CODE_ID_YES) 
                  	{ 
                  		echo 'checked="checked"'; 
                  	}
                  	elseif ($row['wear_retainer'] == HARD_CODE_ID_YES) 
                  	{
                  	 	echo 'checked="checked" '; 
                  	} 
                  ?>>
                  <label for="medical_history_wear_retainer_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_wear_retainer" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_wear_retainer_no" type="radio" class="radio-col-navy" 
                  <?php 
                  if ($this->input->post('medical_history_wear_retainer') == HARD_CODE_ID_NO) 
                  	{ 
                  		echo 'checked="checked"'; 
                  	}
                  	elseif ($row['wear_retainer'] == HARD_CODE_ID_NO) 
                  	{
                  	 	echo 'checked="checked" '; 
                  	} 
                  ?>>
                  <label for="medical_history_wear_retainer_no">No</label>              
                </div>
                
                <?php if (form_error('medical_history_wear_retainer')) { echo form_error('medical_history_wear_retainer'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you have untreated gum disease?</label>
                <div class="radio">
                  <input name="medical_history_untreated_gum_disease" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_untreated_gum_disease_yes" type="radio" class="radio-col-navy" 
                  <?php 
	                  if ($this->input->post('medical_history_untreated_gum_disease') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['untreated_gum_disease'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_untreated_gum_disease_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_untreated_gum_disease" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_untreated_gum_disease_no" type="radio" class="radio-col-navy" 
                   <?php 
	                  if ($this->input->post('medical_history_untreated_gum_disease') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['untreated_gum_disease'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_untreated_gum_disease_no">No</label>              
                </div>
                <?php if (form_error('medical_history_untreated_gum_disease')) { echo form_error('medical_history_untreated_gum_disease'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you have recent history of jaw locking or severe jaw pain? (past 6 months)</label>
                <div class="radio">
                  <input name="medical_history_jaw_pain" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_jaw_pain_yes" type="radio" class="radio-col-navy" 
                  <?php 
	                  if ($this->input->post('medical_history_jaw_pain') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['jaw_pain'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_jaw_pain_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_jaw_pain" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_jaw_pain_no" type="radio" class="radio-col-navy" 
                 <?php 
	                  if ($this->input->post('medical_history_jaw_pain') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['jaw_pain'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_jaw_pain_no">No</label>              
                </div>
                <?php if (form_error('medical_history_jaw_pain')) { echo form_error('medical_history_jaw_pain'); } ?>
              </div>

            </div>

            <div class="col-md-6 col-12">
              
              <div class="form-group">
                <label>Do you wear veneers?</label>
                <div class="radio">
                  <input name="medical_history_wear_veneers" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_wear_veneers_yes" type="radio" class="radio-col-yellow" 
                  <?php 
	                  if ($this->input->post('medical_history_wear_veneers') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"';
	                    }
	                    elseif ($row['wear_veneers'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_wear_veneers_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_wear_veneers" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_wear_veneers_no" type="radio" class="radio-col-yellow" 
                  <?php 
	                  if ($this->input->post('medical_history_wear_veneers') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"';
	                    }
	                    elseif ($row['wear_veneers'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_wear_veneers_no">No</label>              
                </div>
                <?php if (form_error('medical_history_wear_veneers')) { echo form_error('medical_history_wear_veneers'); } ?>
              </div>
              
              <div class="form-group">
               <div id="wear-veneers-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="WV_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_WV('1','WV_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                 <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_WV_TMQ1_<?php echo $i; ?>">
                                                <input id="WV_TMQ1_<?php echo $i; ?>" name="WV_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-yellow TM" type="checkbox"
                                                <?php 
                                                	foreach($WV_TMQ1 as $WV_tm1)
													{
													   if($WV_tm1==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label title="Teeth Movement" for="WV_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="WV_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_WV('2','WV_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_WV_TMQ2_<?php echo $i; ?>">
                                                 <input id="WV_TMQ2_<?php echo $i; ?>" name="WV_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-yellow TM" type="checkbox"
                                                 <?php 
                                                	foreach($WV_TMQ2 as $WV_tm2)
													{
													   if($WV_tm2==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="WV_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                 <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="WV_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_WV('4','WV_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_WV_TMQ4_<?php echo $i; ?>">
                                                <input id="WV_TMQ4_<?php echo $i; ?>" name="WV_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-yellow TM" type="checkbox"
                                                <?php 
                                                	foreach($WV_TMQ4 as $WV_tm4)
													{
													   if($WV_tm4==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label for="WV_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="WV_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_WV('3','WV_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_WV_TMQ3_<?php echo $i; ?>">
                                                 <input id="WV_TMQ3_<?php echo $i; ?>" name="WV_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-yellow TM" type="checkbox"
                                                 <?php 
                                                	foreach($WV_TMQ3 as $WV_tm3)
													{
													   if($WV_tm3==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="WV_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group">
                <label>Do you have implant?</label>
                <div class="radio">
                  <input name="medical_history_implant" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_implant_yes" type="radio" class="radio-col-maroon" 
                  <?php 
	                  if ($this->input->post('medical_history_implant') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked" '; 
	                  	}
	                  	elseif ($row['implant'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_implant_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_implant" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_implant_no" type="radio" class="radio-col-maroon" 
                  <?php 
	                  if ($this->input->post('medical_history_implant') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked" '; 
	                  	}
	                  	elseif ($row['implant'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_implant_no">No</label>              
                </div>
                <?php if (form_error('medical_history_implant')) { echo form_error('medical_history_implant'); } ?>
              </div>
              
              <div class="form-group">
               <div id="implant-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="IMPLANT_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_IMPLANT('1','IMPLANT_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_IMPLANT_TMQ1_<?php echo $i; ?>">
                                                <input id="IMPLANT_TMQ1_<?php echo $i; ?>" name="IMPLANT_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-maroon TM" type="checkbox" 
                                                <?php 
                                                	foreach($IMPLANT_TMQ1 as $IMPLANT_tm1)
													{
													   if($IMPLANT_tm1==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label title="Teeth Movement" for="IMPLANT_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                 <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="IMPLANT_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_IMPLANT('2','IMPLANT_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_IMPLANT_TMQ2_<?php echo $i; ?>">
                                                 <input id="IMPLANT_TMQ2_<?php echo $i; ?>" name="IMPLANT_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-maroon TM" type="checkbox" 
                                                 <?php 
                                                	foreach($IMPLANT_TMQ2 as $IMPLANT_tm2)
													{
													   if($IMPLANT_tm2==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="IMPLANT_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                 <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="IMPLANT_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_IMPLANT('4','IMPLANT_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                 <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_IMPLANT_TMQ4_<?php echo $i; ?>">
                                                <input id="IMPLANT_TMQ4_<?php echo $i; ?>" name="IMPLANT_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-maroon TM" type="checkbox"
                                                <?php 
                                                	foreach($IMPLANT_TMQ4 as $IMPLANT_tm4)
													{
													   if($IMPLANT_tm4==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label for="IMPLANT_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="IMPLANT_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_IMPLANT('3','IMPLANT_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                              
                                </tr>
                                <tr>
                                 <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_IMPLANT_TMQ3_<?php echo $i; ?>">
                                                 <input id="IMPLANT_TMQ3_<?php echo $i; ?>" name="IMPLANT_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-maroon TM" type="checkbox" 
                                                 <?php 
                                                	foreach($IMPLANT_TMQ3 as $IMPLANT_tm3)
													{
													   if($IMPLANT_tm3==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="IMPLANT_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
            
              <div class="form-group" style="padding-top:50px;">
                <label>Do you have bridge work?</label>
               <div class="radio">
                  <input name="medical_history_bridge_work" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_bridge_work_yes" type="radio" class="radio-col-blue" 
	                  <?php 
	                  if ($this->input->post('medical_history_bridge_work') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['bridge_work'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_bridge_work_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_bridge_work" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_bridge_work_no" type="radio" class="radio-col-blue" 
                 <?php 
	                  if ($this->input->post('medical_history_bridge_work') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['bridge_work'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_bridge_work_no">No</label>              
                </div>
               <?php if (form_error('medical_history_bridge_work')) { echo form_error('medical_history_bridge_work'); } ?>
              </div>
              
              <div class="form-group">
               <div id="bridge-work-missing-teeth-data-area"></div>
               <table align="center" width="100%">
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                              <table width="100%">
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a title="Missing Teeth" href="javascript:void(0)" id="BW_TMQ1_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_BW('1','BW_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                  <?php } ?>
                                 </tr>
                                <tr>
                                  <?php for($i=16; $i>=9; $i--) { ?>
                                            
                                            <td height="24" class="TR_BW_TMQ1_<?php echo $i; ?>">
                                                <input id="BW_TMQ1_<?php echo $i; ?>" name="BW_TMQ1[]" value="<?php echo $i; ?>" class="chk-col-blue TM" type="checkbox"
                                                <?php 
                                                	foreach($BW_TMQ1 as $BW_tm1)
													{
													   if($BW_tm1==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label title="Teeth Movement" for="BW_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                  </tr>
                              </table>
                             </td>
                              <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                              <table width="95%" style="margin-left:5%;">
                                <tr>
                                   <?php for($i=8; $i>=1; $i--) { ?>
                                        
                                          <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="BW_TMQ2_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_BW('2','BW_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                  <?php } ?>
                                </tr>
                                <tr>
                                   <?php for($i=8; $i>=1; $i--) { ?>
                                            
                                            <td height="24" class="TR_BW_TMQ2_<?php echo $i; ?>">
                                                 <input id="BW_TMQ2_<?php echo $i; ?>" name="BW_TMQ2[]" value="<?php echo $i; ?>" class="chk-col-blue TM" type="checkbox"
                                                 <?php 
                                                	foreach($BW_TMQ2 as $BW_tm2)
													{
													   if($BW_tm2==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="BW_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                              <table width="100%" style="margin-top:15px;">
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="BW_TMQ4_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_BW('4','BW_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                        
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=17; $i<=24; $i++) { ?>
                                            
                                            <td height="24" class="TR_BW_TMQ4_<?php echo $i; ?>">
                                                <input id="BW_TMQ4_<?php echo $i; ?>" name="BW_TMQ4[]" value="<?php echo $i; ?>" class="chk-col-blue TM" type="checkbox"
                                                <?php 
                                                	foreach($BW_TMQ4 as $BW_tm4)
													{
													   if($BW_tm4==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                <label for="BW_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php } ?>
                                </tr>
                              </table>
                              </td>
                              <td width="52%" style="border-top:0px;">
                              <table width="95%" style="margin-left:5%; margin-top:15px;">
                                <tr>
                                 <?php for($i=25; $i<=32; $i++) { ?>
                                        
                                           <td width="12%" style="border:0px;"><a href="javascript:void(0)" id="BW_TMQ3_MISSING_<?php echo $i; ?>" onClick="return setMissingTeeth_BW('3','BW_TMQ',<?php echo $i; ?>)"><?php echo $i; ?></a></td>
                                 
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php for($i=25; $i<=32; $i++) { ?>
                                            
                                            <td height="24" class="TR_BW_TMQ3_<?php echo $i; ?>">
                                                 <input id="BW_TMQ3_<?php echo $i; ?>" name="BW_TMQ3[]" value="<?php echo $i; ?>" class="chk-col-blue TM" type="checkbox"
                                                 <?php 
                                                	foreach($BW_TMQ3 as $BW_tm3)
													{
													   if($BW_tm3==$i)
													   {
													   	 echo "checked";
													   }
													}
                                                 ?>>
                                                 <label for="BW_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                            </td>
                                            
                                  <?php }?>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
              </div>
              
              <div class="form-group" style="padding-top:50px;">
                <label>Have you visited dentist in past 6 months?</label>
                <div class="radio">
                  <input name="medical_history_visited_dentist_past_months" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_visited_dentist_past_months_yes" type="radio" class="radio-col-navy" 
                  <?php 
	                  if ($this->input->post('medical_history_visited_dentist_past_months') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['visited_dentist_past_months'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_visited_dentist_past_months_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_visited_dentist_past_months" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_visited_dentist_past_months_no" type="radio" class="radio-col-navy" 
                   <?php 
	                  if ($this->input->post('medical_history_visited_dentist_past_months') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['visited_dentist_past_months'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_visited_dentist_past_months_no">No</label>              
                </div>
                <?php if (form_error('medical_history_visited_dentist_past_months')) { echo form_error('medical_history_visited_dentist_past_months'); } ?>
              </div>
              
              <div class="form-group">
              	<?php 
				  		if ($this->input->post('submit') || $this->input->post('medical_history_other_problem_teeth')) {
				  			
									$other_problem_teeth = $this->input->post('medical_history_other_problem_teeth');	
							
						} else {
							
									$other_problem_teeth = $row['other_problem_teeth'];
						}
				 ?>
                <label>Do you have any other problems with you teeth?</label>
                <input type="text" class="form-control" name="medical_history_other_problem_teeth" value="<?php echo $other_problem_teeth; ?>" />
        	    <?php if (form_error('medical_history_other_problem_teeth')) { echo form_error('medical_history_other_problem_teeth'); } ?>
              </div>
              
              <div class="form-group">
              	<?php 
				  		if ($this->input->post('submit') || $this->input->post('medical_history_medications')) {
				  			
									$medications = $this->input->post('medical_history_medications');	
							
						} else {
							
									$medications = $row['medications'];
						}
				 ?>
                <label>Do you take any medications?</label>
                <input type="text" class="form-control" name="medical_history_medications" value="<?php 
                echo $medications; ?>" />
                <?php if (form_error('medical_history_medications')) { echo form_error('medical_history_medications'); } ?>
              </div>
              


              <div class="form-group">
              	<?php 
				  		if ($this->input->post('submit') || $this->input->post('medical_history_allergies')) {
				  			
									$allergies = $this->input->post('medical_history_allergies');	
							
						} else {
							
									$allergies = $row['allergies'];
						}
				 ?>
                <label>Do you have any allergies?</label>
                <input type="text" class="form-control" name="medical_history_allergies" value="<?php echo $allergies; ?>" />
                <?php if (form_error('medical_history_allergies')) { echo form_error('medical_history_allergies'); } ?>
              </div>
              
              <div class="form-group">
                <label>Are you pregnant?</label>
                <div class="radio">
                  <input name="medical_history_pregnant" value="<?php echo HARD_CODE_ID_YES; ?>" id="medical_history_pregnant_yes" type="radio" class="radio-col-navy" 
                  <?php 
	                  if ($this->input->post('medical_history_pregnant') == HARD_CODE_ID_YES) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['pregnant'] == HARD_CODE_ID_YES) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_pregnant_yes">Yes</label>      
                   &nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="medical_history_pregnant" value="<?php echo HARD_CODE_ID_NO; ?>" id="medical_history_pregnant_no" type="radio" class="radio-col-navy" 
                 <?php 
	                  if ($this->input->post('medical_history_pregnant') == HARD_CODE_ID_NO) 
	                  	{ 
	                  		echo 'checked="checked"'; 
	                  	}
	                  	elseif ($row['pregnant'] == HARD_CODE_ID_NO) 
	                  	{
	                  	 	echo 'checked="checked" '; 
	                  	} 
                  ?>>
                  <label for="medical_history_pregnant_no">No</label>              
                </div>
                <?php if (form_error('medical_history_pregnant')) { echo form_error('medical_history_visited_dentist_past_months'); } ?>
              </div>
              
              <div class="form-group">
                <label>Do you have any of the following diseases?</label>
                <?php 
					if ($diseases) {
						
						/*$medicalDiseases = $this->input->post('medical_history_diseases');*/
							
						foreach($diseases->result() as $disease) {
							
							
							/*if ($this->input->post('submit') || $this->input->post('medical_history_diseases')) 
							{
								 
								  if (in_array($disease->id,$medicalDiseases)) 
								  {

										  $checkedDiseases = 'checked';	
								  }
								   else 
								   {

										  $checkedDiseases = NULL;
								  }
							} 

							else 
							{*/

							$addedDiseases1	= $this->model_shared->getRecordMultipleWhereOrderBy('disease_id',MEDICAL_PATIENT_DISEASES_TABLE,array('is_deleted' => HARD_CODE_ID_NO,'case_id' => $row['id'],'disease_id ' => $disease->id),'id','ASC');

							   	
								 	
							/*}*/
							
				?>
						
						<div class="checkbox">
							<input type="checkbox" name="medical_history_diseases[]" id="disease_<?php echo $disease->id; ?>" 
							<?php 
									/*echo $checkedDiseases; */
									$medicalDiseases = $this->input->post('medical_history_diseases');
									if ($this->input->post('submit') || $this->input->post('medical_history_diseases'))
									{
										 
										  if (in_array($disease->id,$medicalDiseases)) 
										  {

												  echo "checked";
										  }
										   else 
										   {

												  $checkedDiseases = NULL;
										  }
									}

									else
									{
										foreach($addedDiseases1->result() as $ad)
										{
											$added_diseas=$ad->disease_id;
											if($added_diseas==$disease->id)
											{
												echo "checked";
											}
											else
											{
												$checkedDiseases = NULL;
											}
										}
									} 
							 
							 ?> 
							value="<?php echo $disease->id; ?>">
							<label for="disease_<?php echo $disease->id; ?>"><?php echo $disease->name; ?></label>
						</div>
                        <?php if (form_error('medical_history_diseases')) { echo form_error('medical_history_diseases'); } ?>
				
				<?php	
						}	
					}
				?>
                </div>



                 <div class="form-group row" style="margin-top: 140px;">
				  <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
				  <div class="col-sm-10 offset-md-2">
					 <button type="submit" name="submit" value="Save" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
                     <button type="submit" name="submit" value="Save & Close" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-open"></i> Save & Close</button>
                     <button type="button" class="btn btn-warning" onclick="window.location.href='<?php echo base_url(); ?>cases-list/'">Cancel</button>
				  </div>
				</div>


               
              </div>

            </div>
         </div>
       
      </div>











  </form> 
    </section>
    <!-- /.content -->
    
     <?php 
			if ($countrySelected) {
					
					echo '<script language="javascript" type="text/javascript">
								
								$(document).ready(function() {
									
									 $("#country").trigger("change");
								});
					 	  </script>';
				}
				
			if ($companySelected) {
					
					echo '<script language="javascript" type="text/javascript">
								
								getCaseTypes('.$companySelected.');
					 	  
						  </script>';
			}
	?>
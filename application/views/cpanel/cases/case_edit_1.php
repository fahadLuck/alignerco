<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>

<script language="javascript" type="text/javascript">
		
		 $(document).ready(function () {
		  
				  $("input[name='impression'").change(function() {
							
							var impression = $(this).val();
							
							$('input[name="impression_particulars"]').prop('checked', false); 
							
							if (impression == 1) {
								
								$('#impression_particulars-area').show();	
							
							} else {
								
								$('#impression_particulars-area').hide();		
							}
							
							return false;
							
				  });
				  
				  $('#country').change(function() {
							
							var country = $(this).val();
							
							if (country == 168) {
									
									$('#city-area').show();
									
									<?php  if ($caseUpdateFlag == false) { 
											echo "$('#city').prop('disabled', 'disabled');";
										  }
									?>
										
							} else {
									
									$('#city').val(null).trigger('change');
									$('#city-area').hide();	
							}
							
							//$('#case-type-area').hide();	
				  });
				  
				  $('#country').change(function() {
							
							var country 		= $(this).val();
							var selectedCompany = 0;
							
							<?php 
									
									if ($this->input->post('submit') || $this->input->post('company')) {
				  			
											$companySelected = $this->input->post('company');	
							
									} else {
							
											$companySelected = $row['distributor'];
									}
					 		
							?>
							
							<?php if ($companySelected) { ?>
							
								 selectedCompany = <?php echo $companySelected; ?>;
							
							<?php } ?>
							
								$.ajax({
									  type   	  : "POST",
									  dataType 	  : "HTML",
									  url		  : "<?php echo  base_url();?>load-companies-by-country/",
									  data		  : {  
									  						country 			: country,
									  						selectedCompany 	: selectedCompany
													}
									
										 }).done(function( rowData ) {
									
									  			if (rowData) {
						  								
														$('#company-area').html(rowData);
														
														<?php  if ($caseUpdateFlag == false) { 
																	echo "$('#company').prop('disabled', 'disabled');";
										  						}
														?>
						   						}
									  
								}); // END Ajax Request
							
				  });
				  
				  $(document).on('change', '#case_type', function() {
							
						var caseType 		= $(this).val();
						var caseTypeName	= $("#case_type :selected").text();
						
						if (caseType == 8 || caseType == 9) {
							  
							    $('#case_type_particulars').empty();
							  	$('#case_type_particulars').append('<option value="both">'+caseTypeName+'-Both (U,L)</option><option value="upper">'+caseTypeName+'-U</option><option value="upper">'+caseTypeName+'-L</option>'); 
								
								$('#case-class-area').show();
						
						} else {
						
								$('#case-class-area').hide();
							
						}
						
						
				  });
		 });
		 
		 function getCaseTypes(company) {
				
				if (company) {
				
					$('#case-type-area').show();	
						
							var selectedCaseType = 0;
							
							<?php 
									
									if ($this->input->post('submit') || $this->input->post('case_type')) {
				  			
											$caseTypeSelected = $this->input->post('case_type');	
							
									} else {
							
											$caseTypeSelected = $row['case_type'];
									}
					 		
							?>
							
							
							<?php if ($caseTypeSelected) { ?>
							
								 selectedCaseType = <?php echo $caseTypeSelected; ?>;
							
							<?php } ?>
							
								$.ajax({
									
									  type   	  : "POST",
									  dataType 	  : "HTML",
									  url		  : "<?php echo base_url();?>load-company-case-types/",
									  data		  : {  
									  						company 			: company,
									  						selectedCaseType 	: selectedCaseType
													}
									
										 }).done(function( rowData ) {
									
									  			if (rowData) {
						  								
														$('#case-types').html(rowData);
														$('#case-type-area').show();	
														
														if (selectedCaseType) {
															  
															  if (selectedCaseType == 8 || selectedCaseType == 9) {
																
																	$('#case-class-area').show();			
															  }
														}
														
														<?php  if ($caseUpdateFlag == false) { 
																	
																	echo "$('#case_type').prop('disabled', 'disabled');";
										 					   }
														?>
						   						}
									  
								}); // END Ajax Request
								
				} else {
								
						$('#case-type-area').hide();
						$('#case-types').val(null).trigger('change');	
				}
		 }

</script>
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>manage-cases/">Cases</a></li>
        <li class="breadcrumb-item active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      					<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
     
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="glyphicon glyphicon-pencil"></i> Case</h3>
      </div>
   	  
      <form id="frm" name="frm" action="<?php echo base_url(); ?>case-edit/<?php echo encodeString($row['id']); ?>" method="post" enctype="multipart/form-data">
       <input type="hidden" name="old_patient_name" value="<?php echo $row['patient']; ?>" />
       <input type="hidden" name="old_rx" value="<?php echo $row['RX_form']; ?>" />
       <input type="hidden" name="old_x_ray_opg" value="<?php echo $row['x_rays_opg']; ?>" />
       <input type="hidden" name="old_x_ray_ceph" value="<?php echo $row['x_rays_ceph']; ?>" />
       <input type="hidden" name="old_file_assessment" value="<?php echo $row['file_assessment']; ?>" />
									
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Patient Name</label>
				  <div class="col-sm-10">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('patient_name')) {
				  			
									$patientName = $this->input->post('patient_name');	
							
						} else {
							
									$patientName = $row['patient'];
						}
				   ?>
                    
                    <input name="patient_name" class="form-control" type="text" value="<?php echo $patientName; ?>" <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?> />
                    <?php if (form_error('patient_name')) { echo form_error('patient_name'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Age</label>
				  <div class="col-sm-10">
					
                   <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('age')) {
				  			
									$patientAge = $this->input->post('age');	
							
						} else {
							
									$patientAge = $row['age'];
						}
				   ?>
                    
                    <input name="age" class="form-control" type="number" min="1" value="<?php echo $patientAge; ?>">
                    <?php if (form_error('age')) { echo form_error('age'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Gender</label>
                    <div class="col-sm-10">
                      <div class="radio">
                      
                      <?php 
                    
                          if ($this->input->post('gender')) {
                              
                                      $gender = $this->input->post('gender');	
                              
                           } else {
                              
                                      $gender = $row['gender'];
                           }
                  ?>
                      
                      
                        <input name="gender" value="<?php echo HARD_CODE_ID_MALE; ?>" id="Option_8" type="radio" class="radio-col-purple" <?php if ($gender == HARD_CODE_ID_MALE) { echo 'checked="checked" '; } ?>>
                        <label for="Option_8">Male</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="gender" value="<?php echo HARD_CODE_ID_FEMALE; ?>" id="Option_9" type="radio" class="radio-col-purple" <?php if ($gender == HARD_CODE_ID_FEMALE) { echo 'checked="checked" '; } ?>>
                        <label for="Option_9">Female</label>              
                      </div>
                      
                      <?php if (form_error('gender')) { echo form_error('gender'); } ?>
                    </div>
                 </div>
                 
                 <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Nature Of Patient</label>
				  <div class="col-sm-10">
					
                   <?php 
                    
                          if ($this->input->post('nature_of_patient')) {
                              
                                      $natureOfPatient = $this->input->post('nature_of_patient');	
                              
                           } else {
                              
                                      $natureOfPatient = $row['nature_of_patient'];
                           }
                  ?>
                    
                    
                    <div class="radio">
                  	  <input name="nature_of_patient" value="Overseas" id="Option_1" type="radio" class="radio-col-yellow" <?php if ($natureOfPatient == 'Overseas') { echo 'checked="checked" '; } ?> <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
					  <label for="Option_1">Overseas</label>                    
                  </div>
                    <div class="radio">
                        <input name="nature_of_patient" value="Local" id="Option_2" type="radio" class="radio-col-yellow" <?php if ($natureOfPatient == 'Local') { echo 'checked="checked" '; } ?> <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                        <label for="Option_2">Local</label>                    
                    </div>
                    <?php if (form_error('nature_of_patient')) { echo form_error('nature_of_patient'); } ?>
                  </div>
				</div>
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Receive Date</label>
				  <div class="col-sm-10">
				  
                  <?php 
                    
                          if ($this->input->post('receive_date')) {
                              
                                      $receiveDate = $this->input->post('receive_date');	
                              
                           } else {
                              
                                      $receiveDate = $row['receive_date'];
                           }
                  ?>
				  
                  <input class="form-control" name="receive_date" type="date" value="<?php echo $receiveDate; ?>" id="" <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?> />
                  <?php if (form_error('receive_date')) { echo form_error('receive_date'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Doctor</label>
				  <div class="col-sm-10">
					
					 <?php 
                    
                          if ($this->input->post('doctor_id')) {
                              
                                      $doctorSelected = $this->input->post('doctor_id');	
                              
                              } else {
                              
                                      $doctorSelected = $row['doctor'];
                              }
                     ?>
                    
                    <select name="doctor_id" id="doctor_id" class="form-control select2" style="width: 100%;">
                    <option value="">Select a doctor</option>
 								<?php 
                                              if ($doctors->num_rows() > 0) {
                                                      
                                                      foreach($doctors->result() as $doctor) {
                                                          
                                                                    $doctorID			=	$doctor->id;
                                                                    $doctorName			=	$doctor->name;
																	$doctorEmail		=	$doctor->email;
																	$doctorPhone		=	$doctor->phone;
                                                                    
																	if ($doctorPhone) {
																		 
																		  $doctorName = $doctorName.' ['.$doctorPhone.']';
																	}
																    
																	if ($doctorEmail) {
																		 
																		  $doctorName = $doctorName.' ('.$doctorEmail.') ';
																	}
																	
																	if ($doctorSelected  == $doctorID) {
																	
																			$selectedDoctor = 'selected = selected';	
																	
																	}	else {
																			
																			$selectedDoctor = NULL;		
																	}
                                                                  
                                                                  echo '<option value="'.$doctorID.'" '.$selectedDoctor.'>'.$doctorName.'</option>';
                                                      }	
                                              }
                                ?>                 
                   </select>
                    <?php if (form_error('doctor_id')) { echo form_error('doctor_id'); } ?>
				  </div>
				</div>
                
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Impression Type</label>
				  <div class="col-sm-10">
                   <?php 
                    
                          if ($this->input->post('impression')) {
                              
                                      $impression = $this->input->post('impression');	
                              
                           } else {
                              
                                      $impression = $row['impression_type'];
                           }
							
						 if ($impressionTypes->num_rows() > 0) {
                                                      
                               	$counterImpression = 3;
							    foreach($impressionTypes->result() as $impressionType) {
					?>
                    			
                                 <div class="radio">
                                      <input name="impression" value="<?php echo $impressionType->id; ?>" id="Option_<?php echo $counterImpression; ?>" type="radio" class="radio-col-blue" <?php if ($impression == $impressionType->id) { echo 'checked="checked" '; } ?> <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                                      <label for="Option_<?php echo $counterImpression; ?>"><?php echo $impressionType->name; ?></label>                    
                                 </div>
                    
                    <?php	
				   					$counterImpression++;
								}
						 }
				    ?>
                    <?php if (form_error('impression')) { echo form_error('impression'); } ?>
                  </div>
				</div>
                
                
                <div class="form-group row" id="impression_particulars-area" style="display:none;">
                   
                   <?php 
				   		
						 if ($this->input->post('impression_particulars')) {
                              
                                      $impressionParticulars = $this->input->post('impression_particulars');	
                              
                           } else {
                              
                                      $impressionParticulars = $row['impression_particulars'];
                           }
						
				   ?>
                   
                    <label for="" class="col-sm-2 col-form-label">Impression</label>
                    <div class="col-sm-10">
                      <div class="radio">
                        <input name="impression_particulars" value="impression" id="Option_19" type="radio" class="radio-col-teal" <?php if ($impressionParticulars == 'impression') { echo 'checked="checked" '; } ?> <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                        <label for="Option_19">Impression</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="impression_particulars" value="cast_model" id="Option_20" type="radio" class="radio-col-teal" <?php if ($impressionParticulars == 'cast_model') { echo 'checked="checked" '; } ?> <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                        <label for="Option_20">Cast Model</label>              
                      </div>
                      
                      <?php if (form_error('impression_particulars')) { echo form_error('impression_particulars'); } ?>
                    </div>
              </div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Arch (es)</label>
				  <div class="col-sm-10">
					  
                   <?php 
                    
                          if ($this->input->post('submit') || $this->input->post('arch_upper')) {
                              
                                      $archUpper = $this->input->post('arch_upper');	
                              
                           } else {
                              
                                      $archUpper = $row['arch_upper'];
                           }
						   
						   if ($this->input->post('submit') || $this->input->post('arch_lower')) {
                              
                                      $archLower = $this->input->post('arch_lower');	
                              
                           } else {
                              
                                      $archLower = $row['arch_lower'];
                           }
                  ?>
                      
                      <div class="demo-checkbox">
                          <input id="md_checkbox_1" name="arch_upper" value="<?php echo HARD_CODE_ID_YES ?>" class="chk-col-green" type="checkbox" <?php if ($archUpper == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?> <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                          <label for="md_checkbox_1">Upper</label>
                          
                          <input id="md_checkbox_2" name="arch_lower" value="<?php echo HARD_CODE_ID_YES ?>" class="chk-col-green" type="checkbox" <?php if ($archLower == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?> <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                          <label for="md_checkbox_2">Lower</label>
                      </div>                 
                   <?php if (form_error('arch')) { echo form_error('arch'); } ?>
                  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Radio Graphs</label>
				  <div class="col-sm-10">

					  <?php 
                    
                          if ($this->input->post('submit') || $this->input->post('radio_graph_opg')) {
                              
                                      $radioGraphOPG = $this->input->post('radio_graph_opg');	
                              
                           } else {
                              
                                      $radioGraphOPG = $row['radio_graphs_opg'];
                           }
						   
						   if ($this->input->post('submit') || $this->input->post('radio_graph_ceph')) {
                              
                                      $radioGraphCEPH = $this->input->post('radio_graph_ceph');	
                              
                           } else {
                              
                                      $radioGraphCEPH = $row['radio_graphs_ceph'];
                           }
                  ?>
                    

                      <div class="demo-checkbox">
                         <input id="md_checkbox_3" name="radio_graph_opg" value="<?php echo HARD_CODE_ID_YES; ?>" class="chk-col-red" type="checkbox" <?php if ($radioGraphOPG == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                         <label for="md_checkbox_3">OPG</label>
                            
                          <input id="md_checkbox_4" name="radio_graph_ceph" value="<?php echo HARD_CODE_ID_YES; ?>" class="chk-col-red" type="checkbox" <?php if ($radioGraphCEPH ==  HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                          <label for="md_checkbox_4">Ceph</label>
                      </div>                 
                   <?php if (form_error('radio_graph')) { echo form_error('radio_graph'); } ?>
                  </div>
				</div>
                
                <div class="form-group row">
                	
                  <?php 
				   		
						 if ($this->input->post('bite')) {
                              
                                      $bite = $this->input->post('bite');	
                              
                           } else {
                              
                                      $bite = $row['bite'];
                           }
						
				   ?>
                   
                    <label for="" class="col-sm-2 col-form-label">Bite Registration</label>
                    <div class="col-sm-10">
                      <div class="radio">
                        <input name="bite" value="<?php echo HARD_CODE_ID_YES; ?>" id="Option_12" type="radio" class="radio-col-purple" <?php if ($bite == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                        <label for="Option_12">Yes</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="bite" value="<?php echo HARD_CODE_ID_NO; ?>" id="Option_13" type="radio" class="radio-col-purple" <?php if ($bite == HARD_CODE_ID_NO) { echo 'checked="checked" '; } ?>>
                        <label for="Option_13">No</label>              
                      </div>
                      
                      <?php if (form_error('bite')) { echo form_error('bite'); } ?>
                    </div>
              </div>
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Workable Upper</label>
                    
                    <?php 
				   		
						 if ($this->input->post('impressions_workable_upper')) {
                              
                                      $impressionsWorkableUpper = $this->input->post('impressions_workable_upper');	
                              
                           } else {
                              
                                      $impressionsWorkableUpper = $row['impression_workable_upper'];
                           }
						
				   ?>
                    
                    <div class="col-sm-10">
                      <div class="radio">
                        <input name="impressions_workable_upper" value="<?php echo HARD_CODE_ID_YES; ?>" id="Option_14" type="radio" class="radio-col-yellow" <?php if ($impressionsWorkableUpper == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                        <label for="Option_14">Yes</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="impressions_workable_upper" value="<?php echo HARD_CODE_ID_NO; ?>" id="Option_15" type="radio" class="radio-col-yellow" <?php if ($impressionsWorkableUpper == HARD_CODE_ID_NO) { echo 'checked="checked" '; } ?>>
                        <label for="Option_15">No</label>              
                      </div>
                      
                      <?php if (form_error('impressions_workable_upper')) { echo form_error('impressions_workable_upper'); } ?>
                    </div>
                 </div>
                 
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Workable Lower</label>
                    
                   <?php 
				   		
						 if ($this->input->post('impression_workable_lower')) {
                              
                                      $impressionsWorkableLower = $this->input->post('impression_workable_lower');	
                              
                           } else {
                              
                                      $impressionsWorkableLower = $row['impression_workable_lower'];
                           }
						
				   ?>
                    
                    <div class="col-sm-10">
                      <div class="radio">
                        <input name="impressions_workable_lower" value="<?php echo HARD_CODE_ID_YES; ?>" id="Option_16" type="radio" class="radio-col-navy" <?php if ($impressionsWorkableLower == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?>>
                        <label for="Option_16">Yes</label>      
                       	 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="impressions_workable_lower" value="<?php echo HARD_CODE_ID_NO; ?>" id="Option_17" type="radio" class="radio-col-navy" <?php if ($impressionsWorkableLower == HARD_CODE_ID_NO) { echo 'checked="checked" '; } ?>>
                        <label for="Option_17">No</label>              
                      </div>
                      
                      <?php if (form_error('impressions_workable_lower')) { echo form_error('impressions_workable_lower'); } ?>
                    </div>
                 </div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Intra & Extra Oral Pictures</label>
				  <div class="col-sm-10">
					  
                     <?php 
				   		
						 if ($this->input->post('intra_extra_oral_pictures')) {
                              
                                      $intraExtraOralPictures = $this->input->post('intra_extra_oral_pictures');	
                              
                           } else {
                              
                                      $intraExtraOralPictures = $row['intra_extra_oral_pictures'];
                           }
						
				   ?>
                      
                      <select name="intra_extra_oral_pictures" id="intra_extra_oral_pictures" class="form-control select2" style="width: 100%;">
                       <option value="">Select a option</option>
                       <option value="<?php echo HARD_CODE_ID_YES; ?>" <?php if ($intraExtraOralPictures == HARD_CODE_ID_YES) { echo 'selected=selected'; } ?>>Yes</option>
                        <option value="<?php echo HARD_CODE_ID_NO; ?>" <?php if ($intraExtraOralPictures == HARD_CODE_ID_NO) { echo 'selected=selected'; } ?>>No</option>
                      </select>
                     <?php if (form_error('intra_extra_oral_pictures')) { echo form_error('intra_extra_oral_pictures'); } ?>
                  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Country</label>
				  <div class="col-sm-10">
					
                    <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('country')) {
				  			
									$countrySelected = $this->input->post('country');	
							
						} else {
							
									$countrySelected = $row['country'];
						}
				   ?>
                    
                    <select name="country" id="country" class="form-control select2" style="width: 100%;" <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                     <?php 
					 		if ($countries->num_rows() > 0) {
                                                      
                                                      foreach($countries->result() as $country) {
                                                          
                                                                  $countryID			=	$country->id;
                                                                  $countryName			=	$country->name;
                                                                      
                                                                      if ($countrySelected == $countryID) {
                                                                      
                                                                              $selectedCountry = 'selected = selected';	
                                                                      
                                                                      }	else {
                                                                              
                                                                              $selectedCountry = NULL;		
                                                                      }
                                                                  
                                                                  echo '<option value="'.$countryID.'" '.$selectedCountry.'>'.$countryName.'</option>';
                                                      }	
                                              }
					 ?>
                    </select>
                    <?php if (form_error('country')) { echo form_error('country'); } ?>
                  </div>
				</div>
                
                <div id="city-area" <?php if ($countrySelected == 168) { echo 'style="display:block;"'; } else { echo 'style="display:none;"';} ?>>
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">City</label>
				  <div class="col-sm-10">
					
                    <select name="city" id="city" class="form-control select2" style="width: 100%;">
                     <option value="">Select a city</option>
					 <?php 
					
						if ($this->input->post('submit') || $this->input->post('city')) {
				  			
									$citySelected = $this->input->post('city');	
							
						} else {
							
									$citySelected = $row['city'];
						}
							
						if ($cities->num_rows() > 0) {
												  
								  foreach($cities->result() as $city) {
									  
											  $cityID				=	$city->id;
											  $cityName				=	$city->name;
												  
												  if ($citySelected == $cityID) {
												  
														  $selectedCity = 'selected = selected';	
												  
												  }	else {
														  
														  $selectedCity = NULL;		
												  }
											  
											  echo '<option value="'.$cityID.'" '.$selectedCity.'>'.$cityName.'</option>';
								  }	
						  }
					 ?>
                   </select>
                    <?php if (form_error('city')) { echo form_error('city'); } ?>
                  </div>
				</div>
                </div>
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Distributor</label>
				  <div class="col-sm-10">
				   <div id="company-area">
                    <select name="company" id="company" class="form-control select2" style="width: 100%;" onchange="return getCaseTypes(this.value);" <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                    <option value="">Select a company</option>
					 <?php 
					 		if ($this->input->post('submit') || $this->input->post('company')) {
				  			
									$companySelected = $this->input->post('company');	
							
							} else {
							
									$companySelected = $row['distributor'];
							}
					 		
							if ($companies->num_rows() > 0) {
                                                      
									foreach($companies->result() as $company) {
										
												$companyID				=	$company->id;
												$companyName			=	$company->name;
													
													if ($companySelected == $companyID) {
													
															$selectedCompany = 'selected = selected';	
													
													}	else {
															
															$selectedCompany = NULL;		
													}
												
												echo '<option value="'.$companyID.'" '.$selectedCompany.'>'.$companyName.'</option>';
									}	
							}
  						 ?>
                    </select>
                  </div>
                   <?php if (form_error('company')) { echo form_error('company'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" id="case-type-area" style="display:none;">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Case Type</label>
				  <div class="col-sm-10">
				   <div id="case-types"></div>
                   <?php if (form_error('case_type')) { echo form_error('case_type'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" id="case-class-area" style="display:none;">
                    <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                    <div class="col-sm-10">
                    	<?php 
								
								if ($this->input->post('case_type')){
										
										$caseType = $this->input->post('case_type');	
								
								} else {
										
										$caseType = $row['case_type'];
											
								}
								
								if ($caseType == 8) {
										
										$upperName = 'CPB-U';
										$lowerName = 'CPB-L';	
										$bothName  = 'CPB- Both (U,L)';	
								
								} else if ($caseType == 9) {
										
										$upperName = 'CPE-U';
										$lowerName = 'CPE-L';
										$bothName  = 'CPB-Both (U,L)';		
								
									
								}
								
								
								if ($this->input->post('case_type_particulars')){
										
										$caseTypeParticulars = $this->input->post('case_type_particulars');	
								
								} else {
										
										$caseTypeParticulars = $row['case_type_particulars'];
											
								}
								
						?>
                    
                       <select name="case_type_particulars" id="case_type_particulars" class="form-control select2 type-class" style="width: 100%;" <?php if ($caseUpdateFlag == false) { echo 'disabled'; } ?>>
                        <option value="both" <?php if ($caseTypeParticulars == 'both') { echo 'selected = selected'; } ?>><?php echo $bothName; ?></option>
                        <option value="upper" <?php if ($caseTypeParticulars == 'upper') { echo 'selected = selected'; } ?>><?php echo $upperName; ?></option>
                        <option value="lower" <?php if ($caseTypeParticulars == 'lower') { echo 'selected = selected'; } ?>><?php echo $lowerName; ?></option>
                       </select>
                      
                      <?php if (form_error('case_type_particulars')) { echo form_error('case_type_particulars'); } ?>
                    </div>
                 </div>
                 
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">RX Form</label>
				  <div class="col-sm-10">
					<input id="file_data_rx" name="file_data_rx" type="file">
 					<?php if (form_error('file_data_rx')) { echo ' <p class="help-block text-red">'.form_error('file_data_rx').'</p>'; } ?> 
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">X-rays (OPG)</label>
				  <div class="col-sm-10">
					<input id="file_data_x_ray_opg" name="file_data_x_ray_opg" type="file">
 					<?php if (form_error('file_data_x_ray_opg')) { echo ' <p class="help-block text-red">'.form_error('file_data_x_ray_opg').'</p>'; } ?> 
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">X-rays (Ceph)</label>
				  <div class="col-sm-10">
					<input id="file_data_x_ray_ceph" name="file_data_x_ray_ceph" type="file">
 					<?php if (form_error('file_data_x_ray_ceph')) { echo ' <p class="help-block text-red">'.form_error('file_data_x_ray_ceph').'</p>'; } ?> 
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Assessment File</label>
				  <div class="col-sm-10">
					<input id="file_data_assessment" name="file_data_assessment" type="file">
 					<?php if (form_error('file_data_assessment')) { echo ' <p class="help-block text-red">'.form_error('file_data_assessment').'</p>'; } ?> 
				  </div>
				</div>
               
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Priority</label>
				  <div class="col-sm-10">
					
                    <?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('priority')) {
				  			
									$priority = $this->input->post('priority');	
							
						} else {
							
									$priority = $row['priority'];
						}
				   ?>
                    
                    <div class="radio">
                  	  <input name="priority" value="<?php echo PRIORITY_HIGH; ?>" id="Option_5" type="radio" class="radio-col-red" <?php if ($priority == PRIORITY_HIGH) { echo 'checked="checked" '; } ?>>
					  <label for="Option_5">High</label>                    
                  	</div>
                    
                    <div class="radio">
                  	  <input name="priority" value="<?php echo PRIORITY_MEDIUM; ?>" id="Option_6" type="radio" class="radio-col-yellow" <?php if ($priority == PRIORITY_MEDIUM) { echo 'checked="checked" '; } ?>>
					  <label for="Option_6">Medium</label>                    
                  	</div>
                    
                    <div class="radio">
                  	  <input name="priority" value="<?php echo PRIORITY_LOW; ?>" id="Option_7" type="radio" class="radio-col-green" <?php if ($priority == PRIORITY_LOW) { echo 'checked="checked" '; } ?>>
					  <label for="Option_7">Low</label>                    
                  	</div>
                     
                    <?php if (form_error('priority')) { echo form_error('priority'); } ?>
                  </div>
				</div>
                
                <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Remarks / Comments</label>
				  <div class="col-sm-10">
					
					<?php 
				  
				  		if ($this->input->post('submit') || $this->input->post('comments')) {
				  			
									$description = $this->input->post('comments');	
							
						} else {
							
									$description = $row['description'];
						}
				   ?>
                    
                    <textarea name="comments" class="form-control" rows="3" placeholder=""><?php echo $description; ?></textarea>
                    <?php if (form_error('comments')) { echo form_error('comments'); } ?>
				  </div>
				</div>
                
                <?php if ($row['RX_form'] || $row['x_rays_opg'] || $row['x_rays_ceph'] || $row['file_assessment'] || $row['stl_1'] || $row['stl_2']) { ?>  
               
                 <!-- fancybox -->
				<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend_assets/vendor_components/lightbox-master/dist/ekko-lightbox.css" />
     
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Attachments</label>
				  <div class="col-sm-10">
					<div class="timeline-body">
						  <?php if ($row['RX_form']) { ?> 
                       			 <a href="<?php echo CASE_RX_FORM_URL_PATH.$row['RX_form']; ?>" data-toggle="lightbox" data-title="RX Form" data-gallery="multiimages"><img data-toggle="tooltip" title="" data-original-title="RX Form" src="<?php echo CASE_RX_FORM_URL_PATH.'thumbnail/thumbnail_'.$row['RX_form']; ?>" alt="" class="margin" /></a>
                 		  <?php } ?>
                         
                          <?php if ($row['x_rays_opg']) { ?> 
                       			 <a href="<?php echo CASE_X_RAY_OPG_URL_PATH.'/'.$row['x_rays_opg']; ?>" data-toggle="lightbox" data-title="X-rays (OPG)" data-gallery="multiimages"><img data-toggle="tooltip" title="" data-original-title="X-rays (OPG)" src="<?php echo CASE_X_RAY_OPG_URL_PATH.'/thumbnail/thumbnail_'.$row['x_rays_opg']; ?>" alt="" class="margin" /></a>
                 		  <?php } ?>
                          
                          <?php if ($row['x_rays_ceph']) { ?> 
                       			 <a href="<?php echo CASE_X_RAY_CEPH_URL_PATH.'/'.$row['x_rays_ceph']; ?>" data-toggle="lightbox" data-title="X-rays (Ceph)" data-gallery="multiimages"><img data-toggle="tooltip" title="" data-original-title="X-rays (Ceph)" src="<?php echo CASE_X_RAY_CEPH_URL_PATH.'/thumbnail/thumbnail_'.$row['x_rays_ceph']; ?>" alt="" class="margin" /></a>
                 		  <?php } ?>
                          
                          <?php if ($row['file_assessment']) { ?> 
                       			 <a href="<?php echo CASE_FILE_ASSESSMENT_URL_PATH; ?><?php echo $row['file_assessment']; ?>"><img data-toggle="tooltip" title="" data-original-title="Case Assessment File" src="<?php echo base_url() ?>backend_images/document_icon.png" alt="" class="margin" /></a>
                 		  <?php } ?>
                          
                          <?php if ($row['stl_1']) { ?> 
                       			 <a href="<?php echo CASE_FILE_STL_URL_PATH; ?><?php echo $row['stl_1']; ?>"><img data-toggle="tooltip" title="" data-original-title="STL File (Upper)" src="<?php echo base_url() ?>backend_images/document_icon.png" alt="" class="margin" /></a>
                 		  <?php } ?>
                          
                          <?php if ($row['stl_2']) { ?> 
                       			 <a href="<?php echo CASE_FILE_STL_URL_PATH; ?><?php echo $row['stl_2']; ?>"><img data-toggle="tooltip" title="" data-original-title="STL File (Lower)" src="<?php echo base_url() ?>backend_images/document_icon.png" alt="" class="margin" /></a>
                 		  <?php } ?>
                          
                          
						</div>
				  </div>
				</div>
                
			    <?php } ?>
                
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
				  <div class="col-sm-10 offset-md-2">
					 <button type="submit" name="submit" value="Save" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
                     <button type="submit" name="submit" value="Save & Close" class="btn btn-blue"><i class="glyphicon glyphicon-floppy-open"></i> Save & Close</button>
                     <button type="button" class="btn btn-warning" onclick="window.location.href='<?php echo base_url(); ?>cases-listing/'">Cancel</button>
				  </div>
				</div>
                
                
				
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
     
      </form>   
        
        
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
  
    </section>
    <!-- /.content -->
    
     <?php 
			
			if (($impression && $impression == CASE_IMPRESSION_TYPE_PHYSICAL)) {
			   		
					 echo '<script language="javascript" type="text/javascript">
					  
								  $(document).ready(function() {
									  
									  $("#impression_particulars-area").show();
								  });
							
							</script>';
		   }
			
			if ($countrySelected) {
					
					echo '<script language="javascript" type="text/javascript">
								
								$(document).ready(function() {
									
									 $("#country").trigger("change");
								});
					      </script>';
				}
				
			if ($companySelected) {
					
					echo '<script language="javascript" type="text/javascript">
								
								getCaseTypes('.$companySelected.');
					 	  
						  </script>';
			}
			
	?>
<?php error_reporting(0); ?>
<script language="javascript" type="text/javascript">
    
 $(document).ready(function () {
  
  var totalCases = <?php echo $totalCases; ?>;
      
      if (totalCases) {

        $('#waiting-for-approval-frm-area').show(); 
        
      } else {
        
        $('#waiting-for-approval-frm-area').hide(); 
      }
      
  
  $(document).on( "click", ".waiting_check_box_case", function() {
          /*alert('test');*/
        
        var caseID     = $(this).val();
        var caseNumber = $(this).attr('key'); 
        
         if ($(this).prop('checked')==true){ 
         
          $('#waiting_checkboxes').append('<input type="checkbox" id="case_'+caseNumber+'" name="list_case[]" value="'+caseID+'" checked />');
         
         } else {
            
            $('#case_'+caseNumber).remove();
         }
        
        var length = $('input[name="waiting_list_case_checkbox[]"]:checked').length;
        
        if (length > 0) {
            
            
            
            $('#save-btn-area').attr('type','button');
            $('#save-btn-area').removeClass('disabled');
        } else{
            
            $('#save-btn-area').attr('type','button');
            $('#save-btn-area').addClass('disabled');
        }
        
      }); 
      
      
      $(document).on( "click", "#save-btn-area", function() {
        
        var length = $('input[name="waiting_list_case_checkbox[]"]:checked').length;
        
        var link = $('#link').val();
        var code = $('#code').val();
        var doctor = $('#doctor').val();
        
        /*if (length > 0 && link != '' && code != '' && doctor != '') {*/
          if (length > 0 ) {
          
          $( "#frm-case-assign-to-doctor").submit();
          return true;
        
        } else {
          
          return false; 
        }
        
      }); 
 
  });
  

</script>






<script>
$(document).ready(function(){
    $('#doctor').change(function(){

        /*var caseID     = $(this).val();
        var caseNumber = $(this).attr('key');*/ 
         doctorID=$(this).val();
        
         $.ajax({
                 url:"<?php echo base_url() ?>get-doctors",
                 method:"post",
                // dataType: 'json',
                data:{doctorID :doctorID},
                // dataType: 'json',
                success: function(data)
                {
                  /*alert(doctorID);*/
                  $("#stat").html(data);   
                },
                 error: function(xhr, status, error) 
                 {
                     // check status && error
                     alert("this is error "+error);
                  },
            });

         });
        
      });  
</script>




    <div class="box box-default" id="waiting-for-approval-frm-area">
        <div class="box-header with-border">
          <h3 class="box-title">Assign Case To Dentist</h3>
        </div>
        <!-- /.box-header -->
      
      <form id="frm-case-assign-to-doctor" name="frm" action="<?php echo base_url(); ?>case-assign-to-doctor/" method="post" enctype="multipart/form-data" autocomplete="off">
        <div id='waiting_checkboxes'></div>
        <div class="box-body">
          <div class="row">

           <!--  <div class="col-md-3 col-6">
              <div class="form-group">
                <label>Link</label> -->
                <input name="link" id="link" class="form-control" type="hidden" value="<?php echo $this->input->post('link'); ?>" id="example-text-input" placeholder="Enter Link Here">
              <!--   <?php //if (form_error('link')) { echo form_error('link'); } ?>
              </div>
            </div> -->

            <!-- <div class="col-md-2 col-6">
              <div class="form-group">
                <label>Code</label> -->
                <input name="code" id="code" class="form-control" type="hidden" value="<?php echo $this->input->post('code'); ?>" id="example-text-input" placeholder="Enter Code Here">
                <!-- <?php //if (form_error('code')) { echo form_error('code'); } ?>
              </div>
            </div> -->

            <div class="col-md-7 col-6">
              <div class="form-group">
                <label>Dentist</label>
                <select name="doctor" id="doctor" class="form-control select2" style="width: 100%;">
                      <option value="">Select Dentist</option>
                          <?php 
                                if ($employees->num_rows() > 0) 
                                {
                                        
                                    foreach($employees->result() as $doctor) 
                                    {
                                      
                                      $doctorID  = $doctor->employeeID;
                                      $doctorName  = $doctor->employeeName;
                                        
                                        if ($this->input->post('doctor') == $doctorID) {
                                        
                                            $selectedService = 'selected = selected'; 
                                        
                                        } else {
                                            
                                            $selectedService = NULL;    
                                        }
                                          
                                          echo '<option value="'.$doctorID.'" '.$selectedService.'>'.$doctorName.'</option>';
                                      } 
                                }
                           ?>                 
                      
                   </select>
              </div>
            </div>


            <div class="col-md-5 col-6" id="stat">
              
            </div>

          </div>
      
           <div class="pull-right">
              <button id="save-btn-area" style="display:block;" type="button" onClick="retun abc();" class="btn btn-blue disabled pull-right"><i class="fa fa-send"></i> Assign</button>
           </div> 
           
        </div>
      </form>   
  </div>







<div class="row">
        <div class="col-xl-12">
           <div class="box">
             <div class="box-header">
              <h3 class="box-title"><i class='fa fa-medkit'></i>  Cases (<?php echo number_format($totalCases); ?>) </h3>

               <!-- <i class='fa fa-filter' class="btn btn-default" data-toggle="modal" data-target="#modal-default" style="float: right;"></i> -->

               <a style="float: right;" href="JavaScript:Void(0);" data-target=".searchCasesPopUp" data-toggle="modal"><i class="fa fa-filter" data-toggle="tooltip" title="" data-original-title="Filters"></i></a>

          </div>


             <div class="box-body">
            <div class="table-responsive">
                  <table class="table table-responsive table-striped" style="width:100%">
                    <tbody>
                    <tr>
                      <th>&nbsp;</th>
                      <th> ID</th>
                      <th>Patient </th>
                      <th>Stage </th>
                      <th>Status	</th>
                      <th>Dentist Case Status</th>
                      <th>Status Date</th>
                      <th>Date</th>
                      <th>&nbsp;</th>
                     
                     <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
                                              
                      <th width="1%">&nbsp;</th>
                      
                     <?php } ?>
                     
                      </tr>
                  
                      <?php if ($cases) {
                                   
                          foreach($cases->result() as $case) {
                              
                              $caseID 							   = $case->caseID;
                              $patientName 						 = $case->patientName;
                              $companyName 			 			 = $case->companyName;
                              $receiveDate 			 			 = $case->receiveDate;
                              
                              $holdFlag 							 = $case->caseHoldFlag;
                              $shippedFlag 						 = $case->caseShippedFlag;
                              
                              $productionStageID		 	 = $case->productionStageID;
                              $productionStageName 	 	 = $case->productionStageName;
                              
                              $caseStatusCreated 			 = $case->caseStatusCreated;
                              $statusID 				 			 = $case->statusID;
                              $statusName 	 					 = $case->statusName;
                              $doctorCaseStatusNames   = $case->doctorCaseStatusName;
                              if($doctorCaseStatusNames=='Pending')
                              {
                                $doctorCaseStatusName  = 'Assign';
                              }
                              else
                              {
                                $doctorCaseStatusName  = $doctorCaseStatusNames;
                              }
                              
                              $caseStatusCreatedDate				 = date('F d, Y',$caseStatusCreated);
                              $caseStatusCreatedTime		 		 = date('h:i A',$caseStatusCreated);
                              $caseStatusCreatedTimeAgo			 = timeAgo($caseStatusCreated).' ago'; // Calling From Shared Helper

                              $doctorCaseHistory          = $this->model_case->getDoctorCaseHistory($caseID)->row_array();
                              if($doctorCaseHistory)
                              {
                                   $doctorCaseHistoryCreated     = $doctorCaseHistory['doctorCaseHistoryCreated'];
                                  $doctorCaseHistoryCreatedBy   = $doctorCaseHistory['doctorCaseHistoryCreatedBy'];

                                  $caseHistoryCreatorAssignJobs   = getEmployeeAssignJobs($doctorCaseHistoryCreatedBy); // Calling From HR Employees Helper

                                  $doctorCaseHistoryCreatedDate     = date('d M. Y',$doctorCaseHistoryCreated);
                                  $doctorCaseHistoryCreatedTime     = date('h:i A',$doctorCaseHistoryCreated);
                                  $doctorCaseHistoryCreatedTimeAgo    = timeAgo($doctorCaseHistoryCreated).' ago';
                              }



                              $doctorAssignedCaseHistory          = $this->model_case->getAssignedDoctorCaseHistory($caseID)->row_array();
                              if($doctorAssignedCaseHistory)
                              {
                                   $doctorAssignedCaseHistoryCreated     = $doctorAssignedCaseHistory['doctorCaseHistoryCreated'];
                                  $doctorAssignedCaseHistoryCreatedBy   = $doctorAssignedCaseHistory['doctorCaseHistoryCreatedBy'];

                                  $doctorAssignedCaseHistoryCreatedDate     = date('d M. Y',$doctorAssignedCaseHistoryCreated);
                                  $doctorAssignedCaseHistoryCreatedTime     = date('h:i A',$doctorAssignedCaseHistoryCreated);
                                  $doctorAssignedCaseHistoryCreatedTimeAgo    = timeAgo($doctorAssignedCaseHistoryCreated).' ago';
                              }

                             

                              $assignCount=$case->totalDoctorCases;
                              
                              
                              if ($receiveDate) {
                                          
                                  $receiveDate = date('d M Y',strtotime($receiveDate));
                              }
                              
                              if (empty($productionStageID)) {
                                          
                                  $productionStageName = '-----';
                              }
                              
                              if (empty($statusID)) {
                                          
                                  $statusName = '-----';
                              }
                              
                              $link = base_url().'case/'.encodeString($caseID);
                              
                              if ($statusID == CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY) {
                                  
                                  $link = base_url().'kit-send-to-customer/'.encodeString($caseID);
                              }
                              
                          ?>
                                           <tr>

                                            <td>
                                               <?php if ($statusID  == CASE_STATUS_WAITING_FOR_APPROVAL_WAITING AND $assignCount==0) { ?>
                                                   <input name="waiting_list_case_checkbox[]" value="<?php echo encodeString($caseID); ?>" key="<?php echo $caseID; ?>" type="checkbox" id="box_<?php echo $caseID; ?>" class="filled-in chk-col-yellow waiting_check_box_case">
                                                   <label for="box_<?php echo $caseID; ?>"></label>
                                                <?php } ?>
                                            </td>



                                              <td height="59"><span data-toggle="tooltip" title="" data-original-title="ID"><a target="_blank" href="<?php echo $link;?>"><?php echo $caseID; ?></a></span></td>
                                              <td>
                                                <span data-toggle="tooltip" title="" data-original-title="Patient Name" style="font-size:12px;"><a target="_blank" href="<?php echo $link;?>"><strong><?php echo $patientName; ?></strong></a></span>
                                                  <br />
                                                <span data-toggle="tooltip" title="" data-original-title="Distributor"><a target="_blank" href="<?php echo $link;?>"><?php echo $companyName; ?></a></span>
                                             </td>
                                              <td><span data-toggle="tooltip" title="" data-original-title="Production Stage"><a target="_blank" href="<?php echo $link;?>"><?php echo $productionStageName; ?></a></span></td>

                                              <td><span data-toggle="tooltip" title="" data-original-title="Case Status"><a target="_blank" href="<?php echo $link;?>"><?php echo $statusName; ?> <?php if ($holdFlag == HARD_CODE_ID_YES) { echo '(HOLD)'; } ?></a></span></td>


                                               <td><span data-toggle="tooltip" title="" data-original-title="Case Status"><a target="_blank" href="<?php echo $link;?>">
                                                  <?php 
                                                      if($assignCount==0) 
                                                      {
                                                         ?><span style="color: red;">Not Assigned To Doctor</span><?php 
                                                      }
                                                      elseif($doctorCaseStatusName=='Modified')
                                                      {
                                                        ?><span style="color: orange;"><?= $doctorCaseStatusName; ?>
                                                        </span><?php 
                                                      }
                                                      else
                                                      {
                                                        ?><span style=""><?= $doctorCaseStatusName; ?>
                                                        </span><?php 
                                                      }
                                                  ?>
                                               </a></span></td>

                                               <td>
                                                  <?php
                                                    if($assignCount==0) 
                                                    {
                                                        
                                                    }
                                                    else
                                                    {
                                                         ?><span>
                                                        (<?php echo $doctorAssignedCaseHistoryCreatedDate; ?> <br><?php echo $doctorAssignedCaseHistoryCreatedTime; ?>)
                                                        <br>-<br>
                                                        (<?php echo $doctorCaseHistoryCreatedDate; ?> <br><?php echo $doctorCaseHistoryCreatedTime; ?>)
                                                        </span><?php
                                                    }
                                                  ?>    
                                               </td>


                                             <td>
                                                  <span data-toggle="tooltip" title="" data-original-title="Case Received Date">
                                                  <a target="_blank" href="<?php echo $link;?>"><?php echo $receiveDate; ?></a>
                                                 </span>
                                             </td>
                                             <td>
                                               <div class="pull-right">
                                                    <span style="font-size:12px;" data-toggle="tooltip" title="" data-original-title="<?php echo $caseStatusCreatedDate; ?> <?php echo $caseStatusCreatedTime; ?>"><a target="_blank" href="<?php echo $link;?>"><i class="fa fa-clock-o"></i> <?php echo $caseStatusCreatedTimeAgo; ?></a></span>
                                               </div>
                                              </td>
                                             
                                              <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
                                              
                                              <td>
                                                  <a target="_blank" href="<?php echo base_url();?>edit-case/<?php echo encodeString($case->caseID);?>" class="btn btn-primary" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>

                                                  <a href="<?php echo base_url(); ?>case-remove/<?php echo encodeString($case->caseID); ?>/" onclick="return confirm('Are you sure you want to remove case <?php echo $patientName.' ('.$caseID.')'; ?> ?');" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>

                                              </td>
                                            
                                            <?php } ?>
                                           
                                            </tr>
                          <?php 	
                                      }
                                      
                                      
                                      
                                
                                } else {
                          ?>
                                    <tr>
                                        <td colspan="8">No Case found.</td>
                                    </tr>
                          
                          <?php
                                    
                                }
                          ?>
                    
                    </tbody>
                  </table>
              </div>
            <div class="box-footer clearfix">
               <?php echo $pagingLink; ?>
            </div>
            </div>
          </div>
      	</div>
    </div>




      <!-- <div class="col-md-4">
          <div class="box">
            <div class="box-header with-border">
            
            </div>
            <div class="box-body">
             
        <div class="modal fade bs-example-modal-lg" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
             

              <div class="modal-body">


        <div class="box-body">
          <form method="post" action="<?= base_url() ?>cases-list">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Case/Patient Name</label>
               <input type="text" name="patient" class="form-control select2">
              </div>
             
              <div class="form-group">
                <label>Disabled</label>
                <select class="form-control select2" disabled="disabled" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
              
            </div>
           
            <div class="col-md-6">
              <div class="form-group">
                <label>Case ID</label>
                <input type="text" name="case_id" class="form-control select2">
              </div>
             
              <div class="form-group">
                <label>Minimal</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
             
            </div>
           
          </div>
         
          <button type="submit" class="btn btn-success waves-effect text-left" >Search</button>
          </form>
        </div>
       
      



              </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>

                

              </div>


            </div>
            
          </div>
          
        </div>
        
              <img src="../../images/model2.png" alt="default" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive" />
            </div>
          </div>
        </div> -->


         
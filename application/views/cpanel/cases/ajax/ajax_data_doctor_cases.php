<?php  $totalCounter = $this->model_case->countDoctorCases($teamID)->num_rows(); ?>

<div class="row">
        <div class="col-xl-12">
           <div class="box">
             <div class="box-header">
              <h3 class="box-title"><i class='fa fa-medkit'></i>  Cases (<?php echo number_format($totalCounter); ?>) </h3>

               <!-- <i class='fa fa-filter' class="btn btn-default" data-toggle="modal" data-target="#modal-default" style="float: right;"></i> -->

               <a style="float: right;" href="JavaScript:Void(0);" data-target=".searchCasesPopUp" data-toggle="modal"><i class="fa fa-filter" data-toggle="tooltip" title="" data-original-title="Filters"></i></a>

          </div>


             <div class="box-body">
            <div class="table-responsive">
                  <table class="table table-responsive table-striped" style="width:100%">
                    <tbody>
                    <tr>
                      <th width="7%"> ID</th>
                      <th width="14%">Patient </th>
                     <!--  <th width="14%">Stage </th>
                      <th width="22%">Status	</th> -->
                       <th width="22%">Dentist Case Status</th>
                      <th width="13%">Date</th>
                      <th width="9%">&nbsp;</th>
                     
                     <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
                                              
                      <th width="1%">&nbsp;</th>
                      
                     <?php } ?>
                     
                      </tr>
                  
                      <?php if ($cases) {
                                   
                                  foreach($cases->result() as $case) {
                                      
                                      $caseID 							 = $case->caseID;
                                      $doctorID                = $case->doctorID;
                                      $statusID                = $case->statusID;
                                      $patientName 						 = $case->patientName;
                                      $companyName 			 			 = $case->companyName;
                                      $receiveDate 			 			 = $case->receiveDate;
                                      
                                      $holdFlag 							 = $case->caseHoldFlag;
                                      $shippedFlag 						 = $case->caseShippedFlag;
                                      
                                      $productionStageID		 			 = $case->productionStageID;
                                      $productionStageName 	 			 = $case->productionStageName;
                                      
                                      $caseStatusCreated 				 	 = $case->caseStatusCreated;
                                      $statusID 				 			 = $case->statusID;
                                      $statusName 	 					 = $case->statusName;
                                      $doctorCaseStatusName              = $case->doctorCaseStatusName;
                                      
                                      $caseStatusCreatedDate				 = date('F d, Y',$caseStatusCreated);
                                      $caseStatusCreatedTime		 		 = date('h:i A',$caseStatusCreated);
                                      $caseStatusCreatedTimeAgo			 = timeAgo($caseStatusCreated).' ago'; // Calling From Shared Helper

                                      $doctorCaseCreated           = $case->doctorCaseCreated;
                                      $doctorCaseCreatedDate         = date('F d, Y',$doctorCaseCreated);
                                      $doctorCaseCreatedTime         = date('h:i A',$doctorCaseCreated);
                                      $doctorCaseCreatedTimeAgo      = timeAgo($doctorCaseCreated).' ago'; // Calling From Shared Helper

                                      $totalCounter = $this->model_case->countDoctorCases($teamID)->num_rows();

                                      if($doctorID==$teamID)
                                      {
                                      
                                      
                                      if ($receiveDate) {
                                                  
                                          $receiveDate = date('d M Y',strtotime($receiveDate));
                                      }
                                      
                                      if (empty($productionStageID)) {
                                                  
                                          $productionStageName = '-----';
                                      }
                                      
                                      if (empty($statusID)) {
                                                  
                                          $statusName = '-----';
                                      }
                                      
                                      $link = base_url().'case/'.encodeString($caseID);
                                      
                                      if ($statusID == CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY) {
                                          
                                          $link = base_url().'kit-send-to-customer/'.encodeString($caseID);
                                      }
                                      
                                  ?>
                                           <tr>
                                              <td height="59"><span data-toggle="tooltip" title="" data-original-title="ID"><a target="_blank" href="<?php echo $link;?>"><?php echo $caseID; ?></a></span></td>
                                              <td>
                                                <span data-toggle="tooltip" title="" data-original-title="Patient Name" style="font-size:12px;"><a target="_blank" href="<?php echo $link;?>"><strong><?php echo $patientName; ?></strong></a></span>
                                                  <br />
                                                <span data-toggle="tooltip" title="" data-original-title="Distributor"><a target="_blank" href="<?php echo $link;?>"><?php echo $companyName; ?></a></span>
                                             </td>
                                             <!--  <td><span data-toggle="tooltip" title="" data-original-title="Production Stage"><a target="_blank" href="<?php echo $link;?>"><?php echo $productionStageName; ?></a></span></td>
                                              <td><span data-toggle="tooltip" title="" data-original-title="Case Status"><a target="_blank" href="<?php echo $link;?>"><?php echo $statusName; ?> <?php if ($holdFlag == HARD_CODE_ID_YES) { echo '(HOLD)'; } ?></a></span></td> -->

                                              <td><span data-toggle="tooltip" title="" data-original-title="Case Status"><a target="_blank" href="<?php echo $link;?>">
                                                  <?php 
                                                        ?><span style=""><?= $doctorCaseStatusName; ?></span><?php 
                                                  ?>
                                               </a></span></td>
                                               
                                             <td>
                                                  <span data-toggle="tooltip" title="" data-original-title="Case Received Date">
                                                  <a target="_blank" href="<?php echo $link;?>"><?php echo $doctorCaseCreatedDate; ?></a>
                                                 </span>
                                             </td>
                                             <td>
                                               <div class="pull-right">
                                                    <span style="font-size:12px;" data-toggle="tooltip" title="" data-original-title="<?php echo $caseStatusCreatedDate; ?> <?php echo $caseStatusCreatedTime; ?>"><a target="_blank" href="<?php echo $link;?>"><i class="fa fa-clock-o"></i> <?php echo $doctorCaseCreatedTimeAgo; ?></a></span>
                                               </div>
                                              </td>
                                             
                                              <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
                                              
                                              <td>
                                                  <a target="_blank" href="<?php echo base_url();?>edit-case/<?php echo encodeString($case->caseID);?>" class="btn btn-primary" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                              </td>
                                            
                                            <?php } ?>
                                           
                                            </tr>
                          <?php 	
                                      }
                                    }
                                      
                                      
                                      
                                
                                } else {
                          ?>
                                    <tr>
                                        <td colspan="8">No Case found.</td>
                                    </tr>
                          
                          <?php
                                    
                                }
                          ?>
                    
                    </tbody>
                  </table>
              </div>
            <div class="box-footer clearfix">
               <?php echo $pagingLink; ?>
            </div>
            </div>
          </div>
      	</div>
    </div>




      <!-- <div class="col-md-4">
          <div class="box">
            <div class="box-header with-border">
            
            </div>
            <div class="box-body">
             
        <div class="modal fade bs-example-modal-lg" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
             

              <div class="modal-body">


        <div class="box-body">
          <form method="post" action="<?= base_url() ?>cases-list">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Case/Patient Name</label>
               <input type="text" name="patient" class="form-control select2">
              </div>
             
              <div class="form-group">
                <label>Disabled</label>
                <select class="form-control select2" disabled="disabled" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
              
            </div>
           
            <div class="col-md-6">
              <div class="form-group">
                <label>Case ID</label>
                <input type="text" name="case_id" class="form-control select2">
              </div>
             
              <div class="form-group">
                <label>Minimal</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
             
            </div>
           
          </div>
         
          <button type="submit" class="btn btn-success waves-effect text-left" >Search</button>
          </form>
        </div>
       
      



              </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>

                

              </div>


            </div>
            
          </div>
          
        </div>
        
              <img src="../../images/model2.png" alt="default" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive" />
            </div>
          </div>
        </div> -->


         
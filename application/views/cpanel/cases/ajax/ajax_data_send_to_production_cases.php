
<script language="javascript" type="text/javascript">
		
 $(document).ready(function () {
	
	var totalCases = <?php echo $totalCases; ?>;
			
			if (totalCases) {

				$('#impressions-send-to-production-frm-area').show();	
				
			} else {
				
				$('#impressions-send-to-production-frm-area').hide();	
			}
			
	
	$(document).on( "click", ".check_box_case", function() {
					
				
				var caseID 	   = $(this).val();
				var caseNumber = $(this).attr('key'); 
				
				 if ($(this).prop('checked')==true){ 
				 
				 	$('#checkboxes').append('<input type="checkbox" id="case_'+caseNumber+'" name="list_case[]" value="'+caseID+'" checked />');
				 
				 } else {
						
						$('#case_'+caseNumber).remove();
				 }
				
				var length = $('input[name="list_case_checkbox[]"]:checked').length;
				
				if (length > 0) {
						
						
						
						$('#save-btn-area').attr('type','button');
						$('#save-btn-area').removeClass('disabled');
				} else{
						
						$('#save-btn-area').attr('type','button');
						$('#save-btn-area').addClass('disabled');
				}
				
			});	
			
			
			$(document).on( "click", "#save-btn-area", function() {
				
				var length = $('input[name="list_case_checkbox[]"]:checked').length;
				
				var trackingNumber = $('#tracking_number').val();
				var courierService = $('#courier_service').val();
				
				if (length > 0 && trackingNumber != '' && courierService != '') {
					
					$( "#frm-send-to-production").submit();
					return true;
				
				} else {
					
					return false;	
				}
				
			});	
 
  });
  
  
 
 
</script>
<div class="box box-default" id="impressions-send-to-production-frm-area">
        <div class="box-header with-border">
          <h3 class="box-title">Impressions Send to Production</h3>
        </div>
        <!-- /.box-header -->
      
      <form id="frm-send-to-production" name="frm" action="<?php echo base_url(); ?>impressions-send-to-production/" method="post" enctype="multipart/form-data" autocomplete="off">
        <div id='checkboxes'></div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label>Tracking Number</label>
                <input name="tracking_number" id="tracking_number" class="form-control" type="text" value="<?php echo $this->input->post('tracking_number'); ?>" id="example-text-input">
                <?php if (form_error('tracking_number')) { echo form_error('tracking_number'); } ?>
              </div>
            </div>

            <div class="col-md-6 col-12">
              <div class="form-group">
                <label>Courier Service</label>
                <select name="courier_service" id="courier_service" class="form-control select2" style="width: 100%;">
                      <option value="">Courier Service</option>
					   <?php 
                              if ($deliveryServices->num_rows() > 0) {
                                      
								foreach($deliveryServices->result() as $deliveryService) {
									
								  $deliveryServiceID	=	$deliveryService->id;
								  $deliveryServiceName	=	$deliveryService->name;
									  
									  if ($this->input->post('courier_service') == $deliveryServiceID) {
									  
											  $selectedService = 'selected = selected';	
									  
									  }	else {
											  
											  $selectedService = NULL;		
									  }
											
											echo '<option value="'.$deliveryServiceID.'" '.$selectedService.'>'.$deliveryServiceName.'</option>';
                                      }	
                              }
                       ?>                 
                      
                   </select>
              </div>
            </div>

          </div>
			
           <div class="pull-right">
              <button id="save-btn-area" style="display:block;" type="button" onClick="retun abc();" class="btn btn-blue disabled pull-right"><i class="fa fa-send"></i> Send</button>
           </div> 
           
        </div>
      </form>   
      </div> 











<div class="row" id="first-div">


            <div class="box-body">
              <a class="btn btn-app bg-blue">
                <i class="fa fa-check-square-o" aria-hidden="true"></i> Recieved Cases
              </a>

              <a class="btn btn-app bg-default" href="javascript:void(0)" onclick="showhide('first-div','second-div');">
                <i class="fa fa-window-maximize" aria-hidden="true"></i> Missing Cases
              </a>

              <!-- <a class="btn btn-app bg-teal">
                <span class="badge bg-aqua">2</span>
                <i class="fa fa-envelope"></i> Inbox
              </a> -->
              
           </div>

        <div class="col-xl-12">
           <div class="box">
            <div class="box-header">
     		 <h3 class="box-title"><i class='fa fa-medkit'></i>  Cases (<?php echo number_format($totalCases); ?>)</h3>

         <a style="float: right;" href="JavaScript:Void(0);" data-target=".searchCasesPopUp" data-toggle="modal"><i class="fa fa-filter" data-toggle="tooltip" title="" data-original-title="Filters"></i></a>

  		 </div>

            <div class="box-body">
              <div class="table-responsive">
                    <table class="table table-responsive table-striped" style="width:100%">
                      <tbody>
                      <tr>
                        <th width="2%">&nbsp;</th>
                        <th width="8%"> ID</th>
                        <th width="19%">Patient </th>
                        <th width="21%">Stage </th>
                        <th width="26%">Status	</th>
                        <th width="13%">Date</th>
                        <th width="11%">&nbsp;</th>
                       
                       <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
                                              
                      <th width="1%">&nbsp;</th>
                      
                     <?php } ?>
                       
                        </tr>
                    
                        <?php if ($cases) {
                                    
                                    foreach($cases->result() as $case) {
                                        
                                        $caseID 							 = $case->caseID;
                                        $patientName 						 = $case->patientName;
                                        $companyName 			 			 = $case->companyName;
                                        $receiveDate 			 			 = $case->receiveDate;
                                        
                                        $holdFlag 							 = $case->caseHoldFlag;
                                        
                                        $productionStageID		 			 = $case->productionStageID;
                                        $productionStageName 	 			 = $case->productionStageName;
                                        
                                        $caseStatusCreated 				 	 = $case->caseStatusCreated;
                                        $statusID 				 			 = $case->statusID;
                                        $statusName 	 					 = $case->statusName;
                                        
                                        $caseStatusCreatedDate				 = date('F d, Y',$caseStatusCreated);
                                        $caseStatusCreatedTime		 		 = date('h:i A',$caseStatusCreated);
                                        $caseStatusCreatedTimeAgo			 = timeAgo($caseStatusCreated).' ago'; // Calling From Shared Helper

                                        $missingCount=$case->totalMissingCases;
                                        $missingCaseStatus=$case->missingCaseStatus;
                                        
                                        
                                        if ($receiveDate) {
                                                    
                                            $receiveDate = date('d M Y',strtotime($receiveDate));
                                        }
                                        
                                        if (empty($productionStageID)) {
                                                    
                                            $productionStageName = '-----';
                                        }
                                        
                                        if (empty($statusID)) {
                                                    
                                            $statusName = '-----';
                                        }
                                        
                                        $link = base_url().'case/'.encodeString($caseID);
                                        
                                        if ($statusID == CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY) {
                                            
                                            $link = base_url().'kit-send-to-customer/'.encodeString($caseID);
                                        }

                                        /*if($missingCount==0)
                                        {*/
                                        
                                    ?>
                                             <tr>
                                               <td>
                                               <?php if ($statusID  == CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_READY OR ($missingCount!=0 AND $missingCaseStatus!=HARD_CODE_ID_CLEAR)) { ?>
                                               	   <input name="list_case_checkbox[]" value="<?php echo encodeString($caseID); ?>" key="<?php echo $caseID; ?>" type="checkbox" id="box_<?php echo $caseID; ?>" class="filled-in chk-col-yellow check_box_case">
                                                   <label for="box_<?php echo $caseID; ?>"></label>
                                                <?php } ?>
                                                </td>
                                                <td height="59"><span data-toggle="tooltip" title="" data-original-title="ID"><a target="_blank" href="<?php echo $link;?>"><?php echo $caseID; ?></a></span></td>
                                                <td>
                                                  <span data-toggle="tooltip" title="" data-original-title="Patient Name" style="font-size:12px;"><a target="_blank" href="<?php echo $link;?>"><strong><?php echo $patientName; ?></strong></a></span>
                                                    <br />
                                                  <span data-toggle="tooltip" title="" data-original-title="Distributor"><a target="_blank" href="<?php echo $link;?>"><?php echo $companyName; ?></a></span>
                                               </td>
                                                <td><span data-toggle="tooltip" title="" data-original-title="Production Stage"><a target="_blank" href="<?php echo $link;?>"><?php echo $productionStageName; ?></a></span></td>
                                                <td><span data-toggle="tooltip" title="" data-original-title="Case Status"><a target="_blank" href="<?php echo $link;?>"><?php echo $statusName; ?> <?php if ($holdFlag == HARD_CODE_ID_YES) { echo '(HOLD)'; } ?></a></span></td>
                                               <td>
                                                    <span data-toggle="tooltip" title="" data-original-title="Case Received Date">
                                                    <a target="_blank" href="<?php echo $link;?>"><?php echo $receiveDate; ?></a>
                                                   </span>
                                               </td>
                                               <td>
                                                 <div class="pull-right">
                                                      <span style="font-size:12px;" data-toggle="tooltip" title="" data-original-title="<?php echo $caseStatusCreatedDate; ?> <?php echo $caseStatusCreatedTime; ?>"><a target="_blank" href="<?php echo $link;?>"><i class="fa fa-clock-o"></i> <?php echo $caseStatusCreatedTimeAgo; ?></a></span>
                                                 </div>
                                                </td>
                                               
                                                <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
                                              
                                                <td>
                                                    <a target="_blank" href="<?php echo base_url();?>edit-case/<?php echo encodeString($case->caseID);?>" class="btn btn-primary" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>

                                                    <a href="<?php echo base_url(); ?>case-remove/<?php echo encodeString($case->caseID); ?>/" onclick="return confirm('Are you sure you want to remove case <?php echo $patientName.' ('.$caseID.')'; ?> ?');" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>

                                                </td>
                                            
                                            <?php } ?>
                                             
                                              </tr>
                            <?php 	
                                        /*}*/

                                      }
                                  
                                  } else {
                            ?>
                                      <tr>
                                          <td colspan="8">No Case found.</td>
                                      </tr>
                            
                            <?php
                                      
                                  }


                            ?>
                      
                      </tbody>
                    </table>
                </div>
            <div class="box-footer clearfix">
               <?php echo $pagingLink; ?>
            </div>
            </div>
          </div>
      	</div>
    </div>

  












  <div class="row" id="second-div" style="display: none;">


            <div class="box-body">
              <a class="btn btn-app bg-default" href="javascript:void(0)" onclick="showhide('second-div','first-div');">
                <i class="fa fa-check-square-o" aria-hidden="true"></i> Recieved Cases
              </a>

              <a class="btn btn-app bg-blue" href="javascript:void(0)" >
                <i class="fa fa-window-maximize" aria-hidden="true"></i> Missing Cases
              </a>

              <!-- <a class="btn btn-app bg-teal">
                <span class="badge bg-aqua">2</span>
                <i class="fa fa-envelope"></i> Inbox
              </a> -->
              
           </div>

        <div class="col-xl-12">
           <div class="box">
            <div class="box-header">
         <h3 class="box-title"><i class='fa fa-medkit'></i> Missing Cases (<?php echo number_format($missing_totalRows); ?>)</h3>

          <a style="float: right;" href="JavaScript:Void(0);" data-target=".searchCasesPopUp" data-toggle="modal"><i class="fa fa-filter" data-toggle="tooltip" title="" data-original-title="Filters"></i></a>
          
       </div>

            <div class="box-body">
              <div class="table-responsive">
                    <table class="table table-responsive table-striped" style="width:100%">
                      <tbody>
                      <tr>
                        <th width="2%">&nbsp;</th>
                        <th width="8%"> ID</th>
                        <th width="19%">Patient </th>
                        <th width="21%">Stage </th>
                        <th width="26%">Status  </th>
                        <th width="13%">Date</th>
                        <th width="11%">&nbsp;</th>
                       
                       <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
                                                
                        <?php } ?>
                       
                        </tr>
                    
                        <?php 
                        /*$missingCasesNum=$missing_cases->num_rows();*/
                        if ($missing_cases) {
                                    
                                    foreach($missing_cases->result() as $missingCase) {
                                        
                                        $caseID                = $missingCase->caseID;
                                        $patientName             = $missingCase->patientName;
                                        $companyName             = $missingCase->companyName;
                                        $receiveDate             = $missingCase->receiveDate;
                                        
                                        $holdFlag                = $missingCase->caseHoldFlag;
                                        
                                        $productionStageID           = $missingCase->productionStageID;
                                        $productionStageName         = $missingCase->productionStageName;
                                        
                                        $caseStatusCreated           = $missingCase->caseStatusCreated;
                                        $statusID                = $missingCase->statusID;
                                        $statusName              = $missingCase->statusName;
                                        
                                        $caseStatusCreatedDate         = date('F d, Y',$caseStatusCreated);
                                        $caseStatusCreatedTime         = date('h:i A',$caseStatusCreated);
                                        $caseStatusCreatedTimeAgo      = timeAgo($caseStatusCreated).' ago'; // Calling From Shared Helper

                                        $missingCaseStatus=$missingCase->missingCaseStatus;
                                        
                                        
                                        if ($receiveDate) {
                                                    
                                            $receiveDate = date('d M Y',strtotime($receiveDate));
                                        }
                                        
                                        if (empty($productionStageID)) {
                                                    
                                            $productionStageName = '-----';
                                        }
                                        
                                        if (empty($statusID)) {
                                                    
                                            $statusName = '-----';
                                        }
                                        
                                        $link = base_url().'case/'.encodeString($caseID);
                                        
                                        if ($statusID == CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY) {
                                            
                                            $link = base_url().'kit-send-to-customer/'.encodeString($caseID);
                                        }

                                        if ($missingCaseStatus!=HARD_CODE_ID_CLEAR) { //check if missing case is not clear start
                                        
                                    ?>
                                             <tr>
                                               <!-- <td>
                                               <?php if ($statusID  != CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_READY) { ?>
                                                   <input name="list_case_checkbox[]" value="<?php echo encodeString($caseID); ?>" key="<?php echo $caseID; ?>" type="checkbox" id="box_<?php echo $caseID; ?>" class="filled-in chk-col-yellow check_box_case">
                                                   <label for="box_<?php echo $caseID; ?>"></label>
                                                <?php } ?>
                                                </td> -->

                                                <td>
                                                  <?php if ($missingCaseStatus!=HARD_CODE_ID_CLEAR) { ?>
                                                   <input name="list_case_checkbox[]" value="<?php echo encodeString($caseID); ?>" key="<?php echo $caseID; ?>" type="checkbox" id="box_<?php echo $caseID; ?>" class="filled-in chk-col-yellow check_box_case">
                                                   <label for="box_<?php echo $caseID; ?>"></label>
                                                 <?php } ?>
                                                </td>

                                                <td height="59"><span data-toggle="tooltip" title="" data-original-title="ID"><a target="_blank" href="<?php echo $link;?>"><?php echo $caseID; ?></a></span></td>
                                                <td>
                                                  <span data-toggle="tooltip" title="" data-original-title="Patient Name" style="font-size:12px;"><a target="_blank" href="<?php echo $link;?>"><strong><?php echo $patientName; ?></strong></a></span>
                                                    <br />
                                                  <span data-toggle="tooltip" title="" data-original-title="Distributor"><a target="_blank" href="<?php echo $link;?>"><?php echo $companyName; ?></a></span>
                                               </td>
                                                <td><span data-toggle="tooltip" title="" data-original-title="Production Stage"><a target="_blank" href="<?php echo $link;?>"><?php echo $productionStageName; ?></a></span></td>
                                                <td><span data-toggle="tooltip" title="" data-original-title="Case Status"><a target="_blank" href="<?php echo $link;?>"><?php echo $statusName; ?> <?php if ($holdFlag == HARD_CODE_ID_YES) { echo '(HOLD)'; } ?></a></span></td>
                                               <td>
                                                    <span data-toggle="tooltip" title="" data-original-title="Case Received Date">
                                                    <a target="_blank" href="<?php echo $link;?>"><?php echo $receiveDate; ?></a>
                                                   </span>
                                               </td>
                                               <td>
                                                 <div class="pull-right">
                                                      <span style="font-size:12px;" data-toggle="tooltip" title="" data-original-title="<?php echo $caseStatusCreatedDate; ?> <?php echo $caseStatusCreatedTime; ?>"><a target="_blank" href="<?php echo $link;?>"><i class="fa fa-clock-o"></i> <?php echo $caseStatusCreatedTimeAgo; ?></a></span>
                                                 </div>
                                                </td>
                                               
                                                <?php if (in_array(MODULE_CASE_EDIT,$accessModules)) { ?>
                                                
                                               <?php } ?>
                                             
                                              </tr>
                            <?php   
                                        }
                                  
                                  } 

                                } //check if missing case is not clear end

                                   else {
                            ?>
                                      <tr>
                                          <td colspan="8">No Case found.</td>
                                      </tr>
                            
                            <?php
                                      
                                  }
                            ?>
                      
                      </tbody>
                    </table>
                </div>
            <div class="box-footer clearfix">
               <?php echo $pagingLink; ?>
            </div>
            </div>
          </div>
        </div>
    </div>


    <script>
     function showhide(showid,hideid) {

            showobj = document.getElementById(showid);
            hideobj = document.getElementById(hideid);

            //showobj.style.display = 'block';

            //hideobj.style.display = 'none';
            $("#"+hideid).slideToggle('slow');
            $("#"+showid).slideToggle('slow');




        }
</script>
<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>

<script language="javascript" type="text/javascript">

$(document).ready(function(){
    
	
	$("#delivery_service_send_customer").change(function(){
		
		 	var inputValue = this.value;
		
			$('#delivery_service_receive_customer').val(inputValue);
			$('#delivery_service_receive_customer').trigger('change');
	});
	
	$(".reset-btn").click(function(){
        
		$("#frm-kit-send-to-customer").trigger("reset");
		
		$("#delivery_service_send_customer").val('').trigger('change');
		$("#delivery_service_receive_customer").val('').trigger('change')
    });
});
</script>

  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="breadcrumb-item">Kit Send to Customer</li>
  </ol>
 </section>

	<?php if ($caseFlag == false) { ?>
    
	<div class="pad margin no-print">
      <div class="callout callout-danger" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> ERROR:</h4>
        No case found.
      </div>
    </div>
    
    <?php } ?>
	
    <?php if ($caseFlag == true) { 
			
			$encodeCaseID					= encodeString($case['caseID']); // Calling From General Helper
			
			$caseID 						= $case['caseID'];
			$patientName 					= $case['patientName'];
			$patientAge 					= $case['patientAge'];
			$genderName 					= $case['genderName'];
			$archUpper 						= $case['archUpper'];
			$archLower 						= $case['archLower'];
			$companyID 						= $case['companyID'];
			$companyName 					= $case['companyName'];
			$companyLogo 					= $case['companyLogo'];
			$countryName 					= $case['countryName'];
			$caseComments					= $case['caseComments'];
			$caseReceiveDate 				= $case['receiveDate'];
			$caseCreated					= $case['caseCreated'];
			$caseCreatedEmployeeName		= $case['employeeName'];
			$caseCreatedEmployeeCode		= $case['employeeCode'];
			$caseCreatedEmployeeID			= $case['employeeID'];
			
			$employeeAssignJobs				= getEmployeeAssignJobs($caseCreatedEmployeeID); // Calling From HR Employees Helper
										
			if ($employeeAssignJobs) {
						
				$employeeAssignJob 								= $employeeAssignJobs->row_array();
				$caseCreatedEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
				
			
			} else {
					
					$caseCreatedEmployeeAssignJobDescription 	= NULL;
			}	   		
			
			$caseCreatedDate		= date('d F. Y',$caseCreated);
			$caseCreatedTime		= date('h:i A',$caseCreated);
			$caseCreatedTimeAgo		= timeAgo($caseCreated).' ago';
			
			$caseReceived			= date('l, d F Y',strtotime($caseReceiveDate));
			$caseReceivedTimeAgo	= timeAgo(strtotime($caseReceiveDate)).' ago';
			
			if (empty($caseComments)) {
				
					$caseComments  = '';
			}
	?>
   
    <!-- Main content -->
    <section class="invoice printableArea">
    
    					 <?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
      
      <div class="row">
        <div class="col-12">
          <h2 class="page-header">
            CASE RECEIVED
            <small class="pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $caseReceivedTimeAgo; ?>"><?php echo $caseReceived; ?></small>
          </h2>
        </div>
      </div>
    
      <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
          Case ID: <?php echo $caseID; ?>
          
          <address>
            <span data-toggle="tooltip" title="" data-original-title="Patient Name" data-placement="top"><strong class="text-blue"><?php echo $patientName; ?></strong></span><br>
            <span data-toggle="tooltip" title="" data-original-title="Age" data-placement="top">Age: <?php echo $patientAge; ?></span><br />
            <span data-toggle="tooltip" title="" data-original-title="Age" data-placement="top"><?php echo $genderName; ?></span><br />
            <span data-toggle="tooltip" title="" data-original-title="Arch(es)" data-placement="top"> 
						   
			   <?php if ($archUpper == HARD_CODE_ID_YES && $archLower == HARD_CODE_ID_YES) {
                             echo 'Upper, Lower'; 
                     } else {
                        
                        if ($archUpper == HARD_CODE_ID_YES) {
                                
                                echo 'Upper';	
                        }
                        
                        if ($archLower == HARD_CODE_ID_YES) {
                                
                                echo 'Lower';	
                        }	 
                     }
               ?>
               </span> 
              
          </address>
          
        </div>
        <!-- /.col -->
        <div class="col-sm-6 invoice-col text-right">
         
          <address>
             <?php if ($companyLogo) { ?>
              <img class="profile-user-img img-fluid mx-auto" style="margin:0px;" src="<?php echo base_url(); ?>backend_images/companies/<?php echo $companyLogo; ?>" alt="Company Logo"><br>
             <?php } ?> 
             <strong class="text-green"><?php echo $companyName; ?></strong><br>
            
			<?php echo $countryName; ?><br />
            
            <?php if ($caseComments) { ?>
            
            Comments / Remarks: <?php echo $caseComments; ?>
            
            <?php } ?>
           <br />
           <small>Uploaded by:</small><br>
           <span data-toggle="tooltip" title="" data-original-title="<?php echo $caseCreatedEmployeeAssignJobDescription; ?>"><?php echo $caseCreatedEmployeeName; ?></span><br>
           <span data-toggle="tooltip" title="" data-original-title="<?php echo $caseCreatedTimeAgo; ?>"><?php echo $caseCreatedDate; ?> <?php echo $caseCreatedTime; ?></span><br>

          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-12 invoice-col">
			<div class="invoice-details row no-margin" style="background-color:#FFF;">
			  <div class="col-md-6 col-lg-4" style="color:#000"><b>Production Stage: </b> <span style="margin-left:10px;" id="present-stage-name"><?php echo $presentProductionStageName; ?></span></div>
			  <div class="col-md-6 col-lg-4" style="color:#000"><b>Status:</b> <span style="margin-left:10px;"><?php echo $presentStatusName; ?></span></div>
			</div>
		</div>
      <!-- /.col -->
      </div>
     
      <br /><br />
      <form id="frm-kit-send-to-customer" action="<?php echo base_url(); ?>kit-send-to-customer/<?php echo encodeString($caseID); ?>" method="post" autocomplete="off">
     	<input type="hidden" name="presentCaseStageID" value="<?php echo $presentCaseStageID; ?>"  />
      <div class="row">
          
          <div class="col-12 col-sm-6">
              
              <p class="lead"><b>Send to Customer:</b></p>
  
                  <div class="form-group">
                    <input type="text" class="form-control" name="tracking_number_send_customer" id="tracking_number_send_customer" value="<?php echo $this->input->post('tracking_number_send_customer'); ?>" placeholder="Tracking Number" />
                    <?php if (form_error('tracking_number_send_customer')) { echo form_error('tracking_number_send_customer'); } ?>    
                  </div>
                  
                  <div class="form-group">
                   <select name="delivery_service_send_customer" id="delivery_service_send_customer" class="form-control select2" style="width: 100%;">
                      <option value="">Courier Service</option>
					  <?php 
                              if ($deliveryServices->num_rows() > 0) {
                                      
                                      foreach($deliveryServices->result() as $deliveryService) {
                                          
                                                  $deliveryServiceID				=	$deliveryService->id;
                                                  $deliveryServiceName				=	$deliveryService->name;
                                                      
                                                      if ($this->input->post('delivery_service_send_customer') == $deliveryServiceID) {
                                                      
                                                              $selectedService = 'selected = selected';	
                                                      
                                                      }	else {
                                                              
                                                              $selectedService = NULL;		
                                                      }
                                                  
                                                  echo '<option value="'.$deliveryServiceID.'" '.$selectedService.'>'.$deliveryServiceName.'</option>';
                                      }	
                              }
                       ?>                 
                      
                   </select>
                   <?php if (form_error('delivery_service_send_customer')) { echo form_error('delivery_service_send_customer'); } ?>      
                  </div>
                  
                  <div class="form-group">
                  
                  <div class="form-group" style="margin-top:25px;">
                    <textarea name="comments" class="form-control" rows="3" placeholder="Add a comment..."><?php echo $this->input->post('comments'); ?></textarea>
                    <?php if (form_error('comments')) { echo form_error('comments'); } ?>      
                  </div>
                  
                  <div class="box-footer clearfix pull-right">
                  <button type="submit" class="btn btn-blue" id=""> <i class="glyphicon glyphicon-floppy-disk"></i> Save </button>
                  <button type="button" class=" btn btn-warning reset-btn">Cancel</button>
                  </div> 
                          
                </div>
               
          </div>
  
          
          
          <div class="col-12 col-sm-6 text-left receipt-details-area">
           <p class="lead"><b>Receiving from Customer:</b></p>
           <div class="form-group">
              <input type="text" class="form-control" name="tracking_number_receive_customer" id="tracking_number_receive_customer" value="<?php echo $this->input->post('tracking_number_receive_customer'); ?>" placeholder="Tracking Number" />
              <?php if (form_error('tracking_number_receive_customer')) { echo form_error('tracking_number_receive_customer'); } ?>    
            </div>
         
            <div class="form-group">
               <select name="delivery_service_receive_customer" id="delivery_service_receive_customer" class="form-control select2" style="width: 100%;">
                  <option value="">Courier Service</option>
                  <?php 
                          if ($deliveryServices->num_rows() > 0) {
                                  
                                  foreach($deliveryServices->result() as $deliveryService) {
                                      
                                              $deliveryServiceID				=	$deliveryService->id;
                                              $deliveryServiceName				=	$deliveryService->name;
                                                  
                                                  if ($this->input->post('delivery_service_receive_customer') == $deliveryServiceID) {
                                                  
                                                          $selectedService = 'selected = selected';	
                                                  
                                                  }	else {
                                                          
                                                          $selectedService = NULL;		
                                                  }
                                              
                                              echo '<option value="'.$deliveryServiceID.'" '.$selectedService.'>'.$deliveryServiceName.'</option>';
                                  }	
                          }
                   ?>                 
                  
               </select>
               <?php if (form_error('delivery_service_receive_customer')) { echo form_error('delivery_service_receive_customer'); } ?>      
           </div>

         </div>
       </div>
     </form>
      
    </section>
    
  <?php } ?>
   
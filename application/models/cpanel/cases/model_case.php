<?php 
error_reporting(0);
 if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

class Model_Case extends CI_Model {
		
		public function __construct() {
			
			$this->caseStatusReadyArray     	= array(
														CASE_STATUS_KIT_SHIP_TO_CUSTOMER_READY,
														CASE_STATUS_KIT_TO_CUSTOMER_IN_TRANSIT,
														CASE_STATUS_IMPRESSIONS_TO_NY_READY,
														CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_READY,
														CASE_STATUS_READY_FOR_UPLOADING_READY,
														CASE_STATUS_WAITING_FOR_APPROVAL_WAITING,
														CASE_STATUS_PRODUCTION_TO_NY_READY,
														CASE_STATUS_ALIGNERS_TO_CUSTOMER_READY
														);
											
			$this->caseStatusTransitArray    	= array(
														CASE_STATUS_IMPRESSIONS_TO_NY_IN_TRANSIT,
														CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_IN_TRANSIT,
														CASE_STATUS_PRODUCTION_TO_NY_IN_TRANSIT,
														CASE_STATUS_ALIGNERS_TO_CUSTOMER_IN_TRANSIT
														);
			
			$this->caseStatusProcessArray    	= array();
														
			$this->caseStatusHoldArray    		= array();
														
			$this->caseStatusOFFHoldArray    	= array();
			
			$this->caseStatusModificationArray  = array(
														CASE_STATUS_WAITING_FOR_APPROVAL_MODIFICATION
														);
			
			$this->caseStatusDoneArray  	 	= array(
														CASE_STATUS_KIT_SHIP_TO_CUSTOMER_SHIPPED,
														CASE_STATUS_KIT_TO_CUSTOMER_RECEIVED,
														CASE_STATUS_IMPRESSIONS_TO_NY_RECEIVED,
														CASE_STATUS_IMPRESSIONS_TO_PRODUCTION_RECEIVED,
														CASE_STATUS_READY_FOR_UPLOADING_DONE,
														CASE_STATUS_WAITING_FOR_APPROVAL_APPROVED,
														CASE_STATUS_PRODUCTION_TO_NY_RECEIVED,
														CASE_STATUS_ALIGNERS_TO_CUSTOMER_RECEIVED
														);
			
			$this->caseStatusRejectArray  		= array();
		
		}
		
		function getCompanyCaseTypes($companyID) {
					
			$this->db->select('caseType.id as caseTypeID,
							   caseType.name as caseTypeName');
									  
			$this->db->from(MEDICAL_COMPANY_CASE_TYPES_TABLE.' as companyCaseType');
			
			
			$this->db->where('companyCaseType.company_id',$companyID);
			$this->db->where('companyCaseType.is_deleted',HARD_CODE_ID_NO);
			
			$this->db->join(MEDICAL_CASE_TYPE_TABLE.' as caseType', 'caseType.id = companyCaseType.case_id', 'left');
			
			$this->db->order_by('caseType.name', 'ASC');
			
			$result = $this->db->get(); 

			return $result;
		
	}
		
		function getCasePresentStage($caseID) {
				
					$this->db->select('caseStage.id as caseStageID,
									   caseStage.case_id as caseID,
									   caseStage.created as caseStageMoveDate,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName');
											  
					$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');
					
					$this->db->where('medicalCase.id',$caseID);
					$this->db->where('caseStage.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage','caseStage.id = medicalCase.present_case_stage_id','left');
					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage','productionStage.id = caseStage.production_stage_id','left');
					
					$this->db->order_by('caseStage.id', 'DESC');
					$this->db->limit(1);
						
					$result = $this->db->get();
					
					return $result;
		
		}
		
		function getCasePresentStatus($caseID) {
				
					$this->db->select('caseStatus.id as tableID,
									   caseStatus.id as caseStatusID,
									   caseStatus.case_id as caseID,
									   caseStatus.created as caseStatusMoveDate,
									   status.id as statusID,
									   status.name as statusName,
									   status.parent as statusParentID');
											  
					$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');
					
					$this->db->where('medicalCase.id',$caseID);
					$this->db->where('caseStatus.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.case_stage_id = medicalCase.present_case_stage_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');
					
					$this->db->order_by('caseStatus.id', 'DESC');
					$this->db->limit(1); 
					
						
					$result = $this->db->get();
					
					return $result;
		
	}
	
		function getCasePresentOperator($caseID) {
				
					$this->db->select('caseOperator.case_id as caseID,
									   caseOperator.created as caseOperatorMoveDate,
									   caseOperator.id as caseOperatorID,
									   caseOperator.assignment as caseOperatorAssignmentType,
									   caseOperator.processing_done as caseOperatorProcessingDone,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');
					
					$this->db->where('medicalCase.id',$caseID);
					$this->db->where('caseOperator.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASE_OPERATOR_TABLE.' as caseOperator', 'caseOperator.case_stage_id = medicalCase.present_case_stage_id', 'left');
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = caseOperator.team_id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');	
					
					$this->db->order_by('caseOperator.id', 'DESC');
					$this->db->limit(1); 
						
					$result = $this->db->get();
				
					return $result;
					
				
		}
		
		function getCaseChangeStageHistory($caseID) {
			
					$this->db->select('caseStage.id as tableID,
									   caseStage.id as caseStageID,
									   caseStage.case_id as caseID,
									   caseStage.auto_move as caseStageAutoMove,
									   caseStage.created as caseStageCreated,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName');
											  
					$this->db->from(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage');
					
					$this->db->where('caseStage.case_id',$caseID);
					$this->db->where('caseStage.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage','productionStage.id = caseStage.production_stage_id','left');
					
					$this->db->order_by('caseStage.id','DESC');
						
					$result = $this->db->get();
					
					return $result;
					
						
		}
		
		function getCaseChangeStatusHistory($caseStageID,$caseID) {
			
					$this->db->select('caseStatus.id as tableID,
									   caseStatus.id as caseStatusID,
									   caseStatus.case_id as caseID,
									   caseStatus.auto_move as caseStatusAutoMove,
									   caseStatus.created as caseStatusCreated,
									   caseStatus.description as caseStatusComments,
									   status.id as statusID,
									   status.name as statusName,
									   status.parent as statusParentID,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MEDICAL_CASE_STATUS_TABLE.' as caseStatus');
					
					$this->db->where('caseStatus.case_stage_id',$caseStageID);
					$this->db->where('caseStatus.case_id',$caseID);
					
					$this->db->where('caseStatus.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status','status.id = caseStatus.status_id','left');
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = caseStatus.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					
					$this->db->order_by('caseStatus.id', 'DESC');
						
					$result = $this->db->get();
					
					return $result;
					
						
		}
		
		function getCaseChangeOperatorHistory($caseID,$caseStageID,$caseStatusID) {
			
					$this->db->select('caseOperator.case_id as caseID,
									   caseOperator.description as caseOperatorComments,
									   caseOperator.assignment as caseOperatorAssignmentType,
									   caseOperator.resume_case as caseResumeFlag,
									   caseOperator.created_by as caseOperatorCreatedBy,
									   caseOperator.created as caseOperatorCreated,
									   creatorOrganizationEmployee.id as creatorEmployeeTeamID,
									   creatorOrganizationEmployee.id as creatorEmployeeID,
									   creatorOrganizationEmployee.code as creatorEmployeeCode,
									   creatorOrganizationEmployee.employee_number as creatorEmployeeNumber,
									   creatorHR.name as creatorEmployeeName,
									   creatorHR.photo as creatorEmployeePhoto,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MEDICAL_CASE_OPERATOR_TABLE.' as caseOperator');
					
					$this->db->where('caseOperator.case_id',$caseID);
					$this->db->where('caseOperator.case_stage_id',$caseStageID);
					$this->db->where('caseOperator.case_status_id',$caseStatusID);
					$this->db->where('caseOperator.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as creatorOrganizationEmployee', 'creatorOrganizationEmployee.id = caseOperator.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as creatorHR', 'creatorOrganizationEmployee.HR_id = creatorHR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as creatorPrefix', 'creatorHR.prefix = creatorPrefix.id', 'left');
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = caseOperator.team_id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					
					$this->db->order_by('caseOperator.id', 'DESC');
						
					$result = $this->db->get();
					
					return $result;
					
						
		}
		
		function getCaseOperatorCreatedBy($teamID) {
			
					$this->db->select('organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					
					$this->db->where('organizationEmployee.id',$teamID);
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
						
					$result = $this->db->get();
				
					return $result;
					
						
		}
		
		function getCaseStatusOperator($caseID,$caseStageID,$caseStatusID) {
			
					$this->db->select('caseOperator.case_id as caseID,
									   caseOperator.description as caseOperatorComments,
									   caseOperator.assignment as caseOperatorAssignmentType,
									   caseOperator.resume_case as caseResumeFlag,
									   caseOperator.created as caseOperatorCreated,
									   creatorOrganizationEmployee.id as creatorEmployeeTeamID,
									   creatorOrganizationEmployee.id as creatorEmployeeID,
									   creatorOrganizationEmployee.code as creatorEmployeeCode,
									   creatorOrganizationEmployee.employee_number as creatorEmployeeNumber,
									   creatorHR.name as creatorEmployeeName,
									   creatorHR.photo as creatorEmployeePhoto,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MEDICAL_CASE_OPERATOR_TABLE.' as caseOperator');
					
					$this->db->where('caseOperator.case_id',$caseID);
					$this->db->where('caseOperator.case_stage_id',$caseStageID);
					$this->db->where('caseOperator.case_status_id',$caseStatusID);
					$this->db->where('caseOperator.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as creatorOrganizationEmployee', 'creatorOrganizationEmployee.id = caseOperator.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as creatorHR', 'creatorOrganizationEmployee.HR_id = creatorHR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as creatorPrefix', 'creatorHR.prefix = creatorPrefix.id', 'left');
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = caseOperator.team_id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					
					$this->db->order_by('caseOperator.id', 'DESC'); 
						
					$result = $this->db->get();
					
					return $result;
		}
		
		function countProductionStageCases($stageID) {
					
			 $stageFamilyArray	= array();
			
					$IDs 		 = $this->stageIDsParentToChild($stageID);
				
					if ($IDs->num_rows() > 0) {
							
							$stageFamilyArray[] = $stageID;
							
							foreach($IDs->result() as $ID) {
									
									$stageFamilyArray[]	= $ID->childID_1;
									
									if ($ID->childID_2) {
										
										$stageFamilyArray[]	= $ID->childID_2;
									}	
									
									if ($ID->childID_3) {
										
										$stageFamilyArray[]	= $ID->childID_3;
									}	
									
									if ($ID->childID_4) {
										
										$stageFamilyArray[]	= $ID->childID_4;
									}	
									
									if ($ID->childID_5) {
										
										$stageFamilyArray[]	= $ID->childID_5;
									}			
											
							}	
						}
					
					
					$this->db->select('count(medicalCase.id) as totalCases');
											  
					$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');
					
					if ($stageID != RECEIVE_ORDER_ENTRY) {
							
							$this->db->where_in('caseStage.production_stage_id',$stageFamilyArray);
					}
					
					$this->db->where('medicalCase.status',HARD_CODE_ID_ACTIVATED);
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					
					if ($stageID != RECEIVE_ORDER_ENTRY) {
						
						$this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');
						
						
					}
						
					$result = $this->db->get();
			
					if ($result->num_rows() >0) {
					
						$result		= $result->row_array();
						$result		= $result['totalCases'];
						
						return $result;
						
					} else {
						
						return 0;
					}
					
				
	}
	
		function stageIDsParentToChild($parentID) {
				
					$SQL = "SELECT
							   p.id AS parentID,
							   p.name AS parentName,
							   c1.id AS childID_1,
							   c1.name AS childName_1,
							   c2.id AS childID_2,
							   c2.name AS childName_2,
							   c3.id AS childID_3,
							   c3.name AS childName_3,
							   c4.id AS childID_4,
							   c4.name AS childName_4,
							   c5.id AS childID_5,
							   c5.name AS childName_5
						     
							 FROM 
  								
								  ".MEDICAL_PRODUCTION_STAGES_TABLE." p
								  LEFT JOIN ".MEDICAL_PRODUCTION_STAGES_TABLE." c1
    							  ON c1.parent = p.id
								  LEFT JOIN ".MEDICAL_PRODUCTION_STAGES_TABLE." c2
    							  ON c2.parent = c1.id
								  LEFT JOIN ".MEDICAL_PRODUCTION_STAGES_TABLE." c3
   								  ON c3.parent = c2.id
								  LEFT JOIN ".MEDICAL_PRODUCTION_STAGES_TABLE." c4
								  ON c4.parent = c3.id
								  LEFT JOIN ".MEDICAL_PRODUCTION_STAGES_TABLE." c5
								  ON c5.parent = c4.id
							 
							 WHERE
   							
							
   							 p.id=".$parentID;
							 
						$result = $this->db->query($SQL);
						
						return $result;
							 
							 
					
		
	}
	
		function getStageCases($stageID) {
				
				$stageFamilyArray	= array();
				
				$IDs 				= $this->stageIDsParentToChild($stageID);
				
				$stageFamilyArray[] = $stageID;
				
				if ($IDs->num_rows() > 0) {
						
					foreach($IDs->result() as $ID) {
								
								if ($ID->childID_1) {
							
									//$stageFamilyArray[]	= $ID->childID_1;
								}
								
								if ($ID->childID_2) {
									
									//$stageFamilyArray[]	= $ID->childID_2;
								}	
								
								if ($ID->childID_3) {
									
									//$stageFamilyArray[]	= $ID->childID_3;
								}	
								
								if ($ID->childID_4) {
									
									//$stageFamilyArray[]	= $ID->childID_4;
								}	
								
								if ($ID->childID_5) {
									
									//$stageFamilyArray[]	= $ID->childID_5;
								}	
						}
					} 
					
					$this->db->select('medicalCase.id as tableID,
									   medicalCase.id as caseID,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   country.id as countryID,
									   country.name as countryName,
									   company.id as companyID,
									   company.name as companyName');
											  
					$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');
					
					$this->db->where_in('caseStage.production_stage_id',$stageFamilyArray);
					
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('caseStage.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');
					$this->db->join(COUNTRIES_TABLE.' as country', 'country.id = medicalCase.country', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');
					
					$this->db->order_by('medicalCase.id', 'DESC');
						
					$result = $this->db->get(); 
					
					
					return $result;
		
	}
	
		function casesListing($page,$recordperpage,$searchParameters) {
					
					if ($searchParameters['searchTimePeriod'] == 'due') {
							
							$dueIDs = $this->dueSetupTimelineCases();
					}
					
					if ($searchParameters['searchTimePeriod'] == 'before') {
							
							$beforeIDs = $this->beforeSetupTimelineCases();
					}
					
					if ($searchParameters['searchTimePeriod'] == 'late-complete') {
							
							$lateIDs = $this->lateSetupTimelineCases();
					}
					
					if ($searchParameters['searchTimePeriod'] == 'incomplete') {
							
							$delayIDs = $this->delaySetupTimelineCases();
					}
					
					$this->db->select('doctorCase.doctor_id as doctorID,
						               doctorCase.link as caseLink,
						               doctorCase.code as caseCode,
						               medicalCase.id as tableID,
									   medicalCase.id as caseID,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.hold as caseHoldFlag,
									   medicalCase.ship as caseShippedFlag,
									   country.id as countryID,
									   country.name as countryName,
									   company.id as companyID,
									   company.name as companyName,
									   createdEmployee.id as employeeTeamID,
									   createdEmployee.id as employeeID,
									   createdEmployee.code as employeeCode,
									   createdEmployee.employee_number as employeeNumber,
									   createdHR.name as employeeName,
									   createdHR.photo as employeePhoto,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName,
									   status.id as statusID,
									   status.name as statusName,
									   caseStatus.created as caseStatusCreated,
									   missingCase.status as missingCaseStatus,
									   COUNT(missingCase.id) as totalMissingCases, 
									   COUNT(doctorCase.id) as totalDoctorCases,
									   drCaseStatus.name as doctorCaseStatusName
									   ');
												  
					$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');
					
					$this->db->where('medicalCase.status',HARD_CODE_ID_ACTIVATED);
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					
					/*if ($searchParameters['searchCaseID']) {
								
							$this->db->where('medicalCase.id',$searchParameters['searchCaseID']);
						
					}*/

					$count = count($searchParameters['searchCaseIDs']);
					if($count==1)
					{
						if ($searchParameters['searchCaseID']) 
						{	
							$this->db->where('medicalCase.id',$searchParameters['searchCaseID']);
					    }
					}
					else
					{
						if ($searchParameters['searchCaseIDs']) 
						{
							$this->db->where_in('medicalCase.id',$searchParameters['searchCaseIDs']);
						}
					}
					
					if ($searchParameters['searchPatientName']) {
								
								/*$this->db->where('medicalCase.patient',$searchParameters['searchPatientName']);*/
								$this->db->like('medicalCase.patient',$searchParameters['searchPatientName']);
						
					}
					
					if ($searchParameters['searchDistributor']) {
								
								$this->db->where('medicalCase.distributor',$searchParameters['searchDistributor']);
						
					}
					
					if ($searchParameters['searchCountry']) {
								
								$this->db->where('medicalCase.country',$searchParameters['searchCountry']);
						
					}

					//case received date from to date to search start 
					if ($searchParameters['searchReceivedDate'] && $searchParameters['searchReceivedDateTo'] == '') {
								
								$searchReceivedDate = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
								
								$this->db->where('medicalCase.receive_date',$searchReceivedDate);
						
					} else if ($searchParameters['searchReceivedDate'] == '' && $searchParameters['searchReceivedDateTo']) {
								
								$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
								
								$this->db->where('medicalCase.receive_date <=',$searchReceivedDateTo);
					
					 } else if ($searchParameters['searchReceivedDate'] != '' && $searchParameters['searchReceivedDateTo']!= '') {
								
								$searchReceivedDate   = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
								$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
								
								$this->db->where('medicalCase.receive_date >=',$searchReceivedDate);
								$this->db->where('medicalCase.receive_date <=',$searchReceivedDateTo);
					 }
					 //case received date from to date to search end
					 


					 //case approved by doctor date from to date to search start 
					if ($searchParameters['searchApprovedDate'] && $searchParameters['searchApprovedDateTo'] == '') {
								
								$searchApprovedDate = DATABASE_DATE_FORMAT($searchParameters['searchApprovedDate']);
								
								$this->db->where('doctorCaseHistory.receive_date',$searchApprovedDate);
								$this->db->where('doctorCaseHistory.status_id',APPROVED);
						
					} else if ($searchParameters['searchApprovedDate'] == '' && $searchParameters['searchApprovedDateTo']) {
								
								$searchApprovedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchApprovedDateTo']);
								
								$this->db->where('doctorCaseHistory.receive_date <=',$searchApprovedDateTo);
								$this->db->where('doctorCaseHistory.status_id',APPROVED);
					
					 } else if ($searchParameters['searchApprovedDate'] != '' && $searchParameters['searchApprovedDateTo']!= '') {
								
								$searchApprovedDate   = DATABASE_DATE_FORMAT($searchParameters['searchApprovedDate']);
								$searchApprovedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchApprovedDateTo']);
								
								$this->db->where('doctorCaseHistory.receive_date >=',$searchApprovedDate);
								$this->db->where('doctorCaseHistory.receive_date <=',$searchApprovedDateTo);
								$this->db->where('doctorCaseHistory.status_id',APPROVED);
					 }
					 //case approved by doctor date from to date to search end


					
					if ($searchParameters['searchStage'] && $searchParameters['searchStage'] != RECEIVE_ORDER_ENTRY) {
							
						 $this->db->where('caseStage.production_stage_id =',$searchParameters['searchStage']);
					}
					
					if ($searchParameters['searchStatus']) {
								
								if ($searchParameters['searchStatus'] == 'ready') {
										
									   $this->db->where('caseStage.production_stage_id !=',WAITING_FOR_APPROVAL);
									   $this->db->where_in('caseStatus.status_id',$this->caseStatusReadyArray);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'hold') {
										
									   $this->db->where_in('caseStatus.status_id',$this->caseStatusHoldArray);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'process') {
									   
									   $process_Plus_OFFHold_array = array_merge($this->caseStatusProcessArray,$this->caseStatusOFFHoldArray);
									   
									    $this->db->where_in('caseStatus.status_id',$process_Plus_OFFHold_array);
								}
								
								if ($searchParameters['searchStatus'] == 'modification') {
										
									   $this->db->where('caseOperator.modification_case',HARD_CODE_ID_YES);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'waiting') {
										
									  $this->db->where('caseStage.production_stage_id',WAITING_FOR_APPROVAL);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'reject') {
										
									  $this->db->where('caseOperator.rejected_case',HARD_CODE_ID_YES);
									  
									  $process_Plus_OFFHold_array = array_merge($this->caseStatusProcessArray,$this->caseStatusOFFHoldArray);
									  $this->db->where_in('caseStatus.status_id',$process_Plus_OFFHold_array);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'shipped') {
										
									 $this->db->where('medicalCase.ship',HARD_CODE_ID_NO);
									 $this->db->where('caseStatus.status_id',CASE_STATUS_QUALITY_OF_CASES_DONE);
									  
								}
					}
					
					$this->db->join(COUNTRIES_TABLE.' as country', 'country.id = medicalCase.country', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');
					
					$this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as createdEmployee', 'createdEmployee.id = medicalCase.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as createdHR', 'createdEmployee.HR_id = createdHR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as createdPrefix', 'createdHR.prefix = createdPrefix.id', 'left');


                    //change by fahad
					$this->db->join(MISSING_CASES_TABLE.' as missingCase', 'medicalCase.id = missingCase.case_id', 'left');

					$this->db->join(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase', 'medicalCase.id = doctorCase.case_id', 'left');

					$this->db->join(MEDICAL_DOCTOR_ASSIGN_CASE_STATUS_TABLE.' as doctorCaseStatus', 'doctorCaseStatus.case_id = doctorCase.case_id', 'left');

					$this->db->join(MEDICAL_DOCTOR_CASE_STATUS_TABLE.' as drCaseStatus', 'doctorCase.status_id = drCaseStatus.id', 'left');

					$this->db->join(MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE.' as doctorCaseHistory', 'medicalCase.id = doctorCaseHistory.case_id', 'left');



					
					$this->db->order_by('medicalCase.created', 'DESC');
				    
					$this->db->group_by('medicalCase.id');
					
					$this->db->limit($recordperpage , $page);
					
					$result = $this->db->get();  
					
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
		
		}







		function doctorCasesListing($page,$recordperpage,$searchParameters,$teamID) {
					
					if ($searchParameters['searchTimePeriod'] == 'due') {
							
							$dueIDs = $this->dueSetupTimelineCases();
					}
					
					if ($searchParameters['searchTimePeriod'] == 'before') {
							
							$beforeIDs = $this->beforeSetupTimelineCases();
					}
					
					if ($searchParameters['searchTimePeriod'] == 'late-complete') {
							
							$lateIDs = $this->lateSetupTimelineCases();
					}
					
					if ($searchParameters['searchTimePeriod'] == 'incomplete') {
							
							$delayIDs = $this->delaySetupTimelineCases();
					}
					
					$this->db->select('doctorCase.doctor_id as doctorID,
						               doctorCase.link as caseLink,
						               doctorCase.code as caseCode,
						               medicalCase.id as tableID,
									   medicalCase.id as caseID,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.hold as caseHoldFlag,
									   medicalCase.ship as caseShippedFlag,
									   country.id as countryID,
									   country.name as countryName,
									   company.id as companyID,
									   company.name as companyName,
									   createdEmployee.id as employeeTeamID,
									   createdEmployee.id as employeeID,
									   createdEmployee.code as employeeCode,
									   createdEmployee.employee_number as employeeNumber,
									   createdHR.name as employeeName,
									   createdHR.photo as employeePhoto,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName,
									   status.id as statusID,
									   status.name as statusName,
									   caseStatus.created as caseStatusCreated,
									   COUNT(doctorCase.id) as totalDoctorCases,
									   doctorCase.status_id as statusID,
									   drCaseStatus.name as doctorCaseStatusName,
									   doctorCase.created as doctorCaseCreated');
												  
					/*$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');*/
					 $this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
					
					$this->db->where('doctorCase.doctor_id',$teamID);
					$this->db->where('medicalCase.status',HARD_CODE_ID_ACTIVATED);
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					
					if ($searchParameters['searchCaseID']) {
								
								$this->db->where('medicalCase.id',$searchParameters['searchCaseID']);
						
					}
					
					if ($searchParameters['searchPatientName']) {
								
								/*$this->db->where('medicalCase.patient',$searchParameters['searchPatientName']);*/
								$this->db->like('medicalCase.patient',$searchParameters['searchPatientName']);
						
					}
					
					if ($searchParameters['searchDistributor']) {
								
								$this->db->where('medicalCase.distributor',$searchParameters['searchDistributor']);
						
					}
					
					if ($searchParameters['searchCountry']) {
								
								$this->db->where('medicalCase.country',$searchParameters['searchCountry']);
						
					}
					
					if ($searchParameters['searchReceivedDate'] && $searchParameters['searchReceivedDateTo'] == '') {
								
								$searchReceivedDate = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
								
								$this->db->where('medicalCase.receive_date',$searchReceivedDate);
						
					} else if ($searchParameters['searchReceivedDate'] == '' && $searchParameters['searchReceivedDateTo']) {
								
								$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
								
								$this->db->where('medicalCase.receive_date <=',$searchReceivedDateTo);
					
					 } else if ($searchParameters['searchReceivedDate'] != '' && $searchParameters['searchReceivedDateTo']!= '') {
								
								$searchReceivedDate   = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
								$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
								
								$this->db->where('medicalCase.receive_date >=',$searchReceivedDate);
								$this->db->where('medicalCase.receive_date <=',$searchReceivedDateTo);
					 }
					
					/*if ($searchParameters['searchStage'] && $searchParameters['searchStage'] != RECEIVE_ORDER_ENTRY) {
							
						 $this->db->where('caseStage.production_stage_id =',$searchParameters['searchStage']);
					}*/
					
					if ($searchParameters['searchStatus']) {
								
								if ($searchParameters['searchStatus'] == 'ready') {
										
									   $this->db->where('caseStage.production_stage_id !=',WAITING_FOR_APPROVAL);
									   $this->db->where_in('caseStatus.status_id',$this->caseStatusReadyArray);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'hold') {
										
									   $this->db->where_in('caseStatus.status_id',$this->caseStatusHoldArray);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'process') {
									   
									   $process_Plus_OFFHold_array = array_merge($this->caseStatusProcessArray,$this->caseStatusOFFHoldArray);
									   
									    $this->db->where_in('caseStatus.status_id',$process_Plus_OFFHold_array);
								}
								
								if ($searchParameters['searchStatus'] == 'modification') {
										
									   $this->db->where('caseOperator.modification_case',HARD_CODE_ID_YES);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'waiting') {
										
									  $this->db->where('caseStage.production_stage_id',WAITING_FOR_APPROVAL);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'reject') {
										
									  $this->db->where('caseOperator.rejected_case',HARD_CODE_ID_YES);
									  
									  $process_Plus_OFFHold_array = array_merge($this->caseStatusProcessArray,$this->caseStatusOFFHoldArray);
									  $this->db->where_in('caseStatus.status_id',$process_Plus_OFFHold_array);
									  
								}
								
								if ($searchParameters['searchStatus'] == 'shipped') {
										
									 $this->db->where('medicalCase.ship',HARD_CODE_ID_NO);
									 $this->db->where('caseStatus.status_id',CASE_STATUS_QUALITY_OF_CASES_DONE);
									  
								}
					}

					//change by fahad starts
					$this->db->join(MEDICAL_CASES_TABLE.' as medicalCase', 'medicalCase.id = doctorCase.case_id', 'left');
					//change by fahad ends
					
					$this->db->join(COUNTRIES_TABLE.' as country', 'country.id = medicalCase.country', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');
					
					$this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as createdEmployee', 'createdEmployee.id = medicalCase.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as createdHR', 'createdEmployee.HR_id = createdHR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as createdPrefix', 'createdHR.prefix = createdPrefix.id', 'left');


                    //change by fahad
					/*$this->db->join(MISSING_CASES_TABLE.' as missingCase', 'medicalCase.id = missingCase.case_id', 'left');*/
          
					$this->db->join(MEDICAL_DOCTOR_CASE_STATUS_TABLE.' as drCaseStatus', 'doctorCase.status_id = drCaseStatus.id', 'left');

					/*$this->db->join(MEDICAL_DOCTOR_CASE_STATUS_TABLE.' as drCaseStatus', 'doctorCaseStatus.status_id = drCaseStatus.id', 'left');*/

					/*$this->db->join(MEDICAL_DOCTOR_ASSIGN_CASE_STATUS_TABLE.' as doctorCaseStatus', 'doctorCaseStatus.case_id = doctorCase.case_id', 'left');*/



					
					$this->db->order_by('medicalCase.created', 'DESC');
				    
					$this->db->group_by('medicalCase.id');
					
					$this->db->limit($recordperpage , $page);
					
					$result = $this->db->get();  
					
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
		
		}





	
		function casesNum($searchParameters) {
					
					$this->db->select('medicalCase.id as tableID');
												  
					$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');
					
					
					$this->db->where('medicalCase.status',HARD_CODE_ID_ACTIVATED);
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					
					
					/*if ($searchParameters['searchCaseID']) {
								
								$this->db->where('medicalCase.id',$searchParameters['searchCaseID']);
						
					}*/

					$count = count($searchParameters['searchCaseIDs']);
					if($count==1)
					{
						if ($searchParameters['searchCaseID']) 
						{	
							$this->db->where('medicalCase.id',$searchParameters['searchCaseID']);
					    }
					}
					else
					{
						if ($searchParameters['searchCaseIDs']) 
						{
							$this->db->where_in('medicalCase.id',$searchParameters['searchCaseIDs']);
						}
					}
					
					if ($searchParameters['searchPatientName']) {
								
								$this->db->where('medicalCase.patient',$searchParameters['searchPatientName']);
						
					}
					
					if ($searchParameters['searchDistributor']) {
								
								$this->db->where('medicalCase.distributor',$searchParameters['searchDistributor']);
						
					}
					
					if ($searchParameters['searchCountry']) {
								
								$this->db->where('medicalCase.country',$searchParameters['searchCountry']);
						
					}



					//case received date from to date to search start
					if ($searchParameters['searchReceivedDate'] && $searchParameters['searchReceivedDateTo'] == '') {
								
								$searchReceivedDate = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
								
								$this->db->where('medicalCase.receive_date',$searchReceivedDate);
						
					} else if ($searchParameters['searchReceivedDate'] == '' && $searchParameters['searchReceivedDateTo']) {
								
								$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
								
								$this->db->where('medicalCase.receive_date <=',$searchReceivedDateTo);
					
					 } else if ($searchParameters['searchReceivedDate'] != '' && $searchParameters['searchReceivedDateTo']!= '') {
								
								$searchReceivedDate   = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDate']);
								$searchReceivedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchReceivedDateTo']);
								
								$this->db->where('medicalCase.receive_date >=',$searchReceivedDate);
								$this->db->where('medicalCase.receive_date <=',$searchReceivedDateTo);
					 }
					 //case received date from to date to search end
					


					//case approved by doctor date from to date to search start 
					if ($searchParameters['searchApprovedDate'] && $searchParameters['searchApprovedDateTo'] == '') {
								
								$searchApprovedDate = DATABASE_DATE_FORMAT($searchParameters['searchApprovedDate']);
								
								$this->db->where('doctorCaseHistory.receive_date',$searchApprovedDate);
								$this->db->where('doctorCaseHistory.status_id',APPROVED);
						
					} else if ($searchParameters['searchApprovedDate'] == '' && $searchParameters['searchApprovedDateTo']) {
								
								$searchApprovedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchApprovedDateTo']);
								
								$this->db->where('doctorCaseHistory.receive_date <=',$searchApprovedDateTo);
								$this->db->where('doctorCaseHistory.status_id',APPROVED);
					
					 } else if ($searchParameters['searchApprovedDate'] != '' && $searchParameters['searchApprovedDateTo']!= '') {
								
								$searchApprovedDate   = DATABASE_DATE_FORMAT($searchParameters['searchApprovedDate']);
								$searchApprovedDateTo = DATABASE_DATE_FORMAT($searchParameters['searchApprovedDateTo']);
								
								$this->db->where('doctorCaseHistory.receive_date >=',$searchApprovedDate);
								$this->db->where('doctorCaseHistory.receive_date <=',$searchApprovedDateTo);
								$this->db->where('doctorCaseHistory.status_id',APPROVED);
					 }
					 //case approved by doctor date from to date to search end
					 


					
					 if ($searchParameters['searchStage'] && $searchParameters['searchStage'] != RECEIVE_ORDER_ENTRY) {
							
						 $this->db->where('caseStage.production_stage_id =',$searchParameters['searchStage']);
					 }
					
					if ($searchParameters['searchStatus']) {
								
							  if ($searchParameters['searchStatus'] == 'ready') {
									  
									 $this->db->where('caseStage.production_stage_id !=',WAITING_FOR_APPROVAL);
									 $this->db->where_in('caseStatus.status_id',$this->caseStatusReadyArray);
									
							  }
							  
							  if ($searchParameters['searchStatus'] == 'hold') {
									  
									 $this->db->where_in('caseStatus.status_id',$this->caseStatusHoldArray);
									
							  }
							  
							  if ($searchParameters['searchStatus'] == 'process') {
									 
										$process_Plus_OFFHold_array = array_merge($this->caseStatusProcessArray,$this->caseStatusOFFHoldArray);
									 
										$this->db->where_in('caseStatus.status_id',$process_Plus_OFFHold_array);
							  }
							  
							  if ($searchParameters['searchStatus'] == 'modification') {
									  
									 $this->db->where('caseOperator.modification_case',HARD_CODE_ID_YES);
									
							  }
							  
							  if ($searchParameters['searchStatus'] == 'waiting') {
									  
									$this->db->where('caseStage.production_stage_id',WAITING_FOR_APPROVAL);
									
							  }
							  
							  if ($searchParameters['searchStatus'] == 'reject') {
									  
									$this->db->where('caseOperator.rejected_case',HARD_CODE_ID_YES);
									
									$process_Plus_OFFHold_array = array_merge($this->caseStatusProcessArray,$this->caseStatusOFFHoldArray);
									$this->db->where_in('caseStatus.status_id',$process_Plus_OFFHold_array);
									
							  }
					   }
						
					$this->db->join(COUNTRIES_TABLE.' as country', 'country.id = medicalCase.country', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');
					
					$this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');
					
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as createdEmployee', 'createdEmployee.id = medicalCase.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as createdHR', 'createdEmployee.HR_id = createdHR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as createdPrefix', 'createdHR.prefix = createdPrefix.id', 'left');

					$this->db->join(MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE.' as doctorCaseHistory', 'medicalCase.id = doctorCaseHistory.case_id', 'left');	
					
					$this->db->group_by('medicalCase.id');
		         	
					$result = $this->db->get(); 
		
					return $result->num_rows();
		}
		
		function getCaseInfo($caseID) {
				 
					$this->db->select('medicalCase.id as tableID,
									   medicalCase.id as caseID,
									   medicalCase.created as caseCreated,
									   medicalCase.patient as patientName,
									   medicalCase.age as patientAge,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.arch_upper as archUpper,
									   medicalCase.arch_lower as archLower,
									   medicalCase.description as caseComments,
									   medicalCase.ship as caseShipped,
									   medicalCase.RX_form as RX_form,
									   medicalCase.x_rays_opg as x_rays_opg,
									   medicalCase.x_rays_ceph as x_rays_ceph,
									   medicalCase.file_assessment as file_assessment,
									   medicalCase.distributor as distributor,
									   medicalCase.patient_state as patient_state,
									   gender.id as genderID,
									   gender.name as genderName,
									   country.id as countryID,
									   country.name as countryName,
									   company.id as companyID,
									   company.name as companyName,
									   company.logo as companyLogo,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');
					
					$this->db->where('medicalCase.id',$caseID);
					
					$this->db->join(COUNTRIES_TABLE.' as country', 'country.id = medicalCase.country', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');
					
					$this->db->join(GENDER_TABLE.' as gender', 'medicalCase.gender = gender.id', 'left');
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = medicalCase.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
						
					$result = $this->db->get();
					
					return $result;
		}
		
		function getCaseStatusComments($caseID) {
				
					$this->db->select('caseStatus.case_id as caseID,
									   caseStatus.description as caseStatusComments,
									   caseStatus.created_by as caseStatusCreatedBy,
									   caseStatus.created as caseStatusCreated,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MEDICAL_CASE_STATUS_TABLE.' as caseStatus');
					
					$this->db->where('caseStatus.case_id',$caseID);
					$this->db->where('caseStatus.description !=','');
					$this->db->where('caseStatus.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = caseStatus.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');	
					
					$this->db->order_by('caseStatus.id', 'DESC');
					
					$result = $this->db->get();
					
					return $result;
					
				
		}
		
		function getKitSendReceiveCustomerHistory($caseID) {
				
					$this->db->select('kitSendReceive.id as tableID,
									   kitSendReceive.case_id as caseID,
									   kitSendReceive.tracking_number_sending as trackingNumberSending,
									   kitSendReceive.tracking_number_receiving as trackingNumberReceiving,
									   deliveryServiceSend.id as deliveryServiceSendID,
									   deliveryServiceSend.name as deliveryServiceSendName,
									   deliveryServiceReceive.id as deliveryServiceReceiveID,
									   deliveryServiceReceive.name as deliveryServiceReceiveName,
									   kitSendReceive.created as kitSendReceiveCreated,
									   kitSendReceive.created_by as kitSendReceiveCreatedBy,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MEDICAL_KIT_SEND_RECEIVE_CUSTOMER_TABLE.' as kitSendReceive');
					$this->db->join(DELIVERY_SERVICES_TABLE.' as deliveryServiceSend', 'deliveryServiceSend.id = kitSendReceive.delivery_service_sending', 'left');
					$this->db->join(DELIVERY_SERVICES_TABLE.' as deliveryServiceReceive', 'deliveryServiceReceive.id = kitSendReceive.delivery_service_receiving', 'left');
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = kitSendReceive.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					
					$this->db->where('kitSendReceive.case_id',$caseID);
					$this->db->where('kitSendReceive.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->order_by('kitSendReceive.id','DESC');
					
					$result = $this->db->get(); 
					
					return $result;
					
				
	}
	
		function getImpressionsSendReceiveProductionHistory($caseID) {
				
					$this->db->select('impressionSendReceive.id as tableID,
									   impressionSendReceive.case_id as caseID,
									   impressionSendReceive.tracking_number_sending as trackingNumberSending,
									   impressionSendReceive.tracking_number_receiving as trackingNumberReceiving,
									   deliveryServiceSend.id as deliveryServiceSendID,
									   deliveryServiceSend.name as deliveryServiceSendName,
									   deliveryServiceReceive.id as deliveryServiceReceiveID,
									   deliveryServiceReceive.name as deliveryServiceReceiveName,
									   impressionSendReceive.created as impressionsSendReceiveCreated,
									   impressionSendReceive.created_by as impressionsSendReceiveCreatedBy,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto');
											  
					$this->db->from(MEDICAL_IMPRESSIONS_SEND_RECEIVE_PRODUCTION_TABLE.' as impressionSendReceive');
					$this->db->join(DELIVERY_SERVICES_TABLE.' as deliveryServiceSend', 'deliveryServiceSend.id = impressionSendReceive.delivery_service_sending', 'left');
					$this->db->join(DELIVERY_SERVICES_TABLE.' as deliveryServiceReceive', 'deliveryServiceReceive.id = impressionSendReceive.delivery_service_receiving', 'left');
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = impressionSendReceive.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					
					$this->db->where('impressionSendReceive.case_id',$caseID);
					$this->db->where('impressionSendReceive.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->order_by('impressionSendReceive.id','DESC');
					
					$result = $this->db->get(); 
					
					return $result;
					
				
		}
		
		function getAlignersSendReceiveCustomerHistory($caseID) {
				
			$this->db->select('alignersSendReceive.id as tableID,
							   alignersSendReceive.case_id as caseID,
							   alignersSendReceive.tracking_number_sending as trackingNumberSending,
							   deliveryServiceSend.id as deliveryServiceSendID,
							   deliveryServiceSend.name as deliveryServiceSendName,
							   alignersSendReceive.created as alignersSendReceiveCreated,
							   alignersSendReceive.created_by as alignersSendReceiveCreatedBy,
							   alignersSendReceive.id as employeeTeamID,
							   organizationEmployee.id as employeeID,
							   organizationEmployee.code as employeeCode,
							   organizationEmployee.employee_number as employeeNumber,
							   HR.name as employeeName,
							   HR.photo as employeePhoto');
									  
			$this->db->from(MEDICAL_ALIGNERS_SEND_RECEIVE_PRODUCTION_TABLE.' as alignersSendReceive');
			$this->db->join(DELIVERY_SERVICES_TABLE.' as deliveryServiceSend', 'deliveryServiceSend.id = alignersSendReceive.delivery_service_sending', 'left');
			$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = alignersSendReceive.created_by', 'left');
			$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
			
			$this->db->where('alignersSendReceive.case_id',$caseID);
			$this->db->where('alignersSendReceive.is_deleted',HARD_CODE_ID_NO);
			
			$this->db->order_by('alignersSendReceive.id','DESC');
			
			$result = $this->db->get(); 
			
			return $result;
				
		}
		
		function getStageStatus($stageID) {
					
					$this->db->select('status.id as tableID,
									   status.id as statusID,
									   status.name as statusName,
									   status.parent as statusParent');
					 
					 $this->db->from(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status');
					
					 $this->db->where('status.parent',HARD_CODE_ID_PARENT_OR_INDEPENDENT);
					
					 $this->db->where('status.production_stage_id',$stageID);
					 $this->db->where('status.is_deleted',HARD_CODE_ID_NO);
					
					 $this->db->order_by('status.order', 'ASC');
					 
					 $result = $this->db->get(); 
		
					 if ($result->num_rows() > 0) {
		
		       				return $result;
		
				     } else {
		
		    		 		return false;
		   		     }
		
	}
	
		function getStatusChilds($statusID) {
					
					$this->db->select('status.id as tableID,
									   status.id as statusID,
									   status.name as statusName,
									   status.parent as statusParent');
					 
					 $this->db->from(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status');
					
					 $this->db->where('status.parent',$statusID);
					
					 $this->db->where('status.is_deleted',HARD_CODE_ID_NO);
					
					 $this->db->order_by('status.order', 'ASC');
					 
					 $result = $this->db->get(); 
		
					 if ($result->num_rows() > 0) {
		
		       				return $result;
		
				     } else {
		
		    		 		return false;
		   		     }
		
	}
		
		function getStageOperators($stageID) {
					
					$this->db->select('workers.id as tableID,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   organizationEmployee.code as employeeCode,
									   HR.mobile as employeeMobile,
									   HR.photo as employeePhoto');
					 
					 $this->db->from(MEDICAL_PRODUCTION_STAGE_WORKERS_TABLE.' as workers');
					 $this->db->where('workers.production_stage_id',$stageID);
					 $this->db->where('workers.is_deleted',HARD_CODE_ID_NO);
					
					 $this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = workers.team_id', 'left'); 
					 $this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					 $this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					
					 $this->db->order_by('organizationEmployee.code', 'ASC');
					 $this->db->order_by('HR.name', 'ASC');
					
					 $result = $this->db->get(); 
		
					 if ($result->num_rows() > 0) {
		
		       				return $result;
		
				     } else {
		
		    		 		return false;
		   		     }
		
	}
		
		function statusInfoByID($statusID) {
			
					$this->db->select('status.id as statusID,
									   status.name as statusName,
									   status.description as statusDescription,
									   status.parent as statusParentID,
									   status.production_stage_id as statusProductionStageID,
									  ');
											  
					$this->db->from(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status');
					
					$this->db->where('status.id',$statusID);
					$this->db->where('status.is_deleted',HARD_CODE_ID_NO);
						
					$result = $this->db->get();
					
					return $result;
		}
		
		function stageInfoByID($stageID) {
			
					$this->db->select('stage.id as stageID,
									   stage.name as stageName,
									   stage.description as stageDescription,
									   stage.parent as stageParentID');
											  
					$this->db->from(MEDICAL_PRODUCTION_STAGES_TABLE.' as stage');
					
					$this->db->where('stage.id',$stageID);
					$this->db->where('stage.is_deleted',HARD_CODE_ID_NO);
						
					$result = $this->db->get();
					
					return $result;
					
						
		}
	
		function statusName($statusID) {
			
					$this->db->select('status.name as statusName');
											  
					$this->db->from(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status');
					
					$this->db->where('status.id',$statusID);
					$this->db->where('status.is_deleted',HARD_CODE_ID_NO);
						
					$result = $this->db->get();
					
					return $result;
					
						
		}




		function getMissingCases() { 
				
					$this->db->select('missingCase.id as tableID,
						               COUNT(missingCase.id) as totalCases,
									   missingCase.case_id  as caseID,
									   missingCase.tracking_number_sending as trackingNumber,
									   deliveryServices.name as deliveryServicesName');
											  
					$this->db->from(MISSING_CASES_TABLE.' as missingCase');
					
					$this->db->where('missingCase.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(DELIVERY_SERVICES_TABLE.' as deliveryServices', 'missingCase.delivery_service_sending  = deliveryServices.id', 'left');
					
					/*$this->db->order_by('caseStatus.id', 'DESC');
					$this->db->limit(1); */

					$this->db->group_by('missingCase.tracking_number_sending');
					
						
					$result = $this->db->get();
					
					return $result;
		
	}



	function getMissingCasesDetails($tableID) 
	{
									
					$this->db->select('missingCase.tracking_number_sending as trackingNumber,
									   medicalCase.id as caseID,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.hold as caseHoldFlag,
									   medicalCase.ship as caseShippedFlag,
									   company.name as companyName,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName,
									   status.id as statusID,
									   status.name as statusName,
									   caseStatus.created as caseStatusCreated,
									   HR.name as employName
									   ');
												  
					/*$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');*/
					$this->db->from(MISSING_CASES_TABLE.' as missingCase');
					
					$this->db->where('missingCase.tracking_number_sending',$tableID);
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASES_TABLE.' as medicalCase', 'medicalCase.id = missingCase.case_id', 'left');

					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');

                     $this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');

					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');



					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as Team', 'Team.id = medicalCase.created_by', 'left');

					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'HR.id = Team.HR_id', 'left');
						
					$this->db->order_by('missingCase.case_id', 'DESC');
					$result = $this->db->get();  
		       		return $result;
		
	}




	function getMissingCasesList($page,$recordPerPage,$searchParameters) 
	{
									
					$this->db->select('missingCase.tracking_number_sending as trackingNumber,
									   medicalCase.id as tableID,
									   medicalCase.id as caseID,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.hold as caseHoldFlag,
									   medicalCase.ship as caseShippedFlag,
									   country.id as countryID,
									   country.name as countryName,
									   company.id as companyID,
									   company.name as companyName,
									   createdEmployee.id as employeeTeamID,
									   createdEmployee.id as employeeID,
									   createdEmployee.code as employeeCode,
									   createdEmployee.employee_number as employeeNumber,
									   createdHR.name as employeeName,
									   createdHR.photo as employeePhoto,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName,
									   status.id as statusID,
									   status.name as statusName,
									   caseStatus.created as caseStatusCreated,
									   missingCase.status as missingCaseStatus,
									   HR.name as employName
									   ');
												  
					/*$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');*/
					$this->db->from(MISSING_CASES_TABLE.' as missingCase');
					
					/*$this->db->where('missingCase.tracking_number_sending',$tableID);*/
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASES_TABLE.' as medicalCase', 'medicalCase.id = missingCase.case_id', 'left');

					$this->db->join(COUNTRIES_TABLE.' as country', 'country.id = medicalCase.country', 'left');
					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');

                     $this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');

					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');

					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as createdEmployee', 'createdEmployee.id = medicalCase.created_by', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as createdHR', 'createdEmployee.HR_id = createdHR.id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');



					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as Team', 'Team.id = medicalCase.created_by', 'left');

					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'HR.id = Team.HR_id', 'left');
						
					
					$result = $this->db->get();  
		       		return $result;
		
	}




	function getDoctorCases($tableID) 
	{
									
					$this->db->select('
									   medicalCase.id as caseID,
									   medicalCase.patient_state as caseState,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.hold as caseHoldFlag,
									   medicalCase.ship as caseShippedFlag,
									   doctorCase.status_id as caseStatusID,
									   company.name as companyName,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName,
									   status.id as statusID,
									   status.name as statusName,
									   caseStatus.created as caseStatusCreated,
									   HR.name as employName
									   ');
												  
					/*$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');*/
					$this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
					
					$this->db->where('doctorCase.doctor_id',$tableID);
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASES_TABLE.' as medicalCase', 'medicalCase.id = doctorCase.case_id', 'left');

					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');

                     $this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');

					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');



					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as Team', 'Team.id = doctorCase.doctor_id', 'left');

					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'HR.id = Team.HR_id', 'left');
						
					$this->db->order_by('doctorCase.case_id', 'DESC');
					$result = $this->db->get();  
		       		return $result;
		
	}


	function checkDoctorCase($caseID) 
	{
									
					$this->db->select('
						               doctorCase.doctor_id as doctorID,
						               doctorCase.case_id as doctorCaseID,
						               doctorCase.link as caseLink,
						               doctorCase.code as caseCode,
						               doctorCase.status_id as caseStatusID,
						               doctorCase.patient_approve_status as patientStatusID,
						               doctorCase.comment as caseComment,
						               doctorCase.created as created,
									   medicalCase.id as caseID,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.hold as caseHoldFlag,
									   medicalCase.ship as caseShippedFlag,
									   company.name as companyName,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName,
									   status.id as statusID,
									   status.name as statusName,
									   caseStatus.created as caseStatusCreated,
									   HR.name as employName
									   ');
												  
					/*$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');*/
					$this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
					
					$this->db->where('doctorCase.case_id',$caseID);
					$this->db->where('doctorCase.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('doctorCase.created !=', '');
					
					$this->db->join(MEDICAL_CASES_TABLE.' as medicalCase', 'medicalCase.id = doctorCase.case_id', 'left');

					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');

                     $this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');

					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');



					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as Team', 'Team.id = doctorCase.doctor_id', 'left');

					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'HR.id = Team.HR_id', 'left');
						
					$this->db->order_by('doctorCase.case_id', 'DESC');
					$result = $this->db->get();  
		       		return $result;
		
	}



	function checkDoctorCaseForPatient($caseID) 
	{
									
					$this->db->select('
						               doctorCase.doctor_id as doctorID,
						               doctorCase.case_id as doctorCaseID,
						               doctorCase.link as caseLink,
						               doctorCase.code as caseCode,
						               doctorCase.status_id as caseStatusID,
						               doctorCase.patient_approve_status as patientStatusID,
						               doctorCase.comment as caseComment,
						               doctorCase.created as created,
									   medicalCase.id as caseID,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.hold as caseHoldFlag,
									   medicalCase.ship as caseShippedFlag,
									   company.name as companyName,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName,
									   status.id as statusID,
									   status.name as statusName,
									   caseStatus.created as caseStatusCreated,
									   HR.name as employName
									   ');
												  
					/*$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');*/
					$this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
					
					$this->db->where('doctorCase.case_id',$caseID);
					$this->db->where('doctorCase.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASES_TABLE.' as medicalCase', 'medicalCase.id = doctorCase.case_id', 'left');

					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');

                     $this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');

					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');
					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');



					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as Team', 'Team.id = doctorCase.doctor_id', 'left');

					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'HR.id = Team.HR_id', 'left');
						
					$this->db->order_by('doctorCase.case_id', 'DESC');
					$result = $this->db->get();  
		       		return $result;
		
	}




	function countDoctorStageCases($stageID,$teamID) 
	{
									
					$this->db->select('
						               doctorCase.doctor_id as doctorID,
						               doctorCase.link as caseLink,
						               doctorCase.code as caseCode,
									   medicalCase.id as caseID,
									   medicalCase.patient as patientName,
									   medicalCase.receive_date as receiveDate,
									   medicalCase.hold as caseHoldFlag,
									   medicalCase.ship as caseShippedFlag,
									   company.name as companyName,
									   productionStage.id as productionStageID,
									   productionStage.name as productionStageName,
									   status.id as statusID,
									   status.name as statusName,
									   caseStatus.created as caseStatusCreated,
									   HR.name as employName,
                                       ');
												  
					/*$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');*/
					$this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
					
					$this->db->where('doctorCase.doctor_id',$teamID);
					$this->db->where('doctorCase.status_id',$stageID);
					$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MEDICAL_CASES_TABLE.' as medicalCase', 'medicalCase.id = doctorCase.case_id', 'left');

					$this->db->join(MEDICAL_COMPANIES_TABLE.' as company', 'company.id = medicalCase.distributor', 'left');

                     $this->db->join(MEDICAL_CASE_PRODUCTION_STAGE_TABLE.' as caseStage', 'caseStage.id = medicalCase.present_case_stage_id', 'left');

					$this->db->join(MEDICAL_PRODUCTION_STAGES_TABLE.' as productionStage', 'productionStage.id = caseStage.production_stage_id', 'left');
					
					$this->db->join(MEDICAL_CASE_STATUS_TABLE.' as caseStatus', 'caseStatus.id = medicalCase.present_case_status_id', 'left');

					$this->db->join(MEDICAL_PRODUCTION_STAGE_STATUS_TABLE.' as status', 'status.id = caseStatus.status_id', 'left');

					/*$this->db->join(MEDICAL_DOCTOR_ASSIGN_CASE_STATUS_TABLE.' as doctorCaseStatus', 'doctorCaseStatus.case_id = doctorCase.case_id', 'left');*/



					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as Team', 'Team.id = doctorCase.doctor_id', 'left');

					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'HR.id = Team.HR_id', 'left');
						
					$this->db->order_by('doctorCase.case_id', 'DESC');
					$result = $this->db->get();  
		       		return $result;
		
	}



	function countDoctorCases($teamID) 
	{
									
			$this->db->select('
				               doctorCase.doctor_id as doctorID,
				               doctorCase.link as caseLink,
				               doctorCase.code as caseCode,
							   medicalCase.id as caseID,
							   medicalCase.patient as patientName,
							   medicalCase.receive_date as receiveDate,
							   medicalCase.hold as caseHoldFlag,
							   medicalCase.ship as caseShippedFlag
	                           ');
										  
			/*$this->db->from(MEDICAL_CASES_TABLE.' as medicalCase');*/
			$this->db->from(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase');
			
			$this->db->where('doctorCase.doctor_id',$teamID);
			$this->db->where('medicalCase.is_deleted',HARD_CODE_ID_NO);
			
			$this->db->join(MEDICAL_CASES_TABLE.' as medicalCase', 'medicalCase.id = doctorCase.case_id', 'left');

				
			$this->db->order_by('doctorCase.case_id', 'DESC');
			$result = $this->db->get();  
	   		return $result;
		
	}



	function getDoctorCaseHistory($caseID) {
				
					$this->db->select('doctorCase.doctor_id as doctorID,
						               doctorCaseHistory.link as caseLink,
						               doctorCaseHistory.code as caseCode,
						               doctorCaseHistory.comment as caseComment,
						               doctorCase.status_id as caseStatusID,
									   doctorCaseHistory.created as doctorCaseHistoryCreated,
									   doctorCaseHistory.created_by as doctorCaseHistoryCreatedBy,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.employee_number as employeeNumber,
									   HR.name as employeeName,
									   HR.photo as employeePhoto,
									   drCaseStatus.name as doctorCaseStatusName');
											  
					$this->db->from(MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE.' as doctorCaseHistory');

					$this->db->join(MEDICAL_CASES_ASSIGN_TO_DOCTOR_TABLE.' as doctorCase', 'doctorCase.case_id = doctorCaseHistory.case_id', 'left');

					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationEmployee.id = doctorCaseHistory.created_by', 'left');

					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');

					$this->db->join(MEDICAL_DOCTOR_CASE_STATUS_TABLE.' as drCaseStatus', 'doctorCaseHistory.status_id = drCaseStatus.id', 'left');
					
					$this->db->where('doctorCaseHistory.case_id',$caseID);
					$this->db->where('doctorCaseHistory.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->order_by('doctorCaseHistory.id','DESC');
					
					$result = $this->db->get(); 
					
					return $result;
					
				
	}



	function getAssignedDoctorCaseHistory($caseID) {
				
					$this->db->select('
									   doctorCaseHistory.created as doctorCaseHistoryCreated,
									   doctorCaseHistory.created_by as doctorCaseHistoryCreatedBy');
											  
					$this->db->from(MEDICAL_DOCTOR_ASSIGN_CASE_HISTORY_TABLE.' as doctorCaseHistory');

					
					$this->db->where('doctorCaseHistory.case_id',$caseID);
					$this->db->where('doctorCaseHistory.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->order_by('doctorCaseHistory.id','ASC');
					
					$result = $this->db->get(); 
					
					return $result;
					
				
	}



	function getPatientCasePhotos($caseID) {
					
					  $this->db->select('organizationOfficialAccount.id as tableID,
										 organizationOfficialAccount.id  as accountID,
										 organizationOfficialAccount.code  as Case_ID,
										 organizationOfficialAccount.account_status  as officialAccountStatus,
										 patientPicture.name  as pictureName,
										 patientPicture.type  as pictureType,
										 patientPicture.public_url  as picturePublicURL
										 ');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as organizationOfficialAccount');
				
					$this->db->where('organizationOfficialAccount.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('organizationOfficialAccount.type',HARD_CODE_ID_USER_TYPE_PATIENT);
					$this->db->where('organizationOfficialAccount.code',$caseID);

					/*$this->db->join(MEDICAL_CASE_PATIENT_PICTURES_TABLE.' as patientPicture', 'organizationOfficialAccount.id = patientPicture.account_id', 'left');*/

					$this->db->join(MEDICAL_CASE_PATIENT_PICTURES_TABLE.' as patientPicture', 'patientPicture.account_id = organizationOfficialAccount.id', 'left');
					
					/*$this->db->order_by('organizationOfficialAccount.id', 'ASC');
					
					$this->db->group_by('organizationOfficialAccount.id');*/
					
					$result = $this->db->get(); 
		  	
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}





 }
?>
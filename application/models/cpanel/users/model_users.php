<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

class Model_users extends CI_Model {
	
	function getModules() {
						
					$this->db->select('module.id as tableID,module.id as moduleID,module.name_singular as moduleSingularName,module.name_plural as modulePluralName');
											  
					$this->db->from(MODULES_TABLE.' as module');
					$this->db->where('module.parent',0);
					$this->db->order_by('module.order', 'ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}
	
	function getRoles($organizationID) {
						
					$this->db->select('organizationRole.id as tableID,role.id as roleID,role.name as roleName,role.sef_url as roleSefURL,role.description as roleDescription');
											  
					$this->db->from(MY_ORGANIZATION_JOB_ROLES_TABLE.' as organizationRole');
					
					$this->db->where('organizationRole.organization_id',$organizationID);
					$this->db->where('organizationRole.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(JOB_ROLES_TABLE.' as role', 'organizationRole.role_id = role.id', 'left');
					
					$this->db->order_by('organizationRole.order', 'ASC');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
		
	}
	
	function getRoleInfoByID($organizationID,$roleID) {
						
					$this->db->select('organizationRole.id as tableID,role.id as roleID,role.name as roleName,role.sef_url as roleSefURL,role.description as roleDescription');
											  
					$this->db->from(MY_ORGANIZATION_JOB_ROLES_TABLE.' as organizationRole');
					
					$this->db->where('organizationRole.organization_id',$organizationID);
					$this->db->where('organizationRole.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('role.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('role.id',$roleID);
					
					$this->db->join(JOB_ROLES_TABLE.' as role', 'organizationRole.role_id = role.id', 'left');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}
	
	function getRoleInfoBySefURL($organizationID,$sefURL) {
						
					$this->db->select('organizationRole.id as tableID,role.id as roleID,role.name as roleName,role.sef_url as roleSefURL,role.description as roleDescription');
											  
					$this->db->from(MY_ORGANIZATION_JOB_ROLES_TABLE.' as organizationRole');
					
					$this->db->where('organizationRole.organization_id',$organizationID);
					$this->db->where('organizationRole.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('role.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('role.sef_url',$sefURL);
					
					
					$this->db->join(JOB_ROLES_TABLE.' as role', 'organizationRole.role_id = role.id', 'left');
					
					$result = $this->db->get()->row(); 
		  			
					if ($result->num_rows() > 0) {
		
		       				return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
			
			
		
	}
	
	function getOfficialEmployees($organizationID,$searchParms) {
					
					  $this->db->select('organizationOfficialAccount.id as tableID,
					  					 organizationEmployee.id as employeeTeamID,
										 organizationEmployee.id as employeeID,
										 organizationEmployee.code as employeeCode,
										 HR.photo as employeePhoto,
										 organizationOfficialAccount.email as officialAccountEmail,
										 organizationOfficialAccount.username as username,
										 organizationOfficialAccount.mobile as officialAccountMobile,
										 organizationOfficialAccount.show_password as officialAccountPasswordVisible,
										 organizationOfficialAccount.account_status as officialAccountStatus,
										 prefix.id as prefixID,prefix.name as prefixName,
										 HR.name as employeeName,
										 organizationEmployee.email as employeeEmail,
										 HR.mobile_formatted as employeeMobileFormatted,
										 HR.mobile as employeeMobile,
										 teamRole.role_id as roleID');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as organizationOfficialAccount');
					
					if ($searchParms['searchCode']) {
					
						$this->db->where('organizationEmployee.code',$searchParms['searchCode']);
					}
					
					if ($searchParms['searchName']) {
					
						$this->db->like('HR.name',$searchParms['searchName']);
					}
					
					if ($searchParms['searchRole']) {
					
						$this->db->like('teamRole.role_id',$searchParms['searchRole']);
					}
					
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationOfficialAccount.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->where('organizationOfficialAccount.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationOfficialAccount.team_id = organizationEmployee.id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					
					$this->db->join(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE.' as teamRole', 'teamRole.team_id = organizationEmployee.id', 'left');
					
					$this->db->order_by('HR.name', 'ASC');
					
					$this->db->group_by('organizationOfficialAccount.team_id');
					
					$result = $this->db->get(); 
		  	
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}



	function getPatients($organizationID,$searchParms) {
					
					  $this->db->select('organizationOfficialAccount.id as tableID,
										 organizationOfficialAccount.id  as accountID,
										 organizationOfficialAccount.code  as CaseID,
										 patientPicture.id  as patientPictureID,
										 patientPicture.name  as pictureName,
										 patientPicture.type  as pictureType,
										 patientPicture.public_url  as picturePublicURL,
										 organizationOfficialAccount.email as officialAccountEmail,
										 organizationOfficialAccount.username as username,
										 organizationOfficialAccount.mobile as officialAccountMobile,
										 organizationOfficialAccount.show_password as officialAccountPasswordVisible,
										 organizationOfficialAccount.account_status as officialAccountStatus
										 ');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as organizationOfficialAccount');
				
					$this->db->where('organizationOfficialAccount.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('organizationOfficialAccount.type',HARD_CODE_ID_USER_TYPE_PATIENT);

					/*$this->db->join(MEDICAL_CASE_PATIENT_PICTURES_TABLE.' as patientPicture', 'organizationOfficialAccount.id = patientPicture.account_id', 'left');*/

					$this->db->join(MEDICAL_CASE_PATIENT_PICTURES_TABLE.' as patientPicture', 'patientPicture.account_id = organizationOfficialAccount.id', 'left');
					
					$this->db->order_by('organizationOfficialAccount.id', 'ASC');
					
					$this->db->group_by('organizationOfficialAccount.id');
					
					$result = $this->db->get(); 
		  	
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}



	function getPatientDetails($organizationID,$tableID) {
					
					  $this->db->select('organizationOfficialAccount.id as tableID,
										 organizationOfficialAccount.id  as accountID,
										 organizationOfficialAccount.code  as CaseID,
										 organizationOfficialAccount.account_status  as officialAccountStatus,
										 patientPicture.name  as pictureName,
										 patientPicture.type  as pictureType,
										 patientPicture.public_url  as picturePublicURL,
										 organizationOfficialAccount.email as officialAccountEmail,
										 organizationOfficialAccount.email as email,
										 organizationOfficialAccount.username as username,
										 organizationOfficialAccount.mobile as officialAccountMobile,
										 organizationOfficialAccount.show_password as officialAccountPasswordVisible,
										 organizationOfficialAccount.account_status as officialAccountStatus
										 ');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as organizationOfficialAccount');
				
					$this->db->where('organizationOfficialAccount.is_deleted',HARD_CODE_ID_NO);
					$this->db->where('organizationOfficialAccount.type',HARD_CODE_ID_USER_TYPE_PATIENT);
					$this->db->where('organizationOfficialAccount.id',$tableID);

					/*$this->db->join(MEDICAL_CASE_PATIENT_PICTURES_TABLE.' as patientPicture', 'organizationOfficialAccount.id = patientPicture.account_id', 'left');*/

					$this->db->join(MEDICAL_CASE_PATIENT_PICTURES_TABLE.' as patientPicture', 'patientPicture.account_id = organizationOfficialAccount.id', 'left');
					
					$this->db->order_by('organizationOfficialAccount.id', 'ASC');
					
					$this->db->group_by('organizationOfficialAccount.id');
					
					$result = $this->db->get(); 
		  	
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}

	
	function getEmployeeAssignedRoles($organizationID,$employeeID) {
						
					$this->db->select('teamRole.id as tableID,role.id as roleID,role.name as roleName');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_ROLES_TABLE.' as teamRole');
					
					$this->db->where('teamRole.organization_id',$organizationID);
					$this->db->where('teamRole.team_id',$employeeID);
					$this->db->where('teamRole.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->where('role.is_deleted',HARD_CODE_ID_NO);
					
					$this->db->join(JOB_ROLES_TABLE.' as role', 'teamRole.role_id = role.id', 'left');
					
					$result = $this->db->get(); 
		  			
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}
	
	function getEmployeesForUsers($organizationID) {
				
					$this->db->select('organizationEmployee.id as tableID,
									   organizationEmployee.id as employeeTeamID,
									   organizationEmployee.id as employeeID,
									   organizationEmployee.code as employeeCode,
									   HR.id as employeeHRID,prefix.id as prefixID,
									   prefix.name as prefixName,
									   HR.name as employeeName,
									   organizationEmployee.code as employeeCode,
									   organizationEmployee.email as employeeEmail,
									   HR.identity_card_formatted as employeeCNICFormatted,
									   HR.identity_card as employeeCNIC,
									   HR.personal_email as employeePersonalEmail,
									   HR.mobile_formatted as employeeMobileFormatted,
									   HR.mobile as employeeMobile');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee');
					
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_RESIGNED);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_LAYOFF);
					$this->db->where('organizationEmployee.employment_status !=',HARD_CODE_ID_TERMINATE);
					$this->db->where('organizationEmployee.is_deleted',HARD_CODE_ID_NO);
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					
					$this->db->order_by('organizationEmployee.code', 'ASC');
					
					$result = $this->db->get(); 
		
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}
	
	function getUserAccountInfoByID($organizationID,$tableID) {
					
					 $this->db->select('organizationOfficialAccount.id as tableID,
					 	                organizationOfficialAccount.username as username,
					  					organizationEmployee.id as employeeID,
										organizationEmployee.code as employeeCode,
										HR.name as employeeName,
										organizationEmployee.employee_number as employeeNumber,
										prefix.id as prefixID,prefix.name as prefixName,
										organizationOfficialAccount.account_status as officialAccountStatus');
											  
					$this->db->from(MY_ORGANIZATION_TEAM_OFFICIALS_TABLE.' as organizationOfficialAccount');
					$this->db->where('organizationOfficialAccount.id',$tableID);
					$this->db->where('organizationEmployee.organization_id',$organizationID);
					$this->db->where('organizationOfficialAccount.is_deleted',HARD_CODE_ID_NO);
					$this->db->join(MY_ORGANIZATION_TEAM_TABLE.' as organizationEmployee', 'organizationOfficialAccount.team_id = organizationEmployee.id', 'left');
					$this->db->join(APPLICATION_HR_TABLE.' as HR', 'organizationEmployee.HR_id = HR.id', 'left');
					$this->db->join(PREFIX_TABLE.' as prefix', 'HR.prefix = prefix.id', 'left');
					
				
					$this->db->order_by('HR.name', 'ASC');
					
					$result = $this->db->get(); 
		  	
					if ($result->num_rows() > 0) {
		
		       			return $result;
		
				   } else {
		
		    		 	return false;
		   		  }
	}
	
 }
?>
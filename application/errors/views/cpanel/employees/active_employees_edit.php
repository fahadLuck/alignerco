<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item">Employees</li>
        <li class="breadcrumb-item active">Edit Employ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      					<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
									$this->session->unset_userdata('admin_msg');
									$this->session->unset_userdata('admin_msg_error');
								 } 
			    		?>
     
      <!-- Basic Forms -->
     
     <form id="frm" name="frm" action="<?php echo base_url(); ?>employee-edit/<?php echo $tableID; ?>" method="post" enctype="multipart/form-data" autocomplete="off">

     	<input type="hidden" name="HRID" value="<?php echo $employees->HRID  ?>">
     	<input type="hidden" name="tableID" value="<?php echo $employees->tableID  ?>">
		
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="glyphicon glyphicon-briefcase"></i> Official </h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Prefix</label>
				  <div class="col-sm-10">
					<select name="prefix" id="prefix" class="form-control select2 <?php if (form_error('prefix')) { echo 'custom-select'; } ?>">
                          <option value="">Select Prefix</option>
                           <?php
                              if ($prefix) {
                                      
                                      foreach($prefix->result() as $pre) {
                                          
                                          if ($this->input->post('prefix')) {
                                                      
                                                      $selectPrefix = $this->input->post('prefix');	
                                          } else {
                                                     $selectPrefix = NULL;	
                                          }
                                          
                                          if ($pre->tableID ==  $selectPrefix) {
                                                              
                                                              $selectedPrefix  = 'selected = selected';
                                                  } else {
                                                              $selectedPrefix  = '';
                                                  }
                                      
                                         echo '<option value="'.$pre->tableID.'" '.$selectedPrefix.'>'.$pre->prefixName.'</option>';	
                                      }	
                              }
                          ?>
                    </select>
                    <?php if (form_error('prefix')) { echo form_error('prefix'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Name<span style="color:red;">*</span></label>
				  <div class="col-sm-10">
					<input name="name" class="form-control" type="text" value="<?php if(isset($employees) and $submit==''){ echo $employees->employeeName; } if(isset($submit) AND $employees){ echo $this->input->post('name'); } ?>">
                    <?php if (form_error('name')) { echo form_error('name'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Father Name</label>
				  <div class="col-sm-10">
					<input name="father_name" class="form-control" type="text" value="<?php if(isset($employees) and $submit==''){ echo $employees->fatherName; } if(isset($submit) AND $employees){ echo $this->input->post('father_name'); } ?>">
                    <?php if (form_error('father_name')) { echo form_error('father_name'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Email<span style="color:red;">*</span></label>
				  <div class="col-sm-10">
					<input name="email" class="form-control" type="text" value="<?php if(isset($employees) and $submit==''){ echo $employees->employeeEmail; } if(isset($submit) AND $employees){ echo $this->input->post('email'); } ?>" placeholder="This will be the login ID of employee..">
                    <?php if (form_error('email')) { echo form_error('email'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Code<span style="color:red;">*</span></label>
				  <div class="col-sm-10">
					<input name="employee_code" class="form-control" type="text" value="<?php if(isset($employees) and $submit==''){ echo $employees->employeeCode; } if(isset($submit) AND $employees){ echo $this->input->post('employee_code'); } ?>" placeholder="Employee code..">
                    <?php if (form_error('employee_code')) { echo form_error('employee_code'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Date of Joining</label>
				  <div class="col-sm-10">
					 <?php 
							  $joinDate = $this->input->post('employee_join_date');
							  
							  if (empty($joinDate)) {
										  
									$joinDate = JAVASCRIPT_DATE_FORMAT(); // Calling From Application Helper
							  } 
									
					  ?>
                    <input class="form-control" name="employee_join_date" id="employee_join_date" type="date" value="<?php echo $joinDate; ?>">
                    <?php if (form_error('employee_join_date')) { echo form_error('employee_join_date'); } ?>
                   
				  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Employment Status</label>
				  <div class="col-sm-10">
					
                   <?php $leftStatusArray = array(HARD_CODE_ID_SUSPENDED,HARD_CODE_ID_RESIGNED,HARD_CODE_ID_TERMINATE,HARD_CODE_ID_LAYOFF,HARD_CODE_ID_REJOINING_PER,HARD_CODE_ID_REJOINING_PROB); ?>
                    
                    <select name="employment_status" id="employment_status" class="form-control select2 <?php if (form_error('prefix')) { echo 'custom-select'; } ?>">
                          <option value="">Select Employment Status</option>
                           <?php
								  if ($employmentStatus) {
										  
										  foreach($employmentStatus->result() as $employment) {
											  
															  if (in_array($employment->tableID,$leftStatusArray)) {
																	
																	continue;  
															  }
															  
															  if ($this->input->post('employment_status')) {
																		  
																		  $selectEmploymentStatus = $this->input->post('employment_status');	
															  } else {
																		 $selectEmploymentStatus = NULL;	
															  }
															  
															  if ($employment->tableID ==  $selectEmploymentStatus) {
																				  
																				  $selectedEmployement  = 'selected = selected';
																	  } else {
																				  $selectedEmployement  = '';
																	  }
														  
														  echo '<option value="'.$employment->tableID.'" '.$selectedEmployement.'>'.$employment->employmentStatusName.' '.fillBrackets($employment->employmentStatusCode).'</option>';	
											 }	
										 }
							?>
	                    </select>
                    	<?php if (form_error('employment_status')) { echo form_error('employment_status'); } ?>
				  </div>
				</div>
             
              </div>  
            </div>
          </div>
        </div>
        
        <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="glyphicon glyphicon-user"></i> Personal </h3>
      </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Date of Birth</label>
				  <div class="col-sm-10">
					
					 <?php 
							   $dateOfBirth = $this->input->post('date_of_birth');
										  
							   if (empty($dateOfBirth)) {
													  
								   $dateOfBirth = JAVASCRIPT_DATE_FORMAT(); // Calling From Application Helper
							   } 
                                              
                     ?>
                    <input class="form-control" name="date_of_birth" id="date_of_birth" type="date" value="<?php echo $dateOfBirth; ?>">
                    <?php if (form_error('date_of_birth')) { echo form_error('date_of_birth'); } ?>
                   
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Gender<span style="color:red;">*</span></label>
				  <div class="col-sm-10">
					<select name="gender" id="gender" class="form-control select2">
                          <option value="">Select Gender</option>
                           <?php
									/*if ($gender) {
											
											foreach($gender->result() as $gen) {
												
												if ($employees->genderName) {
															
															$selectGender = $employees->genderName;	
												} else {
														   $selectGender = NULL;	
												}
												
												if ($gen->tableID ==  $selectGender) {
																	
																	$selectedGender  = 'selected = selected';
														} else {
																	$selectedGender  = '';
														}
											
											  echo '<option value="'.$gen->tableID.'" '.$selectedGender.'>'.$gen->genderName.' '.fillBrackets($gen->genderCode).'</option>';	
											}	
										}*/
							foreach($gender->result() as $gen) 
                            {?>
                               <?php
                            		if(isset($employees) and $submit=='')
                            		{
                            			?><option value="<?php echo $gen->genderID ?>" <?php echo (isset($employees) && $employees->genderName == $gen->genderName) ? ' selected="selected"' : '' ; ?>><?php echo $gen->genderName ?>  (<?php echo $gen->genderCode ?>)</option><?php
                            		}

                            		if(isset($employees) AND $submit!='')
                            		{
                            			$form_gender=$this->input->post('gender');
                            			?><option value="<?php echo $gen->genderID ?>" <?php echo $form_gender == $gen->genderID ? ' selected="selected"' : '' ; ?>><?php echo $gen->genderName ?>  (<?php echo $gen->genderCode ?>)</option><?php
                            		}
                            	?>
							
							<?php }  ?>
                     </select>
                    <?php if (form_error('gender')) { echo form_error('gender'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Nationality</label>
				  <div class="col-sm-10">
					<select name="nationality" id="nationality" class="form-control select2">
                          <option value="">Select Nationality</option>
                           <?php
								  if ($nationalities) {
										  
										  foreach($nationalities->result() as $nationality) {
											  
												if ($this->input->post('nationality')) {
															
															$selectNationality = $this->input->post('nationality');	
												} else {
														   $selectNationality = NULL;	
												}
												
												if ($nationality->tableID ==  $selectNationality) {
																	
																	$selectedNationality  = 'selected = selected';
														} else {
																	$selectedNationality  = '';
														}
											
											echo '<option value="'.$nationality->tableID.'" '.$selectedNationality.'>'.$nationality->nationalityName.'</option>';	
										  }	
								  }
							?>
                     </select>
                     <?php if (form_error('nationality')) { echo form_error('nationality'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Blood Group</label>
				  <div class="col-sm-10">
					<select name="blood_group" id="blood_group" class="form-control select2">
                          <option value="">Select Blood Group</option>
						   <?php
								if ($bloodGroups) {
										
									foreach($bloodGroups->result() as $bloodGroup) {
											
											if ($this->input->post('blood_group')) {
														
														$selectBlood = $this->input->post('blood_group');	
											} else {
														$selectBlood = NULL;	
											}
											
											if ($bloodGroup->tableID ==  $selectBlood) {
																
																$selectedBlood  = 'selected = selected';
													} else {
																$selectedBlood  = '';
													}
										
										echo '<option value="'.$bloodGroup->tableID.'" '.$selectedBlood.'>'.$bloodGroup->bloodGroupName.'</option>';	
									 }	
								}
							?>
                     </select>
                     <?php if (form_error('blood_group')) { echo form_error('blood_group'); } ?>
				  </div>
				</div>
               
              </div>  
            </div>
          </div>
        </div>
        
        <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="glyphicon glyphicon-envelope"></i> Contact </h3>
        </div>
      			
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Personal Email</label>
				  <div class="col-sm-10">
					<input name="personal_email" class="form-control" type="text" value="<?php if(isset($employees) and $submit==''){ echo $employees->personalEmail; } if(isset($submit) AND $employees){ echo $this->input->post('personal_email'); } ?>">
                    <?php if (form_error('personal_email')) { echo form_error('personal_email'); } ?>
				  </div>
				</div>
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">Mobile No.<span style="color:red;">*</span></label>
				  <div class="col-sm-10">
					<input name="mobile" class="form-control mask-mob-phone" type="text" value="<?php if(isset($employees) and $submit==''){ echo $employees->employeeMobileFormatted; } if(isset($submit) AND $employees){ echo $this->input->post('mobile'); } ?>">
                    <?php if (form_error('mobile')) { echo form_error('mobile'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Street Address</label>
				  <div class="col-sm-10">
					<input name="street_address" class="form-control" type="text" value="<?php echo $this->input->post('street_address'); ?>">
                    <?php if (form_error('street_address')) { echo form_error('street_address'); } ?>
				  </div>
				</div>
                
             	<div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">Country</label>
				  <div class="col-sm-10">
					<select name="country" id="country" class="form-control select2">
                          <option value="">Select Country</option>
                           <?php  
									 if ($countries) {
											
											if ($this->input->post('country')) {
														
													   $selectAble = $this->input->post('country');	
											} else {
													   $selectAble = HARD_CODE_ID_COUNTRY_PAKISTAN;	
											}
											
											
											foreach ($countries->result() as $country) {
													
												if ($country->id ==  $selectAble) {
															
															$selectedCoutry  = 'selected = selected';
												} else {
															$selectedCoutry  = '';
												}
							 ?>
                             <option value="<?php echo $country->id;  ?>" <?php echo $selectedCoutry; ?>><?php echo $country->name; ?></option>
                                            
							<?php 			}
                                       }
                            ?>
                    </select>
                    <?php if (form_error('country')) { echo form_error('country'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">State</label>
				  <div class="col-sm-10">
					<select name="state" id="state" class="form-control select2">
                          <option value="">Select State</option>
                           <?php  
								   if ($states) {
										  
										  if ($this->input->post('state')) {
													  
													  $selectAbleState = $this->input->post('state');	
										  } else {
													  $selectAbleState  = HARD_CODE_ID_STATE_PUNJAB;
										  }
										  
										  
										  foreach ($states->result() as $st) {
												  
											  if ($st->id ==  $selectAbleState) {
														  
														  $selectedState  = 'selected = selected';
											  } else {
														  $selectedState  = '';
											  }
							  ?>
                                      
                              <option value="<?php echo $st->id; ?>" <?php echo $selectedState; ?>><?php echo $st->name; ?></option>
                                            
                              <?php 		}
										 }
							  ?>
                    </select>
                    <?php if (form_error('state')) { echo form_error('state'); } ?>
				  </div>
				</div>
                
                <div class="form-group row" style="display:none;">
				  <label for="" class="col-sm-2 col-form-label">City</label>
				  <div class="col-sm-10">
					<select name="city" id="city" class="form-control select2">
                          <option value="">Select City</option>
                           <?php  
									 if ($cities) {
											
											if ($this->input->post('city')) {
														
														$selectAble = $this->input->post('city');	
											} else {
													   $selectAble  = HARD_CODE_ID_CITY_LAHORE;	
											}
											
											
											foreach ($cities->result() as $city) {
													
													if ($city->id ==  $selectAble) {
																
																$selectedCity  = 'selected = selected';
													} else {
																$selectedCity  = '';
													}
							  ?>
                                      
                               <option value="<?php echo $city->id;  ?>" <?php echo $selectedCity; ?>><?php echo $city->name; ?></option>
                                            
							  <?php 			}
                                        }
                               ?>
                    </select>
                    <?php if (form_error('city')) { echo form_error('city'); } ?>
				  </div>
				</div>
               	
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
				  <div class="col-sm-10 offset-md-2">
					<button class="btn btn-blue" type="submit" name="submit" value="save"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>

                    <button class="btn btn-warning" type="button" onclick="window.location.href='<?php echo base_url(); ?>employees/'">Cancel</button>
                  </div>
				</div>
                 
              </div>  
            </div>
          </div>
        </div>
        
        
 
     </div>
      
    </form>   
  
    </section>

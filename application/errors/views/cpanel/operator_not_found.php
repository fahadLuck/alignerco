<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url(); ?>backend_images/favicon.ico">
	
    <?php 
 
 			   if ( @$pageMeta && $metaType == 'external' ) {
				  
					  $pageName  			 	  = $pageMeta['name'];
					  $pageTitle 		      	  = $pageMeta['meta_title'];
					  $pageDescription  		  = $pageMeta['meta_description'];
					  $pageKeyword      	 	  = $pageMeta['meta_keyword'];
				  
			  } elseif ( $metaType == 'internal' ) {
				  
					  $pageName   		    = @$pageName;
					  $pageTitle 		    = @$pageTitle;
					  $pageDescription 		= @$pageDescription;
					  $pageKeywords       	= @$pageKeyword;
			  }
  ?>
    
    <title><?php echo $pageTitle; ?></title>
    
	<!-- Bootstrap v4.0.0-beta -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/master_style.css">
	
	<!-- maximum_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/skins/_all-skins.css">
	
	<!-- morris chart -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/morris.js/morris.css">
	
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/jvectormap/jquery-jvectormap.css">
	
	<!-- date picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
	
	<!-- daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css">
	
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
	
    <!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/select2/dist/css/select2.min.css">	

	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/master_style.css">

	<!-- maximum_admin Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/skins/_all-skins.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

     
  </head>
  
  <?php 
  	
		$loggingEmployee = getLoggingEmployee($this->session->userdata(USER_TEAM_ID_SESSION));
		
		if ($loggingEmployee) {
			
				$loggingEmployee 	= $loggingEmployee->row_array();	
				$employeeGender	 	= $loggingEmployee['employeeGenderID'];
					
				if ($employeeGender) {
							
							if ($employeeGender == HARD_CODE_ID_MALE)	 {
									
									$setSkinColor = 'skin-green';	
							}
							
							if ($employeeGender == HARD_CODE_ID_FEMALE)	 {
									
									$setSkinColor = 'skin-purple';	
							}
				} else {
					
					$setSkinColor = 'skin-yellow';	
				}
				
		} else {
				
				$setSkinColor = 'skin-green';	
		}
	
  
  ?>

<body class="hold-transition <?php echo $setSkinColor; ?> sidebar-collapse sidebar-mini"><div class="error-body">
      <div class="error-page">

        <div class="error-content">
         	<div class="container">
         	 <br />
             <br />
        		<h2 class="headline text-red">Not Found</h2>
        		<br />
             	<br /><br />
             	<br /><br />
             	<br />
			  <h3 class="margin-top-0">&nbsp;<i class="fa fa-warning text-red"></i> Operator not exist in any Production Stage !</h3>

			  <p>
				Please contact support department.
				<div class="text-center">
				  <a href="<?php echo base_url(); ?>logout/" class="btn btn-info btn-block btn-flat margin-top-10">Logout</a>
				</div>
			  </p>

          </div>
        </div>
        <!-- /.error-content -->
        <footer class="main-footer">
        	Copyright &copy; 2019 <a href="https://www.http://portal.clearpathortho.pk/">ClearPathorth</a>. All Rights Reserved.
		</footer>
 
      </div>
      <!-- /.error-page -->
     </div></body>
</html>

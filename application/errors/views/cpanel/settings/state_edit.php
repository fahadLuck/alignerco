<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item">State</li>
        <li class="breadcrumb-item active">Edit State</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      					<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
									$this->session->unset_userdata('admin_msg');
									$this->session->unset_userdata('admin_msg_error');
								 } 
			    		?>
     
      <!-- Basic Forms -->
     
     <form id="frm" name="frm" action="<?php echo base_url(); ?>state-edit/<?php echo $tableID; ?>" method="post" enctype="multipart/form-data" autocomplete="off">

     	<input type="hidden" name="tableID" value="<?php echo $activeState->tableID  ?>">
		
     <div class="box box-default">	
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	
                
                
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">State<span style="color:red;">*</span></label>
				  <div class="col-sm-10">
					<input name="state" class="form-control" type="text" value="<?php if(isset($activeState) AND $this->input->post('state')==''){ echo $activeState->stateName; } if($activeState AND $this->input->post('state')!=''){ echo $this->input->post('state'); } ?>">
                    <?php if (form_error('state')) { echo form_error('state'); } ?>
				  </div>
				</div>
                

				<div class="form-group row" style="">
				  <label for="" class="col-sm-2 col-form-label">Country</label>
				  <div class="col-sm-10">
					<select name="country" id="country" class="form-control select2">
                          <option value="">Select Country</option>
                           <?php  
									 if ($countries) {
											
											if ($this->input->post('country')) {
														
													   $selectAble = $this->input->post('country');	
											} else {
													   $selectAble =  $activeState->country_id;	
											}
											
											
											foreach ($countries->result() as $country) {
													
												if ($country->id ==  $selectAble) {
															
															$selectedCoutry  = 'selected = selected';
												} else {
															$selectedCoutry  = '';
												}
							 ?>
                             <option value="<?php echo $country->id;  ?>" <?php echo $selectedCoutry; ?>><?php echo $country->name; ?></option>
                                            
							<?php 			}
                                       }
                            ?>
                    </select>
                    <?php if (form_error('country')) { echo form_error('country'); } ?>
				  </div>
				</div>


	
               	
                <div class="form-group row">
				  <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
				  <div class="col-sm-10 offset-md-2">
					<button class="btn btn-blue" type="submit" name="submit" value="save"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>

                    <button class="btn btn-warning" type="button" onclick="window.location.href='<?php echo base_url(); ?>states/'">Cancel</button>
                  </div>
				</div>
                 
              </div>  
            </div>
          </div>
        </div>
        
        
 
     </div>
      
    </form>   
  
    </section>



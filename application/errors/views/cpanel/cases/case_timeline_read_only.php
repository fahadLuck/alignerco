<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.js"></script>

<script type="text/javascript" language="javascript">
        
    $(document).ready(function(){
  		
	   $('.checklistPopUp').click(function() {
					
					var caseID 				= $(this).attr('case');
					
					$('#case-quality-checklist-title').html('In Line Quality Checking Before Dispatch (Case #'+caseID+')');
					
					 $.ajax({
							  type   	  : "POST",
							  dataType 	  : "HTML",
							  url		  : "<?php echo base_url();?>load-case-quality-checklist/",
							  data		  : {  
												caseID 	: caseID,
													
											}
							
								 }).done(function(response) {
							
										if (response) {
												
												$('#case-quality-checklist-data').html(response);
										}
							  
						}); // END Ajax Request
					  
				  });
				  
	   $('.checklistPrint').click(function() {
			
			 $("#frm-quality-checklist").submit();  
	   });
 });
 
 </script>

	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>case-timeline-read-out/">Case Timeline</a></li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     	
        				<?php
								 if ($this->session->userdata('admin_msg') !='') {
									
										$alertClass	 	= 'alert-info';
										$alertHeading 	= 'Success';	 
										$alertMessage	= $this->session->userdata('admin_msg');
										
										$alertIcon 		= 'icon fa fa-check'; 	 
								 
								 } else if ($this->session->userdata('admin_msg_error')!='') {
								
									   $alertClass	 	= 'alert-danger';	 
									   $alertHeading 	= 'ERROR';	
									   $alertMessage 	= $this->session->userdata('admin_msg_error');
									   
									   $alertIcon 		= 'icon fa fa-ban'; 
								 }
								
								 if ( $this->session->userdata('admin_msg') !='' || $this->session->userdata('admin_msg_error') !='' )  {
						?>
								  <div class="alert <?php echo $alertClass; ?> alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="<?php echo $alertIcon; ?>"></i> <?php echo $alertHeading; ?>!</h4>
                                        <?php echo $alertMessage; ?>
                                  </div>
							
						<?php 
									
										$this->session->unset_userdata('admin_msg');
										$this->session->unset_userdata('admin_msg_error');
								  
								  } 
			    		?>
     
     <?php 
	 		$todayDate 						= date('d-m-Y',time());
			
			$caseID 						= $case['caseID'];
			$patientName 					= $case['patientName'];
			$portalNumber 					= $case['portalNumber'];
			$doctorName 					= $case['doctorName'];
			$natureOfPatient 				= $case['natureOfPatient'];
			$archUpper 						= $case['archUpper'];
			$archLower 						= $case['archLower'];
			$impressionID 					= $case['impressionID'];
			$impressionName 				= $case['impressionName'];
			$companyName 					= $case['companyName'];
			$companyLogo 					= $case['companyLogo'];
			$countryName 					= $case['countryName'];
			$cityName 						= $case['cityName'];
			$caseTypeID 					= $case['caseTypeID'];
			$caseTypeName 					= $case['caseTypeName'];
			$caseComments					= $case['caseComments'];
			$caseShipped					= $case['caseShipped'];
			$caseReceiveDate 				= $case['receiveDate'];
			$caseCreated					= $case['caseCreated'];
			$caseCreatedEmployeeName		= $case['employeeName'];
			$caseCreatedEmployeeCode		= $case['employeeCode'];
			$caseCreatedEmployeeID			= $case['employeeID'];
			
			$companyID 						= $case['companyID'];
			
			$employeeAssignJobs				= getEmployeeAssignJobs($caseCreatedEmployeeID); // Calling From HR Employees Helper
										
			if ($employeeAssignJobs) {
						
				$employeeAssignJob 								= $employeeAssignJobs->row_array();
				$caseCreatedEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
				
			
			} else {
					
					$caseCreatedEmployeeAssignJobDescription 	= NULL;
			}	   		
			
			$caseCreatedDate		= date('d F. Y',$caseCreated);
			$caseCreatedTime		= date('h:i A',$caseCreated);
			$caseCreatedTimeAgo		= timeAgo($caseCreated).' ago';
			
			$caseReceived			= date('l, d F Y',strtotime($caseReceiveDate));
			
			if (empty($doctorName)) {
				
					$doctorName  = '-----';
			}
			
			if (empty($caseTypeName)) {
				
					$caseTypeName  = '-----';
			}
			
			if (empty($caseComments)) {
				
					$caseComments  = '-----';
			}
		
	 ?>
     
     
     <div class="row">
       
        <div class="col-xl-4 col-lg-5">

          <div class="box box-primary">
            <div class="box-body box-profile">
             <?php if ($companyLogo) { ?>
              <img class="profile-user-img img-fluid mx-auto d-block" src="<?php echo base_url(); ?>backend_images/companies/<?php echo $companyLogo; ?>" alt="Company Logo">
             <?php } ?>
              <h3 class="profile-username text-center"><?php echo $patientName; ?></h3>

              <p class="text-muted text-center">Receive Date: <?php echo date('d F. Y',strtotime($caseReceiveDate)); ?></p>
			  
              <div class="row">
              	<div class="col-12">
              		<div class="profile-user-info">
						<p>Case ID </p>
						<h6 class="margin-bottom"><?php echo $caseID; ?></h6>
                        <p>Portal Number </p>
						<h6 class="margin-bottom"><?php echo $portalNumber; ?></h6>
						<p>Doctor</p>
						<h6 class="margin-bottom"><?php echo $doctorName; ?></h6> 
						<p>Nature Of Patient</p>
						<h6 class="margin-bottom"><?php echo $natureOfPatient; ?></h6>
                        <p>Impression Type</p>
						<h6 class="margin-bottom"><?php echo $impressionName; ?></h6>
                        <p>Arch (es)</p>
						<h6 class="margin-bottom">
					      <?php if ($archUpper == HARD_CODE_ID_YES) { ?>
                          
                              <input id="md_checkbox_1" name="arch_upper" value="<?php echo HARD_CODE_ID_YES ?>" class="chk-col-green" type="checkbox" <?php if ($archUpper == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?> disabled />
                              <label for="md_checkbox_1">Upper</label>
                           &nbsp;&nbsp;
						  <?php } ?>
                          
                           <?php if ($archLower == HARD_CODE_ID_YES) { ?>
                            
                                <input id="md_checkbox_2" name="arch_lower" value="<?php echo HARD_CODE_ID_YES ?>" class="chk-col-green" type="checkbox" <?php if ($archLower == HARD_CODE_ID_YES) { echo 'checked="checked" '; } ?> disabled />
                                <label for="md_checkbox_2">Lower</label>
                          <?php } ?>
                        </h6>
                        <p>Distributor</p>
						<h6 class="margin-bottom"><?php echo $companyName; ?></h6>
                        <p>Case Type</p>
						<h6 class="margin-bottom"><?php echo $caseTypeName; ?></h6>
                        <p>Country</p>
						<h6 class="margin-bottom"><?php echo $countryName; ?></h6>
                        <?php if ($countryName == 'Pakistan') { ?>
                         
                        	<p>City</p>
							<h6 class="margin-bottom"><?php echo $cityName; ?></h6>
                        
						<?php } ?>
                        
                        <p>Comments / Remarks</p>
						<h6 class="margin-bottom"><?php echo $caseComments; ?></h6>
					</div>
             	</div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        
        <div class="col-xl-8 col-lg-7">
          
          <div class="nav-tabs-custom">
            
            <ul class="nav nav-tabs">
              
              <li><a class="active" href="#timeline" data-toggle="tab">Timeline</a></li>
              
              <?php if ($caseSetupData->num_rows() > 0) { ?>
              	
                <li><a href="#setupDataPreview" data-toggle="tab">Treatment Plan</a></li>
              
			  <?php } ?>
              
              <?php if ($caseTreatmentData->num_rows() > 0) { ?>
              	
                <li><a href="#treatmentDataPreview" data-toggle="tab">Stages</a></li>
              
			  <?php } ?>
             
             </ul>
                        
            <div class="tab-content">
             
              <div class="active tab-pane" id="timeline">
                <ul class="timeline">
					
                    <?php if ($caseShipped == HARD_CODE_ID_YES) {
						
						
							$caseShipmentInfo = $this->model_case->shippedCaseInfo($caseID);
						    	
								if ($caseShipmentInfo->num_rows() > 0) {
							
									$caseShipmentInfo = $caseShipmentInfo->row_array();
									
									$caseShipmentLotNumber 		 		 = $caseShipmentInfo['shippedCaseLotNumber'];
									$caseShipmentCreated 		 		 = $caseShipmentInfo['shippedCaseCreated'];
									$caseShipmentCreatedEmployeeID 		 = $caseShipmentInfo['employeeID'];
									$caseShipmentCreatedEmployeeName 	 = $caseShipmentInfo['employeeName'];
									$caseShipmentCreatedEmployeeCode 	 = $caseShipmentInfo['employeeCode'];
									$caseShipmentCreatedEmployeePhoto 	 = $caseShipmentInfo['employeePhoto'];
														
									$shipmentCreatedDate		 		 = date('D, M d Y,',$caseShipmentCreated);
									$shipmentCreatedTime				 = date('h:i A',$caseShipmentCreated);
									$shipmentCreatedTimeAgo				 = timeAgo($caseShipmentCreated).' ago';
									
									$employeeAssignJobs					 = getEmployeeAssignJobs($caseShipmentCreatedEmployeeID); // Calling From HR Employees Helper
														
									if ($employeeAssignJobs) {
																
										 $employeeAssignJob 								= $employeeAssignJobs->row_array();
										 $caseShipmentCreatedEmployeeAssignJobDescription	= $employeeAssignJob['jobPositionName'];
										  
									  
									 } else {
											  
										 $caseShipmentCreatedEmployeeAssignJobDescription 	= NULL;
									 }
									 
									  $photoPath 		= USER_DEFAULT_IMAGE_URL_PATH; 
									  $photoLargePath   = USER_DEFAULT_IMAGE_URL_PATH; 
																							   
									   if ($caseShipmentCreatedEmployeePhoto) {
												
												$photoPath 		= HR_OFFICIALS_USERS_URL_PATH.'thumbnail_'.$caseShipmentCreatedEmployeePhoto;   
												$photoLargePath = HR_OFFICIALS_USERS_URL_PATH.$caseShipmentCreatedEmployeePhoto;   
									   } 	   		
										
										
										?>
                                      
                                        <li class="time-label">
                                              <span class="bg-green">
                                                Case Shipped
                                              </span>
                                              
                                              <div class="pull-right" style="margin-right:15px;">
                                                       
                                                       <!-- Temporary Quality Checklist Area-->
                                               		   
                                                        <?php 
														   	
														  $caseQualityChecklistFlag = true;
														  
														  if ($presentProductionStageID == EPOXY_POURING) {
															  
															   $caseQualityChecklistFlag = false;
														  }
														  
														  if ($impressionID == CASE_IMPRESSION_TYPE_STL && $presentProductionStageID == _3D_SCANNING_EDITING) {
															  
															   $caseQualityChecklistFlag = false;
														  }
														  
														  if ($impressionID == CASE_IMPRESSION_TYPE_STL && $presentProductionStageID == QUALITY_OF_SCANNED_EDIT_CASES) {
															  
															   $caseQualityChecklistFlag = false;
														  }
														  
														  if ($caseQualityChecklistFlag == true) { ?>
                                                           
                                                           <span style="margin-right:10px;"><a href="JavaScript:Void(0);" data-target=".checklistQualityPopUp" data-toggle="modal" class="checklistPopUp" case="<?php echo $caseID; ?>"><i data-toggle="tooltip" title="" data-original-title="Case Quality Checklist" class="fa fa-file-text-o"></i></a></span>
                                                        
                                                              <!--POP Model for Case Quality Checklist-->
                                                              <div class="modal fade bs-example-modal-lg checklistQualityPopUp" role="dialog" aria-labelledby="caseQualityChecklistPopUp" aria-hidden="true" style="display:none;">
                                                                <div class="modal-dialog modal-lg">
                                                                 <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="caseQualityChecklistPopUp"><span id="case-quality-checklist-title"></span></h4>
                                                                         <div class="pull-right">
                                                                         <a href="JavaScript:Void(0);" class="checklistPrint" case="<?php echo $caseID; ?>" style="margin-right:15px;"><i data-toggle="tooltip" title="" data-original-title="Print" class="fa fa-print"></i></a>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Close" >×</button>
                                                                        </div>
                                                                     </div>
                                                                   
                                                                <div class="modal-body">
                                                                        
                                                                        <div class="row" style="margin-left:2px;" id="case-quality-checklist-data"></div>
                                                                </div>
                                                                 
                                                                </div>
                                                                 
                                                               
                                                               </div>
                                                              
                                                              </div>
                                                           
                                                        
                                                       
                                                        <?php } ?>
                                                       
                                                       <!--End Temporary Quality Checklist Area-->
                                                    
                                                   	 <span style="padding-right:10px;"><a target="_blank" href="<?php echo base_url(); ?>case-notes/<?php echo encodeString($caseID); ?>/"><i data-toggle="tooltip" title=""data-original-title="Case Notes" class="fa fa-commenting"></i></a></span>
                                                     <span><a href="<?php echo base_url(); ?>case/<?php echo encodeString($caseID); ?>/"><i data-toggle="tooltip" title=""data-original-title="Case Details" class="fa fa-medkit"></i></a></span>
                                               	   
                                                     
                                                     </div>
                                        </li>
					
                                        <li>
                                          <i class="ion ion-plane bg-aqua"></i>
                    
                                          <div class="timeline-item">
                                            
                                            <span class="time"><?php echo $shipmentCreatedDate; ?> <?php echo $shipmentCreatedTime; ?></span>
                    
                                            <h3 class="timeline-header" style="color:#111; font-weight:600;">Case Was Shipped</h3>
                    						
                                            <span class="time" style="font-size:11px;">
                                                    	
                                              	<i class="fa fa-clock-o"></i> <?php echo $shipmentCreatedTimeAgo; ?>
                                                    
                                            </span>
                                            
                                            <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                                                                            	
                                                  
                                                 <div class="user-block">
                                                    
													<?php if ($caseShipmentCreatedEmployeePhoto) {  ?>
                                                  
                                                      <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseShipmentCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseShipmentCreatedEmployeeName; ?> - <?php echo $caseShipmentCreatedEmployeeCode; ?> (<?php echo $caseShipmentCreatedEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                  
                                                    <?php } else { ?>
                                                      
                                                      <img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                      
                                                    <?php } ?>
                                                        
                                                        <span class="username" style="font-size:13px; color:#7dab2e; font-weight:500;">
                                                          
                                                          <?php echo $caseShipmentCreatedEmployeeName; ?> - <?php echo $caseShipmentCreatedEmployeeCode; ?> (<?php echo $caseShipmentCreatedEmployeeAssignJobDescription; ?>)
                                          
                                                               shipped the case with lot number #<?php echo $caseShipmentLotNumber; ?></span>
                                                                
                                                                <span class="description" style="margin-bottom:0px; font-size:11px;">
                                                                                                          
															<?php 	
                                                                
                                                                if ($todayDate == date('d-m-Y',$caseShipmentCreated)) {
                                                        
                                                                        echo $shipmentCreatedTimeAgo;
                                                        
                                                                } else {
                                                
                                                                        echo $shipmentCreatedDate.' '.$shipmentCreatedTime;
                                                                }
                                                              
                                                              ?>
                                                        </span>
                                                   </div>
                                             </div>
                                          </div>
                                        </li>
                            <?php   		   	
							
								}
						  }
					?>
                    
                    <?php 
							$totalStages = $caseStageHistory->num_rows();
							
							if ($totalStages) {
										
									$i 				 = $totalStages;
									$stageCounter 	 = 1;
									$operatorCounter = 1;
									foreach($caseStageHistory->result() as $stageHistory) {
											
											$caseStageID 					= $stageHistory->caseStageID;
											$productionStageID 				= $stageHistory->productionStageID;
											$productionStageName 			= $stageHistory->productionStageName;
											$caseStageCreated 				= $stageHistory->caseStageCreated;
											
											$stageCreatedDate				= date('d F. Y',$caseStageCreated);
											$stageCreatedTime				= date('h:i A',$caseStageCreated);
											
											$caseStatusHistory				= caseChangeStatusHistory($caseStageID,$caseID); // Calling From Case Helper
											
					?>
                    
                                         <li class="time-label"><span class="bg-green"><?php echo $productionStageName; ?></span>
                                              
                                              <?php 
											   
												if ($i == $totalStages && $caseShipped == HARD_CODE_ID_NO) { ?>
                                              		
                                                    <div class="pull-right" style="margin-right:15px;">
                                                       
                                                       <!-- Temporary Quality Checklist Area-->
                                               		   
                                                        <?php 
														   	
														  $caseQualityChecklistFlag = true;
														  
														  if ($presentProductionStageID == EPOXY_POURING) {
															  
															   $caseQualityChecklistFlag = false;
														  }
														  
														  if ($impressionID == CASE_IMPRESSION_TYPE_STL && $presentProductionStageID == _3D_SCANNING_EDITING) {
															  
															   $caseQualityChecklistFlag = false;
														  }
														  
														  if ($impressionID == CASE_IMPRESSION_TYPE_STL && $presentProductionStageID == QUALITY_OF_SCANNED_EDIT_CASES) {
															  
															   $caseQualityChecklistFlag = false;
														  }
														  
														  if ($caseQualityChecklistFlag == true) { ?>
                                                           
                                                           <span style="margin-right:10px;"><a href="JavaScript:Void(0);" data-target=".checklistQualityPopUp" data-toggle="modal" class="checklistPopUp" case="<?php echo $caseID; ?>"><i data-toggle="tooltip" title="" data-original-title="Case Quality Checklist" class="fa fa-file-text-o"></i></a></span>
                                                        
                                                              <!--POP Model for Case Quality Checklist-->
                                                              <div class="modal fade bs-example-modal-lg checklistQualityPopUp" role="dialog" aria-labelledby="caseQualityChecklistPopUp" aria-hidden="true" style="display:none;">
                                                                <div class="modal-dialog modal-lg">
                                                                 <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="caseQualityChecklistPopUp"><span id="case-quality-checklist-title"></span></h4>
                                                                         <div class="pull-right">
                                                                         <a href="JavaScript:Void(0);" class="checklistPrint" case="<?php echo $caseID; ?>" style="margin-right:15px;"><i data-toggle="tooltip" title="" data-original-title="Print" class="fa fa-print"></i></a>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Close" >×</button>
                                                                        </div>
                                                                     </div>
                                                                   
                                                                <div class="modal-body">
                                                                        
                                                                        <div class="row" style="margin-left:2px;" id="case-quality-checklist-data"></div>
                                                                </div>
                                                                 
                                                                </div>
                                                                 
                                                               
                                                               </div>
                                                              
                                                              </div>
                                                           
                                                        
                                                       
                                                        <?php } ?>
                                                       
                                                       <!--End Temporary Quality Checklist Area-->
                                                    
                                                   	 <span style="padding-right:10px;"><a target="_blank" href="<?php echo base_url(); ?>case-notes/<?php echo encodeString($caseID); ?>/"><i data-toggle="tooltip" title=""data-original-title="Case Notes" class="fa fa-commenting"></i></a></span>
                                                     <span><a href="<?php echo base_url(); ?>case/<?php echo encodeString($caseID); ?>/"><i data-toggle="tooltip" title="data-original-title="Case Details" class="fa fa-medkit"></i></a></span>
                                               	   
                                                     
                                                     </div>
											   
											   <?php } 
											   		
													$i--;
											    ?>
                                         </li>
                                         
                                         <?php 
												if ($caseStatusHistory) {
									
													$statusCounter = 1;
													foreach($caseStatusHistory->result() as $statusHistory) {
														
														$caseStatusID 			 			 = $statusHistory->caseStatusID;
														$statusID 				 			 = $statusHistory->statusID;
														$statusName 			 			 = $statusHistory->statusName;
														$statusParentID 		 			 = $statusHistory->statusParentID;
														$caseStatusComments 	 			 = $statusHistory->caseStatusComments;
														$caseStatusAutoMove 	 			 = $statusHistory->caseStatusAutoMove;
														$caseStatusCreated 		 			 = $statusHistory->caseStatusCreated;
														$caseStatusCreatedEmployeeID 		 = $statusHistory->employeeID;
														$caseStatusCreatedEmployeeName 		 = $statusHistory->employeeName;
														$caseStatusCreatedEmployeeCode 		 = $statusHistory->employeeCode;
														$caseStatusCreatedEmployeePhoto 	 = $statusHistory->employeePhoto;
														
														$statusCreatedDate		 			 = date('D, M d Y,',$caseStatusCreated);
														$statusCreatedTime					 = date('h:i A',$caseStatusCreated);
														$statusCreatedTimeAgo				 = timeAgo($caseStatusCreated).' ago';
														
														$employeeAssignJobs					= getEmployeeAssignJobs($caseStatusCreatedEmployeeID); // Calling From HR Employees Helper
														
														if ($employeeAssignJobs) {
																					
															 $employeeAssignJob 								= $employeeAssignJobs->row_array();
															 $caseStatusCreatedEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
															  
														  
														 } else {
																  
															 $caseStatusCreatedEmployeeAssignJobDescription 	= NULL;
														 }
														 
														  $caseStatusCreatedPhotoPath = NULL; 
														  $photoFlag 				  = false;
																							   
														   if ($caseStatusCreatedEmployeePhoto) {
																	
																	$caseStatusCreatedPhotoPath = HR_OFFICIALS_USERS_URL_PATH.'thumbnail_'.$caseStatusCreatedEmployeePhoto;   
																	$photoFlag = true;
														   } else {
															  	  
																    $caseStatusCreatedPhotoPath = USER_DEFAULT_IMAGE_URL_PATH; 
														    }	   		   	
										?>
                                        			
                                                <li>
                                                  <i class="fa fa-tags bg-blue"></i>
                            					   <div class="timeline-item">
                                                   
                                                    <span class="time"><?php echo $statusCreatedDate; ?> <?php echo $statusCreatedTime; ?></span>
                                             
                                                    <h3 class="timeline-header" style="color:#111; font-weight:600;">
                                                     		
														   <?php if ($statusParentID) { 
																	   
																	   echo '('.statusName($statusParentID).') '; // Calling From Case Helper
																  }
															?>
                                                            
															<?php echo $statusName; ?>
                                                         
                                                    </h3>
                                                    
                                                     <?php if ($caseStatusAutoMove == HARD_CODE_ID_YES) { ?>
                                                     
													 <?php } ?>
                                                    
                                                    <span class="time" style="font-size:11px;">
                                                    	
                                                    	<i class="fa fa-clock-o"></i> <?php echo $statusCreatedTimeAgo; ?>
                                                    
                                                     </span>
                                                    
                                                     <?php if ($productionStageID == RECEIVE_ORDER_ENTRY) { ?>
                                                    
                                                      <div class="timeline-body">
                                                              
                                                              <span style="font-size:0.9em;"> 
                                                              	Case is created by <strong><?php echo $caseCreatedEmployeeName; ?> - <?php echo $caseCreatedEmployeeCode; ?> (<?php echo $caseCreatedEmployeeAssignJobDescription; ?>)</strong>
                                                              </span>
                                                              </div>
                                                    <?php } ?>
                                                   
                                                    <?php if ($productionStageID != RECEIVE_ORDER_ENTRY) { ?>
                                                    
                                                          
                                                          		<?php if (in_array($statusID,$caseStatusReadyArray)) { ?>
                                                                			
                                                                            <?php  if ($caseStatusAutoMove == HARD_CODE_ID_YES) { ?>
                                                                                 
                                                                                        <div class="timeline-body">
                                                                          
                                                                                             <span style="font-size:0.8em;"> 
                                                               									<i class="fa fa-forward"></i> automaticaly forworded
                                                                 							 </span>
                                                                                         
                                                                                         </div>
                                                                                   
                                                                              <?php } else if ($caseStatusAutoMove == HARD_CODE_ID_NO) { ?>
                                                                               		
                                                                                    	<div class="timeline-body">
                                                                    
                                                                                             <span style="font-size:0.9em;"> 
                                                                                                Case status updated by <strong><?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)</strong>
                                                                                             </span>
                                                                                   
                                                                                  		 </div>
                                                                                         
                                                                                       <?php if($caseStatusComments) { ?>
                                                                                         
                                                                                        <div class="activitytimeline">
                                                                              
                                                                                                   <p style="margin-bottom:10px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                                                                   <br />
                                                                                              
                                                                                          </div>
                                                                                          
                                                                                       <?php } ?>
                                                                               
                                                                               <?php } ?>
                                                                
                                                                <?php
																			
																				
																				
																		} else if (in_array($statusID,$caseStatusProcessArray)) {
																			
																				
																				$caseStatusOperator = $this->model_case->getCaseStatusOperator($caseID,$caseStageID,$caseStatusID);
																				
																				if ($caseStatusOperator->num_rows() == 1) {
																						
																							  $caseStatusOperator = $caseStatusOperator->row_array();
																	
																							  $caseOperatorComments 			 = $caseStatusOperator['caseOperatorComments'];
																							  $caseOperatorAssignmentType 		 = $caseStatusOperator['caseOperatorAssignmentType'];
																							  $caseOperatorResumeFlag 			 = $caseStatusOperator['caseResumeFlag'];
																							  $caseOperatorCreated 				 = $caseStatusOperator['caseOperatorCreated'];
																		
																							  $operatorEmployeeID 				 = $caseStatusOperator['employeeID'];
																							  $operatorEmployeeCode 			 = $caseStatusOperator['employeeCode'];
																							  $operatorEmployeeName 			 = $caseStatusOperator['employeeName'];
																							  $operatorEmployeePhoto 			 = $caseStatusOperator['employeePhoto'];
																							  
																							  $operatorCreatedDate				 = date('F d. Y',$caseOperatorCreated);
																							  $operatorCreatedTime		 		 = date('h:i A',$caseOperatorCreated);
																							  $operatorCreatedTimeAgo			 = timeAgo($caseOperatorCreated).' ago'; // Calling From Shared Helper
																							  
																							  $employeeAssignJobs				 = getEmployeeAssignJobs($operatorEmployeeID); // Calling From HR Employees Helper
																							  
																							  if ($employeeAssignJobs) {
																											
																									$employeeAssignJob 								= $employeeAssignJobs->row_array();
																									$caseOperatorEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
																									
																								
																							   } else {
																										
																										$caseOperatorEmployeeAssignJobDescription 	= NULL;
																							   }
																							   
																							   $photoPath 		= USER_DEFAULT_IMAGE_URL_PATH; 
																							   $photoLargePath  = USER_DEFAULT_IMAGE_URL_PATH; 
																							   
																							   if ($operatorEmployeePhoto) {
																										
																										$photoPath 		= HR_OFFICIALS_USERS_URL_PATH.'thumbnail_'.$operatorEmployeePhoto;   
																										$photoLargePath = HR_OFFICIALS_USERS_URL_PATH.$operatorEmployeePhoto;   
																							   } 	   		
																	?>
                                                                    							
                                                                                                
                                                                                                <div class="post" style="margin-left:20px; padding-top:20px;">
                                                                            	
																									  <?php if ($caseOperatorAssignmentType == HARD_CODE_PICK) { ?>
                                                                                                           
                                                                                                           <div class="user-block">
                                                                                                              <?php if ($operatorEmployeePhoto) { ?>
                                                                                                           	
                                                                                                                <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $operatorEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                                                                            
                                                                                                              <?php } else { ?>
                                                                                                             	
                                                                                                                <img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                                                                                
                                                                                                              <?php } ?>
                                                                                                                  
                                                                                                                  
                                                                                                                <?php if ($caseOperatorResumeFlag == HARD_CODE_ID_YES) { ?>
																												 
                                                                                                                  <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                                                    
                                                                                                                     <a href="javascript:void(0);" style="color:#389af0"><?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>) 's</a>
                                                                                                    				    case <span class="label" style="background:#D11283;">resumed processing</span> by
                                                                                                                       
                                                                                                                       <span style="color:#000">
																													   
																													   <?php echo $caseStatusOperator['creatorEmployeeName']; ?> - <?php echo $caseStatusOperator['creatorEmployeeCode']; ?>
                                                                                                                       
                                                                                                                       <?php 
																													   		
																															$creatorEmployeeAssignJobs	 = getEmployeeAssignJobs($caseStatusOperator['creatorEmployeeID']); // Calling From HR Employees Helper
																															
																															if ($creatorEmployeeAssignJobs) {
																											
																																$creatorEmployeeAssignJobs 								= $creatorEmployeeAssignJobs->row_array();
																																$caseOperatorCreatorEmployeeAssignJobDescription		= $creatorEmployeeAssignJobs['jobPositionName'];
																								
																															   } else {
																																		
																																		$caseOperatorCreatorEmployeeAssignJobDescription 	= NULL;
																															   }
																													   ?>
                                                                                                                       
                                                                                                                       (<?php echo $caseOperatorCreatorEmployeeAssignJobDescription; ?>)
                                                                                                                       
                                                                                                                       </span>
                                                                                                                       
                                                                                                                        and <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                                                                        
																														<?php echo $IS_WAS; ?> now                                                                                                     
                                                                                                                    
                                                                                                                        working as operator. 
                                                                                                      
                                                                                                                      </span>
                                                                                                                 
																												 <?php } else { ?>
                                                                                                                 
                                                                                                                
                                                                                                                  <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                                                    
                                                                                                                     <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)
                                                                                                    
                                                                                                                          <span class="label label-warning">picked</span> the case and
                                                                                                    
                                                                                                                      <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                                                      
                                                                                                                      <?php echo $IS_WAS; ?>  working as operator. 
                                                                                                      
                                                                                                                      </span>
                                                                                                   				 
                                                                                                                 <?php } ?>
                                                                                                   
                                                                                                                      <span class="description" style="margin-bottom: <?php if($caseOperatorComments) { echo 0; } else {echo '15';} ?>px; font-size:11px;">
                                                                                                          
                                                                                                                              <?php 	
                                                                                                                                  
                                                                                                                                  if ($todayDate == date('d-m-Y',$caseOperatorCreated)) {
                                                                                                                          
                                                                                                                                          echo $operatorCreatedTimeAgo;
                                                                                                                          
                                                                                                                                  } else {
                                                                                                                  
                                                                                                                                          echo $operatorCreatedDate.' '.$operatorCreatedTime;
                                                                                                                                  }
                                                                                                                                
                                                                                                                                ?>
                                                                                                                          </span>
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="activitytimeline">
                                                                                                
                                                                                                                       <?php if($caseOperatorComments) { ?>
                                                                                                      
                                                                                                                           <p style="margin-bottom:10px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseOperatorComments; ?>”</p>
                                                                                                                           <br />
                                                                                                                       
                                                                                                                       <?php } ?>
                                                                                                                      
                                                                                                                    </div>
                                                                                                      
                                                                                                      <?php } else if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED || $caseOperatorAssignmentType == HARD_CODE_AUTO_ASSIGNED) { ?>
                                                                                
                                                                                				
                                                                                                                      <div class="user-block">
                                                                                                                       
                                                                                                                       <?php if ($operatorEmployeePhoto) { ?>
                                                                                                           	
                                                                                                              				  <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $operatorEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                                                                                        
																														<?php } else { ?>
                                                                                                                       	     
                                                                                                                             <img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                                                                                        
																														<?php } ?>
                                                                                                                             <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                                                        
                                                                                                                                    <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)
                                                                                                              
                                                                                                                                            <span class="label label-info"><?php if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED) { echo 'assigned'; } else if ($caseOperatorAssignmentType == HARD_CODE_AUTO_ASSIGNED) { echo 'auto assigned'; } ?></span> the case and
                                                                                                              
                                                                                                                                     <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                                                                
                                                                                                                                     <?php echo $IS_WAS; ?>  working as operator. 
                                                                                                          
                                                                                                                              </span>
                                                                                                       
                                                                                                                               <span class="description" style="margin-bottom: <?php if($caseOperatorComments) { echo 0; } else { echo '15';} ?>px; font-size:11px;">
                                                                                                              
                                                                                                                                  <?php 	
                                                                                                                                      
                                                                                                                                      if ($todayDate == date('d-m-Y',$caseOperatorCreated)) {
                                                                                                                              
                                                                                                                                              echo $operatorCreatedTimeAgo;
                                                                                                                              
                                                                                                                                      } else {
                                                                                                                      
                                                                                                                                              echo $operatorCreatedDate.' '.$operatorCreatedTime;
                                                                                                                                      }
                                                                                                                                    
                                                                                                                                   ?>
                                                                                                                                 </span>
                                                                                                                           </div>	
                                                                                                     
                                                                                                <div class="activitytimeline">
                                                                      
                                                                                                       <?php if($caseOperatorComments) { ?>
                                                                                      
                                                                                                           <p style="margin-bottom:10px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseOperatorComments; ?>”</p>
                                                                                                           <br />
                                                                                                       <?php } ?>
                                                                                                      
                                                                                                 </div>
																				
																				
																				<?php } ?>
                                                                                
                                                                            </div>
                                                                                                
                                                                    			
                                                                    
                                                                    <?php
																				} else {
																						
																						$caseOperatorHistory   = caseChangeOperatorHistory($caseID,$caseStageID,$caseStatusID); // Calling From Case Helper
																						
																						if ($caseOperatorHistory) {
																 	  
																							  $operatorCounter = 1;
																							  foreach($caseOperatorHistory->result() as $operatorHistory) {
																								  
																								  $caseOperatorComments 			 = $operatorHistory->caseOperatorComments;
																								  $caseOperatorAssignmentType 		 = $operatorHistory->caseOperatorAssignmentType;
																								  $caseOperatorResumeFlag 			 = $operatorHistory->caseResumeFlag;
																								  $caseOperatorCreatedBy 			 = $operatorHistory->caseOperatorCreatedBy;
																								  $caseOperatorCreated 				 = $operatorHistory->caseOperatorCreated;
																			
																								  $operatorEmployeeID 				 = $operatorHistory->employeeID;
																								  $operatorEmployeeCode 			 = $operatorHistory->employeeCode;
																								  $operatorEmployeeName 			 = $operatorHistory->employeeName;
																								  $operatorEmployeePhoto 			 = $operatorHistory->employeePhoto;
																								  
																								  $caseOperatorCreatedBy 			 = getCaseOperatorCreatedBy($caseOperatorCreatedBy); // Calling From Case Helper
																								  
																								  if ($caseOperatorCreatedBy) {
																											
																											$caseOperatorCreatedBy  					= $caseOperatorCreatedBy->row_array();
																											$caseOperatorCreatedByID  					= $caseOperatorCreatedBy['employeeID'];
																											$caseOperatorCreatedByName  				= $caseOperatorCreatedBy['employeeName'];
																											$caseOperatorCreatedByCode 					= $caseOperatorCreatedBy['employeeCode'];
																											
																											$caseOperatorCreatedByAssignJobs			= getEmployeeAssignJobs($caseOperatorCreatedByID); // Calling From HR Employees Helper
																											
																											if ($caseOperatorCreatedByAssignJobs) {
																												
																												$caseOperatorCreatedByAssignJobs 				= $caseOperatorCreatedByAssignJobs->row_array();
																												$caseOperatorCreatedByAssignJobDescription		= $caseOperatorCreatedByAssignJobs['jobPositionName'];
																											}
																										
																								  }
																								  
																								  $operatorCreatedDate				 = date('F d. Y',$caseOperatorCreated);
																								  $operatorCreatedTime		 		 = date('h:i A',$caseOperatorCreated);
																								  $operatorCreatedTimeAgo			 = timeAgo($caseOperatorCreated).' ago'; // Calling From Shared Helper
																								  
																								  $employeeAssignJobs				 = getEmployeeAssignJobs($operatorEmployeeID); // Calling From HR Employees Helper
																								  
																								  if ($employeeAssignJobs) {
																												
																										$employeeAssignJob 								= $employeeAssignJobs->row_array();
																										$caseOperatorEmployeeAssignJobDescription		= $employeeAssignJob['jobPositionName'];
																										
																									
																								   } else {
																											
																											$caseOperatorEmployeeAssignJobDescription 	= NULL;
																								   }
																								   
																								   $photoPath = NULL; 
																								   
																								   if ($operatorEmployeePhoto) {
																											
																											$photoPath = HR_OFFICIALS_USERS_URL_PATH.'thumbnail_'.$operatorEmployeePhoto;   
																								   }  else {
																									 		
																											$photoPath = USER_DEFAULT_IMAGE_URL_PATH;   
																								   }	   		
																	  ?>
                                                                                            
                                                                                            				<div class="post" style="margin-left:20px; padding-top:20px;">
                                                                            	
																													  <?php if ($caseOperatorAssignmentType == HARD_CODE_PICK) { ?>
                                                                                                                           
                                                                                                                           <div class="user-block">
                                                                                                                              
                                                                                                                          	<?php if ($operatorEmployeePhoto) { ?>
                                                                                                           	
                                                                                                              					  <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $operatorEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                                                                                        
																															 <?php } else { ?>
                                                                                                                       	    
                                                                                                                             	 <img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                                                                                        
																															<?php } ?>
                                                                                                                              
                                                                                                                                 
                                                                                                                                   <?php if ($caseOperatorResumeFlag == HARD_CODE_ID_YES) { ?>
																												 
                                                                                                                                          <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                                                                            
                                                                                                                                             <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>) 's
                                                                                                                                                case <span class="label" style="background:#D11283;">resumed processing</span> by
                                                                                                                                               
                                                                                                                                               <span style="color:#000">
                                                                                                                                               
                                                                                                                                               <?php echo $operatorHistory->creatorEmployeeName; ?> - <?php echo $operatorHistory->creatorEmployeeCode; ?>
                                                                                                                                               
                                                                                                                                               <?php 
                                                                                                                                                    
                                                                                                                                                    $creatorEmployeeAssignJobs	 = getEmployeeAssignJobs($operatorHistory->creatorEmployeeID); // Calling From HR Employees Helper
                                                                                                                                                    
                                                                                                                                                    if ($creatorEmployeeAssignJobs) {
                                                                                                                                    
                                                                                                                                                        $creatorEmployeeAssignJobs 								= $creatorEmployeeAssignJobs->row_array();
                                                                                                                                                        $caseOperatorCreatorEmployeeAssignJobDescription		= $creatorEmployeeAssignJobs['jobPositionName'];
                                                                                                                        
                                                                                                                                                       } else {
                                                                                                                                                                
                                                                                                                                                                $caseOperatorCreatorEmployeeAssignJobDescription 	= NULL;
                                                                                                                                                       }
                                                                                                                                               ?>
                                                                                                                                               
                                                                                                                                               (<?php echo $caseOperatorCreatorEmployeeAssignJobDescription; ?>)
                                                                                                                                               
                                                                                                                                               </span>
                                                                                                                                               
                                                                                                                                                and <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                                                                                                
                                                                                                                                                <?php echo $IS_WAS; ?> now                                                                                                     
                                                                                                                                            
                                                                                                                                                working as operator. 
                                                                                                                              
                                                                                                                                              </span>
                                                                                                                                         
                                                                                                                                   <?php } else { ?>
                                                                                                                                                          
                                                                                                                                           <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                                                                                            
                                                                                                                                                             <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)
                                                                                                                                            
                                                                                                                                                                  <span class="label label-warning">picked</span> the case and
                                                                                                                                            
                                                                                                                                                              <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                                                                                              
                                                                                                                                                              <?php echo $IS_WAS; ?>  working as operator. 
                                                                                                                                              
                                                                                                                                                              </span>
                                                                                                                                      
                                                                                                                                   <?php } ?>
                                                                                                                   
                                                                                                                                  <span class="description" style="margin-bottom: <?php if($caseOperatorComments) { echo 0; } else {echo '15';} ?>px; font-size:11px;">
                                                                                                                      
                                                                                                                                          <?php 	
                                                                                                                                              
                                                                                                                                              if ($todayDate == date('d-m-Y',$caseOperatorCreated)) {
                                                                                                                                      
                                                                                                                                                      echo $operatorCreatedTimeAgo;
                                                                                                                                      
                                                                                                                                              } else {
                                                                                                                              
                                                                                                                                                      echo $operatorCreatedDate.' '.$operatorCreatedTime;
                                                                                                                                              }
                                                                                                                                            
                                                                                                                                            ?>
                                                                                                                                      </span>
                                                                                                                                   
                                                                                                                                    </div>
                                                                                                                                    
                                                                                                                                    <div class="activitytimeline">
                                                                                                                
                                                                                                                                       <?php if($caseOperatorComments) { ?>
                                                                                                                      
                                                                                                                                           <p style="margin-bottom:10px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseOperatorComments; ?>”</p>
                                                                                                                                           <br />
                                                                                                                                       
                                                                                                                                       <?php } ?>
                                                                                                                                      
                                                                                                                                    </div>
                                                                                                                      
                                                                                                                      <?php } else if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED || $caseOperatorAssignmentType == HARD_CODE_AUTO_ASSIGNED) { ?>
                                                                                
                                                                                				
                                                                                                                                  <div class="user-block">
                                                                                                                                   
                                                                                                                                       
                                                                                                                                       <?php if ($operatorEmployeePhoto) { ?>
                                                                                                           	
                                                                                                              					  			<a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $operatorEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)"><img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt=""></a>
                                                                                                                        
																																		 <?php } else { ?>
                                                                                                                       	    
                                                                                                                             	 			<img class="img-bordered-sm rounded-circle" src="<?php echo $photoPath; ?>" alt="">
                                                                                                                        
																																		<?php } ?>
                                                                                                                                     
                                                                                                                                          <span class="username" style="font-size:13px; color:#000; font-weight:500;">
                                                                                                                                    
                                                                                                                                                 <?php echo $operatorEmployeeName; ?> - <?php echo $operatorEmployeeCode; ?> (<?php echo $caseOperatorEmployeeAssignJobDescription; ?>)
                                                                                                                          
                                                                                                                                                        <span class="label label-info"><?php if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED) { echo 'assigned'; } else if ($caseOperatorAssignmentType == HARD_CODE_AUTO_ASSIGNED) { echo 'auto assigned'; } ?></span> the case <?php if ($caseOperatorAssignmentType == HARD_CODE_ASSIGNED) { ?>by <span style="color:#000"><?php echo $caseOperatorCreatedByName; ?> - <?php echo $caseOperatorCreatedByCode; ?> (<?php echo $caseOperatorCreatedByAssignJobDescription; ?>)</span> <?php } ?> and
                                                                                                                          
                                                                                                                                                 <?php if ($operatorCounter == 1 && $stageCounter == 1 && $statusCounter == 1) { $IS_WAS = 'is now'; } else { $IS_WAS = 'was'; } ?>
                                                                                                                            
                                                                                                                                                 <?php echo $IS_WAS; ?>  working as operator. 
                                                                                                                      
                                                                                                                                          </span>
                                                                                                                   
                                                                                                                                           <span class="description" style="margin-bottom: <?php if($caseOperatorComments) { echo 0; } else {echo '15';} ?>px; font-size:11px;">
                                                                                                                          
                                                                                                                                              <?php 	
                                                                                                                                                  
                                                                                                                                                  if ($todayDate == date('d-m-Y',$caseOperatorCreated)) {
                                                                                                                                          
                                                                                                                                                          echo $operatorCreatedTimeAgo;
                                                                                                                                          
                                                                                                                                                  } else {
                                                                                                                                  
                                                                                                                                                          echo $operatorCreatedDate.' '.$operatorCreatedTime;
                                                                                                                                                  }
                                                                                                                                                
                                                                                                                                               ?>
                                                                                                                                             </span>
                                                                                                                                       </div>	
                                                                                                     
                                                                                                                                  <div class="activitytimeline">
                                                                                                          
                                                                                                                                           <?php if($caseOperatorComments) { ?>
                                                                                                                          
                                                                                                                                               <p style="margin-bottom:0px; margin-left:22px; font-size:13px;"><i class="fa fa-comments"></i> “<?php echo $caseOperatorComments; ?>”</p>
                                                                                                                                              
                                                                                                                                           <?php } ?>
                                                                                                                                          
                                                                                                                                     </div>
																				
																				
																													  <?php } ?>
                                                                                
                                                                           									  </div>
                                                                                            
                                                                      <?php
																							
																								$operatorCounter++;
																							}
																						
																						}
																				}
																											 
																				
																		}  else if (in_array($statusID,$caseStatusHoldArray)) { 
													?>
                                                    						 <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                                                                               <div class="user-block">
                                                                                                                       
                                                                                     <?php if ($caseStatusCreatedEmployeePhoto && $photoFlag) { ?>
                                                                      
                                                                                       <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseStatusCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)"> <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt=""></a>
                                                                                  
                                                                                       <?php } else { ?>
                                                                                      
                                                                                            <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt="">
                                                                                  
                                                                                      <?php } ?>
                                                                                    
                                                                                
                                                                                        <span class="username" style="font-size:13px; color:#000; font-weight:500">
                                                                                  
                                                                                               <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                                                        
                                                                                                      <span class="label" style="background-color:#001f3f;">hold</span> the case status 
                                                                        
                                                                                               <?php if ($caseStatusComments) { ?>
                                                                                       
                                                                                              		 with following comments
                                                                                              		 <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                                              
                                                                                       		  <?php } ?>
                                                                                     
                                                                                     </span>
                                                                                     
                                                                                     <span class="description" style="margin-bottom:0px; font-size:11px;"> <?php echo $statusCreatedTimeAgo; ?></span>
                                                                 		           
                                                                                   </div>
                                                                                 </div>
                                                    
                                                    <?php
																		
																		} else if (in_array($statusID,$caseStatusOFFHoldArray)) { 
													?>
                                                    						 <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                                                                               <div class="user-block">
                                                                                                                       
                                                                                     <?php if ($caseStatusCreatedEmployeePhoto && $photoFlag) { ?>
                                                                      
                                                                                       <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseStatusCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)"> <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt=""></a>
                                                                                  
                                                                                       <?php } else { ?>
                                                                                      
                                                                                            <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt="">
                                                                                  
                                                                                      <?php } ?>
                                                                                   
                                                                                
                                                                                        <span class="username" style="font-size:13px; color:#000; font-weight:500">
                                                                                  
                                                                                               <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                                                        
                                                                                                      <span class="label" style="background-color:#001f3f;">OFF-hold</span> the case status 
                                                                        
                                                                                               <?php if ($caseStatusComments) { ?>
                                                                                       
                                                                                              		 with following comments
                                                                                              		 <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                                              
                                                                                       		  <?php } ?>
                                                                                     
                                                                                     </span>
                                                                                     
                                                                                     <span class="description" style="margin-bottom:0px; font-size:11px;"> <?php echo $statusCreatedTimeAgo; ?></span>
                                                                 		           
                                                                                   </div>
                                                                                 </div>
                                                    
                                                    <?php
																		
																		} else if (in_array($statusID,$caseStatusDoneArray)) { 
																			
													?>
                                                    						
                                                                               
                                                                            <div class="post" style="margin-left:20px; padding-top:20px; padding-bottom:5px;">
                                                                               
                                                                               <div class="user-block">
                                                                                                                       
                                                                                    <?php if ($caseStatusCreatedEmployeePhoto && $photoFlag) { ?>
                                                                      
                                                                                       <a href="http://ems.clearpathortho.pk/images/cpanel_users/<?php echo $caseStatusCreatedEmployeePhoto; ?>" data-toggle="lightbox" data-title="<?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)"> <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt=""></a>
                                                                                  
                                                                                       <?php } else { ?>
                                                                                      
                                                                                            <img class="img-bordered-sm rounded-circle" src="<?php echo $caseStatusCreatedPhotoPath; ?>" alt="">
                                                                                  
                                                                                      <?php } ?>
                                                                                
                                                                                        <span class="username" style="font-size:13px; color:#000; font-weight:500">
                                                                                  
                                                                                               <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                                                        
                                                                                                      <span class="label" style="background-color:#7dab2e">done</span> the case status 
                                                                        
                                                                                               <?php if ($caseStatusComments) { ?>
                                                                                       
                                                                                              		 with following comments
                                                                                              		 <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                                              
                                                                                       		  <?php } ?>
                                                                                     
                                                                                     </span>
                                                                                     
                                                                                     <span class="description" style="margin-bottom:0px; font-size:11px;"> <?php echo $statusCreatedTimeAgo; ?></span>
                                                                 		           
                                                                                   </div>
                                                                                 </div>
                                                                               
                                                    
                                                    <?php
																				
																		} else {
																			
																				if (in_array($statusID,$caseStatusModificationArray)) {
													?>
                                                    									
                                                                                        <div class="timeline-body">
                                                                                             
                                                                                              <span class="description-text text-black"> 
                                                                                                 
                                                                                                 
                                                                                                  <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                                                                          
                                                                                                           <span class="label" style="background-color:#7460ee;">modification</span> the case status 
                                                                                  
                                                                                                   <?php if ($caseStatusComments) { ?>
                                                                                                   
                                                                                                           with following comments
                                                                                                           <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                                                          
                                                                                                    <?php } ?>
                                                                                                 
                                                                                                 </span>
                                                                                            </div>
                                                    
                                                    <?php
																				
																				} elseif ($statusID == CASE_STATUS_WAITING_FOR_APPROVAL_IN_HOUSE_MODIFICATION) {
													?>
                                                    									
                                                                                        <div class="timeline-body">
                                                                                             
                                                                                              <span class="description-text text-black"> 
                                                                                                 
                                                                                                 
                                                                                                  <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>) makred
                                                                                          
                                                                                                           <span class="label" style="background-color:#AB47BC;">In-house modification</span> the case status 
                                                                                  
                                                                                                   <?php if ($caseStatusComments) { ?>
                                                                                                   
                                                                                                           with following comments
                                                                                                           <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                                                          
                                                                                                    <?php } ?>
                                                                                                 
                                                                                                 </span>
                                                                                            </div>
                                                    
                                                    <?php
																				
																				} else if (in_array($statusID,$caseStatusRejectArray)) {
													?>							
                                                    
                                                                                            <div class="timeline-body">
                                                                                             
                                                                                              <span class="description-text text-black"> 
                                                                                                 
                                                                                                  <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                                                                          
                                                                                                           <span class="label label-danger">rejected</span> the case status 
                                                                                  
                                                                                                   <?php if ($caseStatusComments) { ?>
                                                                                                   
                                                                                                           with following comments
                                                                                                           <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                                                          
                                                                                                    <?php } ?>
                                                                                                 
                                                                                                 </span>
                                                                                            </div>
                                                    <?php
																				 } else {
													?>
													
                                                    										<div class="timeline-body">
                                                                                             
                                                                                              <span class="description-text text-black"> 
                                                                                                 
                                                                                                  <?php echo $caseStatusCreatedEmployeeName; ?> - <?php echo $caseStatusCreatedEmployeeCode; ?> (<?php echo $caseStatusCreatedEmployeeAssignJobDescription; ?>)
                                                                                          
                                                                                                           <span class="label label-danger">hold</span> the case status 
                                                                                  
                                                                                                   <?php if ($caseStatusComments) { ?>
                                                                                                   
                                                                                                           with following comments
                                                                                                           <p style="margin-left:22px; margin-top:10px;"><i class="fa fa-comments"></i> “<?php echo $caseStatusComments; ?>”</p>
                                                                                          
                                                                                                    <?php } ?>
                                                                                                 
                                                                                                 </span>
                                                                                            </div>
                                                    
													<?php	 
																				 }
																		}
													?>
                                                          
                                                         
													 <?php } ?>
                                                    		
                                                    
                      								
                                                  </div>
                                                  
                                                  
                                                  
                                                </li>
                                        
										<?php	
												  $statusCounter++;
													
													}
												}
											
									   ?>    
                                           
                    
                    <?php	
										$stageCounter++;
									
								 	}
							}
					?>
                   
                    <li>
					  <i class="fa fa-clock-o bg-gray"></i>
					</li>
                   
				  </ul>
              </div>
             
              
             <?php if ($caseSetupData->num_rows() > 0) { ?>
             
              <div class="tab-pane" id="setupDataPreview">
 				 
                 <?php 
				 		
							$caseSetupDataRow	 		  = $caseSetupData->row_array();
							$caseDataTableID		  	  = $caseSetupDataRow['tableID'];
							$caseDataIPR		  		  = $caseSetupDataRow['caseDataIPR'];
							$caseDataExtraction		 	  = $caseSetupDataRow['caseDataExtraction'];
							$caseDataArchMovement		  = $caseSetupDataRow['caseDataArchMovement'];
							$caseDataNightTime		  	  = $caseSetupDataRow['caseDataNightTime'];
							$caseData		  			  = $caseSetupDataRow['caseData'];
							$caseDataTreatmentDiagram	  = $caseSetupDataRow['caseDataTreatmentDiagram'];
							$caseDataStageUpper	 		  = $caseSetupDataRow['caseDataStageUpper'];
							$caseDataStageLower	 		  = $caseSetupDataRow['caseDataStageLower'];
							$caseDataCreated		  	  = $caseSetupDataRow['caseDataCreated'];
							$caseDataCreatedBy		  	  = $caseSetupDataRow['caseDataCreatedBy'];
							$caseDataCreatedName		  = $caseSetupDataRow['employeeName'];
							$caseDataCreatedCode		  = $caseSetupDataRow['employeeCode'];
							
							$caseDataDecode		  		  = json_decode($caseData);
							
							$dataArray					  =	array();
							
							
							foreach($caseDataDecode as $key => $arrays){
									
									foreach($arrays as $k => $v){
							
										$dataArray[$k] = $v;
										
									}
							}
						
				 ?>
                 
             	 <form id="" class="form-horizontal form-element col-12" method="" action="" autocomplete="off">
                  <div class="table-responsive" id="">
                  <style>
                   .table td {
                        padding: 0.50rem;
                    }
                   </style>
                   
                   <div class="col-12">
                   
                    <div class="row" style="margin-top:50px;">
                       <div class="col-sm-1" style="padding-left:0px;">
                         <br /><br />
                        <span style="font-weight:500;">&nbsp;</span>
                       </div>
                       <div class="col-sm-11">
                        
                        <table width="100%" align="center">
                          <tr>
                            <td width="48%" style="border-right:1px #000 solid; border-bottom:1px #000 solid; border-top:0px;">
                            <table width="100%">
                              <tr>
                               <?php for($i=8; $i>=1; $i--) { 
                                          
                                          $teethMissing = '1_'.$i;
                                          
                                          if (@in_array($teethMissing,$dataArray['MISSING_TEETH'])) {
                                                  
                                                  $lable = '<strike style="color:#fc4b6c">'.$i.'</strike>';
                                              
                                          } else {
                                                  
                                                  $lable = $i;
                                          }
                                          
                               ?>
                                          
                                          <td width="12%" style="border:0px;"><?php echo $lable; ?></td>
                                
                               <?php } ?>
                                </tr>
                              <tr>
                                <?php for($i=8; $i>=1; $i--) { 
                                          
                                              if (@in_array($i,$dataArray['TMQ1'])) {
                                                      
                                                      $TMQ1_Checked = 'checked=checked';
                                              
                                              } else {
                                                      
                                                      $TMQ1_Checked = NULL;
                                              }
                                ?>
                                          
                                          <td height="60">
                                              <?php if ($TMQ1_Checked) { ?>
                                               <input id="PRV_TMQ1_<?php echo $i; ?>" value="<?php echo $i; ?>" class="chk-col-green" type="checkbox" <?php echo $TMQ1_Checked; ?> readonly>
                                               <label for="PRV_TMQ1_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                              <?php } else { 
                                                          
                                                          $teethMissing = '1_'.$i;
                                          
                                                          if (@in_array($teethMissing,$dataArray['MISSING_TEETH'])) {
                                                  
                                                                      echo '<span style="color:#fc4b6c">x</span>';
                                              
                                                          } else {
                                                  
                                                              echo '-';
                                                          }
                                                      
                                                 } 
                                          ?>
                                          </td>
                                          
                                <?php }?>
                                </tr>
                            </table>
                           </td>
                            <td width="52%" style="border-bottom:1px #000 solid; border-top:0px;">
                            <table width="95%"  style="margin-left:7%;">
                              <tr>
                                <?php for($i=1; $i<=8; $i++) { 
                                          
                                          $teethMissing = '2_'.$i;
                                          
                                          if (@in_array($teethMissing,$dataArray['MISSING_TEETH'])) {
                                                  
                                                  $lable = '<strike style="color:#fc4b6c">'.$i.'</strike>';
                                              
                                          } else {
                                                  
                                                  $lable = $i;
                                          }
                                          
                               ?>
                                          
                                        <td width="12%" style="border:0px;"><?php echo $lable; ?></td>
                                
                               <?php } ?>
                                
                              </tr>
                              <tr>
                                 <?php for($i=1; $i<=8; $i++) { 
                                          
                                          if (@in_array($i,$dataArray['TMQ2'])) {
                                                      
                                                      $TMQ2_Checked = 'checked=checked';
                                              
                                              } else {
                                                      
                                                      $TMQ2_Checked = NULL;
                                              }
                                 ?>
                                          
                                          <td height="60">
                                              <?php if ($TMQ2_Checked) { ?>
                                               <input id="PRV_TMQ2_<?php echo $i; ?>" value="<?php echo $i; ?>" class="chk-col-green" type="checkbox" <?php echo $TMQ2_Checked; ?> readonly>
                                               <label for="PRV_TMQ2_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                               <?php } else { 
                                                          
                                                          $teethMissing = '2_'.$i;
                                          
                                                          if (@in_array($teethMissing,$dataArray['MISSING_TEETH'])) {
                                                  
                                                                      echo '<span style="color:#fc4b6c">x</span>';
                                              
                                                          } else {
                                                  
                                                              echo '-';
                                                          }
                                                      
                                                 } ?>
                                               
                                          </td>
                                          
                                <?php }?>
                              </tr>
                            </table>
                            </td>
                          </tr>
              <tr>
                <td width="48%" style="border-right:1px #000 solid; border-top:0px;">
                <table width="100%" style="margin-top:25px;">
                  <tr>
                   <?php for($i=8; $i>=1; $i--) { 
                              
                              $teethMissing = '4_'.$i;
                              
                              if (@in_array($teethMissing,$dataArray['MISSING_TEETH'])) {
                                      
                                      $lable = '<strike style="color:#fc4b6c">'.$i.'</strike>';
                                  
                              } else {
                                      
                                      $lable = $i;
                              }
                              
                   ?>
                              
                            <td width="12%" style="border:0px;"><?php echo $lable; ?></td>
                    
                   <?php } ?>
                  </tr>
                  <tr>
                    <?php for($i=8; $i>=1; $i--) { 
                              
                              if (@in_array($i,$dataArray['TMQ4'])) {
                                          
                                          $TMQ4_Checked = 'checked=checked';
                                  
                                  } else {
                                          
                                          $TMQ4_Checked = NULL;
                                  }
                    
                    ?>
                              
                              <td height="60">
                                  <?php if ($TMQ4_Checked) { ?>
                                  <input id="PRV_TMQ4_<?php echo $i; ?>" value="<?php echo $i; ?>" class="chk-col-green" type="checkbox" <?php echo $TMQ4_Checked; ?> readonly>
                                  <label for="PRV_TMQ4_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                   <?php } else { 
                                              
                                              $teethMissing = '4_'.$i;
                              
                                              if (@in_array($teethMissing,$dataArray['MISSING_TEETH'])) {
                                      
                                                          echo '<span style="color:#fc4b6c">x</span>';
                                  
                                              } else {
                                      
                                                  echo '-';
                                              }
                                          
                                     } ?>
                                  
                              </td>
                              
                    <?php }?>
                  </tr>
                </table>
                </td>
                <td width="52%" style="border-top:0px;">
                <table width="95%" style="margin-left:7%; margin-top:25px;">
                  <tr>
                    <?php for($i=1; $i<=8; $i++) { 
                              
                              $teethMissing = '3_'.$i;
                              
                              if (@in_array($teethMissing,$dataArray['MISSING_TEETH'])) {
                                      
                                      $lable = '<strike style="color:#fc4b6c">'.$i.'</strike>';
                                  
                              } else {
                                      
                                      $lable = $i;
                              }
                              
                   ?>
                              
                           <td width="12%" style="border:0px;"><?php echo $lable; ?></td>
                    
                  <?php } ?>
                  </tr>
                  <tr>
                    <?php for($i=1; $i<=8; $i++) { 
                              
                              if (@in_array($i,$dataArray['TMQ3'])) {
                                          
                                          $TMQ3_Checked = 'checked=checked';
                                  
                                  } else {
                                          
                                          $TMQ3_Checked = NULL;
                                  }
                    ?>
                              
                              <td height="60">
                                  <?php if ($TMQ3_Checked) { ?>
                                  <input id="PRV_TMQ3_<?php echo $i; ?>" value="<?php echo $i; ?>" class="chk-col-green" type="checkbox" <?php echo $TMQ3_Checked; ?> readonly>
                                  <label for="PRV_TMQ3_<?php echo $i; ?>" style="padding-left:0px; line-height: 0px;">&nbsp;</label>
                                  <?php } else { 
                                              
                                              $teethMissing = '3_'.$i;
                              
                                              if (@in_array($teethMissing,$dataArray['MISSING_TEETH'])) {
                                      
                                                          echo '<span style="color:#fc4b6c">x</span>';
                                  
                                              } else {
                                      
                                                  echo '-';
                                              }
                                          
                                     } ?>
                             
                              </td>
                              
                    <?php }?>
                  </tr>
                </table></td>
              </tr>
            </table>
          			   </div>
	   			    </div>
                    
                    <div class="row" style="margin-top:50px;">
                       <div class="col-sm-2" style="padding-left:0px;">
                          <span style="font-weight:500;">Arch:</span>
                       </div>
                       <div class="col-sm-10">
                        
                        <div class="radio">
                            
                            <input type="radio" class="radio-col-blue" <?php if ($caseDataArchMovement == 'single') { echo 'checked'; } ?>>
                            <label for="arch_single">Single</label>   
                            
                            &nbsp;&nbsp;&nbsp;
                            
                            <input type="radio" class="radio-col-blue" <?php if ($caseDataArchMovement == 'double') { echo 'checked'; } ?>>
                            <label for="arch_double">Double</label>                    
                         
                         </div>
                       </div>
             	    </div>
                    
                    <?php if ($companyID == ALIGNER_CO || $companyID == ALIGNER_CO_CA || $companyID == STRAIGHT_MY_TEETH) { ?>  
                                
                      <div class="row" style="margin-top:50px;">
                         <div class="col-sm-2" style="padding-left:0px;">
                            <span style="font-weight:500;">Night Time:</span>
                         </div>
                         <div class="col-sm-10">
                          
                          <div class="radio">
                              
                              <input type="radio" class="radio-col-yellow" <?php if ($caseDataNightTime == HARD_CODE_ID_YES) { echo 'checked'; } ?>>
                              <label for="night_time_yes">Yes</label>   
                              
                              &nbsp;&nbsp;&nbsp;
                              
                              <input type="radio" class="radio-col-yellow" <?php if ($caseDataNightTime == HARD_CODE_ID_NO) { echo 'checked'; } ?>>
                              <label for="night_time_no">No</label>                    
                           
                           </div>
                         </div>
                      </div>
                                
                    <?php } ?>
                    
                    <div class="" style="margin-top:30px;">
                       
                       <div class="col-sm-12" style="text-align:center">
                        
                         <?php if ($caseDataTreatmentDiagram) { ?>
                   
                          <a href="<?php echo CASE_TREATMENT_DIAGRAM_URL_PATH; ?><?php echo $caseDataTreatmentDiagram; ?>" data-toggle="lightbox" data-title="Case# <?php echo $caseID; ?> - Treatment Diagram"><img id="" src="<?php echo CASE_TREATMENT_DIAGRAM_URL_PATH; ?>thumbnail/<?php echo 'thumbnail_'.$caseDataTreatmentDiagram; ?>" title="Treatment Diagram" /></a>
                  
                      <?php } else { ?>
                      
                      	<span style="color:#fc4b6c">No Treatment diagram exist.</span>
                  
                    <?php } ?>
                       
                       </div>
             		</div>
                    
                    <div class="row" style="margin-top:50px; margin-bottom:40px;">
                       <div class="col-sm-2" style="padding-left:0px;">
                          <span style="font-weight:500;">Stages:</span>
                       </div>
                       <div class="col-sm-10">
                        
                       <div class="row">
                          <div class="col-sm-5">
                           <input type="text" name="" value="<?php echo $caseDataStageUpper; ?>" readonly class="form-control" placeholder="Upper" data-toggle="tooltip" title="" data-original-title="Upper">
                          </div>
                          <div class="col-sm-5">
                           <input type="text" name="" value="<?php echo $caseDataStageLower; ?>" readonly class="form-control" placeholder="Lower" data-toggle="tooltip" title="" data-original-title="Lower">
                          </div>
                          
                        </div>
                       </div>
                     </div>
                    
      			  </div>
                   
                   
                   
                  </div>
                  </form>
                 
                 <form class="form-horizontal form-element">
                 	
                    <?php 
							
							$setupDataCreatorAssignJobs				= getEmployeeAssignJobs($caseDataCreatedBy); // Calling From HR Employees Helper
										
							if ($setupDataCreatorAssignJobs) {
						
									$setupDataCreatorAssignJobs 	= $setupDataCreatorAssignJobs->row_array();
									$setupDataCreatorAssignJobs		= $setupDataCreatorAssignJobs['jobPositionName'];
				
							} else {
					
									$setupDataCreatorAssignJobs 	= NULL;
							}	   		
			
							$caseDataCreatedDate		= date('d F. Y',$caseDataCreated);
							$caseDataCreatedTime		= date('h:i A',$caseDataCreated);
							$caseDataCreatedTimeAgo		= timeAgo($caseDataCreated).' ago';
					?>
                 
                  <div class="form-group">
                    <div class="col-sm-10">
                    <br /><br />
                     <u>Setup Operator:</u> <br />
					<small style="font-size:14px;">
						<?php echo $caseDataCreatedName.' - '.$caseDataCreatedCode; ?> (<?php echo $setupDataCreatorAssignJobs;  ?>) <br />
                    	<?php echo $caseDataCreatedDate; ?> <?php echo $caseDataCreatedTime; ?> (<?php echo $caseDataCreatedTimeAgo; ?>)
                    </small>
                    </div>
                  </div>
                
                </form>
             
              </div>
             
			 <?php } ?>
             
             <?php if ($caseTreatmentData->num_rows() > 0) { ?>
             
             	<div class="tab-pane" id="treatmentDataPreview">
 				  
                  <div class="col-12">
                  
                 <?php 
				 		
							$caseTreatmentDataRow	 		 			  = $caseTreatmentData->row_array();
							$caseTreatmentDataTableID		  			  = $caseTreatmentDataRow['tableID'];
							$caseTreatmentDataIPR		  				  = $caseTreatmentDataRow['caseDataIPR'];
							$caseTreatmentDataExtraction				  = $caseTreatmentDataRow['caseDataExtraction'];
							$caseTreatmentDataEBT		 				  = $caseTreatmentDataRow['caseDataEBT'];
							$caseTreatmentDataCutouts		  			  = $caseTreatmentDataRow['caseCutouts'];
							$caseTreatmentDataSlits	  					  = $caseTreatmentDataRow['caseDataSlits'];
							$caseTreatmentDataAttachment	 			  = $caseTreatmentDataRow['caseDataAttachment'];
							$caseTreatmentDataRetainersUpper			  = $caseTreatmentDataRow['caseDataRetainersUpper'];
							$caseTreatmentDataRetainersLower			  = $caseTreatmentDataRow['caseDataRetainersLower'];
							$caseTreatmentDataStagesUpper	 			  = $caseTreatmentDataRow['caseDataStagesUpper'];
							$caseTreatmentDataStagesLower	 	  		  = $caseTreatmentDataRow['caseDataStagesLower'];
							$caseTreatmentDataCompletedStagesUpper	 	  = $caseTreatmentDataRow['caseDataCompletedStagesUpper'];
							$caseTreatmentDataCompletedStagesLower	 	  = $caseTreatmentDataRow['caseDataCompletedStagesLower'];
							$caseTreatmentDataRemainingStagesUpper	 	  = $caseTreatmentDataRow['caseDataRemainingStagesUpper'];
							$caseTreatmentDataRemainingStagesLower	 	  = $caseTreatmentDataRow['caseDataRemainingStagesLower'];
							$caseTreatmentDataCreated		  			  = $caseTreatmentDataRow['caseDataCreated'];
							$caseTreatmentDataCreatedBy		  			  = $caseTreatmentDataRow['caseDataCreatedBy'];
							$caseTreatmentDataCreatedName				  = $caseTreatmentDataRow['employeeName'];
							$caseTreatmentDataCreatedCode				  = $caseTreatmentDataRow['employeeCode'];
				 ?>
                              
                             <form id="" class="form-horizontal form-element col-12" method="" action="" autocomplete="off">
                             
                              <?php if ($caseTreatmentDataIPR == HARD_CODE_ID_YES) { ?>
                              
                               <div class="row" style="margin-top:20px;" id="">
               					 <div class="col-sm-2" style="padding-left:0px;">
                            	  <span style="font-weight:500;">IPR:</span>
                            	 </div>
                                 
                                 <div class="col-sm-10">
                            	  
                                  <div class="radio">
                                      <input type="radio" name="" id="" value="" class="with-gap radio-col-blue" checked>
                                      <label for="" data-toggle="tooltip" title="" data-original-title="IPR" data-placement="left">Yes</label>                 
                                    
                                      &nbsp;&nbsp;
                                      <input type="radio" name="" id="" value="" class="with-gap radio-col-blue">
                                      <label for="" data-toggle="tooltip" title="" data-original-title="IPR" data-placement="right">No</label>          
                                     
                                    </div>
                                  
                            	 </div>
             				   </div>  
                              
							  <?php } ?>
                              
                              <?php if ($caseTreatmentDataExtraction == HARD_CODE_ID_YES) { ?> 
                              
                               <div class="row" style="margin-top:20px;" id="">
               					 <div class="col-sm-2" style="padding-left:0px;">
                            	  <span style="font-weight:500;">Extraction:</span>
                            	 </div>
                                 
                                 <div class="col-sm-10">
                            	  
                                  <div class="radio">
                                       <input type="radio" name="" id="" value="" class="with-gap radio-col-blue" checked>
                                 	   <label for="" data-toggle="tooltip" title="" data-original-title="Extraction" data-placement="left">Yes</label>                     
                                    	
                                    	&nbsp;&nbsp;
                                       <input type="radio" name="" id="treatment_section_extraction_no" value="" class="with-gap radio-col-blue">
                                       <label for="" data-toggle="tooltip" title="" data-original-title="Extraction" data-placement="right">No</label> 
                                     
                                    </div>
                                  
                            	 </div>
             				   </div>
                               
                              <?php } ?>
                               
                              <?php if ($caseTreatmentDataEBT == HARD_CODE_ID_YES) { ?>
                              
                               <div class="row" style="margin-top:20px;" id="">
               					 <div class="col-sm-2" style="padding-left:0px;">
                            	  <span style="font-weight:500;">EBT:</span>
                            	 </div>
                                 
                                 <div class="col-sm-10">
                            	  
                                  <div class="radio">
                                       <input type="radio" name="" id="" value="" class="with-gap radio-col-blue" checked>
                                       <label for="" data-toggle="tooltip" title="" data-original-title="EBT" data-placement="left">Yes</label>                        
                                    	
                                    	&nbsp;&nbsp;
                                       <input type="radio" name="" id="" value="<?php echo HARD_CODE_ID_NO; ?>" class="with-gap radio-col-blue">
                                       <label for="" data-toggle="tooltip" title="" data-original-title="EBT" data-placement="right">No</label>   
                                     
                                    </div>
                                  
                            	 </div>
             				   </div>
                              
                              <?php } ?>
                              
                              <?php if ($caseTreatmentDataCutouts == HARD_CODE_ID_YES) { ?> 
                              
                               <div class="row" style="margin-top:20px;" id="">
               					 <div class="col-sm-2" style="padding-left:0px;">
                            	  <span style="font-weight:500;">Cutouts:</span>
                            	 </div>
                                 
                                 <div class="col-sm-10">
                            	  
                                  <div class="radio">
                                        <input type="radio" name="" id="" value="" class="with-gap radio-col-blue" checked>
                                   	    <label for="" data-toggle="tooltip" title="" data-original-title="Cutouts" data-placement="left">Yes</label>                             
                                    	
                                    	&nbsp;&nbsp;
                                       <input type="radio" name="" id="" value="<?php echo HARD_CODE_ID_NO; ?>" class="with-gap radio-col-blue">
                                       <label for="" data-toggle="tooltip" title="" data-original-title="Cutouts" data-placement="right">No</label> 
                                     
                                    </div>
                                  
                            	 </div>
             				   </div>
                              
                              <?php } ?>
                               
                              <?php if ($caseTreatmentDataSlits == HARD_CODE_ID_YES) { ?>
                              
                               <div class="row" style="margin-top:20px;" id="">
               					 <div class="col-sm-2" style="padding-left:0px;">
                            	  <span style="font-weight:500;">Slits:</span>
                            	 </div>
                                 
                                 <div class="col-sm-10">
                            	  
                                  <div class="radio">
                                       <input type="radio" name="" id="" value="<?php echo HARD_CODE_ID_YES; ?>" class="with-gap radio-col-blue" checked>
                                    	<label for="" data-toggle="tooltip" title="" data-original-title="Slits" data-placement="left">Yes</label>                                 
                                    	
                                    	&nbsp;&nbsp;
                                       <input type="radio" name="" id="" value="<?php echo HARD_CODE_ID_NO; ?>" class="with-gap radio-col-blue">
                                       <label for="" data-toggle="tooltip" title="" data-original-title="Slits" data-placement="right">No</label>   
                                     
                                    </div>
                                  
                            	 </div>
             				   </div>
                              
                              <?php } ?>
                               
                              <?php if ($caseTreatmentDataAttachment == HARD_CODE_ID_YES) { ?>
                             
                               <div class="row" style="margin-top:20px;" id="">
               					 <div class="col-sm-2" style="padding-left:0px;">
                            	  <span style="font-weight:500;">Attachment:</span>
                            	 </div>
                                 
                                 <div class="col-sm-10">
                            	  
                                  <div class="radio">
                                       <input type="radio" name="" id="treatment_section_attachment_yes" value="" class="with-gap radio-col-blue" checked>
                                       <label for="" data-toggle="tooltip" title="" data-original-title="Attachment" data-placement="left">Yes</label>                               
                                    	
                                    	&nbsp;&nbsp;
                                       <input type="radio" name="" id="treatment_section_attachment_no" value="" class="with-gap radio-col-blue">
                                       <label for="" data-toggle="tooltip" title="" data-original-title="Aattachment" data-placement="right">No</label>    
                                     
                                    </div>
                                  
                            	 </div>
             				   </div>
                               
                              <?php } ?>
                              
                              <?php if ($caseTreatmentDataRetainersUpper == HARD_CODE_ID_YES || $caseTreatmentDataRetainersLower == HARD_CODE_ID_YES) { ?> 
                              
                               <div class="row" style="margin-top:30px;" id="">
               					 <div class="col-sm-2" style="padding-left:0px;">
                            	  <span style="font-weight:500;">Retainers:</span>
                            	 </div>
                                 
                                 <div class="col-sm-10">
                            	  
                                  <div class="radio">
                                      
                                    <?php if ($caseTreatmentDataRetainersUpper == HARD_CODE_ID_YES) { ?>
                                      
                                      <input type="checkbox" name="" id="" value="<?php echo HARD_CODE_ID_YES; ?>" class="chk-col-yellow" checked>
                                      <label for="" data-toggle="tooltip" title="" data-original-title="Retainers">Upper</label>                                     
                                   
                                    <?php } ?>	
                                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <?php if ($caseTreatmentDataRetainersLower == HARD_CODE_ID_YES) { ?> 
                                    
                                       <input type="checkbox" name="" id="" value="" class="chk-col-yellow" checked>
                                       <label for="" data-toggle="tooltip" title="" data-original-title="Retainers">Lower</label>      
                                    
									<?php } ?>
                                    
                                    </div>
                                  
                            	 </div>
             				   </div>
                               
                             <?php } ?>
                               
                               <div class="row" style="margin-top:50px;">
               					 <div class="col-sm-2" style="padding-left:0px;">
                                  	<span style="font-weight:500;">Stages:</span>
                            	 </div>
                                 <div class="col-sm-10">
                            	  
                                 <div class="row">
                                    <div class="col-sm-5">
                                    <input type="text" name="" id="" value="<?php echo $caseTreatmentDataStagesUpper; ?>" class="form-control allow-numbers" placeholder="Upper" data-toggle="tooltip" title="" data-original-title="Upper" readonly />
                                    </div>
                                    <div class="col-sm-5">
                                     <input type="text" name="" id="" value="<?php echo $caseTreatmentDataStagesLower; ?>" class="form-control allow-numbers" placeholder="Lower" data-toggle="tooltip" title="" data-original-title="Lower" readonly />
                                    </div>
                                    
                                  </div>
                            	 </div>
             				   </div>
                               
                               <div class="row" style="margin-top:50px;">
               					 <div class="col-sm-2" style="padding-left:0px;">
                                  	<span style="font-weight:500;">Completed:</span>
                            	 </div>
                                 <div class="col-sm-10">
                            	  
                                 <div class="row">
                                    <div class="col-sm-5">
                                    <input type="text" name="" id="" value="<?php echo $caseTreatmentDataCompletedStagesUpper; ?>" class="form-control allow-numbers" placeholder="Upper" data-toggle="tooltip" title="" data-original-title="Upper" readonly />
                                    </div>
                                    <div class="col-sm-5">
                                     <input type="text" name="" id="" value="<?php echo $caseTreatmentDataCompletedStagesLower; ?>" class="form-control allow-numbers" placeholder="Lower" data-toggle="tooltip" title="" data-original-title="Lower" readonly />
                                    </div>
                                    
                                  </div>
                            	 </div>
             				   </div>
                               
                               <div class="row" style="margin-top:50px; margin-bottom:40px;">
               					 <div class="col-sm-2" style="padding-left:0px;">
                                  	<span style="font-weight:500;">Remaining:</span>
                            	 </div>
                                 <div class="col-sm-10">
                            	  
                                 <div class="row">
                                    <div class="col-sm-5">
                                    <input type="text" name="" id="" value="<?php if ($caseTreatmentDataRemainingStagesUpper) { echo $caseTreatmentDataRemainingStagesUpper; }  else { echo 0; } ?>" class="form-control allow-numbers" placeholder="Upper" data-toggle="tooltip" title="" data-original-title="Upper" readonly />
                                    </div>
                                    <div class="col-sm-5">
                                     <input type="text" name="" id="" value="<?php if ($caseTreatmentDataRemainingStagesLower) { echo  $caseTreatmentDataRemainingStagesLower; }  else { echo 0; } ?>" class="form-control allow-numbers" placeholder="Lower" data-toggle="tooltip" title="" data-original-title="Lower" readonly />
                                    </div>
                                    
                                  </div>
                            	 </div>
             				   </div>
                     </form>
                 
                 </div>
                 
                  <br />
              	  <br />
                 <form class="form-horizontal form-element">
                 	
                    <?php 
							
							$treatmentDataCreatorAssignJobs			= getEmployeeAssignJobs($caseTreatmentDataCreatedBy); // Calling From HR Employees Helper
										
							if ($treatmentDataCreatorAssignJobs) {
						
									$treatmentDataCreatorAssignJobs 	= $treatmentDataCreatorAssignJobs->row_array();
									$treatmentDataCreatorAssignJobs		= $treatmentDataCreatorAssignJobs['jobPositionName'];
				
							} else {
					
									$treatmentDataCreatorAssignJobs 	= NULL;
							}	   		
			
							$caseTreatmentDataCreatedDate		= date('d F. Y',$caseTreatmentDataCreated);
							$caseTreatmentDataCreatedTime		= date('h:i A',$caseTreatmentDataCreated);
							$caseTreatmentDataCreatedTimeAgo	= timeAgo($caseTreatmentDataCreated).' ago';
					?>
                 
                  <div class="form-group">
                    <div class="col-sm-10">
                    <br /><br />
                     <u>Treatment Operator:</u> <br />
					<small style="font-size:14px;">
						<?php echo $caseTreatmentDataCreatedName.' - '.$caseTreatmentDataCreatedCode; ?> (<?php echo $treatmentDataCreatorAssignJobs;  ?>) <br />
                    	<?php echo $caseTreatmentDataCreatedDate; ?> <?php echo $caseTreatmentDataCreatedTime; ?> (<?php echo $caseTreatmentDataCreatedTimeAgo; ?>)
                    </small>
                    </div>
                  </div>
                  
                </form>
              </div>
             
			 <?php } ?>
             
            </div>
          </div>
         </div>
        
      </div>
    </section>
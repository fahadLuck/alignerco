<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>


	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Cases</li>
      
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <div class="row">
        
        <div class="col-12">
           

           
           
          <div class="box">
            <div class="box-header">
              
            
              <h3 class="box-title">Shipments<small>(<?php echo $counter; ?>)</small></h3>


                <div class="box-tools">
                  
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="example" class="display" style="width:100%">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Shipment Number</th>
                  <th>Delievery Services</th>
                  <th>No Of Cases Missing</th>
                  <!-- <th width="305">Official Email</th>
                  <th width="179">Mobile</th>
                  <th width="90">Action</th> -->
                </tr>
                <thead>
               <tbody>
                  <?php
                  $count=1;
                  foreach($missingCases->result() as $mc)
                    {
                      $counter=$count++;
                      $trackingNumber=$mc->trackingNumber;
                      $deliveryServicesName=$mc->deliveryServicesName;
                      $totalCases=$mc->totalCases;
                      $table_ID=$mc->tableID;

                      $tableID = encodeString($mc->trackingNumber);
                    ?>      
                    <tr>
                    <td><?php echo $counter; ?></td>
                    <td><a href="<?php echo base_url() ?>/cases-missing-details/<?php echo $tableID; ?>/"><?php echo $trackingNumber; ?></a></td>
                    <td> <a href="<?php echo base_url() ?>/cases-missing-details/<?php echo $tableID; ?>/"><?php echo $deliveryServicesName; ?></a></td>
                    <td> <a href="<?php echo base_url() ?>/cases-missing-details/<?php echo $tableID; ?>/"><?php echo $totalCases; ?></a></td>
                    <!-- <td>sdfsdfsd</td>

                    <td>
                      adsadad
                    </td> -->
                </tr>
              <?php } ?>
                              
               
               </tbody>
                   <!-- <tfoot>
                      <tr>
                        <th>Sr.</th>
                        <th>Shipment Number</th>
                        <th>Delievery Services</th>
                        <th>No Of Cases Missing</th>
                      </tr>
                  </tfoot> -->
              </table>
            </div>
           
          </div>
		</div>
      </div>
     
    </section>






<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
   
   
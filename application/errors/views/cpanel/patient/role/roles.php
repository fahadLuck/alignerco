<script src="<?php echo base_url();?>backend_assets/js/jquery-3.2.1.min.js"></script>
<script language="javascript" type="text/javascript">
		
		 $(document).ready(function () {
		 		
				$('.update-role').click(function() {
						
					var roleSefURL =   $(this).attr('id');
						
					window.location.href = '<?php echo base_url(); ?>role-edit/'+roleSefURL;
				 });
				
				$('.delete-role').click(function() {
						
						var roleName   =  $(this).attr('name');
						var roleSefURL =   $(this).attr('id');
						
						if (confirm('Are you sure you want to remove '+roleName+' role?')) {
   							
							window.location.href = '<?php echo base_url(); ?>role-remove/'+roleSefURL;
							
						} else {
   							
							return false;
						}
				});
		 });
</script>
<div class="page-wrapper">
      <div class="content container-fluid">
      
          <!-- Page Header -->
          <div class="page-header">
              <div class="row">
                  <div class="col">
                      <h3 class="page-title"><?php echo $pageHeading; ?></h3>
                  </div>
              </div>
          </div>
          <!-- /Page Header -->
          
          <?php if ($this->session->userdata('admin_msg') !='')  { ?>
     
                  <div class="alert alert-secondary alert-dismissible fade show" role="alert">
                      <strong>Alert!</strong> <?php echo $this->session->userdata('admin_msg');?>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                  </div>
  
      	  <?php $this->session->unset_userdata('admin_msg'); } ?>
					
					<div class="row">
						<div class="col-sm-4 col-md-4 col-lg-4 col-xl-3">
							<a href="<?php echo base_url(); ?>role-add/" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Add Roles</a>
							<div class="roles-menu">
								<ul>
                                
                                 <?php  if ($roles) {
									
											$counter = 1;
											foreach ($roles->result() as $role) {
										
												$tableID 		=   encodeString($role->tableID);
												$roleName   	=   $role->roleName;
												$roleSefURL   	=   $role->roleSefURL;
								?>
                                
                                              <li class="">
                                                  <a href="javascript:void(0);"><?php echo $roleName; ?>
                                                      <span class="role-action">
                                                          <span class="action-circle large update-role" id="<?php echo $roleSefURL; ?>">
                                                              <i class="material-icons">edit</i>
                                                          </span>
                                                          <span class="action-circle large delete-btn delete-role" id="<?php echo $roleSefURL; ?>" name="<?php echo $roleName; ?>">
                                                              <i class="material-icons">delete</i>
                                                          </span>
                                                      </span>
                                                  </a>
                                              </li>
                                    
                                <?php } } ?>
								</ul>
							</div>
						</div>
						<div class="col-sm-8 col-md-8 col-lg-8 col-xl-9">
							<h6 class="card-title m-b-20">Module Access</h6>
							<div class="m-b-30">
								<ul class="list-group notification-list">
									
                                    <?php  if ($modules) {
									
											$counter = 1;
											foreach ($modules->result() as $module) {
										
												$tableID 			=   encodeString($module->tableID);
												$moduleSingularName	=   $module->moduleSingularName;
												$moduleNamePlural   =   $module->modulePluralName;
								?>
                                    
                                    <li class="list-group-item">
										<?php echo $moduleSingularName; ?>
										<div class="status-toggle">
											<input type="checkbox" id="staff_module_<?php echo $counter;?>" class="check">
											<label for="staff_module_<?php echo $counter;?>" class="checktoggle">checkbox</label>
										</div>
									</li>
							   
                               <?php $counter++; } } ?>
                                    
                                 
								</ul>
							</div>      	
							
						</div>
					</div>
                </div>
				<!-- /Page Content -->
             </div>
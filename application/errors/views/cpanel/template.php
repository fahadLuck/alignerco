<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="<?php echo base_url(); ?>backend_images/favicon.png"> -->
	
    <?php 
 
 			   if ( @$pageMeta && $metaType == 'external' ) {
				  
					  $pageName  			 	  = $pageMeta['name'];
					  $pageTitle 		      	  = $pageMeta['meta_title'];
					  $pageDescription  		  = $pageMeta['meta_description'];
					  $pageKeyword      	 	  = $pageMeta['meta_keyword'];
				  
			  } elseif ( $metaType == 'internal' ) {
				  
					  $pageName   		    = @$pageName;
					  $pageTitle 		    = @$pageTitle;
					  $pageDescription 		= @$pageDescription;
					  $pageKeywords       	= @$pageKeyword;
			  }
  ?>
    
    <title><?php echo $pageTitle; ?></title>

    
 <!-- changes in file order  -->
    <script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.min.js"></script>






    
	<!-- Bootstrap v4.0.0-beta -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_plugins/iCheck/flat/blue.css">

    
	<!-- theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/master_style.css">
	
	<!-- maximum_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/skins/_all-skins.css">
	
	<!-- date picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
	
	<!-- daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css">
	
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
	
    <!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_assets/vendor_components/select2/dist/css/select2.min.css">	

	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/master_style.css">

	<!-- maximum_admin Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>backend_css/skins/_all-skins.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

     
  </head>
  
  <?php 
  	
		$loggingEmployee = getLoggingEmployee($this->session->userdata(USER_TEAM_ID_SESSION));
		
		if ($loggingEmployee) {
			
				$loggingEmployee 	= $loggingEmployee->row_array();	
				$employeeGender	 	= $loggingEmployee['employeeGenderID'];
					
				if ($employeeGender) {
							
							if ($employeeGender == HARD_CODE_ID_MALE)	 {
									
									$setSkinColor = 'skin-blue';	
							}
							
							if ($employeeGender == HARD_CODE_ID_FEMALE)	 {
									
									$setSkinColor = 'skin-yellow';	
							}
				} else {
					
					$setSkinColor = 'skin-yellow';	
				}
				
		} else {
				
				$setSkinColor = 'skin-blue';	
		}
		
		$sidebarCollapse = NULL;
		
		$result =  $this->model_login->getAccountInfo($this->session->userdata(USER_ACCOUNT_ID_SESSION));
		
		$this->session->set_userdata(USER_PHOTO_SESSION,$result['temMemberPhoto']);
		
		$userPhoto 		= $this->session->userdata(USER_PHOTO_SESSION);
		
		if ($userPhoto) {
				
				$sidebarCollapse = NULL;	
		}
  
  ?>

<body class="hold-transition <?php echo $setSkinColor; ?> <?php echo $sidebarCollapse; ?> sidebar-mini">
<div class="wrapper">

   <?php 
					
		 
					
		$employeeAssignDepartment 			= NULL;
		$employeeAssignJobPosition			= NULL;
		  
		$employeeJobDetails['employeeAssignDepartment'] 	=  $employeeAssignDepartment;
		$employeeJobDetails['employeeAssignJobPosition']    =  $employeeAssignJobPosition;
 			
  ?>

  <?php echo $this->load->view('cpanel/header',$employeeJobDetails); ?>
  
  <?php echo $this->load->view('cpanel/menu',$employeeJobDetails); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $pageHeading; ?>
        <small><?php echo $subHeading; ?></small>
      </h1>
      
    
   <?php echo $contents; ?>
   
  </div>
  <!-- /.content-wrapper -->
 
  <?php echo $this->load->view('cpanel/footer'); ?>

  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  

   <!-- changes in file order  -->
   	 
	<!-- jQuery 3 -->
	<!-- <script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery/dist/jquery.min.js"></script> -->

	
	
	<!-- jQuery UI 1.11.4 -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery-ui/jquery-ui.js"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap v4.0.0-beta -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
	
    <!-- DataTables -->
	<!-- <script src="<?php echo base_url(); ?>backend_assets/vendor_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script> -->
    
	<!-- Sparkline -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>
	
	<!-- jvectormap -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>	
	<script src="<?php echo base_url(); ?>backend_assets/vendor_plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	
	<!-- jQuery Knob Chart -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery-knob/js/jquery.knob.js"></script>
	
	<!-- daterangepicker -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<!-- datepicker -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
	
	<!-- Bootstrap WYSIHTML5 -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
	
	<!-- Slimscroll -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- maximum_admin App -->
	<script src="<?php echo base_url(); ?>backend_js/template.js"></script>
	
	
    <!-- Select2 -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_components/select2/dist/js/select2.full.js"></script>
    
    
    <!-- InputMask -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_plugins/input-mask/jquery.inputmask.js"></script>
	<script src="<?php echo base_url(); ?>backend_assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="<?php echo base_url(); ?>backend_assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js"></script>
    
     <!-- gallery -->
	<script type="text/javascript" src="<?php echo base_url(); ?>backend_assets/vendor_components/gallery/js/animated-masonry-gallery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>backend_assets/vendor_components/gallery/js/jquery.isotope.min.js"></script>
   
    <!-- fancybox -->
    <script type="text/javascript" src="<?php echo base_url(); ?>backend_assets/vendor_components/lightbox-master/dist/ekko-lightbox.js"></script>
    
    <!-- This is data table -->
    <!-- <script src="<?php echo base_url(); ?>backend_assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script> -->
    
     <!-- maximum_admin for advanced form element -->
	<script src="<?php echo base_url(); ?>backend_js/pages/advanced-form-element.js"></script>
    
    <!-- iCheck -->
	<script src="<?php echo base_url(); ?>backend_assets/vendor_plugins/iCheck/icheck.js"></script>
	
	<!-- maximum_admin for mailbox -->
	<script src="<?php echo base_url(); ?>backend_js/pages/mailbox.js"></script>
    
    
    <script type="text/javascript">
    
	$(document).ready(function($) {
    
	    // delegate calls to data-toggle="lightbox"
        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
    
	        event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function() {
                    if (window.console) {
                        return console.log('Checking our the events huh?');
                    }
                },
                onNavigate: function(direction, itemIndex) {
                    if (window.console) {
                        return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                    }
                }
            });
        });
      
	    //Programatically call
        $('#open-image').click(function(e) {
            e.preventDefault();
            $(this).ekkoLightbox();
        });
        
		// navigateTo
        $(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
            event.preventDefault();
            var lb;
            return $(this).ekkoLightbox({
                onShown: function() {
                    lb = this;
                    $(lb.modal_content).on('click', '.modal-footer a', function(e) {
                        e.preventDefault();
                        lb.navigateTo(2);
                    });
                }
            });
        });
    });
   
    </script>

	
</body>
</html>

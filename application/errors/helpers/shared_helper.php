<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 

	
	function DATABASE_DATE_FORMAT($date=NULL) {
			
			if ($date) {
				
				$date = strtotime($date);
				
				$date = date('Y-m-d',$date);
				
				return $date;
				
			} else {
			
				return NULL;
			}
			
			/*
				
				Function Input 	: 1422860066 || 25 - 05 -2016 || Can Any Date Format
				Function Output	: 2016-05-21 (Year-Month-Day)
				
			*/
	}
	
	function DATABASE_NOW_DATE_TIME_FORMAT() {
		
		return time();
		
		/*
				
				Function Input 	: No Input Required
				Function Output	: 1459969639 (Date And Time StrToTime Format)
				
		*/
	}
	
	function DATABASE_NOW_DATE_FORMAT() {
		
		return date('Y-m-d',time());
		
		/*
				
				Function Input 	: No Input Required
				Function Output	: 2016-09-31 (Date Format)
				
		*/
	}
	
	function checkEmptyData($variableData) {
					
				if ($variableData) {
							
						 	return $variableData;
				} else {
						
							return NOT_AVAILABLE;
				}
				
				
				/*
				
				Function Input 	: PRK || ESS || The Educators School|| Any String & Numeric Value
				Function Output	: (PKR) || (ESS) || (The Educators Schol)
				
				*/
	}
	
	function checkEmptyID($ID=NULL) {
		
					if ($ID == NULL || empty($ID)) {
				 
				 			redirect('not-found/');	
					}
	}
	
	function checkEmptySefURL($sefURL=NULL) {
		
					if ($sefURL == NULL || empty($sefURL)) {
				 
				 			redirect('not-found/');	
					}
	}
	
	function getStatusName($ID) {
		
		$CIH    =    & get_instance();
		
		$result = 	 $CIH->model_shared->getRecord('name',STATES_LOOKUP_TABLE,'id',$ID);
		
		if ($result->num_rows() > 0 ) {
				
				 $row = $result->row_array();
				 return $row['name'];
		
		} else {
				return false;
		}
		
	}
	
	function getStatusSefURL($ID) {
		
		$CIH    =    & get_instance();
		
		$result = 	 $CIH->model_shared->getRecord('sef_url',STATES_LOOKUP_TABLE,'id',$ID);
		
		if ($result->num_rows() > 0 ) {
				
				 $row = $result->row_array();
				 
				 	if ($row['sef_url']) {
							
							return $row['sef_url']; 
					
					} else {
							
						return false;			
					}
		
		} else {
			
				return false;
		}
		
	}
	
	function getStatusID($sefURL) {
		
		$CIH    =    & get_instance();
		
		$result = 	 $CIH->model_shared->getRecord('id',STATES_LOOKUP_TABLE,'sef_url',$sefURL);
		
		if ($result->num_rows() > 0 ) {
				
				 $row = $result->row_array();
				 
				 return $row['id']; 
				
		} else {
			
				return false;
		}
		
	}
	
	function getAllCountries() {
		
		$CIH    =  & get_instance();
		
		$result =  $CIH->model_shared->getRecordMultipleWhereOrderBy('id,name,sef_url,',COUNTRIES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED),'name','ASC');
		
		if ($result->num_rows() > 0 ) {
				
				 return $result;
		
		} else {
				return false;
		}
		
	}
	
	function getAllStates() {
		
		$CIH    =  & get_instance();
		
		$result =  $CIH->model_shared->getRecordMultipleWhereOrderBy('id,name,sef_url,',STATES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED),'name','ASC');
		
		if ($result->num_rows() > 0 ) {
				
				 return $result;
		
		} else {
				return false;
		}
		
	}



	function getAllCompanies() {
		
		$CIH    =  & get_instance();
		
		$result =  $CIH->model_shared->getRecordMultipleWhereOrderBy('id,name,sef_url,',MEDICAL_COMPANIES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED),'name','ASC');
		
		if ($result->num_rows() > 0 ) {
				
				 return $result;
		
		} else {
				return false;
		}
		
	}
	


	
	function getAllCities() {
		
		$CIH    =  & get_instance();
		
		$result =  $CIH->model_shared->getRecordMultipleWhereOrderBy('id,name,sef_url,',CITIES_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED),'name','ASC');
		
		if ($result->num_rows() > 0 ) {
				
				 return $result;
		
		} else {
				return false;
		}
		
	}
	
	function getCityName($ID) {
		
		$CIH    =    & get_instance();
		
		$result = 	 $CIH->model_shared->getRecord('name',CITIES_TABLE,'id',$ID);
		
		if ($result->num_rows() > 0 ) {
				
				 $row = $result->row_array();
				 return $row['name'];
		
		} else {
				return false;
		}
		
	}
	
	function getCurrencies() {
				
		 $CIH    =    & get_instance();
		
		$result = 	 $CIH->model_shared->getRecordMultipleWhereOrderBy('id,name,code,symbol',CURRENCY_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED),'name','ASC');
		
		if ($result->num_rows() > 0 ) {
				
				return $result;
				
		} else {
			
				return false;
		}
		
	}
	
	function getDateFormats() {
				
		 $CIH    =    & get_instance();
		
		$result = 	 $CIH->model_shared->getRecordMultipleWhereOrderBy('id,name,description',DATE_FORMATS_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED),'name','ASC');
		
		if ($result->num_rows() > 0 ) {
				
				return $result;
				
		} else {
			
				return false;
		}
		
	}
	
	function getTimeFormats() {
				
		 $CIH    =    & get_instance();
		
		$result = 	 $CIH->model_shared->getRecordMultipleWhereOrderBy('id,name,description',TIME_FORMATS_TABLE,array('publish_status' => HARD_CODE_ID_PUBLISHED),'name','ASC');
		
		if ($result->num_rows() > 0 ) {
				
				return $result;
				
		} else {
			
				return false;
		}
		
	}
	
	function timeAgo($ptime) {
   		
		 $etime = time() - $ptime;
    
			if ($etime < 1) {
				return '0 seconds';
			}
			
			$a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
						30 * 24 * 60 * 60       =>  'month',
						24 * 60 * 60            =>  'day',
						60 * 60                 =>  'hour',
						60                      =>  'minute',
						1                       =>  'second'
						);
			
			foreach ($a as $secs => $str) {
				$d = $etime / $secs;
				if ($d >= 1) {
					$r = round($d);
					return $r . ' ' . $str . ($r > 1 ? 's' : '');
			}
			
	    }
		
		 /*
				
				Function Input 	: 1465364869 (Date Converted Strtotime Format)
				Function Output	: 3 secs ago
				
		 */
	}

	function SEF_URLS($str, $replace=array(), $delimiter='-') {
	
		$str=trim($str);
		if( !empty($replace) ) {
		
			 $str = str_replace((array)$replace, ' ', $str);
		}
		
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		return $clean;
	}
	
	function SEF_URLS_CONCAT($str, $replace=array(), $delimiter='-') {
	
		$str=trim($str);
		if( !empty($replace) ) {
		
			 $str = str_replace((array)$replace, ' ', $str);
		}
		
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		$number	= mt_rand(1,999);
		return $clean.'-'.$number;
	}
	
?>
<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 
	
	function getEmployeeProfile($organizationID,$employeeID) {
	 
		$CIH     =   & get_instance();
	
		$return  =   array();
		
		$result  =   $CIH->model_employee->getEmployeeProfile($organizationID,$employeeID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
	}
	
	function getEmployeeAssignJobs($employeeID) {
	 
		$CIH      =   & get_instance();
		$return  =   array();
		
		$result  =   $CIH->model_employee->getEmployeeAssignJobs($employeeID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }
?>
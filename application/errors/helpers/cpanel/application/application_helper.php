<?php if ( ! defined('BASEPATH')) exit ('No direct script  allow'); 
	
	function userAssignedRolesIDs() {
	 		
			$CIH   =  & get_instance();
				
			$userTeamID			= $CIH->session->userdata(USER_TEAM_ID_SESSION);
		
			$assignedRoles		= array();
			$assignRoles 		= getUserRoles($userTeamID); // Calling From Application Helper
			
			if ($assignRoles) {
							
				  foreach($assignRoles->result() as $assignRole) {
								  
							$assignedRoles[] =    $assignRole->roleID;
				  }
			}
			
			return $assignedRoles;
	}
	
	function userModuleAccessPremissions() {
	 
		$CIH      =   & get_instance();
		
		$userOfficialAccountID				=	$CIH->session->userdata(USER_ACCOUNT_ID_SESSION);
		$userTeamID							=	$CIH->session->userdata(USER_TEAM_ID_SESSION);
		$userOrganizationID					=   $CIH->session->userdata(USER_ORGANIZATION_ID_SESSION);
		$assignedRoles						=	array();
		$accessModules						=	array();
		
		$assignRoles 						= 	getUserRoles($userTeamID); // Calling From Application Helper
		
		if ($assignRoles) {
							
			  foreach($assignRoles->result() as $assignRole) {
						
						    $assignRoleID    	= $assignRole->roleID;
						   
							$assignModules 		= getRolePrivilegesModules($assignRoleID); // Calling From Application Helper
							 
							if ($assignModules) {
									  
								  foreach($assignModules->result() as $module) {
											  
												  $accessModules[] = $module->privilegeModuleID;
								  }	
							}
				  }
		  }
		  
		// Now Check Special User Privielegs Module Access
		$assignModules  = getOfficialAccountPrivilegesModules($userOfficialAccountID); // Calling From Application Helper
			
		if ($assignModules) {
								
					foreach($assignModules->result() as $module) {
							
								$accessModules[] = $module->privilegeModuleID;
					}	
		}
						
		$accessModules = array_unique($accessModules); 
		
		return $accessModules;
	}
	
	function getUserRoles($teamID) {
	 			
			$CIH     =   & get_instance();

			$userOfficialAccountID				=	$CIH->session->userdata(USER_ACCOUNT_ID_SESSION);
		    $userTeamID							=	$CIH->session->userdata(USER_TEAM_ID_SESSION);
		    $userOrganizationID					=   $CIH->session->userdata(USER_ORGANIZATION_ID_SESSION);

		    //IF USER IS PATIENT
		    if($userTeamID=='' AND $userOfficialAccountID!='')
		    {
		    	$result  =  $CIH->model_application->userRolesForPatient($userOfficialAccountID);
		    }

		    //IF USER IS NORMAL USER
		    else
		    {
		    	$result  =  $CIH->model_application->userRoles($teamID);
		    }

		   /* $result  =  $CIH->model_application->userRoles($teamID);*/
		
			
		
			 if ($result) {
		 	 
					 return $result; 
			 
		 	 } else {
			
			 		return false;	
			}
	}
	
	function getRolePrivilegesModules($roleID) {
	
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getRolePrivilegesModules($roleID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }	
 
 	function getOfficialAccountPrivilegesModules($userOfficialAccountID) {
	 
		$CIH      =   & get_instance();
		$return  =   array();
		
		$result   =   $CIH->model_application->getOfficialAccountPrivilegesModules($userOfficialAccountID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }	
 
	function getLoggingEmployee($employeeID) {
	 
		$CIH      =   & get_instance();
		$return  =   array();
		
		$result  =   $CIH->model_application->getLoggingEmployee($employeeID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }	
	
	function userInfo($accountID) {
	 
		$CIH     =   & get_instance();
		$return  =   array();
		
		$result  =   $CIH->model_application->userInfo($accountID);	
		
		 if ($result) {
			 
			 $row = $result->row_array();
			 
			 		if ($row) {
						
							$teamID 					=  $row['teamID'];
							$HRID 						=  $row['teamMemberHRID'];
							
							$designation 				= NULL;
				
							$roles 						= $CIH->model_application->userRoles($teamID);
							
							if ($roles) {
									
									if ($roles->num_rows() > 1) {
										
											$designation = 'Multiple Roles';			
							
									} else {
											
										$role 			= $roles->row_array();
										$designation    = $role['roleName'];
									}
							
							} else {
									
									$designation 			= NULL;		
							}	
							
						$return['accountTeamID']    				 		= $teamID ;
						$return['accountEmail']  	  					 	= $row['accountEmail'];
						$return['accountMobile']    					 	= $row['accountMobile'];
						$return['accountStatus']      				 		= getStatusName($row['accountStatus']); // Calling From Shared Helper
						$return['accountCreated']      				 		= $row['accountCreated'];
						
						$return['organizationID']    						= $row['teamMemberOrganizationID'];
						$return['prefixName']    							= $row['prefixName'];
						$return['name']    								 	= $row['teamMemberName'];
						$return['company']    								= $row['company'];
						$return['email']    								= $row['teamMemberPersonalEmail'];
						$return['designation']    							= $designation;
						$return['CNICFormatted']    						= $row['teamMemberCNICFormatted'];
						$return['CNIC']    								 	= $row['teamMemberCNIC'];
						$return['mobile']    							 	= $row['teamMemberMobile'];
						$return['mobileFormatted']    						= $row['teamMemberMobileFormatted'];
						$return['mobileVerifiedStatus']   					= $row['teamMemberMobileVerifiedStatus'];
						$return['about']    								= $row['teamMemberAbout'];
						$return['photo']    								= $row['teamMemberPhoto'];
							
					} else {
							
							$return['accountTeamID']    					= NULL;
							$return['accountProfileReferenceTable']    		= NULL;
							$return['accountEmail']  	  					= NULL;
							$return['accountMobileFormatted']    			= NULL;
							$return['accountMobile']    					= NULL;
							$return['accountStatus']      				 	= NULL;
							$return['accountCreated']      				 	= NULL;
							$return['accountCreatedBy']      			 	= NULL;
							
							$return['instituteID']    						= NULL;
							$return['prefixName']    						= NULL;
							$return['name']    								= NULL;
							$return['email']    							= NULL;
							$return['designation']    						= NULL;
							$return['CNICFormatted']    					= NULL;
							$return['CNIC']    								= NULL;
							$return['mobile']    							= NULL;
							$return['mobileFormatted']    				 	= NULL;
							$return['mobileVerifiedStatus']   			 	= NULL;
							$return['about']    							= NULL;
							$return['photo']    							= NULL;
							
						
					}
			 
			 return $return; 
			 
		 } else {
			
			 distroySessions(); // Calling From Application Helper
			 redirect('my-dashboard/');
		}
 }
 	
	
	
	function getPrefix() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getPrefix();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }	
 	
	function getGenderMaleFemale() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getGenderMaleFemale();	
	
		 if ($result) {
			
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
	 }
	 
	function getGenderName($ID) {
		
		$CIH      =   & get_instance();
		
		$result   =   $CIH->model_application->getGenderName($ID);	
		
		if ($result->num_rows() > 0 ) {
				
				 $row 	= $result->row_array();
				 
				 $name  =  $row['genderName'];
				 $code  = fillBrackets($row['genderCode']); // Calling From General Helper
				 
				 $name  = $name.' '.$code;
				 
				 return $name;
		
		} else {
				
				return false;
		}
		
	}
 	
	function getMaritalStatus() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getMaritalStatus();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }	
 
 	function getNationalities() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getNationalities();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }
 
 	function getLanguages() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getLanguages();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }	
 	
	function getBloodGroups() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getBloodGroups();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }	
 	
	function getReligions() {
				
				$CIH     = &get_instance();
				
				$return  = array();
				
				$result  = $CIH->model_application->getReligions();	
		
		 		if ($result) {
			 
			 		return $result; 
			 
		 		} else {
			
			 		return false;	
				}
	}
	
	function getOccupations() {
				
				$CIH     =    &get_instance();
				
				$return  =   array();
				
				$result  =   $CIH->model_application->getOccupations();	
		
		 		if ($result) {
			 
			 		return $result; 
			 
		 		} else {
			
			 		return false;	
				}
	}
	
	function getModeOfEmployment() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getModeOfEmployment();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 	}	
	
	function getEmploymentStatus() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getEmploymentStatus();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }
	
	function getPayFrequency() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getPayFrequency();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }
	
	function getDiseases() {
	 
		$CIH      =   & get_instance();
		$return   =   array();
		
		$result   =   $CIH->model_application->getDiseases();	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
 }
	
	function getName($tableName,$id) {
		
		$CIH    =    & get_instance();
		
		$result = 	 $CIH->model_shared->getRecord('name',$tableName,'id',$id);
		
		if ($result->num_rows() > 0 ) {
				
				 $row = $result->row_array();
				 return $row['name'];
		
		} else {
				return 0;
		}
		
	}
	
	
	function getApplicationModuleInfoByID($tableID) {
	 
		$CIH     =   & get_instance();
	
		$return  =   array();
		
		$result  =   $CIH->model_application->getApplicationModuleInfoByID($tableID);	
		
		 if ($result) {
			 
			 return $result; 
			 
		 } else {
			
			 return false;	
		}
    }
	
	
	function getOrganizationConfiguration($organizationID) {
	 
		$CIH     =   & get_instance();
		$return  =   array();
		
		$result  =   $CIH->model_application->getOrganizationConfiguration($organizationID);	
		
		 if ($result) {
			 
			 $row = $result->row_array();
			 
			 		if ($row) {
						
							$return['tableID']    				 				= $row['tableID'];
							$return['currencyID']    				 			= $row['currencyID'];
							$return['currencyCode']    				 			= $row['currencyCode'];
							$return['currencySymbol']    				 		= $row['currencySymbol'];
							$return['dateFormatID']								= $row['dateFormatID'];
							$return['dateFormatDescription']					= $row['dateFormatDescription'];
							$return['timeFormatID']  	  					 	= $row['timeFormatID'];
							$return['timeFormatDescription']					= $row['timeFormatDescription'];
					} 
			 
			 return $return; 
			 
		 } else {
			
			 return false;	
		}
 }
	
	function applicationCurrencyTag($TAG = NULL) {
			
				$CIH     					 =   & get_instance();
				$organizationID				 =   $CIH->session->userdata(USER_ORGANIZATION_ID_SESSION);
				$organizationConfiguration 	 =   getOrganizationConfiguration($organizationID); // Calling From Application Helper
				
				if ($TAG == 'code') {
						
						$currencyTAG 			= $organizationConfiguration['currencyCode'];
						$currencyTAG 			= fillBrackets($currencyTAG); // Calling From General Helper
				
				} else if ($TAG == 'symbol') {
							
							$currencyTAG 			= $organizationConfiguration['currencySymbol'];
				} else {
						    
							$currencyTAG 			= $organizationConfiguration['currencySymbol'];
				}
				
				if ($currencyTAG) {
							
								$currencyCode = $currencyTAG;
				} else {
						
								$currencyCode = NULL;	
				}
				
				return $currencyCode;
	}
 
 	function applicationDateFormat($date=NULL) {
				
				$CIH     						=   & get_instance();
				$organizationID					=   $CIH->session->userdata(USER_ORGANIZATION_ID_SESSION);
				
				$organizationConfiguration 		= getOrganizationConfiguration($organizationID); // Calling From Application Helper
				
				if ($organizationConfiguration) {
					
					$savedDateFormatID	=	$organizationConfiguration['dateFormatID'];
					
				} else {
						
						$savedDateFormatID	=	1;
				}
				
				if ($date == NULL)	{
						
						$date = time();
				}
			
				if ($savedDateFormatID == 1) {
						
							$applicationDateFormat 	= dateMonthDayYear2Digits($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 2) {
							
							$applicationDateFormat 	= dateMonthDayYear($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 3) {
							
							$applicationDateFormat 	= dateFourDigitYearMonthDayWithSlashes($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 4) {
							
							$applicationDateFormat 	= dateYearMonthDayWithDashes($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 5) {
							
							$applicationDateFormat 	= dateDayMonthFourDigitYearWithDots($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 6) {
							
							$applicationDateFormat 	= dateDayMonthFourDigitYearWithDashes($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 7) {
							
							$applicationDateFormat 	= dateDayTextualMonthYear($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 8) {
							
							$applicationDateFormat 	= dateTextualMonthDayYear($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 9) {
							
							$applicationDateFormat 	= dateMonthAbbreviationDayYear($date); // Calling From General Helper
				
				} else if ($savedDateFormatID == 10) {
							
							$applicationDateFormat 	= dateYearMonthAbbreviationDay($date); // Calling From General Helper
				}
				
				
				return $applicationDateFormat;		
	}
	
	function applicationNumberFormat($number=NULL) {
				
				if ($number === NULL) {
						$number = 0;
				}
				
				$applicationNumberFormat =  number_format($number); //number_format((float)$number, 2, '.', '');
				
				return $applicationNumberFormat;		
	}
	
	function JAVASCRIPT_DATE_FORMAT($date=NULL) {
						
						if ($date == NULL)	{
						
								$date = time();
						}
						
						$result = date('d-m-Y',$date);	
						
						return $result;
	}
	
	
	function APPLICATION_DATE_FORMAT($date=NULL) {
		
			if ($date) {
				
				$date = convertStrToTime($date); // Calling From General Helper
				
				$date = date('d-m-Y',$date);
				
				return $date;
				
			} else {
			
				return NOT_AVAILABLE;
			
			}
	}
	
	function distroySessions() {
				
		$arraySessions = array(USER_ACCOUNT_ID_SESSION => '',USER_TEAM_ID_SESSION => '',USER_HR_ID_SESSION => '',USER_ORGANIZATION_ID_SESSION => '',USER_ACCOUNT_STATUS_SESSION =>  '',USER_NAME_SESSION => '',USER_PHOTO_SESSION => '',USER_LOGIN_SESSION => FALSE);
		sessionDistroy($arraySessions); // Calling From Application Helper
								
	}
	
	function sessionDistroy($sessionName) {
				 
				$CIH = & get_instance();
				$CIH->session->unset_userdata($sessionName);
		   
		   return true;
	}
 
?>